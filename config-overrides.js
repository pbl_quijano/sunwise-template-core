const { alias } = require('react-app-rewire-alias');
const path = require('path');

module.exports = {
    paths: function (paths, env) {
        paths.appIndexJs = path.resolve(__dirname, 'src/app.js');
        paths.appSrc = path.resolve(__dirname, 'src');
        return paths;
    },
};

module.exports = function override(config) {
    alias({
        '@api': 'src/api',
        '@components': 'src/components',
        '@constants': 'src/constants',
        '@helpers': 'src/helpers',
        '@main': 'src/modules/main',
        '@models': 'src/models',
        '@modules': 'src/modules',
        '@orm': 'src/orm',
        '@res': 'src/res',
        '@templateCore': 'src/modules/TemplateCore',
    })(config);
    config.entry = `./src/sandbox/app.js`;
    return { ...config };
};
