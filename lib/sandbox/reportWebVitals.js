"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var reportWebVitals = function reportWebVitals(onPerfEntry) {
  if (onPerfEntry && onPerfEntry instanceof Function) {
    Promise.resolve().then(function () {
      return _interopRequireWildcard(require('web-vitals'));
    }).then(function (_ref) {
      var getCLS = _ref.getCLS,
          getFID = _ref.getFID,
          getFCP = _ref.getFCP,
          getLCP = _ref.getLCP,
          getTTFB = _ref.getTTFB;
      getCLS(onPerfEntry);
      getFID(onPerfEntry);
      getFCP(onPerfEntry);
      getLCP(onPerfEntry);
      getTTFB(onPerfEntry);
    });
  }
};

var _default = reportWebVitals;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zYW5kYm94L3JlcG9ydFdlYlZpdGFscy5qcyJdLCJuYW1lcyI6WyJyZXBvcnRXZWJWaXRhbHMiLCJvblBlcmZFbnRyeSIsIkZ1bmN0aW9uIiwidGhlbiIsImdldENMUyIsImdldEZJRCIsImdldEZDUCIsImdldExDUCIsImdldFRURkIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLFdBQUQsRUFBaUI7QUFDckMsTUFBSUEsV0FBVyxJQUFJQSxXQUFXLFlBQVlDLFFBQTFDLEVBQW9EO0FBQ2hEO0FBQUEsNkNBQU8sWUFBUDtBQUFBLE9BQXFCQyxJQUFyQixDQUNJLGdCQUFpRDtBQUFBLFVBQTlDQyxNQUE4QyxRQUE5Q0EsTUFBOEM7QUFBQSxVQUF0Q0MsTUFBc0MsUUFBdENBLE1BQXNDO0FBQUEsVUFBOUJDLE1BQThCLFFBQTlCQSxNQUE4QjtBQUFBLFVBQXRCQyxNQUFzQixRQUF0QkEsTUFBc0I7QUFBQSxVQUFkQyxPQUFjLFFBQWRBLE9BQWM7QUFDN0NKLE1BQUFBLE1BQU0sQ0FBQ0gsV0FBRCxDQUFOO0FBQ0FJLE1BQUFBLE1BQU0sQ0FBQ0osV0FBRCxDQUFOO0FBQ0FLLE1BQUFBLE1BQU0sQ0FBQ0wsV0FBRCxDQUFOO0FBQ0FNLE1BQUFBLE1BQU0sQ0FBQ04sV0FBRCxDQUFOO0FBQ0FPLE1BQUFBLE9BQU8sQ0FBQ1AsV0FBRCxDQUFQO0FBQ0gsS0FQTDtBQVNIO0FBQ0osQ0FaRDs7ZUFjZUQsZSIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHJlcG9ydFdlYlZpdGFscyA9IChvblBlcmZFbnRyeSkgPT4ge1xuICAgIGlmIChvblBlcmZFbnRyeSAmJiBvblBlcmZFbnRyeSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XG4gICAgICAgIGltcG9ydCgnd2ViLXZpdGFscycpLnRoZW4oXG4gICAgICAgICAgICAoeyBnZXRDTFMsIGdldEZJRCwgZ2V0RkNQLCBnZXRMQ1AsIGdldFRURkIgfSkgPT4ge1xuICAgICAgICAgICAgICAgIGdldENMUyhvblBlcmZFbnRyeSk7XG4gICAgICAgICAgICAgICAgZ2V0RklEKG9uUGVyZkVudHJ5KTtcbiAgICAgICAgICAgICAgICBnZXRGQ1Aob25QZXJmRW50cnkpO1xuICAgICAgICAgICAgICAgIGdldExDUChvblBlcmZFbnRyeSk7XG4gICAgICAgICAgICAgICAgZ2V0VFRGQihvblBlcmZFbnRyeSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgcmVwb3J0V2ViVml0YWxzO1xuIl19