"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = configureStore;

var _redux = require("redux");

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _reducer = _interopRequireDefault(require("../reducer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import { reducer as formReducer } from 'redux-form';
function configureStore() {
  var rootReducer = (0, _redux.combineReducers)({
    sunwiseTemplateCore: _reducer.default
  });
  var store = (0, _redux.createStore)(rootReducer, {}, (0, _redux.applyMiddleware)(_reduxThunk.default));
  return store;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zYW5kYm94L3N0b3JlLmpzIl0sIm5hbWVzIjpbImNvbmZpZ3VyZVN0b3JlIiwicm9vdFJlZHVjZXIiLCJzdW53aXNlVGVtcGxhdGVDb3JlIiwicmVkdWNlciIsInN0b3JlIiwidGh1bmsiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFFQTs7OztBQUhBO0FBS2UsU0FBU0EsY0FBVCxHQUEwQjtBQUNyQyxNQUFNQyxXQUFXLEdBQUcsNEJBQWdCO0FBQUVDLElBQUFBLG1CQUFtQixFQUFFQztBQUF2QixHQUFoQixDQUFwQjtBQUVBLE1BQU1DLEtBQUssR0FBRyx3QkFBWUgsV0FBWixFQUF5QixFQUF6QixFQUE2Qiw0QkFBZ0JJLG1CQUFoQixDQUE3QixDQUFkO0FBRUEsU0FBT0QsS0FBUDtBQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgYXBwbHlNaWRkbGV3YXJlLCBjb21iaW5lUmVkdWNlcnMsIGNyZWF0ZVN0b3JlIH0gZnJvbSAncmVkdXgnO1xyXG4vLyBpbXBvcnQgeyByZWR1Y2VyIGFzIGZvcm1SZWR1Y2VyIH0gZnJvbSAncmVkdXgtZm9ybSc7XHJcbmltcG9ydCB0aHVuayBmcm9tICdyZWR1eC10aHVuayc7XHJcblxyXG5pbXBvcnQgcmVkdWNlciBmcm9tICcuLi9yZWR1Y2VyJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGNvbmZpZ3VyZVN0b3JlKCkge1xyXG4gICAgY29uc3Qgcm9vdFJlZHVjZXIgPSBjb21iaW5lUmVkdWNlcnMoeyBzdW53aXNlVGVtcGxhdGVDb3JlOiByZWR1Y2VyIH0pO1xyXG5cclxuICAgIGNvbnN0IHN0b3JlID0gY3JlYXRlU3RvcmUocm9vdFJlZHVjZXIsIHt9LCBhcHBseU1pZGRsZXdhcmUodGh1bmspKTtcclxuXHJcbiAgICByZXR1cm4gc3RvcmU7XHJcbn1cclxuIl19