"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactRedux = require("react-redux");

var _Example = _interopRequireDefault(require("./Example"));

var _reportWebVitals = _interopRequireDefault(require("./reportWebVitals"));

var _store = _interopRequireDefault(require("./store"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var store = (0, _store.default)();

_reactDom.default.render( /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactRedux.Provider, {
  store: store,
  children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Example.default, {})
}), document.getElementById('root')); // If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals


(0, _reportWebVitals.default)();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zYW5kYm94L2FwcC5qcyJdLCJuYW1lcyI6WyJzdG9yZSIsIlJlYWN0RE9NIiwicmVuZGVyIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7Ozs7O0FBRUEsSUFBTUEsS0FBSyxHQUFHLHFCQUFkOztBQUVBQyxrQkFBU0MsTUFBVCxlQUNJLHFCQUFDLG9CQUFEO0FBQVUsRUFBQSxLQUFLLEVBQUVGLEtBQWpCO0FBQUEseUJBQ0kscUJBQUMsZ0JBQUQ7QUFESixFQURKLEVBSUlHLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixNQUF4QixDQUpKLEUsQ0FPQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBSZWFjdERPTSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IHsgUHJvdmlkZXIgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCBFeGFtcGxlIGZyb20gJy4vRXhhbXBsZSc7XG5pbXBvcnQgcmVwb3J0V2ViVml0YWxzIGZyb20gJy4vcmVwb3J0V2ViVml0YWxzJztcbmltcG9ydCBjb25maWd1cmVTdG9yZSBmcm9tICcuL3N0b3JlJztcblxuY29uc3Qgc3RvcmUgPSBjb25maWd1cmVTdG9yZSgpO1xuXG5SZWFjdERPTS5yZW5kZXIoXG4gICAgPFByb3ZpZGVyIHN0b3JlPXtzdG9yZX0+XG4gICAgICAgIDxFeGFtcGxlIC8+XG4gICAgPC9Qcm92aWRlcj4sXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jvb3QnKVxuKTtcblxuLy8gSWYgeW91IHdhbnQgdG8gc3RhcnQgbWVhc3VyaW5nIHBlcmZvcm1hbmNlIGluIHlvdXIgYXBwLCBwYXNzIGEgZnVuY3Rpb25cbi8vIHRvIGxvZyByZXN1bHRzIChmb3IgZXhhbXBsZTogcmVwb3J0V2ViVml0YWxzKGNvbnNvbGUubG9nKSlcbi8vIG9yIHNlbmQgdG8gYW4gYW5hbHl0aWNzIGVuZHBvaW50LiBMZWFybiBtb3JlOiBodHRwczovL2JpdC5seS9DUkEtdml0YWxzXG5yZXBvcnRXZWJWaXRhbHMoKTtcbiJdfQ==