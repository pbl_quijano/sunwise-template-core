"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _WidgetSideMenu = _interopRequireDefault(require("../components/WidgetSideMenu"));

var _types = require("../constants/types");

var _TemplateView = _interopRequireDefault(require("../modules/TemplateView"));

var _PageToolbar = _interopRequireDefault(require("../modules/TemplateView/components/PageToolbar"));

var _withTemplateCore = _interopRequireDefault(require("../hocs/withTemplateCore"));

var _mockup = _interopRequireDefault(require("./mockup"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    width: 100%;\n    background-color: aliceblue;\n"])));

var App = function App(_ref) {
  var initTemplate = _ref.initTemplate;
  (0, _react.useEffect)(function () {
    initTemplate(_mockup.default); //initTemplate(mockup, 'cfdbdec7-87da-4884-931a-01638691c371');
  }, []);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_PageToolbar.default, {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_TemplateView.default, {
      editionLevel: _types.FULL_EDITION_MODE
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetSideMenu.default, {})]
  });
};

App.propTypes = {
  initTemplate: _propTypes.default.func
};

var _default = (0, _withTemplateCore.default)(function () {
  return {
    baseUrl: 'https://test.sunwise.mx',
    token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiZGY5MDI0NzItNDUwMC00MjYyLWFlZWMtZDM3ODc0MDU1OWM2IiwidXNlcm5hbWUiOiJoYW5kQHN1bndpc2UubXgiLCJleHAiOjE2NDQ2Mjk5NjAsImVtYWlsIjoiaGFuZEBzdW53aXNlLm14In0.nQxgcLGGZTxrMW_0QvTXCJdi7M8Pvdb28oAToaqip7M',
    language: 'es-mx',
    googleApiKey: 'AIzaSyCn8IpJbEuNmy999nGIHESALg9Kr_Wp81Q'
  };
})(App);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9zYW5kYm94L0V4YW1wbGUuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiQXBwIiwiaW5pdFRlbXBsYXRlIiwibW9ja3VwIiwiRlVMTF9FRElUSU9OX01PREUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwiYmFzZVVybCIsInRva2VuIiwibGFuZ3VhZ2UiLCJnb29nbGVBcGlLZXkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFFQTs7QUFLQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsK0lBQWY7O0FBTUEsSUFBTUMsR0FBRyxHQUFHLFNBQU5BLEdBQU0sT0FBc0I7QUFBQSxNQUFuQkMsWUFBbUIsUUFBbkJBLFlBQW1CO0FBQzlCLHdCQUFVLFlBQU07QUFDWkEsSUFBQUEsWUFBWSxDQUFDQyxlQUFELENBQVosQ0FEWSxDQUVaO0FBQ0gsR0FIRCxFQUdHLEVBSEg7QUFJQSxzQkFDSSxzQkFBQyxTQUFEO0FBQUEsNEJBQ0kscUJBQUMsb0JBQUQsS0FESixlQUVJLHFCQUFDLHFCQUFEO0FBQWMsTUFBQSxZQUFZLEVBQUVDO0FBQTVCLE1BRkosZUFHSSxxQkFBQyx1QkFBRCxLQUhKO0FBQUEsSUFESjtBQU9ILENBWkQ7O0FBY0FILEdBQUcsQ0FBQ0ksU0FBSixHQUFnQjtBQUNaSCxFQUFBQSxZQUFZLEVBQUVJLG1CQUFVQztBQURaLENBQWhCOztlQUllLCtCQUFpQjtBQUFBLFNBQU87QUFDbkNDLElBQUFBLE9BQU8sRUFBRSx5QkFEMEI7QUFFbkNDLElBQUFBLEtBQUssRUFBRSxzUEFGNEI7QUFHbkNDLElBQUFBLFFBQVEsRUFBRSxPQUh5QjtBQUluQ0MsSUFBQUEsWUFBWSxFQUFFO0FBSnFCLEdBQVA7QUFBQSxDQUFqQixFQUtYVixHQUxXLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBXaWRnZXRTaWRlTWVudSBmcm9tICdAY29tcG9uZW50cy9XaWRnZXRTaWRlTWVudSc7XG5cbmltcG9ydCB7IEZVTExfRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmltcG9ydCBUZW1wbGF0ZVZpZXcgZnJvbSAnQG1vZHVsZXMvVGVtcGxhdGVWaWV3JztcbmltcG9ydCBQYWdlVG9vbGJhciBmcm9tICdAbW9kdWxlcy9UZW1wbGF0ZVZpZXcvY29tcG9uZW50cy9QYWdlVG9vbGJhcic7XG5cbmltcG9ydCB3aXRoVGVtcGxhdGVDb3JlIGZyb20gJy4uL2hvY3Mvd2l0aFRlbXBsYXRlQ29yZSc7XG5cbi8vIGltcG9ydCB7IFRlbXBsYXRlVmlldywgd2l0aFRlbXBsYXRlQ29yZSB9IGZyb20gJy4uL2xpYic7XG4vLyBpbXBvcnQgeyBQQVJUSUFMX0VESVRJT05fTU9ERSB9IGZyb20gJy4uL2xpYi9jb25zdGFudHMvdHlwZXMnO1xuXG5pbXBvcnQgbW9ja3VwIGZyb20gJy4vbW9ja3VwJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IGFsaWNlYmx1ZTtcbmA7XG5cbmNvbnN0IEFwcCA9ICh7IGluaXRUZW1wbGF0ZSB9KSA9PiB7XG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaW5pdFRlbXBsYXRlKG1vY2t1cCk7XG4gICAgICAgIC8vaW5pdFRlbXBsYXRlKG1vY2t1cCwgJ2NmZGJkZWM3LTg3ZGEtNDg4NC05MzFhLTAxNjM4NjkxYzM3MScpO1xuICAgIH0sIFtdKTtcbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgPFBhZ2VUb29sYmFyIC8+XG4gICAgICAgICAgICA8VGVtcGxhdGVWaWV3IGVkaXRpb25MZXZlbD17RlVMTF9FRElUSU9OX01PREV9IC8+XG4gICAgICAgICAgICA8V2lkZ2V0U2lkZU1lbnUgLz5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cbkFwcC5wcm9wVHlwZXMgPSB7XG4gICAgaW5pdFRlbXBsYXRlOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhUZW1wbGF0ZUNvcmUoKCkgPT4gKHtcbiAgICBiYXNlVXJsOiAnaHR0cHM6Ly90ZXN0LnN1bndpc2UubXgnLFxuICAgIHRva2VuOiAnZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5LmV5SjFjMlZ5WDJsa0lqb2laR1k1TURJME56SXRORFV3TUMwME1qWXlMV0ZsWldNdFpETTNPRGMwTURVMU9XTTJJaXdpZFhObGNtNWhiV1VpT2lKb1lXNWtRSE4xYm5kcGMyVXViWGdpTENKbGVIQWlPakUyTkRRMk1qazVOakFzSW1WdFlXbHNJam9pYUdGdVpFQnpkVzUzYVhObExtMTRJbjAublF4Z2NMR0daVHhyTVdfMFF2VFhDSmRpN004UHZkYjI4b0FUb2FxaXA3TScsXG4gICAgbGFuZ3VhZ2U6ICdlcy1teCcsXG4gICAgZ29vZ2xlQXBpS2V5OiAnQUl6YVN5Q244SXBKYkV1Tm15OTk5bkdJSEVTQUxnOUtyX1dwODFRJyxcbn0pKShBcHApO1xuIl19