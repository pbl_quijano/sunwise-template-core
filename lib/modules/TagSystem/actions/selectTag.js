"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _initializeTagLibraryForm = _interopRequireDefault(require("./initializeTagLibraryForm"));

var _resetTagLibraryForm = _interopRequireDefault(require("./resetTagLibraryForm"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(values, hasProposalSelector) {
  return function (dispatch) {
    dispatch((0, _resetTagLibraryForm.default)());
    dispatch({
      type: _actionTypes.SELECT_TAG,
      payload: values && values.tag
    });

    if (values) {
      dispatch((0, _initializeTagLibraryForm.default)(_objectSpread(_objectSpread({}, values), {}, {
        default_value: values.default_label,
        title: values.label,
        hasProposalSelector: hasProposalSelector
      })));
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3NlbGVjdFRhZy5qcyJdLCJuYW1lcyI6WyJ2YWx1ZXMiLCJoYXNQcm9wb3NhbFNlbGVjdG9yIiwiZGlzcGF0Y2giLCJ0eXBlIiwiU0VMRUNUX1RBRyIsInBheWxvYWQiLCJ0YWciLCJkZWZhdWx0X3ZhbHVlIiwiZGVmYXVsdF9sYWJlbCIsInRpdGxlIiwibGFiZWwiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxNQUFELEVBQVNDLG1CQUFUO0FBQUEsU0FBaUMsVUFBQ0MsUUFBRCxFQUFjO0FBQzFEQSxJQUFBQSxRQUFRLENBQUMsbUNBQUQsQ0FBUjtBQUNBQSxJQUFBQSxRQUFRLENBQUM7QUFBRUMsTUFBQUEsSUFBSSxFQUFFQyx1QkFBUjtBQUFvQkMsTUFBQUEsT0FBTyxFQUFFTCxNQUFNLElBQUlBLE1BQU0sQ0FBQ007QUFBOUMsS0FBRCxDQUFSOztBQUNBLFFBQUlOLE1BQUosRUFBWTtBQUNSRSxNQUFBQSxRQUFRLENBQ0osdUVBQ09GLE1BRFA7QUFFSU8sUUFBQUEsYUFBYSxFQUFFUCxNQUFNLENBQUNRLGFBRjFCO0FBR0lDLFFBQUFBLEtBQUssRUFBRVQsTUFBTSxDQUFDVSxLQUhsQjtBQUlJVCxRQUFBQSxtQkFBbUIsRUFBbkJBO0FBSkosU0FESSxDQUFSO0FBUUg7QUFDSixHQWJjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFTEVDVF9UQUcgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmltcG9ydCBpbml0aWFsaXplVGFnTGlicmFyeUZvcm0gZnJvbSAnLi9pbml0aWFsaXplVGFnTGlicmFyeUZvcm0nO1xuaW1wb3J0IHJlc2V0VGFnTGlicmFyeUZvcm0gZnJvbSAnLi9yZXNldFRhZ0xpYnJhcnlGb3JtJztcblxuZXhwb3J0IGRlZmF1bHQgKHZhbHVlcywgaGFzUHJvcG9zYWxTZWxlY3RvcikgPT4gKGRpc3BhdGNoKSA9PiB7XG4gICAgZGlzcGF0Y2gocmVzZXRUYWdMaWJyYXJ5Rm9ybSgpKTtcbiAgICBkaXNwYXRjaCh7IHR5cGU6IFNFTEVDVF9UQUcsIHBheWxvYWQ6IHZhbHVlcyAmJiB2YWx1ZXMudGFnIH0pO1xuICAgIGlmICh2YWx1ZXMpIHtcbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICBpbml0aWFsaXplVGFnTGlicmFyeUZvcm0oe1xuICAgICAgICAgICAgICAgIC4uLnZhbHVlcyxcbiAgICAgICAgICAgICAgICBkZWZhdWx0X3ZhbHVlOiB2YWx1ZXMuZGVmYXVsdF9sYWJlbCxcbiAgICAgICAgICAgICAgICB0aXRsZTogdmFsdWVzLmxhYmVsLFxuICAgICAgICAgICAgICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cbn07XG4iXX0=