"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(isOpenModal) {
  return {
    type: _actionTypes.SET_IS_OPEN_MODAL,
    payload: isOpenModal
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3NldElzT3Blbk1vZGFsLmpzIl0sIm5hbWVzIjpbImlzT3Blbk1vZGFsIiwidHlwZSIsIlNFVF9JU19PUEVOX01PREFMIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxXQUFEO0FBQUEsU0FBa0I7QUFDN0JDLElBQUFBLElBQUksRUFBRUMsOEJBRHVCO0FBRTdCQyxJQUFBQSxPQUFPLEVBQUVIO0FBRm9CLEdBQWxCO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9JU19PUEVOX01PREFMIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAoaXNPcGVuTW9kYWwpID0+ICh7XG4gICAgdHlwZTogU0VUX0lTX09QRU5fTU9EQUwsXG4gICAgcGF5bG9hZDogaXNPcGVuTW9kYWwsXG59KTtcbiJdfQ==