"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(contentViewState) {
  return {
    type: _actionTypes.SET_CONTENT_VIEW_STATE,
    payload: contentViewState
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3NldENvbnRlbnRWaWV3U3RhdGUuanMiXSwibmFtZXMiOlsiY29udGVudFZpZXdTdGF0ZSIsInR5cGUiLCJTRVRfQ09OVEVOVF9WSUVXX1NUQVRFIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxnQkFBRDtBQUFBLFNBQXVCO0FBQ2xDQyxJQUFBQSxJQUFJLEVBQUVDLG1DQUQ0QjtBQUVsQ0MsSUFBQUEsT0FBTyxFQUFFSDtBQUZ5QixHQUF2QjtBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTRVRfQ09OVEVOVF9WSUVXX1NUQVRFIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAoY29udGVudFZpZXdTdGF0ZSkgPT4gKHtcbiAgICB0eXBlOiBTRVRfQ09OVEVOVF9WSUVXX1NUQVRFLFxuICAgIHBheWxvYWQ6IGNvbnRlbnRWaWV3U3RhdGUsXG59KTtcbiJdfQ==