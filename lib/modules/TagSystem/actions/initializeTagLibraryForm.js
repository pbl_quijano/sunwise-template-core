"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tags = require("../../../constants/tags");

var _utils = require("../../../helpers/utils");

var _actionTypes = require("../actionTypes");

var _helpers = require("../helpers");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(values) {
  return function (dispatch, getState) {
    if (_tags.TAG_CURRENCY_FORMAT_TYPES.includes(values.format_type) && (0, _utils.hasValue)(values, 'currency') && (0, _utils.hasValue)(values, 'unit')) {
      var currencies = selectors.getCurrenciesData(getState());
      var newCurrency = (0, _helpers.getCurrencyByUnit)(currencies, values.unit);
      var currency = newCurrency.id;
      var symbol = newCurrency.symbol;
      dispatch({
        type: _actionTypes.INITIALIZE_TAG_LIBRARY_FORM,
        payload: _objectSpread(_objectSpread({}, values), {}, {
          currency: currency,
          symbol: symbol
        })
      });
    } else {
      dispatch({
        type: _actionTypes.INITIALIZE_TAG_LIBRARY_FORM,
        payload: _objectSpread({}, values)
      });
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL2luaXRpYWxpemVUYWdMaWJyYXJ5Rm9ybS5qcyJdLCJuYW1lcyI6WyJ2YWx1ZXMiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwiVEFHX0NVUlJFTkNZX0ZPUk1BVF9UWVBFUyIsImluY2x1ZGVzIiwiZm9ybWF0X3R5cGUiLCJjdXJyZW5jaWVzIiwic2VsZWN0b3JzIiwiZ2V0Q3VycmVuY2llc0RhdGEiLCJuZXdDdXJyZW5jeSIsInVuaXQiLCJjdXJyZW5jeSIsImlkIiwic3ltYm9sIiwidHlwZSIsIklOSVRJQUxJWkVfVEFHX0xJQlJBUllfRk9STSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7ZUFFZSxrQkFBQ0EsTUFBRDtBQUFBLFNBQVksVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQy9DLFFBQ0lDLGdDQUEwQkMsUUFBMUIsQ0FBbUNKLE1BQU0sQ0FBQ0ssV0FBMUMsS0FDQSxxQkFBU0wsTUFBVCxFQUFpQixVQUFqQixDQURBLElBRUEscUJBQVNBLE1BQVQsRUFBaUIsTUFBakIsQ0FISixFQUlFO0FBQ0UsVUFBTU0sVUFBVSxHQUFHQyxTQUFTLENBQUNDLGlCQUFWLENBQTRCTixRQUFRLEVBQXBDLENBQW5CO0FBRUEsVUFBTU8sV0FBVyxHQUFHLGdDQUFrQkgsVUFBbEIsRUFBOEJOLE1BQU0sQ0FBQ1UsSUFBckMsQ0FBcEI7QUFDQSxVQUFNQyxRQUFRLEdBQUdGLFdBQVcsQ0FBQ0csRUFBN0I7QUFDQSxVQUFNQyxNQUFNLEdBQUdKLFdBQVcsQ0FBQ0ksTUFBM0I7QUFFQVosTUFBQUEsUUFBUSxDQUFDO0FBQ0xhLFFBQUFBLElBQUksRUFBRUMsd0NBREQ7QUFFTEMsUUFBQUEsT0FBTyxrQ0FBT2hCLE1BQVA7QUFBZVcsVUFBQUEsUUFBUSxFQUFSQSxRQUFmO0FBQXlCRSxVQUFBQSxNQUFNLEVBQU5BO0FBQXpCO0FBRkYsT0FBRCxDQUFSO0FBSUgsS0FmRCxNQWVPO0FBQ0haLE1BQUFBLFFBQVEsQ0FBQztBQUFFYSxRQUFBQSxJQUFJLEVBQUVDLHdDQUFSO0FBQXFDQyxRQUFBQSxPQUFPLG9CQUFPaEIsTUFBUDtBQUE1QyxPQUFELENBQVI7QUFDSDtBQUNKLEdBbkJjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMgfSBmcm9tICdAY29uc3RhbnRzL3RhZ3MnO1xuXG5pbXBvcnQgeyBoYXNWYWx1ZSB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcblxuaW1wb3J0IHsgSU5JVElBTElaRV9UQUdfTElCUkFSWV9GT1JNIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuaW1wb3J0IHsgZ2V0Q3VycmVuY3lCeVVuaXQgfSBmcm9tICcuLi9oZWxwZXJzJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi9zZWxlY3RvcnMnO1xuXG5leHBvcnQgZGVmYXVsdCAodmFsdWVzKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgaWYgKFxuICAgICAgICBUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHZhbHVlcy5mb3JtYXRfdHlwZSkgJiZcbiAgICAgICAgaGFzVmFsdWUodmFsdWVzLCAnY3VycmVuY3knKSAmJlxuICAgICAgICBoYXNWYWx1ZSh2YWx1ZXMsICd1bml0JylcbiAgICApIHtcbiAgICAgICAgY29uc3QgY3VycmVuY2llcyA9IHNlbGVjdG9ycy5nZXRDdXJyZW5jaWVzRGF0YShnZXRTdGF0ZSgpKTtcblxuICAgICAgICBjb25zdCBuZXdDdXJyZW5jeSA9IGdldEN1cnJlbmN5QnlVbml0KGN1cnJlbmNpZXMsIHZhbHVlcy51bml0KTtcbiAgICAgICAgY29uc3QgY3VycmVuY3kgPSBuZXdDdXJyZW5jeS5pZDtcbiAgICAgICAgY29uc3Qgc3ltYm9sID0gbmV3Q3VycmVuY3kuc3ltYm9sO1xuXG4gICAgICAgIGRpc3BhdGNoKHtcbiAgICAgICAgICAgIHR5cGU6IElOSVRJQUxJWkVfVEFHX0xJQlJBUllfRk9STSxcbiAgICAgICAgICAgIHBheWxvYWQ6IHsgLi4udmFsdWVzLCBjdXJyZW5jeSwgc3ltYm9sIH0sXG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGRpc3BhdGNoKHsgdHlwZTogSU5JVElBTElaRV9UQUdfTElCUkFSWV9GT1JNLCBwYXlsb2FkOiB7IC4uLnZhbHVlcyB9IH0pO1xuICAgIH1cbn07XG4iXX0=