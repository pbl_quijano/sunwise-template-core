"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tags = require("../../../constants/tags");

var _initializeCustomTagForm = _interopRequireDefault(require("./initializeCustomTagForm"));

var _setContentViewState = _interopRequireDefault(require("./setContentViewState"));

var _setIsOpenModal = _interopRequireDefault(require("./setIsOpenModal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(values) {
  return function (dispatch) {
    dispatch((0, _setContentViewState.default)(_tags.EDIT_CUSTOM_TAG_STATE));
    dispatch((0, _setIsOpenModal.default)(true));
    dispatch((0, _initializeCustomTagForm.default)(values, true));
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3ByZXBhcmVFZGl0Q3VzdG9tVGFnLmpzIl0sIm5hbWVzIjpbInZhbHVlcyIsImRpc3BhdGNoIiwiRURJVF9DVVNUT01fVEFHX1NUQVRFIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7ZUFFZSxrQkFBQ0EsTUFBRDtBQUFBLFNBQVksVUFBQ0MsUUFBRCxFQUFjO0FBQ3JDQSxJQUFBQSxRQUFRLENBQUMsa0NBQW9CQywyQkFBcEIsQ0FBRCxDQUFSO0FBQ0FELElBQUFBLFFBQVEsQ0FBQyw2QkFBZSxJQUFmLENBQUQsQ0FBUjtBQUNBQSxJQUFBQSxRQUFRLENBQUMsc0NBQXdCRCxNQUF4QixFQUFnQyxJQUFoQyxDQUFELENBQVI7QUFDSCxHQUpjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEVESVRfQ1VTVE9NX1RBR19TVEFURSB9IGZyb20gJ0Bjb25zdGFudHMvdGFncyc7XHJcblxyXG5pbXBvcnQgaW5pdGlhbGl6ZUN1c3RvbVRhZ0Zvcm0gZnJvbSAnLi9pbml0aWFsaXplQ3VzdG9tVGFnRm9ybSc7XHJcbmltcG9ydCBzZXRDb250ZW50Vmlld1N0YXRlIGZyb20gJy4vc2V0Q29udGVudFZpZXdTdGF0ZSc7XHJcbmltcG9ydCBzZXRJc09wZW5Nb2RhbCBmcm9tICcuL3NldElzT3Blbk1vZGFsJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0ICh2YWx1ZXMpID0+IChkaXNwYXRjaCkgPT4ge1xyXG4gICAgZGlzcGF0Y2goc2V0Q29udGVudFZpZXdTdGF0ZShFRElUX0NVU1RPTV9UQUdfU1RBVEUpKTtcclxuICAgIGRpc3BhdGNoKHNldElzT3Blbk1vZGFsKHRydWUpKTtcclxuICAgIGRpc3BhdGNoKGluaXRpYWxpemVDdXN0b21UYWdGb3JtKHZhbHVlcywgdHJ1ZSkpO1xyXG59O1xyXG4iXX0=