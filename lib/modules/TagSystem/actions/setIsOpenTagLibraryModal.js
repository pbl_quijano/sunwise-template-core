"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(isOpenModal) {
  return {
    type: _actionTypes.SET_IS_OPEN_TAG_LIBARY_MODAL,
    payload: isOpenModal
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3NldElzT3BlblRhZ0xpYnJhcnlNb2RhbC5qcyJdLCJuYW1lcyI6WyJpc09wZW5Nb2RhbCIsInR5cGUiLCJTRVRfSVNfT1BFTl9UQUdfTElCQVJZX01PREFMIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxXQUFEO0FBQUEsU0FBa0I7QUFDN0JDLElBQUFBLElBQUksRUFBRUMseUNBRHVCO0FBRTdCQyxJQUFBQSxPQUFPLEVBQUVIO0FBRm9CLEdBQWxCO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9JU19PUEVOX1RBR19MSUJBUllfTU9EQUwgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0IChpc09wZW5Nb2RhbCkgPT4gKHtcbiAgICB0eXBlOiBTRVRfSVNfT1BFTl9UQUdfTElCQVJZX01PREFMLFxuICAgIHBheWxvYWQ6IGlzT3Blbk1vZGFsLFxufSk7XG4iXX0=