"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _helpers = require("../helpers");

var selectors = _interopRequireWildcard(require("../selectors"));

var _resetCustomTagForm = _interopRequireDefault(require("./resetCustomTagForm"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(values, isEditMode) {
  return function (dispatch, getState) {
    dispatch((0, _resetCustomTagForm.default)());
    var calculator = values.calculator;
    var currencies = selectors.getCurrenciesData(getState());
    dispatch({
      type: _actionTypes.INITIALIZE_CUSTOM_TAG_FORM,
      payload: _objectSpread(_objectSpread({}, values), {}, {
        calculator: isEditMode ? (0, _helpers.updateMultipleCurrencyTags)(calculator, currencies) : calculator
      })
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL2luaXRpYWxpemVDdXN0b21UYWdGb3JtLmpzIl0sIm5hbWVzIjpbInZhbHVlcyIsImlzRWRpdE1vZGUiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwiY2FsY3VsYXRvciIsImN1cnJlbmNpZXMiLCJzZWxlY3RvcnMiLCJnZXRDdXJyZW5jaWVzRGF0YSIsInR5cGUiLCJJTklUSUFMSVpFX0NVU1RPTV9UQUdfRk9STSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxNQUFELEVBQVNDLFVBQVQ7QUFBQSxTQUF3QixVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDM0RELElBQUFBLFFBQVEsQ0FBQyxrQ0FBRCxDQUFSO0FBQ0EsUUFBUUUsVUFBUixHQUF1QkosTUFBdkIsQ0FBUUksVUFBUjtBQUNBLFFBQU1DLFVBQVUsR0FBR0MsU0FBUyxDQUFDQyxpQkFBVixDQUE0QkosUUFBUSxFQUFwQyxDQUFuQjtBQUVBRCxJQUFBQSxRQUFRLENBQUM7QUFDTE0sTUFBQUEsSUFBSSxFQUFFQyx1Q0FERDtBQUVMQyxNQUFBQSxPQUFPLGtDQUNBVixNQURBO0FBRUhJLFFBQUFBLFVBQVUsRUFBRUgsVUFBVSxHQUNoQix5Q0FBMkJHLFVBQTNCLEVBQXVDQyxVQUF2QyxDQURnQixHQUVoQkQ7QUFKSDtBQUZGLEtBQUQsQ0FBUjtBQVNILEdBZGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU5JVElBTElaRV9DVVNUT01fVEFHX0ZPUk0gfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQgeyB1cGRhdGVNdWx0aXBsZUN1cnJlbmN5VGFncyB9IGZyb20gJy4uL2hlbHBlcnMnO1xuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJy4uL3NlbGVjdG9ycyc7XG5cbmltcG9ydCByZXNldEN1c3RvbVRhZ0Zvcm0gZnJvbSAnLi9yZXNldEN1c3RvbVRhZ0Zvcm0nO1xuXG5leHBvcnQgZGVmYXVsdCAodmFsdWVzLCBpc0VkaXRNb2RlKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgZGlzcGF0Y2gocmVzZXRDdXN0b21UYWdGb3JtKCkpO1xuICAgIGNvbnN0IHsgY2FsY3VsYXRvciB9ID0gdmFsdWVzO1xuICAgIGNvbnN0IGN1cnJlbmNpZXMgPSBzZWxlY3RvcnMuZ2V0Q3VycmVuY2llc0RhdGEoZ2V0U3RhdGUoKSk7XG5cbiAgICBkaXNwYXRjaCh7XG4gICAgICAgIHR5cGU6IElOSVRJQUxJWkVfQ1VTVE9NX1RBR19GT1JNLFxuICAgICAgICBwYXlsb2FkOiB7XG4gICAgICAgICAgICAuLi52YWx1ZXMsXG4gICAgICAgICAgICBjYWxjdWxhdG9yOiBpc0VkaXRNb2RlXG4gICAgICAgICAgICAgICAgPyB1cGRhdGVNdWx0aXBsZUN1cnJlbmN5VGFncyhjYWxjdWxhdG9yLCBjdXJyZW5jaWVzKVxuICAgICAgICAgICAgICAgIDogY2FsY3VsYXRvcixcbiAgICAgICAgfSxcbiAgICB9KTtcbn07XG4iXX0=