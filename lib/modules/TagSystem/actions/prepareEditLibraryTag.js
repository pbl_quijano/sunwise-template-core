"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tags = require("../../../constants/tags");

var _initializeTagLibraryForm = _interopRequireDefault(require("./initializeTagLibraryForm"));

var _setContentViewState = _interopRequireDefault(require("./setContentViewState"));

var _setIsOpenModal = _interopRequireDefault(require("./setIsOpenModal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(values) {
  return function (dispatch) {
    dispatch((0, _setContentViewState.default)(_tags.EDIT_LIBRARY_TAG_STATE));
    dispatch((0, _setIsOpenModal.default)(true));
    dispatch((0, _initializeTagLibraryForm.default)(values));
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3ByZXBhcmVFZGl0TGlicmFyeVRhZy5qcyJdLCJuYW1lcyI6WyJ2YWx1ZXMiLCJkaXNwYXRjaCIsIkVESVRfTElCUkFSWV9UQUdfU1RBVEUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFDQTs7QUFDQTs7OztlQUVlLGtCQUFDQSxNQUFEO0FBQUEsU0FBWSxVQUFDQyxRQUFELEVBQWM7QUFDckNBLElBQUFBLFFBQVEsQ0FBQyxrQ0FBb0JDLDRCQUFwQixDQUFELENBQVI7QUFDQUQsSUFBQUEsUUFBUSxDQUFDLDZCQUFlLElBQWYsQ0FBRCxDQUFSO0FBQ0FBLElBQUFBLFFBQVEsQ0FBQyx1Q0FBeUJELE1BQXpCLENBQUQsQ0FBUjtBQUNILEdBSmM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRURJVF9MSUJSQVJZX1RBR19TVEFURSB9IGZyb20gJ0Bjb25zdGFudHMvdGFncyc7XHJcblxyXG5pbXBvcnQgaW5pdGlhbGl6ZVRhZ0xpYnJhcnlGb3JtIGZyb20gJy4vaW5pdGlhbGl6ZVRhZ0xpYnJhcnlGb3JtJztcclxuaW1wb3J0IHNldENvbnRlbnRWaWV3U3RhdGUgZnJvbSAnLi9zZXRDb250ZW50Vmlld1N0YXRlJztcclxuaW1wb3J0IHNldElzT3Blbk1vZGFsIGZyb20gJy4vc2V0SXNPcGVuTW9kYWwnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgKHZhbHVlcykgPT4gKGRpc3BhdGNoKSA9PiB7XHJcbiAgICBkaXNwYXRjaChzZXRDb250ZW50Vmlld1N0YXRlKEVESVRfTElCUkFSWV9UQUdfU1RBVEUpKTtcclxuICAgIGRpc3BhdGNoKHNldElzT3Blbk1vZGFsKHRydWUpKTtcclxuICAgIGRpc3BhdGNoKGluaXRpYWxpemVUYWdMaWJyYXJ5Rm9ybSh2YWx1ZXMpKTtcclxufTtcclxuIl19