"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(value) {
  return {
    type: _actionTypes.SELECT_CATEGORY,
    payload: value
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3NlbGVjdENhdGVnb3J5LmpzIl0sIm5hbWVzIjpbInZhbHVlIiwidHlwZSIsIlNFTEVDVF9DQVRFR09SWSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsS0FBRDtBQUFBLFNBQVk7QUFBRUMsSUFBQUEsSUFBSSxFQUFFQyw0QkFBUjtBQUF5QkMsSUFBQUEsT0FBTyxFQUFFSDtBQUFsQyxHQUFaO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFTEVDVF9DQVRFR09SWSB9IGZyb20gJy4uL2FjdGlvblR5cGVzJztcblxuZXhwb3J0IGRlZmF1bHQgKHZhbHVlKSA9PiAoeyB0eXBlOiBTRUxFQ1RfQ0FURUdPUlksIHBheWxvYWQ6IHZhbHVlIH0pO1xuIl19