"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(values) {
  return {
    type: _actionTypes.FILTER_TAGS,
    payload: values
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL2ZpbHRlclRhZ3MuanMiXSwibmFtZXMiOlsidmFsdWVzIiwidHlwZSIsIkZJTFRFUl9UQUdTIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxNQUFEO0FBQUEsU0FBYTtBQUFFQyxJQUFBQSxJQUFJLEVBQUVDLHdCQUFSO0FBQXFCQyxJQUFBQSxPQUFPLEVBQUVIO0FBQTlCLEdBQWI7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRklMVEVSX1RBR1MgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0ICh2YWx1ZXMpID0+ICh7IHR5cGU6IEZJTFRFUl9UQUdTLCBwYXlsb2FkOiB2YWx1ZXMgfSk7XG4iXX0=