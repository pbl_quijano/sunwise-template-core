"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxForm = require("redux-form");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var _default = function _default(values) {
  return function (dispatch, getState) {
    var calculator = selectors.getDataCalculator(getState());
    var newData = [].concat(_toConsumableArray(calculator), [values]);
    dispatch((0, _reduxForm.change)('custom-tag-form', 'calculator', newData));
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL2FkZFRhZ1RvQ2FsY3VsYXRvci5qcyJdLCJuYW1lcyI6WyJ2YWx1ZXMiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwiY2FsY3VsYXRvciIsInNlbGVjdG9ycyIsImdldERhdGFDYWxjdWxhdG9yIiwibmV3RGF0YSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxNQUFEO0FBQUEsU0FBWSxVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDL0MsUUFBTUMsVUFBVSxHQUFHQyxTQUFTLENBQUNDLGlCQUFWLENBQTRCSCxRQUFRLEVBQXBDLENBQW5CO0FBQ0EsUUFBTUksT0FBTyxnQ0FBT0gsVUFBUCxJQUFtQkgsTUFBbkIsRUFBYjtBQUNBQyxJQUFBQSxRQUFRLENBQUMsdUJBQU8saUJBQVAsRUFBMEIsWUFBMUIsRUFBd0NLLE9BQXhDLENBQUQsQ0FBUjtBQUNILEdBSmM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgY2hhbmdlIH0gZnJvbSAncmVkdXgtZm9ybSc7XG5cbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi9zZWxlY3RvcnMnO1xuXG5leHBvcnQgZGVmYXVsdCAodmFsdWVzKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgY29uc3QgY2FsY3VsYXRvciA9IHNlbGVjdG9ycy5nZXREYXRhQ2FsY3VsYXRvcihnZXRTdGF0ZSgpKTtcbiAgICBjb25zdCBuZXdEYXRhID0gWy4uLmNhbGN1bGF0b3IsIHZhbHVlc107XG4gICAgZGlzcGF0Y2goY2hhbmdlKCdjdXN0b20tdGFnLWZvcm0nLCAnY2FsY3VsYXRvcicsIG5ld0RhdGEpKTtcbn07XG4iXX0=