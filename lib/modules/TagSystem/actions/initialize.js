"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fetchCurrencies = _interopRequireDefault(require("./fetchCurrencies"));

var _fetchDecimalFormats = _interopRequireDefault(require("./fetchDecimalFormats"));

var _fetchTagFormats = _interopRequireDefault(require("./fetchTagFormats"));

var _fetchUnitFormats = _interopRequireDefault(require("./fetchUnitFormats"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default() {
  return function (dispatch) {
    dispatch((0, _fetchDecimalFormats.default)());
    dispatch((0, _fetchTagFormats.default)());
    dispatch((0, _fetchUnitFormats.default)());
    dispatch((0, _fetchCurrencies.default)());
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL2luaXRpYWxpemUuanMiXSwibmFtZXMiOlsiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7OztlQUVlO0FBQUEsU0FBTSxVQUFDQSxRQUFELEVBQWM7QUFDL0JBLElBQUFBLFFBQVEsQ0FBQyxtQ0FBRCxDQUFSO0FBQ0FBLElBQUFBLFFBQVEsQ0FBQywrQkFBRCxDQUFSO0FBQ0FBLElBQUFBLFFBQVEsQ0FBQyxnQ0FBRCxDQUFSO0FBQ0FBLElBQUFBLFFBQVEsQ0FBQywrQkFBRCxDQUFSO0FBQ0gsR0FMYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZmV0Y2hDdXJyZW5jaWVzIGZyb20gJy4vZmV0Y2hDdXJyZW5jaWVzJztcbmltcG9ydCBmZXRjaERlY2ltYWxGb3JtYXRzIGZyb20gJy4vZmV0Y2hEZWNpbWFsRm9ybWF0cyc7XG5pbXBvcnQgZmV0Y2hUYWdGb3JtYXRzIGZyb20gJy4vZmV0Y2hUYWdGb3JtYXRzJztcbmltcG9ydCBmZXRjaFVuaXRGb3JtYXRzIGZyb20gJy4vZmV0Y2hVbml0Rm9ybWF0cyc7XG5cbmV4cG9ydCBkZWZhdWx0ICgpID0+IChkaXNwYXRjaCkgPT4ge1xuICAgIGRpc3BhdGNoKGZldGNoRGVjaW1hbEZvcm1hdHMoKSk7XG4gICAgZGlzcGF0Y2goZmV0Y2hUYWdGb3JtYXRzKCkpO1xuICAgIGRpc3BhdGNoKGZldGNoVW5pdEZvcm1hdHMoKSk7XG4gICAgZGlzcGF0Y2goZmV0Y2hDdXJyZW5jaWVzKCkpO1xufTtcbiJdfQ==