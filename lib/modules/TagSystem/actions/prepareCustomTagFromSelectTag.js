"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _addTagToCalculator = _interopRequireDefault(require("./addTagToCalculator"));

var _setIsOpenTagLibraryModal = _interopRequireDefault(require("./setIsOpenTagLibraryModal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(tag) {
  return function (dispatch) {
    dispatch((0, _addTagToCalculator.default)(tag));
    dispatch((0, _setIsOpenTagLibraryModal.default)(false));
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3ByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnLmpzIl0sIm5hbWVzIjpbInRhZyIsImRpc3BhdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7ZUFFZSxrQkFBQ0EsR0FBRDtBQUFBLFNBQVMsVUFBQ0MsUUFBRCxFQUFjO0FBQ2xDQSxJQUFBQSxRQUFRLENBQUMsaUNBQW1CRCxHQUFuQixDQUFELENBQVI7QUFDQUMsSUFBQUEsUUFBUSxDQUFDLHVDQUF5QixLQUF6QixDQUFELENBQVI7QUFDSCxHQUhjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBhZGRUYWdUb0NhbGN1bGF0b3IgZnJvbSAnLi9hZGRUYWdUb0NhbGN1bGF0b3InO1xuaW1wb3J0IHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbCBmcm9tICcuL3NldElzT3BlblRhZ0xpYnJhcnlNb2RhbCc7XG5cbmV4cG9ydCBkZWZhdWx0ICh0YWcpID0+IChkaXNwYXRjaCkgPT4ge1xuICAgIGRpc3BhdGNoKGFkZFRhZ1RvQ2FsY3VsYXRvcih0YWcpKTtcbiAgICBkaXNwYXRjaChzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwoZmFsc2UpKTtcbn07XG4iXX0=