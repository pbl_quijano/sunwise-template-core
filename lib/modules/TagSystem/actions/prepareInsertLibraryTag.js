"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tags = require("../../../constants/tags");

var _setContentViewState = _interopRequireDefault(require("./setContentViewState"));

var _setIsOpenModal = _interopRequireDefault(require("./setIsOpenModal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default() {
  return function (dispatch) {
    dispatch((0, _setContentViewState.default)(_tags.INSERT_LIBRARY_TAG_STATE));
    dispatch((0, _setIsOpenModal.default)(true));
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zL3ByZXBhcmVJbnNlcnRMaWJyYXJ5VGFnLmpzIl0sIm5hbWVzIjpbImRpc3BhdGNoIiwiSU5TRVJUX0xJQlJBUllfVEFHX1NUQVRFIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBQ0E7Ozs7ZUFFZTtBQUFBLFNBQU0sVUFBQ0EsUUFBRCxFQUFjO0FBQy9CQSxJQUFBQSxRQUFRLENBQUMsa0NBQW9CQyw4QkFBcEIsQ0FBRCxDQUFSO0FBQ0FELElBQUFBLFFBQVEsQ0FBQyw2QkFBZSxJQUFmLENBQUQsQ0FBUjtBQUNILEdBSGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSU5TRVJUX0xJQlJBUllfVEFHX1NUQVRFIH0gZnJvbSAnQGNvbnN0YW50cy90YWdzJztcclxuXHJcbmltcG9ydCBzZXRDb250ZW50Vmlld1N0YXRlIGZyb20gJy4vc2V0Q29udGVudFZpZXdTdGF0ZSc7XHJcbmltcG9ydCBzZXRJc09wZW5Nb2RhbCBmcm9tICcuL3NldElzT3Blbk1vZGFsJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0ICgpID0+IChkaXNwYXRjaCkgPT4ge1xyXG4gICAgZGlzcGF0Y2goc2V0Q29udGVudFZpZXdTdGF0ZShJTlNFUlRfTElCUkFSWV9UQUdfU1RBVEUpKTtcclxuICAgIGRpc3BhdGNoKHNldElzT3Blbk1vZGFsKHRydWUpKTtcclxufTtcclxuIl19