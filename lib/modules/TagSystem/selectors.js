"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUnitFormatsData = exports.getUnitFormats = exports.getTagLibraryValues = exports.getTagLibraryFormErrors = exports.getTagFormatsData = exports.getTagFormats = exports.getSelectedTag = exports.getSelectedCategory = exports.getModel = exports.getIsOpenTagLibraryModal = exports.getIsOpenModal = exports.getIsFetchingUnitFormats = exports.getIsFetchingTags = exports.getIsFetchingTagFormats = exports.getIsFetchingDocuments = exports.getIsFetchingDecimalFormats = exports.getIsFetchingCurrencies = exports.getInitialTagLibraryValues = exports.getInitialCustomTagValues = exports.getFilteredTags = exports.getFetchTags = exports.getFetchCurrenciesData = exports.getFetchCurrencies = exports.getDocumentsData = exports.getDocuments = exports.getDecimalFormatsData = exports.getDecimalFormats = exports.getDataFetchTags = exports.getDataCalculator = exports.getCustomTagValues = exports.getCustomTagFormErrors = exports.getCurrenciesData = exports.getContentViewState = void 0;

var _isObject = _interopRequireDefault(require("lodash/isObject"));

var _uniqBy = _interopRequireDefault(require("lodash/uniqBy"));

var _reduxForm = require("redux-form");

var _reselect = require("reselect");

var selectors = _interopRequireWildcard(require("../main/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var getModel = (0, _reselect.createSelector)(selectors.getLibState, function (sunwiseTemplateCore) {
  return sunwiseTemplateCore.tagSystem;
});
exports.getModel = getModel;
var getContentViewState = (0, _reselect.createSelector)(getModel, function (model) {
  return model.contentViewState;
});
exports.getContentViewState = getContentViewState;
var getIsOpenModal = (0, _reselect.createSelector)(getModel, function (model) {
  return model.isOpenModal;
});
exports.getIsOpenModal = getIsOpenModal;
var getTagFormats = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchTagFormats;
});
exports.getTagFormats = getTagFormats;
var getIsFetchingTagFormats = (0, _reselect.createSelector)(getTagFormats, function (fetchTagFormats) {
  return fetchTagFormats.isFetching;
});
exports.getIsFetchingTagFormats = getIsFetchingTagFormats;
var getTagFormatsData = (0, _reselect.createSelector)(getTagFormats, function (fetchTagFormats) {
  return (0, _uniqBy.default)(Object.keys(fetchTagFormats.data).reduce(function (acc, format_type) {
    return [].concat(_toConsumableArray(acc), _toConsumableArray(fetchTagFormats.data[format_type].map(function (item) {
      return {
        id: item.id,
        example: item.example,
        format: item.format,
        format_type: format_type,
        number_value: item.number_value,
        unit: item.unit
      };
    })));
  }, []), 'id');
});
exports.getTagFormatsData = getTagFormatsData;
var getUnitFormats = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchUnitFormats;
});
exports.getUnitFormats = getUnitFormats;
var getIsFetchingUnitFormats = (0, _reselect.createSelector)(getUnitFormats, function (fetchUnitFormats) {
  return fetchUnitFormats.isFetching;
});
exports.getIsFetchingUnitFormats = getIsFetchingUnitFormats;
var getUnitFormatsData = (0, _reselect.createSelector)(getUnitFormats, function (fetchUnitFormats) {
  return (0, _uniqBy.default)(Object.keys(fetchUnitFormats.data).reduce(function (acc, format_type) {
    return [].concat(_toConsumableArray(acc), _toConsumableArray(fetchUnitFormats.data[format_type].map(function (item) {
      var example = (0, _isObject.default)(item) ? item.example : item;
      var unit = (0, _isObject.default)(item) ? item.companies_catalog : item;
      return {
        example: example,
        unit: unit,
        format_type: format_type
      };
    })));
  }, []), 'unit');
});
exports.getUnitFormatsData = getUnitFormatsData;
var getDecimalFormats = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchDecimalFormats;
});
exports.getDecimalFormats = getDecimalFormats;
var getIsFetchingDecimalFormats = (0, _reselect.createSelector)(getDecimalFormats, function (fetchDecimalFormats) {
  return fetchDecimalFormats.isFetching;
});
exports.getIsFetchingDecimalFormats = getIsFetchingDecimalFormats;
var getDecimalFormatsData = (0, _reselect.createSelector)(getDecimalFormats, function (fetchDecimalFormats) {
  return fetchDecimalFormats.data;
});
exports.getDecimalFormatsData = getDecimalFormatsData;
var getFetchCurrencies = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchCurrencies;
});
exports.getFetchCurrencies = getFetchCurrencies;
var getIsFetchingCurrencies = (0, _reselect.createSelector)(getFetchCurrencies, function (fetchCurrencies) {
  return fetchCurrencies.isFetching;
});
exports.getIsFetchingCurrencies = getIsFetchingCurrencies;
var getFetchCurrenciesData = (0, _reselect.createSelector)(getFetchCurrencies, function (fetchCurrencies) {
  return fetchCurrencies.data || [];
});
exports.getFetchCurrenciesData = getFetchCurrenciesData;
var getCurrenciesData = (0, _reselect.createSelector)(getFetchCurrenciesData, function (currencies) {
  return currencies.filter(function (item) {
    return item.is_enabled && !item.is_deleted && item.currency;
  }).map(function (item) {
    return _objectSpread(_objectSpread({}, item.currency), {}, {
      dollar_price: item.dollar_price
    });
  }) || [];
});
exports.getCurrenciesData = getCurrenciesData;
var getCustomTagFormErrors = (0, _reselect.createSelector)(function (state) {
  return (0, _reduxForm.getFormSyncErrors)('custom-tag-form')(state);
}, function (errors) {
  return errors || {};
});
exports.getCustomTagFormErrors = getCustomTagFormErrors;
var getTagLibraryFormErrors = (0, _reselect.createSelector)(function (state) {
  return (0, _reduxForm.getFormSyncErrors)('tag-library-form')(state);
}, function (errors) {
  return errors || {};
});
exports.getTagLibraryFormErrors = getTagLibraryFormErrors;
var getInitialCustomTagValues = (0, _reselect.createSelector)(getModel, function (model) {
  return model.initialCustomTagValues;
});
exports.getInitialCustomTagValues = getInitialCustomTagValues;
var getInitialTagLibraryValues = (0, _reselect.createSelector)(getModel, function (model) {
  return model.initialTagLibraryValues;
});
exports.getInitialTagLibraryValues = getInitialTagLibraryValues;
var getCustomTagValues = (0, _reselect.createSelector)(function (state) {
  return (0, _reduxForm.getFormValues)('custom-tag-form')(state);
}, function (values) {
  return values || {};
});
exports.getCustomTagValues = getCustomTagValues;
var getTagLibraryValues = (0, _reselect.createSelector)(function (state) {
  return (0, _reduxForm.getFormValues)('tag-library-form')(state);
}, function (values) {
  return values || {};
});
exports.getTagLibraryValues = getTagLibraryValues;
var getDataCalculator = (0, _reselect.createSelector)(getCustomTagValues, function (model) {
  return model.calculator;
});
exports.getDataCalculator = getDataCalculator;
var getIsOpenTagLibraryModal = (0, _reselect.createSelector)(getModel, function (model) {
  return model.isOpenTagLibraryModal;
});
exports.getIsOpenTagLibraryModal = getIsOpenTagLibraryModal;
var getDocuments = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchDocuments;
});
exports.getDocuments = getDocuments;
var getDocumentsData = (0, _reselect.createSelector)(getDocuments, function (model) {
  return model.data;
});
exports.getDocumentsData = getDocumentsData;
var getIsFetchingDocuments = (0, _reselect.createSelector)(getDocuments, function (model) {
  return model.isFetching;
});
exports.getIsFetchingDocuments = getIsFetchingDocuments;
var getFilteredTags = (0, _reselect.createSelector)(getModel, function (model) {
  return model.filteredTags;
});
exports.getFilteredTags = getFilteredTags;
var getFetchTags = (0, _reselect.createSelector)(getModel, function (model) {
  return model.fetchTags;
});
exports.getFetchTags = getFetchTags;
var getDataFetchTags = (0, _reselect.createSelector)(getFetchTags, function (fetchTags) {
  return fetchTags.data || [];
});
exports.getDataFetchTags = getDataFetchTags;
var getIsFetchingTags = (0, _reselect.createSelector)(getFetchTags, function (fetchTags) {
  return fetchTags.isFetching;
});
exports.getIsFetchingTags = getIsFetchingTags;
var getSelectedCategory = (0, _reselect.createSelector)(getModel, function (model) {
  return model.selectedCategory;
});
exports.getSelectedCategory = getSelectedCategory;
var getSelectedTag = (0, _reselect.createSelector)(getModel, function (model) {
  return model.selectedTag;
});
exports.getSelectedTag = getSelectedTag;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9zZWxlY3RvcnMuanMiXSwibmFtZXMiOlsiZ2V0TW9kZWwiLCJzZWxlY3RvcnMiLCJnZXRMaWJTdGF0ZSIsInN1bndpc2VUZW1wbGF0ZUNvcmUiLCJ0YWdTeXN0ZW0iLCJnZXRDb250ZW50Vmlld1N0YXRlIiwibW9kZWwiLCJjb250ZW50Vmlld1N0YXRlIiwiZ2V0SXNPcGVuTW9kYWwiLCJpc09wZW5Nb2RhbCIsImdldFRhZ0Zvcm1hdHMiLCJmZXRjaFRhZ0Zvcm1hdHMiLCJnZXRJc0ZldGNoaW5nVGFnRm9ybWF0cyIsImlzRmV0Y2hpbmciLCJnZXRUYWdGb3JtYXRzRGF0YSIsIk9iamVjdCIsImtleXMiLCJkYXRhIiwicmVkdWNlIiwiYWNjIiwiZm9ybWF0X3R5cGUiLCJtYXAiLCJpdGVtIiwiaWQiLCJleGFtcGxlIiwiZm9ybWF0IiwibnVtYmVyX3ZhbHVlIiwidW5pdCIsImdldFVuaXRGb3JtYXRzIiwiZmV0Y2hVbml0Rm9ybWF0cyIsImdldElzRmV0Y2hpbmdVbml0Rm9ybWF0cyIsImdldFVuaXRGb3JtYXRzRGF0YSIsImNvbXBhbmllc19jYXRhbG9nIiwiZ2V0RGVjaW1hbEZvcm1hdHMiLCJmZXRjaERlY2ltYWxGb3JtYXRzIiwiZ2V0SXNGZXRjaGluZ0RlY2ltYWxGb3JtYXRzIiwiZ2V0RGVjaW1hbEZvcm1hdHNEYXRhIiwiZ2V0RmV0Y2hDdXJyZW5jaWVzIiwiZmV0Y2hDdXJyZW5jaWVzIiwiZ2V0SXNGZXRjaGluZ0N1cnJlbmNpZXMiLCJnZXRGZXRjaEN1cnJlbmNpZXNEYXRhIiwiZ2V0Q3VycmVuY2llc0RhdGEiLCJjdXJyZW5jaWVzIiwiZmlsdGVyIiwiaXNfZW5hYmxlZCIsImlzX2RlbGV0ZWQiLCJjdXJyZW5jeSIsImRvbGxhcl9wcmljZSIsImdldEN1c3RvbVRhZ0Zvcm1FcnJvcnMiLCJzdGF0ZSIsImVycm9ycyIsImdldFRhZ0xpYnJhcnlGb3JtRXJyb3JzIiwiZ2V0SW5pdGlhbEN1c3RvbVRhZ1ZhbHVlcyIsImluaXRpYWxDdXN0b21UYWdWYWx1ZXMiLCJnZXRJbml0aWFsVGFnTGlicmFyeVZhbHVlcyIsImluaXRpYWxUYWdMaWJyYXJ5VmFsdWVzIiwiZ2V0Q3VzdG9tVGFnVmFsdWVzIiwidmFsdWVzIiwiZ2V0VGFnTGlicmFyeVZhbHVlcyIsImdldERhdGFDYWxjdWxhdG9yIiwiY2FsY3VsYXRvciIsImdldElzT3BlblRhZ0xpYnJhcnlNb2RhbCIsImlzT3BlblRhZ0xpYnJhcnlNb2RhbCIsImdldERvY3VtZW50cyIsImZldGNoRG9jdW1lbnRzIiwiZ2V0RG9jdW1lbnRzRGF0YSIsImdldElzRmV0Y2hpbmdEb2N1bWVudHMiLCJnZXRGaWx0ZXJlZFRhZ3MiLCJmaWx0ZXJlZFRhZ3MiLCJnZXRGZXRjaFRhZ3MiLCJmZXRjaFRhZ3MiLCJnZXREYXRhRmV0Y2hUYWdzIiwiZ2V0SXNGZXRjaGluZ1RhZ3MiLCJnZXRTZWxlY3RlZENhdGVnb3J5Iiwic2VsZWN0ZWRDYXRlZ29yeSIsImdldFNlbGVjdGVkVGFnIiwic2VsZWN0ZWRUYWciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVPLElBQU1BLFFBQVEsR0FBRyw4QkFDcEJDLFNBQVMsQ0FBQ0MsV0FEVSxFQUVwQixVQUFDQyxtQkFBRDtBQUFBLFNBQXlCQSxtQkFBbUIsQ0FBQ0MsU0FBN0M7QUFBQSxDQUZvQixDQUFqQjs7QUFLQSxJQUFNQyxtQkFBbUIsR0FBRyw4QkFDL0JMLFFBRCtCLEVBRS9CLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNDLGdCQUFqQjtBQUFBLENBRitCLENBQTVCOztBQUtBLElBQU1DLGNBQWMsR0FBRyw4QkFDMUJSLFFBRDBCLEVBRTFCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNHLFdBQWpCO0FBQUEsQ0FGMEIsQ0FBdkI7O0FBS0EsSUFBTUMsYUFBYSxHQUFHLDhCQUN6QlYsUUFEeUIsRUFFekIsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ0ssZUFBakI7QUFBQSxDQUZ5QixDQUF0Qjs7QUFLQSxJQUFNQyx1QkFBdUIsR0FBRyw4QkFDbkNGLGFBRG1DLEVBRW5DLFVBQUNDLGVBQUQ7QUFBQSxTQUFxQkEsZUFBZSxDQUFDRSxVQUFyQztBQUFBLENBRm1DLENBQWhDOztBQUtBLElBQU1DLGlCQUFpQixHQUFHLDhCQUM3QkosYUFENkIsRUFFN0IsVUFBQ0MsZUFBRCxFQUFxQjtBQUNqQixTQUFPLHFCQUNISSxNQUFNLENBQUNDLElBQVAsQ0FBWUwsZUFBZSxDQUFDTSxJQUE1QixFQUFrQ0MsTUFBbEMsQ0FBeUMsVUFBQ0MsR0FBRCxFQUFNQyxXQUFOLEVBQXNCO0FBQzNELHdDQUNPRCxHQURQLHNCQUVPUixlQUFlLENBQUNNLElBQWhCLENBQXFCRyxXQUFyQixFQUFrQ0MsR0FBbEMsQ0FBc0MsVUFBQ0MsSUFBRDtBQUFBLGFBQVc7QUFDaERDLFFBQUFBLEVBQUUsRUFBRUQsSUFBSSxDQUFDQyxFQUR1QztBQUVoREMsUUFBQUEsT0FBTyxFQUFFRixJQUFJLENBQUNFLE9BRmtDO0FBR2hEQyxRQUFBQSxNQUFNLEVBQUVILElBQUksQ0FBQ0csTUFIbUM7QUFJaERMLFFBQUFBLFdBQVcsRUFBRUEsV0FKbUM7QUFLaERNLFFBQUFBLFlBQVksRUFBRUosSUFBSSxDQUFDSSxZQUw2QjtBQU1oREMsUUFBQUEsSUFBSSxFQUFFTCxJQUFJLENBQUNLO0FBTnFDLE9BQVg7QUFBQSxLQUF0QyxDQUZQO0FBV0gsR0FaRCxFQVlHLEVBWkgsQ0FERyxFQWNILElBZEcsQ0FBUDtBQWdCSCxDQW5CNEIsQ0FBMUI7O0FBc0JBLElBQU1DLGNBQWMsR0FBRyw4QkFDMUI1QixRQUQwQixFQUUxQixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDdUIsZ0JBQWpCO0FBQUEsQ0FGMEIsQ0FBdkI7O0FBS0EsSUFBTUMsd0JBQXdCLEdBQUcsOEJBQ3BDRixjQURvQyxFQUVwQyxVQUFDQyxnQkFBRDtBQUFBLFNBQXNCQSxnQkFBZ0IsQ0FBQ2hCLFVBQXZDO0FBQUEsQ0FGb0MsQ0FBakM7O0FBS0EsSUFBTWtCLGtCQUFrQixHQUFHLDhCQUM5QkgsY0FEOEIsRUFFOUIsVUFBQ0MsZ0JBQUQsRUFBc0I7QUFDbEIsU0FBTyxxQkFDSGQsTUFBTSxDQUFDQyxJQUFQLENBQVlhLGdCQUFnQixDQUFDWixJQUE3QixFQUFtQ0MsTUFBbkMsQ0FBMEMsVUFBQ0MsR0FBRCxFQUFNQyxXQUFOLEVBQXNCO0FBQzVELHdDQUNPRCxHQURQLHNCQUVPVSxnQkFBZ0IsQ0FBQ1osSUFBakIsQ0FBc0JHLFdBQXRCLEVBQW1DQyxHQUFuQyxDQUF1QyxVQUFDQyxJQUFELEVBQVU7QUFDaEQsVUFBTUUsT0FBTyxHQUFHLHVCQUFTRixJQUFULElBQWlCQSxJQUFJLENBQUNFLE9BQXRCLEdBQWdDRixJQUFoRDtBQUNBLFVBQU1LLElBQUksR0FBRyx1QkFBU0wsSUFBVCxJQUNQQSxJQUFJLENBQUNVLGlCQURFLEdBRVBWLElBRk47QUFHQSxhQUFPO0FBQ0hFLFFBQUFBLE9BQU8sRUFBRUEsT0FETjtBQUVIRyxRQUFBQSxJQUFJLEVBQUVBLElBRkg7QUFHSFAsUUFBQUEsV0FBVyxFQUFFQTtBQUhWLE9BQVA7QUFLSCxLQVZFLENBRlA7QUFjSCxHQWZELEVBZUcsRUFmSCxDQURHLEVBaUJILE1BakJHLENBQVA7QUFtQkgsQ0F0QjZCLENBQTNCOztBQXlCQSxJQUFNYSxpQkFBaUIsR0FBRyw4QkFDN0JqQyxRQUQ2QixFQUU3QixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDNEIsbUJBQWpCO0FBQUEsQ0FGNkIsQ0FBMUI7O0FBS0EsSUFBTUMsMkJBQTJCLEdBQUcsOEJBQ3ZDRixpQkFEdUMsRUFFdkMsVUFBQ0MsbUJBQUQ7QUFBQSxTQUF5QkEsbUJBQW1CLENBQUNyQixVQUE3QztBQUFBLENBRnVDLENBQXBDOztBQUtBLElBQU11QixxQkFBcUIsR0FBRyw4QkFDakNILGlCQURpQyxFQUVqQyxVQUFDQyxtQkFBRDtBQUFBLFNBQXlCQSxtQkFBbUIsQ0FBQ2pCLElBQTdDO0FBQUEsQ0FGaUMsQ0FBOUI7O0FBS0EsSUFBTW9CLGtCQUFrQixHQUFHLDhCQUM5QnJDLFFBRDhCLEVBRTlCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNnQyxlQUFqQjtBQUFBLENBRjhCLENBQTNCOztBQUtBLElBQU1DLHVCQUF1QixHQUFHLDhCQUNuQ0Ysa0JBRG1DLEVBRW5DLFVBQUNDLGVBQUQ7QUFBQSxTQUFxQkEsZUFBZSxDQUFDekIsVUFBckM7QUFBQSxDQUZtQyxDQUFoQzs7QUFLQSxJQUFNMkIsc0JBQXNCLEdBQUcsOEJBQ2xDSCxrQkFEa0MsRUFFbEMsVUFBQ0MsZUFBRDtBQUFBLFNBQXFCQSxlQUFlLENBQUNyQixJQUFoQixJQUF3QixFQUE3QztBQUFBLENBRmtDLENBQS9COztBQUtBLElBQU13QixpQkFBaUIsR0FBRyw4QkFDN0JELHNCQUQ2QixFQUU3QixVQUFDRSxVQUFEO0FBQUEsU0FDSUEsVUFBVSxDQUNMQyxNQURMLENBRVEsVUFBQ3JCLElBQUQ7QUFBQSxXQUFVQSxJQUFJLENBQUNzQixVQUFMLElBQW1CLENBQUN0QixJQUFJLENBQUN1QixVQUF6QixJQUF1Q3ZCLElBQUksQ0FBQ3dCLFFBQXREO0FBQUEsR0FGUixFQUlLekIsR0FKTCxDQUlTLFVBQUNDLElBQUQ7QUFBQSwyQ0FDRUEsSUFBSSxDQUFDd0IsUUFEUDtBQUVEQyxNQUFBQSxZQUFZLEVBQUV6QixJQUFJLENBQUN5QjtBQUZsQjtBQUFBLEdBSlQsS0FPVyxFQVJmO0FBQUEsQ0FGNkIsQ0FBMUI7O0FBYUEsSUFBTUMsc0JBQXNCLEdBQUcsOEJBQ2xDLFVBQUNDLEtBQUQ7QUFBQSxTQUFXLGtDQUFrQixpQkFBbEIsRUFBcUNBLEtBQXJDLENBQVg7QUFBQSxDQURrQyxFQUVsQyxVQUFDQyxNQUFEO0FBQUEsU0FBWUEsTUFBTSxJQUFJLEVBQXRCO0FBQUEsQ0FGa0MsQ0FBL0I7O0FBS0EsSUFBTUMsdUJBQXVCLEdBQUcsOEJBQ25DLFVBQUNGLEtBQUQ7QUFBQSxTQUFXLGtDQUFrQixrQkFBbEIsRUFBc0NBLEtBQXRDLENBQVg7QUFBQSxDQURtQyxFQUVuQyxVQUFDQyxNQUFEO0FBQUEsU0FBWUEsTUFBTSxJQUFJLEVBQXRCO0FBQUEsQ0FGbUMsQ0FBaEM7O0FBS0EsSUFBTUUseUJBQXlCLEdBQUcsOEJBQ3JDcEQsUUFEcUMsRUFFckMsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQytDLHNCQUFqQjtBQUFBLENBRnFDLENBQWxDOztBQUtBLElBQU1DLDBCQUEwQixHQUFHLDhCQUN0Q3RELFFBRHNDLEVBRXRDLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNpRCx1QkFBakI7QUFBQSxDQUZzQyxDQUFuQzs7QUFLQSxJQUFNQyxrQkFBa0IsR0FBRyw4QkFDOUIsVUFBQ1AsS0FBRDtBQUFBLFNBQVcsOEJBQWMsaUJBQWQsRUFBaUNBLEtBQWpDLENBQVg7QUFBQSxDQUQ4QixFQUU5QixVQUFDUSxNQUFEO0FBQUEsU0FBWUEsTUFBTSxJQUFJLEVBQXRCO0FBQUEsQ0FGOEIsQ0FBM0I7O0FBS0EsSUFBTUMsbUJBQW1CLEdBQUcsOEJBQy9CLFVBQUNULEtBQUQ7QUFBQSxTQUFXLDhCQUFjLGtCQUFkLEVBQWtDQSxLQUFsQyxDQUFYO0FBQUEsQ0FEK0IsRUFFL0IsVUFBQ1EsTUFBRDtBQUFBLFNBQVlBLE1BQU0sSUFBSSxFQUF0QjtBQUFBLENBRitCLENBQTVCOztBQUtBLElBQU1FLGlCQUFpQixHQUFHLDhCQUM3Qkgsa0JBRDZCLEVBRTdCLFVBQUNsRCxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDc0QsVUFBakI7QUFBQSxDQUY2QixDQUExQjs7QUFLQSxJQUFNQyx3QkFBd0IsR0FBRyw4QkFDcEM3RCxRQURvQyxFQUVwQyxVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDd0QscUJBQWpCO0FBQUEsQ0FGb0MsQ0FBakM7O0FBS0EsSUFBTUMsWUFBWSxHQUFHLDhCQUN4Qi9ELFFBRHdCLEVBRXhCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUMwRCxjQUFqQjtBQUFBLENBRndCLENBQXJCOztBQUtBLElBQU1DLGdCQUFnQixHQUFHLDhCQUM1QkYsWUFENEIsRUFFNUIsVUFBQ3pELEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNXLElBQWpCO0FBQUEsQ0FGNEIsQ0FBekI7O0FBS0EsSUFBTWlELHNCQUFzQixHQUFHLDhCQUNsQ0gsWUFEa0MsRUFFbEMsVUFBQ3pELEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNPLFVBQWpCO0FBQUEsQ0FGa0MsQ0FBL0I7O0FBS0EsSUFBTXNELGVBQWUsR0FBRyw4QkFDM0JuRSxRQUQyQixFQUUzQixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDOEQsWUFBakI7QUFBQSxDQUYyQixDQUF4Qjs7QUFLQSxJQUFNQyxZQUFZLEdBQUcsOEJBQ3hCckUsUUFEd0IsRUFFeEIsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ2dFLFNBQWpCO0FBQUEsQ0FGd0IsQ0FBckI7O0FBS0EsSUFBTUMsZ0JBQWdCLEdBQUcsOEJBQzVCRixZQUQ0QixFQUU1QixVQUFDQyxTQUFEO0FBQUEsU0FBZUEsU0FBUyxDQUFDckQsSUFBVixJQUFrQixFQUFqQztBQUFBLENBRjRCLENBQXpCOztBQUtBLElBQU11RCxpQkFBaUIsR0FBRyw4QkFDN0JILFlBRDZCLEVBRTdCLFVBQUNDLFNBQUQ7QUFBQSxTQUFlQSxTQUFTLENBQUN6RCxVQUF6QjtBQUFBLENBRjZCLENBQTFCOztBQUtBLElBQU00RCxtQkFBbUIsR0FBRyw4QkFDL0J6RSxRQUQrQixFQUUvQixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDb0UsZ0JBQWpCO0FBQUEsQ0FGK0IsQ0FBNUI7O0FBS0EsSUFBTUMsY0FBYyxHQUFHLDhCQUMxQjNFLFFBRDBCLEVBRTFCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNzRSxXQUFqQjtBQUFBLENBRjBCLENBQXZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzT2JqZWN0IGZyb20gJ2xvZGFzaC9pc09iamVjdCc7XG5pbXBvcnQgdW5pcUJ5IGZyb20gJ2xvZGFzaC91bmlxQnknO1xuaW1wb3J0IHsgZ2V0Rm9ybVN5bmNFcnJvcnMsIGdldEZvcm1WYWx1ZXMgfSBmcm9tICdyZWR1eC1mb3JtJztcbmltcG9ydCB7IGNyZWF0ZVNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xuXG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnQG1haW4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGNvbnN0IGdldE1vZGVsID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgc2VsZWN0b3JzLmdldExpYlN0YXRlLFxuICAgIChzdW53aXNlVGVtcGxhdGVDb3JlKSA9PiBzdW53aXNlVGVtcGxhdGVDb3JlLnRhZ1N5c3RlbVxuKTtcblxuZXhwb3J0IGNvbnN0IGdldENvbnRlbnRWaWV3U3RhdGUgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRNb2RlbCxcbiAgICAobW9kZWwpID0+IG1vZGVsLmNvbnRlbnRWaWV3U3RhdGVcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRJc09wZW5Nb2RhbCA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuaXNPcGVuTW9kYWxcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRUYWdGb3JtYXRzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5mZXRjaFRhZ0Zvcm1hdHNcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRJc0ZldGNoaW5nVGFnRm9ybWF0cyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldFRhZ0Zvcm1hdHMsXG4gICAgKGZldGNoVGFnRm9ybWF0cykgPT4gZmV0Y2hUYWdGb3JtYXRzLmlzRmV0Y2hpbmdcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRUYWdGb3JtYXRzRGF0YSA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldFRhZ0Zvcm1hdHMsXG4gICAgKGZldGNoVGFnRm9ybWF0cykgPT4ge1xuICAgICAgICByZXR1cm4gdW5pcUJ5KFxuICAgICAgICAgICAgT2JqZWN0LmtleXMoZmV0Y2hUYWdGb3JtYXRzLmRhdGEpLnJlZHVjZSgoYWNjLCBmb3JtYXRfdHlwZSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgIC4uLmFjYyxcbiAgICAgICAgICAgICAgICAgICAgLi4uZmV0Y2hUYWdGb3JtYXRzLmRhdGFbZm9ybWF0X3R5cGVdLm1hcCgoaXRlbSkgPT4gKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBpdGVtLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgZXhhbXBsZTogaXRlbS5leGFtcGxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiBpdGVtLmZvcm1hdCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdF90eXBlOiBmb3JtYXRfdHlwZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlcl92YWx1ZTogaXRlbS5udW1iZXJfdmFsdWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB1bml0OiBpdGVtLnVuaXQsXG4gICAgICAgICAgICAgICAgICAgIH0pKSxcbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfSwgW10pLFxuICAgICAgICAgICAgJ2lkJ1xuICAgICAgICApO1xuICAgIH1cbik7XG5cbmV4cG9ydCBjb25zdCBnZXRVbml0Rm9ybWF0cyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuZmV0Y2hVbml0Rm9ybWF0c1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldElzRmV0Y2hpbmdVbml0Rm9ybWF0cyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldFVuaXRGb3JtYXRzLFxuICAgIChmZXRjaFVuaXRGb3JtYXRzKSA9PiBmZXRjaFVuaXRGb3JtYXRzLmlzRmV0Y2hpbmdcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRVbml0Rm9ybWF0c0RhdGEgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRVbml0Rm9ybWF0cyxcbiAgICAoZmV0Y2hVbml0Rm9ybWF0cykgPT4ge1xuICAgICAgICByZXR1cm4gdW5pcUJ5KFxuICAgICAgICAgICAgT2JqZWN0LmtleXMoZmV0Y2hVbml0Rm9ybWF0cy5kYXRhKS5yZWR1Y2UoKGFjYywgZm9ybWF0X3R5cGUpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgICAgICAgICAuLi5hY2MsXG4gICAgICAgICAgICAgICAgICAgIC4uLmZldGNoVW5pdEZvcm1hdHMuZGF0YVtmb3JtYXRfdHlwZV0ubWFwKChpdGVtKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBleGFtcGxlID0gaXNPYmplY3QoaXRlbSkgPyBpdGVtLmV4YW1wbGUgOiBpdGVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdW5pdCA9IGlzT2JqZWN0KGl0ZW0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBpdGVtLmNvbXBhbmllc19jYXRhbG9nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBpdGVtO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGFtcGxlOiBleGFtcGxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuaXQ6IHVuaXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0X3R5cGU6IGZvcm1hdF90eXBlLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgXTtcbiAgICAgICAgICAgIH0sIFtdKSxcbiAgICAgICAgICAgICd1bml0J1xuICAgICAgICApO1xuICAgIH1cbik7XG5cbmV4cG9ydCBjb25zdCBnZXREZWNpbWFsRm9ybWF0cyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuZmV0Y2hEZWNpbWFsRm9ybWF0c1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldElzRmV0Y2hpbmdEZWNpbWFsRm9ybWF0cyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldERlY2ltYWxGb3JtYXRzLFxuICAgIChmZXRjaERlY2ltYWxGb3JtYXRzKSA9PiBmZXRjaERlY2ltYWxGb3JtYXRzLmlzRmV0Y2hpbmdcbik7XG5cbmV4cG9ydCBjb25zdCBnZXREZWNpbWFsRm9ybWF0c0RhdGEgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXREZWNpbWFsRm9ybWF0cyxcbiAgICAoZmV0Y2hEZWNpbWFsRm9ybWF0cykgPT4gZmV0Y2hEZWNpbWFsRm9ybWF0cy5kYXRhXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0RmV0Y2hDdXJyZW5jaWVzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5mZXRjaEN1cnJlbmNpZXNcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRJc0ZldGNoaW5nQ3VycmVuY2llcyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldEZldGNoQ3VycmVuY2llcyxcbiAgICAoZmV0Y2hDdXJyZW5jaWVzKSA9PiBmZXRjaEN1cnJlbmNpZXMuaXNGZXRjaGluZ1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldEZldGNoQ3VycmVuY2llc0RhdGEgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRGZXRjaEN1cnJlbmNpZXMsXG4gICAgKGZldGNoQ3VycmVuY2llcykgPT4gZmV0Y2hDdXJyZW5jaWVzLmRhdGEgfHwgW11cbik7XG5cbmV4cG9ydCBjb25zdCBnZXRDdXJyZW5jaWVzRGF0YSA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldEZldGNoQ3VycmVuY2llc0RhdGEsXG4gICAgKGN1cnJlbmNpZXMpID0+XG4gICAgICAgIGN1cnJlbmNpZXNcbiAgICAgICAgICAgIC5maWx0ZXIoXG4gICAgICAgICAgICAgICAgKGl0ZW0pID0+IGl0ZW0uaXNfZW5hYmxlZCAmJiAhaXRlbS5pc19kZWxldGVkICYmIGl0ZW0uY3VycmVuY3lcbiAgICAgICAgICAgIClcbiAgICAgICAgICAgIC5tYXAoKGl0ZW0pID0+ICh7XG4gICAgICAgICAgICAgICAgLi4uaXRlbS5jdXJyZW5jeSxcbiAgICAgICAgICAgICAgICBkb2xsYXJfcHJpY2U6IGl0ZW0uZG9sbGFyX3ByaWNlLFxuICAgICAgICAgICAgfSkpIHx8IFtdXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0Q3VzdG9tVGFnRm9ybUVycm9ycyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIChzdGF0ZSkgPT4gZ2V0Rm9ybVN5bmNFcnJvcnMoJ2N1c3RvbS10YWctZm9ybScpKHN0YXRlKSxcbiAgICAoZXJyb3JzKSA9PiBlcnJvcnMgfHwge31cbik7XG5cbmV4cG9ydCBjb25zdCBnZXRUYWdMaWJyYXJ5Rm9ybUVycm9ycyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIChzdGF0ZSkgPT4gZ2V0Rm9ybVN5bmNFcnJvcnMoJ3RhZy1saWJyYXJ5LWZvcm0nKShzdGF0ZSksXG4gICAgKGVycm9ycykgPT4gZXJyb3JzIHx8IHt9XG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0SW5pdGlhbEN1c3RvbVRhZ1ZhbHVlcyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuaW5pdGlhbEN1c3RvbVRhZ1ZhbHVlc1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldEluaXRpYWxUYWdMaWJyYXJ5VmFsdWVzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5pbml0aWFsVGFnTGlicmFyeVZhbHVlc1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldEN1c3RvbVRhZ1ZhbHVlcyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIChzdGF0ZSkgPT4gZ2V0Rm9ybVZhbHVlcygnY3VzdG9tLXRhZy1mb3JtJykoc3RhdGUpLFxuICAgICh2YWx1ZXMpID0+IHZhbHVlcyB8fCB7fVxuKTtcblxuZXhwb3J0IGNvbnN0IGdldFRhZ0xpYnJhcnlWYWx1ZXMgPSBjcmVhdGVTZWxlY3RvcihcbiAgICAoc3RhdGUpID0+IGdldEZvcm1WYWx1ZXMoJ3RhZy1saWJyYXJ5LWZvcm0nKShzdGF0ZSksXG4gICAgKHZhbHVlcykgPT4gdmFsdWVzIHx8IHt9XG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0RGF0YUNhbGN1bGF0b3IgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRDdXN0b21UYWdWYWx1ZXMsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5jYWxjdWxhdG9yXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5pc09wZW5UYWdMaWJyYXJ5TW9kYWxcbik7XG5cbmV4cG9ydCBjb25zdCBnZXREb2N1bWVudHMgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRNb2RlbCxcbiAgICAobW9kZWwpID0+IG1vZGVsLmZldGNoRG9jdW1lbnRzXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0RG9jdW1lbnRzRGF0YSA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldERvY3VtZW50cyxcbiAgICAobW9kZWwpID0+IG1vZGVsLmRhdGFcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRJc0ZldGNoaW5nRG9jdW1lbnRzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0RG9jdW1lbnRzLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuaXNGZXRjaGluZ1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldEZpbHRlcmVkVGFncyA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuZmlsdGVyZWRUYWdzXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0RmV0Y2hUYWdzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5mZXRjaFRhZ3Ncbik7XG5cbmV4cG9ydCBjb25zdCBnZXREYXRhRmV0Y2hUYWdzID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0RmV0Y2hUYWdzLFxuICAgIChmZXRjaFRhZ3MpID0+IGZldGNoVGFncy5kYXRhIHx8IFtdXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0SXNGZXRjaGluZ1RhZ3MgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRGZXRjaFRhZ3MsXG4gICAgKGZldGNoVGFncykgPT4gZmV0Y2hUYWdzLmlzRmV0Y2hpbmdcbik7XG5cbmV4cG9ydCBjb25zdCBnZXRTZWxlY3RlZENhdGVnb3J5ID0gY3JlYXRlU2VsZWN0b3IoXG4gICAgZ2V0TW9kZWwsXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5zZWxlY3RlZENhdGVnb3J5XG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0U2VsZWN0ZWRUYWcgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRNb2RlbCxcbiAgICAobW9kZWwpID0+IG1vZGVsLnNlbGVjdGVkVGFnXG4pO1xuIl19