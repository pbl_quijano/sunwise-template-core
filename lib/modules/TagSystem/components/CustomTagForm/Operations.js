"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Operations = function Operations(_ref) {
  var handleClickOperator = _ref.handleClickOperator,
      handleInsertNumber = _ref.handleInsertNumber,
      number = _ref.number,
      setNumber = _ref.setNumber;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: '-',
            tag: '-',
            is_operator: true
          });
        },
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-minus"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: '+',
            tag: '+',
            is_operator: true
          });
        },
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fa fa-plus"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: 'x',
            tag: '*',
            is_operator: true
          });
        },
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fa fa-times"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: '÷',
            tag: '/',
            is_operator: true
          });
        },
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-divide"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: '[',
            tag: '[',
            is_operator: true
          });
        },
        type: "button",
        children: "["
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonOperator, {
        onClick: function onClick() {
          return handleClickOperator({
            title: ']',
            tag: ']',
            is_operator: true
          });
        },
        type: "button",
        children: "]"
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.StyledInputGroup, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledInputNumber, {
          id: "number",
          min: "1",
          onChange: function onChange(e) {
            return setNumber(e.target.value);
          },
          placeholder: "--",
          type: "number",
          value: number
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
          className: "input-group-append",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.ButtonInsertNumber, {
            className: "border-left-0",
            disabled: !number || number === 0,
            onClick: function onClick() {
              return handleInsertNumber();
            },
            title: t('Insert number'),
            type: "button",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
              className: "fa fa-arrow-right",
              "aria-hidden": "true"
            })
          })
        })]
      })]
    })
  });
};

Operations.propTypes = {
  handleClickOperator: _propTypes.default.func,
  handleInsertNumber: _propTypes.default.func,
  number: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.number]),
  setNumber: _propTypes.default.func
};
var _default = Operations;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vT3BlcmF0aW9ucy5qcyJdLCJuYW1lcyI6WyJPcGVyYXRpb25zIiwiaGFuZGxlQ2xpY2tPcGVyYXRvciIsImhhbmRsZUluc2VydE51bWJlciIsIm51bWJlciIsInNldE51bWJlciIsInQiLCJ0aXRsZSIsInRhZyIsImlzX29wZXJhdG9yIiwiZSIsInRhcmdldCIsInZhbHVlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsIm9uZU9mVHlwZSIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7QUFPQSxJQUFNQSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxPQUtiO0FBQUEsTUFKRkMsbUJBSUUsUUFKRkEsbUJBSUU7QUFBQSxNQUhGQyxrQkFHRSxRQUhGQSxrQkFHRTtBQUFBLE1BRkZDLE1BRUUsUUFGRkEsTUFFRTtBQUFBLE1BREZDLFNBQ0UsUUFERkEsU0FDRTs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0Esc0JBQ0kscUJBQUMsbUJBQUQ7QUFBQSwyQkFDSSxzQkFBQyxtQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGdDQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFDTEosbUJBQW1CLENBQUM7QUFDaEJLLFlBQUFBLEtBQUssRUFBRSxHQURTO0FBRWhCQyxZQUFBQSxHQUFHLEVBQUUsR0FGVztBQUdoQkMsWUFBQUEsV0FBVyxFQUFFO0FBSEcsV0FBRCxDQURkO0FBQUEsU0FEYjtBQVFJLFFBQUEsSUFBSSxFQUFDLFFBUlQ7QUFBQSwrQkFVSTtBQUFHLFVBQUEsU0FBUyxFQUFDO0FBQWI7QUFWSixRQURKLGVBYUkscUJBQUMsZ0NBQUQ7QUFDSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUNMUCxtQkFBbUIsQ0FBQztBQUNoQkssWUFBQUEsS0FBSyxFQUFFLEdBRFM7QUFFaEJDLFlBQUFBLEdBQUcsRUFBRSxHQUZXO0FBR2hCQyxZQUFBQSxXQUFXLEVBQUU7QUFIRyxXQUFELENBRGQ7QUFBQSxTQURiO0FBUUksUUFBQSxJQUFJLEVBQUMsUUFSVDtBQUFBLCtCQVVJO0FBQUcsVUFBQSxTQUFTLEVBQUM7QUFBYjtBQVZKLFFBYkosZUF5QkkscUJBQUMsZ0NBQUQ7QUFDSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUNMUCxtQkFBbUIsQ0FBQztBQUNoQkssWUFBQUEsS0FBSyxFQUFFLEdBRFM7QUFFaEJDLFlBQUFBLEdBQUcsRUFBRSxHQUZXO0FBR2hCQyxZQUFBQSxXQUFXLEVBQUU7QUFIRyxXQUFELENBRGQ7QUFBQSxTQURiO0FBUUksUUFBQSxJQUFJLEVBQUMsUUFSVDtBQUFBLCtCQVVJO0FBQUcsVUFBQSxTQUFTLEVBQUM7QUFBYjtBQVZKLFFBekJKLGVBcUNJLHFCQUFDLGdDQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFDTFAsbUJBQW1CLENBQUM7QUFDaEJLLFlBQUFBLEtBQUssRUFBRSxHQURTO0FBRWhCQyxZQUFBQSxHQUFHLEVBQUUsR0FGVztBQUdoQkMsWUFBQUEsV0FBVyxFQUFFO0FBSEcsV0FBRCxDQURkO0FBQUEsU0FEYjtBQVFJLFFBQUEsSUFBSSxFQUFDLFFBUlQ7QUFBQSwrQkFVSTtBQUFHLFVBQUEsU0FBUyxFQUFDO0FBQWI7QUFWSixRQXJDSixlQWlESSxxQkFBQyxnQ0FBRDtBQUNJLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQ0xQLG1CQUFtQixDQUFDO0FBQ2hCSyxZQUFBQSxLQUFLLEVBQUUsR0FEUztBQUVoQkMsWUFBQUEsR0FBRyxFQUFFLEdBRlc7QUFHaEJDLFlBQUFBLFdBQVcsRUFBRTtBQUhHLFdBQUQsQ0FEZDtBQUFBLFNBRGI7QUFRSSxRQUFBLElBQUksRUFBQyxRQVJUO0FBQUE7QUFBQSxRQWpESixlQTZESSxxQkFBQyxnQ0FBRDtBQUNJLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQ0xQLG1CQUFtQixDQUFDO0FBQ2hCSyxZQUFBQSxLQUFLLEVBQUUsR0FEUztBQUVoQkMsWUFBQUEsR0FBRyxFQUFFLEdBRlc7QUFHaEJDLFlBQUFBLFdBQVcsRUFBRTtBQUhHLFdBQUQsQ0FEZDtBQUFBLFNBRGI7QUFRSSxRQUFBLElBQUksRUFBQyxRQVJUO0FBQUE7QUFBQSxRQTdESixlQXlFSSxzQkFBQyxrQ0FBRDtBQUFBLGdDQUNJLHFCQUFDLG1DQUFEO0FBQ0ksVUFBQSxFQUFFLEVBQUMsUUFEUDtBQUVJLFVBQUEsR0FBRyxFQUFDLEdBRlI7QUFHSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ0MsQ0FBRDtBQUFBLG1CQUFPTCxTQUFTLENBQUNLLENBQUMsQ0FBQ0MsTUFBRixDQUFTQyxLQUFWLENBQWhCO0FBQUEsV0FIZDtBQUlJLFVBQUEsV0FBVyxFQUFDLElBSmhCO0FBS0ksVUFBQSxJQUFJLEVBQUMsUUFMVDtBQU1JLFVBQUEsS0FBSyxFQUFFUjtBQU5YLFVBREosZUFTSTtBQUFLLFVBQUEsU0FBUyxFQUFDLG9CQUFmO0FBQUEsaUNBQ0kscUJBQUMsb0NBQUQ7QUFDSSxZQUFBLFNBQVMsRUFBQyxlQURkO0FBRUksWUFBQSxRQUFRLEVBQUUsQ0FBQ0EsTUFBRCxJQUFXQSxNQUFNLEtBQUssQ0FGcEM7QUFHSSxZQUFBLE9BQU8sRUFBRTtBQUFBLHFCQUFNRCxrQkFBa0IsRUFBeEI7QUFBQSxhQUhiO0FBSUksWUFBQSxLQUFLLEVBQUVHLENBQUMsQ0FBQyxlQUFELENBSlo7QUFLSSxZQUFBLElBQUksRUFBQyxRQUxUO0FBQUEsbUNBT0k7QUFDSSxjQUFBLFNBQVMsRUFBQyxtQkFEZDtBQUVJLDZCQUFZO0FBRmhCO0FBUEo7QUFESixVQVRKO0FBQUEsUUF6RUo7QUFBQTtBQURKLElBREo7QUFzR0gsQ0E3R0Q7O0FBK0dBTCxVQUFVLENBQUNZLFNBQVgsR0FBdUI7QUFDbkJYLEVBQUFBLG1CQUFtQixFQUFFWSxtQkFBVUMsSUFEWjtBQUVuQlosRUFBQUEsa0JBQWtCLEVBQUVXLG1CQUFVQyxJQUZYO0FBR25CWCxFQUFBQSxNQUFNLEVBQUVVLG1CQUFVRSxTQUFWLENBQW9CLENBQUNGLG1CQUFVRyxNQUFYLEVBQW1CSCxtQkFBVVYsTUFBN0IsQ0FBcEIsQ0FIVztBQUluQkMsRUFBQUEsU0FBUyxFQUFFUyxtQkFBVUM7QUFKRixDQUF2QjtlQU9lZCxVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IENvbCwgUm93IH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCB7XG4gICAgQnV0dG9uSW5zZXJ0TnVtYmVyLFxuICAgIEJ1dHRvbk9wZXJhdG9yLFxuICAgIFN0eWxlZElucHV0R3JvdXAsXG4gICAgU3R5bGVkSW5wdXROdW1iZXIsXG59IGZyb20gJy4uL3N0eWxlZENvbXBvbmVudHMnO1xuXG5jb25zdCBPcGVyYXRpb25zID0gKHtcbiAgICBoYW5kbGVDbGlja09wZXJhdG9yLFxuICAgIGhhbmRsZUluc2VydE51bWJlcixcbiAgICBudW1iZXIsXG4gICAgc2V0TnVtYmVyLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICByZXR1cm4gKFxuICAgICAgICA8Um93PlxuICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICA8QnV0dG9uT3BlcmF0b3JcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsaWNrT3BlcmF0b3Ioe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnLScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFnOiAnLScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNfb3BlcmF0b3I6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLW1pbnVzXCI+PC9pPlxuICAgICAgICAgICAgICAgIDwvQnV0dG9uT3BlcmF0b3I+XG4gICAgICAgICAgICAgICAgPEJ1dHRvbk9wZXJhdG9yXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbGlja09wZXJhdG9yKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJysnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogJysnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzX29wZXJhdG9yOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhIGZhLXBsdXNcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9CdXR0b25PcGVyYXRvcj5cbiAgICAgICAgICAgICAgICA8QnV0dG9uT3BlcmF0b3JcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsaWNrT3BlcmF0b3Ioe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAneCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFnOiAnKicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNfb3BlcmF0b3I6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmEgZmEtdGltZXNcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9CdXR0b25PcGVyYXRvcj5cbiAgICAgICAgICAgICAgICA8QnV0dG9uT3BlcmF0b3JcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsaWNrT3BlcmF0b3Ioe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnw7cnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogJy8nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzX29wZXJhdG9yOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1kaXZpZGVcIj48L2k+XG4gICAgICAgICAgICAgICAgPC9CdXR0b25PcGVyYXRvcj5cbiAgICAgICAgICAgICAgICA8QnV0dG9uT3BlcmF0b3JcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsaWNrT3BlcmF0b3Ioe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnWycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFnOiAnWycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNfb3BlcmF0b3I6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgJiM5MTtcbiAgICAgICAgICAgICAgICA8L0J1dHRvbk9wZXJhdG9yPlxuICAgICAgICAgICAgICAgIDxCdXR0b25PcGVyYXRvclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2xpY2tPcGVyYXRvcih7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICddJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6ICddJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc19vcGVyYXRvcjogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAmIzkzO1xuICAgICAgICAgICAgICAgIDwvQnV0dG9uT3BlcmF0b3I+XG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dE51bWJlclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJudW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgbWluPVwiMVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldE51bWJlcihlLnRhcmdldC52YWx1ZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIi0tXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJudW1iZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e251bWJlcn1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbnB1dC1ncm91cC1hcHBlbmRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b25JbnNlcnROdW1iZXJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJib3JkZXItbGVmdC0wXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IW51bWJlciB8fCBudW1iZXIgPT09IDB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlSW5zZXJ0TnVtYmVyKCl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3QoJ0luc2VydCBudW1iZXInKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmYSBmYS1hcnJvdy1yaWdodFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFyaWEtaGlkZGVuPVwidHJ1ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uSW5zZXJ0TnVtYmVyPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG4gICAgKTtcbn07XG5cbk9wZXJhdGlvbnMucHJvcFR5cGVzID0ge1xuICAgIGhhbmRsZUNsaWNrT3BlcmF0b3I6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZUluc2VydE51bWJlcjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgbnVtYmVyOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSksXG4gICAgc2V0TnVtYmVyOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IE9wZXJhdGlvbnM7XG4iXX0=