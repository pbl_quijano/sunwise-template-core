"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var RefreshIcon = function RefreshIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M6.4 4.2v2.1l2.8-2.8L6.4.7v2.1A5.598 5.598 0 0 0 .8 8.4c0 1.099.322 2.121.868 2.982L2.69 10.36A4.11 4.11 0 0 1 2.2 8.4c0-2.317 1.883-4.2 4.2-4.2Zm4.732 1.218L10.11 6.44c.308.588.49 1.253.49 1.96 0 2.317-1.883 4.2-4.2 4.2v-2.1l-2.8 2.8 2.8 2.8V14c3.094 0 5.6-2.506 5.6-5.6a5.552 5.552 0 0 0-.868-2.982Z",
      fill: "#848BAB",
      fillRule: "nonzero"
    })
  }));
};

RefreshIcon.defaultProps = {
  width: "12",
  height: "17",
  xmlns: "http://www.w3.org/2000/svg"
};

var ResetDefaultValue = function ResetDefaultValue(_ref) {
  var defaultValue = _ref.defaultValue,
      resetToDefaultValue = _ref.resetToDefaultValue,
      showButtonReset = _ref.showButtonReset,
      value = _ref.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if (value != defaultValue && showButtonReset) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      className: "text-right",
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonReset, {
        onClick: function onClick() {
          return resetToDefaultValue();
        },
        type: "button",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
          alt: "reset",
          className: "mr-2",
          src: RefreshIcon
        }), t('Reset to default')]
      })
    });
  }

  return null;
};

ResetDefaultValue.propTypes = {
  defaultValue: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  resetToDefaultValue: _propTypes.default.func,
  showButtonReset: _propTypes.default.bool,
  value: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string])
};
var _default = ResetDefaultValue;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vUmVzZXREZWZhdWx0VmFsdWUuanMiXSwibmFtZXMiOlsiUmVmcmVzaEljb24iLCJSZXNldERlZmF1bHRWYWx1ZSIsImRlZmF1bHRWYWx1ZSIsInJlc2V0VG9EZWZhdWx0VmFsdWUiLCJzaG93QnV0dG9uUmVzZXQiLCJ2YWx1ZSIsInQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvbmVPZlR5cGUiLCJudW1iZXIiLCJzdHJpbmciLCJmdW5jIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBSUE7Ozs7Ozs7Ozs7OztJQUYyQkEsVyxZQUFBQSxXOzs7Ozs7Ozs7O0FBQUFBLFc7Ozs7OztBQUkzQixJQUFNQyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLE9BS3BCO0FBQUEsTUFKRkMsWUFJRSxRQUpGQSxZQUlFO0FBQUEsTUFIRkMsbUJBR0UsUUFIRkEsbUJBR0U7QUFBQSxNQUZGQyxlQUVFLFFBRkZBLGVBRUU7QUFBQSxNQURGQyxLQUNFLFFBREZBLEtBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQUlELEtBQUssSUFBSUgsWUFBVCxJQUF5QkUsZUFBN0IsRUFBOEM7QUFDMUMsd0JBQ0kscUJBQUMsbUJBQUQ7QUFBSyxNQUFBLFNBQVMsRUFBQyxZQUFmO0FBQUEsNkJBQ0ksc0JBQUMsNkJBQUQ7QUFDSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUFNRCxtQkFBbUIsRUFBekI7QUFBQSxTQURiO0FBRUksUUFBQSxJQUFJLEVBQUMsUUFGVDtBQUFBLGdDQUlJO0FBQUssVUFBQSxHQUFHLEVBQUMsT0FBVDtBQUFpQixVQUFBLFNBQVMsRUFBQyxNQUEzQjtBQUFrQyxVQUFBLEdBQUcsRUFBRUg7QUFBdkMsVUFKSixFQUtLTSxDQUFDLENBQUMsa0JBQUQsQ0FMTjtBQUFBO0FBREosTUFESjtBQVdIOztBQUNELFNBQU8sSUFBUDtBQUNILENBckJEOztBQXVCQUwsaUJBQWlCLENBQUNNLFNBQWxCLEdBQThCO0FBQzFCTCxFQUFBQSxZQUFZLEVBQUVNLG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVRSxNQUFYLEVBQW1CRixtQkFBVUcsTUFBN0IsQ0FBcEIsQ0FEWTtBQUUxQlIsRUFBQUEsbUJBQW1CLEVBQUVLLG1CQUFVSSxJQUZMO0FBRzFCUixFQUFBQSxlQUFlLEVBQUVJLG1CQUFVSyxJQUhEO0FBSTFCUixFQUFBQSxLQUFLLEVBQUVHLG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVRSxNQUFYLEVBQW1CRixtQkFBVUcsTUFBN0IsQ0FBcEI7QUFKbUIsQ0FBOUI7ZUFPZVYsaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgQ29sIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIFJlZnJlc2hJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9yZWZyZXNoLnN2Zyc7XG5cbmltcG9ydCB7IEJ1dHRvblJlc2V0IH0gZnJvbSAnLi4vc3R5bGVkQ29tcG9uZW50cyc7XG5cbmNvbnN0IFJlc2V0RGVmYXVsdFZhbHVlID0gKHtcbiAgICBkZWZhdWx0VmFsdWUsXG4gICAgcmVzZXRUb0RlZmF1bHRWYWx1ZSxcbiAgICBzaG93QnV0dG9uUmVzZXQsXG4gICAgdmFsdWUsXG59KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGlmICh2YWx1ZSAhPSBkZWZhdWx0VmFsdWUgJiYgc2hvd0J1dHRvblJlc2V0KSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8Q29sIGNsYXNzTmFtZT1cInRleHQtcmlnaHRcIj5cbiAgICAgICAgICAgICAgICA8QnV0dG9uUmVzZXRcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gcmVzZXRUb0RlZmF1bHRWYWx1ZSgpfVxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwicmVzZXRcIiBjbGFzc05hbWU9XCJtci0yXCIgc3JjPXtSZWZyZXNoSWNvbn0gLz5cbiAgICAgICAgICAgICAgICAgICAge3QoJ1Jlc2V0IHRvIGRlZmF1bHQnKX1cbiAgICAgICAgICAgICAgICA8L0J1dHRvblJlc2V0PlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICk7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xufTtcblxuUmVzZXREZWZhdWx0VmFsdWUucHJvcFR5cGVzID0ge1xuICAgIGRlZmF1bHRWYWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIHJlc2V0VG9EZWZhdWx0VmFsdWU6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNob3dCdXR0b25SZXNldDogUHJvcFR5cGVzLmJvb2wsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5udW1iZXIsIFByb3BUeXBlcy5zdHJpbmddKSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFJlc2V0RGVmYXVsdFZhbHVlO1xuIl19