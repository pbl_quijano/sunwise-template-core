"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactRedux = require("react-redux");

var actions = _interopRequireWildcard(require("../../actions"));

var _helpers = require("../../helpers");

var _TagFormWithCalculator = _interopRequireDefault(require("./TagFormWithCalculator"));

var _TagLibraryModal = _interopRequireDefault(require("./TagLibraryModal"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Container = function Container(_ref) {
  var catalogs = _ref.catalogs,
      tagsLocale = _ref.tagsLocale,
      contentViewState = _ref.contentViewState,
      editionLevel = _ref.editionLevel,
      handleEditTag = _ref.handleEditTag,
      handleInsertTag = _ref.handleInsertTag,
      hasProposalSelector = _ref.hasProposalSelector,
      isInitialazing = _ref.isInitialazing,
      setIsOpenTagLibraryModal = _ref.setIsOpenTagLibraryModal;
  var handleOnSubmit = (0, _helpers.handleSubmitCustomTagBuild)(contentViewState, tagsLocale, handleEditTag, handleInsertTag);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Container, {
      fluid: true,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagFormWithCalculator.default, {
        tagsLocale: tagsLocale,
        contentViewState: contentViewState,
        editionLevel: editionLevel,
        handleOnSubmit: handleOnSubmit,
        isInitialazing: isInitialazing,
        setIsOpenTagLibraryModal: setIsOpenTagLibraryModal
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagLibraryModal.default, {
      catalogs: catalogs,
      tagsLocale: tagsLocale,
      editionLevel: editionLevel,
      hasProposalSelector: hasProposalSelector,
      isInitialazing: isInitialazing
    })]
  });
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    setIsOpenTagLibraryModal: function setIsOpenTagLibraryModal(isOpenModal) {
      return dispatch(actions.setIsOpenTagLibraryModal(isOpenModal));
    }
  };
};

Container.propTypes = {
  catalogs: _propTypes.default.array,
  tagsLocale: _propTypes.default.string,
  contentViewState: _propTypes.default.number,
  editionLevel: _propTypes.default.string,
  handleEditTag: _propTypes.default.func,
  handleInsertTag: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  isInitialazing: _propTypes.default.bool,
  reset: _propTypes.default.func,
  setIsOpenTagLibraryModal: _propTypes.default.func
};

var _default = (0, _reactRedux.connect)(null, mapDispatchToProps)(Container);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vaW5kZXguanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwiY2F0YWxvZ3MiLCJ0YWdzTG9jYWxlIiwiY29udGVudFZpZXdTdGF0ZSIsImVkaXRpb25MZXZlbCIsImhhbmRsZUVkaXRUYWciLCJoYW5kbGVJbnNlcnRUYWciLCJoYXNQcm9wb3NhbFNlbGVjdG9yIiwiaXNJbml0aWFsYXppbmciLCJzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwiLCJoYW5kbGVPblN1Ym1pdCIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwiaXNPcGVuTW9kYWwiLCJhY3Rpb25zIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJzdHJpbmciLCJudW1iZXIiLCJmdW5jIiwiYm9vbCIsInJlc2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBRyxTQUFaQSxTQUFZLE9BVVo7QUFBQSxNQVRGQyxRQVNFLFFBVEZBLFFBU0U7QUFBQSxNQVJGQyxVQVFFLFFBUkZBLFVBUUU7QUFBQSxNQVBGQyxnQkFPRSxRQVBGQSxnQkFPRTtBQUFBLE1BTkZDLFlBTUUsUUFORkEsWUFNRTtBQUFBLE1BTEZDLGFBS0UsUUFMRkEsYUFLRTtBQUFBLE1BSkZDLGVBSUUsUUFKRkEsZUFJRTtBQUFBLE1BSEZDLG1CQUdFLFFBSEZBLG1CQUdFO0FBQUEsTUFGRkMsY0FFRSxRQUZGQSxjQUVFO0FBQUEsTUFERkMsd0JBQ0UsUUFERkEsd0JBQ0U7QUFDRixNQUFNQyxjQUFjLEdBQUcseUNBQ25CUCxnQkFEbUIsRUFFbkJELFVBRm1CLEVBR25CRyxhQUhtQixFQUluQkMsZUFKbUIsQ0FBdkI7QUFPQSxzQkFDSTtBQUFBLDRCQUNJLHFCQUFDLHlCQUFEO0FBQW9CLE1BQUEsS0FBSyxNQUF6QjtBQUFBLDZCQUNJLHFCQUFDLDhCQUFEO0FBQ0ksUUFBQSxVQUFVLEVBQUVKLFVBRGhCO0FBRUksUUFBQSxnQkFBZ0IsRUFBRUMsZ0JBRnRCO0FBR0ksUUFBQSxZQUFZLEVBQUVDLFlBSGxCO0FBSUksUUFBQSxjQUFjLEVBQUVNLGNBSnBCO0FBS0ksUUFBQSxjQUFjLEVBQUVGLGNBTHBCO0FBTUksUUFBQSx3QkFBd0IsRUFBRUM7QUFOOUI7QUFESixNQURKLGVBV0kscUJBQUMsd0JBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRVIsUUFEZDtBQUVJLE1BQUEsVUFBVSxFQUFFQyxVQUZoQjtBQUdJLE1BQUEsWUFBWSxFQUFFRSxZQUhsQjtBQUlJLE1BQUEsbUJBQW1CLEVBQUVHLG1CQUp6QjtBQUtJLE1BQUEsY0FBYyxFQUFFQztBQUxwQixNQVhKO0FBQUEsSUFESjtBQXFCSCxDQXZDRDs7QUF5Q0EsSUFBTUcsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0Q0gsSUFBQUEsd0JBQXdCLEVBQUUsa0NBQUNJLFdBQUQ7QUFBQSxhQUN0QkQsUUFBUSxDQUFDRSxPQUFPLENBQUNMLHdCQUFSLENBQWlDSSxXQUFqQyxDQUFELENBRGM7QUFBQTtBQURZLEdBQWY7QUFBQSxDQUEzQjs7QUFLQWIsU0FBUyxDQUFDZSxTQUFWLEdBQXNCO0FBQ2xCZCxFQUFBQSxRQUFRLEVBQUVlLG1CQUFVQyxLQURGO0FBRWxCZixFQUFBQSxVQUFVLEVBQUVjLG1CQUFVRSxNQUZKO0FBR2xCZixFQUFBQSxnQkFBZ0IsRUFBRWEsbUJBQVVHLE1BSFY7QUFJbEJmLEVBQUFBLFlBQVksRUFBRVksbUJBQVVFLE1BSk47QUFLbEJiLEVBQUFBLGFBQWEsRUFBRVcsbUJBQVVJLElBTFA7QUFNbEJkLEVBQUFBLGVBQWUsRUFBRVUsbUJBQVVJLElBTlQ7QUFPbEJiLEVBQUFBLG1CQUFtQixFQUFFUyxtQkFBVUssSUFQYjtBQVFsQmIsRUFBQUEsY0FBYyxFQUFFUSxtQkFBVUssSUFSUjtBQVNsQkMsRUFBQUEsS0FBSyxFQUFFTixtQkFBVUksSUFUQztBQVVsQlgsRUFBQUEsd0JBQXdCLEVBQUVPLG1CQUFVSTtBQVZsQixDQUF0Qjs7ZUFhZSx5QkFBUSxJQUFSLEVBQWNULGtCQUFkLEVBQWtDWCxTQUFsQyxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IENvbnRhaW5lciBhcyBCb290c3RyYXBDb250YWluZXIgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi8uLi9hY3Rpb25zJztcbmltcG9ydCB7IGhhbmRsZVN1Ym1pdEN1c3RvbVRhZ0J1aWxkIH0gZnJvbSAnLi4vLi4vaGVscGVycyc7XG5cbmltcG9ydCBUYWdGb3JtV2l0aENhbGN1bGF0b3IgZnJvbSAnLi9UYWdGb3JtV2l0aENhbGN1bGF0b3InO1xuaW1wb3J0IFRhZ0xpYnJhcnlNb2RhbCBmcm9tICcuL1RhZ0xpYnJhcnlNb2RhbCc7XG5cbmNvbnN0IENvbnRhaW5lciA9ICh7XG4gICAgY2F0YWxvZ3MsXG4gICAgdGFnc0xvY2FsZSxcbiAgICBjb250ZW50Vmlld1N0YXRlLFxuICAgIGVkaXRpb25MZXZlbCxcbiAgICBoYW5kbGVFZGl0VGFnLFxuICAgIGhhbmRsZUluc2VydFRhZyxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yLFxuICAgIGlzSW5pdGlhbGF6aW5nLFxuICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbCxcbn0pID0+IHtcbiAgICBjb25zdCBoYW5kbGVPblN1Ym1pdCA9IGhhbmRsZVN1Ym1pdEN1c3RvbVRhZ0J1aWxkKFxuICAgICAgICBjb250ZW50Vmlld1N0YXRlLFxuICAgICAgICB0YWdzTG9jYWxlLFxuICAgICAgICBoYW5kbGVFZGl0VGFnLFxuICAgICAgICBoYW5kbGVJbnNlcnRUYWdcbiAgICApO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPD5cbiAgICAgICAgICAgIDxCb290c3RyYXBDb250YWluZXIgZmx1aWQ+XG4gICAgICAgICAgICAgICAgPFRhZ0Zvcm1XaXRoQ2FsY3VsYXRvclxuICAgICAgICAgICAgICAgICAgICB0YWdzTG9jYWxlPXt0YWdzTG9jYWxlfVxuICAgICAgICAgICAgICAgICAgICBjb250ZW50Vmlld1N0YXRlPXtjb250ZW50Vmlld1N0YXRlfVxuICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlT25TdWJtaXQ9e2hhbmRsZU9uU3VibWl0fVxuICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZz17aXNJbml0aWFsYXppbmd9XG4gICAgICAgICAgICAgICAgICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbD17c2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0Jvb3RzdHJhcENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxUYWdMaWJyYXJ5TW9kYWxcbiAgICAgICAgICAgICAgICBjYXRhbG9ncz17Y2F0YWxvZ3N9XG4gICAgICAgICAgICAgICAgdGFnc0xvY2FsZT17dGFnc0xvY2FsZX1cbiAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cbiAgICAgICAgICAgICAgICBoYXNQcm9wb3NhbFNlbGVjdG9yPXtoYXNQcm9wb3NhbFNlbGVjdG9yfVxuICAgICAgICAgICAgICAgIGlzSW5pdGlhbGF6aW5nPXtpc0luaXRpYWxhemluZ31cbiAgICAgICAgICAgIC8+XG4gICAgICAgIDwvPlxuICAgICk7XG59O1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+ICh7XG4gICAgc2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsOiAoaXNPcGVuTW9kYWwpID0+XG4gICAgICAgIGRpc3BhdGNoKGFjdGlvbnMuc2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsKGlzT3Blbk1vZGFsKSksXG59KTtcblxuQ29udGFpbmVyLnByb3BUeXBlcyA9IHtcbiAgICBjYXRhbG9nczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHRhZ3NMb2NhbGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29udGVudFZpZXdTdGF0ZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGFuZGxlRWRpdFRhZzogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlSW5zZXJ0VGFnOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0luaXRpYWxhemluZzogUHJvcFR5cGVzLmJvb2wsXG4gICAgcmVzZXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbDogUHJvcFR5cGVzLmZ1bmMsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG51bGwsIG1hcERpc3BhdGNoVG9Qcm9wcykoQ29udGFpbmVyKTtcbiJdfQ==