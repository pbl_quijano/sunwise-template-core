"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reduxForm = require("redux-form");

var _ReduxFieldInput = _interopRequireDefault(require("../../../../components/ReduxFieldInput"));

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Formula = function Formula(_ref) {
  var calculator = _ref.calculator;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
    className: "mt-3",
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
      children: [calculator.map(function (item) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.TagLabel, {
          background: item.is_operator ? 'transparent' : item.color,
          size: item.is_operator ? 15 : 12,
          children: item.title
        }, "tag-label-".concat(Math.random()));
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reduxForm.Field, {
        component: _ReduxFieldInput.default,
        className: "d-none",
        name: "tag",
        type: "text"
      })]
    })
  });
};

Formula.propTypes = {
  calculator: _propTypes.default.array
};
var _default = Formula;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vRm9ybXVsYS5qcyJdLCJuYW1lcyI6WyJGb3JtdWxhIiwiY2FsY3VsYXRvciIsIm1hcCIsIml0ZW0iLCJpc19vcGVyYXRvciIsImNvbG9yIiwidGl0bGUiLCJNYXRoIiwicmFuZG9tIiwiUmVkdXhGaWVsZElucHV0IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsT0FBTyxHQUFHLFNBQVZBLE9BQVU7QUFBQSxNQUFHQyxVQUFILFFBQUdBLFVBQUg7QUFBQSxzQkFDWixxQkFBQyxtQkFBRDtBQUFLLElBQUEsU0FBUyxFQUFDLE1BQWY7QUFBQSwyQkFDSSxzQkFBQyxtQkFBRDtBQUFBLGlCQUNLQSxVQUFVLENBQUNDLEdBQVgsQ0FBZSxVQUFDQyxJQUFEO0FBQUEsNEJBQ1oscUJBQUMsMEJBQUQ7QUFDSSxVQUFBLFVBQVUsRUFBRUEsSUFBSSxDQUFDQyxXQUFMLEdBQW1CLGFBQW5CLEdBQW1DRCxJQUFJLENBQUNFLEtBRHhEO0FBR0ksVUFBQSxJQUFJLEVBQUVGLElBQUksQ0FBQ0MsV0FBTCxHQUFtQixFQUFuQixHQUF3QixFQUhsQztBQUFBLG9CQUtLRCxJQUFJLENBQUNHO0FBTFYsK0JBRXNCQyxJQUFJLENBQUNDLE1BQUwsRUFGdEIsRUFEWTtBQUFBLE9BQWYsQ0FETCxlQVVJLHFCQUFDLGdCQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUVDLHdCQURmO0FBRUksUUFBQSxTQUFTLEVBQUMsUUFGZDtBQUdJLFFBQUEsSUFBSSxFQUFDLEtBSFQ7QUFJSSxRQUFBLElBQUksRUFBQztBQUpULFFBVko7QUFBQTtBQURKLElBRFk7QUFBQSxDQUFoQjs7QUFzQkFULE9BQU8sQ0FBQ1UsU0FBUixHQUFvQjtBQUNoQlQsRUFBQUEsVUFBVSxFQUFFVSxtQkFBVUM7QUFETixDQUFwQjtlQUllWixPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IENvbCwgUm93IH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IEZpZWxkIH0gZnJvbSAncmVkdXgtZm9ybSc7XG5cbmltcG9ydCBSZWR1eEZpZWxkSW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvUmVkdXhGaWVsZElucHV0JztcblxuaW1wb3J0IHsgVGFnTGFiZWwgfSBmcm9tICcuLi9zdHlsZWRDb21wb25lbnRzJztcblxuY29uc3QgRm9ybXVsYSA9ICh7IGNhbGN1bGF0b3IgfSkgPT4gKFxuICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtM1wiPlxuICAgICAgICA8Q29sPlxuICAgICAgICAgICAge2NhbGN1bGF0b3IubWFwKChpdGVtKSA9PiAoXG4gICAgICAgICAgICAgICAgPFRhZ0xhYmVsXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ9e2l0ZW0uaXNfb3BlcmF0b3IgPyAndHJhbnNwYXJlbnQnIDogaXRlbS5jb2xvcn1cbiAgICAgICAgICAgICAgICAgICAga2V5PXtgdGFnLWxhYmVsLSR7TWF0aC5yYW5kb20oKX1gfVxuICAgICAgICAgICAgICAgICAgICBzaXplPXtpdGVtLmlzX29wZXJhdG9yID8gMTUgOiAxMn1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHtpdGVtLnRpdGxlfVxuICAgICAgICAgICAgICAgIDwvVGFnTGFiZWw+XG4gICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDxGaWVsZFxuICAgICAgICAgICAgICAgIGNvbXBvbmVudD17UmVkdXhGaWVsZElucHV0fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtbm9uZVwiXG4gICAgICAgICAgICAgICAgbmFtZT1cInRhZ1wiXG4gICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9Db2w+XG4gICAgPC9Sb3c+XG4pO1xuXG5Gb3JtdWxhLnByb3BUeXBlcyA9IHtcbiAgICBjYWxjdWxhdG9yOiBQcm9wVHlwZXMuYXJyYXksXG59O1xuXG5leHBvcnQgZGVmYXVsdCBGb3JtdWxhO1xuIl19