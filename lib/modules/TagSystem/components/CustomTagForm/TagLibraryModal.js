"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ModalTitle = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ModalBootstrap = _interopRequireDefault(require("../../../../components/ModalBootstrap"));

var actions = _interopRequireWildcard(require("../../actions"));

var selectors = _interopRequireWildcard(require("../../selectors"));

var _TagLibrary = _interopRequireDefault(require("../TagLibrary"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ModalTitle = _styledComponents.default.h3(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 18px;\n    font-weight: bold;\n    letter-spacing: 0;\n    line-height: 32px;\n    margin-bottom: 0;\n"])));

exports.ModalTitle = ModalTitle;

var TagLibraryModal = function TagLibraryModal(_ref) {
  var catalogs = _ref.catalogs,
      tagsLocale = _ref.tagsLocale,
      editionLevel = _ref.editionLevel,
      hasProposalSelector = _ref.hasProposalSelector,
      isInitialazing = _ref.isInitialazing,
      isOpenTagLibraryModal = _ref.isOpenTagLibraryModal,
      prepareCustomTagFromSelectTag = _ref.prepareCustomTagFromSelectTag,
      setIsOpenTagLibraryModal = _ref.setIsOpenTagLibraryModal;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ModalBootstrap.default, {
    backdropClassName: "second-modal-backdrop",
    className: "second-modal",
    closeButton: false,
    onHide: function onHide() {
      return setIsOpenTagLibraryModal(false);
    },
    show: isOpenTagLibraryModal,
    size: "xl",
    title: /*#__PURE__*/(0, _jsxRuntime.jsxs)(ModalTitle, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
        className: "btn btn-transparent p-0 mr-2",
        onClick: function onClick() {
          return setIsOpenTagLibraryModal(false);
        },
        title: t('Back'),
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-arrow-left mr-2"
        })
      }), t('Select a tag from the library and insert to the formula')]
    }),
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagLibrary.default, {
      catalogs: catalogs,
      tagsLocale: tagsLocale,
      editionLevel: editionLevel,
      hasProposalSelector: hasProposalSelector,
      isInitialazing: isInitialazing,
      isFromCustomTag: true,
      prepareCustomTagFromSelectTag: prepareCustomTagFromSelectTag
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  isOpenTagLibraryModal: selectors.getIsOpenTagLibraryModal
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    prepareCustomTagFromSelectTag: function prepareCustomTagFromSelectTag(tag) {
      return dispatch(actions.prepareCustomTagFromSelectTag(tag));
    },
    setIsOpenTagLibraryModal: function setIsOpenTagLibraryModal(isOpenModal) {
      return dispatch(actions.setIsOpenTagLibraryModal(isOpenModal));
    }
  };
};

TagLibraryModal.propTypes = {
  catalogs: _propTypes.default.array,
  tagsLocale: _propTypes.default.string,
  editionLevel: _propTypes.default.string,
  hasProposalSelector: _propTypes.default.bool,
  isInitialazing: _propTypes.default.bool,
  isOpenTagLibraryModal: _propTypes.default.bool,
  prepareCustomTagFromSelectTag: _propTypes.default.func,
  setIsOpenTagLibraryModal: _propTypes.default.func
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TagLibraryModal);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vVGFnTGlicmFyeU1vZGFsLmpzIl0sIm5hbWVzIjpbIk1vZGFsVGl0bGUiLCJzdHlsZWQiLCJoMyIsIlRhZ0xpYnJhcnlNb2RhbCIsImNhdGFsb2dzIiwidGFnc0xvY2FsZSIsImVkaXRpb25MZXZlbCIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJpc0luaXRpYWxhemluZyIsImlzT3BlblRhZ0xpYnJhcnlNb2RhbCIsInByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnIiwic2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsIiwidCIsIm1hcFN0YXRlVG9Qcm9wcyIsInNlbGVjdG9ycyIsImdldElzT3BlblRhZ0xpYnJhcnlNb2RhbCIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwidGFnIiwiYWN0aW9ucyIsImlzT3Blbk1vZGFsIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJzdHJpbmciLCJib29sIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FBRU8sSUFBTUEsVUFBVSxHQUFHQywwQkFBT0MsRUFBVixpTkFBaEI7Ozs7QUFTUCxJQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLE9BU2xCO0FBQUEsTUFSRkMsUUFRRSxRQVJGQSxRQVFFO0FBQUEsTUFQRkMsVUFPRSxRQVBGQSxVQU9FO0FBQUEsTUFORkMsWUFNRSxRQU5GQSxZQU1FO0FBQUEsTUFMRkMsbUJBS0UsUUFMRkEsbUJBS0U7QUFBQSxNQUpGQyxjQUlFLFFBSkZBLGNBSUU7QUFBQSxNQUhGQyxxQkFHRSxRQUhGQSxxQkFHRTtBQUFBLE1BRkZDLDZCQUVFLFFBRkZBLDZCQUVFO0FBQUEsTUFERkMsd0JBQ0UsUUFERkEsd0JBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUNJLHFCQUFDLHVCQUFEO0FBQ0ksSUFBQSxpQkFBaUIsRUFBQyx1QkFEdEI7QUFFSSxJQUFBLFNBQVMsRUFBQyxjQUZkO0FBR0ksSUFBQSxXQUFXLEVBQUUsS0FIakI7QUFJSSxJQUFBLE1BQU0sRUFBRTtBQUFBLGFBQU1ELHdCQUF3QixDQUFDLEtBQUQsQ0FBOUI7QUFBQSxLQUpaO0FBS0ksSUFBQSxJQUFJLEVBQUVGLHFCQUxWO0FBTUksSUFBQSxJQUFJLEVBQUMsSUFOVDtBQU9JLElBQUEsS0FBSyxlQUNELHNCQUFDLFVBQUQ7QUFBQSw4QkFDSTtBQUNJLFFBQUEsU0FBUyxFQUFDLDhCQURkO0FBRUksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFBTUUsd0JBQXdCLENBQUMsS0FBRCxDQUE5QjtBQUFBLFNBRmI7QUFHSSxRQUFBLEtBQUssRUFBRUMsQ0FBQyxDQUFDLE1BQUQsQ0FIWjtBQUlJLFFBQUEsSUFBSSxFQUFDLFFBSlQ7QUFBQSwrQkFNSTtBQUFHLFVBQUEsU0FBUyxFQUFDO0FBQWI7QUFOSixRQURKLEVBU0tBLENBQUMsQ0FDRSx5REFERixDQVROO0FBQUEsTUFSUjtBQUFBLDJCQXVCSSxxQkFBQyxtQkFBRDtBQUNJLE1BQUEsUUFBUSxFQUFFUixRQURkO0FBRUksTUFBQSxVQUFVLEVBQUVDLFVBRmhCO0FBR0ksTUFBQSxZQUFZLEVBQUVDLFlBSGxCO0FBSUksTUFBQSxtQkFBbUIsRUFBRUMsbUJBSnpCO0FBS0ksTUFBQSxjQUFjLEVBQUVDLGNBTHBCO0FBTUksTUFBQSxlQUFlLE1BTm5CO0FBT0ksTUFBQSw2QkFBNkIsRUFBRUU7QUFQbkM7QUF2QkosSUFESjtBQW1DSCxDQTlDRDs7QUFnREEsSUFBTUcsZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q0osRUFBQUEscUJBQXFCLEVBQUVLLFNBQVMsQ0FBQ0M7QUFEWSxDQUF6QixDQUF4Qjs7QUFJQSxJQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUFlO0FBQ3RDUCxJQUFBQSw2QkFBNkIsRUFBRSx1Q0FBQ1EsR0FBRDtBQUFBLGFBQzNCRCxRQUFRLENBQUNFLE9BQU8sQ0FBQ1QsNkJBQVIsQ0FBc0NRLEdBQXRDLENBQUQsQ0FEbUI7QUFBQSxLQURPO0FBR3RDUCxJQUFBQSx3QkFBd0IsRUFBRSxrQ0FBQ1MsV0FBRDtBQUFBLGFBQ3RCSCxRQUFRLENBQUNFLE9BQU8sQ0FBQ1Isd0JBQVIsQ0FBaUNTLFdBQWpDLENBQUQsQ0FEYztBQUFBO0FBSFksR0FBZjtBQUFBLENBQTNCOztBQU9BakIsZUFBZSxDQUFDa0IsU0FBaEIsR0FBNEI7QUFDeEJqQixFQUFBQSxRQUFRLEVBQUVrQixtQkFBVUMsS0FESTtBQUV4QmxCLEVBQUFBLFVBQVUsRUFBRWlCLG1CQUFVRSxNQUZFO0FBR3hCbEIsRUFBQUEsWUFBWSxFQUFFZ0IsbUJBQVVFLE1BSEE7QUFJeEJqQixFQUFBQSxtQkFBbUIsRUFBRWUsbUJBQVVHLElBSlA7QUFLeEJqQixFQUFBQSxjQUFjLEVBQUVjLG1CQUFVRyxJQUxGO0FBTXhCaEIsRUFBQUEscUJBQXFCLEVBQUVhLG1CQUFVRyxJQU5UO0FBT3hCZixFQUFBQSw2QkFBNkIsRUFBRVksbUJBQVVJLElBUGpCO0FBUXhCZixFQUFBQSx3QkFBd0IsRUFBRVcsbUJBQVVJO0FBUlosQ0FBNUI7O2VBV2UseUJBQVFiLGVBQVIsRUFBeUJHLGtCQUF6QixFQUE2Q2IsZUFBN0MsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgTW9kYWxCb290c3RyYXAgZnJvbSAnQGNvbXBvbmVudHMvTW9kYWxCb290c3RyYXAnO1xuXG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJy4uLy4uL3NlbGVjdG9ycyc7XG5pbXBvcnQgVGFnTGlicmFyeSBmcm9tICcuLi9UYWdMaWJyYXJ5JztcblxuZXhwb3J0IGNvbnN0IE1vZGFsVGl0bGUgPSBzdHlsZWQuaDNgXG4gICAgY29sb3I6ICMwMDI0Mzg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG5gO1xuXG5jb25zdCBUYWdMaWJyYXJ5TW9kYWwgPSAoe1xuICAgIGNhdGFsb2dzLFxuICAgIHRhZ3NMb2NhbGUsXG4gICAgZWRpdGlvbkxldmVsLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgaXNJbml0aWFsYXppbmcsXG4gICAgaXNPcGVuVGFnTGlicmFyeU1vZGFsLFxuICAgIHByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnLFxuICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbCxcbn0pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPE1vZGFsQm9vdHN0cmFwXG4gICAgICAgICAgICBiYWNrZHJvcENsYXNzTmFtZT1cInNlY29uZC1tb2RhbC1iYWNrZHJvcFwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJzZWNvbmQtbW9kYWxcIlxuICAgICAgICAgICAgY2xvc2VCdXR0b249e2ZhbHNlfVxuICAgICAgICAgICAgb25IaWRlPXsoKSA9PiBzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwoZmFsc2UpfVxuICAgICAgICAgICAgc2hvdz17aXNPcGVuVGFnTGlicmFyeU1vZGFsfVxuICAgICAgICAgICAgc2l6ZT1cInhsXCJcbiAgICAgICAgICAgIHRpdGxlPXtcbiAgICAgICAgICAgICAgICA8TW9kYWxUaXRsZT5cbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYnRuIGJ0bi10cmFuc3BhcmVudCBwLTAgbXItMlwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwoZmFsc2UpfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3QoJ0JhY2snKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtYXJyb3ctbGVmdCBtci0yXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAge3QoXG4gICAgICAgICAgICAgICAgICAgICAgICAnU2VsZWN0IGEgdGFnIGZyb20gdGhlIGxpYnJhcnkgYW5kIGluc2VydCB0byB0aGUgZm9ybXVsYSdcbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L01vZGFsVGl0bGU+XG4gICAgICAgICAgICB9XG4gICAgICAgID5cbiAgICAgICAgICAgIDxUYWdMaWJyYXJ5XG4gICAgICAgICAgICAgICAgY2F0YWxvZ3M9e2NhdGFsb2dzfVxuICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGU9e3RhZ3NMb2NhbGV9XG4gICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XG4gICAgICAgICAgICAgICAgaGFzUHJvcG9zYWxTZWxlY3Rvcj17aGFzUHJvcG9zYWxTZWxlY3Rvcn1cbiAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZz17aXNJbml0aWFsYXppbmd9XG4gICAgICAgICAgICAgICAgaXNGcm9tQ3VzdG9tVGFnXG4gICAgICAgICAgICAgICAgcHJlcGFyZUN1c3RvbVRhZ0Zyb21TZWxlY3RUYWc9e3ByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9Nb2RhbEJvb3RzdHJhcD5cbiAgICApO1xufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcbiAgICBpc09wZW5UYWdMaWJyYXJ5TW9kYWw6IHNlbGVjdG9ycy5nZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwsXG59KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiAoe1xuICAgIHByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnOiAodGFnKSA9PlxuICAgICAgICBkaXNwYXRjaChhY3Rpb25zLnByZXBhcmVDdXN0b21UYWdGcm9tU2VsZWN0VGFnKHRhZykpLFxuICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbDogKGlzT3Blbk1vZGFsKSA9PlxuICAgICAgICBkaXNwYXRjaChhY3Rpb25zLnNldElzT3BlblRhZ0xpYnJhcnlNb2RhbChpc09wZW5Nb2RhbCkpLFxufSk7XG5cblRhZ0xpYnJhcnlNb2RhbC5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICB0YWdzTG9jYWxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0luaXRpYWxhemluZzogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNPcGVuVGFnTGlicmFyeU1vZGFsOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBwcmVwYXJlQ3VzdG9tVGFnRnJvbVNlbGVjdFRhZzogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2V0SXNPcGVuVGFnTGlicmFyeU1vZGFsOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFRhZ0xpYnJhcnlNb2RhbCk7XG4iXX0=