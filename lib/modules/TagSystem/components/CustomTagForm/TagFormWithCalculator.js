"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _get = _interopRequireDefault(require("lodash/get"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reduxForm = require("redux-form");

var _reselect = require("reselect");

var _ReduxFieldInput = _interopRequireDefault(require("../../../../components/ReduxFieldInput"));

var _ReduxFieldSelect = _interopRequireDefault(require("../../../../components/ReduxFieldSelect"));

var _tags = require("../../../../constants/tags");

var _types = require("../../../../constants/types");

var _customTagValidate = _interopRequireDefault(require("../../customTagValidate"));

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

var _styledComponents = require("../styledComponents");

var _Formula = _interopRequireDefault(require("./Formula"));

var _FormulaControlButtons = _interopRequireDefault(require("./FormulaControlButtons"));

var _Operations = _interopRequireDefault(require("./Operations"));

var _ReduxFieldArrayItems = _interopRequireDefault(require("./ReduxFieldArrayItems"));

var _ResetDefaultValue = _interopRequireDefault(require("./ResetDefaultValue"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var CalculatorIcon = function CalculatorIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-5-7h24v24H-5z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
        fill: "#FF9A00",
        fillRule: "nonzero",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
          d: "M12.444 0H1.556C.7 0 0 .7 0 1.556v10.888C0 13.3.7 14 1.556 14h10.888C13.3 14 14 13.3 14 12.444V1.556C14 .7 13.3 0 12.444 0Zm0 12.444H1.556V1.556h10.888v10.888Z"
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
          d: "M3 4h4v1H3zM8 10h4v1H8zM8 8h4v1H8zM4.455 12h1.09v-1.455H7v-1.09H5.545V8h-1.09v1.455H3v1.09h1.455zM8.858 7 10 5.86 11.142 7 12 6.143l-1.142-1.147L12 3.856 11.142 3 10 4.14 8.858 3 8 3.857l1.142 1.139L8 6.143z"
        })]
      })]
    })
  }));
};

CalculatorIcon.defaultProps = {
  width: "14",
  height: "14",
  xmlns: "http://www.w3.org/2000/svg"
};

var SettingsIcon = function SettingsIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-1-1h19.2v19.2H-1z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M14.544 9.384c.032-.256.056-.512.056-.784s-.024-.528-.056-.784l1.688-1.32a.404.404 0 0 0 .096-.512l-1.6-2.768a.4.4 0 0 0-.488-.176l-1.992.8a5.845 5.845 0 0 0-1.352-.784l-.304-2.12A.39.39 0 0 0 10.2.6H7a.39.39 0 0 0-.392.336l-.304 2.12c-.488.2-.936.472-1.352.784l-1.992-.8a.453.453 0 0 0-.144-.024.396.396 0 0 0-.344.2l-1.6 2.768a.394.394 0 0 0 .096.512l1.688 1.32c-.032.256-.056.52-.056.784s.024.528.056.784l-1.688 1.32a.404.404 0 0 0-.096.512l1.6 2.768a.4.4 0 0 0 .488.176l1.992-.8c.416.32.864.584 1.352.784l.304 2.12A.39.39 0 0 0 7 16.6h3.2a.39.39 0 0 0 .392-.336l.304-2.12c.488-.2.936-.472 1.352-.784l1.992.8a.453.453 0 0 0 .144.024.396.396 0 0 0 .344-.2l1.6-2.768a.404.404 0 0 0-.096-.512l-1.688-1.32ZM12.96 8.016c.032.248.04.416.04.584 0 .168-.016.344-.04.584l-.112.904.712.56.864.672-.56.968-1.016-.408-.832-.336-.72.544a4.685 4.685 0 0 1-1 .584l-.848.344-.128.904L9.16 15H8.04l-.152-1.08-.128-.904-.848-.344a4.54 4.54 0 0 1-.984-.568l-.728-.56-.848.344-1.016.408-.56-.968.864-.672.712-.56-.112-.904A6.435 6.435 0 0 1 4.2 8.6c0-.16.016-.344.04-.584l.112-.904-.712-.56-.864-.672.56-.968 1.016.408.832.336.72-.544a4.685 4.685 0 0 1 1-.584l.848-.344.128-.904.16-1.08h1.112l.152 1.08.128.904.848.344c.344.144.664.328.984.568l.728.56.848-.344 1.016-.408.56.968-.856.68-.712.56.112.904ZM8.6 5.4a3.2 3.2 0 1 0 0 6.4 3.2 3.2 0 1 0 0-6.4Zm0 4.8c-.88 0-1.6-.72-1.6-1.6C7 7.72 7.72 7 8.6 7c.88 0 1.6.72 1.6 1.6 0 .88-.72 1.6-1.6 1.6Z",
        fill: "#FF9A00",
        fillRule: "nonzero"
      })]
    })
  }));
};

SettingsIcon.defaultProps = {
  width: "17",
  height: "17",
  xmlns: "http://www.w3.org/2000/svg"
};

var TagFormWithCalculator = function TagFormWithCalculator(_ref) {
  var changeInput = _ref.changeInput,
      contentViewState = _ref.contentViewState,
      currencies = _ref.currencies,
      decimalFormatsData = _ref.decimalFormatsData,
      editionLevel = _ref.editionLevel,
      tagsLocale = _ref.tagsLocale,
      errors = _ref.errors,
      formValues = _ref.formValues,
      handleOnSubmit = _ref.handleOnSubmit,
      handleSubmit = _ref.handleSubmit,
      isInitialazing = _ref.isInitialazing,
      setIsOpenTagLibraryModal = _ref.setIsOpenTagLibraryModal,
      unitFormatsData = _ref.unitFormatsData;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useState = (0, _react.useState)(1),
      _useState2 = _slicedToArray(_useState, 2),
      number = _useState2[0],
      setNumber = _useState2[1];

  var data = (0, _get.default)(formValues, 'calculator', []);
  (0, _react.useEffect)(function () {
    (0, _helpers.updateTag)(changeInput, data);
  }, [data]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: "mt-3",
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form, {
      onSubmit: handleSubmit(handleOnSubmit),
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "m-0 mt-3 align-items-center",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
            className: "mb-0",
            children: [!(0, _isEmpty.default)(formValues.title) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
              children: t('Tag title')
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledTitleField, {
              component: _ReduxFieldInput.default,
              name: "title",
              placeholder: t('Tag title'),
              type: "text"
            })]
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "m-0 mt-3",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
            className: "mb-0",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
              children: t('Description')
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
              component: _ReduxFieldInput.default,
              name: "description",
              type: "text"
            })]
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.SettingsBox, {
        className: "mt-3",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.Caption, {
              className: "mb-0",
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(CalculatorIcon, {
                width: "14",
                height: "14",
                className: "mr-2"
              }), t('Calculator')]
            })
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          className: "mb-2",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
              children: t('Tag', {
                count: 2
              })
            })
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reduxForm.FieldArray, {
          changeInput: changeInput,
          component: _ReduxFieldArrayItems.default,
          currencies: currencies,
          decimalFormatsData: decimalFormatsData,
          editionLevel: editionLevel,
          formValues: formValues,
          isInitialazing: isInitialazing,
          name: "calculator",
          type: "array",
          unitFormatsData: unitFormatsData
        }), contentViewState === _tags.CREATE_CUSTOM_TAG_STATE && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          className: "mt-2",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonNew, {
              onClick: function onClick() {
                return setIsOpenTagLibraryModal(true);
              },
              type: "button",
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
                className: "fa fa-plus mr-1",
                "aria-hidden": "true"
              }), t('New tag')]
            })
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {
          className: "mt-3"
        }), contentViewState === _tags.CREATE_CUSTOM_TAG_STATE && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          className: "mt-3",
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
              children: t('Operations')
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Operations.default, {
              handleClickOperator: function handleClickOperator(operator) {
                return (0, _helpers.handleClickOperator)(changeInput, data, operator);
              },
              handleInsertNumber: function handleInsertNumber() {
                return (0, _helpers.handleInsertNumber)(changeInput, data, number, setNumber);
              },
              number: number,
              setNumber: setNumber
            })]
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
          className: "mt-3 align-items-center",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.Caption, {
              className: "mb-0",
              children: t('Formula')
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            className: "text-right",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_FormulaControlButtons.default, {
              calculator: data,
              changeInput: changeInput,
              handleClickEraser: function handleClickEraser() {
                return (0, _helpers.handleClickEraser)(changeInput, data);
              },
              handleClickReset: function handleClickReset() {
                return changeInput('calculator', []);
              },
              contentViewState: contentViewState
            })
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Formula.default, {
          calculator: data
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.SettingsBox, {
        className: "mt-3",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
          className: "align-items-center",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.Caption, {
              className: "mb-0",
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(SettingsIcon, {
                width: "17",
                height: "17",
                className: "mr-2"
              }), t('Setting', {
                count: 2
              })]
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_ResetDefaultValue.default, {
            defaultValue: formValues.default_value,
            showButtonReset: editionLevel === _types.PARTIAL_EDITION_MODE,
            resetToDefaultValue: function resetToDefaultValue() {
              return changeInput('value', formValues.default_value);
            },
            value: formValues.value
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {}), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
          className: "mt-3",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            xs: "3",
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
              className: "mb-0",
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                children: t('Value')
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                component: _ReduxFieldInput.default,
                name: "value",
                onlyNumbers: true,
                type: "string"
              })]
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            xs: "5",
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                children: t('Unit')
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                component: _ReduxFieldInput.default,
                disabled: editionLevel === _types.PARTIAL_EDITION_MODE,
                name: "unit",
                type: "text"
              })]
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            xs: "5",
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                children: t('Decimals')
              }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                component: _ReduxFieldSelect.default,
                disabled: editionLevel === _types.PARTIAL_EDITION_MODE,
                name: "decimal_type",
                normalize: (0, _helpers.normalizeDecimalField)(changeInput, decimalFormatsData, 'decimal_places'),
                options: (0, _helpers.getDecimalFormatsToSelect)(decimalFormatsData, isInitialazing)
              })]
            })
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          className: "mt-3",
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.Caption, {
              className: "mt-3",
              children: t('Default value')
            }), formValues.value && /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.TagLabel, {
              background: formValues.color,
              children: (0, _helpers.getFormattedValue)(true, tagsLocale, formValues)
            })]
          })
        })]
      }), !(0, _isEmpty.default)(errors) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-3",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledAlert, {
            variant: "warning",
            children: t('You need to capture all fields')
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-3 justify-content-center",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          xs: "10",
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonInsert, {
            size: "small",
            type: "submit",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
              className: "fas fa-arrow-right mr-2"
            }), (0, _helpers.getTextButton)(contentViewState)]
          })
        })
      })]
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  currencies: selectors.getCurrenciesData,
  decimalFormatsData: selectors.getDecimalFormatsData,
  errors: selectors.getCustomTagFormErrors,
  formValues: selectors.getCustomTagValues,
  initialValues: selectors.getInitialCustomTagValues,
  unitFormatsData: selectors.getUnitFormatsData
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    changeInput: function changeInput(field, value) {
      return dispatch((0, _reduxForm.change)('custom-tag-form', field, value));
    }
  };
};

TagFormWithCalculator.propTypes = {
  changeInput: _propTypes.default.func,
  tagsLocale: _propTypes.default.string,
  contentViewState: _propTypes.default.number,
  currencies: _propTypes.default.array,
  decimalFormatsData: _propTypes.default.array,
  editionLevel: _propTypes.default.string,
  errors: _propTypes.default.object,
  formValues: _propTypes.default.object,
  handleOnSubmit: _propTypes.default.func,
  handleSubmit: _propTypes.default.func,
  isInitialazing: _propTypes.default.bool,
  setIsOpenTagLibraryModal: _propTypes.default.func,
  unitFormatsData: _propTypes.default.array
};
var FormContainer = (0, _reduxForm.reduxForm)({
  enableReinitialize: true,
  form: 'custom-tag-form',
  validate: _customTagValidate.default
})(TagFormWithCalculator);

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FormContainer);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vVGFnRm9ybVdpdGhDYWxjdWxhdG9yLmpzIl0sIm5hbWVzIjpbIkNhbGN1bGF0b3JJY29uIiwiU2V0dGluZ3NJY29uIiwiVGFnRm9ybVdpdGhDYWxjdWxhdG9yIiwiY2hhbmdlSW5wdXQiLCJjb250ZW50Vmlld1N0YXRlIiwiY3VycmVuY2llcyIsImRlY2ltYWxGb3JtYXRzRGF0YSIsImVkaXRpb25MZXZlbCIsInRhZ3NMb2NhbGUiLCJlcnJvcnMiLCJmb3JtVmFsdWVzIiwiaGFuZGxlT25TdWJtaXQiLCJoYW5kbGVTdWJtaXQiLCJpc0luaXRpYWxhemluZyIsInNldElzT3BlblRhZ0xpYnJhcnlNb2RhbCIsInVuaXRGb3JtYXRzRGF0YSIsInQiLCJudW1iZXIiLCJzZXROdW1iZXIiLCJkYXRhIiwidGl0bGUiLCJSZWR1eEZpZWxkSW5wdXQiLCJjb3VudCIsIlJlZHV4RmllbGRBcnJheUl0ZW1zIiwiQ1JFQVRFX0NVU1RPTV9UQUdfU1RBVEUiLCJvcGVyYXRvciIsImRlZmF1bHRfdmFsdWUiLCJQQVJUSUFMX0VESVRJT05fTU9ERSIsInZhbHVlIiwiUmVkdXhGaWVsZFNlbGVjdCIsImNvbG9yIiwibWFwU3RhdGVUb1Byb3BzIiwic2VsZWN0b3JzIiwiZ2V0Q3VycmVuY2llc0RhdGEiLCJnZXREZWNpbWFsRm9ybWF0c0RhdGEiLCJnZXRDdXN0b21UYWdGb3JtRXJyb3JzIiwiZ2V0Q3VzdG9tVGFnVmFsdWVzIiwiaW5pdGlhbFZhbHVlcyIsImdldEluaXRpYWxDdXN0b21UYWdWYWx1ZXMiLCJnZXRVbml0Rm9ybWF0c0RhdGEiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImZpZWxkIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsInN0cmluZyIsImFycmF5Iiwib2JqZWN0IiwiYm9vbCIsIkZvcm1Db250YWluZXIiLCJlbmFibGVSZWluaXRpYWxpemUiLCJmb3JtIiwidmFsaWRhdGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUdBOztBQUhBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUNBOztBQUtBOztBQUNBOztBQVVBOztBQUNBOztBQVlBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBL0IyQkEsYyxZQUFBQSxjOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxjOzs7Ozs7SUFDQUMsWSxZQUFBQSxZOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLFk7Ozs7OztBQWdDM0IsSUFBTUMscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixPQWN4QjtBQUFBLE1BYkZDLFdBYUUsUUFiRkEsV0FhRTtBQUFBLE1BWkZDLGdCQVlFLFFBWkZBLGdCQVlFO0FBQUEsTUFYRkMsVUFXRSxRQVhGQSxVQVdFO0FBQUEsTUFWRkMsa0JBVUUsUUFWRkEsa0JBVUU7QUFBQSxNQVRGQyxZQVNFLFFBVEZBLFlBU0U7QUFBQSxNQVJGQyxVQVFFLFFBUkZBLFVBUUU7QUFBQSxNQVBGQyxNQU9FLFFBUEZBLE1BT0U7QUFBQSxNQU5GQyxVQU1FLFFBTkZBLFVBTUU7QUFBQSxNQUxGQyxjQUtFLFFBTEZBLGNBS0U7QUFBQSxNQUpGQyxZQUlFLFFBSkZBLFlBSUU7QUFBQSxNQUhGQyxjQUdFLFFBSEZBLGNBR0U7QUFBQSxNQUZGQyx3QkFFRSxRQUZGQSx3QkFFRTtBQUFBLE1BREZDLGVBQ0UsUUFERkEsZUFDRTs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0Esa0JBQTRCLHFCQUFTLENBQVQsQ0FBNUI7QUFBQTtBQUFBLE1BQU9DLE1BQVA7QUFBQSxNQUFlQyxTQUFmOztBQUVBLE1BQU1DLElBQUksR0FBRyxrQkFBSVQsVUFBSixFQUFnQixZQUFoQixFQUE4QixFQUE5QixDQUFiO0FBRUEsd0JBQVUsWUFBTTtBQUNaLDRCQUFVUCxXQUFWLEVBQXVCZ0IsSUFBdkI7QUFDSCxHQUZELEVBRUcsQ0FBQ0EsSUFBRCxDQUZIO0FBSUEsc0JBQ0k7QUFBSyxJQUFBLFNBQVMsRUFBQyxNQUFmO0FBQUEsMkJBQ0ksc0JBQUMsb0JBQUQ7QUFBTSxNQUFBLFFBQVEsRUFBRVAsWUFBWSxDQUFDRCxjQUFELENBQTVCO0FBQUEsOEJBQ0kscUJBQUMsbUJBQUQ7QUFBSyxRQUFBLFNBQVMsRUFBQyw2QkFBZjtBQUFBLCtCQUNJLHFCQUFDLG1CQUFEO0FBQUEsaUNBQ0ksc0JBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQVksWUFBQSxTQUFTLEVBQUMsTUFBdEI7QUFBQSx1QkFDSyxDQUFDLHNCQUFRRCxVQUFVLENBQUNVLEtBQW5CLENBQUQsaUJBQ0cscUJBQUMsMkJBQUQ7QUFBQSx3QkFBWUosQ0FBQyxDQUFDLFdBQUQ7QUFBYixjQUZSLGVBSUkscUJBQUMsa0NBQUQ7QUFDSSxjQUFBLFNBQVMsRUFBRUssd0JBRGY7QUFFSSxjQUFBLElBQUksRUFBQyxPQUZUO0FBR0ksY0FBQSxXQUFXLEVBQUVMLENBQUMsQ0FBQyxXQUFELENBSGxCO0FBSUksY0FBQSxJQUFJLEVBQUM7QUFKVCxjQUpKO0FBQUE7QUFESjtBQURKLFFBREosZUFpQkkscUJBQUMsbUJBQUQ7QUFBSyxRQUFBLFNBQVMsRUFBQyxVQUFmO0FBQUEsK0JBQ0kscUJBQUMsbUJBQUQ7QUFBQSxpQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBWSxZQUFBLFNBQVMsRUFBQyxNQUF0QjtBQUFBLG9DQUNJLHFCQUFDLDJCQUFEO0FBQUEsd0JBQVlBLENBQUMsQ0FBQyxhQUFEO0FBQWIsY0FESixlQUVJLHFCQUFDLDZCQUFEO0FBQ0ksY0FBQSxTQUFTLEVBQUVLLHdCQURmO0FBRUksY0FBQSxJQUFJLEVBQUMsYUFGVDtBQUdJLGNBQUEsSUFBSSxFQUFDO0FBSFQsY0FGSjtBQUFBO0FBREo7QUFESixRQWpCSixlQThCSSxzQkFBQyw2QkFBRDtBQUFhLFFBQUEsU0FBUyxFQUFDLE1BQXZCO0FBQUEsZ0NBQ0kscUJBQUMsbUJBQUQ7QUFBQSxpQ0FDSSxxQkFBQyxtQkFBRDtBQUFBLG1DQUNJLHNCQUFDLHlCQUFEO0FBQVMsY0FBQSxTQUFTLEVBQUMsTUFBbkI7QUFBQSxzQ0FDSSxxQkFBQyxjQUFEO0FBQ0ksZ0JBQUEsS0FBSyxFQUFDLElBRFY7QUFFSSxnQkFBQSxNQUFNLEVBQUMsSUFGWDtBQUdJLGdCQUFBLFNBQVMsRUFBQztBQUhkLGdCQURKLEVBT0tMLENBQUMsQ0FBQyxZQUFELENBUE47QUFBQTtBQURKO0FBREosVUFESixlQWNJLDhCQWRKLGVBZ0JJLHFCQUFDLG1CQUFEO0FBQUssVUFBQSxTQUFTLEVBQUMsTUFBZjtBQUFBLGlDQUNJLHFCQUFDLG1CQUFEO0FBQUEsbUNBQ0kscUJBQUMsMkJBQUQ7QUFBQSx3QkFBWUEsQ0FBQyxDQUFDLEtBQUQsRUFBUTtBQUFFTSxnQkFBQUEsS0FBSyxFQUFFO0FBQVQsZUFBUjtBQUFiO0FBREo7QUFESixVQWhCSixlQXNCSSxxQkFBQyxxQkFBRDtBQUNJLFVBQUEsV0FBVyxFQUFFbkIsV0FEakI7QUFFSSxVQUFBLFNBQVMsRUFBRW9CLDZCQUZmO0FBR0ksVUFBQSxVQUFVLEVBQUVsQixVQUhoQjtBQUlJLFVBQUEsa0JBQWtCLEVBQUVDLGtCQUp4QjtBQUtJLFVBQUEsWUFBWSxFQUFFQyxZQUxsQjtBQU1JLFVBQUEsVUFBVSxFQUFFRyxVQU5oQjtBQU9JLFVBQUEsY0FBYyxFQUFFRyxjQVBwQjtBQVFJLFVBQUEsSUFBSSxFQUFDLFlBUlQ7QUFTSSxVQUFBLElBQUksRUFBQyxPQVRUO0FBVUksVUFBQSxlQUFlLEVBQUVFO0FBVnJCLFVBdEJKLEVBbUNLWCxnQkFBZ0IsS0FBS29CLDZCQUFyQixpQkFDRyxxQkFBQyxtQkFBRDtBQUFLLFVBQUEsU0FBUyxFQUFDLE1BQWY7QUFBQSxpQ0FDSSxxQkFBQyxtQkFBRDtBQUFBLG1DQUNJLHNCQUFDLDJCQUFEO0FBQ0ksY0FBQSxPQUFPLEVBQUU7QUFBQSx1QkFDTFYsd0JBQXdCLENBQUMsSUFBRCxDQURuQjtBQUFBLGVBRGI7QUFJSSxjQUFBLElBQUksRUFBQyxRQUpUO0FBQUEsc0NBTUk7QUFDSSxnQkFBQSxTQUFTLEVBQUMsaUJBRGQ7QUFFSSwrQkFBWTtBQUZoQixnQkFOSixFQVVLRSxDQUFDLENBQUMsU0FBRCxDQVZOO0FBQUE7QUFESjtBQURKLFVBcENSLGVBc0RJO0FBQUksVUFBQSxTQUFTLEVBQUM7QUFBZCxVQXRESixFQXdES1osZ0JBQWdCLEtBQUtvQiw2QkFBckIsaUJBQ0cscUJBQUMsbUJBQUQ7QUFBSyxVQUFBLFNBQVMsRUFBQyxNQUFmO0FBQUEsaUNBQ0ksc0JBQUMsbUJBQUQ7QUFBQSxvQ0FDSSxxQkFBQywyQkFBRDtBQUFBLHdCQUFZUixDQUFDLENBQUMsWUFBRDtBQUFiLGNBREosZUFFSSxxQkFBQyxtQkFBRDtBQUNJLGNBQUEsbUJBQW1CLEVBQUUsNkJBQUNTLFFBQUQ7QUFBQSx1QkFDakIsa0NBQ0l0QixXQURKLEVBRUlnQixJQUZKLEVBR0lNLFFBSEosQ0FEaUI7QUFBQSxlQUR6QjtBQVFJLGNBQUEsa0JBQWtCLEVBQUU7QUFBQSx1QkFDaEIsaUNBQ0l0QixXQURKLEVBRUlnQixJQUZKLEVBR0lGLE1BSEosRUFJSUMsU0FKSixDQURnQjtBQUFBLGVBUnhCO0FBZ0JJLGNBQUEsTUFBTSxFQUFFRCxNQWhCWjtBQWlCSSxjQUFBLFNBQVMsRUFBRUM7QUFqQmYsY0FGSjtBQUFBO0FBREosVUF6RFIsZUFtRkksc0JBQUMsbUJBQUQ7QUFBSyxVQUFBLFNBQVMsRUFBQyx5QkFBZjtBQUFBLGtDQUNJLHFCQUFDLG1CQUFEO0FBQUEsbUNBQ0kscUJBQUMseUJBQUQ7QUFBUyxjQUFBLFNBQVMsRUFBQyxNQUFuQjtBQUFBLHdCQUEyQkYsQ0FBQyxDQUFDLFNBQUQ7QUFBNUI7QUFESixZQURKLGVBSUkscUJBQUMsbUJBQUQ7QUFBSyxZQUFBLFNBQVMsRUFBQyxZQUFmO0FBQUEsbUNBQ0kscUJBQUMsOEJBQUQ7QUFDSSxjQUFBLFVBQVUsRUFBRUcsSUFEaEI7QUFFSSxjQUFBLFdBQVcsRUFBRWhCLFdBRmpCO0FBR0ksY0FBQSxpQkFBaUIsRUFBRTtBQUFBLHVCQUNmLGdDQUFrQkEsV0FBbEIsRUFBK0JnQixJQUEvQixDQURlO0FBQUEsZUFIdkI7QUFNSSxjQUFBLGdCQUFnQixFQUFFO0FBQUEsdUJBQ2RoQixXQUFXLENBQUMsWUFBRCxFQUFlLEVBQWYsQ0FERztBQUFBLGVBTnRCO0FBU0ksY0FBQSxnQkFBZ0IsRUFBRUM7QUFUdEI7QUFESixZQUpKO0FBQUEsVUFuRkosZUFxR0kscUJBQUMsZ0JBQUQ7QUFBUyxVQUFBLFVBQVUsRUFBRWU7QUFBckIsVUFyR0o7QUFBQSxRQTlCSixlQXNJSSxzQkFBQyw2QkFBRDtBQUFhLFFBQUEsU0FBUyxFQUFDLE1BQXZCO0FBQUEsZ0NBQ0ksc0JBQUMsbUJBQUQ7QUFBSyxVQUFBLFNBQVMsRUFBQyxvQkFBZjtBQUFBLGtDQUNJLHFCQUFDLG1CQUFEO0FBQUEsbUNBQ0ksc0JBQUMseUJBQUQ7QUFBUyxjQUFBLFNBQVMsRUFBQyxNQUFuQjtBQUFBLHNDQUNJLHFCQUFDLFlBQUQ7QUFDSSxnQkFBQSxLQUFLLEVBQUMsSUFEVjtBQUVJLGdCQUFBLE1BQU0sRUFBQyxJQUZYO0FBR0ksZ0JBQUEsU0FBUyxFQUFDO0FBSGQsZ0JBREosRUFPS0gsQ0FBQyxDQUFDLFNBQUQsRUFBWTtBQUFFTSxnQkFBQUEsS0FBSyxFQUFFO0FBQVQsZUFBWixDQVBOO0FBQUE7QUFESixZQURKLGVBWUkscUJBQUMsMEJBQUQ7QUFDSSxZQUFBLFlBQVksRUFBRVosVUFBVSxDQUFDZ0IsYUFEN0I7QUFFSSxZQUFBLGVBQWUsRUFDWG5CLFlBQVksS0FBS29CLDJCQUh6QjtBQUtJLFlBQUEsbUJBQW1CLEVBQUU7QUFBQSxxQkFDakJ4QixXQUFXLENBQUMsT0FBRCxFQUFVTyxVQUFVLENBQUNnQixhQUFyQixDQURNO0FBQUEsYUFMekI7QUFRSSxZQUFBLEtBQUssRUFBRWhCLFVBQVUsQ0FBQ2tCO0FBUnRCLFlBWko7QUFBQSxVQURKLGVBd0JJLDhCQXhCSixlQTBCSSxzQkFBQyxtQkFBRDtBQUFLLFVBQUEsU0FBUyxFQUFDLE1BQWY7QUFBQSxrQ0FDSSxxQkFBQyxtQkFBRDtBQUFLLFlBQUEsRUFBRSxFQUFDLEdBQVI7QUFBQSxtQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBWSxjQUFBLFNBQVMsRUFBQyxNQUF0QjtBQUFBLHNDQUNJLHFCQUFDLDJCQUFEO0FBQUEsMEJBQVlaLENBQUMsQ0FBQyxPQUFEO0FBQWIsZ0JBREosZUFFSSxxQkFBQyw2QkFBRDtBQUNJLGdCQUFBLFNBQVMsRUFBRUssd0JBRGY7QUFFSSxnQkFBQSxJQUFJLEVBQUMsT0FGVDtBQUdJLGdCQUFBLFdBQVcsTUFIZjtBQUlJLGdCQUFBLElBQUksRUFBQztBQUpULGdCQUZKO0FBQUE7QUFESixZQURKLGVBYUkscUJBQUMsbUJBQUQ7QUFBSyxZQUFBLEVBQUUsRUFBQyxHQUFSO0FBQUEsbUNBQ0ksc0JBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQUEsc0NBQ0kscUJBQUMsMkJBQUQ7QUFBQSwwQkFBWUwsQ0FBQyxDQUFDLE1BQUQ7QUFBYixnQkFESixlQUVJLHFCQUFDLDZCQUFEO0FBQ0ksZ0JBQUEsU0FBUyxFQUFFSyx3QkFEZjtBQUVJLGdCQUFBLFFBQVEsRUFDSmQsWUFBWSxLQUFLb0IsMkJBSHpCO0FBS0ksZ0JBQUEsSUFBSSxFQUFDLE1BTFQ7QUFNSSxnQkFBQSxJQUFJLEVBQUM7QUFOVCxnQkFGSjtBQUFBO0FBREosWUFiSixlQTJCSSxxQkFBQyxtQkFBRDtBQUFLLFlBQUEsRUFBRSxFQUFDLEdBQVI7QUFBQSxtQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBQSxzQ0FDSSxxQkFBQywyQkFBRDtBQUFBLDBCQUFZWCxDQUFDLENBQUMsVUFBRDtBQUFiLGdCQURKLGVBRUkscUJBQUMsNkJBQUQ7QUFDSSxnQkFBQSxTQUFTLEVBQUVhLHlCQURmO0FBRUksZ0JBQUEsUUFBUSxFQUNKdEIsWUFBWSxLQUFLb0IsMkJBSHpCO0FBS0ksZ0JBQUEsSUFBSSxFQUFDLGNBTFQ7QUFNSSxnQkFBQSxTQUFTLEVBQUUsb0NBQ1B4QixXQURPLEVBRVBHLGtCQUZPLEVBR1AsZ0JBSE8sQ0FOZjtBQVdJLGdCQUFBLE9BQU8sRUFBRSx3Q0FDTEEsa0JBREssRUFFTE8sY0FGSztBQVhiLGdCQUZKO0FBQUE7QUFESixZQTNCSjtBQUFBLFVBMUJKLGVBNEVJLHFCQUFDLG1CQUFEO0FBQUssVUFBQSxTQUFTLEVBQUMsTUFBZjtBQUFBLGlDQUNJLHNCQUFDLG1CQUFEO0FBQUEsb0NBQ0kscUJBQUMseUJBQUQ7QUFBUyxjQUFBLFNBQVMsRUFBQyxNQUFuQjtBQUFBLHdCQUNLRyxDQUFDLENBQUMsZUFBRDtBQUROLGNBREosRUFJS04sVUFBVSxDQUFDa0IsS0FBWCxpQkFDRyxxQkFBQywwQkFBRDtBQUFVLGNBQUEsVUFBVSxFQUFFbEIsVUFBVSxDQUFDb0IsS0FBakM7QUFBQSx3QkFDSyxnQ0FDRyxJQURILEVBRUd0QixVQUZILEVBR0dFLFVBSEg7QUFETCxjQUxSO0FBQUE7QUFESixVQTVFSjtBQUFBLFFBdElKLEVBb09LLENBQUMsc0JBQVFELE1BQVIsQ0FBRCxpQkFDRyxxQkFBQyxtQkFBRDtBQUFLLFFBQUEsU0FBUyxFQUFDLE1BQWY7QUFBQSwrQkFDSSxxQkFBQyxtQkFBRDtBQUFBLGlDQUNJLHFCQUFDLDZCQUFEO0FBQWEsWUFBQSxPQUFPLEVBQUMsU0FBckI7QUFBQSxzQkFDS08sQ0FBQyxDQUFDLGdDQUFEO0FBRE47QUFESjtBQURKLFFBck9SLGVBOE9JLHFCQUFDLG1CQUFEO0FBQUssUUFBQSxTQUFTLEVBQUMsNkJBQWY7QUFBQSwrQkFDSSxxQkFBQyxtQkFBRDtBQUFLLFVBQUEsRUFBRSxFQUFDLElBQVI7QUFBQSxpQ0FDSSxzQkFBQyw4QkFBRDtBQUFjLFlBQUEsSUFBSSxFQUFDLE9BQW5CO0FBQTJCLFlBQUEsSUFBSSxFQUFDLFFBQWhDO0FBQUEsb0NBQ0k7QUFBRyxjQUFBLFNBQVMsRUFBQztBQUFiLGNBREosRUFFSyw0QkFBY1osZ0JBQWQsQ0FGTDtBQUFBO0FBREo7QUFESixRQTlPSjtBQUFBO0FBREosSUFESjtBQTJQSCxDQW5SRDs7QUFxUkEsSUFBTTJCLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0MxQixFQUFBQSxVQUFVLEVBQUUyQixTQUFTLENBQUNDLGlCQUR1QjtBQUU3QzNCLEVBQUFBLGtCQUFrQixFQUFFMEIsU0FBUyxDQUFDRSxxQkFGZTtBQUc3Q3pCLEVBQUFBLE1BQU0sRUFBRXVCLFNBQVMsQ0FBQ0csc0JBSDJCO0FBSTdDekIsRUFBQUEsVUFBVSxFQUFFc0IsU0FBUyxDQUFDSSxrQkFKdUI7QUFLN0NDLEVBQUFBLGFBQWEsRUFBRUwsU0FBUyxDQUFDTSx5QkFMb0I7QUFNN0N2QixFQUFBQSxlQUFlLEVBQUVpQixTQUFTLENBQUNPO0FBTmtCLENBQXpCLENBQXhCOztBQVNBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQWU7QUFDdEN0QyxJQUFBQSxXQUFXLEVBQUUscUJBQUN1QyxLQUFELEVBQVFkLEtBQVI7QUFBQSxhQUNUYSxRQUFRLENBQUMsdUJBQU8saUJBQVAsRUFBMEJDLEtBQTFCLEVBQWlDZCxLQUFqQyxDQUFELENBREM7QUFBQTtBQUR5QixHQUFmO0FBQUEsQ0FBM0I7O0FBS0ExQixxQkFBcUIsQ0FBQ3lDLFNBQXRCLEdBQWtDO0FBQzlCeEMsRUFBQUEsV0FBVyxFQUFFeUMsbUJBQVVDLElBRE87QUFFOUJyQyxFQUFBQSxVQUFVLEVBQUVvQyxtQkFBVUUsTUFGUTtBQUc5QjFDLEVBQUFBLGdCQUFnQixFQUFFd0MsbUJBQVUzQixNQUhFO0FBSTlCWixFQUFBQSxVQUFVLEVBQUV1QyxtQkFBVUcsS0FKUTtBQUs5QnpDLEVBQUFBLGtCQUFrQixFQUFFc0MsbUJBQVVHLEtBTEE7QUFNOUJ4QyxFQUFBQSxZQUFZLEVBQUVxQyxtQkFBVUUsTUFOTTtBQU85QnJDLEVBQUFBLE1BQU0sRUFBRW1DLG1CQUFVSSxNQVBZO0FBUTlCdEMsRUFBQUEsVUFBVSxFQUFFa0MsbUJBQVVJLE1BUlE7QUFTOUJyQyxFQUFBQSxjQUFjLEVBQUVpQyxtQkFBVUMsSUFUSTtBQVU5QmpDLEVBQUFBLFlBQVksRUFBRWdDLG1CQUFVQyxJQVZNO0FBVzlCaEMsRUFBQUEsY0FBYyxFQUFFK0IsbUJBQVVLLElBWEk7QUFZOUJuQyxFQUFBQSx3QkFBd0IsRUFBRThCLG1CQUFVQyxJQVpOO0FBYTlCOUIsRUFBQUEsZUFBZSxFQUFFNkIsbUJBQVVHO0FBYkcsQ0FBbEM7QUFnQkEsSUFBTUcsYUFBYSxHQUFHLDBCQUFVO0FBQzVCQyxFQUFBQSxrQkFBa0IsRUFBRSxJQURRO0FBRTVCQyxFQUFBQSxJQUFJLEVBQUUsaUJBRnNCO0FBRzVCQyxFQUFBQSxRQUFRLEVBQVJBO0FBSDRCLENBQVYsRUFJbkJuRCxxQkFKbUIsQ0FBdEI7O2VBTWUseUJBQVE2QixlQUFSLEVBQXlCUyxrQkFBekIsRUFBNkNVLGFBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xuaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDb2wsIEZvcm0sIFJvdyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGNoYW5nZSwgRmllbGRBcnJheSwgcmVkdXhGb3JtIH0gZnJvbSAncmVkdXgtZm9ybSc7XG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XG5cbmltcG9ydCBSZWR1eEZpZWxkSW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvUmVkdXhGaWVsZElucHV0JztcbmltcG9ydCBSZWR1eEZpZWxkU2VsZWN0IGZyb20gJ0Bjb21wb25lbnRzL1JlZHV4RmllbGRTZWxlY3QnO1xuXG5pbXBvcnQgeyBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSB9IGZyb20gJ0Bjb25zdGFudHMvdGFncyc7XG5pbXBvcnQgeyBQQVJUSUFMX0VESVRJT05fTU9ERSB9IGZyb20gJ0Bjb25zdGFudHMvdHlwZXMnO1xuXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBDYWxjdWxhdG9ySWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvY2FsY3VsYXRvci5zdmcnO1xuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgU2V0dGluZ3NJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9zZXR0aW5ncy5zdmcnO1xuXG5pbXBvcnQgdmFsaWRhdGUgZnJvbSAnLi4vLi4vY3VzdG9tVGFnVmFsaWRhdGUnO1xuaW1wb3J0IHtcbiAgICBnZXRGb3JtYXR0ZWRWYWx1ZSxcbiAgICBnZXREZWNpbWFsRm9ybWF0c1RvU2VsZWN0LFxuICAgIGdldFRleHRCdXR0b24sXG4gICAgaGFuZGxlQ2xpY2tFcmFzZXIsXG4gICAgaGFuZGxlQ2xpY2tPcGVyYXRvcixcbiAgICBoYW5kbGVJbnNlcnROdW1iZXIsXG4gICAgbm9ybWFsaXplRGVjaW1hbEZpZWxkLFxuICAgIHVwZGF0ZVRhZyxcbn0gZnJvbSAnLi4vLi4vaGVscGVycyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vLi4vc2VsZWN0b3JzJztcbmltcG9ydCB7XG4gICAgQnV0dG9uSW5zZXJ0LFxuICAgIEJ1dHRvbk5ldyxcbiAgICBDYXB0aW9uLFxuICAgIEZvcm1MYWJlbCxcbiAgICBTZXR0aW5nc0JveCxcbiAgICBTdHlsZWRBbGVydCxcbiAgICBTdHlsZWRGaWVsZCxcbiAgICBTdHlsZWRUaXRsZUZpZWxkLFxuICAgIFRhZ0xhYmVsLFxufSBmcm9tICcuLi9zdHlsZWRDb21wb25lbnRzJztcblxuaW1wb3J0IEZvcm11bGEgZnJvbSAnLi9Gb3JtdWxhJztcbmltcG9ydCBGb3JtdWxhQ29udHJvbEJ1dHRvbnMgZnJvbSAnLi9Gb3JtdWxhQ29udHJvbEJ1dHRvbnMnO1xuaW1wb3J0IE9wZXJhdGlvbnMgZnJvbSAnLi9PcGVyYXRpb25zJztcbmltcG9ydCBSZWR1eEZpZWxkQXJyYXlJdGVtcyBmcm9tICcuL1JlZHV4RmllbGRBcnJheUl0ZW1zJztcbmltcG9ydCBSZXNldERlZmF1bHRWYWx1ZSBmcm9tICcuL1Jlc2V0RGVmYXVsdFZhbHVlJztcblxuY29uc3QgVGFnRm9ybVdpdGhDYWxjdWxhdG9yID0gKHtcbiAgICBjaGFuZ2VJbnB1dCxcbiAgICBjb250ZW50Vmlld1N0YXRlLFxuICAgIGN1cnJlbmNpZXMsXG4gICAgZGVjaW1hbEZvcm1hdHNEYXRhLFxuICAgIGVkaXRpb25MZXZlbCxcbiAgICB0YWdzTG9jYWxlLFxuICAgIGVycm9ycyxcbiAgICBmb3JtVmFsdWVzLFxuICAgIGhhbmRsZU9uU3VibWl0LFxuICAgIGhhbmRsZVN1Ym1pdCxcbiAgICBpc0luaXRpYWxhemluZyxcbiAgICBzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWwsXG4gICAgdW5pdEZvcm1hdHNEYXRhLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBbbnVtYmVyLCBzZXROdW1iZXJdID0gdXNlU3RhdGUoMSk7XG5cbiAgICBjb25zdCBkYXRhID0gZ2V0KGZvcm1WYWx1ZXMsICdjYWxjdWxhdG9yJywgW10pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgdXBkYXRlVGFnKGNoYW5nZUlucHV0LCBkYXRhKTtcbiAgICB9LCBbZGF0YV0pO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtdC0zXCI+XG4gICAgICAgICAgICA8Rm9ybSBvblN1Ym1pdD17aGFuZGxlU3VibWl0KGhhbmRsZU9uU3VibWl0KX0+XG4gICAgICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJtLTAgbXQtMyBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IWlzRW1wdHkoZm9ybVZhbHVlcy50aXRsZSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdUYWcgdGl0bGUnKX08L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRUaXRsZUZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudD17UmVkdXhGaWVsZElucHV0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwidGl0bGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17dCgnVGFnIHRpdGxlJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICA8L1Jvdz5cblxuICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibS0wIG10LTNcIj5cbiAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdEZXNjcmlwdGlvbicpfTwvRm9ybUxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cImRlc2NyaXB0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgIDwvUm93PlxuXG4gICAgICAgICAgICAgICAgPFNldHRpbmdzQm94IGNsYXNzTmFtZT1cIm10LTNcIj5cbiAgICAgICAgICAgICAgICAgICAgPFJvdz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2w+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENhcHRpb24gY2xhc3NOYW1lPVwibWItMFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2FsY3VsYXRvckljb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PVwiMTRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXItMlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3QoJ0NhbGN1bGF0b3InKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NhcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICAgICAgICAgIDxociAvPlxuXG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibWItMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdUYWcnLCB7IGNvdW50OiAyIH0pfTwvRm9ybUxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgIDwvUm93PlxuXG4gICAgICAgICAgICAgICAgICAgIDxGaWVsZEFycmF5XG4gICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2VJbnB1dD17Y2hhbmdlSW5wdXR9XG4gICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRBcnJheUl0ZW1zfVxuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY2llcz17Y3VycmVuY2llc31cbiAgICAgICAgICAgICAgICAgICAgICAgIGRlY2ltYWxGb3JtYXRzRGF0YT17ZGVjaW1hbEZvcm1hdHNEYXRhfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtVmFsdWVzPXtmb3JtVmFsdWVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgaXNJbml0aWFsYXppbmc9e2lzSW5pdGlhbGF6aW5nfVxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cImNhbGN1bGF0b3JcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImFycmF5XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVuaXRGb3JtYXRzRGF0YT17dW5pdEZvcm1hdHNEYXRhfVxuICAgICAgICAgICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICAgICAgICAgIHtjb250ZW50Vmlld1N0YXRlID09PSBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm10LTJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uTmV3XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldElzT3BlblRhZ0xpYnJhcnlNb2RhbCh0cnVlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZmEgZmEtcGx1cyBtci0xXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcmlhLWhpZGRlbj1cInRydWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0KCdOZXcgdGFnJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uTmV3PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cIm10LTNcIiAvPlxuXG4gICAgICAgICAgICAgICAgICAgIHtjb250ZW50Vmlld1N0YXRlID09PSBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm10LTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdPcGVyYXRpb25zJyl9PC9Gb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxPcGVyYXRpb25zXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbGlja09wZXJhdG9yPXsob3BlcmF0b3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2xpY2tPcGVyYXRvcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wZXJhdG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlSW5zZXJ0TnVtYmVyPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUluc2VydE51bWJlcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0TnVtYmVyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVtYmVyPXtudW1iZXJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXROdW1iZXI9e3NldE51bWJlcn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgICAgICApfVxuXG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtMyBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2w+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENhcHRpb24gY2xhc3NOYW1lPVwibWItMFwiPnt0KCdGb3JtdWxhJyl9PC9DYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sIGNsYXNzTmFtZT1cInRleHQtcmlnaHRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybXVsYUNvbnRyb2xCdXR0b25zXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGN1bGF0b3I9e2RhdGF9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZUlucHV0PXtjaGFuZ2VJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2xpY2tFcmFzZXI9eygpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbGlja0VyYXNlcihjaGFuZ2VJbnB1dCwgZGF0YSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbGlja1Jlc2V0PXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQoJ2NhbGN1bGF0b3InLCBbXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50Vmlld1N0YXRlPXtjb250ZW50Vmlld1N0YXRlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtdWxhIGNhbGN1bGF0b3I9e2RhdGF9IC8+XG4gICAgICAgICAgICAgICAgPC9TZXR0aW5nc0JveD5cblxuICAgICAgICAgICAgICAgIDxTZXR0aW5nc0JveCBjbGFzc05hbWU9XCJtdC0zXCI+XG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwiYWxpZ24taXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDYXB0aW9uIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzSWNvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxN1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxN1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtci0yXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU2V0dGluZycsIHsgY291bnQ6IDIgfSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9DYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8UmVzZXREZWZhdWx0VmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU9e2Zvcm1WYWx1ZXMuZGVmYXVsdF92YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93QnV0dG9uUmVzZXQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWwgPT09IFBBUlRJQUxfRURJVElPTl9NT0RFXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc2V0VG9EZWZhdWx0VmFsdWU9eygpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZUlucHV0KCd2YWx1ZScsIGZvcm1WYWx1ZXMuZGVmYXVsdF92YWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2Zvcm1WYWx1ZXMudmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICAgICAgPGhyIC8+XG5cbiAgICAgICAgICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJtdC0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHhzPVwiM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1MYWJlbD57dCgnVmFsdWUnKX08L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJ2YWx1ZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbmx5TnVtYmVyc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN0cmluZ1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI1XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtTGFiZWw+e3QoJ1VuaXQnKX08L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWwgPT09IFBBUlRJQUxfRURJVElPTl9NT0RFXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwidW5pdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI1XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtTGFiZWw+e3QoJ0RlY2ltYWxzJyl9PC9Gb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50PXtSZWR1eEZpZWxkU2VsZWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gUEFSVElBTF9FRElUSU9OX01PREVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJkZWNpbWFsX3R5cGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbm9ybWFsaXplPXtub3JtYWxpemVEZWNpbWFsRmllbGQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVjaW1hbEZvcm1hdHNEYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdkZWNpbWFsX3BsYWNlcydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zPXtnZXREZWNpbWFsRm9ybWF0c1RvU2VsZWN0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlY2ltYWxGb3JtYXRzRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG5cbiAgICAgICAgICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJtdC0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDYXB0aW9uIGNsYXNzTmFtZT1cIm10LTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3QoJ0RlZmF1bHQgdmFsdWUnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NhcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2Zvcm1WYWx1ZXMudmFsdWUgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGFnTGFiZWwgYmFja2dyb3VuZD17Zm9ybVZhbHVlcy5jb2xvcn0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0Rm9ybWF0dGVkVmFsdWUoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWdzTG9jYWxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1WYWx1ZXNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvVGFnTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICA8L1NldHRpbmdzQm94PlxuXG4gICAgICAgICAgICAgICAgeyFpc0VtcHR5KGVycm9ycykgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm10LTNcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2w+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEFsZXJ0IHZhcmlhbnQ9XCJ3YXJuaW5nXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0KCdZb3UgbmVlZCB0byBjYXB0dXJlIGFsbCBmaWVsZHMnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0eWxlZEFsZXJ0PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm10LTMganVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICA8Q29sIHhzPVwiMTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b25JbnNlcnQgc2l6ZT1cInNtYWxsXCIgdHlwZT1cInN1Ym1pdFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1hcnJvdy1yaWdodCBtci0yXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtnZXRUZXh0QnV0dG9uKGNvbnRlbnRWaWV3U3RhdGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9CdXR0b25JbnNlcnQ+XG4gICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgPC9Gb3JtPlxuICAgICAgICA8L2Rpdj5cbiAgICApO1xufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcbiAgICBjdXJyZW5jaWVzOiBzZWxlY3RvcnMuZ2V0Q3VycmVuY2llc0RhdGEsXG4gICAgZGVjaW1hbEZvcm1hdHNEYXRhOiBzZWxlY3RvcnMuZ2V0RGVjaW1hbEZvcm1hdHNEYXRhLFxuICAgIGVycm9yczogc2VsZWN0b3JzLmdldEN1c3RvbVRhZ0Zvcm1FcnJvcnMsXG4gICAgZm9ybVZhbHVlczogc2VsZWN0b3JzLmdldEN1c3RvbVRhZ1ZhbHVlcyxcbiAgICBpbml0aWFsVmFsdWVzOiBzZWxlY3RvcnMuZ2V0SW5pdGlhbEN1c3RvbVRhZ1ZhbHVlcyxcbiAgICB1bml0Rm9ybWF0c0RhdGE6IHNlbGVjdG9ycy5nZXRVbml0Rm9ybWF0c0RhdGEsXG59KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiAoe1xuICAgIGNoYW5nZUlucHV0OiAoZmllbGQsIHZhbHVlKSA9PlxuICAgICAgICBkaXNwYXRjaChjaGFuZ2UoJ2N1c3RvbS10YWctZm9ybScsIGZpZWxkLCB2YWx1ZSkpLFxufSk7XG5cblRhZ0Zvcm1XaXRoQ2FsY3VsYXRvci5wcm9wVHlwZXMgPSB7XG4gICAgY2hhbmdlSW5wdXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIHRhZ3NMb2NhbGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29udGVudFZpZXdTdGF0ZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBjdXJyZW5jaWVzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgZGVjaW1hbEZvcm1hdHNEYXRhOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgZWRpdGlvbkxldmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGVycm9yczogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBmb3JtVmFsdWVzOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGhhbmRsZU9uU3VibWl0OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYW5kbGVTdWJtaXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGlzSW5pdGlhbGF6aW5nOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZXRJc09wZW5UYWdMaWJyYXJ5TW9kYWw6IFByb3BUeXBlcy5mdW5jLFxuICAgIHVuaXRGb3JtYXRzRGF0YTogUHJvcFR5cGVzLmFycmF5LFxufTtcblxuY29uc3QgRm9ybUNvbnRhaW5lciA9IHJlZHV4Rm9ybSh7XG4gICAgZW5hYmxlUmVpbml0aWFsaXplOiB0cnVlLFxuICAgIGZvcm06ICdjdXN0b20tdGFnLWZvcm0nLFxuICAgIHZhbGlkYXRlLFxufSkoVGFnRm9ybVdpdGhDYWxjdWxhdG9yKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoRm9ybUNvbnRhaW5lcik7XG4iXX0=