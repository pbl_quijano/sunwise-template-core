"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _tags = require("../../../../constants/tags");

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BackspaceIcon = function BackspaceIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M0-1h13.44v13.44H0z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M12.32.68h-8.4c-.386 0-.689.196-.89.493L0 5.72l3.03 4.542c.201.296.504.498.89.498h8.4c.616 0 1.12-.504 1.12-1.12V1.8c0-.616-.504-1.12-1.12-1.12Zm0 8.96H3.96L1.343 5.72l2.61-3.92h8.366v7.84ZM5.83 8.52l2.01-2.01 2.01 2.01.79-.79-2.01-2.01 2.01-2.01-.79-.79-2.01 2.01-2.01-2.01-.79.79 2.01 2.01-2.01 2.01.79.79Z",
        fill: "#848BAB",
        fillRule: "nonzero"
      })]
    })
  }));
};

BackspaceIcon.defaultProps = {
  width: "14",
  height: "11",
  xmlns: "http://www.w3.org/2000/svg"
};

var RefreshIcon = function RefreshIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M6.4 4.2v2.1l2.8-2.8L6.4.7v2.1A5.598 5.598 0 0 0 .8 8.4c0 1.099.322 2.121.868 2.982L2.69 10.36A4.11 4.11 0 0 1 2.2 8.4c0-2.317 1.883-4.2 4.2-4.2Zm4.732 1.218L10.11 6.44c.308.588.49 1.253.49 1.96 0 2.317-1.883 4.2-4.2 4.2v-2.1l-2.8 2.8 2.8 2.8V14c3.094 0 5.6-2.506 5.6-5.6a5.552 5.552 0 0 0-.868-2.982Z",
      fill: "#848BAB",
      fillRule: "nonzero"
    })
  }));
};

RefreshIcon.defaultProps = {
  width: "12",
  height: "17",
  xmlns: "http://www.w3.org/2000/svg"
};

var FormulaControlButtons = function FormulaControlButtons(_ref) {
  var calculator = _ref.calculator,
      handleClickEraser = _ref.handleClickEraser,
      handleClickReset = _ref.handleClickReset,
      contentViewState = _ref.contentViewState;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if (calculator.length === 0 || contentViewState !== _tags.CREATE_CUSTOM_TAG_STATE) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonReset, {
      onClick: function onClick() {
        return handleClickEraser();
      },
      title: t('Delete last element of the formula'),
      type: "button",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
        alt: "backspace",
        className: "mr-2",
        src: BackspaceIcon,
        width: "14"
      }), t('Erase')]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonReset, {
      onClick: function onClick() {
        return handleClickReset();
      },
      title: t('Clear'),
      type: "button",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
        alt: "reset",
        className: "mr-2",
        src: RefreshIcon
      }), t('Reset formula')]
    })]
  });
};

FormulaControlButtons.propTypes = {
  calculator: _propTypes.default.array,
  handleClickEraser: _propTypes.default.func,
  handleClickReset: _propTypes.default.func,
  contentViewState: _propTypes.default.number
};
var _default = FormulaControlButtons;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL0N1c3RvbVRhZ0Zvcm0vRm9ybXVsYUNvbnRyb2xCdXR0b25zLmpzIl0sIm5hbWVzIjpbIkJhY2tzcGFjZUljb24iLCJSZWZyZXNoSWNvbiIsIkZvcm11bGFDb250cm9sQnV0dG9ucyIsImNhbGN1bGF0b3IiLCJoYW5kbGVDbGlja0VyYXNlciIsImhhbmRsZUNsaWNrUmVzZXQiLCJjb250ZW50Vmlld1N0YXRlIiwidCIsImxlbmd0aCIsIkNSRUFURV9DVVNUT01fVEFHX1NUQVRFIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJmdW5jIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFFQTs7QUFLQTs7Ozs7Ozs7Ozs7O0lBSDJCQSxhLFlBQUFBLGE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsYTs7Ozs7O0lBQ0FDLFcsWUFBQUEsVzs7Ozs7Ozs7OztBQUFBQSxXOzs7Ozs7QUFJM0IsSUFBTUMscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixPQUt4QjtBQUFBLE1BSkZDLFVBSUUsUUFKRkEsVUFJRTtBQUFBLE1BSEZDLGlCQUdFLFFBSEZBLGlCQUdFO0FBQUEsTUFGRkMsZ0JBRUUsUUFGRkEsZ0JBRUU7QUFBQSxNQURGQyxnQkFDRSxRQURGQSxnQkFDRTs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EsTUFDSUosVUFBVSxDQUFDSyxNQUFYLEtBQXNCLENBQXRCLElBQ0FGLGdCQUFnQixLQUFLRyw2QkFGekIsRUFHRTtBQUNFLFdBQU8sSUFBUDtBQUNIOztBQUNELHNCQUNJO0FBQUEsNEJBQ0ksc0JBQUMsNkJBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBRTtBQUFBLGVBQU1MLGlCQUFpQixFQUF2QjtBQUFBLE9BRGI7QUFFSSxNQUFBLEtBQUssRUFBRUcsQ0FBQyxDQUFDLG9DQUFELENBRlo7QUFHSSxNQUFBLElBQUksRUFBQyxRQUhUO0FBQUEsOEJBS0k7QUFDSSxRQUFBLEdBQUcsRUFBQyxXQURSO0FBRUksUUFBQSxTQUFTLEVBQUMsTUFGZDtBQUdJLFFBQUEsR0FBRyxFQUFFUCxhQUhUO0FBSUksUUFBQSxLQUFLLEVBQUM7QUFKVixRQUxKLEVBV0tPLENBQUMsQ0FBQyxPQUFELENBWE47QUFBQSxNQURKLGVBY0ksc0JBQUMsNkJBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBRTtBQUFBLGVBQU1GLGdCQUFnQixFQUF0QjtBQUFBLE9BRGI7QUFFSSxNQUFBLEtBQUssRUFBRUUsQ0FBQyxDQUFDLE9BQUQsQ0FGWjtBQUdJLE1BQUEsSUFBSSxFQUFDLFFBSFQ7QUFBQSw4QkFLSTtBQUFLLFFBQUEsR0FBRyxFQUFDLE9BQVQ7QUFBaUIsUUFBQSxTQUFTLEVBQUMsTUFBM0I7QUFBa0MsUUFBQSxHQUFHLEVBQUVOO0FBQXZDLFFBTEosRUFNS00sQ0FBQyxDQUFDLGVBQUQsQ0FOTjtBQUFBLE1BZEo7QUFBQSxJQURKO0FBeUJILENBdENEOztBQXdDQUwscUJBQXFCLENBQUNRLFNBQXRCLEdBQWtDO0FBQzlCUCxFQUFBQSxVQUFVLEVBQUVRLG1CQUFVQyxLQURRO0FBRTlCUixFQUFBQSxpQkFBaUIsRUFBRU8sbUJBQVVFLElBRkM7QUFHOUJSLEVBQUFBLGdCQUFnQixFQUFFTSxtQkFBVUUsSUFIRTtBQUk5QlAsRUFBQUEsZ0JBQWdCLEVBQUVLLG1CQUFVRztBQUpFLENBQWxDO2VBT2VaLHFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCB7IENSRUFURV9DVVNUT01fVEFHX1NUQVRFIH0gZnJvbSAnQGNvbnN0YW50cy90YWdzJztcblxuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgQmFja3NwYWNlSWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvYmFja3NwYWNlLnN2Zyc7XG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBSZWZyZXNoSWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvcmVmcmVzaC5zdmcnO1xuXG5pbXBvcnQgeyBCdXR0b25SZXNldCB9IGZyb20gJy4uL3N0eWxlZENvbXBvbmVudHMnO1xuXG5jb25zdCBGb3JtdWxhQ29udHJvbEJ1dHRvbnMgPSAoe1xuICAgIGNhbGN1bGF0b3IsXG4gICAgaGFuZGxlQ2xpY2tFcmFzZXIsXG4gICAgaGFuZGxlQ2xpY2tSZXNldCxcbiAgICBjb250ZW50Vmlld1N0YXRlLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBpZiAoXG4gICAgICAgIGNhbGN1bGF0b3IubGVuZ3RoID09PSAwIHx8XG4gICAgICAgIGNvbnRlbnRWaWV3U3RhdGUgIT09IENSRUFURV9DVVNUT01fVEFHX1NUQVRFXG4gICAgKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgICA8PlxuICAgICAgICAgICAgPEJ1dHRvblJlc2V0XG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlQ2xpY2tFcmFzZXIoKX1cbiAgICAgICAgICAgICAgICB0aXRsZT17dCgnRGVsZXRlIGxhc3QgZWxlbWVudCBvZiB0aGUgZm9ybXVsYScpfVxuICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICAgICAgYWx0PVwiYmFja3NwYWNlXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXItMlwiXG4gICAgICAgICAgICAgICAgICAgIHNyYz17QmFja3NwYWNlSWNvbn1cbiAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxNFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICB7dCgnRXJhc2UnKX1cbiAgICAgICAgICAgIDwvQnV0dG9uUmVzZXQ+XG4gICAgICAgICAgICA8QnV0dG9uUmVzZXRcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBoYW5kbGVDbGlja1Jlc2V0KCl9XG4gICAgICAgICAgICAgICAgdGl0bGU9e3QoJ0NsZWFyJyl9XG4gICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPGltZyBhbHQ9XCJyZXNldFwiIGNsYXNzTmFtZT1cIm1yLTJcIiBzcmM9e1JlZnJlc2hJY29ufSAvPlxuICAgICAgICAgICAgICAgIHt0KCdSZXNldCBmb3JtdWxhJyl9XG4gICAgICAgICAgICA8L0J1dHRvblJlc2V0PlxuICAgICAgICA8Lz5cbiAgICApO1xufTtcblxuRm9ybXVsYUNvbnRyb2xCdXR0b25zLnByb3BUeXBlcyA9IHtcbiAgICBjYWxjdWxhdG9yOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgaGFuZGxlQ2xpY2tFcmFzZXI6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZUNsaWNrUmVzZXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGNvbnRlbnRWaWV3U3RhdGU6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBGb3JtdWxhQ29udHJvbEJ1dHRvbnM7XG4iXX0=