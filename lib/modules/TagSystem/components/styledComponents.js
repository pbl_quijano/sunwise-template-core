"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TagTitle = exports.TagLabel = exports.StyledTitleField = exports.StyledText = exports.StyledInputNumber = exports.StyledInputGroup = exports.StyledField = exports.StyledAlert = exports.SettingsBox = exports.NavItem = exports.FormLabel = exports.CategoryWrapper = exports.CategoryTitle = exports.Caption = exports.ButtonReset = exports.ButtonOperator = exports.ButtonNew = exports.ButtonInsertNumber = exports.ButtonInsert = void 0;

var _reactBootstrap = require("react-bootstrap");

var _reduxForm = require("redux-form");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _AddButton = _interopRequireDefault(require("../../../components/AddButton"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11, _templateObject12, _templateObject13, _templateObject14, _templateObject15, _templateObject16, _templateObject17, _templateObject18, _templateObject19;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ButtonInsert = (0, _styledComponents.default)(_AddButton.default)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    background: #ff9a00;\n    text-transform: uppercase;\n    box-shadow: none !important;\n    outline: none !important;\n"])));
exports.ButtonInsert = ButtonInsert;

var ButtonInsertNumber = _styledComponents.default.button(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: ", ";\n    width: 40px;\n    display: inline-flex;\n    justify-content: center;\n    align-items: center;\n    border: 0;\n    border-top-left-radius: 0;\n    border-top-right-radius: 3px;\n    border-bottom-right-radius: 3px;\n    border-bottom-left-radius: 0;\n    background-color: ", ";\n    box-shadow: none !important;\n    color: ", ";\n    font-size: 12px;\n    font-weight: 800;\n    letter-spacing: 0.31px;\n    line-height: 15px;\n"])), function (_ref) {
  var _ref$height = _ref.height,
      height = _ref$height === void 0 ? 40 : _ref$height;
  return "".concat(height, "px");
}, function (_ref2) {
  var disabled = _ref2.disabled,
      _ref2$background = _ref2.background,
      background = _ref2$background === void 0 ? '#2F4DFF' : _ref2$background;
  return disabled ? '#e9ecef' : background;
}, function (_ref3) {
  var disabled = _ref3.disabled,
      _ref3$color = _ref3.color,
      color = _ref3$color === void 0 ? '#FFFFFF' : _ref3$color;
  return disabled ? '#848BAB' : color;
});

exports.ButtonInsertNumber = ButtonInsertNumber;

var ButtonNew = _styledComponents.default.button(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    display: inline-flex;\n    justify-content: center;\n    align-items: center;\n    box-sizing: border-box;\n    width: 100%;\n    height: 49px;\n    border: 1px dashed #d3d7eb;\n    border-radius: 5px;\n    background-color: #ffffff;\n    box-shadow: none !important;\n    outline: none !important;\n    color: #002438;\n    font-size: 12px;\n    font-weight: 500;\n    letter-spacing: 0;\n    line-height: 15px;\n    .fa {\n        color: #2f4dff;\n    }\n"])));

exports.ButtonNew = ButtonNew;

var ButtonOperator = _styledComponents.default.button(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    height: 40px;\n    width: 40px;\n    display: inline-flex;\n    justify-content: center;\n    align-items: center;\n    border: 1px solid #eff1fb;\n    border-radius: 3px;\n    background-color: ", ";\n    margin-right: 0.5rem;\n    margin-bottom: 0.5rem;\n    box-shadow: none !important;\n    outline: none !important;\n    color: #848bab;\n    font-size: 12px;\n    font-weight: 800;\n    letter-spacing: 0.31px;\n    line-height: 15px;\n"])), function (_ref4) {
  var disabled = _ref4.disabled;
  return disabled ? '#e9ecef' : '#FFFFFF';
});

exports.ButtonOperator = ButtonOperator;

var ButtonReset = _styledComponents.default.button(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    height: 30px;\n    display: inline-flex;\n    justify-content: center;\n    align-items: center;\n    border: 1px solid #ecedf0;\n    border-radius: 3px;\n    background-color: #ffffff;\n    box-shadow: none !important;\n    outline: none !important;\n    margin-right: 0.5rem;\n    color: #848bab;\n    font-size: 12px;\n    letter-spacing: 0;\n    line-height: 14px;\n    img {\n        object-fit: contain;\n        object-position: center;\n        height: 14px;\n    }\n"])));

exports.ButtonReset = ButtonReset;

var Caption = _styledComponents.default.h5(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 16px;\n    font-weight: 600;\n    letter-spacing: 0;\n    line-height: 19px;\n"])));

exports.Caption = Caption;

var CategoryTitle = _styledComponents.default.h4(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 18px;\n    font-weight: bold;\n    letter-spacing: 0;\n    line-height: 22px;\n"])));

exports.CategoryTitle = CategoryTitle;

var CategoryWrapper = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: calc(100% - 108px);\n    overflow-y: auto;\n"])));

exports.CategoryWrapper = CategoryWrapper;

var FormLabel = _styledComponents.default.label(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n    color: #2f4dff !important;\n    font-size: 10px !important;\n    letter-spacing: 0;\n    line-height: 13px !important;\n    padding-left: 0 !important;\n    margin-bottom: 0;\n"])));

exports.FormLabel = FormLabel;
var NavItem = (0, _styledComponents.default)(_reactBootstrap.NavLink)(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 15px;\n    letter-spacing: 0;\n    line-height: 20px;\n    padding: 0.8rem 1rem;\n    &:hover,\n    &.active {\n        background-color: #f6f8fa;\n    }\n"])));
exports.NavItem = NavItem;

var SettingsBox = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n    width: 100%;\n    min-height: 296px;\n    box-sizing: border-box;\n    border: 1px solid #eff1fb;\n    border-radius: 3px;\n    background-color: rgba(255, 255, 255, 0.02);\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    padding: 1rem;\n"])));

exports.SettingsBox = SettingsBox;
var StyledAlert = (0, _styledComponents.default)(_reactBootstrap.Alert)(_templateObject12 || (_templateObject12 = _taggedTemplateLiteral(["\n    font-size: 11px !important;\n    font-weight: bold;\n"])));
exports.StyledAlert = StyledAlert;
var StyledField = (0, _styledComponents.default)(_reduxForm.Field)(_templateObject13 || (_templateObject13 = _taggedTemplateLiteral(["\n    color: #202253;\n    font-size: 13px;\n    font-weight: 600;\n    letter-spacing: 0;\n    line-height: 16px;\n    padding-left: 0 !important;\n    border: 0 !important;\n    border-bottom: 1px solid #eff1fb !important;\n    border-radius: 0 !important;\n    box-shadow: none !important;\n    outline: none !important;\n"])));
exports.StyledField = StyledField;

var StyledInputGroup = _styledComponents.default.div(_templateObject14 || (_templateObject14 = _taggedTemplateLiteral(["\n    position: relative;\n    display: inline-flex;\n    align-items: stretch;\n    margin-right: 0.5rem;\n    margin-bottom: 0.5rem;\n"])));

exports.StyledInputGroup = StyledInputGroup;
var StyledInputNumber = (0, _styledComponents.default)(_reactBootstrap.Form.Control)(_templateObject15 || (_templateObject15 = _taggedTemplateLiteral(["\n    height: 40px;\n    max-width: 100px;\n    border: 1px solid #eff1fb;\n    border-radius: 3px;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 0;\n    border-bottom-right-radius: 0;\n    border-bottom-left-radius: 3px;\n    background-color: ", ";\n    box-shadow: none !important;\n    color: #202253;\n    font-size: 13px;\n    font-weight: 600;\n    text-align: center;\n    letter-spacing: 0;\n    line-height: 16px;\n    &:focus {\n        border: 1px solid #eff1fb;\n    }\n"])), function (_ref5) {
  var disabled = _ref5.disabled;
  return disabled ? '#e9ecef' : '#FFFFFF';
});
exports.StyledInputNumber = StyledInputNumber;

var StyledText = _styledComponents.default.p(_templateObject16 || (_templateObject16 = _taggedTemplateLiteral(["\n    font-size: 13px;\n"])));

exports.StyledText = StyledText;
var StyledTitleField = (0, _styledComponents.default)(_reduxForm.Field)(_templateObject17 || (_templateObject17 = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 18px;\n    font-weight: bold;\n    letter-spacing: 0;\n    line-height: 22px;\n    padding-left: 0 !important;\n    border: 0 !important;\n    border-bottom: 1px solid #eff1fb !important;\n    border-radius: 0 !important;\n    box-shadow: none !important;\n    outline: none !important;\n"])));
exports.StyledTitleField = StyledTitleField;

var TagLabel = _styledComponents.default.div(_templateObject18 || (_templateObject18 = _taggedTemplateLiteral(["\n    display: inline-flex;\n    flex-direction: row;\n    justify-content: center;\n    align-items: center;\n    width: fit-content;\n    min-height: 24px;\n    padding: 10px;\n    border-radius: 5px;\n    background: ", ";\n    margin-right: 0.5rem;\n    margin-bottom: 0.5rem;\n    color: #002438;\n    font-size: 12px;\n    font-weight: 500;\n    letter-spacing: 0;\n    line-height: 15px;\n"])), function (_ref6) {
  var background = _ref6.background;
  return background || '#EEEEEE';
});

exports.TagLabel = TagLabel;

var TagTitle = _styledComponents.default.h5(_templateObject19 || (_templateObject19 = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 18px;\n    font-weight: bold;\n    letter-spacing: 0;\n    line-height: 22px;\n"])));

exports.TagTitle = TagTitle;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL3N0eWxlZENvbXBvbmVudHMuanMiXSwibmFtZXMiOlsiQnV0dG9uSW5zZXJ0IiwiQWRkQnV0dG9uIiwiQnV0dG9uSW5zZXJ0TnVtYmVyIiwic3R5bGVkIiwiYnV0dG9uIiwiaGVpZ2h0IiwiZGlzYWJsZWQiLCJiYWNrZ3JvdW5kIiwiY29sb3IiLCJCdXR0b25OZXciLCJCdXR0b25PcGVyYXRvciIsIkJ1dHRvblJlc2V0IiwiQ2FwdGlvbiIsImg1IiwiQ2F0ZWdvcnlUaXRsZSIsImg0IiwiQ2F0ZWdvcnlXcmFwcGVyIiwiZGl2IiwiRm9ybUxhYmVsIiwibGFiZWwiLCJOYXZJdGVtIiwiTmF2TGluayIsIlNldHRpbmdzQm94IiwiU3R5bGVkQWxlcnQiLCJBbGVydCIsIlN0eWxlZEZpZWxkIiwiRmllbGQiLCJTdHlsZWRJbnB1dEdyb3VwIiwiU3R5bGVkSW5wdXROdW1iZXIiLCJGb3JtIiwiQ29udHJvbCIsIlN0eWxlZFRleHQiLCJwIiwiU3R5bGVkVGl0bGVGaWVsZCIsIlRhZ0xhYmVsIiwiVGFnVGl0bGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7QUFFTyxJQUFNQSxZQUFZLEdBQUcsK0JBQU9DLGtCQUFQLENBQUgsb05BQWxCOzs7QUFRQSxJQUFNQyxrQkFBa0IsR0FBR0MsMEJBQU9DLE1BQVYsaWhCQUNqQjtBQUFBLHlCQUFHQyxNQUFIO0FBQUEsTUFBR0EsTUFBSCw0QkFBWSxFQUFaO0FBQUEsbUJBQXdCQSxNQUF4QjtBQUFBLENBRGlCLEVBV1A7QUFBQSxNQUFHQyxRQUFILFNBQUdBLFFBQUg7QUFBQSwrQkFBYUMsVUFBYjtBQUFBLE1BQWFBLFVBQWIsaUNBQTBCLFNBQTFCO0FBQUEsU0FDaEJELFFBQVEsR0FBRyxTQUFILEdBQWVDLFVBRFA7QUFBQSxDQVhPLEVBY2xCO0FBQUEsTUFBR0QsUUFBSCxTQUFHQSxRQUFIO0FBQUEsMEJBQWFFLEtBQWI7QUFBQSxNQUFhQSxLQUFiLDRCQUFxQixTQUFyQjtBQUFBLFNBQ0xGLFFBQVEsR0FBRyxTQUFILEdBQWVFLEtBRGxCO0FBQUEsQ0Fka0IsQ0FBeEI7Ozs7QUFzQkEsSUFBTUMsU0FBUyxHQUFHTiwwQkFBT0MsTUFBVixzaEJBQWY7Ozs7QUFzQkEsSUFBTU0sY0FBYyxHQUFHUCwwQkFBT0MsTUFBVix1Z0JBUUg7QUFBQSxNQUFHRSxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQkEsUUFBUSxHQUFHLFNBQUgsR0FBZSxTQUExQztBQUFBLENBUkcsQ0FBcEI7Ozs7QUFvQkEsSUFBTUssV0FBVyxHQUFHUiwwQkFBT0MsTUFBVix3aUJBQWpCOzs7O0FBc0JBLElBQU1RLE9BQU8sR0FBR1QsMEJBQU9VLEVBQVYsMkxBQWI7Ozs7QUFRQSxJQUFNQyxhQUFhLEdBQUdYLDBCQUFPWSxFQUFWLDRMQUFuQjs7OztBQVFBLElBQU1DLGVBQWUsR0FBR2IsMEJBQU9jLEdBQVYsa0xBQXJCOzs7O0FBT0EsSUFBTUMsU0FBUyxHQUFHZiwwQkFBT2dCLEtBQVYsNlBBQWY7OztBQVNBLElBQU1DLE9BQU8sR0FBRywrQkFBT0MsdUJBQVAsQ0FBSCwwUUFBYjs7O0FBWUEsSUFBTUMsV0FBVyxHQUFHbkIsMEJBQU9jLEdBQVYsMFVBQWpCOzs7QUFXQSxJQUFNTSxXQUFXLEdBQUcsK0JBQU9DLHFCQUFQLENBQUgsb0lBQWpCOztBQUtBLElBQU1DLFdBQVcsR0FBRywrQkFBT0MsZ0JBQVAsQ0FBSCw4WUFBakI7OztBQWNBLElBQU1DLGdCQUFnQixHQUFHeEIsMEJBQU9jLEdBQVYsaU5BQXRCOzs7QUFRQSxJQUFNVyxpQkFBaUIsR0FBRywrQkFBT0MscUJBQUtDLE9BQVosQ0FBSCwrakJBU047QUFBQSxNQUFHeEIsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUJBLFFBQVEsR0FBRyxTQUFILEdBQWUsU0FBMUM7QUFBQSxDQVRNLENBQXZCOzs7QUFzQkEsSUFBTXlCLFVBQVUsR0FBRzVCLDBCQUFPNkIsQ0FBVixpR0FBaEI7OztBQUlBLElBQU1DLGdCQUFnQixHQUFHLCtCQUFPUCxnQkFBUCxDQUFILCtZQUF0Qjs7O0FBY0EsSUFBTVEsUUFBUSxHQUFHL0IsMEJBQU9jLEdBQVYsc2RBU0g7QUFBQSxNQUFHVixVQUFILFNBQUdBLFVBQUg7QUFBQSxTQUFvQkEsVUFBVSxJQUFJLFNBQWxDO0FBQUEsQ0FURyxDQUFkOzs7O0FBbUJBLElBQU00QixRQUFRLEdBQUdoQywwQkFBT1UsRUFBViw4TEFBZCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFsZXJ0LCBGb3JtLCBOYXZMaW5rIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IEZpZWxkIH0gZnJvbSAncmVkdXgtZm9ybSc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IEFkZEJ1dHRvbiBmcm9tICdAY29tcG9uZW50cy9BZGRCdXR0b24nO1xuXG5leHBvcnQgY29uc3QgQnV0dG9uSW5zZXJ0ID0gc3R5bGVkKEFkZEJ1dHRvbilgXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogI2ZmOWEwMDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG5gO1xuXG5leHBvcnQgY29uc3QgQnV0dG9uSW5zZXJ0TnVtYmVyID0gc3R5bGVkLmJ1dHRvbmBcbiAgICBoZWlnaHQ6ICR7KHsgaGVpZ2h0ID0gNDAgfSkgPT4gYCR7aGVpZ2h0fXB4YH07XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXI6IDA7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogM3B4O1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAzcHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkeyh7IGRpc2FibGVkLCBiYWNrZ3JvdW5kID0gJyMyRjRERkYnIH0pID0+XG4gICAgICAgIGRpc2FibGVkID8gJyNlOWVjZWYnIDogYmFja2dyb3VuZH07XG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAkeyh7IGRpc2FibGVkLCBjb2xvciA9ICcjRkZGRkZGJyB9KSA9PlxuICAgICAgICBkaXNhYmxlZCA/ICcjODQ4QkFCJyA6IGNvbG9yfTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4zMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuYDtcblxuZXhwb3J0IGNvbnN0IEJ1dHRvbk5ldyA9IHN0eWxlZC5idXR0b25gXG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNDlweDtcbiAgICBib3JkZXI6IDFweCBkYXNoZWQgI2QzZDdlYjtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjMDAyNDM4O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgIC5mYSB7XG4gICAgICAgIGNvbG9yOiAjMmY0ZGZmO1xuICAgIH1cbmA7XG5cbmV4cG9ydCBjb25zdCBCdXR0b25PcGVyYXRvciA9IHN0eWxlZC5idXR0b25gXG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2VmZjFmYjtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBkaXNhYmxlZCB9KSA9PiAoZGlzYWJsZWQgPyAnI2U5ZWNlZicgOiAnI0ZGRkZGRicpfTtcbiAgICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMC4zMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuYDtcblxuZXhwb3J0IGNvbnN0IEJ1dHRvblJlc2V0ID0gc3R5bGVkLmJ1dHRvbmBcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWNlZGYwO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAwLjVyZW07XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIGltZyB7XG4gICAgICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XG4gICAgICAgIG9iamVjdC1wb3NpdGlvbjogY2VudGVyO1xuICAgICAgICBoZWlnaHQ6IDE0cHg7XG4gICAgfVxuYDtcblxuZXhwb3J0IGNvbnN0IENhcHRpb24gPSBzdHlsZWQuaDVgXG4gICAgY29sb3I6ICMwMDI0Mzg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDE5cHg7XG5gO1xuXG5leHBvcnQgY29uc3QgQ2F0ZWdvcnlUaXRsZSA9IHN0eWxlZC5oNGBcbiAgICBjb2xvcjogIzAwMjQzODtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG5gO1xuXG5leHBvcnQgY29uc3QgQ2F0ZWdvcnlXcmFwcGVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAxMDhweCk7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbmA7XG5cbmV4cG9ydCBjb25zdCBGb3JtTGFiZWwgPSBzdHlsZWQubGFiZWxgXG4gICAgY29sb3I6ICMyZjRkZmYgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDEwcHggIWltcG9ydGFudDtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMTNweCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG5gO1xuXG5leHBvcnQgY29uc3QgTmF2SXRlbSA9IHN0eWxlZChOYXZMaW5rKWBcbiAgICBjb2xvcjogIzAwMjQzODtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgcGFkZGluZzogMC44cmVtIDFyZW07XG4gICAgJjpob3ZlcixcbiAgICAmLmFjdGl2ZSB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNmY4ZmE7XG4gICAgfVxuYDtcblxuZXhwb3J0IGNvbnN0IFNldHRpbmdzQm94ID0gc3R5bGVkLmRpdmBcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtaW4taGVpZ2h0OiAyOTZweDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmYxZmI7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4wMik7XG4gICAgYm94LXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMTI5LCAxNTgsIDIwMCwgMC4wNik7XG4gICAgcGFkZGluZzogMXJlbTtcbmA7XG5cbmV4cG9ydCBjb25zdCBTdHlsZWRBbGVydCA9IHN0eWxlZChBbGVydClgXG4gICAgZm9udC1zaXplOiAxMXB4ICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG5gO1xuXG5leHBvcnQgY29uc3QgU3R5bGVkRmllbGQgPSBzdHlsZWQoRmllbGQpYFxuICAgIGNvbG9yOiAjMjAyMjUzO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xuICAgIGJvcmRlcjogMCAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWZmMWZiICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG5gO1xuXG5leHBvcnQgY29uc3QgU3R5bGVkSW5wdXRHcm91cCA9IHN0eWxlZC5kaXZgXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBzdHJldGNoO1xuICAgIG1hcmdpbi1yaWdodDogMC41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IDAuNXJlbTtcbmA7XG5cbmV4cG9ydCBjb25zdCBTdHlsZWRJbnB1dE51bWJlciA9IHN0eWxlZChGb3JtLkNvbnRyb2wpYFxuICAgIGhlaWdodDogNDBweDtcbiAgICBtYXgtd2lkdGg6IDEwMHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmYxZmI7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDNweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBkaXNhYmxlZCB9KSA9PiAoZGlzYWJsZWQgPyAnI2U5ZWNlZicgOiAnI0ZGRkZGRicpfTtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6ICMyMDIyNTM7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgICY6Zm9jdXMge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZWZmMWZiO1xuICAgIH1cbmA7XG5cbmV4cG9ydCBjb25zdCBTdHlsZWRUZXh0ID0gc3R5bGVkLnBgXG4gICAgZm9udC1zaXplOiAxM3B4O1xuYDtcblxuZXhwb3J0IGNvbnN0IFN0eWxlZFRpdGxlRmllbGQgPSBzdHlsZWQoRmllbGQpYFxuICAgIGNvbG9yOiAjMDAyNDM4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMjJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDAgIWltcG9ydGFudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VmZjFmYiAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDAgIWltcG9ydGFudDtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xuYDtcblxuZXhwb3J0IGNvbnN0IFRhZ0xhYmVsID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IGZpdC1jb250ZW50O1xuICAgIG1pbi1oZWlnaHQ6IDI0cHg7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogJHsoeyBiYWNrZ3JvdW5kIH0pID0+IGJhY2tncm91bmQgfHwgJyNFRUVFRUUnfTtcbiAgICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gICAgY29sb3I6ICMwMDI0Mzg7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgbGluZS1oZWlnaHQ6IDE1cHg7XG5gO1xuXG5leHBvcnQgY29uc3QgVGFnVGl0bGUgPSBzdHlsZWQuaDVgXG4gICAgY29sb3I6ICMwMDI0Mzg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xuYDtcbiJdfQ==