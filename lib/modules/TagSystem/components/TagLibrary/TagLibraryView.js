"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _Button = _interopRequireDefault(require("../../../../components/Button"));

var _InputSearch = _interopRequireDefault(require("../../../../components/InputSearch"));

var _tags = require("../../../../constants/tags");

var actions = _interopRequireWildcard(require("../../actions"));

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

var _PlaceholderLibraryTags = _interopRequireDefault(require("./PlaceholderLibraryTags"));

var _TagLibraryContent = _interopRequireDefault(require("./TagLibraryContent"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var LibraryView = function LibraryView(_ref) {
  var catalogs = _ref.catalogs,
      tagsLocale = _ref.tagsLocale,
      fetchDocuments = _ref.fetchDocuments,
      fetchTags = _ref.fetchTags,
      filteredTags = _ref.filteredTags,
      filterTags = _ref.filterTags,
      editionLevel = _ref.editionLevel,
      handleOnSubmit = _ref.handleOnSubmit,
      hasProposalSelector = _ref.hasProposalSelector,
      isFetchingTags = _ref.isFetchingTags,
      isForSmartDocuments = _ref.isForSmartDocuments,
      isFromCustomTag = _ref.isFromCustomTag,
      isInitialazing = _ref.isInitialazing,
      selectCategory = _ref.selectCategory,
      selectedCategory = _ref.selectedCategory,
      selectedTag = _ref.selectedTag,
      selectTag = _ref.selectTag,
      setContentViewState = _ref.setContentViewState,
      tags = _ref.tags;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useState = (0, _react.useState)(''),
      _useState2 = _slicedToArray(_useState, 2),
      searchText = _useState2[0],
      setSearchText = _useState2[1];

  (0, _react.useEffect)(function () {
    fetchTags();
  }, []);
  (0, _react.useEffect)(function () {
    (0, _helpers.searchTags)(tags, searchText, filterTags);
  }, [tags, searchText]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_PlaceholderLibraryTags.default, {
    ready: isFetchingTags || isInitialazing,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          xs: "14",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_InputSearch.default, {
            onChange: function onChange(e) {
              return setSearchText(e.target.value);
            },
            placeholder: t('Search'),
            value: searchText
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          xs: "4",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Button.default, {
            className: "ml-auto",
            color: "#2F4DFF",
            direction: "reverse",
            handleClick: function handleClick() {
              return setContentViewState(_tags.CREATE_CUSTOM_TAG_STATE);
            },
            icon: "fas fa-plus",
            label: t('Create tag', {
              count: 2
            }),
            variant: "bold",
            visible: !isFromCustomTag
          })
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {
        className: "mb-0"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagLibraryContent.default, {
        catalogs: catalogs,
        tagsLocale: tagsLocale,
        editionLevel: editionLevel,
        fetchDocuments: fetchDocuments,
        filteredTags: filteredTags,
        handleOnSubmit: handleOnSubmit,
        handleSelectCategory: function handleSelectCategory(category) {
          return selectCategory(category);
        },
        handleSelectTag: function handleSelectTag(tag) {
          return selectTag(tag, hasProposalSelector);
        },
        hasProposalSelector: hasProposalSelector,
        isForSmartDocuments: isForSmartDocuments,
        isFromCustomTag: isFromCustomTag,
        isInitialazing: isInitialazing,
        selectedCategory: selectedCategory,
        selectedTag: selectedTag
      })]
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  filteredTags: selectors.getFilteredTags,
  isFetchingTags: selectors.getIsFetchingTags,
  selectedCategory: selectors.getSelectedCategory,
  selectedTag: selectors.getSelectedTag,
  tags: selectors.getDataFetchTags
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    fetchTags: function fetchTags() {
      return dispatch(actions.fetchTags());
    },
    filterTags: function filterTags(values) {
      return dispatch(actions.filterTags(values));
    },
    selectCategory: function selectCategory(category) {
      return dispatch(actions.selectCategory(category));
    },
    selectTag: function selectTag(tag, hasProposalSelector) {
      return dispatch(actions.selectTag(tag, hasProposalSelector));
    },
    setContentViewState: function setContentViewState(contentViewState) {
      return dispatch(actions.setContentViewState(contentViewState));
    }
  };
};

LibraryView.propTypes = {
  catalogs: _propTypes.default.array,
  tagsLocale: _propTypes.default.string,
  editionLevel: _propTypes.default.string,
  fetchDocuments: _propTypes.default.func,
  fetchTags: _propTypes.default.func,
  filteredTags: _propTypes.default.object,
  filterTags: _propTypes.default.func,
  handleOnSubmit: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  isFetchingTags: _propTypes.default.bool,
  isForSmartDocuments: _propTypes.default.bool,
  isFromCustomTag: _propTypes.default.bool,
  isInitialazing: _propTypes.default.bool,
  selectCategory: _propTypes.default.func,
  selectedCategory: _propTypes.default.string,
  selectedTag: _propTypes.default.string,
  selectTag: _propTypes.default.func,
  setContentViewState: _propTypes.default.func,
  tags: _propTypes.default.object
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(LibraryView);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvVGFnTGlicmFyeVZpZXcuanMiXSwibmFtZXMiOlsiTGlicmFyeVZpZXciLCJjYXRhbG9ncyIsInRhZ3NMb2NhbGUiLCJmZXRjaERvY3VtZW50cyIsImZldGNoVGFncyIsImZpbHRlcmVkVGFncyIsImZpbHRlclRhZ3MiLCJlZGl0aW9uTGV2ZWwiLCJoYW5kbGVPblN1Ym1pdCIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJpc0ZldGNoaW5nVGFncyIsImlzRm9yU21hcnREb2N1bWVudHMiLCJpc0Zyb21DdXN0b21UYWciLCJpc0luaXRpYWxhemluZyIsInNlbGVjdENhdGVnb3J5Iiwic2VsZWN0ZWRDYXRlZ29yeSIsInNlbGVjdGVkVGFnIiwic2VsZWN0VGFnIiwic2V0Q29udGVudFZpZXdTdGF0ZSIsInRhZ3MiLCJ0Iiwic2VhcmNoVGV4dCIsInNldFNlYXJjaFRleHQiLCJlIiwidGFyZ2V0IiwidmFsdWUiLCJDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSIsImNvdW50IiwiY2F0ZWdvcnkiLCJ0YWciLCJtYXBTdGF0ZVRvUHJvcHMiLCJzZWxlY3RvcnMiLCJnZXRGaWx0ZXJlZFRhZ3MiLCJnZXRJc0ZldGNoaW5nVGFncyIsImdldFNlbGVjdGVkQ2F0ZWdvcnkiLCJnZXRTZWxlY3RlZFRhZyIsImdldERhdGFGZXRjaFRhZ3MiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImFjdGlvbnMiLCJ2YWx1ZXMiLCJjb250ZW50Vmlld1N0YXRlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJzdHJpbmciLCJmdW5jIiwib2JqZWN0IiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxPQW9CZDtBQUFBLE1BbkJGQyxRQW1CRSxRQW5CRkEsUUFtQkU7QUFBQSxNQWxCRkMsVUFrQkUsUUFsQkZBLFVBa0JFO0FBQUEsTUFqQkZDLGNBaUJFLFFBakJGQSxjQWlCRTtBQUFBLE1BaEJGQyxTQWdCRSxRQWhCRkEsU0FnQkU7QUFBQSxNQWZGQyxZQWVFLFFBZkZBLFlBZUU7QUFBQSxNQWRGQyxVQWNFLFFBZEZBLFVBY0U7QUFBQSxNQWJGQyxZQWFFLFFBYkZBLFlBYUU7QUFBQSxNQVpGQyxjQVlFLFFBWkZBLGNBWUU7QUFBQSxNQVhGQyxtQkFXRSxRQVhGQSxtQkFXRTtBQUFBLE1BVkZDLGNBVUUsUUFWRkEsY0FVRTtBQUFBLE1BVEZDLG1CQVNFLFFBVEZBLG1CQVNFO0FBQUEsTUFSRkMsZUFRRSxRQVJGQSxlQVFFO0FBQUEsTUFQRkMsY0FPRSxRQVBGQSxjQU9FO0FBQUEsTUFORkMsY0FNRSxRQU5GQSxjQU1FO0FBQUEsTUFMRkMsZ0JBS0UsUUFMRkEsZ0JBS0U7QUFBQSxNQUpGQyxXQUlFLFFBSkZBLFdBSUU7QUFBQSxNQUhGQyxTQUdFLFFBSEZBLFNBR0U7QUFBQSxNQUZGQyxtQkFFRSxRQUZGQSxtQkFFRTtBQUFBLE1BREZDLElBQ0UsUUFERkEsSUFDRTs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0Esa0JBQW9DLHFCQUFTLEVBQVQsQ0FBcEM7QUFBQTtBQUFBLE1BQU9DLFVBQVA7QUFBQSxNQUFtQkMsYUFBbkI7O0FBRUEsd0JBQVUsWUFBTTtBQUNabEIsSUFBQUEsU0FBUztBQUNaLEdBRkQsRUFFRyxFQUZIO0FBSUEsd0JBQVUsWUFBTTtBQUNaLDZCQUFXZSxJQUFYLEVBQWlCRSxVQUFqQixFQUE2QmYsVUFBN0I7QUFDSCxHQUZELEVBRUcsQ0FBQ2EsSUFBRCxFQUFPRSxVQUFQLENBRkg7QUFJQSxzQkFDSSxxQkFBQywrQkFBRDtBQUF3QixJQUFBLEtBQUssRUFBRVgsY0FBYyxJQUFJRyxjQUFqRDtBQUFBLDJCQUNJO0FBQUEsOEJBQ0ksc0JBQUMsbUJBQUQ7QUFBQSxnQ0FDSSxxQkFBQyxtQkFBRDtBQUFLLFVBQUEsRUFBRSxFQUFDLElBQVI7QUFBQSxpQ0FDSSxxQkFBQyxvQkFBRDtBQUNJLFlBQUEsUUFBUSxFQUFFLGtCQUFDVSxDQUFEO0FBQUEscUJBQU9ELGFBQWEsQ0FBQ0MsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEtBQVYsQ0FBcEI7QUFBQSxhQURkO0FBRUksWUFBQSxXQUFXLEVBQUVMLENBQUMsQ0FBQyxRQUFELENBRmxCO0FBR0ksWUFBQSxLQUFLLEVBQUVDO0FBSFg7QUFESixVQURKLGVBUUkscUJBQUMsbUJBQUQ7QUFBSyxVQUFBLEVBQUUsRUFBQyxHQUFSO0FBQUEsaUNBQ0kscUJBQUMsZUFBRDtBQUNJLFlBQUEsU0FBUyxFQUFDLFNBRGQ7QUFFSSxZQUFBLEtBQUssRUFBQyxTQUZWO0FBR0ksWUFBQSxTQUFTLEVBQUMsU0FIZDtBQUlJLFlBQUEsV0FBVyxFQUFFO0FBQUEscUJBQ1RILG1CQUFtQixDQUFDUSw2QkFBRCxDQURWO0FBQUEsYUFKakI7QUFPSSxZQUFBLElBQUksRUFBQyxhQVBUO0FBUUksWUFBQSxLQUFLLEVBQUVOLENBQUMsQ0FBQyxZQUFELEVBQWU7QUFBRU8sY0FBQUEsS0FBSyxFQUFFO0FBQVQsYUFBZixDQVJaO0FBU0ksWUFBQSxPQUFPLEVBQUMsTUFUWjtBQVVJLFlBQUEsT0FBTyxFQUFFLENBQUNmO0FBVmQ7QUFESixVQVJKO0FBQUEsUUFESixlQXlCSTtBQUFJLFFBQUEsU0FBUyxFQUFDO0FBQWQsUUF6QkosZUEyQkkscUJBQUMsMEJBQUQ7QUFDSSxRQUFBLFFBQVEsRUFBRVgsUUFEZDtBQUVJLFFBQUEsVUFBVSxFQUFFQyxVQUZoQjtBQUdJLFFBQUEsWUFBWSxFQUFFSyxZQUhsQjtBQUlJLFFBQUEsY0FBYyxFQUFFSixjQUpwQjtBQUtJLFFBQUEsWUFBWSxFQUFFRSxZQUxsQjtBQU1JLFFBQUEsY0FBYyxFQUFFRyxjQU5wQjtBQU9JLFFBQUEsb0JBQW9CLEVBQUUsOEJBQUNvQixRQUFEO0FBQUEsaUJBQ2xCZCxjQUFjLENBQUNjLFFBQUQsQ0FESTtBQUFBLFNBUDFCO0FBVUksUUFBQSxlQUFlLEVBQUUseUJBQUNDLEdBQUQ7QUFBQSxpQkFDYlosU0FBUyxDQUFDWSxHQUFELEVBQU1wQixtQkFBTixDQURJO0FBQUEsU0FWckI7QUFhSSxRQUFBLG1CQUFtQixFQUFFQSxtQkFiekI7QUFjSSxRQUFBLG1CQUFtQixFQUFFRSxtQkFkekI7QUFlSSxRQUFBLGVBQWUsRUFBRUMsZUFmckI7QUFnQkksUUFBQSxjQUFjLEVBQUVDLGNBaEJwQjtBQWlCSSxRQUFBLGdCQUFnQixFQUFFRSxnQkFqQnRCO0FBa0JJLFFBQUEsV0FBVyxFQUFFQztBQWxCakIsUUEzQko7QUFBQTtBQURKLElBREo7QUFvREgsQ0FwRkQ7O0FBc0ZBLElBQU1jLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0N6QixFQUFBQSxZQUFZLEVBQUUwQixTQUFTLENBQUNDLGVBRHFCO0FBRTdDdEIsRUFBQUEsY0FBYyxFQUFFcUIsU0FBUyxDQUFDRSxpQkFGbUI7QUFHN0NsQixFQUFBQSxnQkFBZ0IsRUFBRWdCLFNBQVMsQ0FBQ0csbUJBSGlCO0FBSTdDbEIsRUFBQUEsV0FBVyxFQUFFZSxTQUFTLENBQUNJLGNBSnNCO0FBSzdDaEIsRUFBQUEsSUFBSSxFQUFFWSxTQUFTLENBQUNLO0FBTDZCLENBQXpCLENBQXhCOztBQVFBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQWU7QUFDdENsQyxJQUFBQSxTQUFTLEVBQUU7QUFBQSxhQUFNa0MsUUFBUSxDQUFDQyxPQUFPLENBQUNuQyxTQUFSLEVBQUQsQ0FBZDtBQUFBLEtBRDJCO0FBRXRDRSxJQUFBQSxVQUFVLEVBQUUsb0JBQUNrQyxNQUFEO0FBQUEsYUFBWUYsUUFBUSxDQUFDQyxPQUFPLENBQUNqQyxVQUFSLENBQW1Ca0MsTUFBbkIsQ0FBRCxDQUFwQjtBQUFBLEtBRjBCO0FBR3RDMUIsSUFBQUEsY0FBYyxFQUFFLHdCQUFDYyxRQUFEO0FBQUEsYUFBY1UsUUFBUSxDQUFDQyxPQUFPLENBQUN6QixjQUFSLENBQXVCYyxRQUF2QixDQUFELENBQXRCO0FBQUEsS0FIc0I7QUFJdENYLElBQUFBLFNBQVMsRUFBRSxtQkFBQ1ksR0FBRCxFQUFNcEIsbUJBQU47QUFBQSxhQUNQNkIsUUFBUSxDQUFDQyxPQUFPLENBQUN0QixTQUFSLENBQWtCWSxHQUFsQixFQUF1QnBCLG1CQUF2QixDQUFELENBREQ7QUFBQSxLQUoyQjtBQU10Q1MsSUFBQUEsbUJBQW1CLEVBQUUsNkJBQUN1QixnQkFBRDtBQUFBLGFBQ2pCSCxRQUFRLENBQUNDLE9BQU8sQ0FBQ3JCLG1CQUFSLENBQTRCdUIsZ0JBQTVCLENBQUQsQ0FEUztBQUFBO0FBTmlCLEdBQWY7QUFBQSxDQUEzQjs7QUFVQXpDLFdBQVcsQ0FBQzBDLFNBQVosR0FBd0I7QUFDcEJ6QyxFQUFBQSxRQUFRLEVBQUUwQyxtQkFBVUMsS0FEQTtBQUVwQjFDLEVBQUFBLFVBQVUsRUFBRXlDLG1CQUFVRSxNQUZGO0FBR3BCdEMsRUFBQUEsWUFBWSxFQUFFb0MsbUJBQVVFLE1BSEo7QUFJcEIxQyxFQUFBQSxjQUFjLEVBQUV3QyxtQkFBVUcsSUFKTjtBQUtwQjFDLEVBQUFBLFNBQVMsRUFBRXVDLG1CQUFVRyxJQUxEO0FBTXBCekMsRUFBQUEsWUFBWSxFQUFFc0MsbUJBQVVJLE1BTko7QUFPcEJ6QyxFQUFBQSxVQUFVLEVBQUVxQyxtQkFBVUcsSUFQRjtBQVFwQnRDLEVBQUFBLGNBQWMsRUFBRW1DLG1CQUFVRyxJQVJOO0FBU3BCckMsRUFBQUEsbUJBQW1CLEVBQUVrQyxtQkFBVUssSUFUWDtBQVVwQnRDLEVBQUFBLGNBQWMsRUFBRWlDLG1CQUFVSyxJQVZOO0FBV3BCckMsRUFBQUEsbUJBQW1CLEVBQUVnQyxtQkFBVUssSUFYWDtBQVlwQnBDLEVBQUFBLGVBQWUsRUFBRStCLG1CQUFVSyxJQVpQO0FBYXBCbkMsRUFBQUEsY0FBYyxFQUFFOEIsbUJBQVVLLElBYk47QUFjcEJsQyxFQUFBQSxjQUFjLEVBQUU2QixtQkFBVUcsSUFkTjtBQWVwQi9CLEVBQUFBLGdCQUFnQixFQUFFNEIsbUJBQVVFLE1BZlI7QUFnQnBCN0IsRUFBQUEsV0FBVyxFQUFFMkIsbUJBQVVFLE1BaEJIO0FBaUJwQjVCLEVBQUFBLFNBQVMsRUFBRTBCLG1CQUFVRyxJQWpCRDtBQWtCcEI1QixFQUFBQSxtQkFBbUIsRUFBRXlCLG1CQUFVRyxJQWxCWDtBQW1CcEIzQixFQUFBQSxJQUFJLEVBQUV3QixtQkFBVUk7QUFuQkksQ0FBeEI7O2VBc0JlLHlCQUFRakIsZUFBUixFQUF5Qk8sa0JBQXpCLEVBQTZDckMsV0FBN0MsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XG5cbmltcG9ydCBCdXR0b24gZnJvbSAnQGNvbXBvbmVudHMvQnV0dG9uJztcbmltcG9ydCBJbnB1dFNlYXJjaCBmcm9tICdAY29tcG9uZW50cy9JbnB1dFNlYXJjaCc7XG5cbmltcG9ydCB7IENSRUFURV9DVVNUT01fVEFHX1NUQVRFIH0gZnJvbSAnQGNvbnN0YW50cy90YWdzJztcblxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi8uLi9hY3Rpb25zJztcbmltcG9ydCB7IHNlYXJjaFRhZ3MgfSBmcm9tICcuLi8uLi9oZWxwZXJzJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi8uLi9zZWxlY3RvcnMnO1xuXG5pbXBvcnQgUGxhY2Vob2xkZXJMaWJyYXJ5VGFncyBmcm9tICcuL1BsYWNlaG9sZGVyTGlicmFyeVRhZ3MnO1xuaW1wb3J0IFRhZ0xpYnJhcnlDb250ZW50IGZyb20gJy4vVGFnTGlicmFyeUNvbnRlbnQnO1xuXG5jb25zdCBMaWJyYXJ5VmlldyA9ICh7XG4gICAgY2F0YWxvZ3MsXG4gICAgdGFnc0xvY2FsZSxcbiAgICBmZXRjaERvY3VtZW50cyxcbiAgICBmZXRjaFRhZ3MsXG4gICAgZmlsdGVyZWRUYWdzLFxuICAgIGZpbHRlclRhZ3MsXG4gICAgZWRpdGlvbkxldmVsLFxuICAgIGhhbmRsZU9uU3VibWl0LFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgaXNGZXRjaGluZ1RhZ3MsXG4gICAgaXNGb3JTbWFydERvY3VtZW50cyxcbiAgICBpc0Zyb21DdXN0b21UYWcsXG4gICAgaXNJbml0aWFsYXppbmcsXG4gICAgc2VsZWN0Q2F0ZWdvcnksXG4gICAgc2VsZWN0ZWRDYXRlZ29yeSxcbiAgICBzZWxlY3RlZFRhZyxcbiAgICBzZWxlY3RUYWcsXG4gICAgc2V0Q29udGVudFZpZXdTdGF0ZSxcbiAgICB0YWdzLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBbc2VhcmNoVGV4dCwgc2V0U2VhcmNoVGV4dF0gPSB1c2VTdGF0ZSgnJyk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBmZXRjaFRhZ3MoKTtcbiAgICB9LCBbXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBzZWFyY2hUYWdzKHRhZ3MsIHNlYXJjaFRleHQsIGZpbHRlclRhZ3MpO1xuICAgIH0sIFt0YWdzLCBzZWFyY2hUZXh0XSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8UGxhY2Vob2xkZXJMaWJyYXJ5VGFncyByZWFkeT17aXNGZXRjaGluZ1RhZ3MgfHwgaXNJbml0aWFsYXppbmd9PlxuICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICA8Um93PlxuICAgICAgICAgICAgICAgICAgICA8Q29sIHhzPVwiMTRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dFNlYXJjaFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0U2VhcmNoVGV4dChlLnRhcmdldC52YWx1ZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3QoJ1NlYXJjaCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtzZWFyY2hUZXh0fVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibWwtYXV0b1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCIjMkY0REZGXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3Rpb249XCJyZXZlcnNlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0Q29udGVudFZpZXdTdGF0ZShDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbj1cImZhcyBmYS1wbHVzXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQ3JlYXRlIHRhZycsIHsgY291bnQ6IDIgfSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyaWFudD1cImJvbGRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpc2libGU9eyFpc0Zyb21DdXN0b21UYWd9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICA8L1Jvdz5cblxuICAgICAgICAgICAgICAgIDxociBjbGFzc05hbWU9XCJtYi0wXCIgLz5cblxuICAgICAgICAgICAgICAgIDxUYWdMaWJyYXJ5Q29udGVudFxuICAgICAgICAgICAgICAgICAgICBjYXRhbG9ncz17Y2F0YWxvZ3N9XG4gICAgICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGU9e3RhZ3NMb2NhbGV9XG4gICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbD17ZWRpdGlvbkxldmVsfVxuICAgICAgICAgICAgICAgICAgICBmZXRjaERvY3VtZW50cz17ZmV0Y2hEb2N1bWVudHN9XG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcmVkVGFncz17ZmlsdGVyZWRUYWdzfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVPblN1Ym1pdD17aGFuZGxlT25TdWJtaXR9XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVNlbGVjdENhdGVnb3J5PXsoY2F0ZWdvcnkpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RDYXRlZ29yeShjYXRlZ29yeSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVTZWxlY3RUYWc9eyh0YWcpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RUYWcodGFnLCBoYXNQcm9wb3NhbFNlbGVjdG9yKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I9e2hhc1Byb3Bvc2FsU2VsZWN0b3J9XG4gICAgICAgICAgICAgICAgICAgIGlzRm9yU21hcnREb2N1bWVudHM9e2lzRm9yU21hcnREb2N1bWVudHN9XG4gICAgICAgICAgICAgICAgICAgIGlzRnJvbUN1c3RvbVRhZz17aXNGcm9tQ3VzdG9tVGFnfVxuICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZz17aXNJbml0aWFsYXppbmd9XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQ2F0ZWdvcnk9e3NlbGVjdGVkQ2F0ZWdvcnl9XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkVGFnPXtzZWxlY3RlZFRhZ31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC8+XG4gICAgICAgIDwvUGxhY2Vob2xkZXJMaWJyYXJ5VGFncz5cbiAgICApO1xufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcbiAgICBmaWx0ZXJlZFRhZ3M6IHNlbGVjdG9ycy5nZXRGaWx0ZXJlZFRhZ3MsXG4gICAgaXNGZXRjaGluZ1RhZ3M6IHNlbGVjdG9ycy5nZXRJc0ZldGNoaW5nVGFncyxcbiAgICBzZWxlY3RlZENhdGVnb3J5OiBzZWxlY3RvcnMuZ2V0U2VsZWN0ZWRDYXRlZ29yeSxcbiAgICBzZWxlY3RlZFRhZzogc2VsZWN0b3JzLmdldFNlbGVjdGVkVGFnLFxuICAgIHRhZ3M6IHNlbGVjdG9ycy5nZXREYXRhRmV0Y2hUYWdzLFxufSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT4gKHtcbiAgICBmZXRjaFRhZ3M6ICgpID0+IGRpc3BhdGNoKGFjdGlvbnMuZmV0Y2hUYWdzKCkpLFxuICAgIGZpbHRlclRhZ3M6ICh2YWx1ZXMpID0+IGRpc3BhdGNoKGFjdGlvbnMuZmlsdGVyVGFncyh2YWx1ZXMpKSxcbiAgICBzZWxlY3RDYXRlZ29yeTogKGNhdGVnb3J5KSA9PiBkaXNwYXRjaChhY3Rpb25zLnNlbGVjdENhdGVnb3J5KGNhdGVnb3J5KSksXG4gICAgc2VsZWN0VGFnOiAodGFnLCBoYXNQcm9wb3NhbFNlbGVjdG9yKSA9PlxuICAgICAgICBkaXNwYXRjaChhY3Rpb25zLnNlbGVjdFRhZyh0YWcsIGhhc1Byb3Bvc2FsU2VsZWN0b3IpKSxcbiAgICBzZXRDb250ZW50Vmlld1N0YXRlOiAoY29udGVudFZpZXdTdGF0ZSkgPT5cbiAgICAgICAgZGlzcGF0Y2goYWN0aW9ucy5zZXRDb250ZW50Vmlld1N0YXRlKGNvbnRlbnRWaWV3U3RhdGUpKSxcbn0pO1xuXG5MaWJyYXJ5Vmlldy5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICB0YWdzTG9jYWxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBmZXRjaERvY3VtZW50czogUHJvcFR5cGVzLmZ1bmMsXG4gICAgZmV0Y2hUYWdzOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBmaWx0ZXJlZFRhZ3M6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgZmlsdGVyVGFnczogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlT25TdWJtaXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzRmV0Y2hpbmdUYWdzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0ZvclNtYXJ0RG9jdW1lbnRzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0Zyb21DdXN0b21UYWc6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzSW5pdGlhbGF6aW5nOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZWxlY3RDYXRlZ29yeTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2VsZWN0ZWRDYXRlZ29yeTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzZWxlY3RlZFRhZzogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzZWxlY3RUYWc6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldENvbnRlbnRWaWV3U3RhdGU6IFByb3BUeXBlcy5mdW5jLFxuICAgIHRhZ3M6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShMaWJyYXJ5Vmlldyk7XG4iXX0=