"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ButtonLink = _interopRequireDefault(require("../../../../components/ButtonLink"));

var _styledComponents = require("../styledComponents");

var _CategoryList = _interopRequireDefault(require("./CategoryList"));

var _TagForm = _interopRequireDefault(require("./TagForm"));

var _TagList = _interopRequireDefault(require("./TagList"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TagLibraryContent = function TagLibraryContent(_ref) {
  var catalogs = _ref.catalogs,
      tagsLocale = _ref.tagsLocale,
      editionLevel = _ref.editionLevel,
      filteredTags = _ref.filteredTags,
      handleOnSubmit = _ref.handleOnSubmit,
      handleSelectCategory = _ref.handleSelectCategory,
      handleSelectTag = _ref.handleSelectTag,
      hasProposalSelector = _ref.hasProposalSelector,
      isForSmartDocuments = _ref.isForSmartDocuments,
      isFromCustomTag = _ref.isFromCustomTag,
      isInitialazing = _ref.isInitialazing,
      selectedCategory = _ref.selectedCategory,
      selectedTag = _ref.selectedTag;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var tagsDiv = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    tagsDiv.current.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, [selectedCategory]);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
    className: "tags-container",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
      xs: "3",
      className: "pl-0 pr-0 tags-column border-left",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.CategoryTitle, {
        className: "mt-2 p-1 pl-3",
        children: t('Categories')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_CategoryList.default, {
        filteredTags: filteredTags,
        handleSelectCategory: handleSelectCategory,
        isForSmartDocuments: isForSmartDocuments,
        selectedCategory: selectedCategory
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_ButtonLink.default, {
        as: "a",
        className: "mt-2 text-left",
        fontSize: "13px",
        href: "https://academy.sunwise.mx/es/articles/4220938-etiquetas-inteligentes",
        iconConfig: {
          prepend: {
            bgColor: '#FF9A00',
            icon: 'fa fa-eye',
            iconColor: '#FFFFFF',
            size: 'md'
          }
        },
        target: "_blank",
        textColor: "#2F4DFF",
        children: t('Tag manuals')
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      xs: "4",
      className: "pl-0 pr-0 tags-column border-left",
      ref: tagsDiv,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagList.default, {
        filteredTags: filteredTags,
        handleSelectTag: handleSelectTag,
        isFromCustomTag: isFromCustomTag,
        selectedCategory: selectedCategory,
        selectedTag: selectedTag
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      xs: "11",
      className: "tags-column border-left",
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", {
        className: "d-flex flex-column",
        children: [!selectedTag && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          className: "m-0 mt-3",
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledText, {
              children: t('Select a tag to configure')
            })
          })
        }), selectedTag && /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagForm.default, {
          catalogs: catalogs,
          tagsLocale: tagsLocale,
          editionLevel: editionLevel,
          handleOnSubmit: handleOnSubmit,
          hasFilterByActive: true,
          hasProposalSelector: hasProposalSelector,
          isFromCustomTag: isFromCustomTag,
          isInitialazing: isInitialazing,
          isInsertTagMode: true
        })]
      })
    })]
  });
};

TagLibraryContent.propTypes = {
  catalogs: _propTypes.default.array,
  tagsLocale: _propTypes.default.string,
  editionLevel: _propTypes.default.string,
  filteredTags: _propTypes.default.object,
  handleOnSubmit: _propTypes.default.func,
  handleSelectCategory: _propTypes.default.func,
  handleSelectTag: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  isForSmartDocuments: _propTypes.default.bool,
  isFromCustomTag: _propTypes.default.bool,
  isInitialazing: _propTypes.default.bool,
  selectCategory: _propTypes.default.func,
  selectedCategory: _propTypes.default.string,
  selectedTag: _propTypes.default.string,
  selectTag: _propTypes.default.func
};
var _default = TagLibraryContent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvVGFnTGlicmFyeUNvbnRlbnQuanMiXSwibmFtZXMiOlsiVGFnTGlicmFyeUNvbnRlbnQiLCJjYXRhbG9ncyIsInRhZ3NMb2NhbGUiLCJlZGl0aW9uTGV2ZWwiLCJmaWx0ZXJlZFRhZ3MiLCJoYW5kbGVPblN1Ym1pdCIsImhhbmRsZVNlbGVjdENhdGVnb3J5IiwiaGFuZGxlU2VsZWN0VGFnIiwiaGFzUHJvcG9zYWxTZWxlY3RvciIsImlzRm9yU21hcnREb2N1bWVudHMiLCJpc0Zyb21DdXN0b21UYWciLCJpc0luaXRpYWxhemluZyIsInNlbGVjdGVkQ2F0ZWdvcnkiLCJzZWxlY3RlZFRhZyIsInQiLCJ0YWdzRGl2IiwiY3VycmVudCIsInNjcm9sbFRvIiwidG9wIiwibGVmdCIsImJlaGF2aW9yIiwicHJlcGVuZCIsImJnQ29sb3IiLCJpY29uIiwiaWNvbkNvbG9yIiwic2l6ZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5Iiwic3RyaW5nIiwib2JqZWN0IiwiZnVuYyIsImJvb2wiLCJzZWxlY3RDYXRlZ29yeSIsInNlbGVjdFRhZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxJQUFNQSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLE9BY3BCO0FBQUEsTUFiRkMsUUFhRSxRQWJGQSxRQWFFO0FBQUEsTUFaRkMsVUFZRSxRQVpGQSxVQVlFO0FBQUEsTUFYRkMsWUFXRSxRQVhGQSxZQVdFO0FBQUEsTUFWRkMsWUFVRSxRQVZGQSxZQVVFO0FBQUEsTUFURkMsY0FTRSxRQVRGQSxjQVNFO0FBQUEsTUFSRkMsb0JBUUUsUUFSRkEsb0JBUUU7QUFBQSxNQVBGQyxlQU9FLFFBUEZBLGVBT0U7QUFBQSxNQU5GQyxtQkFNRSxRQU5GQSxtQkFNRTtBQUFBLE1BTEZDLG1CQUtFLFFBTEZBLG1CQUtFO0FBQUEsTUFKRkMsZUFJRSxRQUpGQSxlQUlFO0FBQUEsTUFIRkMsY0FHRSxRQUhGQSxjQUdFO0FBQUEsTUFGRkMsZ0JBRUUsUUFGRkEsZ0JBRUU7QUFBQSxNQURGQyxXQUNFLFFBREZBLFdBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLE9BQU8sR0FBRyxtQkFBTyxJQUFQLENBQWhCO0FBRUEsd0JBQVUsWUFBTTtBQUNaQSxJQUFBQSxPQUFPLENBQUNDLE9BQVIsQ0FBZ0JDLFFBQWhCLENBQXlCO0FBQUVDLE1BQUFBLEdBQUcsRUFBRSxDQUFQO0FBQVVDLE1BQUFBLElBQUksRUFBRSxDQUFoQjtBQUFtQkMsTUFBQUEsUUFBUSxFQUFFO0FBQTdCLEtBQXpCO0FBQ0gsR0FGRCxFQUVHLENBQUNSLGdCQUFELENBRkg7QUFJQSxzQkFDSSxzQkFBQyxtQkFBRDtBQUFLLElBQUEsU0FBUyxFQUFDLGdCQUFmO0FBQUEsNEJBQ0ksc0JBQUMsbUJBQUQ7QUFBSyxNQUFBLEVBQUUsRUFBQyxHQUFSO0FBQVksTUFBQSxTQUFTLEVBQUMsbUNBQXRCO0FBQUEsOEJBQ0kscUJBQUMsK0JBQUQ7QUFBZSxRQUFBLFNBQVMsRUFBQyxlQUF6QjtBQUFBLGtCQUNLRSxDQUFDLENBQUMsWUFBRDtBQUROLFFBREosZUFJSSxxQkFBQyxxQkFBRDtBQUNJLFFBQUEsWUFBWSxFQUFFVixZQURsQjtBQUVJLFFBQUEsb0JBQW9CLEVBQUVFLG9CQUYxQjtBQUdJLFFBQUEsbUJBQW1CLEVBQUVHLG1CQUh6QjtBQUlJLFFBQUEsZ0JBQWdCLEVBQUVHO0FBSnRCLFFBSkosZUFVSSxxQkFBQyxtQkFBRDtBQUNJLFFBQUEsRUFBRSxFQUFDLEdBRFA7QUFFSSxRQUFBLFNBQVMsRUFBQyxnQkFGZDtBQUdJLFFBQUEsUUFBUSxFQUFDLE1BSGI7QUFJSSxRQUFBLElBQUksRUFBQyx1RUFKVDtBQUtJLFFBQUEsVUFBVSxFQUFFO0FBQ1JTLFVBQUFBLE9BQU8sRUFBRTtBQUNMQyxZQUFBQSxPQUFPLEVBQUUsU0FESjtBQUVMQyxZQUFBQSxJQUFJLEVBQUUsV0FGRDtBQUdMQyxZQUFBQSxTQUFTLEVBQUUsU0FITjtBQUlMQyxZQUFBQSxJQUFJLEVBQUU7QUFKRDtBQURELFNBTGhCO0FBYUksUUFBQSxNQUFNLEVBQUMsUUFiWDtBQWNJLFFBQUEsU0FBUyxFQUFDLFNBZGQ7QUFBQSxrQkFnQktYLENBQUMsQ0FBQyxhQUFEO0FBaEJOLFFBVko7QUFBQSxNQURKLGVBK0JJLHFCQUFDLG1CQUFEO0FBQ0ksTUFBQSxFQUFFLEVBQUMsR0FEUDtBQUVJLE1BQUEsU0FBUyxFQUFDLG1DQUZkO0FBR0ksTUFBQSxHQUFHLEVBQUVDLE9BSFQ7QUFBQSw2QkFLSSxxQkFBQyxnQkFBRDtBQUNJLFFBQUEsWUFBWSxFQUFFWCxZQURsQjtBQUVJLFFBQUEsZUFBZSxFQUFFRyxlQUZyQjtBQUdJLFFBQUEsZUFBZSxFQUFFRyxlQUhyQjtBQUlJLFFBQUEsZ0JBQWdCLEVBQUVFLGdCQUp0QjtBQUtJLFFBQUEsV0FBVyxFQUFFQztBQUxqQjtBQUxKLE1BL0JKLGVBNkNJLHFCQUFDLG1CQUFEO0FBQUssTUFBQSxFQUFFLEVBQUMsSUFBUjtBQUFhLE1BQUEsU0FBUyxFQUFDLHlCQUF2QjtBQUFBLDZCQUNJO0FBQUssUUFBQSxTQUFTLEVBQUMsb0JBQWY7QUFBQSxtQkFDSyxDQUFDQSxXQUFELGlCQUNHLHFCQUFDLG1CQUFEO0FBQUssVUFBQSxTQUFTLEVBQUMsVUFBZjtBQUFBLGlDQUNJLHFCQUFDLG1CQUFEO0FBQUEsbUNBQ0kscUJBQUMsNEJBQUQ7QUFBQSx3QkFDS0MsQ0FBQyxDQUFDLDJCQUFEO0FBRE47QUFESjtBQURKLFVBRlIsRUFVS0QsV0FBVyxpQkFDUixxQkFBQyxnQkFBRDtBQUNJLFVBQUEsUUFBUSxFQUFFWixRQURkO0FBRUksVUFBQSxVQUFVLEVBQUVDLFVBRmhCO0FBR0ksVUFBQSxZQUFZLEVBQUVDLFlBSGxCO0FBSUksVUFBQSxjQUFjLEVBQUVFLGNBSnBCO0FBS0ksVUFBQSxpQkFBaUIsTUFMckI7QUFNSSxVQUFBLG1CQUFtQixFQUFFRyxtQkFOekI7QUFPSSxVQUFBLGVBQWUsRUFBRUUsZUFQckI7QUFRSSxVQUFBLGNBQWMsRUFBRUMsY0FScEI7QUFTSSxVQUFBLGVBQWU7QUFUbkIsVUFYUjtBQUFBO0FBREosTUE3Q0o7QUFBQSxJQURKO0FBMEVILENBaEdEOztBQWtHQVgsaUJBQWlCLENBQUMwQixTQUFsQixHQUE4QjtBQUMxQnpCLEVBQUFBLFFBQVEsRUFBRTBCLG1CQUFVQyxLQURNO0FBRTFCMUIsRUFBQUEsVUFBVSxFQUFFeUIsbUJBQVVFLE1BRkk7QUFHMUIxQixFQUFBQSxZQUFZLEVBQUV3QixtQkFBVUUsTUFIRTtBQUkxQnpCLEVBQUFBLFlBQVksRUFBRXVCLG1CQUFVRyxNQUpFO0FBSzFCekIsRUFBQUEsY0FBYyxFQUFFc0IsbUJBQVVJLElBTEE7QUFNMUJ6QixFQUFBQSxvQkFBb0IsRUFBRXFCLG1CQUFVSSxJQU5OO0FBTzFCeEIsRUFBQUEsZUFBZSxFQUFFb0IsbUJBQVVJLElBUEQ7QUFRMUJ2QixFQUFBQSxtQkFBbUIsRUFBRW1CLG1CQUFVSyxJQVJMO0FBUzFCdkIsRUFBQUEsbUJBQW1CLEVBQUVrQixtQkFBVUssSUFUTDtBQVUxQnRCLEVBQUFBLGVBQWUsRUFBRWlCLG1CQUFVSyxJQVZEO0FBVzFCckIsRUFBQUEsY0FBYyxFQUFFZ0IsbUJBQVVLLElBWEE7QUFZMUJDLEVBQUFBLGNBQWMsRUFBRU4sbUJBQVVJLElBWkE7QUFhMUJuQixFQUFBQSxnQkFBZ0IsRUFBRWUsbUJBQVVFLE1BYkY7QUFjMUJoQixFQUFBQSxXQUFXLEVBQUVjLG1CQUFVRSxNQWRHO0FBZTFCSyxFQUFBQSxTQUFTLEVBQUVQLG1CQUFVSTtBQWZLLENBQTlCO2VBa0JlL0IsaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VSZWYgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDb2wsIFJvdyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuXG5pbXBvcnQgQnV0dG9uTGluayBmcm9tICdAY29tcG9uZW50cy9CdXR0b25MaW5rJztcblxuaW1wb3J0IHsgQ2F0ZWdvcnlUaXRsZSwgU3R5bGVkVGV4dCB9IGZyb20gJy4uL3N0eWxlZENvbXBvbmVudHMnO1xuXG5pbXBvcnQgQ2F0ZWdvcnlMaXN0IGZyb20gJy4vQ2F0ZWdvcnlMaXN0JztcbmltcG9ydCBUYWdGb3JtIGZyb20gJy4vVGFnRm9ybSc7XG5pbXBvcnQgVGFnTGlzdCBmcm9tICcuL1RhZ0xpc3QnO1xuXG5jb25zdCBUYWdMaWJyYXJ5Q29udGVudCA9ICh7XG4gICAgY2F0YWxvZ3MsXG4gICAgdGFnc0xvY2FsZSxcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgZmlsdGVyZWRUYWdzLFxuICAgIGhhbmRsZU9uU3VibWl0LFxuICAgIGhhbmRsZVNlbGVjdENhdGVnb3J5LFxuICAgIGhhbmRsZVNlbGVjdFRhZyxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yLFxuICAgIGlzRm9yU21hcnREb2N1bWVudHMsXG4gICAgaXNGcm9tQ3VzdG9tVGFnLFxuICAgIGlzSW5pdGlhbGF6aW5nLFxuICAgIHNlbGVjdGVkQ2F0ZWdvcnksXG4gICAgc2VsZWN0ZWRUYWcsXG59KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHRhZ3NEaXYgPSB1c2VSZWYobnVsbCk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICB0YWdzRGl2LmN1cnJlbnQuc2Nyb2xsVG8oeyB0b3A6IDAsIGxlZnQ6IDAsIGJlaGF2aW9yOiAnc21vb3RoJyB9KTtcbiAgICB9LCBbc2VsZWN0ZWRDYXRlZ29yeV0pO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJ0YWdzLWNvbnRhaW5lclwiPlxuICAgICAgICAgICAgPENvbCB4cz1cIjNcIiBjbGFzc05hbWU9XCJwbC0wIHByLTAgdGFncy1jb2x1bW4gYm9yZGVyLWxlZnRcIj5cbiAgICAgICAgICAgICAgICA8Q2F0ZWdvcnlUaXRsZSBjbGFzc05hbWU9XCJtdC0yIHAtMSBwbC0zXCI+XG4gICAgICAgICAgICAgICAgICAgIHt0KCdDYXRlZ29yaWVzJyl9XG4gICAgICAgICAgICAgICAgPC9DYXRlZ29yeVRpdGxlPlxuICAgICAgICAgICAgICAgIDxDYXRlZ29yeUxpc3RcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyZWRUYWdzPXtmaWx0ZXJlZFRhZ3N9XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVNlbGVjdENhdGVnb3J5PXtoYW5kbGVTZWxlY3RDYXRlZ29yeX1cbiAgICAgICAgICAgICAgICAgICAgaXNGb3JTbWFydERvY3VtZW50cz17aXNGb3JTbWFydERvY3VtZW50c31cbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRDYXRlZ29yeT17c2VsZWN0ZWRDYXRlZ29yeX1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxCdXR0b25MaW5rXG4gICAgICAgICAgICAgICAgICAgIGFzPVwiYVwiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm10LTIgdGV4dC1sZWZ0XCJcbiAgICAgICAgICAgICAgICAgICAgZm9udFNpemU9XCIxM3B4XCJcbiAgICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vYWNhZGVteS5zdW53aXNlLm14L2VzL2FydGljbGVzLzQyMjA5MzgtZXRpcXVldGFzLWludGVsaWdlbnRlc1wiXG4gICAgICAgICAgICAgICAgICAgIGljb25Db25maWc9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXBlbmQ6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiZ0NvbG9yOiAnI0ZGOUEwMCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbjogJ2ZhIGZhLWV5ZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWNvbkNvbG9yOiAnI0ZGRkZGRicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZTogJ21kJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgIHRleHRDb2xvcj1cIiMyRjRERkZcIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAge3QoJ1RhZyBtYW51YWxzJyl9XG4gICAgICAgICAgICAgICAgPC9CdXR0b25MaW5rPlxuICAgICAgICAgICAgPC9Db2w+XG5cbiAgICAgICAgICAgIDxDb2xcbiAgICAgICAgICAgICAgICB4cz1cIjRcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBsLTAgcHItMCB0YWdzLWNvbHVtbiBib3JkZXItbGVmdFwiXG4gICAgICAgICAgICAgICAgcmVmPXt0YWdzRGl2fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxUYWdMaXN0XG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcmVkVGFncz17ZmlsdGVyZWRUYWdzfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVTZWxlY3RUYWc9e2hhbmRsZVNlbGVjdFRhZ31cbiAgICAgICAgICAgICAgICAgICAgaXNGcm9tQ3VzdG9tVGFnPXtpc0Zyb21DdXN0b21UYWd9XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkQ2F0ZWdvcnk9e3NlbGVjdGVkQ2F0ZWdvcnl9XG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkVGFnPXtzZWxlY3RlZFRhZ31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9Db2w+XG5cbiAgICAgICAgICAgIDxDb2wgeHM9XCIxMVwiIGNsYXNzTmFtZT1cInRhZ3MtY29sdW1uIGJvcmRlci1sZWZ0XCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWZsZXggZmxleC1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgICAgeyFzZWxlY3RlZFRhZyAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm0tMCBtdC0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZFRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU2VsZWN0IGEgdGFnIHRvIGNvbmZpZ3VyZScpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0eWxlZFRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAge3NlbGVjdGVkVGFnICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUYWdGb3JtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0YWxvZ3M9e2NhdGFsb2dzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGU9e3RhZ3NMb2NhbGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlT25TdWJtaXQ9e2hhbmRsZU9uU3VibWl0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc0ZpbHRlckJ5QWN0aXZlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzUHJvcG9zYWxTZWxlY3Rvcj17aGFzUHJvcG9zYWxTZWxlY3Rvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0Zyb21DdXN0b21UYWc9e2lzRnJvbUN1c3RvbVRhZ31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZz17aXNJbml0aWFsYXppbmd9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNJbnNlcnRUYWdNb2RlXG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgIDwvUm93PlxuICAgICk7XG59O1xuXG5UYWdMaWJyYXJ5Q29udGVudC5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICB0YWdzTG9jYWxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBmaWx0ZXJlZFRhZ3M6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaGFuZGxlT25TdWJtaXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZVNlbGVjdENhdGVnb3J5OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYW5kbGVTZWxlY3RUYWc6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzRm9yU21hcnREb2N1bWVudHM6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzRnJvbUN1c3RvbVRhZzogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNJbml0aWFsYXppbmc6IFByb3BUeXBlcy5ib29sLFxuICAgIHNlbGVjdENhdGVnb3J5OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzZWxlY3RlZENhdGVnb3J5OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlbGVjdGVkVGFnOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlbGVjdFRhZzogUHJvcFR5cGVzLmZ1bmMsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBUYWdMaWJyYXJ5Q29udGVudDtcbiJdfQ==