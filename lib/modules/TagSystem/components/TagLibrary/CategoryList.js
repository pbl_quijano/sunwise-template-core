"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _helpers = require("../../helpers");

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CategoryList = function CategoryList(_ref) {
  var filteredTags = _ref.filteredTags,
      handleSelectCategory = _ref.handleSelectCategory,
      isForSmartDocuments = _ref.isForSmartDocuments,
      selectedCategory = _ref.selectedCategory;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if ((0, _isEmpty.default)(filteredTags)) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
      className: "m-0 mt-3",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledText, {
          children: t('There are no categories to display')
        })
      })
    });
  }

  var categories = (0, _helpers.getFilteredCategories)(filteredTags, isForSmartDocuments);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.CategoryWrapper, {
    children: categories.map(function (category) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.NavItem, {
        className: selectedCategory === category && 'active',
        onClick: function onClick() {
          return handleSelectCategory(category);
        },
        children: filteredTags[category].label
      }, filteredTags[category].label);
    })
  });
};

CategoryList.propTypes = {
  filteredTags: _propTypes.default.object,
  handleSelectCategory: _propTypes.default.func,
  isForSmartDocuments: _propTypes.default.bool,
  selectedCategory: _propTypes.default.string
};
var _default = CategoryList;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvQ2F0ZWdvcnlMaXN0LmpzIl0sIm5hbWVzIjpbIkNhdGVnb3J5TGlzdCIsImZpbHRlcmVkVGFncyIsImhhbmRsZVNlbGVjdENhdGVnb3J5IiwiaXNGb3JTbWFydERvY3VtZW50cyIsInNlbGVjdGVkQ2F0ZWdvcnkiLCJ0IiwiY2F0ZWdvcmllcyIsIm1hcCIsImNhdGVnb3J5IiwibGFiZWwiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJmdW5jIiwiYm9vbCIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOzs7Ozs7QUFFQSxJQUFNQSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxPQUtmO0FBQUEsTUFKRkMsWUFJRSxRQUpGQSxZQUlFO0FBQUEsTUFIRkMsb0JBR0UsUUFIRkEsb0JBR0U7QUFBQSxNQUZGQyxtQkFFRSxRQUZGQSxtQkFFRTtBQUFBLE1BREZDLGdCQUNFLFFBREZBLGdCQUNFOztBQUNGLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxNQUFJLHNCQUFRSixZQUFSLENBQUosRUFBMkI7QUFDdkIsd0JBQ0kscUJBQUMsbUJBQUQ7QUFBSyxNQUFBLFNBQVMsRUFBQyxVQUFmO0FBQUEsNkJBQ0kscUJBQUMsbUJBQUQ7QUFBQSwrQkFDSSxxQkFBQyw0QkFBRDtBQUFBLG9CQUNLSSxDQUFDLENBQUMsb0NBQUQ7QUFETjtBQURKO0FBREosTUFESjtBQVNIOztBQUVELE1BQU1DLFVBQVUsR0FBRyxvQ0FBc0JMLFlBQXRCLEVBQW9DRSxtQkFBcEMsQ0FBbkI7QUFFQSxzQkFDSSxxQkFBQyxpQ0FBRDtBQUFBLGNBQ0tHLFVBQVUsQ0FBQ0MsR0FBWCxDQUFlLFVBQUNDLFFBQUQsRUFBYztBQUMxQiwwQkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsU0FBUyxFQUFFSixnQkFBZ0IsS0FBS0ksUUFBckIsSUFBaUMsUUFEaEQ7QUFHSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUFNTixvQkFBb0IsQ0FBQ00sUUFBRCxDQUExQjtBQUFBLFNBSGI7QUFBQSxrQkFLS1AsWUFBWSxDQUFDTyxRQUFELENBQVosQ0FBdUJDO0FBTDVCLFNBRVNSLFlBQVksQ0FBQ08sUUFBRCxDQUFaLENBQXVCQyxLQUZoQyxDQURKO0FBU0gsS0FWQTtBQURMLElBREo7QUFlSCxDQXBDRDs7QUFzQ0FULFlBQVksQ0FBQ1UsU0FBYixHQUF5QjtBQUNyQlQsRUFBQUEsWUFBWSxFQUFFVSxtQkFBVUMsTUFESDtBQUVyQlYsRUFBQUEsb0JBQW9CLEVBQUVTLG1CQUFVRSxJQUZYO0FBR3JCVixFQUFBQSxtQkFBbUIsRUFBRVEsbUJBQVVHLElBSFY7QUFJckJWLEVBQUFBLGdCQUFnQixFQUFFTyxtQkFBVUk7QUFKUCxDQUF6QjtlQU9lZixZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IENvbCwgUm93IH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCB7IGdldEZpbHRlcmVkQ2F0ZWdvcmllcyB9IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xuaW1wb3J0IHsgQ2F0ZWdvcnlXcmFwcGVyLCBOYXZJdGVtLCBTdHlsZWRUZXh0IH0gZnJvbSAnLi4vc3R5bGVkQ29tcG9uZW50cyc7XG5cbmNvbnN0IENhdGVnb3J5TGlzdCA9ICh7XG4gICAgZmlsdGVyZWRUYWdzLFxuICAgIGhhbmRsZVNlbGVjdENhdGVnb3J5LFxuICAgIGlzRm9yU21hcnREb2N1bWVudHMsXG4gICAgc2VsZWN0ZWRDYXRlZ29yeSxcbn0pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgaWYgKGlzRW1wdHkoZmlsdGVyZWRUYWdzKSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPFJvdyBjbGFzc05hbWU9XCJtLTAgbXQtM1wiPlxuICAgICAgICAgICAgICAgIDxDb2w+XG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZWRUZXh0PlxuICAgICAgICAgICAgICAgICAgICAgICAge3QoJ1RoZXJlIGFyZSBubyBjYXRlZ29yaWVzIHRvIGRpc3BsYXknKX1cbiAgICAgICAgICAgICAgICAgICAgPC9TdHlsZWRUZXh0PlxuICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgY29uc3QgY2F0ZWdvcmllcyA9IGdldEZpbHRlcmVkQ2F0ZWdvcmllcyhmaWx0ZXJlZFRhZ3MsIGlzRm9yU21hcnREb2N1bWVudHMpO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPENhdGVnb3J5V3JhcHBlcj5cbiAgICAgICAgICAgIHtjYXRlZ29yaWVzLm1hcCgoY2F0ZWdvcnkpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICA8TmF2SXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtzZWxlY3RlZENhdGVnb3J5ID09PSBjYXRlZ29yeSAmJiAnYWN0aXZlJ31cbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17ZmlsdGVyZWRUYWdzW2NhdGVnb3J5XS5sYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGhhbmRsZVNlbGVjdENhdGVnb3J5KGNhdGVnb3J5KX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAge2ZpbHRlcmVkVGFnc1tjYXRlZ29yeV0ubGFiZWx9XG4gICAgICAgICAgICAgICAgICAgIDwvTmF2SXRlbT5cbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSl9XG4gICAgICAgIDwvQ2F0ZWdvcnlXcmFwcGVyPlxuICAgICk7XG59O1xuXG5DYXRlZ29yeUxpc3QucHJvcFR5cGVzID0ge1xuICAgIGZpbHRlcmVkVGFnczogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBoYW5kbGVTZWxlY3RDYXRlZ29yeTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaXNGb3JTbWFydERvY3VtZW50czogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2VsZWN0ZWRDYXRlZ29yeTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IENhdGVnb3J5TGlzdDtcbiJdfQ==