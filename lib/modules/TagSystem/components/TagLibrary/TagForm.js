"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _get = _interopRequireDefault(require("lodash/get"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reduxForm = require("redux-form");

var _reselect = require("reselect");

var _ReduxFieldInput = _interopRequireDefault(require("../../../../components/ReduxFieldInput"));

var _ReduxFieldSelect = _interopRequireDefault(require("../../../../components/ReduxFieldSelect"));

var _tags = require("../../../../constants/tags");

var _types = require("../../../../constants/types");

var actions = _interopRequireWildcard(require("../../actions"));

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

var _tagLibraryValidate = _interopRequireDefault(require("../../tagLibraryValidate"));

var _styledComponents = require("../styledComponents");

var _WrapperField = _interopRequireDefault(require("../WrapperField"));

var _FormTitle = _interopRequireDefault(require("./FormTitle"));

var _ProposalSelector = _interopRequireDefault(require("./ProposalSelector"));

var _ResetDefaultValue = _interopRequireDefault(require("./ResetDefaultValue"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SettingsIcon = function SettingsIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-1-1h19.2v19.2H-1z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M14.544 9.384c.032-.256.056-.512.056-.784s-.024-.528-.056-.784l1.688-1.32a.404.404 0 0 0 .096-.512l-1.6-2.768a.4.4 0 0 0-.488-.176l-1.992.8a5.845 5.845 0 0 0-1.352-.784l-.304-2.12A.39.39 0 0 0 10.2.6H7a.39.39 0 0 0-.392.336l-.304 2.12c-.488.2-.936.472-1.352.784l-1.992-.8a.453.453 0 0 0-.144-.024.396.396 0 0 0-.344.2l-1.6 2.768a.394.394 0 0 0 .096.512l1.688 1.32c-.032.256-.056.52-.056.784s.024.528.056.784l-1.688 1.32a.404.404 0 0 0-.096.512l1.6 2.768a.4.4 0 0 0 .488.176l1.992-.8c.416.32.864.584 1.352.784l.304 2.12A.39.39 0 0 0 7 16.6h3.2a.39.39 0 0 0 .392-.336l.304-2.12c.488-.2.936-.472 1.352-.784l1.992.8a.453.453 0 0 0 .144.024.396.396 0 0 0 .344-.2l1.6-2.768a.404.404 0 0 0-.096-.512l-1.688-1.32ZM12.96 8.016c.032.248.04.416.04.584 0 .168-.016.344-.04.584l-.112.904.712.56.864.672-.56.968-1.016-.408-.832-.336-.72.544a4.685 4.685 0 0 1-1 .584l-.848.344-.128.904L9.16 15H8.04l-.152-1.08-.128-.904-.848-.344a4.54 4.54 0 0 1-.984-.568l-.728-.56-.848.344-1.016.408-.56-.968.864-.672.712-.56-.112-.904A6.435 6.435 0 0 1 4.2 8.6c0-.16.016-.344.04-.584l.112-.904-.712-.56-.864-.672.56-.968 1.016.408.832.336.72-.544a4.685 4.685 0 0 1 1-.584l.848-.344.128-.904.16-1.08h1.112l.152 1.08.128.904.848.344c.344.144.664.328.984.568l.728.56.848-.344 1.016-.408.56.968-.856.68-.712.56.112.904ZM8.6 5.4a3.2 3.2 0 1 0 0 6.4 3.2 3.2 0 1 0 0-6.4Zm0 4.8c-.88 0-1.6-.72-1.6-1.6C7 7.72 7.72 7 8.6 7c.88 0 1.6.72 1.6 1.6 0 .88-.72 1.6-1.6 1.6Z",
        fill: "#FF9A00",
        fillRule: "nonzero"
      })]
    })
  }));
};

SettingsIcon.defaultProps = {
  width: "17",
  height: "17",
  xmlns: "http://www.w3.org/2000/svg"
};

var TagForm = function TagForm(_ref) {
  var catalogs = _ref.catalogs,
      changeInput = _ref.changeInput,
      tagsLocale = _ref.tagsLocale,
      currencies = _ref.currencies,
      decimalFormatsData = _ref.decimalFormatsData,
      documents = _ref.documents,
      editionLevel = _ref.editionLevel,
      errors = _ref.errors,
      fetchDocuments = _ref.fetchDocuments,
      formValues = _ref.formValues,
      handleOnSubmit = _ref.handleOnSubmit,
      handleSubmit = _ref.handleSubmit,
      hasFilterByActive = _ref.hasFilterByActive,
      hasProposalSelector = _ref.hasProposalSelector,
      initialValues = _ref.initialValues,
      isFetchingDocuments = _ref.isFetchingDocuments,
      isInsertTagMode = _ref.isInsertTagMode,
      isFromCustomTag = _ref.isFromCustomTag,
      isInitialazing = _ref.isInitialazing,
      tagFormatsData = _ref.tagFormatsData,
      unitFormatsData = _ref.unitFormatsData;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var borderStyle = (0, _helpers.getBorderStyle)(isInsertTagMode, isFromCustomTag);
  var formatType = (0, _get.default)(formValues, 'format_type', '');
  var catalogType = (0, _get.default)(formValues, 'catalog_type', '');

  var isForDocument = _tags.DOCUMENT_TAGS.includes(formatType);

  (0, _react.useEffect)(function () {
    if (!(0, _isEmpty.default)(catalogType)) {
      fetchDocuments(catalogType);
    }
  }, [catalogType]);
  var fieldsDisabled = editionLevel === _types.PARTIAL_EDITION_MODE;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form, {
    onSubmit: handleSubmit(handleOnSubmit),
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.SettingsBox, {
      className: "mt-3",
      style: borderStyle,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_ProposalSelector.default, {
        catalogs: catalogs,
        disabled: fieldsDisabled,
        visible: hasProposalSelector && !_tags.DOCUMENT_TAGS.includes(formatType)
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-2",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_FormTitle.default, {
            formValuesTitle: formValues.title,
            initialValuesTitle: initialValues.title
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.Caption, {
            className: "mt-3",
            children: t('Description')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("p", {
            children: formValues.description
          })]
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.SettingsBox, {
        className: "mt-3",
        style: borderStyle,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
          className: "align-items-center",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.Caption, {
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(SettingsIcon, {
                width: "17",
                height: "17",
                className: "mr-2"
              }), t('Setting', {
                count: 2
              })]
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_ResetDefaultValue.default, {
            defaultValue: formValues.default_value,
            resetToDefaultValue: function resetToDefaultValue() {
              return changeInput('value', formValues.default_value);
            },
            showButtonReset: fieldsDisabled,
            value: formValues.value
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {}), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
          className: "mt-2",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: !isForDocument,
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: (0, _helpers.getColSize)(formatType, 6, 3),
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                className: "mb-0",
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Value')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, _objectSpread({
                  component: _ReduxFieldInput.default,
                  name: "value",
                  type: "string"
                }, (0, _helpers.getPropsValueField)(formatType)))]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: _tags.TAG_CURRENCY_FORMAT_TYPES.includes(formatType),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: "5",
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Currency')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "currency",
                  normalize: (0, _helpers.normalizeCurrencyField)(changeInput, currencies, ['currencyLocale', 'unit', 'symbol']),
                  options: [{
                    label: t('Select currency'),
                    value: '',
                    disabled: true
                  }].concat(_toConsumableArray(currencies.map(function (item) {
                    return {
                      label: item.name,
                      value: item.id
                    };
                  })))
                })]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: isForDocument,
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: "6",
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Catalog type')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "catalog_type",
                  normalize: function normalize(value) {
                    changeInput('smart_tag', '');
                    fetchDocuments(value);
                    return value;
                  },
                  options: (0, _helpers.getUnitsToSelect)(currencies, formatType, isInitialazing, unitFormatsData)
                })]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: isForDocument,
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: "6",
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Document')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "smart_tag",
                  normalize: (0, _helpers.normalizeDocumentField)(changeInput, documents),
                  options: (0, _helpers.getDocumentsToSelect)(documents, hasFilterByActive, isFetchingDocuments)
                })]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: _tags.TAG_NUMBER_FORMAT_TYPES.includes(formatType) || _tags.TAG_PERCENT_FORMAT_TYPES.includes(formatType),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: "5",
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Unit')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "unit",
                  options: (0, _helpers.getUnitsToSelect)(currencies, formatType, isInitialazing, unitFormatsData)
                })]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: _tags.TAG_CURRENCY_FORMAT_TYPES.includes(formatType) || _tags.TAG_ECOLOGIC_TYPES.includes(formatType) || _tags.TAG_NUMBER_FORMAT_TYPES.includes(formatType) || _tags.TAG_PERCENT_FORMAT_TYPES.includes(formatType),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: "5",
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Decimals')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "decimal_type",
                  normalize: (0, _helpers.normalizeDecimalField)(changeInput, decimalFormatsData, 'decimal_places'),
                  options: (0, _helpers.getDecimalFormatsToSelect)(decimalFormatsData, isInitialazing)
                })]
              })
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperField.default, {
            visible: _tags.TAG_TEXT_FORMAT_TYPES.includes(formatType),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
              xs: (0, _helpers.getColSize)(formatType, 6, 5),
              children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
                children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
                  children: t('Format')
                }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
                  component: _ReduxFieldSelect.default,
                  disabled: fieldsDisabled,
                  name: "format",
                  options: (0, _helpers.getTagFormatsToSelect)(isInitialazing, tagFormatsData, formatType)
                })]
              })
            })
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Col, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.Caption, {
              className: "mt-3",
              children: t('Default value')
            }), formValues.value && /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.TagLabel, {
              background: formValues.color,
              children: (0, _helpers.getFormattedValue)(false, tagsLocale, formValues)
            })]
          })
        })]
      }), !(0, _isEmpty.default)(errors) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-3",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledAlert, {
            variant: "warning",
            children: t('You need to capture all fields')
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-3 justify-content-center",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
          xs: "10",
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styledComponents.ButtonInsert, {
            size: "small",
            type: "submit",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
              className: "fas fa-arrow-right mr-2"
            }), (0, _helpers.getTagLibraryTextButton)(isInsertTagMode, isFromCustomTag)]
          })
        })
      })]
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  currencies: selectors.getCurrenciesData,
  decimalFormatsData: selectors.getDecimalFormatsData,
  errors: selectors.getTagLibraryFormErrors,
  documents: selectors.getDocumentsData,
  formValues: selectors.getTagLibraryValues,
  isFetchingDocuments: selectors.getIsFetchingDocuments,
  initialValues: selectors.getInitialTagLibraryValues,
  tagFormatsData: selectors.getTagFormatsData,
  unitFormatsData: selectors.getUnitFormatsData
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    changeInput: function changeInput(field, value) {
      return dispatch((0, _reduxForm.change)('tag-library-form', field, value));
    },
    fetchDocuments: function fetchDocuments(type) {
      return dispatch(actions.fetchDocuments(type));
    }
  };
};

var FormContainer = (0, _reduxForm.reduxForm)({
  enableReinitialize: true,
  form: 'tag-library-form',
  validate: _tagLibraryValidate.default
})(TagForm);
TagForm.propTypes = {
  catalogs: _propTypes.default.array,
  changeInput: _propTypes.default.func,
  tagsLocale: _propTypes.default.string,
  currencies: _propTypes.default.array,
  decimalFormatsData: _propTypes.default.array,
  documents: _propTypes.default.array,
  editionLevel: _propTypes.default.string,
  errors: _propTypes.default.object,
  fetchDocuments: _propTypes.default.func,
  formValues: _propTypes.default.object,
  handleOnSubmit: _propTypes.default.func,
  handleSubmit: _propTypes.default.func,
  hasFilterByActive: _propTypes.default.bool,
  hasProposalSelector: _propTypes.default.bool,
  initializeForm: _propTypes.default.func,
  initialValues: _propTypes.default.object,
  isFetchingCurrencies: _propTypes.default.bool,
  isFetchingDocuments: _propTypes.default.bool,
  isFromCustomTag: _propTypes.default.bool,
  isInitialazing: _propTypes.default.bool,
  isInsertTagMode: _propTypes.default.bool,
  prepareInsert: _propTypes.default.func,
  save: _propTypes.default.func,
  selectedTag: _propTypes.default.object,
  tagFormatsData: _propTypes.default.array,
  unitFormatsData: _propTypes.default.array
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(FormContainer);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvVGFnRm9ybS5qcyJdLCJuYW1lcyI6WyJTZXR0aW5nc0ljb24iLCJUYWdGb3JtIiwiY2F0YWxvZ3MiLCJjaGFuZ2VJbnB1dCIsInRhZ3NMb2NhbGUiLCJjdXJyZW5jaWVzIiwiZGVjaW1hbEZvcm1hdHNEYXRhIiwiZG9jdW1lbnRzIiwiZWRpdGlvbkxldmVsIiwiZXJyb3JzIiwiZmV0Y2hEb2N1bWVudHMiLCJmb3JtVmFsdWVzIiwiaGFuZGxlT25TdWJtaXQiLCJoYW5kbGVTdWJtaXQiLCJoYXNGaWx0ZXJCeUFjdGl2ZSIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJpbml0aWFsVmFsdWVzIiwiaXNGZXRjaGluZ0RvY3VtZW50cyIsImlzSW5zZXJ0VGFnTW9kZSIsImlzRnJvbUN1c3RvbVRhZyIsImlzSW5pdGlhbGF6aW5nIiwidGFnRm9ybWF0c0RhdGEiLCJ1bml0Rm9ybWF0c0RhdGEiLCJ0IiwiYm9yZGVyU3R5bGUiLCJmb3JtYXRUeXBlIiwiY2F0YWxvZ1R5cGUiLCJpc0ZvckRvY3VtZW50IiwiRE9DVU1FTlRfVEFHUyIsImluY2x1ZGVzIiwiZmllbGRzRGlzYWJsZWQiLCJQQVJUSUFMX0VESVRJT05fTU9ERSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJjb3VudCIsImRlZmF1bHRfdmFsdWUiLCJ2YWx1ZSIsIlJlZHV4RmllbGRJbnB1dCIsIlRBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMiLCJSZWR1eEZpZWxkU2VsZWN0IiwibGFiZWwiLCJkaXNhYmxlZCIsIm1hcCIsIml0ZW0iLCJuYW1lIiwiaWQiLCJUQUdfTlVNQkVSX0ZPUk1BVF9UWVBFUyIsIlRBR19QRVJDRU5UX0ZPUk1BVF9UWVBFUyIsIlRBR19FQ09MT0dJQ19UWVBFUyIsIlRBR19URVhUX0ZPUk1BVF9UWVBFUyIsImNvbG9yIiwibWFwU3RhdGVUb1Byb3BzIiwic2VsZWN0b3JzIiwiZ2V0Q3VycmVuY2llc0RhdGEiLCJnZXREZWNpbWFsRm9ybWF0c0RhdGEiLCJnZXRUYWdMaWJyYXJ5Rm9ybUVycm9ycyIsImdldERvY3VtZW50c0RhdGEiLCJnZXRUYWdMaWJyYXJ5VmFsdWVzIiwiZ2V0SXNGZXRjaGluZ0RvY3VtZW50cyIsImdldEluaXRpYWxUYWdMaWJyYXJ5VmFsdWVzIiwiZ2V0VGFnRm9ybWF0c0RhdGEiLCJnZXRVbml0Rm9ybWF0c0RhdGEiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImZpZWxkIiwidHlwZSIsImFjdGlvbnMiLCJGb3JtQ29udGFpbmVyIiwiZW5hYmxlUmVpbml0aWFsaXplIiwiZm9ybSIsInZhbGlkYXRlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJmdW5jIiwic3RyaW5nIiwib2JqZWN0IiwiYm9vbCIsImluaXRpYWxpemVGb3JtIiwiaXNGZXRjaGluZ0N1cnJlbmNpZXMiLCJwcmVwYXJlSW5zZXJ0Iiwic2F2ZSIsInNlbGVjdGVkVGFnIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFHQTs7QUFIQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7QUFRQTs7QUFJQTs7QUFDQTs7QUFjQTs7QUFDQTs7QUFDQTs7QUFTQTs7QUFFQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQWhDMkJBLFksWUFBQUEsWTs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxZOzs7Ozs7QUFrQzNCLElBQU1DLE9BQU8sR0FBRyxTQUFWQSxPQUFVLE9Bc0JWO0FBQUEsTUFyQkZDLFFBcUJFLFFBckJGQSxRQXFCRTtBQUFBLE1BcEJGQyxXQW9CRSxRQXBCRkEsV0FvQkU7QUFBQSxNQW5CRkMsVUFtQkUsUUFuQkZBLFVBbUJFO0FBQUEsTUFsQkZDLFVBa0JFLFFBbEJGQSxVQWtCRTtBQUFBLE1BakJGQyxrQkFpQkUsUUFqQkZBLGtCQWlCRTtBQUFBLE1BaEJGQyxTQWdCRSxRQWhCRkEsU0FnQkU7QUFBQSxNQWZGQyxZQWVFLFFBZkZBLFlBZUU7QUFBQSxNQWRGQyxNQWNFLFFBZEZBLE1BY0U7QUFBQSxNQWJGQyxjQWFFLFFBYkZBLGNBYUU7QUFBQSxNQVpGQyxVQVlFLFFBWkZBLFVBWUU7QUFBQSxNQVhGQyxjQVdFLFFBWEZBLGNBV0U7QUFBQSxNQVZGQyxZQVVFLFFBVkZBLFlBVUU7QUFBQSxNQVRGQyxpQkFTRSxRQVRGQSxpQkFTRTtBQUFBLE1BUkZDLG1CQVFFLFFBUkZBLG1CQVFFO0FBQUEsTUFQRkMsYUFPRSxRQVBGQSxhQU9FO0FBQUEsTUFORkMsbUJBTUUsUUFORkEsbUJBTUU7QUFBQSxNQUxGQyxlQUtFLFFBTEZBLGVBS0U7QUFBQSxNQUpGQyxlQUlFLFFBSkZBLGVBSUU7QUFBQSxNQUhGQyxjQUdFLFFBSEZBLGNBR0U7QUFBQSxNQUZGQyxjQUVFLFFBRkZBLGNBRUU7QUFBQSxNQURGQyxlQUNFLFFBREZBLGVBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLFdBQVcsR0FBRyw2QkFBZU4sZUFBZixFQUFnQ0MsZUFBaEMsQ0FBcEI7QUFDQSxNQUFNTSxVQUFVLEdBQUcsa0JBQUlkLFVBQUosRUFBZ0IsYUFBaEIsRUFBK0IsRUFBL0IsQ0FBbkI7QUFDQSxNQUFNZSxXQUFXLEdBQUcsa0JBQUlmLFVBQUosRUFBZ0IsY0FBaEIsRUFBZ0MsRUFBaEMsQ0FBcEI7O0FBQ0EsTUFBTWdCLGFBQWEsR0FBR0Msb0JBQWNDLFFBQWQsQ0FBdUJKLFVBQXZCLENBQXRCOztBQUVBLHdCQUFVLFlBQU07QUFDWixRQUFJLENBQUMsc0JBQVFDLFdBQVIsQ0FBTCxFQUEyQjtBQUN2QmhCLE1BQUFBLGNBQWMsQ0FBQ2dCLFdBQUQsQ0FBZDtBQUNIO0FBQ0osR0FKRCxFQUlHLENBQUNBLFdBQUQsQ0FKSDtBQU1BLE1BQU1JLGNBQWMsR0FBR3RCLFlBQVksS0FBS3VCLDJCQUF4QztBQUNBLHNCQUNJLHFCQUFDLG9CQUFEO0FBQU0sSUFBQSxRQUFRLEVBQUVsQixZQUFZLENBQUNELGNBQUQsQ0FBNUI7QUFBQSwyQkFDSSxzQkFBQyw2QkFBRDtBQUFhLE1BQUEsU0FBUyxFQUFDLE1BQXZCO0FBQThCLE1BQUEsS0FBSyxFQUFFWSxXQUFyQztBQUFBLDhCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxRQUFRLEVBQUV0QixRQURkO0FBRUksUUFBQSxRQUFRLEVBQUU0QixjQUZkO0FBR0ksUUFBQSxPQUFPLEVBQ0hmLG1CQUFtQixJQUNuQixDQUFDYSxvQkFBY0MsUUFBZCxDQUF1QkosVUFBdkI7QUFMVCxRQURKLGVBU0kscUJBQUMsbUJBQUQ7QUFBSyxRQUFBLFNBQVMsRUFBQyxNQUFmO0FBQUEsK0JBQ0kscUJBQUMsbUJBQUQ7QUFBQSxpQ0FDSSxxQkFBQyxrQkFBRDtBQUNJLFlBQUEsZUFBZSxFQUFFZCxVQUFVLENBQUNxQixLQURoQztBQUVJLFlBQUEsa0JBQWtCLEVBQUVoQixhQUFhLENBQUNnQjtBQUZ0QztBQURKO0FBREosUUFUSixlQWtCSSxxQkFBQyxtQkFBRDtBQUFBLCtCQUNJLHNCQUFDLG1CQUFEO0FBQUEsa0NBQ0kscUJBQUMseUJBQUQ7QUFBUyxZQUFBLFNBQVMsRUFBQyxNQUFuQjtBQUFBLHNCQUEyQlQsQ0FBQyxDQUFDLGFBQUQ7QUFBNUIsWUFESixlQUVJO0FBQUEsc0JBQUlaLFVBQVUsQ0FBQ3NCO0FBQWYsWUFGSjtBQUFBO0FBREosUUFsQkosZUF3Qkksc0JBQUMsNkJBQUQ7QUFBYSxRQUFBLFNBQVMsRUFBQyxNQUF2QjtBQUE4QixRQUFBLEtBQUssRUFBRVQsV0FBckM7QUFBQSxnQ0FDSSxzQkFBQyxtQkFBRDtBQUFLLFVBQUEsU0FBUyxFQUFDLG9CQUFmO0FBQUEsa0NBQ0kscUJBQUMsbUJBQUQ7QUFBQSxtQ0FDSSxzQkFBQyx5QkFBRDtBQUFBLHNDQUNJLHFCQUFDLFlBQUQ7QUFDSSxnQkFBQSxLQUFLLEVBQUMsSUFEVjtBQUVJLGdCQUFBLE1BQU0sRUFBQyxJQUZYO0FBR0ksZ0JBQUEsU0FBUyxFQUFDO0FBSGQsZ0JBREosRUFNS0QsQ0FBQyxDQUFDLFNBQUQsRUFBWTtBQUFFVyxnQkFBQUEsS0FBSyxFQUFFO0FBQVQsZUFBWixDQU5OO0FBQUE7QUFESixZQURKLGVBV0kscUJBQUMsMEJBQUQ7QUFDSSxZQUFBLFlBQVksRUFBRXZCLFVBQVUsQ0FBQ3dCLGFBRDdCO0FBRUksWUFBQSxtQkFBbUIsRUFBRTtBQUFBLHFCQUNqQmhDLFdBQVcsQ0FBQyxPQUFELEVBQVVRLFVBQVUsQ0FBQ3dCLGFBQXJCLENBRE07QUFBQSxhQUZ6QjtBQUtJLFlBQUEsZUFBZSxFQUFFTCxjQUxyQjtBQU1JLFlBQUEsS0FBSyxFQUFFbkIsVUFBVSxDQUFDeUI7QUFOdEIsWUFYSjtBQUFBLFVBREosZUFxQkksOEJBckJKLGVBc0JJLHNCQUFDLG1CQUFEO0FBQUssVUFBQSxTQUFTLEVBQUMsTUFBZjtBQUFBLGtDQUNJLHFCQUFDLHFCQUFEO0FBQWMsWUFBQSxPQUFPLEVBQUUsQ0FBQ1QsYUFBeEI7QUFBQSxtQ0FDSSxxQkFBQyxtQkFBRDtBQUFLLGNBQUEsRUFBRSxFQUFFLHlCQUFXRixVQUFYLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLENBQVQ7QUFBQSxxQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBWSxnQkFBQSxTQUFTLEVBQUMsTUFBdEI7QUFBQSx3Q0FDSSxxQkFBQywyQkFBRDtBQUFBLDRCQUFZRixDQUFDLENBQUMsT0FBRDtBQUFiLGtCQURKLGVBRUkscUJBQUMsNkJBQUQ7QUFDSSxrQkFBQSxTQUFTLEVBQUVjLHdCQURmO0FBRUksa0JBQUEsSUFBSSxFQUFDLE9BRlQ7QUFHSSxrQkFBQSxJQUFJLEVBQUM7QUFIVCxtQkFJUSxpQ0FBbUJaLFVBQW5CLENBSlIsRUFGSjtBQUFBO0FBREo7QUFESixZQURKLGVBZUkscUJBQUMscUJBQUQ7QUFDSSxZQUFBLE9BQU8sRUFBRWEsZ0NBQTBCVCxRQUExQixDQUNMSixVQURLLENBRGI7QUFBQSxtQ0FLSSxxQkFBQyxtQkFBRDtBQUFLLGNBQUEsRUFBRSxFQUFDLEdBQVI7QUFBQSxxQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBQSx3Q0FDSSxxQkFBQywyQkFBRDtBQUFBLDRCQUFZRixDQUFDLENBQUMsVUFBRDtBQUFiLGtCQURKLGVBRUkscUJBQUMsNkJBQUQ7QUFDSSxrQkFBQSxTQUFTLEVBQUVnQix5QkFEZjtBQUVJLGtCQUFBLFFBQVEsRUFBRVQsY0FGZDtBQUdJLGtCQUFBLElBQUksRUFBQyxVQUhUO0FBSUksa0JBQUEsU0FBUyxFQUFFLHFDQUNQM0IsV0FETyxFQUVQRSxVQUZPLEVBR1AsQ0FBQyxnQkFBRCxFQUFtQixNQUFuQixFQUEyQixRQUEzQixDQUhPLENBSmY7QUFTSSxrQkFBQSxPQUFPLEdBQ0g7QUFDSW1DLG9CQUFBQSxLQUFLLEVBQUVqQixDQUFDLENBQUMsaUJBQUQsQ0FEWjtBQUVJYSxvQkFBQUEsS0FBSyxFQUFFLEVBRlg7QUFHSUssb0JBQUFBLFFBQVEsRUFBRTtBQUhkLG1CQURHLDRCQU1BcEMsVUFBVSxDQUFDcUMsR0FBWCxDQUFlLFVBQUNDLElBQUQ7QUFBQSwyQkFBVztBQUN6Qkgsc0JBQUFBLEtBQUssRUFBRUcsSUFBSSxDQUFDQyxJQURhO0FBRXpCUixzQkFBQUEsS0FBSyxFQUFFTyxJQUFJLENBQUNFO0FBRmEscUJBQVg7QUFBQSxtQkFBZixDQU5BO0FBVFgsa0JBRko7QUFBQTtBQURKO0FBTEosWUFmSixlQWdESSxxQkFBQyxxQkFBRDtBQUFjLFlBQUEsT0FBTyxFQUFFbEIsYUFBdkI7QUFBQSxtQ0FDSSxxQkFBQyxtQkFBRDtBQUFLLGNBQUEsRUFBRSxFQUFDLEdBQVI7QUFBQSxxQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBQSx3Q0FDSSxxQkFBQywyQkFBRDtBQUFBLDRCQUFZSixDQUFDLENBQUMsY0FBRDtBQUFiLGtCQURKLGVBRUkscUJBQUMsNkJBQUQ7QUFDSSxrQkFBQSxTQUFTLEVBQUVnQix5QkFEZjtBQUVJLGtCQUFBLFFBQVEsRUFBRVQsY0FGZDtBQUdJLGtCQUFBLElBQUksRUFBQyxjQUhUO0FBSUksa0JBQUEsU0FBUyxFQUFFLG1CQUFDTSxLQUFELEVBQVc7QUFDbEJqQyxvQkFBQUEsV0FBVyxDQUFDLFdBQUQsRUFBYyxFQUFkLENBQVg7QUFDQU8sb0JBQUFBLGNBQWMsQ0FBQzBCLEtBQUQsQ0FBZDtBQUNBLDJCQUFPQSxLQUFQO0FBQ0gsbUJBUkw7QUFTSSxrQkFBQSxPQUFPLEVBQUUsK0JBQ0wvQixVQURLLEVBRUxvQixVQUZLLEVBR0xMLGNBSEssRUFJTEUsZUFKSztBQVRiLGtCQUZKO0FBQUE7QUFESjtBQURKLFlBaERKLGVBd0VJLHFCQUFDLHFCQUFEO0FBQWMsWUFBQSxPQUFPLEVBQUVLLGFBQXZCO0FBQUEsbUNBQ0kscUJBQUMsbUJBQUQ7QUFBSyxjQUFBLEVBQUUsRUFBQyxHQUFSO0FBQUEscUNBQ0ksc0JBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQUEsd0NBQ0kscUJBQUMsMkJBQUQ7QUFBQSw0QkFBWUosQ0FBQyxDQUFDLFVBQUQ7QUFBYixrQkFESixlQUVJLHFCQUFDLDZCQUFEO0FBQ0ksa0JBQUEsU0FBUyxFQUFFZ0IseUJBRGY7QUFFSSxrQkFBQSxRQUFRLEVBQUVULGNBRmQ7QUFHSSxrQkFBQSxJQUFJLEVBQUMsV0FIVDtBQUlJLGtCQUFBLFNBQVMsRUFBRSxxQ0FDUDNCLFdBRE8sRUFFUEksU0FGTyxDQUpmO0FBUUksa0JBQUEsT0FBTyxFQUFFLG1DQUNMQSxTQURLLEVBRUxPLGlCQUZLLEVBR0xHLG1CQUhLO0FBUmIsa0JBRko7QUFBQTtBQURKO0FBREosWUF4RUosZUE4RkkscUJBQUMscUJBQUQ7QUFDSSxZQUFBLE9BQU8sRUFDSDZCLDhCQUF3QmpCLFFBQXhCLENBQWlDSixVQUFqQyxLQUNBc0IsK0JBQXlCbEIsUUFBekIsQ0FBa0NKLFVBQWxDLENBSFI7QUFBQSxtQ0FNSSxxQkFBQyxtQkFBRDtBQUFLLGNBQUEsRUFBRSxFQUFDLEdBQVI7QUFBQSxxQ0FDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBQSx3Q0FDSSxxQkFBQywyQkFBRDtBQUFBLDRCQUFZRixDQUFDLENBQUMsTUFBRDtBQUFiLGtCQURKLGVBRUkscUJBQUMsNkJBQUQ7QUFDSSxrQkFBQSxTQUFTLEVBQUVnQix5QkFEZjtBQUVJLGtCQUFBLFFBQVEsRUFBRVQsY0FGZDtBQUdJLGtCQUFBLElBQUksRUFBQyxNQUhUO0FBSUksa0JBQUEsT0FBTyxFQUFFLCtCQUNMekIsVUFESyxFQUVMb0IsVUFGSyxFQUdMTCxjQUhLLEVBSUxFLGVBSks7QUFKYixrQkFGSjtBQUFBO0FBREo7QUFOSixZQTlGSixlQXNISSxxQkFBQyxxQkFBRDtBQUNJLFlBQUEsT0FBTyxFQUNIZ0IsZ0NBQTBCVCxRQUExQixDQUNJSixVQURKLEtBR0F1Qix5QkFBbUJuQixRQUFuQixDQUE0QkosVUFBNUIsQ0FIQSxJQUlBcUIsOEJBQXdCakIsUUFBeEIsQ0FBaUNKLFVBQWpDLENBSkEsSUFLQXNCLCtCQUF5QmxCLFFBQXpCLENBQWtDSixVQUFsQyxDQVBSO0FBQUEsbUNBVUkscUJBQUMsbUJBQUQ7QUFBSyxjQUFBLEVBQUUsRUFBQyxHQUFSO0FBQUEscUNBQ0ksc0JBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQUEsd0NBQ0kscUJBQUMsMkJBQUQ7QUFBQSw0QkFBWUYsQ0FBQyxDQUFDLFVBQUQ7QUFBYixrQkFESixlQUVJLHFCQUFDLDZCQUFEO0FBQ0ksa0JBQUEsU0FBUyxFQUFFZ0IseUJBRGY7QUFFSSxrQkFBQSxRQUFRLEVBQUVULGNBRmQ7QUFHSSxrQkFBQSxJQUFJLEVBQUMsY0FIVDtBQUlJLGtCQUFBLFNBQVMsRUFBRSxvQ0FDUDNCLFdBRE8sRUFFUEcsa0JBRk8sRUFHUCxnQkFITyxDQUpmO0FBU0ksa0JBQUEsT0FBTyxFQUFFLHdDQUNMQSxrQkFESyxFQUVMYyxjQUZLO0FBVGIsa0JBRko7QUFBQTtBQURKO0FBVkosWUF0SEosZUFxSkkscUJBQUMscUJBQUQ7QUFDSSxZQUFBLE9BQU8sRUFBRTZCLDRCQUFzQnBCLFFBQXRCLENBQStCSixVQUEvQixDQURiO0FBQUEsbUNBR0kscUJBQUMsbUJBQUQ7QUFBSyxjQUFBLEVBQUUsRUFBRSx5QkFBV0EsVUFBWCxFQUF1QixDQUF2QixFQUEwQixDQUExQixDQUFUO0FBQUEscUNBQ0ksc0JBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQUEsd0NBQ0kscUJBQUMsMkJBQUQ7QUFBQSw0QkFBWUYsQ0FBQyxDQUFDLFFBQUQ7QUFBYixrQkFESixlQUVJLHFCQUFDLDZCQUFEO0FBQ0ksa0JBQUEsU0FBUyxFQUFFZ0IseUJBRGY7QUFFSSxrQkFBQSxRQUFRLEVBQUVULGNBRmQ7QUFHSSxrQkFBQSxJQUFJLEVBQUMsUUFIVDtBQUlJLGtCQUFBLE9BQU8sRUFBRSxvQ0FDTFYsY0FESyxFQUVMQyxjQUZLLEVBR0xJLFVBSEs7QUFKYixrQkFGSjtBQUFBO0FBREo7QUFISixZQXJKSjtBQUFBLFVBdEJKLGVBK0xJLHFCQUFDLG1CQUFEO0FBQUEsaUNBQ0ksc0JBQUMsbUJBQUQ7QUFBQSxvQ0FDSSxxQkFBQyx5QkFBRDtBQUFTLGNBQUEsU0FBUyxFQUFDLE1BQW5CO0FBQUEsd0JBQ0tGLENBQUMsQ0FBQyxlQUFEO0FBRE4sY0FESixFQUlLWixVQUFVLENBQUN5QixLQUFYLGlCQUNHLHFCQUFDLDBCQUFEO0FBQVUsY0FBQSxVQUFVLEVBQUV6QixVQUFVLENBQUN1QyxLQUFqQztBQUFBLHdCQUNLLGdDQUNHLEtBREgsRUFFRzlDLFVBRkgsRUFHR08sVUFISDtBQURMLGNBTFI7QUFBQTtBQURKLFVBL0xKO0FBQUEsUUF4QkosRUF5T0ssQ0FBQyxzQkFBUUYsTUFBUixDQUFELGlCQUNHLHFCQUFDLG1CQUFEO0FBQUssUUFBQSxTQUFTLEVBQUMsTUFBZjtBQUFBLCtCQUNJLHFCQUFDLG1CQUFEO0FBQUEsaUNBQ0kscUJBQUMsNkJBQUQ7QUFBYSxZQUFBLE9BQU8sRUFBQyxTQUFyQjtBQUFBLHNCQUNLYyxDQUFDLENBQUMsZ0NBQUQ7QUFETjtBQURKO0FBREosUUExT1IsZUFtUEkscUJBQUMsbUJBQUQ7QUFBSyxRQUFBLFNBQVMsRUFBQyw2QkFBZjtBQUFBLCtCQUNJLHFCQUFDLG1CQUFEO0FBQUssVUFBQSxFQUFFLEVBQUMsSUFBUjtBQUFBLGlDQUNJLHNCQUFDLDhCQUFEO0FBQWMsWUFBQSxJQUFJLEVBQUMsT0FBbkI7QUFBMkIsWUFBQSxJQUFJLEVBQUMsUUFBaEM7QUFBQSxvQ0FDSTtBQUFHLGNBQUEsU0FBUyxFQUFDO0FBQWIsY0FESixFQUVLLHNDQUNHTCxlQURILEVBRUdDLGVBRkgsQ0FGTDtBQUFBO0FBREo7QUFESixRQW5QSjtBQUFBO0FBREosSUFESjtBQW1RSCxDQXZTRDs7QUF5U0EsSUFBTWdDLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0M5QyxFQUFBQSxVQUFVLEVBQUUrQyxTQUFTLENBQUNDLGlCQUR1QjtBQUU3Qy9DLEVBQUFBLGtCQUFrQixFQUFFOEMsU0FBUyxDQUFDRSxxQkFGZTtBQUc3QzdDLEVBQUFBLE1BQU0sRUFBRTJDLFNBQVMsQ0FBQ0csdUJBSDJCO0FBSTdDaEQsRUFBQUEsU0FBUyxFQUFFNkMsU0FBUyxDQUFDSSxnQkFKd0I7QUFLN0M3QyxFQUFBQSxVQUFVLEVBQUV5QyxTQUFTLENBQUNLLG1CQUx1QjtBQU03Q3hDLEVBQUFBLG1CQUFtQixFQUFFbUMsU0FBUyxDQUFDTSxzQkFOYztBQU83QzFDLEVBQUFBLGFBQWEsRUFBRW9DLFNBQVMsQ0FBQ08sMEJBUG9CO0FBUTdDdEMsRUFBQUEsY0FBYyxFQUFFK0IsU0FBUyxDQUFDUSxpQkFSbUI7QUFTN0N0QyxFQUFBQSxlQUFlLEVBQUU4QixTQUFTLENBQUNTO0FBVGtCLENBQXpCLENBQXhCOztBQVlBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQWU7QUFDdEM1RCxJQUFBQSxXQUFXLEVBQUUscUJBQUM2RCxLQUFELEVBQVE1QixLQUFSO0FBQUEsYUFDVDJCLFFBQVEsQ0FBQyx1QkFBTyxrQkFBUCxFQUEyQkMsS0FBM0IsRUFBa0M1QixLQUFsQyxDQUFELENBREM7QUFBQSxLQUR5QjtBQUd0QzFCLElBQUFBLGNBQWMsRUFBRSx3QkFBQ3VELElBQUQ7QUFBQSxhQUFVRixRQUFRLENBQUNHLE9BQU8sQ0FBQ3hELGNBQVIsQ0FBdUJ1RCxJQUF2QixDQUFELENBQWxCO0FBQUE7QUFIc0IsR0FBZjtBQUFBLENBQTNCOztBQU1BLElBQU1FLGFBQWEsR0FBRywwQkFBVTtBQUM1QkMsRUFBQUEsa0JBQWtCLEVBQUUsSUFEUTtBQUU1QkMsRUFBQUEsSUFBSSxFQUFFLGtCQUZzQjtBQUc1QkMsRUFBQUEsUUFBUSxFQUFSQTtBQUg0QixDQUFWLEVBSW5CckUsT0FKbUIsQ0FBdEI7QUFNQUEsT0FBTyxDQUFDc0UsU0FBUixHQUFvQjtBQUNoQnJFLEVBQUFBLFFBQVEsRUFBRXNFLG1CQUFVQyxLQURKO0FBRWhCdEUsRUFBQUEsV0FBVyxFQUFFcUUsbUJBQVVFLElBRlA7QUFHaEJ0RSxFQUFBQSxVQUFVLEVBQUVvRSxtQkFBVUcsTUFITjtBQUloQnRFLEVBQUFBLFVBQVUsRUFBRW1FLG1CQUFVQyxLQUpOO0FBS2hCbkUsRUFBQUEsa0JBQWtCLEVBQUVrRSxtQkFBVUMsS0FMZDtBQU1oQmxFLEVBQUFBLFNBQVMsRUFBRWlFLG1CQUFVQyxLQU5MO0FBT2hCakUsRUFBQUEsWUFBWSxFQUFFZ0UsbUJBQVVHLE1BUFI7QUFRaEJsRSxFQUFBQSxNQUFNLEVBQUUrRCxtQkFBVUksTUFSRjtBQVNoQmxFLEVBQUFBLGNBQWMsRUFBRThELG1CQUFVRSxJQVRWO0FBVWhCL0QsRUFBQUEsVUFBVSxFQUFFNkQsbUJBQVVJLE1BVk47QUFXaEJoRSxFQUFBQSxjQUFjLEVBQUU0RCxtQkFBVUUsSUFYVjtBQVloQjdELEVBQUFBLFlBQVksRUFBRTJELG1CQUFVRSxJQVpSO0FBYWhCNUQsRUFBQUEsaUJBQWlCLEVBQUUwRCxtQkFBVUssSUFiYjtBQWNoQjlELEVBQUFBLG1CQUFtQixFQUFFeUQsbUJBQVVLLElBZGY7QUFlaEJDLEVBQUFBLGNBQWMsRUFBRU4sbUJBQVVFLElBZlY7QUFnQmhCMUQsRUFBQUEsYUFBYSxFQUFFd0QsbUJBQVVJLE1BaEJUO0FBaUJoQkcsRUFBQUEsb0JBQW9CLEVBQUVQLG1CQUFVSyxJQWpCaEI7QUFrQmhCNUQsRUFBQUEsbUJBQW1CLEVBQUV1RCxtQkFBVUssSUFsQmY7QUFtQmhCMUQsRUFBQUEsZUFBZSxFQUFFcUQsbUJBQVVLLElBbkJYO0FBb0JoQnpELEVBQUFBLGNBQWMsRUFBRW9ELG1CQUFVSyxJQXBCVjtBQXFCaEIzRCxFQUFBQSxlQUFlLEVBQUVzRCxtQkFBVUssSUFyQlg7QUFzQmhCRyxFQUFBQSxhQUFhLEVBQUVSLG1CQUFVRSxJQXRCVDtBQXVCaEJPLEVBQUFBLElBQUksRUFBRVQsbUJBQVVFLElBdkJBO0FBd0JoQlEsRUFBQUEsV0FBVyxFQUFFVixtQkFBVUksTUF4QlA7QUF5QmhCdkQsRUFBQUEsY0FBYyxFQUFFbUQsbUJBQVVDLEtBekJWO0FBMEJoQm5ELEVBQUFBLGVBQWUsRUFBRWtELG1CQUFVQztBQTFCWCxDQUFwQjs7ZUE2QmUseUJBQVF0QixlQUFSLEVBQXlCVyxrQkFBekIsRUFBNkNLLGFBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xuaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IENvbCwgRm9ybSwgUm93IH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHsgY2hhbmdlLCByZWR1eEZvcm0gfSBmcm9tICdyZWR1eC1mb3JtJztcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcblxuaW1wb3J0IFJlZHV4RmllbGRJbnB1dCBmcm9tICdAY29tcG9uZW50cy9SZWR1eEZpZWxkSW5wdXQnO1xuaW1wb3J0IFJlZHV4RmllbGRTZWxlY3QgZnJvbSAnQGNvbXBvbmVudHMvUmVkdXhGaWVsZFNlbGVjdCc7XG5cbmltcG9ydCB7XG4gICAgRE9DVU1FTlRfVEFHUyxcbiAgICBUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLFxuICAgIFRBR19FQ09MT0dJQ19UWVBFUyxcbiAgICBUQUdfTlVNQkVSX0ZPUk1BVF9UWVBFUyxcbiAgICBUQUdfUEVSQ0VOVF9GT1JNQVRfVFlQRVMsXG4gICAgVEFHX1RFWFRfRk9STUFUX1RZUEVTLFxufSBmcm9tICdAY29uc3RhbnRzL3RhZ3MnO1xuaW1wb3J0IHsgUEFSVElBTF9FRElUSU9OX01PREUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcblxuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgU2V0dGluZ3NJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9zZXR0aW5ncy5zdmcnO1xuXG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xuaW1wb3J0IHtcbiAgICBnZXRCb3JkZXJTdHlsZSxcbiAgICBnZXRDb2xTaXplLFxuICAgIGdldERlY2ltYWxGb3JtYXRzVG9TZWxlY3QsXG4gICAgZ2V0RG9jdW1lbnRzVG9TZWxlY3QsXG4gICAgZ2V0Rm9ybWF0dGVkVmFsdWUsXG4gICAgZ2V0UHJvcHNWYWx1ZUZpZWxkLFxuICAgIGdldFRhZ0Zvcm1hdHNUb1NlbGVjdCxcbiAgICBnZXRUYWdMaWJyYXJ5VGV4dEJ1dHRvbixcbiAgICBnZXRVbml0c1RvU2VsZWN0LFxuICAgIG5vcm1hbGl6ZUN1cnJlbmN5RmllbGQsXG4gICAgbm9ybWFsaXplRGVjaW1hbEZpZWxkLFxuICAgIG5vcm1hbGl6ZURvY3VtZW50RmllbGQsXG59IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJy4uLy4uL3NlbGVjdG9ycyc7XG5pbXBvcnQgdmFsaWRhdGUgZnJvbSAnLi4vLi4vdGFnTGlicmFyeVZhbGlkYXRlJztcbmltcG9ydCB7XG4gICAgQnV0dG9uSW5zZXJ0LFxuICAgIENhcHRpb24sXG4gICAgRm9ybUxhYmVsLFxuICAgIFNldHRpbmdzQm94LFxuICAgIFN0eWxlZEFsZXJ0LFxuICAgIFN0eWxlZEZpZWxkLFxuICAgIFRhZ0xhYmVsLFxufSBmcm9tICcuLi9zdHlsZWRDb21wb25lbnRzJztcbmltcG9ydCBXcmFwcGVyRmllbGQgZnJvbSAnLi4vV3JhcHBlckZpZWxkJztcblxuaW1wb3J0IEZvcm1UaXRsZSBmcm9tICcuL0Zvcm1UaXRsZSc7XG5pbXBvcnQgUHJvcG9zYWxTZWxlY3RvciBmcm9tICcuL1Byb3Bvc2FsU2VsZWN0b3InO1xuaW1wb3J0IFJlc2V0RGVmYXVsdFZhbHVlIGZyb20gJy4vUmVzZXREZWZhdWx0VmFsdWUnO1xuXG5jb25zdCBUYWdGb3JtID0gKHtcbiAgICBjYXRhbG9ncyxcbiAgICBjaGFuZ2VJbnB1dCxcbiAgICB0YWdzTG9jYWxlLFxuICAgIGN1cnJlbmNpZXMsXG4gICAgZGVjaW1hbEZvcm1hdHNEYXRhLFxuICAgIGRvY3VtZW50cyxcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgZXJyb3JzLFxuICAgIGZldGNoRG9jdW1lbnRzLFxuICAgIGZvcm1WYWx1ZXMsXG4gICAgaGFuZGxlT25TdWJtaXQsXG4gICAgaGFuZGxlU3VibWl0LFxuICAgIGhhc0ZpbHRlckJ5QWN0aXZlLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgaW5pdGlhbFZhbHVlcyxcbiAgICBpc0ZldGNoaW5nRG9jdW1lbnRzLFxuICAgIGlzSW5zZXJ0VGFnTW9kZSxcbiAgICBpc0Zyb21DdXN0b21UYWcsXG4gICAgaXNJbml0aWFsYXppbmcsXG4gICAgdGFnRm9ybWF0c0RhdGEsXG4gICAgdW5pdEZvcm1hdHNEYXRhLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBib3JkZXJTdHlsZSA9IGdldEJvcmRlclN0eWxlKGlzSW5zZXJ0VGFnTW9kZSwgaXNGcm9tQ3VzdG9tVGFnKTtcbiAgICBjb25zdCBmb3JtYXRUeXBlID0gZ2V0KGZvcm1WYWx1ZXMsICdmb3JtYXRfdHlwZScsICcnKTtcbiAgICBjb25zdCBjYXRhbG9nVHlwZSA9IGdldChmb3JtVmFsdWVzLCAnY2F0YWxvZ190eXBlJywgJycpO1xuICAgIGNvbnN0IGlzRm9yRG9jdW1lbnQgPSBET0NVTUVOVF9UQUdTLmluY2x1ZGVzKGZvcm1hdFR5cGUpO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaWYgKCFpc0VtcHR5KGNhdGFsb2dUeXBlKSkge1xuICAgICAgICAgICAgZmV0Y2hEb2N1bWVudHMoY2F0YWxvZ1R5cGUpO1xuICAgICAgICB9XG4gICAgfSwgW2NhdGFsb2dUeXBlXSk7XG5cbiAgICBjb25zdCBmaWVsZHNEaXNhYmxlZCA9IGVkaXRpb25MZXZlbCA9PT0gUEFSVElBTF9FRElUSU9OX01PREU7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZvcm0gb25TdWJtaXQ9e2hhbmRsZVN1Ym1pdChoYW5kbGVPblN1Ym1pdCl9PlxuICAgICAgICAgICAgPFNldHRpbmdzQm94IGNsYXNzTmFtZT1cIm10LTNcIiBzdHlsZT17Ym9yZGVyU3R5bGV9PlxuICAgICAgICAgICAgICAgIDxQcm9wb3NhbFNlbGVjdG9yXG4gICAgICAgICAgICAgICAgICAgIGNhdGFsb2dzPXtjYXRhbG9nc31cbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2ZpZWxkc0Rpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICFET0NVTUVOVF9UQUdTLmluY2x1ZGVzKGZvcm1hdFR5cGUpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtMlwiPlxuICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1UaXRsZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1WYWx1ZXNUaXRsZT17Zm9ybVZhbHVlcy50aXRsZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbml0aWFsVmFsdWVzVGl0bGU9e2luaXRpYWxWYWx1ZXMudGl0bGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICA8L1Jvdz5cblxuICAgICAgICAgICAgICAgIDxSb3c+XG4gICAgICAgICAgICAgICAgICAgIDxDb2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q2FwdGlvbiBjbGFzc05hbWU9XCJtdC0zXCI+e3QoJ0Rlc2NyaXB0aW9uJyl9PC9DYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHA+e2Zvcm1WYWx1ZXMuZGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICA8U2V0dGluZ3NCb3ggY2xhc3NOYW1lPVwibXQtM1wiIHN0eWxlPXtib3JkZXJTdHlsZX0+XG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwiYWxpZ24taXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U2V0dGluZ3NJY29uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjE3XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjE3XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm1yLTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU2V0dGluZycsIHsgY291bnQ6IDIgfSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9DYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8UmVzZXREZWZhdWx0VmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU9e2Zvcm1WYWx1ZXMuZGVmYXVsdF92YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNldFRvRGVmYXVsdFZhbHVlPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2VJbnB1dCgndmFsdWUnLCBmb3JtVmFsdWVzLmRlZmF1bHRfdmFsdWUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3dCdXR0b25SZXNldD17ZmllbGRzRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2Zvcm1WYWx1ZXMudmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICAgICAgPGhyIC8+XG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtMlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXJGaWVsZCB2aXNpYmxlPXshaXNGb3JEb2N1bWVudH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENvbCB4cz17Z2V0Q29sU2l6ZShmb3JtYXRUeXBlLCA2LCAzKX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtTGFiZWw+e3QoJ1ZhbHVlJyl9PC9Gb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVkRmllbGRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwidmFsdWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJzdHJpbmdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXRQcm9wc1ZhbHVlRmllbGQoZm9ybWF0VHlwZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1dyYXBwZXJGaWVsZD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXJGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpc2libGU9e1RBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMuaW5jbHVkZXMoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdFR5cGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI1XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1MYWJlbD57dCgnQ3VycmVuY3knKX08L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudD17UmVkdXhGaWVsZFNlbGVjdH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZmllbGRzRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cImN1cnJlbmN5XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBub3JtYWxpemU9e25vcm1hbGl6ZUN1cnJlbmN5RmllbGQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZUlucHV0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW5jaWVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbJ2N1cnJlbmN5TG9jYWxlJywgJ3VuaXQnLCAnc3ltYm9sJ11cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e1tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IHQoJ1NlbGVjdCBjdXJyZW5jeScpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLmN1cnJlbmNpZXMubWFwKChpdGVtKSA9PiAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGl0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBpdGVtLmlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvV3JhcHBlckZpZWxkPlxuXG4gICAgICAgICAgICAgICAgICAgICAgICA8V3JhcHBlckZpZWxkIHZpc2libGU9e2lzRm9yRG9jdW1lbnR9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI2XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1MYWJlbD57dCgnQ2F0YWxvZyB0eXBlJyl9PC9Gb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVkRmllbGRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRTZWxlY3R9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2ZpZWxkc0Rpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJjYXRhbG9nX3R5cGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vcm1hbGl6ZT17KHZhbHVlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZUlucHV0KCdzbWFydF90YWcnLCAnJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZldGNoRG9jdW1lbnRzKHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17Z2V0VW5pdHNUb1NlbGVjdChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY2llcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0VHlwZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNJbml0aWFsYXppbmcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuaXRGb3JtYXRzRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1dyYXBwZXJGaWVsZD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXJGaWVsZCB2aXNpYmxlPXtpc0ZvckRvY3VtZW50fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHhzPVwiNlwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Hcm91cD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtTGFiZWw+e3QoJ0RvY3VtZW50Jyl9PC9Gb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVkRmllbGRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRTZWxlY3R9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2ZpZWxkc0Rpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJzbWFydF90YWdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5vcm1hbGl6ZT17bm9ybWFsaXplRG9jdW1lbnRGaWVsZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50c1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17Z2V0RG9jdW1lbnRzVG9TZWxlY3QoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50cyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzRmlsdGVyQnlBY3RpdmUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmdEb2N1bWVudHNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9XcmFwcGVyRmllbGQ+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxXcmFwcGVyRmllbGRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmxlPXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVEFHX05VTUJFUl9GT1JNQVRfVFlQRVMuaW5jbHVkZXMoZm9ybWF0VHlwZSkgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgVEFHX1BFUkNFTlRfRk9STUFUX1RZUEVTLmluY2x1ZGVzKGZvcm1hdFR5cGUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCI1XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm1MYWJlbD57dCgnVW5pdCcpfTwvRm9ybUxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50PXtSZWR1eEZpZWxkU2VsZWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtmaWVsZHNEaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwidW5pdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17Z2V0VW5pdHNUb1NlbGVjdChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY2llcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0VHlwZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNJbml0aWFsYXppbmcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuaXRGb3JtYXRzRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1dyYXBwZXJGaWVsZD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXJGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpc2libGU9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLmluY2x1ZGVzKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0VHlwZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFRBR19FQ09MT0dJQ19UWVBFUy5pbmNsdWRlcyhmb3JtYXRUeXBlKSB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBUQUdfTlVNQkVSX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyhmb3JtYXRUeXBlKSB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBUQUdfUEVSQ0VOVF9GT1JNQVRfVFlQRVMuaW5jbHVkZXMoZm9ybWF0VHlwZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPENvbCB4cz1cIjVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdEZWNpbWFscycpfTwvRm9ybUxhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29tcG9uZW50PXtSZWR1eEZpZWxkU2VsZWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtmaWVsZHNEaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwiZGVjaW1hbF90eXBlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBub3JtYWxpemU9e25vcm1hbGl6ZURlY2ltYWxGaWVsZChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhbmdlSW5wdXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlY2ltYWxGb3JtYXRzRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2RlY2ltYWxfcGxhY2VzJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17Z2V0RGVjaW1hbEZvcm1hdHNUb1NlbGVjdChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVjaW1hbEZvcm1hdHNEYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1dyYXBwZXJGaWVsZD5cblxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXJGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpc2libGU9e1RBR19URVhUX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyhmb3JtYXRUeXBlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sIHhzPXtnZXRDb2xTaXplKGZvcm1hdFR5cGUsIDYsIDUpfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uR3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9ybUxhYmVsPnt0KCdGb3JtYXQnKX08L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRGaWVsZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudD17UmVkdXhGaWVsZFNlbGVjdH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZmllbGRzRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cImZvcm1hdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucz17Z2V0VGFnRm9ybWF0c1RvU2VsZWN0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0luaXRpYWxhemluZyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFnRm9ybWF0c0RhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdFR5cGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9XcmFwcGVyRmllbGQ+XG4gICAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgICAgICA8Um93PlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q2FwdGlvbiBjbGFzc05hbWU9XCJtdC0zXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0KCdEZWZhdWx0IHZhbHVlJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9DYXB0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtmb3JtVmFsdWVzLnZhbHVlICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRhZ0xhYmVsIGJhY2tncm91bmQ9e2Zvcm1WYWx1ZXMuY29sb3J9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2dldEZvcm1hdHRlZFZhbHVlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9ybVZhbHVlc1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9UYWdMYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgIDwvU2V0dGluZ3NCb3g+XG5cbiAgICAgICAgICAgICAgICB7IWlzRW1wdHkoZXJyb3JzKSAmJiAoXG4gICAgICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVkQWxlcnQgdmFyaWFudD1cIndhcm5pbmdcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3QoJ1lvdSBuZWVkIHRvIGNhcHR1cmUgYWxsIGZpZWxkcycpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVkQWxlcnQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICAgICAgKX1cblxuICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtMyBqdXN0aWZ5LWNvbnRlbnQtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgIDxDb2wgeHM9XCIxMFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbkluc2VydCBzaXplPVwic21hbGxcIiB0eXBlPVwic3VibWl0XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWFycm93LXJpZ2h0IG1yLTJcIj48L2k+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge2dldFRhZ0xpYnJhcnlUZXh0QnV0dG9uKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0luc2VydFRhZ01vZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzRnJvbUN1c3RvbVRhZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbkluc2VydD5cbiAgICAgICAgICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICA8L1NldHRpbmdzQm94PlxuICAgICAgICA8L0Zvcm0+XG4gICAgKTtcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3Rvcih7XG4gICAgY3VycmVuY2llczogc2VsZWN0b3JzLmdldEN1cnJlbmNpZXNEYXRhLFxuICAgIGRlY2ltYWxGb3JtYXRzRGF0YTogc2VsZWN0b3JzLmdldERlY2ltYWxGb3JtYXRzRGF0YSxcbiAgICBlcnJvcnM6IHNlbGVjdG9ycy5nZXRUYWdMaWJyYXJ5Rm9ybUVycm9ycyxcbiAgICBkb2N1bWVudHM6IHNlbGVjdG9ycy5nZXREb2N1bWVudHNEYXRhLFxuICAgIGZvcm1WYWx1ZXM6IHNlbGVjdG9ycy5nZXRUYWdMaWJyYXJ5VmFsdWVzLFxuICAgIGlzRmV0Y2hpbmdEb2N1bWVudHM6IHNlbGVjdG9ycy5nZXRJc0ZldGNoaW5nRG9jdW1lbnRzLFxuICAgIGluaXRpYWxWYWx1ZXM6IHNlbGVjdG9ycy5nZXRJbml0aWFsVGFnTGlicmFyeVZhbHVlcyxcbiAgICB0YWdGb3JtYXRzRGF0YTogc2VsZWN0b3JzLmdldFRhZ0Zvcm1hdHNEYXRhLFxuICAgIHVuaXRGb3JtYXRzRGF0YTogc2VsZWN0b3JzLmdldFVuaXRGb3JtYXRzRGF0YSxcbn0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+ICh7XG4gICAgY2hhbmdlSW5wdXQ6IChmaWVsZCwgdmFsdWUpID0+XG4gICAgICAgIGRpc3BhdGNoKGNoYW5nZSgndGFnLWxpYnJhcnktZm9ybScsIGZpZWxkLCB2YWx1ZSkpLFxuICAgIGZldGNoRG9jdW1lbnRzOiAodHlwZSkgPT4gZGlzcGF0Y2goYWN0aW9ucy5mZXRjaERvY3VtZW50cyh0eXBlKSksXG59KTtcblxuY29uc3QgRm9ybUNvbnRhaW5lciA9IHJlZHV4Rm9ybSh7XG4gICAgZW5hYmxlUmVpbml0aWFsaXplOiB0cnVlLFxuICAgIGZvcm06ICd0YWctbGlicmFyeS1mb3JtJyxcbiAgICB2YWxpZGF0ZSxcbn0pKFRhZ0Zvcm0pO1xuXG5UYWdGb3JtLnByb3BUeXBlcyA9IHtcbiAgICBjYXRhbG9nczogUHJvcFR5cGVzLmFycmF5LFxuICAgIGNoYW5nZUlucHV0OiBQcm9wVHlwZXMuZnVuYyxcbiAgICB0YWdzTG9jYWxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGN1cnJlbmNpZXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBkZWNpbWFsRm9ybWF0c0RhdGE6IFByb3BUeXBlcy5hcnJheSxcbiAgICBkb2N1bWVudHM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZXJyb3JzOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGZldGNoRG9jdW1lbnRzOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBmb3JtVmFsdWVzOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGhhbmRsZU9uU3VibWl0OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYW5kbGVTdWJtaXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhc0ZpbHRlckJ5QWN0aXZlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpbml0aWFsaXplRm9ybTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaW5pdGlhbFZhbHVlczogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBpc0ZldGNoaW5nQ3VycmVuY2llczogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNGZXRjaGluZ0RvY3VtZW50czogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNGcm9tQ3VzdG9tVGFnOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0luaXRpYWxhemluZzogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNJbnNlcnRUYWdNb2RlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBwcmVwYXJlSW5zZXJ0OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzYXZlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzZWxlY3RlZFRhZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB0YWdGb3JtYXRzRGF0YTogUHJvcFR5cGVzLmFycmF5LFxuICAgIHVuaXRGb3JtYXRzRGF0YTogUHJvcFR5cGVzLmFycmF5LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoRm9ybUNvbnRhaW5lcik7XG4iXX0=