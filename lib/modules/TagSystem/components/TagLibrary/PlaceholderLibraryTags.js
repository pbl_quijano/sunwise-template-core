"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactContentLoader = _interopRequireDefault(require("react-content-loader"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Placeholder = /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
  children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
    className: "m-0 mb-3",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      md: "14",
      lg: "14",
      className: "d-flex",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactContentLoader.default, {
        height: 20
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      md: "4",
      lg: "4",
      className: "d-flex",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactContentLoader.default, {
        height: 20
      })
    })]
  }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Row, {
    className: "m-0 mb-3",
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      md: "3",
      lg: "3",
      className: "d-flex",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactContentLoader.default, {
        height: 100
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      md: "4",
      lg: "4",
      className: "d-flex",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactContentLoader.default, {
        height: 100
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      md: "11",
      lg: "11",
      className: "d-flex",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactContentLoader.default, {
        height: 100
      })
    })]
  })]
});

var PlaceholderLibraryTags = function PlaceholderLibraryTags(_ref) {
  var children = _ref.children,
      ready = _ref.ready;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_jsxRuntime.Fragment, {
    children: ready ? Placeholder : children
  });
};

PlaceholderLibraryTags.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  ready: _propTypes.default.bool
};
var _default = PlaceholderLibraryTags;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvUGxhY2Vob2xkZXJMaWJyYXJ5VGFncy5qcyJdLCJuYW1lcyI6WyJQbGFjZWhvbGRlciIsIlBsYWNlaG9sZGVyTGlicmFyeVRhZ3MiLCJjaGlsZHJlbiIsInJlYWR5IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib25lT2ZUeXBlIiwib2JqZWN0IiwiYXJyYXkiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7Ozs7OztBQUVBLElBQU1BLFdBQVcsZ0JBQ2I7QUFBQSwwQkFDSSxzQkFBQyxtQkFBRDtBQUFLLElBQUEsU0FBUyxFQUFDLFVBQWY7QUFBQSw0QkFDSSxxQkFBQyxtQkFBRDtBQUFLLE1BQUEsRUFBRSxFQUFDLElBQVI7QUFBYSxNQUFBLEVBQUUsRUFBQyxJQUFoQjtBQUFxQixNQUFBLFNBQVMsRUFBQyxRQUEvQjtBQUFBLDZCQUNJLHFCQUFDLDJCQUFEO0FBQWUsUUFBQSxNQUFNLEVBQUU7QUFBdkI7QUFESixNQURKLGVBSUkscUJBQUMsbUJBQUQ7QUFBSyxNQUFBLEVBQUUsRUFBQyxHQUFSO0FBQVksTUFBQSxFQUFFLEVBQUMsR0FBZjtBQUFtQixNQUFBLFNBQVMsRUFBQyxRQUE3QjtBQUFBLDZCQUNJLHFCQUFDLDJCQUFEO0FBQWUsUUFBQSxNQUFNLEVBQUU7QUFBdkI7QUFESixNQUpKO0FBQUEsSUFESixlQVNJLHNCQUFDLG1CQUFEO0FBQUssSUFBQSxTQUFTLEVBQUMsVUFBZjtBQUFBLDRCQUNJLHFCQUFDLG1CQUFEO0FBQUssTUFBQSxFQUFFLEVBQUMsR0FBUjtBQUFZLE1BQUEsRUFBRSxFQUFDLEdBQWY7QUFBbUIsTUFBQSxTQUFTLEVBQUMsUUFBN0I7QUFBQSw2QkFDSSxxQkFBQywyQkFBRDtBQUFlLFFBQUEsTUFBTSxFQUFFO0FBQXZCO0FBREosTUFESixlQUlJLHFCQUFDLG1CQUFEO0FBQUssTUFBQSxFQUFFLEVBQUMsR0FBUjtBQUFZLE1BQUEsRUFBRSxFQUFDLEdBQWY7QUFBbUIsTUFBQSxTQUFTLEVBQUMsUUFBN0I7QUFBQSw2QkFDSSxxQkFBQywyQkFBRDtBQUFlLFFBQUEsTUFBTSxFQUFFO0FBQXZCO0FBREosTUFKSixlQU9JLHFCQUFDLG1CQUFEO0FBQUssTUFBQSxFQUFFLEVBQUMsSUFBUjtBQUFhLE1BQUEsRUFBRSxFQUFDLElBQWhCO0FBQXFCLE1BQUEsU0FBUyxFQUFDLFFBQS9CO0FBQUEsNkJBQ0kscUJBQUMsMkJBQUQ7QUFBZSxRQUFBLE1BQU0sRUFBRTtBQUF2QjtBQURKLE1BUEo7QUFBQSxJQVRKO0FBQUEsRUFESjs7QUF3QkEsSUFBTUMsc0JBQXNCLEdBQUcsU0FBekJBLHNCQUF5QixPQUF5QjtBQUFBLE1BQXRCQyxRQUFzQixRQUF0QkEsUUFBc0I7QUFBQSxNQUFaQyxLQUFZLFFBQVpBLEtBQVk7QUFDcEQsc0JBQU87QUFBQSxjQUFHQSxLQUFLLEdBQUdILFdBQUgsR0FBaUJFO0FBQXpCLElBQVA7QUFDSCxDQUZEOztBQUlBRCxzQkFBc0IsQ0FBQ0csU0FBdkIsR0FBbUM7QUFDL0JGLEVBQUFBLFFBQVEsRUFBRUcsbUJBQVVDLFNBQVYsQ0FBb0IsQ0FBQ0QsbUJBQVVFLE1BQVgsRUFBbUJGLG1CQUFVRyxLQUE3QixDQUFwQixDQURxQjtBQUUvQkwsRUFBQUEsS0FBSyxFQUFFRSxtQkFBVUk7QUFGYyxDQUFuQztlQUtlUixzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBDb2wsIFJvdyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgQ29udGVudExvYWRlciBmcm9tICdyZWFjdC1jb250ZW50LWxvYWRlcic7XG5cbmNvbnN0IFBsYWNlaG9sZGVyID0gKFxuICAgIDw+XG4gICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibS0wIG1iLTNcIj5cbiAgICAgICAgICAgIDxDb2wgbWQ9XCIxNFwiIGxnPVwiMTRcIiBjbGFzc05hbWU9XCJkLWZsZXhcIj5cbiAgICAgICAgICAgICAgICA8Q29udGVudExvYWRlciBoZWlnaHQ9ezIwfSAvPlxuICAgICAgICAgICAgPC9Db2w+XG4gICAgICAgICAgICA8Q29sIG1kPVwiNFwiIGxnPVwiNFwiIGNsYXNzTmFtZT1cImQtZmxleFwiPlxuICAgICAgICAgICAgICAgIDxDb250ZW50TG9hZGVyIGhlaWdodD17MjB9IC8+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG4gICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibS0wIG1iLTNcIj5cbiAgICAgICAgICAgIDxDb2wgbWQ9XCIzXCIgbGc9XCIzXCIgY2xhc3NOYW1lPVwiZC1mbGV4XCI+XG4gICAgICAgICAgICAgICAgPENvbnRlbnRMb2FkZXIgaGVpZ2h0PXsxMDB9IC8+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgbWQ9XCI0XCIgbGc9XCI0XCIgY2xhc3NOYW1lPVwiZC1mbGV4XCI+XG4gICAgICAgICAgICAgICAgPENvbnRlbnRMb2FkZXIgaGVpZ2h0PXsxMDB9IC8+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDxDb2wgbWQ9XCIxMVwiIGxnPVwiMTFcIiBjbGFzc05hbWU9XCJkLWZsZXhcIj5cbiAgICAgICAgICAgICAgICA8Q29udGVudExvYWRlciBoZWlnaHQ9ezEwMH0gLz5cbiAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICA8L1Jvdz5cbiAgICA8Lz5cbik7XG5cbmNvbnN0IFBsYWNlaG9sZGVyTGlicmFyeVRhZ3MgPSAoeyBjaGlsZHJlbiwgcmVhZHkgfSkgPT4ge1xuICAgIHJldHVybiA8PntyZWFkeSA/IFBsYWNlaG9sZGVyIDogY2hpbGRyZW59PC8+O1xufTtcblxuUGxhY2Vob2xkZXJMaWJyYXJ5VGFncy5wcm9wVHlwZXMgPSB7XG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5vYmplY3QsIFByb3BUeXBlcy5hcnJheV0pLFxuICAgIHJlYWR5OiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFBsYWNlaG9sZGVyTGlicmFyeVRhZ3M7XG4iXX0=