"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ReduxFieldSelect = _interopRequireDefault(require("../../../../components/ReduxFieldSelect"));

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var ProposalDataFrom = function ProposalDataFrom() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      disabled = _ref.disabled,
      visible = _ref.visible;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if (!visible) return null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
        className: "mb-0",
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.FormLabel, {
          children: t('What proposal is the data taken from?')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledField, {
          component: _ReduxFieldSelect.default,
          disabled: disabled,
          name: "proposal_number",
          options: [{
            label: t('Select proposal'),
            value: '',
            disabled: true
          }].concat(_toConsumableArray(catalogs.map(function (item) {
            return {
              label: item.name,
              value: item.id
            };
          })))
        })]
      })
    })
  });
};

ProposalDataFrom.propTypes = {
  catalogs: _propTypes.default.array,
  disabled: _propTypes.default.bool,
  visible: _propTypes.default.bool
};
var _default = ProposalDataFrom;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvUHJvcG9zYWxTZWxlY3Rvci5qcyJdLCJuYW1lcyI6WyJQcm9wb3NhbERhdGFGcm9tIiwiY2F0YWxvZ3MiLCJkaXNhYmxlZCIsInZpc2libGUiLCJ0IiwiUmVkdXhGaWVsZFNlbGVjdCIsImxhYmVsIiwidmFsdWUiLCJtYXAiLCJpdGVtIiwibmFtZSIsImlkIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBMEM7QUFBQSxpRkFBUCxFQUFPO0FBQUEsTUFBdkNDLFFBQXVDLFFBQXZDQSxRQUF1QztBQUFBLE1BQTdCQyxRQUE2QixRQUE3QkEsUUFBNkI7QUFBQSxNQUFuQkMsT0FBbUIsUUFBbkJBLE9BQW1COztBQUMvRCx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBRUEsTUFBSSxDQUFDRCxPQUFMLEVBQWMsT0FBTyxJQUFQO0FBRWQsc0JBQ0kscUJBQUMsbUJBQUQ7QUFBQSwyQkFDSSxxQkFBQyxtQkFBRDtBQUFBLDZCQUNJLHNCQUFDLG9CQUFELENBQU0sS0FBTjtBQUFZLFFBQUEsU0FBUyxFQUFDLE1BQXRCO0FBQUEsZ0NBQ0kscUJBQUMsMkJBQUQ7QUFBQSxvQkFDS0MsQ0FBQyxDQUFDLHVDQUFEO0FBRE4sVUFESixlQUlJLHFCQUFDLDZCQUFEO0FBQ0ksVUFBQSxTQUFTLEVBQUVDLHlCQURmO0FBRUksVUFBQSxRQUFRLEVBQUVILFFBRmQ7QUFHSSxVQUFBLElBQUksRUFBQyxpQkFIVDtBQUlJLFVBQUEsT0FBTyxHQUNIO0FBQ0lJLFlBQUFBLEtBQUssRUFBRUYsQ0FBQyxDQUFDLGlCQUFELENBRFo7QUFFSUcsWUFBQUEsS0FBSyxFQUFFLEVBRlg7QUFHSUwsWUFBQUEsUUFBUSxFQUFFO0FBSGQsV0FERyw0QkFNQUQsUUFBUSxDQUFDTyxHQUFULENBQWEsVUFBQ0MsSUFBRDtBQUFBLG1CQUFXO0FBQ3ZCSCxjQUFBQSxLQUFLLEVBQUVHLElBQUksQ0FBQ0MsSUFEVztBQUV2QkgsY0FBQUEsS0FBSyxFQUFFRSxJQUFJLENBQUNFO0FBRlcsYUFBWDtBQUFBLFdBQWIsQ0FOQTtBQUpYLFVBSko7QUFBQTtBQURKO0FBREosSUFESjtBQTJCSCxDQWhDRDs7QUFrQ0FYLGdCQUFnQixDQUFDWSxTQUFqQixHQUE2QjtBQUN6QlgsRUFBQUEsUUFBUSxFQUFFWSxtQkFBVUMsS0FESztBQUV6QlosRUFBQUEsUUFBUSxFQUFFVyxtQkFBVUUsSUFGSztBQUd6QlosRUFBQUEsT0FBTyxFQUFFVSxtQkFBVUU7QUFITSxDQUE3QjtlQU1lZixnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBDb2wsIEZvcm0sIFJvdyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuXG5pbXBvcnQgUmVkdXhGaWVsZFNlbGVjdCBmcm9tICdAY29tcG9uZW50cy9SZWR1eEZpZWxkU2VsZWN0JztcblxuaW1wb3J0IHsgRm9ybUxhYmVsLCBTdHlsZWRGaWVsZCB9IGZyb20gJy4uL3N0eWxlZENvbXBvbmVudHMnO1xuXG5jb25zdCBQcm9wb3NhbERhdGFGcm9tID0gKHsgY2F0YWxvZ3MsIGRpc2FibGVkLCB2aXNpYmxlIH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcblxuICAgIGlmICghdmlzaWJsZSkgcmV0dXJuIG51bGw7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Um93PlxuICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICA8Rm9ybS5Hcm91cCBjbGFzc05hbWU9XCJtYi0wXCI+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgICAgICB7dCgnV2hhdCBwcm9wb3NhbCBpcyB0aGUgZGF0YSB0YWtlbiBmcm9tPycpfVxuICAgICAgICAgICAgICAgICAgICA8L0Zvcm1MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZpZWxkXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21wb25lbnQ9e1JlZHV4RmllbGRTZWxlY3R9XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwicHJvcG9zYWxfbnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM9e1tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiB0KCdTZWxlY3QgcHJvcG9zYWwnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLmNhdGFsb2dzLm1hcCgoaXRlbSkgPT4gKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGl0ZW0ubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGl0ZW0uaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkpLFxuICAgICAgICAgICAgICAgICAgICAgICAgXX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgPC9Sb3c+XG4gICAgKTtcbn07XG5cblByb3Bvc2FsRGF0YUZyb20ucHJvcFR5cGVzID0ge1xuICAgIGNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHZpc2libGU6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgUHJvcG9zYWxEYXRhRnJvbTtcbiJdfQ==