"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ReduxFieldInput = _interopRequireDefault(require("../../../../components/ReduxFieldInput"));

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FormTitle = function FormTitle(_ref) {
  var formValuesTitle = _ref.formValuesTitle,
      initialValuesTitle = _ref.initialValuesTitle;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if ((0, _isEmpty.default)(initialValuesTitle)) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Group, {
      className: "mb-0",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledTitleField, {
        component: _ReduxFieldInput.default,
        name: "title",
        placeholder: t('Tag title'),
        type: "text"
      })
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.TagTitle, {
      children: formValuesTitle
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {})]
  });
};

FormTitle.propTypes = {
  formValuesTitle: _propTypes.default.string,
  initialValuesTitle: _propTypes.default.string
};
var _default = FormTitle;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvRm9ybVRpdGxlLmpzIl0sIm5hbWVzIjpbIkZvcm1UaXRsZSIsImZvcm1WYWx1ZXNUaXRsZSIsImluaXRpYWxWYWx1ZXNUaXRsZSIsInQiLCJSZWR1eEZpZWxkSW5wdXQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHLFNBQVpBLFNBQVksT0FBNkM7QUFBQSxNQUExQ0MsZUFBMEMsUUFBMUNBLGVBQTBDO0FBQUEsTUFBekJDLGtCQUF5QixRQUF6QkEsa0JBQXlCOztBQUMzRCx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EsTUFBSSxzQkFBUUQsa0JBQVIsQ0FBSixFQUFpQztBQUM3Qix3QkFDSSxxQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBWSxNQUFBLFNBQVMsRUFBQyxNQUF0QjtBQUFBLDZCQUNJLHFCQUFDLGtDQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUVFLHdCQURmO0FBRUksUUFBQSxJQUFJLEVBQUMsT0FGVDtBQUdJLFFBQUEsV0FBVyxFQUFFRCxDQUFDLENBQUMsV0FBRCxDQUhsQjtBQUlJLFFBQUEsSUFBSSxFQUFDO0FBSlQ7QUFESixNQURKO0FBVUg7O0FBQ0Qsc0JBQ0k7QUFBQSw0QkFDSSxxQkFBQywwQkFBRDtBQUFBLGdCQUFXRjtBQUFYLE1BREosZUFFSSw4QkFGSjtBQUFBLElBREo7QUFNSCxDQXBCRDs7QUFzQkFELFNBQVMsQ0FBQ0ssU0FBVixHQUFzQjtBQUNsQkosRUFBQUEsZUFBZSxFQUFFSyxtQkFBVUMsTUFEVDtBQUVsQkwsRUFBQUEsa0JBQWtCLEVBQUVJLG1CQUFVQztBQUZaLENBQXRCO2VBS2VQLFMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaXNFbXB0eSBmcm9tICdsb2Rhc2gvaXNFbXB0eSc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgRm9ybSB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuXG5pbXBvcnQgUmVkdXhGaWVsZElucHV0IGZyb20gJ0Bjb21wb25lbnRzL1JlZHV4RmllbGRJbnB1dCc7XG5cbmltcG9ydCB7IFN0eWxlZFRpdGxlRmllbGQsIFRhZ1RpdGxlIH0gZnJvbSAnLi4vc3R5bGVkQ29tcG9uZW50cyc7XG5cbmNvbnN0IEZvcm1UaXRsZSA9ICh7IGZvcm1WYWx1ZXNUaXRsZSwgaW5pdGlhbFZhbHVlc1RpdGxlIH0pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgaWYgKGlzRW1wdHkoaW5pdGlhbFZhbHVlc1RpdGxlKSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY2xhc3NOYW1lPVwibWItMFwiPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRUaXRsZUZpZWxkXG4gICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudD17UmVkdXhGaWVsZElucHV0fVxuICAgICAgICAgICAgICAgICAgICBuYW1lPVwidGl0bGVcIlxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17dCgnVGFnIHRpdGxlJyl9XG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9Gb3JtLkdyb3VwPlxuICAgICAgICApO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgICA8PlxuICAgICAgICAgICAgPFRhZ1RpdGxlPntmb3JtVmFsdWVzVGl0bGV9PC9UYWdUaXRsZT5cbiAgICAgICAgICAgIDxociAvPlxuICAgICAgICA8Lz5cbiAgICApO1xufTtcblxuRm9ybVRpdGxlLnByb3BUeXBlcyA9IHtcbiAgICBmb3JtVmFsdWVzVGl0bGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaW5pdGlhbFZhbHVlc1RpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgRm9ybVRpdGxlO1xuIl19