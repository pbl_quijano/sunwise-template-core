"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _tags = require("../../../../constants/tags");

var _styledComponents = require("../styledComponents");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TagList = function TagList(_ref) {
  var filteredTags = _ref.filteredTags,
      handleSelectTag = _ref.handleSelectTag,
      isFromCustomTag = _ref.isFromCustomTag,
      selectedCategory = _ref.selectedCategory,
      selectedTag = _ref.selectedTag;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if ((0, _isNil.default)(selectedCategory) || (0, _isNil.default)(filteredTags[selectedCategory])) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
      className: "m-0 mt-3",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.StyledText, {
          children: t('Select a category')
        })
      })
    });
  }

  var getTag = function getTag(tag) {
    var filteredTag = filteredTags[selectedCategory].values[tag];
    return _objectSpread(_objectSpread({}, filteredTag), {}, {
      tag: tag
    });
  };

  var isDisabled = function isDisabled(tag) {
    if (isFromCustomTag && tag.calculable === 'false') {
      return true;
    }

    if (isFromCustomTag && _tags.TAG_ALL_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
      return true;
    }

    if (isFromCustomTag && tag.calculable === 'true' && _tags.TAG_ALL_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
      return true;
    }

    return false;
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
    className: "mt-3",
    children: Object.keys(filteredTags[selectedCategory].values).map(function (tag) {
      return !_tags.HIDDEN_TAGS.includes(tag) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_styledComponents.NavItem, {
        className: selectedTag === tag && 'active',
        disabled: isDisabled(getTag(tag)),
        "format-type": getTag(tag).format_type || 'none',
        onClick: function onClick() {
          return handleSelectTag(getTag(tag));
        },
        children: getTag(tag).label
      }, "tag-".concat(tag));
    })
  });
};

TagList.propTypes = {
  filteredTags: _propTypes.default.object,
  handleSelectTag: _propTypes.default.func,
  isFromCustomTag: _propTypes.default.bool,
  selectedCategory: _propTypes.default.string,
  selectedTag: _propTypes.default.string
};
var _default = TagList;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1RhZ0xpYnJhcnkvVGFnTGlzdC5qcyJdLCJuYW1lcyI6WyJUYWdMaXN0IiwiZmlsdGVyZWRUYWdzIiwiaGFuZGxlU2VsZWN0VGFnIiwiaXNGcm9tQ3VzdG9tVGFnIiwic2VsZWN0ZWRDYXRlZ29yeSIsInNlbGVjdGVkVGFnIiwidCIsImdldFRhZyIsInRhZyIsImZpbHRlcmVkVGFnIiwidmFsdWVzIiwiaXNEaXNhYmxlZCIsImNhbGN1bGFibGUiLCJUQUdfQUxMX1RFWFRfRk9STUFUX1RZUEVTIiwiaW5jbHVkZXMiLCJmb3JtYXRfdHlwZSIsIk9iamVjdCIsImtleXMiLCJtYXAiLCJISURERU5fVEFHUyIsImxhYmVsIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiZnVuYyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsT0FBTyxHQUFHLFNBQVZBLE9BQVUsT0FNVjtBQUFBLE1BTEZDLFlBS0UsUUFMRkEsWUFLRTtBQUFBLE1BSkZDLGVBSUUsUUFKRkEsZUFJRTtBQUFBLE1BSEZDLGVBR0UsUUFIRkEsZUFHRTtBQUFBLE1BRkZDLGdCQUVFLFFBRkZBLGdCQUVFO0FBQUEsTUFERkMsV0FDRSxRQURGQSxXQUNFOztBQUNGLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxNQUFJLG9CQUFNRixnQkFBTixLQUEyQixvQkFBTUgsWUFBWSxDQUFDRyxnQkFBRCxDQUFsQixDQUEvQixFQUFzRTtBQUNsRSx3QkFDSSxxQkFBQyxtQkFBRDtBQUFLLE1BQUEsU0FBUyxFQUFDLFVBQWY7QUFBQSw2QkFDSSxxQkFBQyxtQkFBRDtBQUFBLCtCQUNJLHFCQUFDLDRCQUFEO0FBQUEsb0JBQWFFLENBQUMsQ0FBQyxtQkFBRDtBQUFkO0FBREo7QUFESixNQURKO0FBT0g7O0FBRUQsTUFBTUMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBQ0MsR0FBRCxFQUFTO0FBQ3BCLFFBQU1DLFdBQVcsR0FBR1IsWUFBWSxDQUFDRyxnQkFBRCxDQUFaLENBQStCTSxNQUEvQixDQUFzQ0YsR0FBdEMsQ0FBcEI7QUFDQSwyQ0FBWUMsV0FBWjtBQUF5QkQsTUFBQUEsR0FBRyxFQUFFQTtBQUE5QjtBQUNILEdBSEQ7O0FBS0EsTUFBTUcsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0gsR0FBRCxFQUFTO0FBQ3hCLFFBQUlMLGVBQWUsSUFBSUssR0FBRyxDQUFDSSxVQUFKLEtBQW1CLE9BQTFDLEVBQW1EO0FBQy9DLGFBQU8sSUFBUDtBQUNIOztBQUNELFFBQ0lULGVBQWUsSUFDZlUsZ0NBQTBCQyxRQUExQixDQUFtQ04sR0FBRyxDQUFDTyxXQUF2QyxDQUZKLEVBR0U7QUFDRSxhQUFPLElBQVA7QUFDSDs7QUFDRCxRQUNJWixlQUFlLElBQ2ZLLEdBQUcsQ0FBQ0ksVUFBSixLQUFtQixNQURuQixJQUVBQyxnQ0FBMEJDLFFBQTFCLENBQW1DTixHQUFHLENBQUNPLFdBQXZDLENBSEosRUFJRTtBQUNFLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBUDtBQUNILEdBbEJEOztBQW9CQSxzQkFDSTtBQUFLLElBQUEsU0FBUyxFQUFDLE1BQWY7QUFBQSxjQUNLQyxNQUFNLENBQUNDLElBQVAsQ0FBWWhCLFlBQVksQ0FBQ0csZ0JBQUQsQ0FBWixDQUErQk0sTUFBM0MsRUFBbURRLEdBQW5ELENBQXVELFVBQUNWLEdBQUQsRUFBUztBQUM3RCxhQUNJLENBQUNXLGtCQUFZTCxRQUFaLENBQXFCTixHQUFyQixDQUFELGlCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUVILFdBQVcsS0FBS0csR0FBaEIsSUFBdUIsUUFEdEM7QUFFSSxRQUFBLFFBQVEsRUFBRUcsVUFBVSxDQUFDSixNQUFNLENBQUNDLEdBQUQsQ0FBUCxDQUZ4QjtBQUdJLHVCQUFhRCxNQUFNLENBQUNDLEdBQUQsQ0FBTixDQUFZTyxXQUFaLElBQTJCLE1BSDVDO0FBS0ksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFBTWIsZUFBZSxDQUFDSyxNQUFNLENBQUNDLEdBQUQsQ0FBUCxDQUFyQjtBQUFBLFNBTGI7QUFBQSxrQkFPS0QsTUFBTSxDQUFDQyxHQUFELENBQU4sQ0FBWVk7QUFQakIsdUJBSWdCWixHQUpoQixFQUZSO0FBYUgsS0FkQTtBQURMLElBREo7QUFtQkgsQ0E5REQ7O0FBZ0VBUixPQUFPLENBQUNxQixTQUFSLEdBQW9CO0FBQ2hCcEIsRUFBQUEsWUFBWSxFQUFFcUIsbUJBQVVDLE1BRFI7QUFFaEJyQixFQUFBQSxlQUFlLEVBQUVvQixtQkFBVUUsSUFGWDtBQUdoQnJCLEVBQUFBLGVBQWUsRUFBRW1CLG1CQUFVRyxJQUhYO0FBSWhCckIsRUFBQUEsZ0JBQWdCLEVBQUVrQixtQkFBVUksTUFKWjtBQUtoQnJCLEVBQUFBLFdBQVcsRUFBRWlCLG1CQUFVSTtBQUxQLENBQXBCO2VBUWUxQixPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgQ29sLCBSb3cgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcblxuaW1wb3J0IHsgSElEREVOX1RBR1MsIFRBR19BTExfVEVYVF9GT1JNQVRfVFlQRVMgfSBmcm9tICdAY29uc3RhbnRzL3RhZ3MnO1xuXG5pbXBvcnQgeyBOYXZJdGVtLCBTdHlsZWRUZXh0IH0gZnJvbSAnLi4vc3R5bGVkQ29tcG9uZW50cyc7XG5cbmNvbnN0IFRhZ0xpc3QgPSAoe1xuICAgIGZpbHRlcmVkVGFncyxcbiAgICBoYW5kbGVTZWxlY3RUYWcsXG4gICAgaXNGcm9tQ3VzdG9tVGFnLFxuICAgIHNlbGVjdGVkQ2F0ZWdvcnksXG4gICAgc2VsZWN0ZWRUYWcsXG59KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGlmIChpc05pbChzZWxlY3RlZENhdGVnb3J5KSB8fCBpc05pbChmaWx0ZXJlZFRhZ3Nbc2VsZWN0ZWRDYXRlZ29yeV0pKSB7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8Um93IGNsYXNzTmFtZT1cIm0tMCBtdC0zXCI+XG4gICAgICAgICAgICAgICAgPENvbD5cbiAgICAgICAgICAgICAgICAgICAgPFN0eWxlZFRleHQ+e3QoJ1NlbGVjdCBhIGNhdGVnb3J5Jyl9PC9TdHlsZWRUZXh0PlxuICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgY29uc3QgZ2V0VGFnID0gKHRhZykgPT4ge1xuICAgICAgICBjb25zdCBmaWx0ZXJlZFRhZyA9IGZpbHRlcmVkVGFnc1tzZWxlY3RlZENhdGVnb3J5XS52YWx1ZXNbdGFnXTtcbiAgICAgICAgcmV0dXJuIHsgLi4uZmlsdGVyZWRUYWcsIHRhZzogdGFnIH07XG4gICAgfTtcblxuICAgIGNvbnN0IGlzRGlzYWJsZWQgPSAodGFnKSA9PiB7XG4gICAgICAgIGlmIChpc0Zyb21DdXN0b21UYWcgJiYgdGFnLmNhbGN1bGFibGUgPT09ICdmYWxzZScpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIGlzRnJvbUN1c3RvbVRhZyAmJlxuICAgICAgICAgICAgVEFHX0FMTF9URVhUX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyh0YWcuZm9ybWF0X3R5cGUpXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgaXNGcm9tQ3VzdG9tVGFnICYmXG4gICAgICAgICAgICB0YWcuY2FsY3VsYWJsZSA9PT0gJ3RydWUnICYmXG4gICAgICAgICAgICBUQUdfQUxMX1RFWFRfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSlcbiAgICAgICAgKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibXQtM1wiPlxuICAgICAgICAgICAge09iamVjdC5rZXlzKGZpbHRlcmVkVGFnc1tzZWxlY3RlZENhdGVnb3J5XS52YWx1ZXMpLm1hcCgodGFnKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgIUhJRERFTl9UQUdTLmluY2x1ZGVzKHRhZykgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPE5hdkl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3NlbGVjdGVkVGFnID09PSB0YWcgJiYgJ2FjdGl2ZSd9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2lzRGlzYWJsZWQoZ2V0VGFnKHRhZykpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdC10eXBlPXtnZXRUYWcodGFnKS5mb3JtYXRfdHlwZSB8fCAnbm9uZSd9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgdGFnLSR7dGFnfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlU2VsZWN0VGFnKGdldFRhZyh0YWcpKX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0VGFnKHRhZykubGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICA8L05hdkl0ZW0+XG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSl9XG4gICAgICAgIDwvZGl2PlxuICAgICk7XG59O1xuXG5UYWdMaXN0LnByb3BUeXBlcyA9IHtcbiAgICBmaWx0ZXJlZFRhZ3M6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaGFuZGxlU2VsZWN0VGFnOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBpc0Zyb21DdXN0b21UYWc6IFByb3BUeXBlcy5ib29sLFxuICAgIHNlbGVjdGVkQ2F0ZWdvcnk6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2VsZWN0ZWRUYWc6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBUYWdMaXN0O1xuIl19