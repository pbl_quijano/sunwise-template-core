"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WrapperField = function WrapperField(_ref) {
  var children = _ref.children,
      visible = _ref.visible;

  if (!visible) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_jsxRuntime.Fragment, {
    children: children
  });
};

WrapperField.propTypes = {
  children: _propTypes.default.object,
  visible: _propTypes.default.bool
};
var _default = WrapperField;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9jb21wb25lbnRzL1dyYXBwZXJGaWVsZC5qcyJdLCJuYW1lcyI6WyJXcmFwcGVyRmllbGQiLCJjaGlsZHJlbiIsInZpc2libGUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7OztBQUVBLElBQU1BLFlBQVksR0FBRyxTQUFmQSxZQUFlLE9BQTJCO0FBQUEsTUFBeEJDLFFBQXdCLFFBQXhCQSxRQUF3QjtBQUFBLE1BQWRDLE9BQWMsUUFBZEEsT0FBYzs7QUFDNUMsTUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDVixXQUFPLElBQVA7QUFDSDs7QUFDRCxzQkFBTztBQUFBLGNBQUdEO0FBQUgsSUFBUDtBQUNILENBTEQ7O0FBT0FELFlBQVksQ0FBQ0csU0FBYixHQUF5QjtBQUNyQkYsRUFBQUEsUUFBUSxFQUFFRyxtQkFBVUMsTUFEQztBQUVyQkgsRUFBQUEsT0FBTyxFQUFFRSxtQkFBVUU7QUFGRSxDQUF6QjtlQUtlTixZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgV3JhcHBlckZpZWxkID0gKHsgY2hpbGRyZW4sIHZpc2libGUgfSkgPT4ge1xuICAgIGlmICghdmlzaWJsZSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmV0dXJuIDw+e2NoaWxkcmVufTwvPjtcbn07XG5cbldyYXBwZXJGaWVsZC5wcm9wVHlwZXMgPSB7XG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBXcmFwcGVyRmllbGQ7XG4iXX0=