"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ModalTitle = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ModalBootstrap = _interopRequireDefault(require("../../components/ModalBootstrap"));

var _tags = require("../../constants/tags");

var _types = require("../../constants/types");

var _contexts = require("../../helpers/contexts");

var mainActions = _interopRequireWildcard(require("../main/actions"));

var actions = _interopRequireWildcard(require("./actions"));

var _ContentView = _interopRequireDefault(require("./components/ContentView"));

var _helpers = require("./helpers");

var selectors = _interopRequireWildcard(require("./selectors"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ModalTitle = _styledComponents.default.h3(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: #002438;\n    font-size: 18px;\n    font-weight: bold;\n    letter-spacing: 0;\n    line-height: 32px;\n    margin-bottom: 0;\n"])));

exports.ModalTitle = ModalTitle;

var TagSystem = function TagSystem(_ref) {
  var _ref$availableCatalog = _ref.availableCatalogs,
      availableCatalogs = _ref$availableCatalog === void 0 ? [] : _ref$availableCatalog,
      contentViewState = _ref.contentViewState,
      editionLevel = _ref.editionLevel,
      fetchTagsCurrency = _ref.fetchTagsCurrency,
      froalaInstance = _ref.froalaInstance,
      initialize = _ref.initialize,
      isFetchingCurrencies = _ref.isFetchingCurrencies,
      isFetchingDecimalFormats = _ref.isFetchingDecimalFormats,
      isFetchingTagFormats = _ref.isFetchingTagFormats,
      isFetchingUnitFormats = _ref.isFetchingUnitFormats,
      isOpenModal = _ref.isOpenModal,
      setContentViewState = _ref.setContentViewState,
      setIsOpenModal = _ref.setIsOpenModal,
      templateType = _ref.templateType;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      tagsLocale = _useContext.tagsLocale;

  (0, _react.useEffect)(function () {
    if (!tagsLocale) {
      fetchTagsCurrency();
    }
  }, [tagsLocale]);

  if (!tagsLocale) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ModalBootstrap.default, {
    closeButton: true,
    className: "modal-".concat(contentViewState),
    onEnter: function onEnter() {
      return initialize();
    },
    onHide: function onHide() {
      setContentViewState(_tags.HIDDEN_MODAL_STATE);
      setIsOpenModal(false);
    },
    onExit: function onExit() {
      return setContentViewState(_tags.HIDDEN_MODAL_STATE);
    },
    show: isOpenModal,
    size: contentViewState === _tags.INSERT_LIBRARY_TAG_STATE ? 'xl' : 'lg',
    title: /*#__PURE__*/(0, _jsxRuntime.jsxs)(ModalTitle, {
      children: [contentViewState === _tags.CREATE_CUSTOM_TAG_STATE && /*#__PURE__*/(0, _jsxRuntime.jsx)("button", {
        className: "btn btn-transparent p-0 mr-2",
        onClick: function onClick() {
          return setContentViewState(_tags.INSERT_LIBRARY_TAG_STATE);
        },
        title: t('Back'),
        type: "button",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-arrow-left mr-2"
        })
      }), (0, _helpers.getTitleModal)(contentViewState)]
    }),
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ContentView.default, {
      catalogs: availableCatalogs,
      contentViewState: contentViewState,
      editionLevel: editionLevel,
      handleEditTag: (0, _helpers.handleEditTagBuild)(froalaInstance, setIsOpenModal),
      handleInsertTag: (0, _helpers.handleInsertTagBuild)(froalaInstance, setIsOpenModal),
      hasProposalSelector: templateType === _types.MULTIPROPOSAL_TYPE,
      isInitialazing: isFetchingCurrencies || isFetchingDecimalFormats || isFetchingTagFormats || isFetchingUnitFormats,
      tagsLocale: tagsLocale,
      templateType: templateType
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  contentViewState: selectors.getContentViewState,
  isFetchingCurrencies: selectors.getIsFetchingCurrencies,
  isFetchingDecimalFormats: selectors.getIsFetchingDecimalFormats,
  isFetchingTagFormats: selectors.getIsFetchingTagFormats,
  isFetchingUnitFormats: selectors.getIsFetchingUnitFormats,
  isOpenModal: selectors.getIsOpenModal
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    fetchTagsCurrency: function fetchTagsCurrency() {
      return dispatch(mainActions.fetchTagsCurrency());
    },
    initialize: function initialize() {
      return dispatch(actions.initialize());
    },
    reset: function reset() {
      return dispatch(actions.reset());
    },
    setContentViewState: function setContentViewState(contentViewState) {
      return dispatch(actions.setContentViewState(contentViewState));
    },
    setIsOpenModal: function setIsOpenModal(isOpenModal) {
      return dispatch(actions.setIsOpenModal(isOpenModal));
    }
  };
};

TagSystem.propTypes = {
  availableCatalogs: _propTypes.default.array,
  contentViewState: _propTypes.default.number,
  editionLevel: _propTypes.default.string,
  fetchTagsCurrency: _propTypes.default.func,
  froalaInstance: _propTypes.default.object,
  initialize: _propTypes.default.func,
  isFetchingCurrencies: _propTypes.default.bool,
  isFetchingDecimalFormats: _propTypes.default.bool,
  isFetchingTagFormats: _propTypes.default.bool,
  isFetchingUnitFormats: _propTypes.default.bool,
  isOpenModal: _propTypes.default.bool,
  setContentViewState: _propTypes.default.func,
  setIsOpenModal: _propTypes.default.func,
  templateType: _propTypes.default.number
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TagSystem);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9pbmRleC5qcyJdLCJuYW1lcyI6WyJNb2RhbFRpdGxlIiwic3R5bGVkIiwiaDMiLCJUYWdTeXN0ZW0iLCJhdmFpbGFibGVDYXRhbG9ncyIsImNvbnRlbnRWaWV3U3RhdGUiLCJlZGl0aW9uTGV2ZWwiLCJmZXRjaFRhZ3NDdXJyZW5jeSIsImZyb2FsYUluc3RhbmNlIiwiaW5pdGlhbGl6ZSIsImlzRmV0Y2hpbmdDdXJyZW5jaWVzIiwiaXNGZXRjaGluZ0RlY2ltYWxGb3JtYXRzIiwiaXNGZXRjaGluZ1RhZ0Zvcm1hdHMiLCJpc0ZldGNoaW5nVW5pdEZvcm1hdHMiLCJpc09wZW5Nb2RhbCIsInNldENvbnRlbnRWaWV3U3RhdGUiLCJzZXRJc09wZW5Nb2RhbCIsInRlbXBsYXRlVHlwZSIsInQiLCJHZW5lcmFsQ29udGV4dCIsInRhZ3NMb2NhbGUiLCJISURERU5fTU9EQUxfU1RBVEUiLCJJTlNFUlRfTElCUkFSWV9UQUdfU1RBVEUiLCJDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSIsIk1VTFRJUFJPUE9TQUxfVFlQRSIsIm1hcFN0YXRlVG9Qcm9wcyIsInNlbGVjdG9ycyIsImdldENvbnRlbnRWaWV3U3RhdGUiLCJnZXRJc0ZldGNoaW5nQ3VycmVuY2llcyIsImdldElzRmV0Y2hpbmdEZWNpbWFsRm9ybWF0cyIsImdldElzRmV0Y2hpbmdUYWdGb3JtYXRzIiwiZ2V0SXNGZXRjaGluZ1VuaXRGb3JtYXRzIiwiZ2V0SXNPcGVuTW9kYWwiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsIm1haW5BY3Rpb25zIiwiYWN0aW9ucyIsInJlc2V0IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJudW1iZXIiLCJzdHJpbmciLCJmdW5jIiwib2JqZWN0IiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBS0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBS0E7Ozs7Ozs7Ozs7Ozs7O0FBRU8sSUFBTUEsVUFBVSxHQUFHQywwQkFBT0MsRUFBVixpTkFBaEI7Ozs7QUFTUCxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxPQWVaO0FBQUEsbUNBZEZDLGlCQWNFO0FBQUEsTUFkRkEsaUJBY0Usc0NBZGtCLEVBY2xCO0FBQUEsTUFiRkMsZ0JBYUUsUUFiRkEsZ0JBYUU7QUFBQSxNQVpGQyxZQVlFLFFBWkZBLFlBWUU7QUFBQSxNQVhGQyxpQkFXRSxRQVhGQSxpQkFXRTtBQUFBLE1BVkZDLGNBVUUsUUFWRkEsY0FVRTtBQUFBLE1BVEZDLFVBU0UsUUFURkEsVUFTRTtBQUFBLE1BUkZDLG9CQVFFLFFBUkZBLG9CQVFFO0FBQUEsTUFQRkMsd0JBT0UsUUFQRkEsd0JBT0U7QUFBQSxNQU5GQyxvQkFNRSxRQU5GQSxvQkFNRTtBQUFBLE1BTEZDLHFCQUtFLFFBTEZBLHFCQUtFO0FBQUEsTUFKRkMsV0FJRSxRQUpGQSxXQUlFO0FBQUEsTUFIRkMsbUJBR0UsUUFIRkEsbUJBR0U7QUFBQSxNQUZGQyxjQUVFLFFBRkZBLGNBRUU7QUFBQSxNQURGQyxZQUNFLFFBREZBLFlBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLG9CQUF1Qix1QkFBV0Msd0JBQVgsQ0FBdkI7QUFBQSxNQUFRQyxVQUFSLGVBQVFBLFVBQVI7O0FBQ0Esd0JBQVUsWUFBTTtBQUNaLFFBQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiYixNQUFBQSxpQkFBaUI7QUFDcEI7QUFDSixHQUpELEVBSUcsQ0FBQ2EsVUFBRCxDQUpIOztBQU1BLE1BQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiLFdBQU8sSUFBUDtBQUNIOztBQUNELHNCQUNJLHFCQUFDLHVCQUFEO0FBQ0ksSUFBQSxXQUFXLE1BRGY7QUFFSSxJQUFBLFNBQVMsa0JBQVdmLGdCQUFYLENBRmI7QUFHSSxJQUFBLE9BQU8sRUFBRTtBQUFBLGFBQU1JLFVBQVUsRUFBaEI7QUFBQSxLQUhiO0FBSUksSUFBQSxNQUFNLEVBQUUsa0JBQU07QUFDVk0sTUFBQUEsbUJBQW1CLENBQUNNLHdCQUFELENBQW5CO0FBQ0FMLE1BQUFBLGNBQWMsQ0FBQyxLQUFELENBQWQ7QUFDSCxLQVBMO0FBUUksSUFBQSxNQUFNLEVBQUU7QUFBQSxhQUFNRCxtQkFBbUIsQ0FBQ00sd0JBQUQsQ0FBekI7QUFBQSxLQVJaO0FBU0ksSUFBQSxJQUFJLEVBQUVQLFdBVFY7QUFVSSxJQUFBLElBQUksRUFBRVQsZ0JBQWdCLEtBQUtpQiw4QkFBckIsR0FBZ0QsSUFBaEQsR0FBdUQsSUFWakU7QUFXSSxJQUFBLEtBQUssZUFDRCxzQkFBQyxVQUFEO0FBQUEsaUJBQ0tqQixnQkFBZ0IsS0FBS2tCLDZCQUFyQixpQkFDRztBQUNJLFFBQUEsU0FBUyxFQUFDLDhCQURkO0FBRUksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFDTFIsbUJBQW1CLENBQUNPLDhCQUFELENBRGQ7QUFBQSxTQUZiO0FBS0ksUUFBQSxLQUFLLEVBQUVKLENBQUMsQ0FBQyxNQUFELENBTFo7QUFNSSxRQUFBLElBQUksRUFBQyxRQU5UO0FBQUEsK0JBUUk7QUFBRyxVQUFBLFNBQVMsRUFBQztBQUFiO0FBUkosUUFGUixFQWFLLDRCQUFjYixnQkFBZCxDQWJMO0FBQUEsTUFaUjtBQUFBLDJCQTZCSSxxQkFBQyxvQkFBRDtBQUNJLE1BQUEsUUFBUSxFQUFFRCxpQkFEZDtBQUVJLE1BQUEsZ0JBQWdCLEVBQUVDLGdCQUZ0QjtBQUdJLE1BQUEsWUFBWSxFQUFFQyxZQUhsQjtBQUlJLE1BQUEsYUFBYSxFQUFFLGlDQUNYRSxjQURXLEVBRVhRLGNBRlcsQ0FKbkI7QUFRSSxNQUFBLGVBQWUsRUFBRSxtQ0FDYlIsY0FEYSxFQUViUSxjQUZhLENBUnJCO0FBWUksTUFBQSxtQkFBbUIsRUFBRUMsWUFBWSxLQUFLTyx5QkFaMUM7QUFhSSxNQUFBLGNBQWMsRUFDVmQsb0JBQW9CLElBQ3BCQyx3QkFEQSxJQUVBQyxvQkFGQSxJQUdBQyxxQkFqQlI7QUFtQkksTUFBQSxVQUFVLEVBQUVPLFVBbkJoQjtBQW9CSSxNQUFBLFlBQVksRUFBRUg7QUFwQmxCO0FBN0JKLElBREo7QUFzREgsQ0FqRkQ7O0FBbUZBLElBQU1RLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0NwQixFQUFBQSxnQkFBZ0IsRUFBRXFCLFNBQVMsQ0FBQ0MsbUJBRGlCO0FBRTdDakIsRUFBQUEsb0JBQW9CLEVBQUVnQixTQUFTLENBQUNFLHVCQUZhO0FBRzdDakIsRUFBQUEsd0JBQXdCLEVBQUVlLFNBQVMsQ0FBQ0csMkJBSFM7QUFJN0NqQixFQUFBQSxvQkFBb0IsRUFBRWMsU0FBUyxDQUFDSSx1QkFKYTtBQUs3Q2pCLEVBQUFBLHFCQUFxQixFQUFFYSxTQUFTLENBQUNLLHdCQUxZO0FBTTdDakIsRUFBQUEsV0FBVyxFQUFFWSxTQUFTLENBQUNNO0FBTnNCLENBQXpCLENBQXhCOztBQVNBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQWU7QUFDdEMzQixJQUFBQSxpQkFBaUIsRUFBRTtBQUFBLGFBQU0yQixRQUFRLENBQUNDLFdBQVcsQ0FBQzVCLGlCQUFaLEVBQUQsQ0FBZDtBQUFBLEtBRG1CO0FBRXRDRSxJQUFBQSxVQUFVLEVBQUU7QUFBQSxhQUFNeUIsUUFBUSxDQUFDRSxPQUFPLENBQUMzQixVQUFSLEVBQUQsQ0FBZDtBQUFBLEtBRjBCO0FBR3RDNEIsSUFBQUEsS0FBSyxFQUFFO0FBQUEsYUFBTUgsUUFBUSxDQUFDRSxPQUFPLENBQUNDLEtBQVIsRUFBRCxDQUFkO0FBQUEsS0FIK0I7QUFJdEN0QixJQUFBQSxtQkFBbUIsRUFBRSw2QkFBQ1YsZ0JBQUQ7QUFBQSxhQUNqQjZCLFFBQVEsQ0FBQ0UsT0FBTyxDQUFDckIsbUJBQVIsQ0FBNEJWLGdCQUE1QixDQUFELENBRFM7QUFBQSxLQUppQjtBQU10Q1csSUFBQUEsY0FBYyxFQUFFLHdCQUFDRixXQUFEO0FBQUEsYUFDWm9CLFFBQVEsQ0FBQ0UsT0FBTyxDQUFDcEIsY0FBUixDQUF1QkYsV0FBdkIsQ0FBRCxDQURJO0FBQUE7QUFOc0IsR0FBZjtBQUFBLENBQTNCOztBQVVBWCxTQUFTLENBQUNtQyxTQUFWLEdBQXNCO0FBQ2xCbEMsRUFBQUEsaUJBQWlCLEVBQUVtQyxtQkFBVUMsS0FEWDtBQUVsQm5DLEVBQUFBLGdCQUFnQixFQUFFa0MsbUJBQVVFLE1BRlY7QUFHbEJuQyxFQUFBQSxZQUFZLEVBQUVpQyxtQkFBVUcsTUFITjtBQUlsQm5DLEVBQUFBLGlCQUFpQixFQUFFZ0MsbUJBQVVJLElBSlg7QUFLbEJuQyxFQUFBQSxjQUFjLEVBQUUrQixtQkFBVUssTUFMUjtBQU1sQm5DLEVBQUFBLFVBQVUsRUFBRThCLG1CQUFVSSxJQU5KO0FBT2xCakMsRUFBQUEsb0JBQW9CLEVBQUU2QixtQkFBVU0sSUFQZDtBQVFsQmxDLEVBQUFBLHdCQUF3QixFQUFFNEIsbUJBQVVNLElBUmxCO0FBU2xCakMsRUFBQUEsb0JBQW9CLEVBQUUyQixtQkFBVU0sSUFUZDtBQVVsQmhDLEVBQUFBLHFCQUFxQixFQUFFMEIsbUJBQVVNLElBVmY7QUFXbEIvQixFQUFBQSxXQUFXLEVBQUV5QixtQkFBVU0sSUFYTDtBQVlsQjlCLEVBQUFBLG1CQUFtQixFQUFFd0IsbUJBQVVJLElBWmI7QUFhbEIzQixFQUFBQSxjQUFjLEVBQUV1QixtQkFBVUksSUFiUjtBQWNsQjFCLEVBQUFBLFlBQVksRUFBRXNCLG1CQUFVRTtBQWROLENBQXRCOztlQWlCZSx5QkFBUWhCLGVBQVIsRUFBeUJRLGtCQUF6QixFQUE2QzlCLFNBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ29udGV4dCwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IE1vZGFsQm9vdHN0cmFwIGZyb20gJ0Bjb21wb25lbnRzL01vZGFsQm9vdHN0cmFwJztcblxuaW1wb3J0IHtcbiAgICBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSxcbiAgICBISURERU5fTU9EQUxfU1RBVEUsXG4gICAgSU5TRVJUX0xJQlJBUllfVEFHX1NUQVRFLFxufSBmcm9tICdAY29uc3RhbnRzL3RhZ3MnO1xuaW1wb3J0IHsgTVVMVElQUk9QT1NBTF9UWVBFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmltcG9ydCB7IEdlbmVyYWxDb250ZXh0IH0gZnJvbSAnQGhlbHBlcnMvY29udGV4dHMnO1xuXG5pbXBvcnQgKiBhcyBtYWluQWN0aW9ucyBmcm9tICdAbWFpbi9hY3Rpb25zJztcblxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuL2FjdGlvbnMnO1xuaW1wb3J0IENvbnRlbnRWaWV3IGZyb20gJy4vY29tcG9uZW50cy9Db250ZW50Vmlldyc7XG5pbXBvcnQge1xuICAgIGdldFRpdGxlTW9kYWwsXG4gICAgaGFuZGxlRWRpdFRhZ0J1aWxkLFxuICAgIGhhbmRsZUluc2VydFRhZ0J1aWxkLFxufSBmcm9tICcuL2hlbHBlcnMnO1xuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJy4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGNvbnN0IE1vZGFsVGl0bGUgPSBzdHlsZWQuaDNgXG4gICAgY29sb3I6ICMwMDI0Mzg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG5gO1xuXG5jb25zdCBUYWdTeXN0ZW0gPSAoe1xuICAgIGF2YWlsYWJsZUNhdGFsb2dzID0gW10sXG4gICAgY29udGVudFZpZXdTdGF0ZSxcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgZmV0Y2hUYWdzQ3VycmVuY3ksXG4gICAgZnJvYWxhSW5zdGFuY2UsXG4gICAgaW5pdGlhbGl6ZSxcbiAgICBpc0ZldGNoaW5nQ3VycmVuY2llcyxcbiAgICBpc0ZldGNoaW5nRGVjaW1hbEZvcm1hdHMsXG4gICAgaXNGZXRjaGluZ1RhZ0Zvcm1hdHMsXG4gICAgaXNGZXRjaGluZ1VuaXRGb3JtYXRzLFxuICAgIGlzT3Blbk1vZGFsLFxuICAgIHNldENvbnRlbnRWaWV3U3RhdGUsXG4gICAgc2V0SXNPcGVuTW9kYWwsXG4gICAgdGVtcGxhdGVUeXBlLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCB7IHRhZ3NMb2NhbGUgfSA9IHVzZUNvbnRleHQoR2VuZXJhbENvbnRleHQpO1xuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIGlmICghdGFnc0xvY2FsZSkge1xuICAgICAgICAgICAgZmV0Y2hUYWdzQ3VycmVuY3koKTtcbiAgICAgICAgfVxuICAgIH0sIFt0YWdzTG9jYWxlXSk7XG5cbiAgICBpZiAoIXRhZ3NMb2NhbGUpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICAgIDxNb2RhbEJvb3RzdHJhcFxuICAgICAgICAgICAgY2xvc2VCdXR0b25cbiAgICAgICAgICAgIGNsYXNzTmFtZT17YG1vZGFsLSR7Y29udGVudFZpZXdTdGF0ZX1gfVxuICAgICAgICAgICAgb25FbnRlcj17KCkgPT4gaW5pdGlhbGl6ZSgpfVxuICAgICAgICAgICAgb25IaWRlPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgc2V0Q29udGVudFZpZXdTdGF0ZShISURERU5fTU9EQUxfU1RBVEUpO1xuICAgICAgICAgICAgICAgIHNldElzT3Blbk1vZGFsKGZhbHNlKTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBvbkV4aXQ9eygpID0+IHNldENvbnRlbnRWaWV3U3RhdGUoSElEREVOX01PREFMX1NUQVRFKX1cbiAgICAgICAgICAgIHNob3c9e2lzT3Blbk1vZGFsfVxuICAgICAgICAgICAgc2l6ZT17Y29udGVudFZpZXdTdGF0ZSA9PT0gSU5TRVJUX0xJQlJBUllfVEFHX1NUQVRFID8gJ3hsJyA6ICdsZyd9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgICAgPE1vZGFsVGl0bGU+XG4gICAgICAgICAgICAgICAgICAgIHtjb250ZW50Vmlld1N0YXRlID09PSBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYnRuIGJ0bi10cmFuc3BhcmVudCBwLTAgbXItMlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0Q29udGVudFZpZXdTdGF0ZShJTlNFUlRfTElCUkFSWV9UQUdfU1RBVEUpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXt0KCdCYWNrJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWFycm93LWxlZnQgbXItMlwiPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICB7Z2V0VGl0bGVNb2RhbChjb250ZW50Vmlld1N0YXRlKX1cbiAgICAgICAgICAgICAgICA8L01vZGFsVGl0bGU+XG4gICAgICAgICAgICB9XG4gICAgICAgID5cbiAgICAgICAgICAgIDxDb250ZW50Vmlld1xuICAgICAgICAgICAgICAgIGNhdGFsb2dzPXthdmFpbGFibGVDYXRhbG9nc31cbiAgICAgICAgICAgICAgICBjb250ZW50Vmlld1N0YXRlPXtjb250ZW50Vmlld1N0YXRlfVxuICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbD17ZWRpdGlvbkxldmVsfVxuICAgICAgICAgICAgICAgIGhhbmRsZUVkaXRUYWc9e2hhbmRsZUVkaXRUYWdCdWlsZChcbiAgICAgICAgICAgICAgICAgICAgZnJvYWxhSW5zdGFuY2UsXG4gICAgICAgICAgICAgICAgICAgIHNldElzT3Blbk1vZGFsXG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICBoYW5kbGVJbnNlcnRUYWc9e2hhbmRsZUluc2VydFRhZ0J1aWxkKFxuICAgICAgICAgICAgICAgICAgICBmcm9hbGFJbnN0YW5jZSxcbiAgICAgICAgICAgICAgICAgICAgc2V0SXNPcGVuTW9kYWxcbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I9e3RlbXBsYXRlVHlwZSA9PT0gTVVMVElQUk9QT1NBTF9UWVBFfVxuICAgICAgICAgICAgICAgIGlzSW5pdGlhbGF6aW5nPXtcbiAgICAgICAgICAgICAgICAgICAgaXNGZXRjaGluZ0N1cnJlbmNpZXMgfHxcbiAgICAgICAgICAgICAgICAgICAgaXNGZXRjaGluZ0RlY2ltYWxGb3JtYXRzIHx8XG4gICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmdUYWdGb3JtYXRzIHx8XG4gICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmdVbml0Rm9ybWF0c1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0YWdzTG9jYWxlPXt0YWdzTG9jYWxlfVxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVHlwZT17dGVtcGxhdGVUeXBlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9Nb2RhbEJvb3RzdHJhcD5cbiAgICApO1xufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcbiAgICBjb250ZW50Vmlld1N0YXRlOiBzZWxlY3RvcnMuZ2V0Q29udGVudFZpZXdTdGF0ZSxcbiAgICBpc0ZldGNoaW5nQ3VycmVuY2llczogc2VsZWN0b3JzLmdldElzRmV0Y2hpbmdDdXJyZW5jaWVzLFxuICAgIGlzRmV0Y2hpbmdEZWNpbWFsRm9ybWF0czogc2VsZWN0b3JzLmdldElzRmV0Y2hpbmdEZWNpbWFsRm9ybWF0cyxcbiAgICBpc0ZldGNoaW5nVGFnRm9ybWF0czogc2VsZWN0b3JzLmdldElzRmV0Y2hpbmdUYWdGb3JtYXRzLFxuICAgIGlzRmV0Y2hpbmdVbml0Rm9ybWF0czogc2VsZWN0b3JzLmdldElzRmV0Y2hpbmdVbml0Rm9ybWF0cyxcbiAgICBpc09wZW5Nb2RhbDogc2VsZWN0b3JzLmdldElzT3Blbk1vZGFsLFxufSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT4gKHtcbiAgICBmZXRjaFRhZ3NDdXJyZW5jeTogKCkgPT4gZGlzcGF0Y2gobWFpbkFjdGlvbnMuZmV0Y2hUYWdzQ3VycmVuY3koKSksXG4gICAgaW5pdGlhbGl6ZTogKCkgPT4gZGlzcGF0Y2goYWN0aW9ucy5pbml0aWFsaXplKCkpLFxuICAgIHJlc2V0OiAoKSA9PiBkaXNwYXRjaChhY3Rpb25zLnJlc2V0KCkpLFxuICAgIHNldENvbnRlbnRWaWV3U3RhdGU6IChjb250ZW50Vmlld1N0YXRlKSA9PlxuICAgICAgICBkaXNwYXRjaChhY3Rpb25zLnNldENvbnRlbnRWaWV3U3RhdGUoY29udGVudFZpZXdTdGF0ZSkpLFxuICAgIHNldElzT3Blbk1vZGFsOiAoaXNPcGVuTW9kYWwpID0+XG4gICAgICAgIGRpc3BhdGNoKGFjdGlvbnMuc2V0SXNPcGVuTW9kYWwoaXNPcGVuTW9kYWwpKSxcbn0pO1xuXG5UYWdTeXN0ZW0ucHJvcFR5cGVzID0ge1xuICAgIGF2YWlsYWJsZUNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgY29udGVudFZpZXdTdGF0ZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZmV0Y2hUYWdzQ3VycmVuY3k6IFByb3BUeXBlcy5mdW5jLFxuICAgIGZyb2FsYUluc3RhbmNlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGluaXRpYWxpemU6IFByb3BUeXBlcy5mdW5jLFxuICAgIGlzRmV0Y2hpbmdDdXJyZW5jaWVzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0ZldGNoaW5nRGVjaW1hbEZvcm1hdHM6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzRmV0Y2hpbmdUYWdGb3JtYXRzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc0ZldGNoaW5nVW5pdEZvcm1hdHM6IFByb3BUeXBlcy5ib29sLFxuICAgIGlzT3Blbk1vZGFsOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZXRDb250ZW50Vmlld1N0YXRlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzZXRJc09wZW5Nb2RhbDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdGVtcGxhdGVUeXBlOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoVGFnU3lzdGVtKTtcbiJdfQ==