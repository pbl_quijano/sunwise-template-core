"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateTag = exports.updateMultipleCurrencyTags = exports.searchTags = exports.normalizeDocumentField = exports.normalizeDecimalField = exports.normalizeCurrencyField = exports.handleSubmitTagLibraryBuild = exports.handleSubmitCustomTagBuild = exports.handleInsertTagBuild = exports.handleInsertNumber = exports.handleEditTagBuild = exports.handleClickOperator = exports.handleClickEraser = exports.getUnitsToSelect = exports.getTitleModal = exports.getTextButton = exports.getTagLibraryTextButton = exports.getTagFormatsToSelect = exports.getPropsValueField = exports.getFormattedValue = exports.getFilteredCategories = exports.getDocumentsToSelect = exports.getDecimalFormatsToSelect = exports.getCurrencyByUnit = exports.getColSize = exports.getBorderStyle = exports.generateFormatter = void 0;

var _i18next = _interopRequireDefault(require("i18next"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _tags = require("../../constants/tags");

var _utils = require("../../helpers/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var capitalize = function capitalize(value) {
  if (typeof value !== 'string') return '';
  return value.toLowerCase().charAt(0).toUpperCase() + value.toLowerCase().slice(1);
};

var generateFormatter = function generateFormatter(tag) {
  if (_tags.TAG_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
    return tag.format;
  }

  if (_tags.TAG_ECOLOGIC_TYPES.includes(tag.format_type) || _tags.TAG_NUMBER_FORMAT_TYPES.includes(tag.format_type) || _tags.TAG_PERCENT_FORMAT_TYPES.includes(tag.format_type)) {
    return "".concat(getPatternFormat(tag.decimal_places), " ").concat(tag.unit);
  }

  if (_tags.TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type)) {
    return "".concat(tag.symbol).concat(getPatternFormat(tag.decimal_places), " ").concat(tag.unit);
  }

  return '';
};

exports.generateFormatter = generateFormatter;

var getBorderStyle = function getBorderStyle(isInsertTagMode, isFromCustomTag) {
  if (isFromCustomTag || isInsertTagMode) {
    return {};
  }

  return {
    border: 0,
    padding: 0,
    boxShadow: 'none'
  };
};

exports.getBorderStyle = getBorderStyle;

var getChildNode = function getChildNode(element, num) {
  var children = element;

  for (var i = 0; i < num; i++) {
    if (children.length > 0 && !(0, _isNil.default)(children[0].lastElementChild)) {
      children = children[0].children;
    }
  }

  return children[0];
};

var getColSize = function getColSize(formatType, size1, size2) {
  return _tags.TAG_ALL_TEXT_FORMAT_TYPES.includes(formatType) ? size1 : size2;
};

exports.getColSize = getColSize;

var getCurrencyByUnit = function getCurrencyByUnit(currencies, unit) {
  return currencies.find(function (currency) {
    return currency.abbreviation === unit;
  }) || {
    id: null,
    symbol: ''
  };
};

exports.getCurrencyByUnit = getCurrencyByUnit;

var getDecimalFormatsToSelect = function getDecimalFormatsToSelect(decimalFormats, isInitialazing) {
  return [{
    label: isInitialazing ? _i18next.default.t('Loading decimals...') : _i18next.default.t('Select decimals'),
    value: '',
    disabled: true
  }].concat(_toConsumableArray(decimalFormats.map(function (item) {
    return {
      label: item.example,
      value: item.type
    };
  })));
};

exports.getDecimalFormatsToSelect = getDecimalFormatsToSelect;

var getDocumentsToSelect = function getDocumentsToSelect(documents, hasFilterByActive, isFetching) {
  var documentsFiltered = documents;

  if (hasFilterByActive) {
    documentsFiltered = documents.filter(function (item) {
      return item.is_active;
    });
  }

  return [{
    label: isFetching ? "".concat(_i18next.default.t('Loading document', {
      count: 2
    }), "...") : _i18next.default.t('Select a document'),
    value: '',
    disabled: true
  }].concat(_toConsumableArray(documentsFiltered.map(function (item) {
    return {
      label: item.name,
      value: item.id,
      disabled: !item.is_active
    };
  })));
};

exports.getDocumentsToSelect = getDocumentsToSelect;

var getFilteredCategories = function getFilteredCategories(tags, isForSmartDocuments) {
  if (!isForSmartDocuments) {
    return Object.keys(tags).filter(function (category) {
      return category !== _tags.SMART_DOCUMENTS_CATEGORY;
    });
  }

  return Object.keys(tags);
};

exports.getFilteredCategories = getFilteredCategories;

var getFormattedValue = function getFormattedValue(isNewTag, defaultCurrencyLocale, tag) {
  if (_tags.TAG_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
    switch (tag.format) {
      case 'MAYÚSCULAS':
        return tag.value.toUpperCase();

      case 'minúsculas':
        return tag.value.toLowerCase();

      case 'Normal':
        return capitalize(tag.value);

      case 'Oración':
        return sentence(tag.value);

      case 'Sunwise':
        return tag.value;
    }
  }

  if (_tags.TAG_ECOLOGIC_TYPES.includes(tag.format_type) || _tags.TAG_NUMBER_FORMAT_TYPES.includes(tag.format_type) || _tags.TAG_PERCENT_FORMAT_TYPES.includes(tag.format_type) || _tags.TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type) && isNewTag) {
    var newValue = tag.decimal_places == 0 ? Math.trunc(tag.value) : tag.value;
    return (0, _utils.numberFormat)(newValue, {
      decimals: tag.decimal_places,
      locale: defaultCurrencyLocale,
      style: 'decimal',
      unit: tag.unit
    });
  }

  if (_tags.TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type) && !(0, _isEmpty.default)(tag.unit)) {
    var locale = !(0, _isEmpty.default)(tag.currencyLocale) ? tag.currencyLocale : defaultCurrencyLocale;
    return (0, _utils.numberFormat)(tag.value, {
      currency: tag.unit,
      decimals: tag.decimal_places,
      locale: locale,
      style: 'currency'
    });
  }

  return tag.value;
};

exports.getFormattedValue = getFormattedValue;

var getLabelTextForUnits = function getLabelTextForUnits(formatType, isInitialazing) {
  if (_tags.DOCUMENT_TAGS.includes(formatType) && isInitialazing) {
    return _i18next.default.t('Loading catalogs...');
  }

  if (_tags.DOCUMENT_TAGS.includes(formatType) && !isInitialazing) {
    return _i18next.default.t('Select a catalog');
  }

  if (isInitialazing) {
    return _i18next.default.t('Loading units...');
  }

  return _i18next.default.t('Select unit');
};

var getParsedUnitFormats = function getParsedUnitFormats(currencies, formatType, unitFormats) {
  if (!['price-kwh', 'watt-price'].includes(formatType)) {
    return unitFormats;
  }

  var unitFiltered = unitFormats.find(function (item) {
    return item.format_type === formatType;
  });

  if (unitFiltered) {
    return currencies.map(function (item) {
      return {
        example: "".concat(item.abbreviation).concat(unitFiltered.unit),
        unit: "".concat(item.abbreviation).concat(unitFiltered.unit),
        format_type: formatType
      };
    });
  }

  return [];
};

var getPatternFormat = function getPatternFormat(decimal_places) {
  if (parseInt(decimal_places) > 0) {
    return "{:0,.".concat(decimal_places, "f}");
  }

  return '{:,}';
};

var getPropsValueField = function getPropsValueField(formatType) {
  if (_tags.TAG_ALL_TEXT_FORMAT_TYPES.includes(formatType)) {
    return {
      type: 'text'
    };
  }

  return {
    onlyNumbers: true
  };
};

exports.getPropsValueField = getPropsValueField;

var getTagElement = function getTagElement(froalaInstance) {
  if (froalaInstance) {
    return froalaInstance.$box.find(".fr-command[selected=\"true\"]");
  }

  return null;
};

var getTagFormatsToSelect = function getTagFormatsToSelect(isInitialazing, tagFormats, formatType) {
  var type = _tags.DOCUMENT_TAGS.includes(formatType) ? 'text' : formatType;
  var items = tagFormats.filter(function (item) {
    return item.format_type === type;
  }).map(function (item) {
    return {
      label: item.example,
      value: item.format
    };
  });
  return [{
    label: isInitialazing ? _i18next.default.t('Loading formats...') : _i18next.default.t('Select format'),
    value: '',
    disabled: true
  }].concat(_toConsumableArray(items));
};

exports.getTagFormatsToSelect = getTagFormatsToSelect;

var getTextButton = function getTextButton(contentViewState) {
  if (contentViewState === _tags.CREATE_CUSTOM_TAG_STATE) {
    return _i18next.default.t('Create tag');
  }

  return _i18next.default.t('Save settings');
};

exports.getTextButton = getTextButton;

var getTagLibraryTextButton = function getTagLibraryTextButton(isInsertTagMode, isFromCustomTag) {
  if (isFromCustomTag) {
    return _i18next.default.t('Insert to formula');
  }

  if (isInsertTagMode) {
    return _i18next.default.t('Insert tag');
  }

  return _i18next.default.t('Save settings');
};

exports.getTagLibraryTextButton = getTagLibraryTextButton;

var getTitleModal = function getTitleModal(option) {
  switch (option) {
    case _tags.CREATE_CUSTOM_TAG_STATE:
      return _i18next.default.t('Create smart tags');

    case _tags.EDIT_LIBRARY_TAG_STATE:
      return _i18next.default.t('Set up library tag');

    case _tags.EDIT_CUSTOM_TAG_STATE:
      return _i18next.default.t('Set up own tag');

    default:
      return _i18next.default.t('Select library tag');
  }
};

exports.getTitleModal = getTitleModal;

var getUnitsToSelect = function getUnitsToSelect(currencies, formatType, isInitialazing, unitFormats) {
  var items = getParsedUnitFormats(currencies, formatType, unitFormats).filter(function (item) {
    return item.format_type === formatType;
  }).map(function (item) {
    return {
      label: item.example,
      value: item.unit
    };
  });
  return [{
    label: getLabelTextForUnits(formatType, isInitialazing),
    value: '',
    disabled: true
  }].concat(_toConsumableArray(items));
};

exports.getUnitsToSelect = getUnitsToSelect;

var handleClickEraser = function handleClickEraser(changeInput, data) {
  var newData = _toConsumableArray(data);

  var index = newData.length - 1;
  newData.splice(index, 1);
  changeInput('calculator', newData);
};

exports.handleClickEraser = handleClickEraser;

var handleClickOperator = function handleClickOperator(changeInput, data, operator) {
  changeInput('calculator', [].concat(_toConsumableArray(data), [operator]));
};

exports.handleClickOperator = handleClickOperator;

var handleInsertNumber = function handleInsertNumber(changeInput, data, number, setNumber) {
  changeInput('calculator', [].concat(_toConsumableArray(data), [{
    title: number,
    tag: number,
    is_operator: true
  }]));
  setNumber(1);
};

exports.handleInsertNumber = handleInsertNumber;

var handleInsertTagBuild = function handleInsertTagBuild(froalaInstance, setIsOpenModal) {
  return function (tag) {
    var settings = JSON.stringify(tag);
    froalaInstance.selection.restore();
    froalaInstance.html.insert("<span>&nbsp;&nbsp;</span><mark class=\"fr-command sunwise-tag\" data-calculator=\"".concat(tag.is_new_tag, "\" data-tag=\"").concat(tag.tag, "\" data-settings='").concat(settings, "' title=\"").concat(tag.title, "\" style=\"background-color: ").concat(tag.color, "\">").concat(tag.text_value, "</mark><span>&nbsp;&nbsp;</span>"));
    froalaInstance.undo.saveStep();
    setIsOpenModal(false);
  };
};

exports.handleInsertTagBuild = handleInsertTagBuild;

var handleEditTagBuild = function handleEditTagBuild(froalaInstance, setIsOpenModal) {
  return function (tag) {
    var tagElement = getTagElement(froalaInstance);

    if (tagElement) {
      var settings = JSON.stringify(tag);
      tagElement.attr('contenteditable', false);
      tagElement.attr('data-settings', settings);
      tagElement.attr('title', tag.title);
      tagElement.attr('selected', false);
      tagElement.css('background-color', tag.color);

      if (getChildNode(tagElement, 10)) {
        getChildNode(tagElement, 10).innerText = tag.text_value;
      } else {
        tagElement.text(tag.text_value);
      }

      setIsOpenModal(false);
    }
  };
};

exports.handleEditTagBuild = handleEditTagBuild;

var handleSubmitCustomTagBuild = function handleSubmitCustomTagBuild(contentViewState, tagsLocale, handleEditTag, handleInsertTag) {
  return function (values) {
    var format = generateFormatter(values);

    var newItem = _objectSpread(_objectSpread({}, values), {}, {
      format: format,
      text_value: getFormattedValue(true, tagsLocale, values)
    });

    if (contentViewState === _tags.CREATE_CUSTOM_TAG_STATE) {
      handleInsertTag(newItem);
    } else {
      handleEditTag(newItem);
    }
  };
};

exports.handleSubmitCustomTagBuild = handleSubmitCustomTagBuild;

var handleSubmitTagLibraryBuild = function handleSubmitTagLibraryBuild(tagsLocale, isFromCustomTag, contentViewState, handleEditTag, handleInsertTag, prepareCustomTagFromSelectTag) {
  return function (values) {
    var format = generateFormatter(values);

    if (isFromCustomTag) {
      prepareCustomTagFromSelectTag(_objectSpread(_objectSpread({}, values), {}, {
        format: format,
        is_operator: false,
        title: values.label
      }));
    } else {
      var newItem = _objectSpread(_objectSpread({}, values), {}, {
        is_new_tag: false,
        format: format,
        text_value: getFormattedValue(false, tagsLocale, values)
      });

      if (contentViewState === _tags.INSERT_LIBRARY_TAG_STATE) {
        handleInsertTag(newItem);
      } else {
        handleEditTag(newItem);
      }
    }
  };
};

exports.handleSubmitTagLibraryBuild = handleSubmitTagLibraryBuild;

var normalizeCurrencyField = function normalizeCurrencyField(changeInput, currencies, names) {
  return function (value) {
    var item = currencies.find(function (item) {
      return item.id === value;
    });

    if (!(0, _isNil.default)(item)) {
      changeInput(names[0], item.locale);
      changeInput(names[1], item.abbreviation);
      changeInput(names[2], item.symbol);
    }

    return value;
  };
};

exports.normalizeCurrencyField = normalizeCurrencyField;

var normalizeDecimalField = function normalizeDecimalField(changeInput, decimalFormats, name) {
  return function (value) {
    var type = decimalFormats.find(function (decimal) {
      return decimal.type === value;
    });

    if (!(0, _isNil.default)(type)) {
      changeInput(name, type.decimal_places);
    }

    return value;
  };
};

exports.normalizeDecimalField = normalizeDecimalField;

var normalizeDocumentField = function normalizeDocumentField(changeInput, documents) {
  return function (value) {
    var document = documents.find(function (item) {
      return item.id === value;
    });

    if (!(0, _isNil.default)(document)) {
      changeInput('value', document.name);
      changeInput('default_value', document.name);
    }

    return value;
  };
};

exports.normalizeDocumentField = normalizeDocumentField;

var searchTags = function searchTags(tags) {
  var searchText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
  var filterTags = arguments.length > 2 ? arguments[2] : undefined;

  if (searchText === '') {
    filterTags(tags);
    return;
  }

  var tempFiltered = {};
  Object.keys(tags).forEach(function (category) {
    var hasItems = false;
    var items = {};
    Object.keys(tags[category].values).forEach(function (subcategory) {
      if (subcategory.toUpperCase().search(searchText.toUpperCase()) >= 0 || tags[category].values[subcategory].label.toUpperCase().search(searchText.toUpperCase()) >= 0 || tags[category].values[subcategory].description && tags[category].values[subcategory].description.toUpperCase().search(searchText.toUpperCase()) >= 0 || tags[category].values[subcategory].format_type && tags[category].values[subcategory].format_type.toUpperCase().search(searchText.toUpperCase()) >= 0) {
        hasItems = true;
        items[subcategory] = tags[category].values[subcategory];
      }
    });

    if (hasItems) {
      tempFiltered[category] = {
        label: tags[category].label,
        values: items
      };
    }
  });
  filterTags(tempFiltered);
};

exports.searchTags = searchTags;

var sentence = function sentence(value) {
  if (typeof value !== 'string') return '';
  return value.split(' ').map(capitalize).join(' ');
};

var updateMultipleCurrencyTags = function updateMultipleCurrencyTags(calculator, currencies) {
  var items = calculator.map(function (item) {
    if (!item.is_operator && _tags.TAG_CURRENCY_FORMAT_TYPES.includes(item.format_type) && (0, _utils.hasValue)(item, 'currency') && (0, _utils.hasValue)(item, 'unit')) {
      var currency = getCurrencyByUnit(currencies, item.unit);
      return _objectSpread(_objectSpread({}, item), {}, {
        currency: currency.id,
        symbol: currency.symbol,
        locale: currency.locale
      });
    }

    return item;
  });
  return items;
};

exports.updateMultipleCurrencyTags = updateMultipleCurrencyTags;

var updateTag = function updateTag(changeInput, data) {
  var formula = data.map(function (e) {
    return e.tag;
  }).join(' ');
  changeInput('tag', formula);
};

exports.updateTag = updateTag;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RhZ1N5c3RlbS9oZWxwZXJzLmpzIl0sIm5hbWVzIjpbImNhcGl0YWxpemUiLCJ2YWx1ZSIsInRvTG93ZXJDYXNlIiwiY2hhckF0IiwidG9VcHBlckNhc2UiLCJzbGljZSIsImdlbmVyYXRlRm9ybWF0dGVyIiwidGFnIiwiVEFHX1RFWFRfRk9STUFUX1RZUEVTIiwiaW5jbHVkZXMiLCJmb3JtYXRfdHlwZSIsImZvcm1hdCIsIlRBR19FQ09MT0dJQ19UWVBFUyIsIlRBR19OVU1CRVJfRk9STUFUX1RZUEVTIiwiVEFHX1BFUkNFTlRfRk9STUFUX1RZUEVTIiwiZ2V0UGF0dGVybkZvcm1hdCIsImRlY2ltYWxfcGxhY2VzIiwidW5pdCIsIlRBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMiLCJzeW1ib2wiLCJnZXRCb3JkZXJTdHlsZSIsImlzSW5zZXJ0VGFnTW9kZSIsImlzRnJvbUN1c3RvbVRhZyIsImJvcmRlciIsInBhZGRpbmciLCJib3hTaGFkb3ciLCJnZXRDaGlsZE5vZGUiLCJlbGVtZW50IiwibnVtIiwiY2hpbGRyZW4iLCJpIiwibGVuZ3RoIiwibGFzdEVsZW1lbnRDaGlsZCIsImdldENvbFNpemUiLCJmb3JtYXRUeXBlIiwic2l6ZTEiLCJzaXplMiIsIlRBR19BTExfVEVYVF9GT1JNQVRfVFlQRVMiLCJnZXRDdXJyZW5jeUJ5VW5pdCIsImN1cnJlbmNpZXMiLCJmaW5kIiwiY3VycmVuY3kiLCJhYmJyZXZpYXRpb24iLCJpZCIsImdldERlY2ltYWxGb3JtYXRzVG9TZWxlY3QiLCJkZWNpbWFsRm9ybWF0cyIsImlzSW5pdGlhbGF6aW5nIiwibGFiZWwiLCJpMThuZXh0IiwidCIsImRpc2FibGVkIiwibWFwIiwiaXRlbSIsImV4YW1wbGUiLCJ0eXBlIiwiZ2V0RG9jdW1lbnRzVG9TZWxlY3QiLCJkb2N1bWVudHMiLCJoYXNGaWx0ZXJCeUFjdGl2ZSIsImlzRmV0Y2hpbmciLCJkb2N1bWVudHNGaWx0ZXJlZCIsImZpbHRlciIsImlzX2FjdGl2ZSIsImNvdW50IiwibmFtZSIsImdldEZpbHRlcmVkQ2F0ZWdvcmllcyIsInRhZ3MiLCJpc0ZvclNtYXJ0RG9jdW1lbnRzIiwiT2JqZWN0Iiwia2V5cyIsImNhdGVnb3J5IiwiU01BUlRfRE9DVU1FTlRTX0NBVEVHT1JZIiwiZ2V0Rm9ybWF0dGVkVmFsdWUiLCJpc05ld1RhZyIsImRlZmF1bHRDdXJyZW5jeUxvY2FsZSIsInNlbnRlbmNlIiwibmV3VmFsdWUiLCJNYXRoIiwidHJ1bmMiLCJkZWNpbWFscyIsImxvY2FsZSIsInN0eWxlIiwiY3VycmVuY3lMb2NhbGUiLCJnZXRMYWJlbFRleHRGb3JVbml0cyIsIkRPQ1VNRU5UX1RBR1MiLCJnZXRQYXJzZWRVbml0Rm9ybWF0cyIsInVuaXRGb3JtYXRzIiwidW5pdEZpbHRlcmVkIiwicGFyc2VJbnQiLCJnZXRQcm9wc1ZhbHVlRmllbGQiLCJvbmx5TnVtYmVycyIsImdldFRhZ0VsZW1lbnQiLCJmcm9hbGFJbnN0YW5jZSIsIiRib3giLCJnZXRUYWdGb3JtYXRzVG9TZWxlY3QiLCJ0YWdGb3JtYXRzIiwiaXRlbXMiLCJnZXRUZXh0QnV0dG9uIiwiY29udGVudFZpZXdTdGF0ZSIsIkNSRUFURV9DVVNUT01fVEFHX1NUQVRFIiwiZ2V0VGFnTGlicmFyeVRleHRCdXR0b24iLCJnZXRUaXRsZU1vZGFsIiwib3B0aW9uIiwiRURJVF9MSUJSQVJZX1RBR19TVEFURSIsIkVESVRfQ1VTVE9NX1RBR19TVEFURSIsImdldFVuaXRzVG9TZWxlY3QiLCJoYW5kbGVDbGlja0VyYXNlciIsImNoYW5nZUlucHV0IiwiZGF0YSIsIm5ld0RhdGEiLCJpbmRleCIsInNwbGljZSIsImhhbmRsZUNsaWNrT3BlcmF0b3IiLCJvcGVyYXRvciIsImhhbmRsZUluc2VydE51bWJlciIsIm51bWJlciIsInNldE51bWJlciIsInRpdGxlIiwiaXNfb3BlcmF0b3IiLCJoYW5kbGVJbnNlcnRUYWdCdWlsZCIsInNldElzT3Blbk1vZGFsIiwic2V0dGluZ3MiLCJKU09OIiwic3RyaW5naWZ5Iiwic2VsZWN0aW9uIiwicmVzdG9yZSIsImh0bWwiLCJpbnNlcnQiLCJpc19uZXdfdGFnIiwiY29sb3IiLCJ0ZXh0X3ZhbHVlIiwidW5kbyIsInNhdmVTdGVwIiwiaGFuZGxlRWRpdFRhZ0J1aWxkIiwidGFnRWxlbWVudCIsImF0dHIiLCJjc3MiLCJpbm5lclRleHQiLCJ0ZXh0IiwiaGFuZGxlU3VibWl0Q3VzdG9tVGFnQnVpbGQiLCJ0YWdzTG9jYWxlIiwiaGFuZGxlRWRpdFRhZyIsImhhbmRsZUluc2VydFRhZyIsInZhbHVlcyIsIm5ld0l0ZW0iLCJoYW5kbGVTdWJtaXRUYWdMaWJyYXJ5QnVpbGQiLCJwcmVwYXJlQ3VzdG9tVGFnRnJvbVNlbGVjdFRhZyIsIklOU0VSVF9MSUJSQVJZX1RBR19TVEFURSIsIm5vcm1hbGl6ZUN1cnJlbmN5RmllbGQiLCJuYW1lcyIsIm5vcm1hbGl6ZURlY2ltYWxGaWVsZCIsImRlY2ltYWwiLCJub3JtYWxpemVEb2N1bWVudEZpZWxkIiwiZG9jdW1lbnQiLCJzZWFyY2hUYWdzIiwic2VhcmNoVGV4dCIsImZpbHRlclRhZ3MiLCJ0ZW1wRmlsdGVyZWQiLCJmb3JFYWNoIiwiaGFzSXRlbXMiLCJzdWJjYXRlZ29yeSIsInNlYXJjaCIsImRlc2NyaXB0aW9uIiwic3BsaXQiLCJqb2luIiwidXBkYXRlTXVsdGlwbGVDdXJyZW5jeVRhZ3MiLCJjYWxjdWxhdG9yIiwidXBkYXRlVGFnIiwiZm9ybXVsYSIsImUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFlQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLEtBQUQsRUFBVztBQUMxQixNQUFJLE9BQU9BLEtBQVAsS0FBaUIsUUFBckIsRUFBK0IsT0FBTyxFQUFQO0FBQy9CLFNBQ0lBLEtBQUssQ0FBQ0MsV0FBTixHQUFvQkMsTUFBcEIsQ0FBMkIsQ0FBM0IsRUFBOEJDLFdBQTlCLEtBQ0FILEtBQUssQ0FBQ0MsV0FBTixHQUFvQkcsS0FBcEIsQ0FBMEIsQ0FBMUIsQ0FGSjtBQUlILENBTkQ7O0FBUU8sSUFBTUMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDQyxHQUFELEVBQVM7QUFDdEMsTUFBSUMsNEJBQXNCQyxRQUF0QixDQUErQkYsR0FBRyxDQUFDRyxXQUFuQyxDQUFKLEVBQXFEO0FBQ2pELFdBQU9ILEdBQUcsQ0FBQ0ksTUFBWDtBQUNIOztBQUNELE1BQ0lDLHlCQUFtQkgsUUFBbkIsQ0FBNEJGLEdBQUcsQ0FBQ0csV0FBaEMsS0FDQUcsOEJBQXdCSixRQUF4QixDQUFpQ0YsR0FBRyxDQUFDRyxXQUFyQyxDQURBLElBRUFJLCtCQUF5QkwsUUFBekIsQ0FBa0NGLEdBQUcsQ0FBQ0csV0FBdEMsQ0FISixFQUlFO0FBQ0UscUJBQVVLLGdCQUFnQixDQUFDUixHQUFHLENBQUNTLGNBQUwsQ0FBMUIsY0FBa0RULEdBQUcsQ0FBQ1UsSUFBdEQ7QUFDSDs7QUFDRCxNQUFJQyxnQ0FBMEJULFFBQTFCLENBQW1DRixHQUFHLENBQUNHLFdBQXZDLENBQUosRUFBeUQ7QUFDckQscUJBQVVILEdBQUcsQ0FBQ1ksTUFBZCxTQUF1QkosZ0JBQWdCLENBQUNSLEdBQUcsQ0FBQ1MsY0FBTCxDQUF2QyxjQUNJVCxHQUFHLENBQUNVLElBRFI7QUFHSDs7QUFDRCxTQUFPLEVBQVA7QUFDSCxDQWpCTTs7OztBQW1CQSxJQUFNRyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLGVBQUQsRUFBa0JDLGVBQWxCLEVBQXNDO0FBQ2hFLE1BQUlBLGVBQWUsSUFBSUQsZUFBdkIsRUFBd0M7QUFDcEMsV0FBTyxFQUFQO0FBQ0g7O0FBQ0QsU0FBTztBQUNIRSxJQUFBQSxNQUFNLEVBQUUsQ0FETDtBQUVIQyxJQUFBQSxPQUFPLEVBQUUsQ0FGTjtBQUdIQyxJQUFBQSxTQUFTLEVBQUU7QUFIUixHQUFQO0FBS0gsQ0FUTTs7OztBQVdQLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLE9BQUQsRUFBVUMsR0FBVixFQUFrQjtBQUNuQyxNQUFJQyxRQUFRLEdBQUdGLE9BQWY7O0FBQ0EsT0FBSyxJQUFJRyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixHQUFwQixFQUF5QkUsQ0FBQyxFQUExQixFQUE4QjtBQUMxQixRQUFJRCxRQUFRLENBQUNFLE1BQVQsR0FBa0IsQ0FBbEIsSUFBdUIsQ0FBQyxvQkFBTUYsUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZRyxnQkFBbEIsQ0FBNUIsRUFBaUU7QUFDN0RILE1BQUFBLFFBQVEsR0FBR0EsUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZQSxRQUF2QjtBQUNIO0FBQ0o7O0FBQ0QsU0FBT0EsUUFBUSxDQUFDLENBQUQsQ0FBZjtBQUNILENBUkQ7O0FBVU8sSUFBTUksVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0MsVUFBRCxFQUFhQyxLQUFiLEVBQW9CQyxLQUFwQjtBQUFBLFNBQ3RCQyxnQ0FBMEI1QixRQUExQixDQUFtQ3lCLFVBQW5DLElBQWlEQyxLQUFqRCxHQUF5REMsS0FEbkM7QUFBQSxDQUFuQjs7OztBQUdBLElBQU1FLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQ0MsVUFBRCxFQUFhdEIsSUFBYixFQUFzQjtBQUNuRCxTQUNJc0IsVUFBVSxDQUFDQyxJQUFYLENBQWdCLFVBQUNDLFFBQUQ7QUFBQSxXQUFjQSxRQUFRLENBQUNDLFlBQVQsS0FBMEJ6QixJQUF4QztBQUFBLEdBQWhCLEtBQWlFO0FBQzdEMEIsSUFBQUEsRUFBRSxFQUFFLElBRHlEO0FBRTdEeEIsSUFBQUEsTUFBTSxFQUFFO0FBRnFELEdBRHJFO0FBTUgsQ0FQTTs7OztBQVNBLElBQU15Qix5QkFBeUIsR0FBRyxTQUE1QkEseUJBQTRCLENBQUNDLGNBQUQsRUFBaUJDLGNBQWpCLEVBQW9DO0FBQ3pFLFVBQ0k7QUFDSUMsSUFBQUEsS0FBSyxFQUFFRCxjQUFjLEdBQ2ZFLGlCQUFRQyxDQUFSLENBQVUscUJBQVYsQ0FEZSxHQUVmRCxpQkFBUUMsQ0FBUixDQUFVLGlCQUFWLENBSFY7QUFJSWhELElBQUFBLEtBQUssRUFBRSxFQUpYO0FBS0lpRCxJQUFBQSxRQUFRLEVBQUU7QUFMZCxHQURKLDRCQVFPTCxjQUFjLENBQUNNLEdBQWYsQ0FBbUIsVUFBQ0MsSUFBRDtBQUFBLFdBQVc7QUFDN0JMLE1BQUFBLEtBQUssRUFBRUssSUFBSSxDQUFDQyxPQURpQjtBQUU3QnBELE1BQUFBLEtBQUssRUFBRW1ELElBQUksQ0FBQ0U7QUFGaUIsS0FBWDtBQUFBLEdBQW5CLENBUlA7QUFhSCxDQWRNOzs7O0FBZ0JBLElBQU1DLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsQ0FDaENDLFNBRGdDLEVBRWhDQyxpQkFGZ0MsRUFHaENDLFVBSGdDLEVBSS9CO0FBQ0QsTUFBSUMsaUJBQWlCLEdBQUdILFNBQXhCOztBQUNBLE1BQUlDLGlCQUFKLEVBQXVCO0FBQ25CRSxJQUFBQSxpQkFBaUIsR0FBR0gsU0FBUyxDQUFDSSxNQUFWLENBQWlCLFVBQUNSLElBQUQ7QUFBQSxhQUFVQSxJQUFJLENBQUNTLFNBQWY7QUFBQSxLQUFqQixDQUFwQjtBQUNIOztBQUNELFVBQ0k7QUFDSWQsSUFBQUEsS0FBSyxFQUFFVyxVQUFVLGFBQ1JWLGlCQUFRQyxDQUFSLENBQVUsa0JBQVYsRUFBOEI7QUFBRWEsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBOUIsQ0FEUSxXQUVYZCxpQkFBUUMsQ0FBUixDQUFVLG1CQUFWLENBSFY7QUFJSWhELElBQUFBLEtBQUssRUFBRSxFQUpYO0FBS0lpRCxJQUFBQSxRQUFRLEVBQUU7QUFMZCxHQURKLDRCQVFPUyxpQkFBaUIsQ0FBQ1IsR0FBbEIsQ0FBc0IsVUFBQ0MsSUFBRDtBQUFBLFdBQVc7QUFDaENMLE1BQUFBLEtBQUssRUFBRUssSUFBSSxDQUFDVyxJQURvQjtBQUVoQzlELE1BQUFBLEtBQUssRUFBRW1ELElBQUksQ0FBQ1QsRUFGb0I7QUFHaENPLE1BQUFBLFFBQVEsRUFBRSxDQUFDRSxJQUFJLENBQUNTO0FBSGdCLEtBQVg7QUFBQSxHQUF0QixDQVJQO0FBY0gsQ0F2Qk07Ozs7QUF5QkEsSUFBTUcscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDQyxJQUFELEVBQU9DLG1CQUFQLEVBQStCO0FBQ2hFLE1BQUksQ0FBQ0EsbUJBQUwsRUFBMEI7QUFDdEIsV0FBT0MsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQVosRUFBa0JMLE1BQWxCLENBQ0gsVUFBQ1MsUUFBRDtBQUFBLGFBQWNBLFFBQVEsS0FBS0MsOEJBQTNCO0FBQUEsS0FERyxDQUFQO0FBR0g7O0FBQ0QsU0FBT0gsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQVosQ0FBUDtBQUNILENBUE07Ozs7QUFTQSxJQUFNTSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLFFBQUQsRUFBV0MscUJBQVgsRUFBa0NsRSxHQUFsQyxFQUEwQztBQUN2RSxNQUFJQyw0QkFBc0JDLFFBQXRCLENBQStCRixHQUFHLENBQUNHLFdBQW5DLENBQUosRUFBcUQ7QUFDakQsWUFBUUgsR0FBRyxDQUFDSSxNQUFaO0FBQ0ksV0FBSyxZQUFMO0FBQ0ksZUFBT0osR0FBRyxDQUFDTixLQUFKLENBQVVHLFdBQVYsRUFBUDs7QUFDSixXQUFLLFlBQUw7QUFDSSxlQUFPRyxHQUFHLENBQUNOLEtBQUosQ0FBVUMsV0FBVixFQUFQOztBQUNKLFdBQUssUUFBTDtBQUNJLGVBQU9GLFVBQVUsQ0FBQ08sR0FBRyxDQUFDTixLQUFMLENBQWpCOztBQUNKLFdBQUssU0FBTDtBQUNJLGVBQU95RSxRQUFRLENBQUNuRSxHQUFHLENBQUNOLEtBQUwsQ0FBZjs7QUFDSixXQUFLLFNBQUw7QUFDSSxlQUFPTSxHQUFHLENBQUNOLEtBQVg7QUFWUjtBQVlIOztBQUNELE1BQ0lXLHlCQUFtQkgsUUFBbkIsQ0FBNEJGLEdBQUcsQ0FBQ0csV0FBaEMsS0FDQUcsOEJBQXdCSixRQUF4QixDQUFpQ0YsR0FBRyxDQUFDRyxXQUFyQyxDQURBLElBRUFJLCtCQUF5QkwsUUFBekIsQ0FBa0NGLEdBQUcsQ0FBQ0csV0FBdEMsQ0FGQSxJQUdDUSxnQ0FBMEJULFFBQTFCLENBQW1DRixHQUFHLENBQUNHLFdBQXZDLEtBQXVEOEQsUUFKNUQsRUFLRTtBQUNFLFFBQU1HLFFBQVEsR0FDVnBFLEdBQUcsQ0FBQ1MsY0FBSixJQUFzQixDQUF0QixHQUEwQjRELElBQUksQ0FBQ0MsS0FBTCxDQUFXdEUsR0FBRyxDQUFDTixLQUFmLENBQTFCLEdBQWtETSxHQUFHLENBQUNOLEtBRDFEO0FBRUEsV0FBTyx5QkFBYTBFLFFBQWIsRUFBdUI7QUFDMUJHLE1BQUFBLFFBQVEsRUFBRXZFLEdBQUcsQ0FBQ1MsY0FEWTtBQUUxQitELE1BQUFBLE1BQU0sRUFBRU4scUJBRmtCO0FBRzFCTyxNQUFBQSxLQUFLLEVBQUUsU0FIbUI7QUFJMUIvRCxNQUFBQSxJQUFJLEVBQUVWLEdBQUcsQ0FBQ1U7QUFKZ0IsS0FBdkIsQ0FBUDtBQU1IOztBQUNELE1BQ0lDLGdDQUEwQlQsUUFBMUIsQ0FBbUNGLEdBQUcsQ0FBQ0csV0FBdkMsS0FDQSxDQUFDLHNCQUFRSCxHQUFHLENBQUNVLElBQVosQ0FGTCxFQUdFO0FBQ0UsUUFBTThELE1BQU0sR0FBRyxDQUFDLHNCQUFReEUsR0FBRyxDQUFDMEUsY0FBWixDQUFELEdBQ1QxRSxHQUFHLENBQUMwRSxjQURLLEdBRVRSLHFCQUZOO0FBR0EsV0FBTyx5QkFBYWxFLEdBQUcsQ0FBQ04sS0FBakIsRUFBd0I7QUFDM0J3QyxNQUFBQSxRQUFRLEVBQUVsQyxHQUFHLENBQUNVLElBRGE7QUFFM0I2RCxNQUFBQSxRQUFRLEVBQUV2RSxHQUFHLENBQUNTLGNBRmE7QUFHM0IrRCxNQUFBQSxNQUFNLEVBQU5BLE1BSDJCO0FBSTNCQyxNQUFBQSxLQUFLLEVBQUU7QUFKb0IsS0FBeEIsQ0FBUDtBQU1IOztBQUNELFNBQU96RSxHQUFHLENBQUNOLEtBQVg7QUFDSCxDQTdDTTs7OztBQStDUCxJQUFNaUYsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFDaEQsVUFBRCxFQUFhWSxjQUFiLEVBQWdDO0FBQ3pELE1BQUlxQyxvQkFBYzFFLFFBQWQsQ0FBdUJ5QixVQUF2QixLQUFzQ1ksY0FBMUMsRUFBMEQ7QUFDdEQsV0FBT0UsaUJBQVFDLENBQVIsQ0FBVSxxQkFBVixDQUFQO0FBQ0g7O0FBRUQsTUFBSWtDLG9CQUFjMUUsUUFBZCxDQUF1QnlCLFVBQXZCLEtBQXNDLENBQUNZLGNBQTNDLEVBQTJEO0FBQ3ZELFdBQU9FLGlCQUFRQyxDQUFSLENBQVUsa0JBQVYsQ0FBUDtBQUNIOztBQUVELE1BQUlILGNBQUosRUFBb0I7QUFDaEIsV0FBT0UsaUJBQVFDLENBQVIsQ0FBVSxrQkFBVixDQUFQO0FBQ0g7O0FBRUQsU0FBT0QsaUJBQVFDLENBQVIsQ0FBVSxhQUFWLENBQVA7QUFDSCxDQWREOztBQWdCQSxJQUFNbUMsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFDN0MsVUFBRCxFQUFhTCxVQUFiLEVBQXlCbUQsV0FBekIsRUFBeUM7QUFDbEUsTUFBSSxDQUFDLENBQUMsV0FBRCxFQUFjLFlBQWQsRUFBNEI1RSxRQUE1QixDQUFxQ3lCLFVBQXJDLENBQUwsRUFBdUQ7QUFDbkQsV0FBT21ELFdBQVA7QUFDSDs7QUFDRCxNQUFNQyxZQUFZLEdBQUdELFdBQVcsQ0FBQzdDLElBQVosQ0FDakIsVUFBQ1ksSUFBRDtBQUFBLFdBQVVBLElBQUksQ0FBQzFDLFdBQUwsS0FBcUJ3QixVQUEvQjtBQUFBLEdBRGlCLENBQXJCOztBQUdBLE1BQUlvRCxZQUFKLEVBQWtCO0FBQ2QsV0FBTy9DLFVBQVUsQ0FBQ1ksR0FBWCxDQUFlLFVBQUNDLElBQUQ7QUFBQSxhQUFXO0FBQzdCQyxRQUFBQSxPQUFPLFlBQUtELElBQUksQ0FBQ1YsWUFBVixTQUF5QjRDLFlBQVksQ0FBQ3JFLElBQXRDLENBRHNCO0FBRTdCQSxRQUFBQSxJQUFJLFlBQUttQyxJQUFJLENBQUNWLFlBQVYsU0FBeUI0QyxZQUFZLENBQUNyRSxJQUF0QyxDQUZ5QjtBQUc3QlAsUUFBQUEsV0FBVyxFQUFFd0I7QUFIZ0IsT0FBWDtBQUFBLEtBQWYsQ0FBUDtBQUtIOztBQUNELFNBQU8sRUFBUDtBQUNILENBZkQ7O0FBaUJBLElBQU1uQixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLGNBQUQsRUFBb0I7QUFDekMsTUFBSXVFLFFBQVEsQ0FBQ3ZFLGNBQUQsQ0FBUixHQUEyQixDQUEvQixFQUFrQztBQUM5QiwwQkFBZUEsY0FBZjtBQUNIOztBQUNELFNBQU8sTUFBUDtBQUNILENBTEQ7O0FBT08sSUFBTXdFLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ3RELFVBQUQsRUFBZ0I7QUFDOUMsTUFBSUcsZ0NBQTBCNUIsUUFBMUIsQ0FBbUN5QixVQUFuQyxDQUFKLEVBQW9EO0FBQ2hELFdBQU87QUFBRW9CLE1BQUFBLElBQUksRUFBRTtBQUFSLEtBQVA7QUFDSDs7QUFDRCxTQUFPO0FBQUVtQyxJQUFBQSxXQUFXLEVBQUU7QUFBZixHQUFQO0FBQ0gsQ0FMTTs7OztBQU9QLElBQU1DLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBQ0MsY0FBRCxFQUFvQjtBQUN0QyxNQUFJQSxjQUFKLEVBQW9CO0FBQ2hCLFdBQU9BLGNBQWMsQ0FBQ0MsSUFBZixDQUFvQnBELElBQXBCLGtDQUFQO0FBQ0g7O0FBQ0QsU0FBTyxJQUFQO0FBQ0gsQ0FMRDs7QUFPTyxJQUFNcUQscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUNqQy9DLGNBRGlDLEVBRWpDZ0QsVUFGaUMsRUFHakM1RCxVQUhpQyxFQUloQztBQUNELE1BQU1vQixJQUFJLEdBQUc2QixvQkFBYzFFLFFBQWQsQ0FBdUJ5QixVQUF2QixJQUFxQyxNQUFyQyxHQUE4Q0EsVUFBM0Q7QUFDQSxNQUFNNkQsS0FBSyxHQUFHRCxVQUFVLENBQ25CbEMsTUFEUyxDQUNGLFVBQUNSLElBQUQ7QUFBQSxXQUFVQSxJQUFJLENBQUMxQyxXQUFMLEtBQXFCNEMsSUFBL0I7QUFBQSxHQURFLEVBRVRILEdBRlMsQ0FFTCxVQUFDQyxJQUFEO0FBQUEsV0FBVztBQUNaTCxNQUFBQSxLQUFLLEVBQUVLLElBQUksQ0FBQ0MsT0FEQTtBQUVacEQsTUFBQUEsS0FBSyxFQUFFbUQsSUFBSSxDQUFDekM7QUFGQSxLQUFYO0FBQUEsR0FGSyxDQUFkO0FBTUEsVUFDSTtBQUNJb0MsSUFBQUEsS0FBSyxFQUFFRCxjQUFjLEdBQ2ZFLGlCQUFRQyxDQUFSLENBQVUsb0JBQVYsQ0FEZSxHQUVmRCxpQkFBUUMsQ0FBUixDQUFVLGVBQVYsQ0FIVjtBQUlJaEQsSUFBQUEsS0FBSyxFQUFFLEVBSlg7QUFLSWlELElBQUFBLFFBQVEsRUFBRTtBQUxkLEdBREosNEJBUU82QyxLQVJQO0FBVUgsQ0F0Qk07Ozs7QUF3QkEsSUFBTUMsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDQyxnQkFBRCxFQUFzQjtBQUMvQyxNQUFJQSxnQkFBZ0IsS0FBS0MsNkJBQXpCLEVBQWtEO0FBQzlDLFdBQU9sRCxpQkFBUUMsQ0FBUixDQUFVLFlBQVYsQ0FBUDtBQUNIOztBQUNELFNBQU9ELGlCQUFRQyxDQUFSLENBQVUsZUFBVixDQUFQO0FBQ0gsQ0FMTTs7OztBQU9BLElBQU1rRCx1QkFBdUIsR0FBRyxTQUExQkEsdUJBQTBCLENBQUM5RSxlQUFELEVBQWtCQyxlQUFsQixFQUFzQztBQUN6RSxNQUFJQSxlQUFKLEVBQXFCO0FBQ2pCLFdBQU8wQixpQkFBUUMsQ0FBUixDQUFVLG1CQUFWLENBQVA7QUFDSDs7QUFDRCxNQUFJNUIsZUFBSixFQUFxQjtBQUNqQixXQUFPMkIsaUJBQVFDLENBQVIsQ0FBVSxZQUFWLENBQVA7QUFDSDs7QUFDRCxTQUFPRCxpQkFBUUMsQ0FBUixDQUFVLGVBQVYsQ0FBUDtBQUNILENBUk07Ozs7QUFVQSxJQUFNbUQsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDQyxNQUFELEVBQVk7QUFDckMsVUFBUUEsTUFBUjtBQUNJLFNBQUtILDZCQUFMO0FBQ0ksYUFBT2xELGlCQUFRQyxDQUFSLENBQVUsbUJBQVYsQ0FBUDs7QUFDSixTQUFLcUQsNEJBQUw7QUFDSSxhQUFPdEQsaUJBQVFDLENBQVIsQ0FBVSxvQkFBVixDQUFQOztBQUNKLFNBQUtzRCwyQkFBTDtBQUNJLGFBQU92RCxpQkFBUUMsQ0FBUixDQUFVLGdCQUFWLENBQVA7O0FBQ0o7QUFDSSxhQUFPRCxpQkFBUUMsQ0FBUixDQUFVLG9CQUFWLENBQVA7QUFSUjtBQVVILENBWE07Ozs7QUFhQSxJQUFNdUQsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUM1QmpFLFVBRDRCLEVBRTVCTCxVQUY0QixFQUc1QlksY0FINEIsRUFJNUJ1QyxXQUo0QixFQUszQjtBQUNELE1BQU1VLEtBQUssR0FBR1gsb0JBQW9CLENBQUM3QyxVQUFELEVBQWFMLFVBQWIsRUFBeUJtRCxXQUF6QixDQUFwQixDQUNUekIsTUFEUyxDQUNGLFVBQUNSLElBQUQ7QUFBQSxXQUFVQSxJQUFJLENBQUMxQyxXQUFMLEtBQXFCd0IsVUFBL0I7QUFBQSxHQURFLEVBRVRpQixHQUZTLENBRUwsVUFBQ0MsSUFBRDtBQUFBLFdBQVc7QUFDWkwsTUFBQUEsS0FBSyxFQUFFSyxJQUFJLENBQUNDLE9BREE7QUFFWnBELE1BQUFBLEtBQUssRUFBRW1ELElBQUksQ0FBQ25DO0FBRkEsS0FBWDtBQUFBLEdBRkssQ0FBZDtBQU9BLFVBQ0k7QUFDSThCLElBQUFBLEtBQUssRUFBRW1DLG9CQUFvQixDQUFDaEQsVUFBRCxFQUFhWSxjQUFiLENBRC9CO0FBRUk3QyxJQUFBQSxLQUFLLEVBQUUsRUFGWDtBQUdJaUQsSUFBQUEsUUFBUSxFQUFFO0FBSGQsR0FESiw0QkFNTzZDLEtBTlA7QUFRSCxDQXJCTTs7OztBQXVCQSxJQUFNVSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLFdBQUQsRUFBY0MsSUFBZCxFQUF1QjtBQUNwRCxNQUFNQyxPQUFPLHNCQUFPRCxJQUFQLENBQWI7O0FBQ0EsTUFBTUUsS0FBSyxHQUFHRCxPQUFPLENBQUM3RSxNQUFSLEdBQWlCLENBQS9CO0FBQ0E2RSxFQUFBQSxPQUFPLENBQUNFLE1BQVIsQ0FBZUQsS0FBZixFQUFzQixDQUF0QjtBQUNBSCxFQUFBQSxXQUFXLENBQUMsWUFBRCxFQUFlRSxPQUFmLENBQVg7QUFDSCxDQUxNOzs7O0FBT0EsSUFBTUcsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDTCxXQUFELEVBQWNDLElBQWQsRUFBb0JLLFFBQXBCLEVBQWlDO0FBQ2hFTixFQUFBQSxXQUFXLENBQUMsWUFBRCwrQkFBbUJDLElBQW5CLElBQXlCSyxRQUF6QixHQUFYO0FBQ0gsQ0FGTTs7OztBQUlBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ1AsV0FBRCxFQUFjQyxJQUFkLEVBQW9CTyxNQUFwQixFQUE0QkMsU0FBNUIsRUFBMEM7QUFDeEVULEVBQUFBLFdBQVcsQ0FBQyxZQUFELCtCQUNKQyxJQURJLElBRVA7QUFBRVMsSUFBQUEsS0FBSyxFQUFFRixNQUFUO0FBQWlCM0csSUFBQUEsR0FBRyxFQUFFMkcsTUFBdEI7QUFBOEJHLElBQUFBLFdBQVcsRUFBRTtBQUEzQyxHQUZPLEdBQVg7QUFJQUYsRUFBQUEsU0FBUyxDQUFDLENBQUQsQ0FBVDtBQUNILENBTk07Ozs7QUFRQSxJQUFNRyxvQkFBb0IsR0FDN0IsU0FEU0Esb0JBQ1QsQ0FBQzNCLGNBQUQsRUFBaUI0QixjQUFqQjtBQUFBLFNBQW9DLFVBQUNoSCxHQUFELEVBQVM7QUFDekMsUUFBTWlILFFBQVEsR0FBR0MsSUFBSSxDQUFDQyxTQUFMLENBQWVuSCxHQUFmLENBQWpCO0FBQ0FvRixJQUFBQSxjQUFjLENBQUNnQyxTQUFmLENBQXlCQyxPQUF6QjtBQUNBakMsSUFBQUEsY0FBYyxDQUFDa0MsSUFBZixDQUFvQkMsTUFBcEIsNkZBQ3NGdkgsR0FBRyxDQUFDd0gsVUFEMUYsMkJBQ21IeEgsR0FBRyxDQUFDQSxHQUR2SCwrQkFDOElpSCxRQUQ5SSx1QkFDa0tqSCxHQUFHLENBQUM2RyxLQUR0SywwQ0FDeU03RyxHQUFHLENBQUN5SCxLQUQ3TSxnQkFDdU56SCxHQUFHLENBQUMwSCxVQUQzTjtBQUdBdEMsSUFBQUEsY0FBYyxDQUFDdUMsSUFBZixDQUFvQkMsUUFBcEI7QUFDQVosSUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDtBQUNILEdBUkQ7QUFBQSxDQURHOzs7O0FBV0EsSUFBTWEsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDekMsY0FBRCxFQUFpQjRCLGNBQWpCO0FBQUEsU0FBb0MsVUFBQ2hILEdBQUQsRUFBUztBQUMzRSxRQUFJOEgsVUFBVSxHQUFHM0MsYUFBYSxDQUFDQyxjQUFELENBQTlCOztBQUNBLFFBQUkwQyxVQUFKLEVBQWdCO0FBQ1osVUFBTWIsUUFBUSxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZW5ILEdBQWYsQ0FBakI7QUFDQThILE1BQUFBLFVBQVUsQ0FBQ0MsSUFBWCxDQUFnQixpQkFBaEIsRUFBbUMsS0FBbkM7QUFDQUQsTUFBQUEsVUFBVSxDQUFDQyxJQUFYLENBQWdCLGVBQWhCLEVBQWlDZCxRQUFqQztBQUNBYSxNQUFBQSxVQUFVLENBQUNDLElBQVgsQ0FBZ0IsT0FBaEIsRUFBeUIvSCxHQUFHLENBQUM2RyxLQUE3QjtBQUNBaUIsTUFBQUEsVUFBVSxDQUFDQyxJQUFYLENBQWdCLFVBQWhCLEVBQTRCLEtBQTVCO0FBQ0FELE1BQUFBLFVBQVUsQ0FBQ0UsR0FBWCxDQUFlLGtCQUFmLEVBQW1DaEksR0FBRyxDQUFDeUgsS0FBdkM7O0FBQ0EsVUFBSXRHLFlBQVksQ0FBQzJHLFVBQUQsRUFBYSxFQUFiLENBQWhCLEVBQWtDO0FBQzlCM0csUUFBQUEsWUFBWSxDQUFDMkcsVUFBRCxFQUFhLEVBQWIsQ0FBWixDQUE2QkcsU0FBN0IsR0FBeUNqSSxHQUFHLENBQUMwSCxVQUE3QztBQUNILE9BRkQsTUFFTztBQUNISSxRQUFBQSxVQUFVLENBQUNJLElBQVgsQ0FBZ0JsSSxHQUFHLENBQUMwSCxVQUFwQjtBQUNIOztBQUNEVixNQUFBQSxjQUFjLENBQUMsS0FBRCxDQUFkO0FBQ0g7QUFDSixHQWhCaUM7QUFBQSxDQUEzQjs7OztBQWtCQSxJQUFNbUIsMEJBQTBCLEdBQ25DLFNBRFNBLDBCQUNULENBQUN6QyxnQkFBRCxFQUFtQjBDLFVBQW5CLEVBQStCQyxhQUEvQixFQUE4Q0MsZUFBOUM7QUFBQSxTQUNBLFVBQUNDLE1BQUQsRUFBWTtBQUNSLFFBQU1uSSxNQUFNLEdBQUdMLGlCQUFpQixDQUFDd0ksTUFBRCxDQUFoQzs7QUFDQSxRQUFNQyxPQUFPLG1DQUNORCxNQURNO0FBRVRuSSxNQUFBQSxNQUFNLEVBQU5BLE1BRlM7QUFHVHNILE1BQUFBLFVBQVUsRUFBRTFELGlCQUFpQixDQUFDLElBQUQsRUFBT29FLFVBQVAsRUFBbUJHLE1BQW5CO0FBSHBCLE1BQWI7O0FBS0EsUUFBSTdDLGdCQUFnQixLQUFLQyw2QkFBekIsRUFBa0Q7QUFDOUMyQyxNQUFBQSxlQUFlLENBQUNFLE9BQUQsQ0FBZjtBQUNILEtBRkQsTUFFTztBQUNISCxNQUFBQSxhQUFhLENBQUNHLE9BQUQsQ0FBYjtBQUNIO0FBQ0osR0FiRDtBQUFBLENBREc7Ozs7QUFnQkEsSUFBTUMsMkJBQTJCLEdBQ3BDLFNBRFNBLDJCQUNULENBQ0lMLFVBREosRUFFSXJILGVBRkosRUFHSTJFLGdCQUhKLEVBSUkyQyxhQUpKLEVBS0lDLGVBTEosRUFNSUksNkJBTko7QUFBQSxTQVFBLFVBQUNILE1BQUQsRUFBWTtBQUNSLFFBQU1uSSxNQUFNLEdBQUdMLGlCQUFpQixDQUFDd0ksTUFBRCxDQUFoQzs7QUFDQSxRQUFJeEgsZUFBSixFQUFxQjtBQUNqQjJILE1BQUFBLDZCQUE2QixpQ0FDdEJILE1BRHNCO0FBRXpCbkksUUFBQUEsTUFBTSxFQUFOQSxNQUZ5QjtBQUd6QjBHLFFBQUFBLFdBQVcsRUFBRSxLQUhZO0FBSXpCRCxRQUFBQSxLQUFLLEVBQUUwQixNQUFNLENBQUMvRjtBQUpXLFNBQTdCO0FBTUgsS0FQRCxNQU9PO0FBQ0gsVUFBTWdHLE9BQU8sbUNBQ05ELE1BRE07QUFFVGYsUUFBQUEsVUFBVSxFQUFFLEtBRkg7QUFHVHBILFFBQUFBLE1BQU0sRUFBTkEsTUFIUztBQUlUc0gsUUFBQUEsVUFBVSxFQUFFMUQsaUJBQWlCLENBQUMsS0FBRCxFQUFRb0UsVUFBUixFQUFvQkcsTUFBcEI7QUFKcEIsUUFBYjs7QUFNQSxVQUFJN0MsZ0JBQWdCLEtBQUtpRCw4QkFBekIsRUFBbUQ7QUFDL0NMLFFBQUFBLGVBQWUsQ0FBQ0UsT0FBRCxDQUFmO0FBQ0gsT0FGRCxNQUVPO0FBQ0hILFFBQUFBLGFBQWEsQ0FBQ0csT0FBRCxDQUFiO0FBQ0g7QUFDSjtBQUNKLEdBOUJEO0FBQUEsQ0FERzs7OztBQWlDQSxJQUFNSSxzQkFBc0IsR0FDL0IsU0FEU0Esc0JBQ1QsQ0FBQ3pDLFdBQUQsRUFBY25FLFVBQWQsRUFBMEI2RyxLQUExQjtBQUFBLFNBQW9DLFVBQUNuSixLQUFELEVBQVc7QUFDM0MsUUFBTW1ELElBQUksR0FBR2IsVUFBVSxDQUFDQyxJQUFYLENBQWdCLFVBQUNZLElBQUQ7QUFBQSxhQUFVQSxJQUFJLENBQUNULEVBQUwsS0FBWTFDLEtBQXRCO0FBQUEsS0FBaEIsQ0FBYjs7QUFDQSxRQUFJLENBQUMsb0JBQU1tRCxJQUFOLENBQUwsRUFBa0I7QUFDZHNELE1BQUFBLFdBQVcsQ0FBQzBDLEtBQUssQ0FBQyxDQUFELENBQU4sRUFBV2hHLElBQUksQ0FBQzJCLE1BQWhCLENBQVg7QUFDQTJCLE1BQUFBLFdBQVcsQ0FBQzBDLEtBQUssQ0FBQyxDQUFELENBQU4sRUFBV2hHLElBQUksQ0FBQ1YsWUFBaEIsQ0FBWDtBQUNBZ0UsTUFBQUEsV0FBVyxDQUFDMEMsS0FBSyxDQUFDLENBQUQsQ0FBTixFQUFXaEcsSUFBSSxDQUFDakMsTUFBaEIsQ0FBWDtBQUNIOztBQUVELFdBQU9sQixLQUFQO0FBQ0gsR0FURDtBQUFBLENBREc7Ozs7QUFZQSxJQUFNb0oscUJBQXFCLEdBQzlCLFNBRFNBLHFCQUNULENBQUMzQyxXQUFELEVBQWM3RCxjQUFkLEVBQThCa0IsSUFBOUI7QUFBQSxTQUF1QyxVQUFDOUQsS0FBRCxFQUFXO0FBQzlDLFFBQU1xRCxJQUFJLEdBQUdULGNBQWMsQ0FBQ0wsSUFBZixDQUFvQixVQUFDOEcsT0FBRDtBQUFBLGFBQWFBLE9BQU8sQ0FBQ2hHLElBQVIsS0FBaUJyRCxLQUE5QjtBQUFBLEtBQXBCLENBQWI7O0FBQ0EsUUFBSSxDQUFDLG9CQUFNcUQsSUFBTixDQUFMLEVBQWtCO0FBQ2RvRCxNQUFBQSxXQUFXLENBQUMzQyxJQUFELEVBQU9ULElBQUksQ0FBQ3RDLGNBQVosQ0FBWDtBQUNIOztBQUNELFdBQU9mLEtBQVA7QUFDSCxHQU5EO0FBQUEsQ0FERzs7OztBQVNBLElBQU1zSixzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUM3QyxXQUFELEVBQWNsRCxTQUFkO0FBQUEsU0FBNEIsVUFBQ3ZELEtBQUQsRUFBVztBQUN6RSxRQUFNdUosUUFBUSxHQUFHaEcsU0FBUyxDQUFDaEIsSUFBVixDQUFlLFVBQUNZLElBQUQ7QUFBQSxhQUFVQSxJQUFJLENBQUNULEVBQUwsS0FBWTFDLEtBQXRCO0FBQUEsS0FBZixDQUFqQjs7QUFDQSxRQUFJLENBQUMsb0JBQU11SixRQUFOLENBQUwsRUFBc0I7QUFDbEI5QyxNQUFBQSxXQUFXLENBQUMsT0FBRCxFQUFVOEMsUUFBUSxDQUFDekYsSUFBbkIsQ0FBWDtBQUNBMkMsTUFBQUEsV0FBVyxDQUFDLGVBQUQsRUFBa0I4QyxRQUFRLENBQUN6RixJQUEzQixDQUFYO0FBQ0g7O0FBQ0QsV0FBTzlELEtBQVA7QUFDSCxHQVBxQztBQUFBLENBQS9COzs7O0FBU0EsSUFBTXdKLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUN4RixJQUFELEVBQXVDO0FBQUEsTUFBaEN5RixVQUFnQyx1RUFBbkIsRUFBbUI7QUFBQSxNQUFmQyxVQUFlOztBQUM3RCxNQUFJRCxVQUFVLEtBQUssRUFBbkIsRUFBdUI7QUFDbkJDLElBQUFBLFVBQVUsQ0FBQzFGLElBQUQsQ0FBVjtBQUNBO0FBQ0g7O0FBRUQsTUFBTTJGLFlBQVksR0FBRyxFQUFyQjtBQUVBekYsRUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQVosRUFBa0I0RixPQUFsQixDQUEwQixVQUFDeEYsUUFBRCxFQUFjO0FBQ3BDLFFBQUl5RixRQUFRLEdBQUcsS0FBZjtBQUNBLFFBQUkvRCxLQUFLLEdBQUcsRUFBWjtBQUVBNUIsSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQUksQ0FBQ0ksUUFBRCxDQUFKLENBQWV5RSxNQUEzQixFQUFtQ2UsT0FBbkMsQ0FBMkMsVUFBQ0UsV0FBRCxFQUFpQjtBQUN4RCxVQUNJQSxXQUFXLENBQUMzSixXQUFaLEdBQTBCNEosTUFBMUIsQ0FBaUNOLFVBQVUsQ0FBQ3RKLFdBQVgsRUFBakMsS0FDSSxDQURKLElBRUE2RCxJQUFJLENBQUNJLFFBQUQsQ0FBSixDQUFleUUsTUFBZixDQUFzQmlCLFdBQXRCLEVBQW1DaEgsS0FBbkMsQ0FDSzNDLFdBREwsR0FFSzRKLE1BRkwsQ0FFWU4sVUFBVSxDQUFDdEosV0FBWCxFQUZaLEtBRXlDLENBSnpDLElBS0M2RCxJQUFJLENBQUNJLFFBQUQsQ0FBSixDQUFleUUsTUFBZixDQUFzQmlCLFdBQXRCLEVBQW1DRSxXQUFuQyxJQUNHaEcsSUFBSSxDQUFDSSxRQUFELENBQUosQ0FBZXlFLE1BQWYsQ0FBc0JpQixXQUF0QixFQUFtQ0UsV0FBbkMsQ0FDSzdKLFdBREwsR0FFSzRKLE1BRkwsQ0FFWU4sVUFBVSxDQUFDdEosV0FBWCxFQUZaLEtBRXlDLENBUjdDLElBU0M2RCxJQUFJLENBQUNJLFFBQUQsQ0FBSixDQUFleUUsTUFBZixDQUFzQmlCLFdBQXRCLEVBQW1DckosV0FBbkMsSUFDR3VELElBQUksQ0FBQ0ksUUFBRCxDQUFKLENBQWV5RSxNQUFmLENBQXNCaUIsV0FBdEIsRUFBbUNySixXQUFuQyxDQUNLTixXQURMLEdBRUs0SixNQUZMLENBRVlOLFVBQVUsQ0FBQ3RKLFdBQVgsRUFGWixLQUV5QyxDQWJqRCxFQWNFO0FBQ0UwSixRQUFBQSxRQUFRLEdBQUcsSUFBWDtBQUNBL0QsUUFBQUEsS0FBSyxDQUFDZ0UsV0FBRCxDQUFMLEdBQXFCOUYsSUFBSSxDQUFDSSxRQUFELENBQUosQ0FBZXlFLE1BQWYsQ0FBc0JpQixXQUF0QixDQUFyQjtBQUNIO0FBQ0osS0FuQkQ7O0FBcUJBLFFBQUlELFFBQUosRUFBYztBQUNWRixNQUFBQSxZQUFZLENBQUN2RixRQUFELENBQVosR0FBeUI7QUFDckJ0QixRQUFBQSxLQUFLLEVBQUVrQixJQUFJLENBQUNJLFFBQUQsQ0FBSixDQUFldEIsS0FERDtBQUVyQitGLFFBQUFBLE1BQU0sRUFBRS9DO0FBRmEsT0FBekI7QUFJSDtBQUNKLEdBL0JEO0FBaUNBNEQsRUFBQUEsVUFBVSxDQUFDQyxZQUFELENBQVY7QUFDSCxDQTFDTTs7OztBQTRDUCxJQUFNbEYsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ3pFLEtBQUQsRUFBVztBQUN4QixNQUFJLE9BQU9BLEtBQVAsS0FBaUIsUUFBckIsRUFBK0IsT0FBTyxFQUFQO0FBQy9CLFNBQU9BLEtBQUssQ0FBQ2lLLEtBQU4sQ0FBWSxHQUFaLEVBQWlCL0csR0FBakIsQ0FBcUJuRCxVQUFyQixFQUFpQ21LLElBQWpDLENBQXNDLEdBQXRDLENBQVA7QUFDSCxDQUhEOztBQUtPLElBQU1DLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsQ0FBQ0MsVUFBRCxFQUFhOUgsVUFBYixFQUE0QjtBQUNsRSxNQUFNd0QsS0FBSyxHQUFHc0UsVUFBVSxDQUFDbEgsR0FBWCxDQUFlLFVBQUNDLElBQUQsRUFBVTtBQUNuQyxRQUNJLENBQUNBLElBQUksQ0FBQ2lFLFdBQU4sSUFDQW5HLGdDQUEwQlQsUUFBMUIsQ0FBbUMyQyxJQUFJLENBQUMxQyxXQUF4QyxDQURBLElBRUEscUJBQVMwQyxJQUFULEVBQWUsVUFBZixDQUZBLElBR0EscUJBQVNBLElBQVQsRUFBZSxNQUFmLENBSkosRUFLRTtBQUNFLFVBQU1YLFFBQVEsR0FBR0gsaUJBQWlCLENBQUNDLFVBQUQsRUFBYWEsSUFBSSxDQUFDbkMsSUFBbEIsQ0FBbEM7QUFDQSw2Q0FDT21DLElBRFA7QUFFSVgsUUFBQUEsUUFBUSxFQUFFQSxRQUFRLENBQUNFLEVBRnZCO0FBR0l4QixRQUFBQSxNQUFNLEVBQUVzQixRQUFRLENBQUN0QixNQUhyQjtBQUlJNEQsUUFBQUEsTUFBTSxFQUFFdEMsUUFBUSxDQUFDc0M7QUFKckI7QUFNSDs7QUFDRCxXQUFPM0IsSUFBUDtBQUNILEdBaEJhLENBQWQ7QUFpQkEsU0FBTzJDLEtBQVA7QUFDSCxDQW5CTTs7OztBQXFCQSxJQUFNdUUsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBQzVELFdBQUQsRUFBY0MsSUFBZCxFQUF1QjtBQUM1QyxNQUFNNEQsT0FBTyxHQUFHNUQsSUFBSSxDQUFDeEQsR0FBTCxDQUFTLFVBQUNxSCxDQUFEO0FBQUEsV0FBT0EsQ0FBQyxDQUFDakssR0FBVDtBQUFBLEdBQVQsRUFBdUI0SixJQUF2QixDQUE0QixHQUE1QixDQUFoQjtBQUNBekQsRUFBQUEsV0FBVyxDQUFDLEtBQUQsRUFBUTZELE9BQVIsQ0FBWDtBQUNILENBSE0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaTE4bmV4dCBmcm9tICdpMThuZXh0JztcbmltcG9ydCBpc0VtcHR5IGZyb20gJ2xvZGFzaC9pc0VtcHR5JztcbmltcG9ydCBpc05pbCBmcm9tICdsb2Rhc2gvaXNOaWwnO1xuXG5pbXBvcnQge1xuICAgIENSRUFURV9DVVNUT01fVEFHX1NUQVRFLFxuICAgIERPQ1VNRU5UX1RBR1MsXG4gICAgRURJVF9DVVNUT01fVEFHX1NUQVRFLFxuICAgIEVESVRfTElCUkFSWV9UQUdfU1RBVEUsXG4gICAgSU5TRVJUX0xJQlJBUllfVEFHX1NUQVRFLFxuICAgIFNNQVJUX0RPQ1VNRU5UU19DQVRFR09SWSxcbiAgICBUQUdfQUxMX1RFWFRfRk9STUFUX1RZUEVTLFxuICAgIFRBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMsXG4gICAgVEFHX0VDT0xPR0lDX1RZUEVTLFxuICAgIFRBR19OVU1CRVJfRk9STUFUX1RZUEVTLFxuICAgIFRBR19QRVJDRU5UX0ZPUk1BVF9UWVBFUyxcbiAgICBUQUdfVEVYVF9GT1JNQVRfVFlQRVMsXG59IGZyb20gJ0Bjb25zdGFudHMvdGFncyc7XG5cbmltcG9ydCB7IGhhc1ZhbHVlLCBudW1iZXJGb3JtYXQgfSBmcm9tICdAaGVscGVycy91dGlscyc7XG5cbmNvbnN0IGNhcGl0YWxpemUgPSAodmFsdWUpID0+IHtcbiAgICBpZiAodHlwZW9mIHZhbHVlICE9PSAnc3RyaW5nJykgcmV0dXJuICcnO1xuICAgIHJldHVybiAoXG4gICAgICAgIHZhbHVlLnRvTG93ZXJDYXNlKCkuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgK1xuICAgICAgICB2YWx1ZS50b0xvd2VyQ2FzZSgpLnNsaWNlKDEpXG4gICAgKTtcbn07XG5cbmV4cG9ydCBjb25zdCBnZW5lcmF0ZUZvcm1hdHRlciA9ICh0YWcpID0+IHtcbiAgICBpZiAoVEFHX1RFWFRfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSkpIHtcbiAgICAgICAgcmV0dXJuIHRhZy5mb3JtYXQ7XG4gICAgfVxuICAgIGlmIChcbiAgICAgICAgVEFHX0VDT0xPR0lDX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSkgfHxcbiAgICAgICAgVEFHX05VTUJFUl9GT1JNQVRfVFlQRVMuaW5jbHVkZXModGFnLmZvcm1hdF90eXBlKSB8fFxuICAgICAgICBUQUdfUEVSQ0VOVF9GT1JNQVRfVFlQRVMuaW5jbHVkZXModGFnLmZvcm1hdF90eXBlKVxuICAgICkge1xuICAgICAgICByZXR1cm4gYCR7Z2V0UGF0dGVybkZvcm1hdCh0YWcuZGVjaW1hbF9wbGFjZXMpfSAke3RhZy51bml0fWA7XG4gICAgfVxuICAgIGlmIChUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSkpIHtcbiAgICAgICAgcmV0dXJuIGAke3RhZy5zeW1ib2x9JHtnZXRQYXR0ZXJuRm9ybWF0KHRhZy5kZWNpbWFsX3BsYWNlcyl9ICR7XG4gICAgICAgICAgICB0YWcudW5pdFxuICAgICAgICB9YDtcbiAgICB9XG4gICAgcmV0dXJuICcnO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldEJvcmRlclN0eWxlID0gKGlzSW5zZXJ0VGFnTW9kZSwgaXNGcm9tQ3VzdG9tVGFnKSA9PiB7XG4gICAgaWYgKGlzRnJvbUN1c3RvbVRhZyB8fCBpc0luc2VydFRhZ01vZGUpIHtcbiAgICAgICAgcmV0dXJuIHt9O1xuICAgIH1cbiAgICByZXR1cm4ge1xuICAgICAgICBib3JkZXI6IDAsXG4gICAgICAgIHBhZGRpbmc6IDAsXG4gICAgICAgIGJveFNoYWRvdzogJ25vbmUnLFxuICAgIH07XG59O1xuXG5jb25zdCBnZXRDaGlsZE5vZGUgPSAoZWxlbWVudCwgbnVtKSA9PiB7XG4gICAgbGV0IGNoaWxkcmVuID0gZWxlbWVudDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG51bTsgaSsrKSB7XG4gICAgICAgIGlmIChjaGlsZHJlbi5sZW5ndGggPiAwICYmICFpc05pbChjaGlsZHJlblswXS5sYXN0RWxlbWVudENoaWxkKSkge1xuICAgICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlblswXS5jaGlsZHJlbjtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gY2hpbGRyZW5bMF07XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0Q29sU2l6ZSA9IChmb3JtYXRUeXBlLCBzaXplMSwgc2l6ZTIpID0+XG4gICAgVEFHX0FMTF9URVhUX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyhmb3JtYXRUeXBlKSA/IHNpemUxIDogc2l6ZTI7XG5cbmV4cG9ydCBjb25zdCBnZXRDdXJyZW5jeUJ5VW5pdCA9IChjdXJyZW5jaWVzLCB1bml0KSA9PiB7XG4gICAgcmV0dXJuIChcbiAgICAgICAgY3VycmVuY2llcy5maW5kKChjdXJyZW5jeSkgPT4gY3VycmVuY3kuYWJicmV2aWF0aW9uID09PSB1bml0KSB8fCB7XG4gICAgICAgICAgICBpZDogbnVsbCxcbiAgICAgICAgICAgIHN5bWJvbDogJycsXG4gICAgICAgIH1cbiAgICApO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldERlY2ltYWxGb3JtYXRzVG9TZWxlY3QgPSAoZGVjaW1hbEZvcm1hdHMsIGlzSW5pdGlhbGF6aW5nKSA9PiB7XG4gICAgcmV0dXJuIFtcbiAgICAgICAge1xuICAgICAgICAgICAgbGFiZWw6IGlzSW5pdGlhbGF6aW5nXG4gICAgICAgICAgICAgICAgPyBpMThuZXh0LnQoJ0xvYWRpbmcgZGVjaW1hbHMuLi4nKVxuICAgICAgICAgICAgICAgIDogaTE4bmV4dC50KCdTZWxlY3QgZGVjaW1hbHMnKSxcbiAgICAgICAgICAgIHZhbHVlOiAnJyxcbiAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICAuLi5kZWNpbWFsRm9ybWF0cy5tYXAoKGl0ZW0pID0+ICh7XG4gICAgICAgICAgICBsYWJlbDogaXRlbS5leGFtcGxlLFxuICAgICAgICAgICAgdmFsdWU6IGl0ZW0udHlwZSxcbiAgICAgICAgfSkpLFxuICAgIF07XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0RG9jdW1lbnRzVG9TZWxlY3QgPSAoXG4gICAgZG9jdW1lbnRzLFxuICAgIGhhc0ZpbHRlckJ5QWN0aXZlLFxuICAgIGlzRmV0Y2hpbmdcbikgPT4ge1xuICAgIGxldCBkb2N1bWVudHNGaWx0ZXJlZCA9IGRvY3VtZW50cztcbiAgICBpZiAoaGFzRmlsdGVyQnlBY3RpdmUpIHtcbiAgICAgICAgZG9jdW1lbnRzRmlsdGVyZWQgPSBkb2N1bWVudHMuZmlsdGVyKChpdGVtKSA9PiBpdGVtLmlzX2FjdGl2ZSk7XG4gICAgfVxuICAgIHJldHVybiBbXG4gICAgICAgIHtcbiAgICAgICAgICAgIGxhYmVsOiBpc0ZldGNoaW5nXG4gICAgICAgICAgICAgICAgPyBgJHtpMThuZXh0LnQoJ0xvYWRpbmcgZG9jdW1lbnQnLCB7IGNvdW50OiAyIH0pfS4uLmBcbiAgICAgICAgICAgICAgICA6IGkxOG5leHQudCgnU2VsZWN0IGEgZG9jdW1lbnQnKSxcbiAgICAgICAgICAgIHZhbHVlOiAnJyxcbiAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICAuLi5kb2N1bWVudHNGaWx0ZXJlZC5tYXAoKGl0ZW0pID0+ICh7XG4gICAgICAgICAgICBsYWJlbDogaXRlbS5uYW1lLFxuICAgICAgICAgICAgdmFsdWU6IGl0ZW0uaWQsXG4gICAgICAgICAgICBkaXNhYmxlZDogIWl0ZW0uaXNfYWN0aXZlLFxuICAgICAgICB9KSksXG4gICAgXTtcbn07XG5cbmV4cG9ydCBjb25zdCBnZXRGaWx0ZXJlZENhdGVnb3JpZXMgPSAodGFncywgaXNGb3JTbWFydERvY3VtZW50cykgPT4ge1xuICAgIGlmICghaXNGb3JTbWFydERvY3VtZW50cykge1xuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXModGFncykuZmlsdGVyKFxuICAgICAgICAgICAgKGNhdGVnb3J5KSA9PiBjYXRlZ29yeSAhPT0gU01BUlRfRE9DVU1FTlRTX0NBVEVHT1JZXG4gICAgICAgICk7XG4gICAgfVxuICAgIHJldHVybiBPYmplY3Qua2V5cyh0YWdzKTtcbn07XG5cbmV4cG9ydCBjb25zdCBnZXRGb3JtYXR0ZWRWYWx1ZSA9IChpc05ld1RhZywgZGVmYXVsdEN1cnJlbmN5TG9jYWxlLCB0YWcpID0+IHtcbiAgICBpZiAoVEFHX1RFWFRfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSkpIHtcbiAgICAgICAgc3dpdGNoICh0YWcuZm9ybWF0KSB7XG4gICAgICAgICAgICBjYXNlICdNQVnDmlNDVUxBUyc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRhZy52YWx1ZS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgY2FzZSAnbWluw7pzY3VsYXMnOlxuICAgICAgICAgICAgICAgIHJldHVybiB0YWcudmFsdWUudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIGNhc2UgJ05vcm1hbCc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNhcGl0YWxpemUodGFnLnZhbHVlKTtcbiAgICAgICAgICAgIGNhc2UgJ09yYWNpw7NuJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gc2VudGVuY2UodGFnLnZhbHVlKTtcbiAgICAgICAgICAgIGNhc2UgJ1N1bndpc2UnOlxuICAgICAgICAgICAgICAgIHJldHVybiB0YWcudmFsdWU7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaWYgKFxuICAgICAgICBUQUdfRUNPTE9HSUNfVFlQRVMuaW5jbHVkZXModGFnLmZvcm1hdF90eXBlKSB8fFxuICAgICAgICBUQUdfTlVNQkVSX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyh0YWcuZm9ybWF0X3R5cGUpIHx8XG4gICAgICAgIFRBR19QRVJDRU5UX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyh0YWcuZm9ybWF0X3R5cGUpIHx8XG4gICAgICAgIChUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLmluY2x1ZGVzKHRhZy5mb3JtYXRfdHlwZSkgJiYgaXNOZXdUYWcpXG4gICAgKSB7XG4gICAgICAgIGNvbnN0IG5ld1ZhbHVlID1cbiAgICAgICAgICAgIHRhZy5kZWNpbWFsX3BsYWNlcyA9PSAwID8gTWF0aC50cnVuYyh0YWcudmFsdWUpIDogdGFnLnZhbHVlO1xuICAgICAgICByZXR1cm4gbnVtYmVyRm9ybWF0KG5ld1ZhbHVlLCB7XG4gICAgICAgICAgICBkZWNpbWFsczogdGFnLmRlY2ltYWxfcGxhY2VzLFxuICAgICAgICAgICAgbG9jYWxlOiBkZWZhdWx0Q3VycmVuY3lMb2NhbGUsXG4gICAgICAgICAgICBzdHlsZTogJ2RlY2ltYWwnLFxuICAgICAgICAgICAgdW5pdDogdGFnLnVuaXQsXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoXG4gICAgICAgIFRBR19DVVJSRU5DWV9GT1JNQVRfVFlQRVMuaW5jbHVkZXModGFnLmZvcm1hdF90eXBlKSAmJlxuICAgICAgICAhaXNFbXB0eSh0YWcudW5pdClcbiAgICApIHtcbiAgICAgICAgY29uc3QgbG9jYWxlID0gIWlzRW1wdHkodGFnLmN1cnJlbmN5TG9jYWxlKVxuICAgICAgICAgICAgPyB0YWcuY3VycmVuY3lMb2NhbGVcbiAgICAgICAgICAgIDogZGVmYXVsdEN1cnJlbmN5TG9jYWxlO1xuICAgICAgICByZXR1cm4gbnVtYmVyRm9ybWF0KHRhZy52YWx1ZSwge1xuICAgICAgICAgICAgY3VycmVuY3k6IHRhZy51bml0LFxuICAgICAgICAgICAgZGVjaW1hbHM6IHRhZy5kZWNpbWFsX3BsYWNlcyxcbiAgICAgICAgICAgIGxvY2FsZSxcbiAgICAgICAgICAgIHN0eWxlOiAnY3VycmVuY3knLFxuICAgICAgICB9KTtcbiAgICB9XG4gICAgcmV0dXJuIHRhZy52YWx1ZTtcbn07XG5cbmNvbnN0IGdldExhYmVsVGV4dEZvclVuaXRzID0gKGZvcm1hdFR5cGUsIGlzSW5pdGlhbGF6aW5nKSA9PiB7XG4gICAgaWYgKERPQ1VNRU5UX1RBR1MuaW5jbHVkZXMoZm9ybWF0VHlwZSkgJiYgaXNJbml0aWFsYXppbmcpIHtcbiAgICAgICAgcmV0dXJuIGkxOG5leHQudCgnTG9hZGluZyBjYXRhbG9ncy4uLicpO1xuICAgIH1cblxuICAgIGlmIChET0NVTUVOVF9UQUdTLmluY2x1ZGVzKGZvcm1hdFR5cGUpICYmICFpc0luaXRpYWxhemluZykge1xuICAgICAgICByZXR1cm4gaTE4bmV4dC50KCdTZWxlY3QgYSBjYXRhbG9nJyk7XG4gICAgfVxuXG4gICAgaWYgKGlzSW5pdGlhbGF6aW5nKSB7XG4gICAgICAgIHJldHVybiBpMThuZXh0LnQoJ0xvYWRpbmcgdW5pdHMuLi4nKTtcbiAgICB9XG5cbiAgICByZXR1cm4gaTE4bmV4dC50KCdTZWxlY3QgdW5pdCcpO1xufTtcblxuY29uc3QgZ2V0UGFyc2VkVW5pdEZvcm1hdHMgPSAoY3VycmVuY2llcywgZm9ybWF0VHlwZSwgdW5pdEZvcm1hdHMpID0+IHtcbiAgICBpZiAoIVsncHJpY2Uta3doJywgJ3dhdHQtcHJpY2UnXS5pbmNsdWRlcyhmb3JtYXRUeXBlKSkge1xuICAgICAgICByZXR1cm4gdW5pdEZvcm1hdHM7XG4gICAgfVxuICAgIGNvbnN0IHVuaXRGaWx0ZXJlZCA9IHVuaXRGb3JtYXRzLmZpbmQoXG4gICAgICAgIChpdGVtKSA9PiBpdGVtLmZvcm1hdF90eXBlID09PSBmb3JtYXRUeXBlXG4gICAgKTtcbiAgICBpZiAodW5pdEZpbHRlcmVkKSB7XG4gICAgICAgIHJldHVybiBjdXJyZW5jaWVzLm1hcCgoaXRlbSkgPT4gKHtcbiAgICAgICAgICAgIGV4YW1wbGU6IGAke2l0ZW0uYWJicmV2aWF0aW9ufSR7dW5pdEZpbHRlcmVkLnVuaXR9YCxcbiAgICAgICAgICAgIHVuaXQ6IGAke2l0ZW0uYWJicmV2aWF0aW9ufSR7dW5pdEZpbHRlcmVkLnVuaXR9YCxcbiAgICAgICAgICAgIGZvcm1hdF90eXBlOiBmb3JtYXRUeXBlLFxuICAgICAgICB9KSk7XG4gICAgfVxuICAgIHJldHVybiBbXTtcbn07XG5cbmNvbnN0IGdldFBhdHRlcm5Gb3JtYXQgPSAoZGVjaW1hbF9wbGFjZXMpID0+IHtcbiAgICBpZiAocGFyc2VJbnQoZGVjaW1hbF9wbGFjZXMpID4gMCkge1xuICAgICAgICByZXR1cm4gYHs6MCwuJHtkZWNpbWFsX3BsYWNlc31mfWA7XG4gICAgfVxuICAgIHJldHVybiAnezosfSc7XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0UHJvcHNWYWx1ZUZpZWxkID0gKGZvcm1hdFR5cGUpID0+IHtcbiAgICBpZiAoVEFHX0FMTF9URVhUX0ZPUk1BVF9UWVBFUy5pbmNsdWRlcyhmb3JtYXRUeXBlKSkge1xuICAgICAgICByZXR1cm4geyB0eXBlOiAndGV4dCcgfTtcbiAgICB9XG4gICAgcmV0dXJuIHsgb25seU51bWJlcnM6IHRydWUgfTtcbn07XG5cbmNvbnN0IGdldFRhZ0VsZW1lbnQgPSAoZnJvYWxhSW5zdGFuY2UpID0+IHtcbiAgICBpZiAoZnJvYWxhSW5zdGFuY2UpIHtcbiAgICAgICAgcmV0dXJuIGZyb2FsYUluc3RhbmNlLiRib3guZmluZChgLmZyLWNvbW1hbmRbc2VsZWN0ZWQ9XCJ0cnVlXCJdYCk7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldFRhZ0Zvcm1hdHNUb1NlbGVjdCA9IChcbiAgICBpc0luaXRpYWxhemluZyxcbiAgICB0YWdGb3JtYXRzLFxuICAgIGZvcm1hdFR5cGVcbikgPT4ge1xuICAgIGNvbnN0IHR5cGUgPSBET0NVTUVOVF9UQUdTLmluY2x1ZGVzKGZvcm1hdFR5cGUpID8gJ3RleHQnIDogZm9ybWF0VHlwZTtcbiAgICBjb25zdCBpdGVtcyA9IHRhZ0Zvcm1hdHNcbiAgICAgICAgLmZpbHRlcigoaXRlbSkgPT4gaXRlbS5mb3JtYXRfdHlwZSA9PT0gdHlwZSlcbiAgICAgICAgLm1hcCgoaXRlbSkgPT4gKHtcbiAgICAgICAgICAgIGxhYmVsOiBpdGVtLmV4YW1wbGUsXG4gICAgICAgICAgICB2YWx1ZTogaXRlbS5mb3JtYXQsXG4gICAgICAgIH0pKTtcbiAgICByZXR1cm4gW1xuICAgICAgICB7XG4gICAgICAgICAgICBsYWJlbDogaXNJbml0aWFsYXppbmdcbiAgICAgICAgICAgICAgICA/IGkxOG5leHQudCgnTG9hZGluZyBmb3JtYXRzLi4uJylcbiAgICAgICAgICAgICAgICA6IGkxOG5leHQudCgnU2VsZWN0IGZvcm1hdCcpLFxuICAgICAgICAgICAgdmFsdWU6ICcnLFxuICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWUsXG4gICAgICAgIH0sXG4gICAgICAgIC4uLml0ZW1zLFxuICAgIF07XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0VGV4dEJ1dHRvbiA9IChjb250ZW50Vmlld1N0YXRlKSA9PiB7XG4gICAgaWYgKGNvbnRlbnRWaWV3U3RhdGUgPT09IENSRUFURV9DVVNUT01fVEFHX1NUQVRFKSB7XG4gICAgICAgIHJldHVybiBpMThuZXh0LnQoJ0NyZWF0ZSB0YWcnKTtcbiAgICB9XG4gICAgcmV0dXJuIGkxOG5leHQudCgnU2F2ZSBzZXR0aW5ncycpO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldFRhZ0xpYnJhcnlUZXh0QnV0dG9uID0gKGlzSW5zZXJ0VGFnTW9kZSwgaXNGcm9tQ3VzdG9tVGFnKSA9PiB7XG4gICAgaWYgKGlzRnJvbUN1c3RvbVRhZykge1xuICAgICAgICByZXR1cm4gaTE4bmV4dC50KCdJbnNlcnQgdG8gZm9ybXVsYScpO1xuICAgIH1cbiAgICBpZiAoaXNJbnNlcnRUYWdNb2RlKSB7XG4gICAgICAgIHJldHVybiBpMThuZXh0LnQoJ0luc2VydCB0YWcnKTtcbiAgICB9XG4gICAgcmV0dXJuIGkxOG5leHQudCgnU2F2ZSBzZXR0aW5ncycpO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldFRpdGxlTW9kYWwgPSAob3B0aW9uKSA9PiB7XG4gICAgc3dpdGNoIChvcHRpb24pIHtcbiAgICAgICAgY2FzZSBDUkVBVEVfQ1VTVE9NX1RBR19TVEFURTpcbiAgICAgICAgICAgIHJldHVybiBpMThuZXh0LnQoJ0NyZWF0ZSBzbWFydCB0YWdzJyk7XG4gICAgICAgIGNhc2UgRURJVF9MSUJSQVJZX1RBR19TVEFURTpcbiAgICAgICAgICAgIHJldHVybiBpMThuZXh0LnQoJ1NldCB1cCBsaWJyYXJ5IHRhZycpO1xuICAgICAgICBjYXNlIEVESVRfQ1VTVE9NX1RBR19TVEFURTpcbiAgICAgICAgICAgIHJldHVybiBpMThuZXh0LnQoJ1NldCB1cCBvd24gdGFnJyk7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gaTE4bmV4dC50KCdTZWxlY3QgbGlicmFyeSB0YWcnKTtcbiAgICB9XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0VW5pdHNUb1NlbGVjdCA9IChcbiAgICBjdXJyZW5jaWVzLFxuICAgIGZvcm1hdFR5cGUsXG4gICAgaXNJbml0aWFsYXppbmcsXG4gICAgdW5pdEZvcm1hdHNcbikgPT4ge1xuICAgIGNvbnN0IGl0ZW1zID0gZ2V0UGFyc2VkVW5pdEZvcm1hdHMoY3VycmVuY2llcywgZm9ybWF0VHlwZSwgdW5pdEZvcm1hdHMpXG4gICAgICAgIC5maWx0ZXIoKGl0ZW0pID0+IGl0ZW0uZm9ybWF0X3R5cGUgPT09IGZvcm1hdFR5cGUpXG4gICAgICAgIC5tYXAoKGl0ZW0pID0+ICh7XG4gICAgICAgICAgICBsYWJlbDogaXRlbS5leGFtcGxlLFxuICAgICAgICAgICAgdmFsdWU6IGl0ZW0udW5pdCxcbiAgICAgICAgfSkpO1xuXG4gICAgcmV0dXJuIFtcbiAgICAgICAge1xuICAgICAgICAgICAgbGFiZWw6IGdldExhYmVsVGV4dEZvclVuaXRzKGZvcm1hdFR5cGUsIGlzSW5pdGlhbGF6aW5nKSxcbiAgICAgICAgICAgIHZhbHVlOiAnJyxcbiAgICAgICAgICAgIGRpc2FibGVkOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICAuLi5pdGVtcyxcbiAgICBdO1xufTtcblxuZXhwb3J0IGNvbnN0IGhhbmRsZUNsaWNrRXJhc2VyID0gKGNoYW5nZUlucHV0LCBkYXRhKSA9PiB7XG4gICAgY29uc3QgbmV3RGF0YSA9IFsuLi5kYXRhXTtcbiAgICBjb25zdCBpbmRleCA9IG5ld0RhdGEubGVuZ3RoIC0gMTtcbiAgICBuZXdEYXRhLnNwbGljZShpbmRleCwgMSk7XG4gICAgY2hhbmdlSW5wdXQoJ2NhbGN1bGF0b3InLCBuZXdEYXRhKTtcbn07XG5cbmV4cG9ydCBjb25zdCBoYW5kbGVDbGlja09wZXJhdG9yID0gKGNoYW5nZUlucHV0LCBkYXRhLCBvcGVyYXRvcikgPT4ge1xuICAgIGNoYW5nZUlucHV0KCdjYWxjdWxhdG9yJywgWy4uLmRhdGEsIG9wZXJhdG9yXSk7XG59O1xuXG5leHBvcnQgY29uc3QgaGFuZGxlSW5zZXJ0TnVtYmVyID0gKGNoYW5nZUlucHV0LCBkYXRhLCBudW1iZXIsIHNldE51bWJlcikgPT4ge1xuICAgIGNoYW5nZUlucHV0KCdjYWxjdWxhdG9yJywgW1xuICAgICAgICAuLi5kYXRhLFxuICAgICAgICB7IHRpdGxlOiBudW1iZXIsIHRhZzogbnVtYmVyLCBpc19vcGVyYXRvcjogdHJ1ZSB9LFxuICAgIF0pO1xuICAgIHNldE51bWJlcigxKTtcbn07XG5cbmV4cG9ydCBjb25zdCBoYW5kbGVJbnNlcnRUYWdCdWlsZCA9XG4gICAgKGZyb2FsYUluc3RhbmNlLCBzZXRJc09wZW5Nb2RhbCkgPT4gKHRhZykgPT4ge1xuICAgICAgICBjb25zdCBzZXR0aW5ncyA9IEpTT04uc3RyaW5naWZ5KHRhZyk7XG4gICAgICAgIGZyb2FsYUluc3RhbmNlLnNlbGVjdGlvbi5yZXN0b3JlKCk7XG4gICAgICAgIGZyb2FsYUluc3RhbmNlLmh0bWwuaW5zZXJ0KFxuICAgICAgICAgICAgYDxzcGFuPiZuYnNwOyZuYnNwOzwvc3Bhbj48bWFyayBjbGFzcz1cImZyLWNvbW1hbmQgc3Vud2lzZS10YWdcIiBkYXRhLWNhbGN1bGF0b3I9XCIke3RhZy5pc19uZXdfdGFnfVwiIGRhdGEtdGFnPVwiJHt0YWcudGFnfVwiIGRhdGEtc2V0dGluZ3M9JyR7c2V0dGluZ3N9JyB0aXRsZT1cIiR7dGFnLnRpdGxlfVwiIHN0eWxlPVwiYmFja2dyb3VuZC1jb2xvcjogJHt0YWcuY29sb3J9XCI+JHt0YWcudGV4dF92YWx1ZX08L21hcms+PHNwYW4+Jm5ic3A7Jm5ic3A7PC9zcGFuPmBcbiAgICAgICAgKTtcbiAgICAgICAgZnJvYWxhSW5zdGFuY2UudW5kby5zYXZlU3RlcCgpO1xuICAgICAgICBzZXRJc09wZW5Nb2RhbChmYWxzZSk7XG4gICAgfTtcblxuZXhwb3J0IGNvbnN0IGhhbmRsZUVkaXRUYWdCdWlsZCA9IChmcm9hbGFJbnN0YW5jZSwgc2V0SXNPcGVuTW9kYWwpID0+ICh0YWcpID0+IHtcbiAgICBsZXQgdGFnRWxlbWVudCA9IGdldFRhZ0VsZW1lbnQoZnJvYWxhSW5zdGFuY2UpO1xuICAgIGlmICh0YWdFbGVtZW50KSB7XG4gICAgICAgIGNvbnN0IHNldHRpbmdzID0gSlNPTi5zdHJpbmdpZnkodGFnKTtcbiAgICAgICAgdGFnRWxlbWVudC5hdHRyKCdjb250ZW50ZWRpdGFibGUnLCBmYWxzZSk7XG4gICAgICAgIHRhZ0VsZW1lbnQuYXR0cignZGF0YS1zZXR0aW5ncycsIHNldHRpbmdzKTtcbiAgICAgICAgdGFnRWxlbWVudC5hdHRyKCd0aXRsZScsIHRhZy50aXRsZSk7XG4gICAgICAgIHRhZ0VsZW1lbnQuYXR0cignc2VsZWN0ZWQnLCBmYWxzZSk7XG4gICAgICAgIHRhZ0VsZW1lbnQuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgdGFnLmNvbG9yKTtcbiAgICAgICAgaWYgKGdldENoaWxkTm9kZSh0YWdFbGVtZW50LCAxMCkpIHtcbiAgICAgICAgICAgIGdldENoaWxkTm9kZSh0YWdFbGVtZW50LCAxMCkuaW5uZXJUZXh0ID0gdGFnLnRleHRfdmFsdWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0YWdFbGVtZW50LnRleHQodGFnLnRleHRfdmFsdWUpO1xuICAgICAgICB9XG4gICAgICAgIHNldElzT3Blbk1vZGFsKGZhbHNlKTtcbiAgICB9XG59O1xuXG5leHBvcnQgY29uc3QgaGFuZGxlU3VibWl0Q3VzdG9tVGFnQnVpbGQgPVxuICAgIChjb250ZW50Vmlld1N0YXRlLCB0YWdzTG9jYWxlLCBoYW5kbGVFZGl0VGFnLCBoYW5kbGVJbnNlcnRUYWcpID0+XG4gICAgKHZhbHVlcykgPT4ge1xuICAgICAgICBjb25zdCBmb3JtYXQgPSBnZW5lcmF0ZUZvcm1hdHRlcih2YWx1ZXMpO1xuICAgICAgICBjb25zdCBuZXdJdGVtID0ge1xuICAgICAgICAgICAgLi4udmFsdWVzLFxuICAgICAgICAgICAgZm9ybWF0LFxuICAgICAgICAgICAgdGV4dF92YWx1ZTogZ2V0Rm9ybWF0dGVkVmFsdWUodHJ1ZSwgdGFnc0xvY2FsZSwgdmFsdWVzKSxcbiAgICAgICAgfTtcbiAgICAgICAgaWYgKGNvbnRlbnRWaWV3U3RhdGUgPT09IENSRUFURV9DVVNUT01fVEFHX1NUQVRFKSB7XG4gICAgICAgICAgICBoYW5kbGVJbnNlcnRUYWcobmV3SXRlbSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoYW5kbGVFZGl0VGFnKG5ld0l0ZW0pO1xuICAgICAgICB9XG4gICAgfTtcblxuZXhwb3J0IGNvbnN0IGhhbmRsZVN1Ym1pdFRhZ0xpYnJhcnlCdWlsZCA9XG4gICAgKFxuICAgICAgICB0YWdzTG9jYWxlLFxuICAgICAgICBpc0Zyb21DdXN0b21UYWcsXG4gICAgICAgIGNvbnRlbnRWaWV3U3RhdGUsXG4gICAgICAgIGhhbmRsZUVkaXRUYWcsXG4gICAgICAgIGhhbmRsZUluc2VydFRhZyxcbiAgICAgICAgcHJlcGFyZUN1c3RvbVRhZ0Zyb21TZWxlY3RUYWdcbiAgICApID0+XG4gICAgKHZhbHVlcykgPT4ge1xuICAgICAgICBjb25zdCBmb3JtYXQgPSBnZW5lcmF0ZUZvcm1hdHRlcih2YWx1ZXMpO1xuICAgICAgICBpZiAoaXNGcm9tQ3VzdG9tVGFnKSB7XG4gICAgICAgICAgICBwcmVwYXJlQ3VzdG9tVGFnRnJvbVNlbGVjdFRhZyh7XG4gICAgICAgICAgICAgICAgLi4udmFsdWVzLFxuICAgICAgICAgICAgICAgIGZvcm1hdCxcbiAgICAgICAgICAgICAgICBpc19vcGVyYXRvcjogZmFsc2UsXG4gICAgICAgICAgICAgICAgdGl0bGU6IHZhbHVlcy5sYWJlbCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc3QgbmV3SXRlbSA9IHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZXMsXG4gICAgICAgICAgICAgICAgaXNfbmV3X3RhZzogZmFsc2UsXG4gICAgICAgICAgICAgICAgZm9ybWF0LFxuICAgICAgICAgICAgICAgIHRleHRfdmFsdWU6IGdldEZvcm1hdHRlZFZhbHVlKGZhbHNlLCB0YWdzTG9jYWxlLCB2YWx1ZXMpLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGlmIChjb250ZW50Vmlld1N0YXRlID09PSBJTlNFUlRfTElCUkFSWV9UQUdfU1RBVEUpIHtcbiAgICAgICAgICAgICAgICBoYW5kbGVJbnNlcnRUYWcobmV3SXRlbSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGhhbmRsZUVkaXRUYWcobmV3SXRlbSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuXG5leHBvcnQgY29uc3Qgbm9ybWFsaXplQ3VycmVuY3lGaWVsZCA9XG4gICAgKGNoYW5nZUlucHV0LCBjdXJyZW5jaWVzLCBuYW1lcykgPT4gKHZhbHVlKSA9PiB7XG4gICAgICAgIGNvbnN0IGl0ZW0gPSBjdXJyZW5jaWVzLmZpbmQoKGl0ZW0pID0+IGl0ZW0uaWQgPT09IHZhbHVlKTtcbiAgICAgICAgaWYgKCFpc05pbChpdGVtKSkge1xuICAgICAgICAgICAgY2hhbmdlSW5wdXQobmFtZXNbMF0sIGl0ZW0ubG9jYWxlKTtcbiAgICAgICAgICAgIGNoYW5nZUlucHV0KG5hbWVzWzFdLCBpdGVtLmFiYnJldmlhdGlvbik7XG4gICAgICAgICAgICBjaGFuZ2VJbnB1dChuYW1lc1syXSwgaXRlbS5zeW1ib2wpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHZhbHVlO1xuICAgIH07XG5cbmV4cG9ydCBjb25zdCBub3JtYWxpemVEZWNpbWFsRmllbGQgPVxuICAgIChjaGFuZ2VJbnB1dCwgZGVjaW1hbEZvcm1hdHMsIG5hbWUpID0+ICh2YWx1ZSkgPT4ge1xuICAgICAgICBjb25zdCB0eXBlID0gZGVjaW1hbEZvcm1hdHMuZmluZCgoZGVjaW1hbCkgPT4gZGVjaW1hbC50eXBlID09PSB2YWx1ZSk7XG4gICAgICAgIGlmICghaXNOaWwodHlwZSkpIHtcbiAgICAgICAgICAgIGNoYW5nZUlucHV0KG5hbWUsIHR5cGUuZGVjaW1hbF9wbGFjZXMpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9O1xuXG5leHBvcnQgY29uc3Qgbm9ybWFsaXplRG9jdW1lbnRGaWVsZCA9IChjaGFuZ2VJbnB1dCwgZG9jdW1lbnRzKSA9PiAodmFsdWUpID0+IHtcbiAgICBjb25zdCBkb2N1bWVudCA9IGRvY3VtZW50cy5maW5kKChpdGVtKSA9PiBpdGVtLmlkID09PSB2YWx1ZSk7XG4gICAgaWYgKCFpc05pbChkb2N1bWVudCkpIHtcbiAgICAgICAgY2hhbmdlSW5wdXQoJ3ZhbHVlJywgZG9jdW1lbnQubmFtZSk7XG4gICAgICAgIGNoYW5nZUlucHV0KCdkZWZhdWx0X3ZhbHVlJywgZG9jdW1lbnQubmFtZSk7XG4gICAgfVxuICAgIHJldHVybiB2YWx1ZTtcbn07XG5cbmV4cG9ydCBjb25zdCBzZWFyY2hUYWdzID0gKHRhZ3MsIHNlYXJjaFRleHQgPSAnJywgZmlsdGVyVGFncykgPT4ge1xuICAgIGlmIChzZWFyY2hUZXh0ID09PSAnJykge1xuICAgICAgICBmaWx0ZXJUYWdzKHRhZ3MpO1xuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc3QgdGVtcEZpbHRlcmVkID0ge307XG5cbiAgICBPYmplY3Qua2V5cyh0YWdzKS5mb3JFYWNoKChjYXRlZ29yeSkgPT4ge1xuICAgICAgICBsZXQgaGFzSXRlbXMgPSBmYWxzZTtcbiAgICAgICAgbGV0IGl0ZW1zID0ge307XG5cbiAgICAgICAgT2JqZWN0LmtleXModGFnc1tjYXRlZ29yeV0udmFsdWVzKS5mb3JFYWNoKChzdWJjYXRlZ29yeSkgPT4ge1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgIHN1YmNhdGVnb3J5LnRvVXBwZXJDYXNlKCkuc2VhcmNoKHNlYXJjaFRleHQudG9VcHBlckNhc2UoKSkgPj1cbiAgICAgICAgICAgICAgICAgICAgMCB8fFxuICAgICAgICAgICAgICAgIHRhZ3NbY2F0ZWdvcnldLnZhbHVlc1tzdWJjYXRlZ29yeV0ubGFiZWxcbiAgICAgICAgICAgICAgICAgICAgLnRvVXBwZXJDYXNlKClcbiAgICAgICAgICAgICAgICAgICAgLnNlYXJjaChzZWFyY2hUZXh0LnRvVXBwZXJDYXNlKCkpID49IDAgfHxcbiAgICAgICAgICAgICAgICAodGFnc1tjYXRlZ29yeV0udmFsdWVzW3N1YmNhdGVnb3J5XS5kZXNjcmlwdGlvbiAmJlxuICAgICAgICAgICAgICAgICAgICB0YWdzW2NhdGVnb3J5XS52YWx1ZXNbc3ViY2F0ZWdvcnldLmRlc2NyaXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAudG9VcHBlckNhc2UoKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnNlYXJjaChzZWFyY2hUZXh0LnRvVXBwZXJDYXNlKCkpID49IDApIHx8XG4gICAgICAgICAgICAgICAgKHRhZ3NbY2F0ZWdvcnldLnZhbHVlc1tzdWJjYXRlZ29yeV0uZm9ybWF0X3R5cGUgJiZcbiAgICAgICAgICAgICAgICAgICAgdGFnc1tjYXRlZ29yeV0udmFsdWVzW3N1YmNhdGVnb3J5XS5mb3JtYXRfdHlwZVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRvVXBwZXJDYXNlKClcbiAgICAgICAgICAgICAgICAgICAgICAgIC5zZWFyY2goc2VhcmNoVGV4dC50b1VwcGVyQ2FzZSgpKSA+PSAwKVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgaGFzSXRlbXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGl0ZW1zW3N1YmNhdGVnb3J5XSA9IHRhZ3NbY2F0ZWdvcnldLnZhbHVlc1tzdWJjYXRlZ29yeV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChoYXNJdGVtcykge1xuICAgICAgICAgICAgdGVtcEZpbHRlcmVkW2NhdGVnb3J5XSA9IHtcbiAgICAgICAgICAgICAgICBsYWJlbDogdGFnc1tjYXRlZ29yeV0ubGFiZWwsXG4gICAgICAgICAgICAgICAgdmFsdWVzOiBpdGVtcyxcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIGZpbHRlclRhZ3ModGVtcEZpbHRlcmVkKTtcbn07XG5cbmNvbnN0IHNlbnRlbmNlID0gKHZhbHVlKSA9PiB7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSAhPT0gJ3N0cmluZycpIHJldHVybiAnJztcbiAgICByZXR1cm4gdmFsdWUuc3BsaXQoJyAnKS5tYXAoY2FwaXRhbGl6ZSkuam9pbignICcpO1xufTtcblxuZXhwb3J0IGNvbnN0IHVwZGF0ZU11bHRpcGxlQ3VycmVuY3lUYWdzID0gKGNhbGN1bGF0b3IsIGN1cnJlbmNpZXMpID0+IHtcbiAgICBjb25zdCBpdGVtcyA9IGNhbGN1bGF0b3IubWFwKChpdGVtKSA9PiB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICFpdGVtLmlzX29wZXJhdG9yICYmXG4gICAgICAgICAgICBUQUdfQ1VSUkVOQ1lfRk9STUFUX1RZUEVTLmluY2x1ZGVzKGl0ZW0uZm9ybWF0X3R5cGUpICYmXG4gICAgICAgICAgICBoYXNWYWx1ZShpdGVtLCAnY3VycmVuY3knKSAmJlxuICAgICAgICAgICAgaGFzVmFsdWUoaXRlbSwgJ3VuaXQnKVxuICAgICAgICApIHtcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbmN5ID0gZ2V0Q3VycmVuY3lCeVVuaXQoY3VycmVuY2llcywgaXRlbS51bml0KTtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgLi4uaXRlbSxcbiAgICAgICAgICAgICAgICBjdXJyZW5jeTogY3VycmVuY3kuaWQsXG4gICAgICAgICAgICAgICAgc3ltYm9sOiBjdXJyZW5jeS5zeW1ib2wsXG4gICAgICAgICAgICAgICAgbG9jYWxlOiBjdXJyZW5jeS5sb2NhbGUsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBpdGVtO1xuICAgIH0pO1xuICAgIHJldHVybiBpdGVtcztcbn07XG5cbmV4cG9ydCBjb25zdCB1cGRhdGVUYWcgPSAoY2hhbmdlSW5wdXQsIGRhdGEpID0+IHtcbiAgICBjb25zdCBmb3JtdWxhID0gZGF0YS5tYXAoKGUpID0+IGUudGFnKS5qb2luKCcgJyk7XG4gICAgY2hhbmdlSW5wdXQoJ3RhZycsIGZvcm11bGEpO1xufTtcbiJdfQ==