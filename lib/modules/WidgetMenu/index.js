"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _TitleWithDetail = _interopRequireDefault(require("../../components/TitleWithDetail"));

var _types = require("../../constants/types");

var _contexts = require("../../helpers/contexts");

var mainSelectors = _interopRequireWildcard(require("../main/selectors"));

var templateViewActions = _interopRequireWildcard(require("../TemplateView/actions"));

var templateViewSelectors = _interopRequireWildcard(require("../TemplateView/selectors"));

var templateCoreActions = _interopRequireWildcard(require("../TemplateCore/actions"));

var templateCoreSelectors = _interopRequireWildcard(require("../TemplateCore/selectors"));

var _WidgetItem = _interopRequireDefault(require("./components/WidgetItem"));

var _helpers = require("./helpers");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Section = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    border-top: 1px solid #eff1fb;\n    display: block;\n    padding: 27px;\n    ", "\n"])), function (_ref) {
  var disabled = _ref.disabled;
  return disabled && "opacity: 0.6;\n        cursor: not-allowed;\n        pointer-events: none;";
});

var Separator = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    width: calc(100% - 16px);\n    display: block;\n    border-top: 1px solid #eff1fb;\n    margin: 10px auto;\n    margin-bottom: 10px;\n"])));

var WidgetsMenu = function WidgetsMenu(_ref2) {
  var addWidget = _ref2.addWidget,
      canUpdatePanelsSowing = _ref2.canUpdatePanelsSowing,
      canUpdateProjectLocation = _ref2.canUpdateProjectLocation,
      chartWidgets = _ref2.chartWidgets,
      disabled = _ref2.disabled,
      selectedPageId = _ref2.selectedPageId,
      selectedPageIsBlocked = _ref2.selectedPageIsBlocked,
      selectedPageInfiniteMode = _ref2.selectedPageInfiniteMode,
      selectedPageOrientation = _ref2.selectedPageOrientation,
      setDroppingItem = _ref2.setDroppingItem,
      tableWidgets = _ref2.tableWidgets,
      templateType = _ref2.templateType;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      onChangeInPage = _useContext.onChangeInPage;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useState = (0, _react.useState)(selectedPageInfiniteMode),
      _useState2 = _slicedToArray(_useState, 2),
      infiniteModeControl = _useState2[0],
      setInfiniteModeControl = _useState2[1];

  var hasSummarySupport = (0, _react.useMemo)(function () {
    return templateType === _types.MULTIPROPOSAL_TYPE;
  }, [templateType]);

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _helpers.prepareWidgetsData)(tableWidgets, chartWidgets, canUpdateProjectLocation, canUpdatePanelsSowing);
  }, [tableWidgets, chartWidgets, canUpdateProjectLocation, canUpdatePanelsSowing]),
      basicElements = _useMemo.basicElements,
      tableElements = _useMemo.tableElements,
      graphElements = _useMemo.graphElements;

  (0, _react.useEffect)(function () {
    if (selectedPageInfiniteMode !== infiniteModeControl) {
      setInfiniteModeControl(selectedPageInfiniteMode);
    }
  }, [selectedPageInfiniteMode]);
  var handleSetDroppingItem = (0, _react.useCallback)(function (id) {
    setDroppingItem(id ? (0, _helpers.getPlaceholderData)(id, [].concat(_toConsumableArray(tableWidgets), _toConsumableArray(chartWidgets)), selectedPageOrientation) : null);
  }, [tableWidgets, chartWidgets, selectedPageOrientation]);
  var handleAddNewItem = (0, _react.useCallback)(function (type) {
    return addWidget(selectedPageId, type, null, hasSummarySupport, onChangeInPage);
  }, [selectedPageId, hasSummarySupport, onChangeInPage]);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Section, {
    disabled: selectedPageIsBlocked || disabled,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_TitleWithDetail.default, {
      className: "mb-0",
      children: t('Components')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Container, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        className: "mt-2",
        children: basicElements.map(function (item) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetItem.default, {
            data: item,
            onSetDroppingItem: handleSetDroppingItem,
            onAddNewItem: handleAddNewItem
          }, item.id);
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Separator, {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        children: tableElements.map(function (item) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetItem.default, {
            data: item,
            onSetDroppingItem: handleSetDroppingItem,
            onAddNewItem: handleAddNewItem
          }, item.id);
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Separator, {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
        children: graphElements.map(function (item) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetItem.default, {
            data: item,
            onSetDroppingItem: handleSetDroppingItem,
            onAddNewItem: handleAddNewItem
          }, item.id);
        })
      })]
    })]
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  chartWidgets: mainSelectors.getDataFetchChartWidgets,
  selectedPageId: templateViewSelectors.getSelectedPageId,
  selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked,
  selectedPageInfiniteMode: templateViewSelectors.getSelectedPageInfiniteMode,
  selectedPageOrientation: templateViewSelectors.getSelectedPageOrientation,
  tableWidgets: mainSelectors.getDataFetchTableWidgets,
  templateType: templateCoreSelectors.getCurrentTemplateType
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addWidget: function addWidget(pageId, type, defaultPosition, hasSummarySupport, onChangeInPage) {
      return dispatch(templateCoreActions.addWidget(pageId, type, defaultPosition, hasSummarySupport, onChangeInPage));
    },
    setDroppingItem: function setDroppingItem(item) {
      return dispatch(templateViewActions.droppingItem(item));
    }
  };
};

WidgetsMenu.propTypes = {
  addWidget: _propTypes.default.func,
  canUpdatePanelsSowing: _propTypes.default.bool,
  canUpdateProjectLocation: _propTypes.default.bool,
  chartWidgets: _propTypes.default.array,
  disabled: _propTypes.default.bool,
  selectedPageId: _propTypes.default.string,
  selectedPageIsBlocked: _propTypes.default.bool,
  selectedPageInfiniteMode: _propTypes.default.bool,
  selectedPageOrientation: _propTypes.default.string,
  setDroppingItem: _propTypes.default.func,
  tableWidgets: _propTypes.default.array,
  templateType: _propTypes.default.number
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(WidgetsMenu);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1dpZGdldE1lbnUvaW5kZXguanMiXSwibmFtZXMiOlsiU2VjdGlvbiIsInN0eWxlZCIsImRpdiIsImRpc2FibGVkIiwiU2VwYXJhdG9yIiwiV2lkZ2V0c01lbnUiLCJhZGRXaWRnZXQiLCJjYW5VcGRhdGVQYW5lbHNTb3dpbmciLCJjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb24iLCJjaGFydFdpZGdldHMiLCJzZWxlY3RlZFBhZ2VJZCIsInNlbGVjdGVkUGFnZUlzQmxvY2tlZCIsInNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSIsInNlbGVjdGVkUGFnZU9yaWVudGF0aW9uIiwic2V0RHJvcHBpbmdJdGVtIiwidGFibGVXaWRnZXRzIiwidGVtcGxhdGVUeXBlIiwiR2VuZXJhbENvbnRleHQiLCJvbkNoYW5nZUluUGFnZSIsInQiLCJpbmZpbml0ZU1vZGVDb250cm9sIiwic2V0SW5maW5pdGVNb2RlQ29udHJvbCIsImhhc1N1bW1hcnlTdXBwb3J0IiwiTVVMVElQUk9QT1NBTF9UWVBFIiwiYmFzaWNFbGVtZW50cyIsInRhYmxlRWxlbWVudHMiLCJncmFwaEVsZW1lbnRzIiwiaGFuZGxlU2V0RHJvcHBpbmdJdGVtIiwiaWQiLCJoYW5kbGVBZGROZXdJdGVtIiwidHlwZSIsIm1hcCIsIml0ZW0iLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYWluU2VsZWN0b3JzIiwiZ2V0RGF0YUZldGNoQ2hhcnRXaWRnZXRzIiwidGVtcGxhdGVWaWV3U2VsZWN0b3JzIiwiZ2V0U2VsZWN0ZWRQYWdlSWQiLCJnZXRTZWxlY3RlZFBhZ2VJc0Jsb2NrZWQiLCJnZXRTZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGUiLCJnZXRTZWxlY3RlZFBhZ2VPcmllbnRhdGlvbiIsImdldERhdGFGZXRjaFRhYmxlV2lkZ2V0cyIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVR5cGUiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsInBhZ2VJZCIsImRlZmF1bHRQb3NpdGlvbiIsInRlbXBsYXRlQ29yZUFjdGlvbnMiLCJ0ZW1wbGF0ZVZpZXdBY3Rpb25zIiwiZHJvcHBpbmdJdGVtIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsImJvb2wiLCJhcnJheSIsInN0cmluZyIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxPQUFPLEdBQUdDLDBCQUFPQyxHQUFWLCtKQUlQO0FBQUEsTUFBR0MsUUFBSCxRQUFHQSxRQUFIO0FBQUEsU0FDRUEsUUFBUSxnRkFEVjtBQUFBLENBSk8sQ0FBYjs7QUFXQSxJQUFNQyxTQUFTLEdBQUdILDBCQUFPQyxHQUFWLG1OQUFmOztBQVFBLElBQU1HLFdBQVcsR0FBRyxTQUFkQSxXQUFjLFFBYWQ7QUFBQSxNQVpGQyxTQVlFLFNBWkZBLFNBWUU7QUFBQSxNQVhGQyxxQkFXRSxTQVhGQSxxQkFXRTtBQUFBLE1BVkZDLHdCQVVFLFNBVkZBLHdCQVVFO0FBQUEsTUFURkMsWUFTRSxTQVRGQSxZQVNFO0FBQUEsTUFSRk4sUUFRRSxTQVJGQSxRQVFFO0FBQUEsTUFQRk8sY0FPRSxTQVBGQSxjQU9FO0FBQUEsTUFORkMscUJBTUUsU0FORkEscUJBTUU7QUFBQSxNQUxGQyx3QkFLRSxTQUxGQSx3QkFLRTtBQUFBLE1BSkZDLHVCQUlFLFNBSkZBLHVCQUlFO0FBQUEsTUFIRkMsZUFHRSxTQUhGQSxlQUdFO0FBQUEsTUFGRkMsWUFFRSxTQUZGQSxZQUVFO0FBQUEsTUFERkMsWUFDRSxTQURGQSxZQUNFOztBQUNGLG9CQUEyQix1QkFBV0Msd0JBQVgsQ0FBM0I7QUFBQSxNQUFRQyxjQUFSLGVBQVFBLGNBQVI7O0FBQ0Esd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUVBLGtCQUFzRCxxQkFDbERQLHdCQURrRCxDQUF0RDtBQUFBO0FBQUEsTUFBT1EsbUJBQVA7QUFBQSxNQUE0QkMsc0JBQTVCOztBQUdBLE1BQU1DLGlCQUFpQixHQUFHLG9CQUN0QjtBQUFBLFdBQU1OLFlBQVksS0FBS08seUJBQXZCO0FBQUEsR0FEc0IsRUFFdEIsQ0FBQ1AsWUFBRCxDQUZzQixDQUExQjs7QUFLQSxpQkFBd0Qsb0JBQ3BEO0FBQUEsV0FDSSxpQ0FDSUQsWUFESixFQUVJTixZQUZKLEVBR0lELHdCQUhKLEVBSUlELHFCQUpKLENBREo7QUFBQSxHQURvRCxFQVFwRCxDQUNJUSxZQURKLEVBRUlOLFlBRkosRUFHSUQsd0JBSEosRUFJSUQscUJBSkosQ0FSb0QsQ0FBeEQ7QUFBQSxNQUFRaUIsYUFBUixZQUFRQSxhQUFSO0FBQUEsTUFBdUJDLGFBQXZCLFlBQXVCQSxhQUF2QjtBQUFBLE1BQXNDQyxhQUF0QyxZQUFzQ0EsYUFBdEM7O0FBZ0JBLHdCQUFVLFlBQU07QUFDWixRQUFJZCx3QkFBd0IsS0FBS1EsbUJBQWpDLEVBQXNEO0FBQ2xEQyxNQUFBQSxzQkFBc0IsQ0FBQ1Qsd0JBQUQsQ0FBdEI7QUFDSDtBQUNKLEdBSkQsRUFJRyxDQUFDQSx3QkFBRCxDQUpIO0FBTUEsTUFBTWUscUJBQXFCLEdBQUcsd0JBQzFCLFVBQUNDLEVBQUQsRUFBUTtBQUNKZCxJQUFBQSxlQUFlLENBQ1hjLEVBQUUsR0FDSSxpQ0FDSUEsRUFESiwrQkFFUWIsWUFGUixzQkFFeUJOLFlBRnpCLElBR0lJLHVCQUhKLENBREosR0FNSSxJQVBLLENBQWY7QUFTSCxHQVh5QixFQVkxQixDQUFDRSxZQUFELEVBQWVOLFlBQWYsRUFBNkJJLHVCQUE3QixDQVowQixDQUE5QjtBQWVBLE1BQU1nQixnQkFBZ0IsR0FBRyx3QkFDckIsVUFBQ0MsSUFBRDtBQUFBLFdBQ0l4QixTQUFTLENBQ0xJLGNBREssRUFFTG9CLElBRkssRUFHTCxJQUhLLEVBSUxSLGlCQUpLLEVBS0xKLGNBTEssQ0FEYjtBQUFBLEdBRHFCLEVBU3JCLENBQUNSLGNBQUQsRUFBaUJZLGlCQUFqQixFQUFvQ0osY0FBcEMsQ0FUcUIsQ0FBekI7QUFZQSxzQkFDSSxzQkFBQyxPQUFEO0FBQVMsSUFBQSxRQUFRLEVBQUVQLHFCQUFxQixJQUFJUixRQUE1QztBQUFBLDRCQUNJLHFCQUFDLHdCQUFEO0FBQWlCLE1BQUEsU0FBUyxFQUFDLE1BQTNCO0FBQUEsZ0JBQ0tnQixDQUFDLENBQUMsWUFBRDtBQUROLE1BREosZUFJSSxzQkFBQyx5QkFBRDtBQUFBLDhCQUNJLHFCQUFDLG1CQUFEO0FBQUssUUFBQSxTQUFTLEVBQUMsTUFBZjtBQUFBLGtCQUNLSyxhQUFhLENBQUNPLEdBQWQsQ0FBa0IsVUFBQ0MsSUFBRDtBQUFBLDhCQUNmLHFCQUFDLG1CQUFEO0FBRUksWUFBQSxJQUFJLEVBQUVBLElBRlY7QUFHSSxZQUFBLGlCQUFpQixFQUFFTCxxQkFIdkI7QUFJSSxZQUFBLFlBQVksRUFBRUU7QUFKbEIsYUFDU0csSUFBSSxDQUFDSixFQURkLENBRGU7QUFBQSxTQUFsQjtBQURMLFFBREosZUFXSSxxQkFBQyxTQUFELEtBWEosZUFZSSxxQkFBQyxtQkFBRDtBQUFBLGtCQUNLSCxhQUFhLENBQUNNLEdBQWQsQ0FBa0IsVUFBQ0MsSUFBRDtBQUFBLDhCQUNmLHFCQUFDLG1CQUFEO0FBRUksWUFBQSxJQUFJLEVBQUVBLElBRlY7QUFHSSxZQUFBLGlCQUFpQixFQUFFTCxxQkFIdkI7QUFJSSxZQUFBLFlBQVksRUFBRUU7QUFKbEIsYUFDU0csSUFBSSxDQUFDSixFQURkLENBRGU7QUFBQSxTQUFsQjtBQURMLFFBWkosZUFzQkkscUJBQUMsU0FBRCxLQXRCSixlQXVCSSxxQkFBQyxtQkFBRDtBQUFBLGtCQUNLRixhQUFhLENBQUNLLEdBQWQsQ0FBa0IsVUFBQ0MsSUFBRDtBQUFBLDhCQUNmLHFCQUFDLG1CQUFEO0FBRUksWUFBQSxJQUFJLEVBQUVBLElBRlY7QUFHSSxZQUFBLGlCQUFpQixFQUFFTCxxQkFIdkI7QUFJSSxZQUFBLFlBQVksRUFBRUU7QUFKbEIsYUFDU0csSUFBSSxDQUFDSixFQURkLENBRGU7QUFBQSxTQUFsQjtBQURMLFFBdkJKO0FBQUEsTUFKSjtBQUFBLElBREo7QUF5Q0gsQ0FuSEQ7O0FBcUhBLElBQU1LLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0N4QixFQUFBQSxZQUFZLEVBQUV5QixhQUFhLENBQUNDLHdCQURpQjtBQUU3Q3pCLEVBQUFBLGNBQWMsRUFBRTBCLHFCQUFxQixDQUFDQyxpQkFGTztBQUc3QzFCLEVBQUFBLHFCQUFxQixFQUFFeUIscUJBQXFCLENBQUNFLHdCQUhBO0FBSTdDMUIsRUFBQUEsd0JBQXdCLEVBQUV3QixxQkFBcUIsQ0FBQ0csMkJBSkg7QUFLN0MxQixFQUFBQSx1QkFBdUIsRUFBRXVCLHFCQUFxQixDQUFDSSwwQkFMRjtBQU03Q3pCLEVBQUFBLFlBQVksRUFBRW1CLGFBQWEsQ0FBQ08sd0JBTmlCO0FBTzdDekIsRUFBQUEsWUFBWSxFQUFFMEIscUJBQXFCLENBQUNDO0FBUFMsQ0FBekIsQ0FBeEI7O0FBVUEsSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0Q3ZDLElBQUFBLFNBQVMsRUFBRSxtQkFDUHdDLE1BRE8sRUFFUGhCLElBRk8sRUFHUGlCLGVBSE8sRUFJUHpCLGlCQUpPLEVBS1BKLGNBTE87QUFBQSxhQU9QMkIsUUFBUSxDQUNKRyxtQkFBbUIsQ0FBQzFDLFNBQXBCLENBQ0l3QyxNQURKLEVBRUloQixJQUZKLEVBR0lpQixlQUhKLEVBSUl6QixpQkFKSixFQUtJSixjQUxKLENBREksQ0FQRDtBQUFBLEtBRDJCO0FBaUJ0Q0osSUFBQUEsZUFBZSxFQUFFLHlCQUFDa0IsSUFBRDtBQUFBLGFBQVVhLFFBQVEsQ0FBQ0ksbUJBQW1CLENBQUNDLFlBQXBCLENBQWlDbEIsSUFBakMsQ0FBRCxDQUFsQjtBQUFBO0FBakJxQixHQUFmO0FBQUEsQ0FBM0I7O0FBb0JBM0IsV0FBVyxDQUFDOEMsU0FBWixHQUF3QjtBQUNwQjdDLEVBQUFBLFNBQVMsRUFBRThDLG1CQUFVQyxJQUREO0FBRXBCOUMsRUFBQUEscUJBQXFCLEVBQUU2QyxtQkFBVUUsSUFGYjtBQUdwQjlDLEVBQUFBLHdCQUF3QixFQUFFNEMsbUJBQVVFLElBSGhCO0FBSXBCN0MsRUFBQUEsWUFBWSxFQUFFMkMsbUJBQVVHLEtBSko7QUFLcEJwRCxFQUFBQSxRQUFRLEVBQUVpRCxtQkFBVUUsSUFMQTtBQU1wQjVDLEVBQUFBLGNBQWMsRUFBRTBDLG1CQUFVSSxNQU5OO0FBT3BCN0MsRUFBQUEscUJBQXFCLEVBQUV5QyxtQkFBVUUsSUFQYjtBQVFwQjFDLEVBQUFBLHdCQUF3QixFQUFFd0MsbUJBQVVFLElBUmhCO0FBU3BCekMsRUFBQUEsdUJBQXVCLEVBQUV1QyxtQkFBVUksTUFUZjtBQVVwQjFDLEVBQUFBLGVBQWUsRUFBRXNDLG1CQUFVQyxJQVZQO0FBV3BCdEMsRUFBQUEsWUFBWSxFQUFFcUMsbUJBQVVHLEtBWEo7QUFZcEJ2QyxFQUFBQSxZQUFZLEVBQUVvQyxtQkFBVUs7QUFaSixDQUF4Qjs7ZUFlZSx5QkFBUXhCLGVBQVIsRUFBeUJXLGtCQUF6QixFQUE2Q3ZDLFdBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ2FsbGJhY2ssIHVzZUNvbnRleHQsIHVzZUVmZmVjdCwgdXNlTWVtbywgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBDb250YWluZXIsIFJvdyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgVGl0bGVXaXRoRGV0YWlsIGZyb20gJ0Bjb21wb25lbnRzL1RpdGxlV2l0aERldGFpbCc7XG5cbmltcG9ydCB7IE1VTFRJUFJPUE9TQUxfVFlQRSB9IGZyb20gJ0Bjb25zdGFudHMvdHlwZXMnO1xuXG5pbXBvcnQgeyBHZW5lcmFsQ29udGV4dCB9IGZyb20gJ0BoZWxwZXJzL2NvbnRleHRzJztcblxuaW1wb3J0ICogYXMgbWFpblNlbGVjdG9ycyBmcm9tICdAbWFpbi9zZWxlY3RvcnMnO1xuXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZVZpZXdBY3Rpb25zIGZyb20gJ0Btb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zJztcbmltcG9ydCAqIGFzIHRlbXBsYXRlVmlld1NlbGVjdG9ycyBmcm9tICdAbW9kdWxlcy9UZW1wbGF0ZVZpZXcvc2VsZWN0b3JzJztcblxuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlQWN0aW9ucyBmcm9tICdAdGVtcGxhdGVDb3JlL2FjdGlvbnMnO1xuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlU2VsZWN0b3JzIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvc2VsZWN0b3JzJztcblxuaW1wb3J0IFdpZGdldEl0ZW0gZnJvbSAnLi9jb21wb25lbnRzL1dpZGdldEl0ZW0nO1xuaW1wb3J0IHsgZ2V0UGxhY2Vob2xkZXJEYXRhLCBwcmVwYXJlV2lkZ2V0c0RhdGEgfSBmcm9tICcuL2hlbHBlcnMnO1xuXG5jb25zdCBTZWN0aW9uID0gc3R5bGVkLmRpdmBcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2VmZjFmYjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBwYWRkaW5nOiAyN3B4O1xuICAgICR7KHsgZGlzYWJsZWQgfSkgPT5cbiAgICAgICAgZGlzYWJsZWQgJiZcbiAgICAgICAgYG9wYWNpdHk6IDAuNjtcbiAgICAgICAgY3Vyc29yOiBub3QtYWxsb3dlZDtcbiAgICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7YH1cbmA7XG5cbmNvbnN0IFNlcGFyYXRvciA9IHN0eWxlZC5kaXZgXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDE2cHgpO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZWZmMWZiO1xuICAgIG1hcmdpbjogMTBweCBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG5gO1xuXG5jb25zdCBXaWRnZXRzTWVudSA9ICh7XG4gICAgYWRkV2lkZ2V0LFxuICAgIGNhblVwZGF0ZVBhbmVsc1Nvd2luZyxcbiAgICBjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb24sXG4gICAgY2hhcnRXaWRnZXRzLFxuICAgIGRpc2FibGVkLFxuICAgIHNlbGVjdGVkUGFnZUlkLFxuICAgIHNlbGVjdGVkUGFnZUlzQmxvY2tlZCxcbiAgICBzZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGUsXG4gICAgc2VsZWN0ZWRQYWdlT3JpZW50YXRpb24sXG4gICAgc2V0RHJvcHBpbmdJdGVtLFxuICAgIHRhYmxlV2lkZ2V0cyxcbiAgICB0ZW1wbGF0ZVR5cGUsXG59KSA9PiB7XG4gICAgY29uc3QgeyBvbkNoYW5nZUluUGFnZSB9ID0gdXNlQ29udGV4dChHZW5lcmFsQ29udGV4dCk7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuXG4gICAgY29uc3QgW2luZmluaXRlTW9kZUNvbnRyb2wsIHNldEluZmluaXRlTW9kZUNvbnRyb2xdID0gdXNlU3RhdGUoXG4gICAgICAgIHNlbGVjdGVkUGFnZUluZmluaXRlTW9kZVxuICAgICk7XG4gICAgY29uc3QgaGFzU3VtbWFyeVN1cHBvcnQgPSB1c2VNZW1vKFxuICAgICAgICAoKSA9PiB0ZW1wbGF0ZVR5cGUgPT09IE1VTFRJUFJPUE9TQUxfVFlQRSxcbiAgICAgICAgW3RlbXBsYXRlVHlwZV1cbiAgICApO1xuXG4gICAgY29uc3QgeyBiYXNpY0VsZW1lbnRzLCB0YWJsZUVsZW1lbnRzLCBncmFwaEVsZW1lbnRzIH0gPSB1c2VNZW1vKFxuICAgICAgICAoKSA9PlxuICAgICAgICAgICAgcHJlcGFyZVdpZGdldHNEYXRhKFxuICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0cyxcbiAgICAgICAgICAgICAgICBjaGFydFdpZGdldHMsXG4gICAgICAgICAgICAgICAgY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uLFxuICAgICAgICAgICAgICAgIGNhblVwZGF0ZVBhbmVsc1Nvd2luZ1xuICAgICAgICAgICAgKSxcbiAgICAgICAgW1xuICAgICAgICAgICAgdGFibGVXaWRnZXRzLFxuICAgICAgICAgICAgY2hhcnRXaWRnZXRzLFxuICAgICAgICAgICAgY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uLFxuICAgICAgICAgICAgY2FuVXBkYXRlUGFuZWxzU293aW5nLFxuICAgICAgICBdXG4gICAgKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGUgIT09IGluZmluaXRlTW9kZUNvbnRyb2wpIHtcbiAgICAgICAgICAgIHNldEluZmluaXRlTW9kZUNvbnRyb2woc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlKTtcbiAgICAgICAgfVxuICAgIH0sIFtzZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGVdKTtcblxuICAgIGNvbnN0IGhhbmRsZVNldERyb3BwaW5nSXRlbSA9IHVzZUNhbGxiYWNrKFxuICAgICAgICAoaWQpID0+IHtcbiAgICAgICAgICAgIHNldERyb3BwaW5nSXRlbShcbiAgICAgICAgICAgICAgICBpZFxuICAgICAgICAgICAgICAgICAgICA/IGdldFBsYWNlaG9sZGVyRGF0YShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFsuLi50YWJsZVdpZGdldHMsIC4uLmNoYXJ0V2lkZ2V0c10sXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkUGFnZU9yaWVudGF0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICA6IG51bGxcbiAgICAgICAgICAgICk7XG4gICAgICAgIH0sXG4gICAgICAgIFt0YWJsZVdpZGdldHMsIGNoYXJ0V2lkZ2V0cywgc2VsZWN0ZWRQYWdlT3JpZW50YXRpb25dXG4gICAgKTtcblxuICAgIGNvbnN0IGhhbmRsZUFkZE5ld0l0ZW0gPSB1c2VDYWxsYmFjayhcbiAgICAgICAgKHR5cGUpID0+XG4gICAgICAgICAgICBhZGRXaWRnZXQoXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRQYWdlSWQsXG4gICAgICAgICAgICAgICAgdHlwZSxcbiAgICAgICAgICAgICAgICBudWxsLFxuICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXG4gICAgICAgICAgICApLFxuICAgICAgICBbc2VsZWN0ZWRQYWdlSWQsIGhhc1N1bW1hcnlTdXBwb3J0LCBvbkNoYW5nZUluUGFnZV1cbiAgICApO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFNlY3Rpb24gZGlzYWJsZWQ9e3NlbGVjdGVkUGFnZUlzQmxvY2tlZCB8fCBkaXNhYmxlZH0+XG4gICAgICAgICAgICA8VGl0bGVXaXRoRGV0YWlsIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICB7dCgnQ29tcG9uZW50cycpfVxuICAgICAgICAgICAgPC9UaXRsZVdpdGhEZXRhaWw+XG4gICAgICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxSb3cgY2xhc3NOYW1lPVwibXQtMlwiPlxuICAgICAgICAgICAgICAgICAgICB7YmFzaWNFbGVtZW50cy5tYXAoKGl0ZW0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxXaWRnZXRJdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtpdGVtLmlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE9e2l0ZW19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25TZXREcm9wcGluZ0l0ZW09e2hhbmRsZVNldERyb3BwaW5nSXRlbX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkFkZE5ld0l0ZW09e2hhbmRsZUFkZE5ld0l0ZW19XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICA8L1Jvdz5cbiAgICAgICAgICAgICAgICA8U2VwYXJhdG9yPjwvU2VwYXJhdG9yPlxuICAgICAgICAgICAgICAgIDxSb3c+XG4gICAgICAgICAgICAgICAgICAgIHt0YWJsZUVsZW1lbnRzLm1hcCgoaXRlbSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgPFdpZGdldEl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2l0ZW0uaWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YT17aXRlbX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvblNldERyb3BwaW5nSXRlbT17aGFuZGxlU2V0RHJvcHBpbmdJdGVtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQWRkTmV3SXRlbT17aGFuZGxlQWRkTmV3SXRlbX1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgIDwvUm93PlxuICAgICAgICAgICAgICAgIDxTZXBhcmF0b3I+PC9TZXBhcmF0b3I+XG4gICAgICAgICAgICAgICAgPFJvdz5cbiAgICAgICAgICAgICAgICAgICAge2dyYXBoRWxlbWVudHMubWFwKChpdGVtKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8V2lkZ2V0SXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17aXRlbS5pZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhPXtpdGVtfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uU2V0RHJvcHBpbmdJdGVtPXtoYW5kbGVTZXREcm9wcGluZ0l0ZW19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25BZGROZXdJdGVtPXtoYW5kbGVBZGROZXdJdGVtfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgPC9Sb3c+XG4gICAgICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICAgICAgPC9TZWN0aW9uPlxuICAgICk7XG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xuICAgIGNoYXJ0V2lkZ2V0czogbWFpblNlbGVjdG9ycy5nZXREYXRhRmV0Y2hDaGFydFdpZGdldHMsXG4gICAgc2VsZWN0ZWRQYWdlSWQ6IHRlbXBsYXRlVmlld1NlbGVjdG9ycy5nZXRTZWxlY3RlZFBhZ2VJZCxcbiAgICBzZWxlY3RlZFBhZ2VJc0Jsb2NrZWQ6IHRlbXBsYXRlVmlld1NlbGVjdG9ycy5nZXRTZWxlY3RlZFBhZ2VJc0Jsb2NrZWQsXG4gICAgc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlLFxuICAgIHNlbGVjdGVkUGFnZU9yaWVudGF0aW9uOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlT3JpZW50YXRpb24sXG4gICAgdGFibGVXaWRnZXRzOiBtYWluU2VsZWN0b3JzLmdldERhdGFGZXRjaFRhYmxlV2lkZ2V0cyxcbiAgICB0ZW1wbGF0ZVR5cGU6IHRlbXBsYXRlQ29yZVNlbGVjdG9ycy5nZXRDdXJyZW50VGVtcGxhdGVUeXBlLFxufSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT4gKHtcbiAgICBhZGRXaWRnZXQ6IChcbiAgICAgICAgcGFnZUlkLFxuICAgICAgICB0eXBlLFxuICAgICAgICBkZWZhdWx0UG9zaXRpb24sXG4gICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxuICAgICAgICBvbkNoYW5nZUluUGFnZVxuICAgICkgPT5cbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLmFkZFdpZGdldChcbiAgICAgICAgICAgICAgICBwYWdlSWQsXG4gICAgICAgICAgICAgICAgdHlwZSxcbiAgICAgICAgICAgICAgICBkZWZhdWx0UG9zaXRpb24sXG4gICAgICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcbiAgICAgICAgICAgIClcbiAgICAgICAgKSxcbiAgICBzZXREcm9wcGluZ0l0ZW06IChpdGVtKSA9PiBkaXNwYXRjaCh0ZW1wbGF0ZVZpZXdBY3Rpb25zLmRyb3BwaW5nSXRlbShpdGVtKSksXG59KTtcblxuV2lkZ2V0c01lbnUucHJvcFR5cGVzID0ge1xuICAgIGFkZFdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgY2FuVXBkYXRlUGFuZWxzU293aW5nOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb246IFByb3BUeXBlcy5ib29sLFxuICAgIGNoYXJ0V2lkZ2V0czogUHJvcFR5cGVzLmFycmF5LFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZWxlY3RlZFBhZ2VJZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzZWxlY3RlZFBhZ2VJc0Jsb2NrZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHNlbGVjdGVkUGFnZUluZmluaXRlTW9kZTogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2VsZWN0ZWRQYWdlT3JpZW50YXRpb246IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2V0RHJvcHBpbmdJdGVtOiBQcm9wVHlwZXMuZnVuYyxcbiAgICB0YWJsZVdpZGdldHM6IFByb3BUeXBlcy5hcnJheSxcbiAgICB0ZW1wbGF0ZVR5cGU6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShXaWRnZXRzTWVudSk7XG4iXX0=