"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBootstrap = require("react-bootstrap");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _constants = require("../constants");

var _helpers = require("../helpers");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    background-color: #ffffff;\n    border-radius: 3px;\n    border: 1px solid #ecedf0;\n    color: #848bab;\n    cursor: pointer;\n    display: flex;\n    align-items: center;\n    height: 78px;\n    margin: 4px;\n    transition: all 0.2s linear;\n    position: relative;\n    ", "\n    &.dragging,\n    &:hover {\n        background-color: #202253;\n        border: 1px solid #202253;\n        color: #fff;\n    }\n"])), function (props) {
  return props.group === _constants.BASIC_COMPONENTS ? (0, _styledComponents.css)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n                  flex-direction: column;\n                  justify-content: center;\n              "]))) : (0, _styledComponents.css)(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n                  width: 100%;\n              "])));
});

var IconContent = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    display: flex;\n    justify-content: center;\n    width: 42px;\n    height: 24px;\n    ", "\n    pointer-events: none;\n    svg {\n        height: 24px;\n    }\n"])), function (props) {
  return props.group === _constants.BASIC_COMPONENTS ? (0, _styledComponents.css)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n                  margin-bottom: 4px;\n              "]))) : (0, _styledComponents.css)(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n                  margin-right: 5px;\n              "])));
});

var NameText = _styledComponents.default.span(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    font-size: 11px;\n    line-height: 14px;\n    min-height: 14px;\n    pointer-events: none;\n    text-align: ", ";\n    width: calc(100% - 20px);\n"])), function (props) {
  return props.group === _constants.BASIC_COMPONENTS ? 'center' : 'left';
});

var WidgetItem = function WidgetItem(_ref) {
  var data = _ref.data,
      _ref$draggable = _ref.draggable,
      draggable = _ref$draggable === void 0 ? true : _ref$draggable,
      onAddNewItem = _ref.onAddNewItem,
      onSetDroppingItem = _ref.onSetDroppingItem;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isDragging = _useState2[0],
      setIsDragging = _useState2[1];

  var Icon = data.icon,
      id = data.id,
      name = data.name;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
    xs: data.group === _constants.BASIC_COMPONENTS ? 6 : 18,
    md: data.group === _constants.BASIC_COMPONENTS ? 6 : 18,
    className: "p-0 mb-2",
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
      className: isDragging && 'dragging',
      draggable: "".concat(draggable),
      onDoubleClick: (0, _helpers.onWidgetDoubleClickBuild)(id, onAddNewItem),
      onDragEnd: (0, _helpers.onWidgetDragEndBuild)(onSetDroppingItem, setIsDragging),
      onDragStart: (0, _helpers.onWidgetDragStartBuild)(id, onSetDroppingItem, setIsDragging),
      group: data.group,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(IconContent, {
        group: data.group,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Icon, {})
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(NameText, {
        group: data.group,
        children: name
      })]
    })
  });
};

WidgetItem.propTypes = {
  data: _propTypes.default.object,
  draggable: _propTypes.default.bool,
  onAddNewItem: _propTypes.default.func,
  onSetDroppingItem: _propTypes.default.func
};
var _default = WidgetItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1dpZGdldE1lbnUvY29tcG9uZW50cy9XaWRnZXRJdGVtLmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsInByb3BzIiwiZ3JvdXAiLCJCQVNJQ19DT01QT05FTlRTIiwiY3NzIiwiSWNvbkNvbnRlbnQiLCJOYW1lVGV4dCIsInNwYW4iLCJXaWRnZXRJdGVtIiwiZGF0YSIsImRyYWdnYWJsZSIsIm9uQWRkTmV3SXRlbSIsIm9uU2V0RHJvcHBpbmdJdGVtIiwiaXNEcmFnZ2luZyIsInNldElzRHJhZ2dpbmciLCJJY29uIiwiaWNvbiIsImlkIiwibmFtZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm9iamVjdCIsImJvb2wiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFNQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLG1nQkFhVCxVQUFDQyxLQUFEO0FBQUEsU0FDRUEsS0FBSyxDQUFDQyxLQUFOLEtBQWdCQywyQkFBaEIsT0FDTUMscUJBRE4sc0xBS01BLHFCQUxOLHdIQURGO0FBQUEsQ0FiUyxDQUFmOztBQThCQSxJQUFNQyxXQUFXLEdBQUdOLDBCQUFPQyxHQUFWLCtPQUtYLFVBQUNDLEtBQUQ7QUFBQSxTQUNFQSxLQUFLLENBQUNDLEtBQU4sS0FBZ0JDLDJCQUFoQixPQUNNQyxxQkFETixzSUFJTUEscUJBSk4sOEhBREY7QUFBQSxDQUxXLENBQWpCOztBQW1CQSxJQUFNRSxRQUFRLEdBQUdQLDBCQUFPUSxJQUFWLGdPQUtJLFVBQUNOLEtBQUQ7QUFBQSxTQUNWQSxLQUFLLENBQUNDLEtBQU4sS0FBZ0JDLDJCQUFoQixHQUFtQyxRQUFuQyxHQUE4QyxNQURwQztBQUFBLENBTEosQ0FBZDs7QUFVQSxJQUFNSyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxPQUtiO0FBQUEsTUFKRkMsSUFJRSxRQUpGQSxJQUlFO0FBQUEsNEJBSEZDLFNBR0U7QUFBQSxNQUhGQSxTQUdFLCtCQUhVLElBR1Y7QUFBQSxNQUZGQyxZQUVFLFFBRkZBLFlBRUU7QUFBQSxNQURGQyxpQkFDRSxRQURGQSxpQkFDRTs7QUFDRixrQkFBb0MscUJBQVMsS0FBVCxDQUFwQztBQUFBO0FBQUEsTUFBT0MsVUFBUDtBQUFBLE1BQW1CQyxhQUFuQjs7QUFDQSxNQUFjQyxJQUFkLEdBQWlDTixJQUFqQyxDQUFRTyxJQUFSO0FBQUEsTUFBb0JDLEVBQXBCLEdBQWlDUixJQUFqQyxDQUFvQlEsRUFBcEI7QUFBQSxNQUF3QkMsSUFBeEIsR0FBaUNULElBQWpDLENBQXdCUyxJQUF4QjtBQUVBLHNCQUNJLHFCQUFDLG1CQUFEO0FBQ0ksSUFBQSxFQUFFLEVBQUVULElBQUksQ0FBQ1AsS0FBTCxLQUFlQywyQkFBZixHQUFrQyxDQUFsQyxHQUFzQyxFQUQ5QztBQUVJLElBQUEsRUFBRSxFQUFFTSxJQUFJLENBQUNQLEtBQUwsS0FBZUMsMkJBQWYsR0FBa0MsQ0FBbEMsR0FBc0MsRUFGOUM7QUFHSSxJQUFBLFNBQVMsRUFBQyxVQUhkO0FBQUEsMkJBS0ksc0JBQUMsU0FBRDtBQUNJLE1BQUEsU0FBUyxFQUFFVSxVQUFVLElBQUksVUFEN0I7QUFFSSxNQUFBLFNBQVMsWUFBS0gsU0FBTCxDQUZiO0FBR0ksTUFBQSxhQUFhLEVBQUUsdUNBQXlCTyxFQUF6QixFQUE2Qk4sWUFBN0IsQ0FIbkI7QUFJSSxNQUFBLFNBQVMsRUFBRSxtQ0FDUEMsaUJBRE8sRUFFUEUsYUFGTyxDQUpmO0FBUUksTUFBQSxXQUFXLEVBQUUscUNBQ1RHLEVBRFMsRUFFVEwsaUJBRlMsRUFHVEUsYUFIUyxDQVJqQjtBQWFJLE1BQUEsS0FBSyxFQUFFTCxJQUFJLENBQUNQLEtBYmhCO0FBQUEsOEJBZUkscUJBQUMsV0FBRDtBQUFhLFFBQUEsS0FBSyxFQUFFTyxJQUFJLENBQUNQLEtBQXpCO0FBQUEsK0JBQ0kscUJBQUMsSUFBRDtBQURKLFFBZkosZUFtQkkscUJBQUMsUUFBRDtBQUFVLFFBQUEsS0FBSyxFQUFFTyxJQUFJLENBQUNQLEtBQXRCO0FBQUEsa0JBQThCZ0I7QUFBOUIsUUFuQko7QUFBQTtBQUxKLElBREo7QUE2QkgsQ0F0Q0Q7O0FBd0NBVixVQUFVLENBQUNXLFNBQVgsR0FBdUI7QUFDbkJWLEVBQUFBLElBQUksRUFBRVcsbUJBQVVDLE1BREc7QUFFbkJYLEVBQUFBLFNBQVMsRUFBRVUsbUJBQVVFLElBRkY7QUFHbkJYLEVBQUFBLFlBQVksRUFBRVMsbUJBQVVHLElBSEw7QUFJbkJYLEVBQUFBLGlCQUFpQixFQUFFUSxtQkFBVUc7QUFKVixDQUF2QjtlQU9lZixVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgQ29sIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCBzdHlsZWQsIHsgY3NzIH0gZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBCQVNJQ19DT01QT05FTlRTIH0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmltcG9ydCB7XG4gICAgb25XaWRnZXREb3VibGVDbGlja0J1aWxkLFxuICAgIG9uV2lkZ2V0RHJhZ0VuZEJ1aWxkLFxuICAgIG9uV2lkZ2V0RHJhZ1N0YXJ0QnVpbGQsXG59IGZyb20gJy4uL2hlbHBlcnMnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2VjZWRmMDtcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogNzhweDtcbiAgICBtYXJnaW46IDRweDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBsaW5lYXI7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICR7KHByb3BzKSA9PlxuICAgICAgICBwcm9wcy5ncm91cCA9PT0gQkFTSUNfQ09NUE9ORU5UU1xuICAgICAgICAgICAgPyBjc3NgXG4gICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgICAgICAgIGBcbiAgICAgICAgICAgIDogY3NzYFxuICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgIGB9XG4gICAgJi5kcmFnZ2luZyxcbiAgICAmOmhvdmVyIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzIwMjI1MztcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzIwMjI1MztcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgfVxuYDtcblxuY29uc3QgSWNvbkNvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgd2lkdGg6IDQycHg7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgICR7KHByb3BzKSA9PlxuICAgICAgICBwcm9wcy5ncm91cCA9PT0gQkFTSUNfQ09NUE9ORU5UU1xuICAgICAgICAgICAgPyBjc3NgXG4gICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiA0cHg7XG4gICAgICAgICAgICAgIGBcbiAgICAgICAgICAgIDogY3NzYFxuICAgICAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gICAgICAgICAgICAgIGB9XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gICAgc3ZnIHtcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xuICAgIH1cbmA7XG5cbmNvbnN0IE5hbWVUZXh0ID0gc3R5bGVkLnNwYW5gXG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIG1pbi1oZWlnaHQ6IDE0cHg7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gICAgdGV4dC1hbGlnbjogJHsocHJvcHMpID0+XG4gICAgICAgIHByb3BzLmdyb3VwID09PSBCQVNJQ19DT01QT05FTlRTID8gJ2NlbnRlcicgOiAnbGVmdCd9O1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAyMHB4KTtcbmA7XG5cbmNvbnN0IFdpZGdldEl0ZW0gPSAoe1xuICAgIGRhdGEsXG4gICAgZHJhZ2dhYmxlID0gdHJ1ZSxcbiAgICBvbkFkZE5ld0l0ZW0sXG4gICAgb25TZXREcm9wcGluZ0l0ZW0sXG59KSA9PiB7XG4gICAgY29uc3QgW2lzRHJhZ2dpbmcsIHNldElzRHJhZ2dpbmddID0gdXNlU3RhdGUoZmFsc2UpO1xuICAgIGNvbnN0IHsgaWNvbjogSWNvbiwgaWQsIG5hbWUgfSA9IGRhdGE7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29sXG4gICAgICAgICAgICB4cz17ZGF0YS5ncm91cCA9PT0gQkFTSUNfQ09NUE9ORU5UUyA/IDYgOiAxOH1cbiAgICAgICAgICAgIG1kPXtkYXRhLmdyb3VwID09PSBCQVNJQ19DT01QT05FTlRTID8gNiA6IDE4fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwicC0wIG1iLTJcIlxuICAgICAgICA+XG4gICAgICAgICAgICA8Q29udGFpbmVyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtpc0RyYWdnaW5nICYmICdkcmFnZ2luZyd9XG4gICAgICAgICAgICAgICAgZHJhZ2dhYmxlPXtgJHtkcmFnZ2FibGV9YH1cbiAgICAgICAgICAgICAgICBvbkRvdWJsZUNsaWNrPXtvbldpZGdldERvdWJsZUNsaWNrQnVpbGQoaWQsIG9uQWRkTmV3SXRlbSl9XG4gICAgICAgICAgICAgICAgb25EcmFnRW5kPXtvbldpZGdldERyYWdFbmRCdWlsZChcbiAgICAgICAgICAgICAgICAgICAgb25TZXREcm9wcGluZ0l0ZW0sXG4gICAgICAgICAgICAgICAgICAgIHNldElzRHJhZ2dpbmdcbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIG9uRHJhZ1N0YXJ0PXtvbldpZGdldERyYWdTdGFydEJ1aWxkKFxuICAgICAgICAgICAgICAgICAgICBpZCxcbiAgICAgICAgICAgICAgICAgICAgb25TZXREcm9wcGluZ0l0ZW0sXG4gICAgICAgICAgICAgICAgICAgIHNldElzRHJhZ2dpbmdcbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIGdyb3VwPXtkYXRhLmdyb3VwfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxJY29uQ29udGVudCBncm91cD17ZGF0YS5ncm91cH0+XG4gICAgICAgICAgICAgICAgICAgIDxJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQ29udGVudD5cblxuICAgICAgICAgICAgICAgIDxOYW1lVGV4dCBncm91cD17ZGF0YS5ncm91cH0+e25hbWV9PC9OYW1lVGV4dD5cbiAgICAgICAgICAgIDwvQ29udGFpbmVyPlxuICAgICAgICA8L0NvbD5cbiAgICApO1xufTtcblxuV2lkZ2V0SXRlbS5wcm9wVHlwZXMgPSB7XG4gICAgZGF0YTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBkcmFnZ2FibGU6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uQWRkTmV3SXRlbTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgb25TZXREcm9wcGluZ0l0ZW06IFByb3BUeXBlcy5mdW5jLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgV2lkZ2V0SXRlbTtcbiJdfQ==