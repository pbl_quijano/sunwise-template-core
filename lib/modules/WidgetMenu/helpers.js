"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.prepareWidgetsData = exports.onWidgetDragStartBuild = exports.onWidgetDragEndBuild = exports.onWidgetDoubleClickBuild = exports.getPlaceholderData = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18next = _interopRequireDefault(require("i18next"));

var _template = require("../../constants/template");

var _constants = require("../TemplateCore/constants");

var _constants2 = require("./constants");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var AddImageIcon = function AddImageIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-2-.03h24v24H-2z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M16 19.97H2v-14h9v-2H2c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-9h-2v9zM8.21 16.8l-1.96-2.36-2.75 3.53h11l-3.54-4.71-2.75 3.54zM18 3.97v-3h-2v3h-3c.01.01 0 2 0 2h3v2.99c.01.01 2 0 2 0V5.97h3v-2h-3z",
        fill: "#D3D7EB",
        fillRule: "nonzero"
      })]
    })
  }));
};

AddImageIcon.defaultProps = {
  viewBox: "0 0 21 22",
  xmlns: "http://www.w3.org/2000/svg"
};

var ClipartIcon = function ClipartIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M285.94 512H512V285.94H285.94zm29.973-196.09h166.12v166.12h-166.12zM389.84 244.32c67.359 0 122.16-54.801 122.16-122.16C512 54.805 457.199 0 389.84 0S267.68 54.801 267.68 122.16s54.801 122.16 122.16 122.16zm0-214.34c50.832 0 92.188 41.355 92.188 92.188 0 50.832-41.355 92.184-92.188 92.184-50.832 0-92.184-41.355-92.184-92.188S339.011 29.98 389.84 29.98zM0 231.31h262.7L131.35 0zm51.488-29.973 79.863-140.64 79.863 140.64zM63.648 291.52 0 401.76 63.648 512h127.3L254.6 401.76l-63.652-110.25h-127.3zm109.99 190.51H80.95l-46.344-80.27 46.344-80.273h92.688l46.344 80.273z",
      fill: "#D3D7EB"
    })
  }));
};

ClipartIcon.defaultProps = {
  viewBox: "0 0 512 512",
  xmlns: "http://www.w3.org/2000/svg"
};

var CombinedIcon = function CombinedIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M.7 5.4c0-.663.402-1.462.935-1.857L5.682.551C6.254.128 7.15.128 7.718.55l4.047 2.992c.533.391.935 1.19.935 1.857v10.914c0 .774-.625 1.405-1.393 1.405H2.093c-.768 0-1.393-.63-1.393-1.405zm4.8-.367a1.371 1.371 0 1 0 2.743 0 1.371 1.371 0 0 0-2.743 0z",
      fill: "#D3D7EB",
      fillRule: "nonzero"
    })
  }));
};

CombinedIcon.defaultProps = {
  viewBox: "0 0 13 18",
  xmlns: "http://www.w3.org/2000/svg"
};

var GraphIcon = function GraphIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M16 176V40H0v144a8 8 0 0 0 8 8h184v-16z",
      fill: "#D3D7EB"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M72 112a8 8 0 0 0-8-8H40a8 8 0 0 0-8 8v56h40zM128 80a8 8 0 0 0-8-8H96a8 8 0 0 0-8 8v88h40zM184 48a8 8 0 0 0-8-8h-24a8 8 0 0 0-8 8v120h40z",
      fill: "#D3D7EB"
    })]
  }));
};

GraphIcon.defaultProps = {
  width: "512",
  height: "512",
  viewBox: "0 0 192 192",
  xmlns: "http://www.w3.org/2000/svg"
};

var MarkerIcon = function MarkerIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M6 0a6 6 0 0 1 6 6c0 2.21-2 5.543-6 10-4-4.457-6-7.79-6-10a6 6 0 0 1 6-6zm0 4a2 2 0 1 0 0 4 2 2 0 0 0 0-4z",
      fill: "#D3D7EB",
      fillRule: "evenodd"
    })
  }));
};

MarkerIcon.defaultProps = {
  viewBox: "0 0 12 16",
  xmlns: "http://www.w3.org/2000/svg"
};

var PowerSettingsIcon = function PowerSettingsIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-3-2.03h24v24H-3z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M10 .97H8v10h2v-10zm4.83 2.17-1.42 1.42A6.92 6.92 0 0 1 16 9.97c0 3.87-3.13 7-7 7A6.995 6.995 0 0 1 4.58 4.55L3.17 3.14A8.932 8.932 0 0 0 0 9.97a9 9 0 0 0 18 0c0-2.74-1.23-5.18-3.17-6.83z",
        fill: "#D3D7EB",
        fillRule: "nonzero"
      })]
    })
  }));
};

PowerSettingsIcon.defaultProps = {
  viewBox: "0 0 18 19",
  xmlns: "http://www.w3.org/2000/svg"
};

var SegmentsMapIcon = function SegmentsMapIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M6.168 9.368C6.195 9.172 6.026 9 5.808 9H2.045c-.175 0-.325.113-.357.269l-.275 1.367h4.58l.175-1.268ZM5.881 11.454H1.248L.753 13.91h4.79l.338-2.455ZM6.458 13.91l.337-2.456h4.41l.337 2.455H6.458ZM5.43 14.727H.589l-.582 2.887c-.04.201.13.386.357.386H4.66c.183 0 .338-.123.36-.287l.41-2.986ZM6.345 14.727h5.31l.4 2.905c.026.196-.143.368-.36.368h-5.39c-.218 0-.386-.172-.36-.368l.4-2.905ZM12.569 14.727l.41 2.986c.023.164.177.287.36.287h4.298c.226 0 .398-.185.357-.386l-.582-2.887H12.57ZM17.247 13.91l-.495-2.456H12.12l.337 2.455h4.791ZM12.006 10.636h4.581l-.275-1.367C16.28 9.113 16.13 9 15.955 9h-3.763c-.219 0-.387.172-.36.368l.174 1.268ZM11.092 10.636H6.908l.185-1.349c.023-.164.177-.287.36-.287h3.093c.183 0 .338.123.36.287l.186 1.35ZM9.022 0C7.315 0 5.77 1.31 5.77 3.334c0 1.293.996 2.813 2.984 4.565a.414.414 0 0 0 .54 0c1.984-1.752 2.98-3.272 2.98-4.565C12.274 1.309 10.73 0 9.023 0Zm0 4.066a.816.816 0 0 1-.813-.814c0-.447.366-.813.813-.813.447 0 .813.366.813.813a.816.816 0 0 1-.813.814Z",
      fill: "#D3D7EB"
    })
  }));
};

SegmentsMapIcon.defaultProps = {
  width: "18",
  height: "18",
  fill: "none",
  xmlns: "http://www.w3.org/2000/svg"
};

var SeparatorIcon = function SeparatorIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M42.5 0v1H0V0z",
      fill: "#D3D7EB",
      fillRule: "evenodd"
    })
  }));
};

SeparatorIcon.defaultProps = {
  viewBox: "0 0 43 1",
  xmlns: "http://www.w3.org/2000/svg"
};

var SignatureIcon = function SignatureIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      fill: "#D3D7EB",
      d: "M623.2 192c-51.8 3.5-125.7 54.7-163.1 71.5-29.1 13.1-54.2 24.4-76.1 24.4-22.6 0-26-16.2-21.3-51.9 1.1-8 11.7-79.2-42.7-76.1-25.1 1.5-64.3 24.8-169.5 126L192 182.2c30.4-75.9-53.2-151.5-129.7-102.8L7.4 116.3C0 121-2.2 130.9 2.5 138.4l17.2 27c4.7 7.5 14.6 9.7 22.1 4.9l58-38.9c18.4-11.7 40.7 7.2 32.7 27.1L34.3 404.1C27.5 421 37 448 64 448c8.3 0 16.5-3.2 22.6-9.4 42.2-42.2 154.7-150.7 211.2-195.8-2.2 28.5-2.1 58.9 20.6 83.8 15.3 16.8 37.3 25.3 65.5 25.3 35.6 0 68-14.6 102.3-30 33-14.8 99-62.6 138.4-65.8 8.5-.7 15.2-7.3 15.2-15.8v-32.1c.2-9.1-7.5-16.8-16.6-16.2z"
    })
  }));
};

SignatureIcon.defaultProps = {
  'aria-hidden': "true",
  'data-prefix': "fas",
  'data-icon': "signature",
  className: "svg-inline--fa fa-signature fa-w-20",
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 640 512"
};

var TableIcon = function TableIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M453.33 469.33H58.67c-32.363 0-58.668-26.301-58.668-58.664v-352C.002 26.303 26.307-.002 58.67-.002h394.66c32.363 0 58.668 26.305 58.668 58.668v352c0 32.363-26.305 58.664-58.668 58.664zM58.67 32c-14.699 0-26.668 11.969-26.668 26.668v352c0 14.699 11.969 26.664 26.668 26.664h394.66c14.699 0 26.668-11.965 26.668-26.664v-352C479.998 43.969 468.029 32 453.33 32z",
      fill: "#D3D7EB"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M496 160H16c-8.832 0-16-7.168-16-16s7.168-16 16-16h480c8.832 0 16 7.168 16 16s-7.168 16-16 16zM496 266.67H16c-8.832 0-16-7.168-16-16s7.168-16 16-16h480c8.832 0 16 7.168 16 16s-7.168 16-16 16zM496 373.33H16c-8.832 0-16-7.168-16-16s7.168-16 16-16h480c8.832 0 16 7.168 16 16s-7.168 16-16 16z",
      fill: "#D3D7EB"
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M133.33 469.33c-8.832 0-16-7.168-16-16V144c0-8.832 7.168-16 16-16s16 7.168 16 16v309.33c0 8.832-7.168 16-16 16zM256 469.33c-8.832 0-16-7.168-16-16V144c0-8.832 7.168-16 16-16s16 7.168 16 16v309.33c0 8.832-7.168 16-16 16zM378.67 469.33c-8.832 0-16-7.168-16-16V144c0-8.832 7.168-16 16-16s16 7.168 16 16v309.33c0 8.832-7.168 16-16 16z",
      fill: "#D3D7EB"
    })]
  }));
};

TableIcon.defaultProps = {
  width: "512pt",
  height: "512pt",
  viewBox: "0 -21 512 512",
  xmlns: "http://www.w3.org/2000/svg"
};

var TextFormatIcon = function TextFormatIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "none",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M-8-5.456h36v36H-8z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M0 19.611v2.933h21v-2.933H0zm6.75-6.16h7.5l1.35 3.227h3.15L11.625.545h-2.25L2.25 16.678H5.4l1.35-3.227zM10.5 3.448l2.805 7.363h-5.61L10.5 3.448z",
        fill: "#D3D7EB",
        fillRule: "nonzero"
      })]
    })
  }));
};

TextFormatIcon.defaultProps = {
  viewBox: "0 0 21 23",
  xmlns: "http://www.w3.org/2000/svg"
};

var getMenuData = function getMenuData() {
  return [{
    name: _i18next.default.t('Text and drawing elements'),
    items: [{
      id: 'text',
      name: _i18next.default.t('Text'),
      icon: TextFormatIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }, {
      id: 'clip-art',
      name: _i18next.default.t('Shapes'),
      icon: ClipartIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }, {
      id: 'separator',
      name: _i18next.default.t('Separator'),
      icon: SeparatorIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }, {
      id: 'signature',
      name: _i18next.default.t('Signature'),
      icon: SignatureIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }]
  }, {
    name: _i18next.default.t('Image'),
    items: [{
      id: 'image',
      name: _i18next.default.t('Image'),
      icon: AddImageIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }, {
      id: 'helioscope',
      name: 'Helioscope',
      disabled: true,
      icon: PowerSettingsIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }, {
      id: 'aurora',
      name: 'Aurora',
      disabled: true,
      icon: CombinedIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: true
    }]
  }];
};

var getPlaceholderData = function getPlaceholderData(widgetName, onlineWidgets, orientation) {
  switch (widgetName) {
    case 'clip-art':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.clipArt.height,
        width: _constants.DEFAULT_WIDGET_VALUES.clipArt.width
      };

    case 'image':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.image.height,
        width: _constants.DEFAULT_WIDGET_VALUES.image.width
      };

    case 'map':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.map.height,
        width: _constants.DEFAULT_WIDGET_VALUES.map.width
      };

    case 'segments-map':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.segmentsMap.height,
        width: _constants.DEFAULT_WIDGET_VALUES.segmentsMap.width
      };

    case 'text':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.text.height,
        width: _constants.DEFAULT_WIDGET_VALUES.text.width
      };

    case 'separator':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.separator.height,
        width: _constants.DEFAULT_WIDGET_VALUES.separator.width
      };

    case 'signature':
      return {
        id: widgetName,
        height: _constants.DEFAULT_WIDGET_VALUES.signature.height,
        width: _constants.DEFAULT_WIDGET_VALUES.signature.width
      };

    default:
      var _iterator = _createForOfIteratorHelper(onlineWidgets),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var _step$value = _step.value,
              id = _step$value.id,
              meta = _step$value.meta;

          if (widgetName === id) {
            var freeGrid = meta.freeGrid,
                height = meta.height,
                _meta$supportVersion = meta.supportVersion,
                supportVersion = _meta$supportVersion === void 0 ? 1 : _meta$supportVersion,
                width = meta.width;
            var realWidth = supportVersion === 1 && !freeGrid ? width * _template.GRID_WIDTH : width;
            var realHeight = supportVersion === 1 && !freeGrid ? height * _template.GRID_HEIGHT : height;
            var limitX = orientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
            var limitY = orientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
            return {
              id: id,
              height: realHeight > limitY ? limitY : realHeight,
              width: realWidth > limitX ? limitX : realWidth
            };
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return null;
  }
};

exports.getPlaceholderData = getPlaceholderData;

var onWidgetDoubleClickBuild = function onWidgetDoubleClickBuild(id, onAddNewItem) {
  return function () {
    onAddNewItem(id);
  };
};

exports.onWidgetDoubleClickBuild = onWidgetDoubleClickBuild;

var onWidgetDragEndBuild = function onWidgetDragEndBuild(onSetDroppingItem, setIsDragging) {
  return function () {
    setIsDragging(false);
    onSetDroppingItem(null);
  };
};

exports.onWidgetDragEndBuild = onWidgetDragEndBuild;

var onWidgetDragStartBuild = function onWidgetDragStartBuild(id, onSetDroppingItem, setIsDragging) {
  return function (event) {
    event.dataTransfer.setData('text', '');
    setIsDragging(true);
    onSetDroppingItem(id);
  };
};

exports.onWidgetDragStartBuild = onWidgetDragStartBuild;

var prepareWidgetsData = function prepareWidgetsData() {
  var tableWidgets = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var chartWidgets = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var canUpdateProjectLocation = arguments.length > 2 ? arguments[2] : undefined;
  var canUpdatePanelsSowing = arguments.length > 3 ? arguments[3] : undefined;
  return [].concat(_toConsumableArray(getMenuData()), [{
    name: _i18next.default.t('Map'),
    items: [{
      id: 'map',
      name: _i18next.default.t('Location'),
      icon: MarkerIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: canUpdateProjectLocation
    }, {
      id: 'segments-map',
      name: _i18next.default.t('Solar panel layout'),
      icon: SegmentsMapIcon,
      group: _constants2.BASIC_COMPONENTS,
      visible: canUpdatePanelsSowing
    }]
  }, {
    name: _i18next.default.t('Table', {
      count: 2
    }),
    items: tableWidgets.map(function (tableWidget) {
      return _objectSpread(_objectSpread({}, tableWidget), {}, {
        icon: TableIcon,
        group: _constants2.TABLE_COMPONENTS,
        visible: true
      });
    })
  }, {
    name: _i18next.default.t('Graphics'),
    items: chartWidgets.map(function (chartWidget) {
      return _objectSpread(_objectSpread({}, chartWidget), {}, {
        // defaultValue: {
        //     ...chartWidget.defaultValue,
        //     colors: buildGraphsColors(chartWidget.defaultValue),
        // },
        icon: GraphIcon,
        group: _constants2.GRAPH_COMPONENTS,
        visible: true
      });
    })
  }]).filter(function (item) {
    return !item.hidden;
  }).reduce(function (acc, widgetGroup) {
    return [].concat(_toConsumableArray(acc), _toConsumableArray(widgetGroup.items.filter(function (item) {
      return item.visible;
    })));
  }, []).filter(function (item) {
    return !item.disabled;
  }).reduce(function (acc, current) {
    var key = '';

    switch (current.group) {
      case _constants2.BASIC_COMPONENTS:
        key = 'basicElements';
        break;

      case _constants2.TABLE_COMPONENTS:
        key = 'tableElements';
        break;

      case _constants2.GRAPH_COMPONENTS:
        key = 'graphElements';
        break;

      default:
        return acc;
    }

    return _objectSpread(_objectSpread({}, acc), {}, _defineProperty({}, key, [].concat(_toConsumableArray(acc[key]), [current])));
  }, {
    basicElements: [],
    tableElements: [],
    graphElements: []
  });
};

exports.prepareWidgetsData = prepareWidgetsData;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1dpZGdldE1lbnUvaGVscGVycy5qcyJdLCJuYW1lcyI6WyJBZGRJbWFnZUljb24iLCJDbGlwYXJ0SWNvbiIsIkNvbWJpbmVkSWNvbiIsIkdyYXBoSWNvbiIsIk1hcmtlckljb24iLCJQb3dlclNldHRpbmdzSWNvbiIsIlNlZ21lbnRzTWFwSWNvbiIsIlNlcGFyYXRvckljb24iLCJTaWduYXR1cmVJY29uIiwiVGFibGVJY29uIiwiVGV4dEZvcm1hdEljb24iLCJnZXRNZW51RGF0YSIsIm5hbWUiLCJpMThuZXh0IiwidCIsIml0ZW1zIiwiaWQiLCJpY29uIiwiZ3JvdXAiLCJCQVNJQ19DT01QT05FTlRTIiwidmlzaWJsZSIsImRpc2FibGVkIiwiZ2V0UGxhY2Vob2xkZXJEYXRhIiwid2lkZ2V0TmFtZSIsIm9ubGluZVdpZGdldHMiLCJvcmllbnRhdGlvbiIsImhlaWdodCIsIkRFRkFVTFRfV0lER0VUX1ZBTFVFUyIsImNsaXBBcnQiLCJ3aWR0aCIsImltYWdlIiwibWFwIiwic2VnbWVudHNNYXAiLCJ0ZXh0Iiwic2VwYXJhdG9yIiwic2lnbmF0dXJlIiwibWV0YSIsImZyZWVHcmlkIiwic3VwcG9ydFZlcnNpb24iLCJyZWFsV2lkdGgiLCJHUklEX1dJRFRIIiwicmVhbEhlaWdodCIsIkdSSURfSEVJR0hUIiwibGltaXRYIiwiUEFHRV9XSURUSCIsIlBBR0VfSEVJR0hUIiwibGltaXRZIiwib25XaWRnZXREb3VibGVDbGlja0J1aWxkIiwib25BZGROZXdJdGVtIiwib25XaWRnZXREcmFnRW5kQnVpbGQiLCJvblNldERyb3BwaW5nSXRlbSIsInNldElzRHJhZ2dpbmciLCJvbldpZGdldERyYWdTdGFydEJ1aWxkIiwiZXZlbnQiLCJkYXRhVHJhbnNmZXIiLCJzZXREYXRhIiwicHJlcGFyZVdpZGdldHNEYXRhIiwidGFibGVXaWRnZXRzIiwiY2hhcnRXaWRnZXRzIiwiY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uIiwiY2FuVXBkYXRlUGFuZWxzU293aW5nIiwiY291bnQiLCJ0YWJsZVdpZGdldCIsIlRBQkxFX0NPTVBPTkVOVFMiLCJjaGFydFdpZGdldCIsIkdSQVBIX0NPTVBPTkVOVFMiLCJmaWx0ZXIiLCJpdGVtIiwiaGlkZGVuIiwicmVkdWNlIiwiYWNjIiwid2lkZ2V0R3JvdXAiLCJjdXJyZW50Iiwia2V5IiwiYmFzaWNFbGVtZW50cyIsInRhYmxlRWxlbWVudHMiLCJncmFwaEVsZW1lbnRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFtQkE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBZDJCQSxZLFlBQUFBLFk7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsWTs7Ozs7SUFDQUMsVyxZQUFBQSxXOzs7Ozs7Ozs7QUFBQUEsVzs7Ozs7SUFDQUMsWSxZQUFBQSxZOzs7Ozs7Ozs7O0FBQUFBLFk7Ozs7O0lBQ0FDLFMsWUFBQUEsUzs7Ozs7Ozs7Ozs7O0FBQUFBLFM7Ozs7Ozs7SUFDQUMsVSxZQUFBQSxVOzs7Ozs7Ozs7O0FBQUFBLFU7Ozs7O0lBQ0FDLGlCLFlBQUFBLGlCOzs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLGlCOzs7OztJQUNBQyxlLFlBQUFBLGU7Ozs7Ozs7OztBQUFBQSxlOzs7Ozs7O0lBQ0FDLGEsWUFBQUEsYTs7Ozs7Ozs7OztBQUFBQSxhOzs7OztJQUNBQyxhLFlBQUFBLGE7Ozs7Ozs7OztBQUFBQSxhOzs7Ozs7Ozs7SUFDQUMsUyxZQUFBQSxTOzs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsUzs7Ozs7OztJQUNBQyxjLFlBQUFBLGM7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsYzs7Ozs7QUFVM0IsSUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsR0FBTTtBQUN0QixTQUFPLENBQ0g7QUFDSUMsSUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLDJCQUFWLENBRFY7QUFFSUMsSUFBQUEsS0FBSyxFQUFFLENBQ0g7QUFDSUMsTUFBQUEsRUFBRSxFQUFFLE1BRFI7QUFFSUosTUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLE1BQVYsQ0FGVjtBQUdJRyxNQUFBQSxJQUFJLEVBQUVQLGNBSFY7QUFJSVEsTUFBQUEsS0FBSyxFQUFFQyw0QkFKWDtBQUtJQyxNQUFBQSxPQUFPLEVBQUU7QUFMYixLQURHLEVBUUg7QUFDSUosTUFBQUEsRUFBRSxFQUFFLFVBRFI7QUFFSUosTUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLFFBQVYsQ0FGVjtBQUdJRyxNQUFBQSxJQUFJLEVBQUVoQixXQUhWO0FBSUlpQixNQUFBQSxLQUFLLEVBQUVDLDRCQUpYO0FBS0lDLE1BQUFBLE9BQU8sRUFBRTtBQUxiLEtBUkcsRUFlSDtBQUNJSixNQUFBQSxFQUFFLEVBQUUsV0FEUjtBQUVJSixNQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsV0FBVixDQUZWO0FBR0lHLE1BQUFBLElBQUksRUFBRVYsYUFIVjtBQUlJVyxNQUFBQSxLQUFLLEVBQUVDLDRCQUpYO0FBS0lDLE1BQUFBLE9BQU8sRUFBRTtBQUxiLEtBZkcsRUFzQkg7QUFDSUosTUFBQUEsRUFBRSxFQUFFLFdBRFI7QUFFSUosTUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLFdBQVYsQ0FGVjtBQUdJRyxNQUFBQSxJQUFJLEVBQUVULGFBSFY7QUFJSVUsTUFBQUEsS0FBSyxFQUFFQyw0QkFKWDtBQUtJQyxNQUFBQSxPQUFPLEVBQUU7QUFMYixLQXRCRztBQUZYLEdBREcsRUFrQ0g7QUFDSVIsSUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLE9BQVYsQ0FEVjtBQUVJQyxJQUFBQSxLQUFLLEVBQUUsQ0FDSDtBQUNJQyxNQUFBQSxFQUFFLEVBQUUsT0FEUjtBQUVJSixNQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsT0FBVixDQUZWO0FBR0lHLE1BQUFBLElBQUksRUFBRWpCLFlBSFY7QUFJSWtCLE1BQUFBLEtBQUssRUFBRUMsNEJBSlg7QUFLSUMsTUFBQUEsT0FBTyxFQUFFO0FBTGIsS0FERyxFQVFIO0FBQ0lKLE1BQUFBLEVBQUUsRUFBRSxZQURSO0FBRUlKLE1BQUFBLElBQUksRUFBRSxZQUZWO0FBR0lTLE1BQUFBLFFBQVEsRUFBRSxJQUhkO0FBSUlKLE1BQUFBLElBQUksRUFBRVosaUJBSlY7QUFLSWEsTUFBQUEsS0FBSyxFQUFFQyw0QkFMWDtBQU1JQyxNQUFBQSxPQUFPLEVBQUU7QUFOYixLQVJHLEVBZ0JIO0FBQ0lKLE1BQUFBLEVBQUUsRUFBRSxRQURSO0FBRUlKLE1BQUFBLElBQUksRUFBRSxRQUZWO0FBR0lTLE1BQUFBLFFBQVEsRUFBRSxJQUhkO0FBSUlKLE1BQUFBLElBQUksRUFBRWYsWUFKVjtBQUtJZ0IsTUFBQUEsS0FBSyxFQUFFQyw0QkFMWDtBQU1JQyxNQUFBQSxPQUFPLEVBQUU7QUFOYixLQWhCRztBQUZYLEdBbENHLENBQVA7QUErREgsQ0FoRUQ7O0FBa0VPLElBQU1FLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsVUFBRCxFQUFhQyxhQUFiLEVBQTRCQyxXQUE1QixFQUE0QztBQUMxRSxVQUFRRixVQUFSO0FBQ0ksU0FBSyxVQUFMO0FBQ0ksYUFBTztBQUNIUCxRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JDLE9BQXRCLENBQThCRixNQUZuQztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQkMsT0FBdEIsQ0FBOEJDO0FBSGxDLE9BQVA7O0FBS0osU0FBSyxPQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JHLEtBQXRCLENBQTRCSixNQUZqQztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQkcsS0FBdEIsQ0FBNEJEO0FBSGhDLE9BQVA7O0FBS0osU0FBSyxLQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JJLEdBQXRCLENBQTBCTCxNQUYvQjtBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQkksR0FBdEIsQ0FBMEJGO0FBSDlCLE9BQVA7O0FBS0osU0FBSyxjQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JLLFdBQXRCLENBQWtDTixNQUZ2QztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQkssV0FBdEIsQ0FBa0NIO0FBSHRDLE9BQVA7O0FBS0osU0FBSyxNQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JNLElBQXRCLENBQTJCUCxNQUZoQztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQk0sSUFBdEIsQ0FBMkJKO0FBSC9CLE9BQVA7O0FBS0osU0FBSyxXQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JPLFNBQXRCLENBQWdDUixNQUZyQztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQk8sU0FBdEIsQ0FBZ0NMO0FBSHBDLE9BQVA7O0FBS0osU0FBSyxXQUFMO0FBQ0ksYUFBTztBQUNIYixRQUFBQSxFQUFFLEVBQUVPLFVBREQ7QUFFSEcsUUFBQUEsTUFBTSxFQUFFQyxpQ0FBc0JRLFNBQXRCLENBQWdDVCxNQUZyQztBQUdIRyxRQUFBQSxLQUFLLEVBQUVGLGlDQUFzQlEsU0FBdEIsQ0FBZ0NOO0FBSHBDLE9BQVA7O0FBS0o7QUFBQSxpREFDK0JMLGFBRC9CO0FBQUE7O0FBQUE7QUFDSSw0REFBMEM7QUFBQTtBQUFBLGNBQTdCUixFQUE2QixlQUE3QkEsRUFBNkI7QUFBQSxjQUF6Qm9CLElBQXlCLGVBQXpCQSxJQUF5Qjs7QUFDdEMsY0FBSWIsVUFBVSxLQUFLUCxFQUFuQixFQUF1QjtBQUNuQixnQkFDSXFCLFFBREosR0FLSUQsSUFMSixDQUNJQyxRQURKO0FBQUEsZ0JBRUlYLE1BRkosR0FLSVUsSUFMSixDQUVJVixNQUZKO0FBQUEsdUNBS0lVLElBTEosQ0FHSUUsY0FISjtBQUFBLGdCQUdJQSxjQUhKLHFDQUdxQixDQUhyQjtBQUFBLGdCQUlJVCxLQUpKLEdBS0lPLElBTEosQ0FJSVAsS0FKSjtBQU1BLGdCQUFNVSxTQUFTLEdBQ1hELGNBQWMsS0FBSyxDQUFuQixJQUF3QixDQUFDRCxRQUF6QixHQUNNUixLQUFLLEdBQUdXLG9CQURkLEdBRU1YLEtBSFY7QUFJQSxnQkFBTVksVUFBVSxHQUNaSCxjQUFjLEtBQUssQ0FBbkIsSUFBd0IsQ0FBQ0QsUUFBekIsR0FDTVgsTUFBTSxHQUFHZ0IscUJBRGYsR0FFTWhCLE1BSFY7QUFLQSxnQkFBTWlCLE1BQU0sR0FDUmxCLFdBQVcsS0FBSyxVQUFoQixHQUE2Qm1CLG9CQUE3QixHQUEwQ0MscUJBRDlDO0FBRUEsZ0JBQU1DLE1BQU0sR0FDUnJCLFdBQVcsS0FBSyxVQUFoQixHQUE2Qm9CLHFCQUE3QixHQUEyQ0Qsb0JBRC9DO0FBRUEsbUJBQU87QUFDSDVCLGNBQUFBLEVBQUUsRUFBRkEsRUFERztBQUVIVSxjQUFBQSxNQUFNLEVBQUVlLFVBQVUsR0FBR0ssTUFBYixHQUFzQkEsTUFBdEIsR0FBK0JMLFVBRnBDO0FBR0haLGNBQUFBLEtBQUssRUFBRVUsU0FBUyxHQUFHSSxNQUFaLEdBQXFCQSxNQUFyQixHQUE4Qko7QUFIbEMsYUFBUDtBQUtIO0FBQ0o7QUE1Qkw7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUE2QkksYUFBTyxJQUFQO0FBeEVSO0FBMEVILENBM0VNOzs7O0FBNkVBLElBQU1RLHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsQ0FBQy9CLEVBQUQsRUFBS2dDLFlBQUw7QUFBQSxTQUFzQixZQUFNO0FBQ2hFQSxJQUFBQSxZQUFZLENBQUNoQyxFQUFELENBQVo7QUFDSCxHQUZ1QztBQUFBLENBQWpDOzs7O0FBSUEsSUFBTWlDLG9CQUFvQixHQUM3QixTQURTQSxvQkFDVCxDQUFDQyxpQkFBRCxFQUFvQkMsYUFBcEI7QUFBQSxTQUFzQyxZQUFNO0FBQ3hDQSxJQUFBQSxhQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0FELElBQUFBLGlCQUFpQixDQUFDLElBQUQsQ0FBakI7QUFDSCxHQUhEO0FBQUEsQ0FERzs7OztBQU1BLElBQU1FLHNCQUFzQixHQUMvQixTQURTQSxzQkFDVCxDQUFDcEMsRUFBRCxFQUFLa0MsaUJBQUwsRUFBd0JDLGFBQXhCO0FBQUEsU0FBMEMsVUFBQ0UsS0FBRCxFQUFXO0FBQ2pEQSxJQUFBQSxLQUFLLENBQUNDLFlBQU4sQ0FBbUJDLE9BQW5CLENBQTJCLE1BQTNCLEVBQW1DLEVBQW5DO0FBQ0FKLElBQUFBLGFBQWEsQ0FBQyxJQUFELENBQWI7QUFDQUQsSUFBQUEsaUJBQWlCLENBQUNsQyxFQUFELENBQWpCO0FBQ0gsR0FKRDtBQUFBLENBREc7Ozs7QUFPQSxJQUFNd0Msa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixHQUs3QjtBQUFBLE1BSkRDLFlBSUMsdUVBSmMsRUFJZDtBQUFBLE1BSERDLFlBR0MsdUVBSGMsRUFHZDtBQUFBLE1BRkRDLHdCQUVDO0FBQUEsTUFEREMscUJBQ0M7QUFDRCxTQUFPLDZCQUNBakQsV0FBVyxFQURYLElBRUg7QUFDSUMsSUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLEtBQVYsQ0FEVjtBQUVJQyxJQUFBQSxLQUFLLEVBQUUsQ0FDSDtBQUNJQyxNQUFBQSxFQUFFLEVBQUUsS0FEUjtBQUVJSixNQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsVUFBVixDQUZWO0FBR0lHLE1BQUFBLElBQUksRUFBRWIsVUFIVjtBQUlJYyxNQUFBQSxLQUFLLEVBQUVDLDRCQUpYO0FBS0lDLE1BQUFBLE9BQU8sRUFBRXVDO0FBTGIsS0FERyxFQVFIO0FBQ0kzQyxNQUFBQSxFQUFFLEVBQUUsY0FEUjtBQUVJSixNQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsb0JBQVYsQ0FGVjtBQUdJRyxNQUFBQSxJQUFJLEVBQUVYLGVBSFY7QUFJSVksTUFBQUEsS0FBSyxFQUFFQyw0QkFKWDtBQUtJQyxNQUFBQSxPQUFPLEVBQUV3QztBQUxiLEtBUkc7QUFGWCxHQUZHLEVBcUJIO0FBQ0loRCxJQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsT0FBVixFQUFtQjtBQUFFK0MsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBbkIsQ0FEVjtBQUVJOUMsSUFBQUEsS0FBSyxFQUFFMEMsWUFBWSxDQUFDMUIsR0FBYixDQUFpQixVQUFDK0IsV0FBRDtBQUFBLDZDQUNqQkEsV0FEaUI7QUFFcEI3QyxRQUFBQSxJQUFJLEVBQUVSLFNBRmM7QUFHcEJTLFFBQUFBLEtBQUssRUFBRTZDLDRCQUhhO0FBSXBCM0MsUUFBQUEsT0FBTyxFQUFFO0FBSlc7QUFBQSxLQUFqQjtBQUZYLEdBckJHLEVBOEJIO0FBQ0lSLElBQUFBLElBQUksRUFBRUMsaUJBQVFDLENBQVIsQ0FBVSxVQUFWLENBRFY7QUFFSUMsSUFBQUEsS0FBSyxFQUFFMkMsWUFBWSxDQUFDM0IsR0FBYixDQUFpQixVQUFDaUMsV0FBRCxFQUFpQjtBQUNyQyw2Q0FDT0EsV0FEUDtBQUVJO0FBQ0E7QUFDQTtBQUNBO0FBQ0EvQyxRQUFBQSxJQUFJLEVBQUVkLFNBTlY7QUFPSWUsUUFBQUEsS0FBSyxFQUFFK0MsNEJBUFg7QUFRSTdDLFFBQUFBLE9BQU8sRUFBRTtBQVJiO0FBVUgsS0FYTTtBQUZYLEdBOUJHLEdBOENGOEMsTUE5Q0UsQ0E4Q0ssVUFBQ0MsSUFBRDtBQUFBLFdBQVUsQ0FBQ0EsSUFBSSxDQUFDQyxNQUFoQjtBQUFBLEdBOUNMLEVBK0NGQyxNQS9DRSxDQWdEQyxVQUFDQyxHQUFELEVBQU1DLFdBQU47QUFBQSx3Q0FDT0QsR0FEUCxzQkFFT0MsV0FBVyxDQUFDeEQsS0FBWixDQUFrQm1ELE1BQWxCLENBQXlCLFVBQUNDLElBQUQ7QUFBQSxhQUFVQSxJQUFJLENBQUMvQyxPQUFmO0FBQUEsS0FBekIsQ0FGUDtBQUFBLEdBaERELEVBb0RDLEVBcERELEVBc0RGOEMsTUF0REUsQ0FzREssVUFBQ0MsSUFBRDtBQUFBLFdBQVUsQ0FBQ0EsSUFBSSxDQUFDOUMsUUFBaEI7QUFBQSxHQXRETCxFQXVERmdELE1BdkRFLENBd0RDLFVBQUNDLEdBQUQsRUFBTUUsT0FBTixFQUFrQjtBQUNkLFFBQUlDLEdBQUcsR0FBRyxFQUFWOztBQUNBLFlBQVFELE9BQU8sQ0FBQ3RELEtBQWhCO0FBQ0ksV0FBS0MsNEJBQUw7QUFDSXNELFFBQUFBLEdBQUcsR0FBRyxlQUFOO0FBQ0E7O0FBQ0osV0FBS1YsNEJBQUw7QUFDSVUsUUFBQUEsR0FBRyxHQUFHLGVBQU47QUFDQTs7QUFDSixXQUFLUiw0QkFBTDtBQUNJUSxRQUFBQSxHQUFHLEdBQUcsZUFBTjtBQUNBOztBQUNKO0FBQ0ksZUFBT0gsR0FBUDtBQVhSOztBQWFBLDJDQUNPQSxHQURQLDJCQUVLRyxHQUZMLCtCQUVlSCxHQUFHLENBQUNHLEdBQUQsQ0FGbEIsSUFFeUJELE9BRnpCO0FBSUgsR0EzRUYsRUE0RUM7QUFDSUUsSUFBQUEsYUFBYSxFQUFFLEVBRG5CO0FBRUlDLElBQUFBLGFBQWEsRUFBRSxFQUZuQjtBQUdJQyxJQUFBQSxhQUFhLEVBQUU7QUFIbkIsR0E1RUQsQ0FBUDtBQWtGSCxDQXhGTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBpMThuZXh0IGZyb20gJ2kxOG5leHQnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEdSSURfSEVJR0hULFxyXG4gICAgR1JJRF9XSURUSCxcclxuICAgIFBBR0VfSEVJR0hULFxyXG4gICAgUEFHRV9XSURUSCxcclxufSBmcm9tICdAY29uc3RhbnRzL3RlbXBsYXRlJztcclxuXHJcbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIEFkZEltYWdlSWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvYWRkSW1hZ2Uuc3ZnJztcclxuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgQ2xpcGFydEljb24gfSBmcm9tICdAcmVzL2ljb25zL2NsaXBhcnQuc3ZnJztcclxuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgQ29tYmluZWRJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9jb21iaW5lZC5zdmcnO1xyXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBHcmFwaEljb24gfSBmcm9tICdAcmVzL2ljb25zL2dyYXBoLnN2Zyc7XHJcbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIE1hcmtlckljb24gfSBmcm9tICdAcmVzL2ljb25zL21hcmtlci5zdmcnO1xyXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBQb3dlclNldHRpbmdzSWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvcG93ZXJTZXR0aW5ncy5zdmcnO1xyXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBTZWdtZW50c01hcEljb24gfSBmcm9tICdAcmVzL2ljb25zL3NlZ21lbnRzTWFwLnN2Zyc7XHJcbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIFNlcGFyYXRvckljb24gfSBmcm9tICdAcmVzL2ljb25zL3NlcGFyYXRvci5zdmcnO1xyXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBTaWduYXR1cmVJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9zaWduYXR1cmUuc3ZnJztcclxuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgVGFibGVJY29uIH0gZnJvbSAnQHJlcy9pY29ucy90YWJsZS5zdmcnO1xyXG5pbXBvcnQgeyBSZWFjdENvbXBvbmVudCBhcyBUZXh0Rm9ybWF0SWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvdGV4dEZvcm1hdC5zdmcnO1xyXG5cclxuaW1wb3J0IHsgREVGQVVMVF9XSURHRVRfVkFMVUVTIH0gZnJvbSAnQHRlbXBsYXRlQ29yZS9jb25zdGFudHMnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEJBU0lDX0NPTVBPTkVOVFMsXHJcbiAgICBHUkFQSF9DT01QT05FTlRTLFxyXG4gICAgVEFCTEVfQ09NUE9ORU5UUyxcclxufSBmcm9tICcuL2NvbnN0YW50cyc7XHJcblxyXG5jb25zdCBnZXRNZW51RGF0YSA9ICgpID0+IHtcclxuICAgIHJldHVybiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ1RleHQgYW5kIGRyYXdpbmcgZWxlbWVudHMnKSxcclxuICAgICAgICAgICAgaXRlbXM6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IGkxOG5leHQudCgnVGV4dCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIGljb246IFRleHRGb3JtYXRJY29uLFxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwOiBCQVNJQ19DT01QT05FTlRTLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnY2xpcC1hcnQnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IGkxOG5leHQudCgnU2hhcGVzJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogQ2xpcGFydEljb24sXHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXA6IEJBU0lDX0NPTVBPTkVOVFMsXHJcbiAgICAgICAgICAgICAgICAgICAgdmlzaWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICdzZXBhcmF0b3InLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IGkxOG5leHQudCgnU2VwYXJhdG9yJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogU2VwYXJhdG9ySWNvbixcclxuICAgICAgICAgICAgICAgICAgICBncm91cDogQkFTSUNfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ3NpZ25hdHVyZScsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogaTE4bmV4dC50KCdTaWduYXR1cmUnKSxcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiBTaWduYXR1cmVJY29uLFxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwOiBCQVNJQ19DT01QT05FTlRTLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ0ltYWdlJyksXHJcbiAgICAgICAgICAgIGl0ZW1zOiBbXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICdpbWFnZScsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogaTE4bmV4dC50KCdJbWFnZScpLFxyXG4gICAgICAgICAgICAgICAgICAgIGljb246IEFkZEltYWdlSWNvbixcclxuICAgICAgICAgICAgICAgICAgICBncm91cDogQkFTSUNfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2hlbGlvc2NvcGUnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdIZWxpb3Njb3BlJyxcclxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBpY29uOiBQb3dlclNldHRpbmdzSWNvbixcclxuICAgICAgICAgICAgICAgICAgICBncm91cDogQkFTSUNfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2F1cm9yYScsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0F1cm9yYScsXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogQ29tYmluZWRJY29uLFxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwOiBCQVNJQ19DT01QT05FTlRTLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgIH0sXHJcbiAgICBdO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFBsYWNlaG9sZGVyRGF0YSA9ICh3aWRnZXROYW1lLCBvbmxpbmVXaWRnZXRzLCBvcmllbnRhdGlvbikgPT4ge1xyXG4gICAgc3dpdGNoICh3aWRnZXROYW1lKSB7XHJcbiAgICAgICAgY2FzZSAnY2xpcC1hcnQnOlxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgaWQ6IHdpZGdldE5hbWUsXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IERFRkFVTFRfV0lER0VUX1ZBTFVFUy5jbGlwQXJ0LmhlaWdodCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBERUZBVUxUX1dJREdFVF9WQUxVRVMuY2xpcEFydC53aWR0aCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICBjYXNlICdpbWFnZSc6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBpZDogd2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogREVGQVVMVF9XSURHRVRfVkFMVUVTLmltYWdlLmhlaWdodCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBERUZBVUxUX1dJREdFVF9WQUxVRVMuaW1hZ2Uud2lkdGgsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgY2FzZSAnbWFwJzpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIGlkOiB3aWRnZXROYW1lLFxyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBERUZBVUxUX1dJREdFVF9WQUxVRVMubWFwLmhlaWdodCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBERUZBVUxUX1dJREdFVF9WQUxVRVMubWFwLndpZHRoLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIGNhc2UgJ3NlZ21lbnRzLW1hcCc6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBpZDogd2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogREVGQVVMVF9XSURHRVRfVkFMVUVTLnNlZ21lbnRzTWFwLmhlaWdodCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBERUZBVUxUX1dJREdFVF9WQUxVRVMuc2VnbWVudHNNYXAud2lkdGgsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgY2FzZSAndGV4dCc6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBpZDogd2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogREVGQVVMVF9XSURHRVRfVkFMVUVTLnRleHQuaGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgd2lkdGg6IERFRkFVTFRfV0lER0VUX1ZBTFVFUy50ZXh0LndpZHRoLFxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIGNhc2UgJ3NlcGFyYXRvcic6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICBpZDogd2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogREVGQVVMVF9XSURHRVRfVkFMVUVTLnNlcGFyYXRvci5oZWlnaHQsXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogREVGQVVMVF9XSURHRVRfVkFMVUVTLnNlcGFyYXRvci53aWR0aCxcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICBjYXNlICdzaWduYXR1cmUnOlxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgaWQ6IHdpZGdldE5hbWUsXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IERFRkFVTFRfV0lER0VUX1ZBTFVFUy5zaWduYXR1cmUuaGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgd2lkdGg6IERFRkFVTFRfV0lER0VUX1ZBTFVFUy5zaWduYXR1cmUud2lkdGgsXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgZm9yIChjb25zdCB7IGlkLCBtZXRhIH0gb2Ygb25saW5lV2lkZ2V0cykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHdpZGdldE5hbWUgPT09IGlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmcmVlR3JpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgIH0gPSBtZXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlYWxXaWR0aCA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1cHBvcnRWZXJzaW9uID09PSAxICYmICFmcmVlR3JpZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPyB3aWR0aCAqIEdSSURfV0lEVEhcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogd2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgcmVhbEhlaWdodCA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1cHBvcnRWZXJzaW9uID09PSAxICYmICFmcmVlR3JpZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBoZWlnaHQgKiBHUklEX0hFSUdIVFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBoZWlnaHQ7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxpbWl0WCA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGxpbWl0WSA9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHJlYWxIZWlnaHQgPiBsaW1pdFkgPyBsaW1pdFkgOiByZWFsSGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogcmVhbFdpZHRoID4gbGltaXRYID8gbGltaXRYIDogcmVhbFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgb25XaWRnZXREb3VibGVDbGlja0J1aWxkID0gKGlkLCBvbkFkZE5ld0l0ZW0pID0+ICgpID0+IHtcclxuICAgIG9uQWRkTmV3SXRlbShpZCk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgb25XaWRnZXREcmFnRW5kQnVpbGQgPVxyXG4gICAgKG9uU2V0RHJvcHBpbmdJdGVtLCBzZXRJc0RyYWdnaW5nKSA9PiAoKSA9PiB7XHJcbiAgICAgICAgc2V0SXNEcmFnZ2luZyhmYWxzZSk7XHJcbiAgICAgICAgb25TZXREcm9wcGluZ0l0ZW0obnVsbCk7XHJcbiAgICB9O1xyXG5cclxuZXhwb3J0IGNvbnN0IG9uV2lkZ2V0RHJhZ1N0YXJ0QnVpbGQgPVxyXG4gICAgKGlkLCBvblNldERyb3BwaW5nSXRlbSwgc2V0SXNEcmFnZ2luZykgPT4gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgZXZlbnQuZGF0YVRyYW5zZmVyLnNldERhdGEoJ3RleHQnLCAnJyk7XHJcbiAgICAgICAgc2V0SXNEcmFnZ2luZyh0cnVlKTtcclxuICAgICAgICBvblNldERyb3BwaW5nSXRlbShpZCk7XHJcbiAgICB9O1xyXG5cclxuZXhwb3J0IGNvbnN0IHByZXBhcmVXaWRnZXRzRGF0YSA9IChcclxuICAgIHRhYmxlV2lkZ2V0cyA9IFtdLFxyXG4gICAgY2hhcnRXaWRnZXRzID0gW10sXHJcbiAgICBjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb24sXHJcbiAgICBjYW5VcGRhdGVQYW5lbHNTb3dpbmdcclxuKSA9PiB7XHJcbiAgICByZXR1cm4gW1xyXG4gICAgICAgIC4uLmdldE1lbnVEYXRhKCksXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ01hcCcpLFxyXG4gICAgICAgICAgICBpdGVtczogW1xyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnbWFwJyxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ0xvY2F0aW9uJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogTWFya2VySWNvbixcclxuICAgICAgICAgICAgICAgICAgICBncm91cDogQkFTSUNfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiBjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb24sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnc2VnbWVudHMtbWFwJyxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ1NvbGFyIHBhbmVsIGxheW91dCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIGljb246IFNlZ21lbnRzTWFwSWNvbixcclxuICAgICAgICAgICAgICAgICAgICBncm91cDogQkFTSUNfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgICAgICB2aXNpYmxlOiBjYW5VcGRhdGVQYW5lbHNTb3dpbmcsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBuYW1lOiBpMThuZXh0LnQoJ1RhYmxlJywgeyBjb3VudDogMiB9KSxcclxuICAgICAgICAgICAgaXRlbXM6IHRhYmxlV2lkZ2V0cy5tYXAoKHRhYmxlV2lkZ2V0KSA9PiAoe1xyXG4gICAgICAgICAgICAgICAgLi4udGFibGVXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICBpY29uOiBUYWJsZUljb24sXHJcbiAgICAgICAgICAgICAgICBncm91cDogVEFCTEVfQ09NUE9ORU5UUyxcclxuICAgICAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgIH0pKSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgbmFtZTogaTE4bmV4dC50KCdHcmFwaGljcycpLFxyXG4gICAgICAgICAgICBpdGVtczogY2hhcnRXaWRnZXRzLm1hcCgoY2hhcnRXaWRnZXQpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4uY2hhcnRXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZGVmYXVsdFZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC4uLmNoYXJ0V2lkZ2V0LmRlZmF1bHRWYWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgY29sb3JzOiBidWlsZEdyYXBoc0NvbG9ycyhjaGFydFdpZGdldC5kZWZhdWx0VmFsdWUpLFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgaWNvbjogR3JhcGhJY29uLFxyXG4gICAgICAgICAgICAgICAgICAgIGdyb3VwOiBHUkFQSF9DT01QT05FTlRTLFxyXG4gICAgICAgICAgICAgICAgICAgIHZpc2libGU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICB9LFxyXG4gICAgXVxyXG4gICAgICAgIC5maWx0ZXIoKGl0ZW0pID0+ICFpdGVtLmhpZGRlbilcclxuICAgICAgICAucmVkdWNlKFxyXG4gICAgICAgICAgICAoYWNjLCB3aWRnZXRHcm91cCkgPT4gW1xyXG4gICAgICAgICAgICAgICAgLi4uYWNjLFxyXG4gICAgICAgICAgICAgICAgLi4ud2lkZ2V0R3JvdXAuaXRlbXMuZmlsdGVyKChpdGVtKSA9PiBpdGVtLnZpc2libGUpLFxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICBbXVxyXG4gICAgICAgIClcclxuICAgICAgICAuZmlsdGVyKChpdGVtKSA9PiAhaXRlbS5kaXNhYmxlZClcclxuICAgICAgICAucmVkdWNlKFxyXG4gICAgICAgICAgICAoYWNjLCBjdXJyZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQga2V5ID0gJyc7XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKGN1cnJlbnQuZ3JvdXApIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIEJBU0lDX0NPTVBPTkVOVFM6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleSA9ICdiYXNpY0VsZW1lbnRzJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBUQUJMRV9DT01QT05FTlRTOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXkgPSAndGFibGVFbGVtZW50cyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgR1JBUEhfQ09NUE9ORU5UUzpcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5ID0gJ2dyYXBoRWxlbWVudHMnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAuLi5hY2MsXHJcbiAgICAgICAgICAgICAgICAgICAgW2tleV06IFsuLi5hY2Nba2V5XSwgY3VycmVudF0sXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBiYXNpY0VsZW1lbnRzOiBbXSxcclxuICAgICAgICAgICAgICAgIHRhYmxlRWxlbWVudHM6IFtdLFxyXG4gICAgICAgICAgICAgICAgZ3JhcGhFbGVtZW50czogW10sXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG59O1xyXG4iXX0=