"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _immutabilityHelper = _interopRequireDefault(require("immutability-helper"));

var _utils = require("../../helpers/utils");

var _actionTypes = require("./actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var INITIAL_STATE = {
  currencyConfig: {},
  token: null,
  baseUrl: null,
  language: null,
  googleApiKey: null,
  froalaLicenseKey: '',
  fetchCatalogs: {
    isFetching: false,
    data: [],
    error: null
  },
  fetchTableWidgets: {
    isFetching: true,
    data: [],
    errors: []
  },
  fetchChartWidgets: {
    isFetching: true,
    data: [],
    errors: []
  },
  fetchImageGallery: {
    isFetching: false,
    data: [],
    errors: null
  },
  initializing: true
};

function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actionTypes.FETCH_CATALOGS:
      return (0, _immutabilityHelper.default)(state, {
        fetchCatalogs: {
          $merge: {
            isFetching: true,
            errors: []
          }
        }
      });

    case _actionTypes.FETCH_CATALOGS_SUCCESS:
      return (0, _immutabilityHelper.default)(state, {
        fetchCatalogs: {
          $merge: {
            isFetching: false,
            data: action.payload
          }
        }
      });

    case _actionTypes.FETCH_CATALOGS_FAILURE:
      return (0, _immutabilityHelper.default)(state, {
        fetchCatalogs: {
          $merge: {
            isFetching: false,
            errors: action.payload
          }
        }
      });

    case _actionTypes.FETCH_CHART_CURRENCY:
      {
        var chartIso = (0, _utils.getCurrencyIso)(action.payload);
        var chartLocale = (0, _utils.getCurrencyLocale)(action.payload);
        return (0, _immutabilityHelper.default)(state, {
          currencyConfig: {
            $merge: {
              chartIso: chartIso,
              chartLocale: chartLocale
            }
          }
        });
      }

    case _actionTypes.FETCH_IMAGE_GALLERY:
      return (0, _immutabilityHelper.default)(state, {
        fetchImageGallery: {
          $merge: {
            isFetching: true,
            errors: []
          }
        }
      });

    case _actionTypes.FETCH_IMAGE_GALLERY_SUCCESS:
      return (0, _immutabilityHelper.default)(state, {
        fetchImageGallery: {
          $merge: {
            isFetching: false,
            data: action.payload
          }
        }
      });

    case _actionTypes.FETCH_IMAGE_GALLERY_FAILURE:
      return (0, _immutabilityHelper.default)(state, {
        fetchImageGallery: {
          $merge: {
            isFetching: false,
            errors: action.payload
          }
        }
      });

    case _actionTypes.FETCH_TABLE_CURRENCY:
      {
        var tableIso = (0, _utils.getCurrencyIso)(action.payload);
        var tableLocale = (0, _utils.getCurrencyLocale)(action.payload);
        return (0, _immutabilityHelper.default)(state, {
          currencyConfig: {
            $merge: {
              tableIso: tableIso,
              tableLocale: tableLocale
            }
          }
        });
      }

    case _actionTypes.FETCH_TABLE_WIDGETS:
      return (0, _immutabilityHelper.default)(state, {
        fetchTableWidgets: {
          $merge: {
            isFetching: true,
            errors: []
          }
        }
      });

    case _actionTypes.FETCH_TABLE_WIDGETS_SUCCESS:
      return (0, _immutabilityHelper.default)(state, {
        fetchTableWidgets: {
          $merge: {
            isFetching: false,
            data: action.payload
          }
        }
      });

    case _actionTypes.FETCH_TABLE_WIDGETS_FAILURE:
      return (0, _immutabilityHelper.default)(state, {
        fetchTableWidgets: {
          $merge: {
            isFetching: false,
            errors: action.payload
          }
        }
      });

    case _actionTypes.FETCH_TAGS_CURRENCY:
      {
        var tagsLocale = (0, _utils.getCurrencyLocale)(action.payload);
        return (0, _immutabilityHelper.default)(state, {
          currencyConfig: {
            $merge: {
              tagsLocale: tagsLocale
            }
          }
        });
      }

    case _actionTypes.FETCH_CHART_WIDGETS:
      return (0, _immutabilityHelper.default)(state, {
        fetchChartWidgets: {
          $merge: {
            isFetching: true,
            errors: []
          }
        }
      });

    case _actionTypes.FETCH_CHART_WIDGETS_SUCCESS:
      return (0, _immutabilityHelper.default)(state, {
        fetchChartWidgets: {
          $merge: {
            isFetching: false,
            data: action.payload
          }
        }
      });

    case _actionTypes.FETCH_CHART_WIDGETS_FAILURE:
      return (0, _immutabilityHelper.default)(state, {
        fetchTableWidgets: {
          $merge: {
            isFetching: false,
            errors: action.payload
          }
        }
      });

    case _actionTypes.INITIALIZE:
      return (0, _immutabilityHelper.default)(state, {
        initializing: {
          $set: true
        }
      });

    case _actionTypes.INITIALIZE_FAILURE:
    case _actionTypes.INITIALIZE_SUCCESS:
      return (0, _immutabilityHelper.default)(state, {
        initializing: {
          $set: false
        }
      });

    case _actionTypes.RESET:
      return (0, _immutabilityHelper.default)(state, {
        $set: INITIAL_STATE
      });

    case _actionTypes.SET_TEMPLATE_CONFIG:
      {
        var _action$payload = action.payload,
            baseUrl = _action$payload.baseUrl,
            froalaLicenseKey = _action$payload.froalaLicenseKey,
            googleApiKey = _action$payload.googleApiKey,
            token = _action$payload.token,
            language = _action$payload.language;
        return (0, _immutabilityHelper.default)(state, {
          baseUrl: {
            $set: baseUrl
          },
          token: {
            $set: token
          },
          googleApiKey: {
            $set: googleApiKey
          },
          froalaLicenseKey: {
            $set: froalaLicenseKey
          },
          language: {
            $set: language
          }
        });
      }

    default:
      return state;
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vcmVkdWNlci5qcyJdLCJuYW1lcyI6WyJJTklUSUFMX1NUQVRFIiwiY3VycmVuY3lDb25maWciLCJ0b2tlbiIsImJhc2VVcmwiLCJsYW5ndWFnZSIsImdvb2dsZUFwaUtleSIsImZyb2FsYUxpY2Vuc2VLZXkiLCJmZXRjaENhdGFsb2dzIiwiaXNGZXRjaGluZyIsImRhdGEiLCJlcnJvciIsImZldGNoVGFibGVXaWRnZXRzIiwiZXJyb3JzIiwiZmV0Y2hDaGFydFdpZGdldHMiLCJmZXRjaEltYWdlR2FsbGVyeSIsImluaXRpYWxpemluZyIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsIkZFVENIX0NBVEFMT0dTIiwiJG1lcmdlIiwiRkVUQ0hfQ0FUQUxPR1NfU1VDQ0VTUyIsInBheWxvYWQiLCJGRVRDSF9DQVRBTE9HU19GQUlMVVJFIiwiRkVUQ0hfQ0hBUlRfQ1VSUkVOQ1kiLCJjaGFydElzbyIsImNoYXJ0TG9jYWxlIiwiRkVUQ0hfSU1BR0VfR0FMTEVSWSIsIkZFVENIX0lNQUdFX0dBTExFUllfU1VDQ0VTUyIsIkZFVENIX0lNQUdFX0dBTExFUllfRkFJTFVSRSIsIkZFVENIX1RBQkxFX0NVUlJFTkNZIiwidGFibGVJc28iLCJ0YWJsZUxvY2FsZSIsIkZFVENIX1RBQkxFX1dJREdFVFMiLCJGRVRDSF9UQUJMRV9XSURHRVRTX1NVQ0NFU1MiLCJGRVRDSF9UQUJMRV9XSURHRVRTX0ZBSUxVUkUiLCJGRVRDSF9UQUdTX0NVUlJFTkNZIiwidGFnc0xvY2FsZSIsIkZFVENIX0NIQVJUX1dJREdFVFMiLCJGRVRDSF9DSEFSVF9XSURHRVRTX1NVQ0NFU1MiLCJGRVRDSF9DSEFSVF9XSURHRVRTX0ZBSUxVUkUiLCJJTklUSUFMSVpFIiwiJHNldCIsIklOSVRJQUxJWkVfRkFJTFVSRSIsIklOSVRJQUxJWkVfU1VDQ0VTUyIsIlJFU0VUIiwiU0VUX1RFTVBMQVRFX0NPTkZJRyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOztBQUVBOzs7O0FBdUJBLElBQU1BLGFBQWEsR0FBRztBQUNsQkMsRUFBQUEsY0FBYyxFQUFFLEVBREU7QUFFbEJDLEVBQUFBLEtBQUssRUFBRSxJQUZXO0FBR2xCQyxFQUFBQSxPQUFPLEVBQUUsSUFIUztBQUlsQkMsRUFBQUEsUUFBUSxFQUFFLElBSlE7QUFLbEJDLEVBQUFBLFlBQVksRUFBRSxJQUxJO0FBTWxCQyxFQUFBQSxnQkFBZ0IsRUFBRSxFQU5BO0FBT2xCQyxFQUFBQSxhQUFhLEVBQUU7QUFDWEMsSUFBQUEsVUFBVSxFQUFFLEtBREQ7QUFFWEMsSUFBQUEsSUFBSSxFQUFFLEVBRks7QUFHWEMsSUFBQUEsS0FBSyxFQUFFO0FBSEksR0FQRztBQVlsQkMsRUFBQUEsaUJBQWlCLEVBQUU7QUFDZkgsSUFBQUEsVUFBVSxFQUFFLElBREc7QUFFZkMsSUFBQUEsSUFBSSxFQUFFLEVBRlM7QUFHZkcsSUFBQUEsTUFBTSxFQUFFO0FBSE8sR0FaRDtBQWlCbEJDLEVBQUFBLGlCQUFpQixFQUFFO0FBQ2ZMLElBQUFBLFVBQVUsRUFBRSxJQURHO0FBRWZDLElBQUFBLElBQUksRUFBRSxFQUZTO0FBR2ZHLElBQUFBLE1BQU0sRUFBRTtBQUhPLEdBakJEO0FBc0JsQkUsRUFBQUEsaUJBQWlCLEVBQUU7QUFDZk4sSUFBQUEsVUFBVSxFQUFFLEtBREc7QUFFZkMsSUFBQUEsSUFBSSxFQUFFLEVBRlM7QUFHZkcsSUFBQUEsTUFBTSxFQUFFO0FBSE8sR0F0QkQ7QUEyQmxCRyxFQUFBQSxZQUFZLEVBQUU7QUEzQkksQ0FBdEI7O0FBOEJlLG9CQUF5QztBQUFBLE1BQS9CQyxLQUErQix1RUFBdkJoQixhQUF1QjtBQUFBLE1BQVJpQixNQUFROztBQUNwRCxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDSSxTQUFLQywyQkFBTDtBQUNJLGFBQU8saUNBQU9ILEtBQVAsRUFBYztBQUNqQlQsUUFBQUEsYUFBYSxFQUFFO0FBQ1hhLFVBQUFBLE1BQU0sRUFBRTtBQUNKWixZQUFBQSxVQUFVLEVBQUUsSUFEUjtBQUVKSSxZQUFBQSxNQUFNLEVBQUU7QUFGSjtBQURHO0FBREUsT0FBZCxDQUFQOztBQVNKLFNBQUtTLG1DQUFMO0FBQ0ksYUFBTyxpQ0FBT0wsS0FBUCxFQUFjO0FBQ2pCVCxRQUFBQSxhQUFhLEVBQUU7QUFDWGEsVUFBQUEsTUFBTSxFQUFFO0FBQ0paLFlBQUFBLFVBQVUsRUFBRSxLQURSO0FBRUpDLFlBQUFBLElBQUksRUFBRVEsTUFBTSxDQUFDSztBQUZUO0FBREc7QUFERSxPQUFkLENBQVA7O0FBU0osU0FBS0MsbUNBQUw7QUFDSSxhQUFPLGlDQUFPUCxLQUFQLEVBQWM7QUFDakJULFFBQUFBLGFBQWEsRUFBRTtBQUNYYSxVQUFBQSxNQUFNLEVBQUU7QUFDSlosWUFBQUEsVUFBVSxFQUFFLEtBRFI7QUFFSkksWUFBQUEsTUFBTSxFQUFFSyxNQUFNLENBQUNLO0FBRlg7QUFERztBQURFLE9BQWQsQ0FBUDs7QUFTSixTQUFLRSxpQ0FBTDtBQUEyQjtBQUN2QixZQUFNQyxRQUFRLEdBQUcsMkJBQWVSLE1BQU0sQ0FBQ0ssT0FBdEIsQ0FBakI7QUFDQSxZQUFNSSxXQUFXLEdBQUcsOEJBQWtCVCxNQUFNLENBQUNLLE9BQXpCLENBQXBCO0FBQ0EsZUFBTyxpQ0FBT04sS0FBUCxFQUFjO0FBQ2pCZixVQUFBQSxjQUFjLEVBQUU7QUFDWm1CLFlBQUFBLE1BQU0sRUFBRTtBQUNKSyxjQUFBQSxRQUFRLEVBQVJBLFFBREk7QUFFSkMsY0FBQUEsV0FBVyxFQUFYQTtBQUZJO0FBREk7QUFEQyxTQUFkLENBQVA7QUFRSDs7QUFDRCxTQUFLQyxnQ0FBTDtBQUNJLGFBQU8saUNBQU9YLEtBQVAsRUFBYztBQUNqQkYsUUFBQUEsaUJBQWlCLEVBQUU7QUFDZk0sVUFBQUEsTUFBTSxFQUFFO0FBQ0paLFlBQUFBLFVBQVUsRUFBRSxJQURSO0FBRUpJLFlBQUFBLE1BQU0sRUFBRTtBQUZKO0FBRE87QUFERixPQUFkLENBQVA7O0FBU0osU0FBS2dCLHdDQUFMO0FBQ0ksYUFBTyxpQ0FBT1osS0FBUCxFQUFjO0FBQ2pCRixRQUFBQSxpQkFBaUIsRUFBRTtBQUNmTSxVQUFBQSxNQUFNLEVBQUU7QUFDSlosWUFBQUEsVUFBVSxFQUFFLEtBRFI7QUFFSkMsWUFBQUEsSUFBSSxFQUFFUSxNQUFNLENBQUNLO0FBRlQ7QUFETztBQURGLE9BQWQsQ0FBUDs7QUFTSixTQUFLTyx3Q0FBTDtBQUNJLGFBQU8saUNBQU9iLEtBQVAsRUFBYztBQUNqQkYsUUFBQUEsaUJBQWlCLEVBQUU7QUFDZk0sVUFBQUEsTUFBTSxFQUFFO0FBQ0paLFlBQUFBLFVBQVUsRUFBRSxLQURSO0FBRUpJLFlBQUFBLE1BQU0sRUFBRUssTUFBTSxDQUFDSztBQUZYO0FBRE87QUFERixPQUFkLENBQVA7O0FBU0osU0FBS1EsaUNBQUw7QUFBMkI7QUFDdkIsWUFBTUMsUUFBUSxHQUFHLDJCQUFlZCxNQUFNLENBQUNLLE9BQXRCLENBQWpCO0FBQ0EsWUFBTVUsV0FBVyxHQUFHLDhCQUFrQmYsTUFBTSxDQUFDSyxPQUF6QixDQUFwQjtBQUNBLGVBQU8saUNBQU9OLEtBQVAsRUFBYztBQUNqQmYsVUFBQUEsY0FBYyxFQUFFO0FBQ1ptQixZQUFBQSxNQUFNLEVBQUU7QUFDSlcsY0FBQUEsUUFBUSxFQUFSQSxRQURJO0FBRUpDLGNBQUFBLFdBQVcsRUFBWEE7QUFGSTtBQURJO0FBREMsU0FBZCxDQUFQO0FBUUg7O0FBRUQsU0FBS0MsZ0NBQUw7QUFDSSxhQUFPLGlDQUFPakIsS0FBUCxFQUFjO0FBQ2pCTCxRQUFBQSxpQkFBaUIsRUFBRTtBQUNmUyxVQUFBQSxNQUFNLEVBQUU7QUFDSlosWUFBQUEsVUFBVSxFQUFFLElBRFI7QUFFSkksWUFBQUEsTUFBTSxFQUFFO0FBRko7QUFETztBQURGLE9BQWQsQ0FBUDs7QUFTSixTQUFLc0Isd0NBQUw7QUFDSSxhQUFPLGlDQUFPbEIsS0FBUCxFQUFjO0FBQ2pCTCxRQUFBQSxpQkFBaUIsRUFBRTtBQUNmUyxVQUFBQSxNQUFNLEVBQUU7QUFDSlosWUFBQUEsVUFBVSxFQUFFLEtBRFI7QUFFSkMsWUFBQUEsSUFBSSxFQUFFUSxNQUFNLENBQUNLO0FBRlQ7QUFETztBQURGLE9BQWQsQ0FBUDs7QUFTSixTQUFLYSx3Q0FBTDtBQUNJLGFBQU8saUNBQU9uQixLQUFQLEVBQWM7QUFDakJMLFFBQUFBLGlCQUFpQixFQUFFO0FBQ2ZTLFVBQUFBLE1BQU0sRUFBRTtBQUNKWixZQUFBQSxVQUFVLEVBQUUsS0FEUjtBQUVKSSxZQUFBQSxNQUFNLEVBQUVLLE1BQU0sQ0FBQ0s7QUFGWDtBQURPO0FBREYsT0FBZCxDQUFQOztBQVNKLFNBQUtjLGdDQUFMO0FBQTBCO0FBQ3RCLFlBQU1DLFVBQVUsR0FBRyw4QkFBa0JwQixNQUFNLENBQUNLLE9BQXpCLENBQW5CO0FBQ0EsZUFBTyxpQ0FBT04sS0FBUCxFQUFjO0FBQ2pCZixVQUFBQSxjQUFjLEVBQUU7QUFDWm1CLFlBQUFBLE1BQU0sRUFBRTtBQUNKaUIsY0FBQUEsVUFBVSxFQUFWQTtBQURJO0FBREk7QUFEQyxTQUFkLENBQVA7QUFPSDs7QUFFRCxTQUFLQyxnQ0FBTDtBQUNJLGFBQU8saUNBQU90QixLQUFQLEVBQWM7QUFDakJILFFBQUFBLGlCQUFpQixFQUFFO0FBQ2ZPLFVBQUFBLE1BQU0sRUFBRTtBQUNKWixZQUFBQSxVQUFVLEVBQUUsSUFEUjtBQUVKSSxZQUFBQSxNQUFNLEVBQUU7QUFGSjtBQURPO0FBREYsT0FBZCxDQUFQOztBQVNKLFNBQUsyQix3Q0FBTDtBQUNJLGFBQU8saUNBQU92QixLQUFQLEVBQWM7QUFDakJILFFBQUFBLGlCQUFpQixFQUFFO0FBQ2ZPLFVBQUFBLE1BQU0sRUFBRTtBQUNKWixZQUFBQSxVQUFVLEVBQUUsS0FEUjtBQUVKQyxZQUFBQSxJQUFJLEVBQUVRLE1BQU0sQ0FBQ0s7QUFGVDtBQURPO0FBREYsT0FBZCxDQUFQOztBQVNKLFNBQUtrQix3Q0FBTDtBQUNJLGFBQU8saUNBQU94QixLQUFQLEVBQWM7QUFDakJMLFFBQUFBLGlCQUFpQixFQUFFO0FBQ2ZTLFVBQUFBLE1BQU0sRUFBRTtBQUNKWixZQUFBQSxVQUFVLEVBQUUsS0FEUjtBQUVKSSxZQUFBQSxNQUFNLEVBQUVLLE1BQU0sQ0FBQ0s7QUFGWDtBQURPO0FBREYsT0FBZCxDQUFQOztBQVNKLFNBQUttQix1QkFBTDtBQUNJLGFBQU8saUNBQU96QixLQUFQLEVBQWM7QUFDakJELFFBQUFBLFlBQVksRUFBRTtBQUNWMkIsVUFBQUEsSUFBSSxFQUFFO0FBREk7QUFERyxPQUFkLENBQVA7O0FBS0osU0FBS0MsK0JBQUw7QUFDQSxTQUFLQywrQkFBTDtBQUNJLGFBQU8saUNBQU81QixLQUFQLEVBQWM7QUFDakJELFFBQUFBLFlBQVksRUFBRTtBQUNWMkIsVUFBQUEsSUFBSSxFQUFFO0FBREk7QUFERyxPQUFkLENBQVA7O0FBTUosU0FBS0csa0JBQUw7QUFDSSxhQUFPLGlDQUFPN0IsS0FBUCxFQUFjO0FBQ2pCMEIsUUFBQUEsSUFBSSxFQUFFMUM7QUFEVyxPQUFkLENBQVA7O0FBSUosU0FBSzhDLGdDQUFMO0FBQTBCO0FBQ3RCLDhCQUNJN0IsTUFBTSxDQUFDSyxPQURYO0FBQUEsWUFBUW5CLE9BQVIsbUJBQVFBLE9BQVI7QUFBQSxZQUFpQkcsZ0JBQWpCLG1CQUFpQkEsZ0JBQWpCO0FBQUEsWUFBbUNELFlBQW5DLG1CQUFtQ0EsWUFBbkM7QUFBQSxZQUFpREgsS0FBakQsbUJBQWlEQSxLQUFqRDtBQUFBLFlBQXdERSxRQUF4RCxtQkFBd0RBLFFBQXhEO0FBRUEsZUFBTyxpQ0FBT1ksS0FBUCxFQUFjO0FBQ2pCYixVQUFBQSxPQUFPLEVBQUU7QUFDTHVDLFlBQUFBLElBQUksRUFBRXZDO0FBREQsV0FEUTtBQUlqQkQsVUFBQUEsS0FBSyxFQUFFO0FBQ0h3QyxZQUFBQSxJQUFJLEVBQUV4QztBQURILFdBSlU7QUFPakJHLFVBQUFBLFlBQVksRUFBRTtBQUNWcUMsWUFBQUEsSUFBSSxFQUFFckM7QUFESSxXQVBHO0FBVWpCQyxVQUFBQSxnQkFBZ0IsRUFBRTtBQUNkb0MsWUFBQUEsSUFBSSxFQUFFcEM7QUFEUSxXQVZEO0FBYWpCRixVQUFBQSxRQUFRLEVBQUU7QUFDTnNDLFlBQUFBLElBQUksRUFBRXRDO0FBREE7QUFiTyxTQUFkLENBQVA7QUFpQkg7O0FBRUQ7QUFDSSxhQUFPWSxLQUFQO0FBdk1SO0FBeU1IIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHVwZGF0ZSBmcm9tICdpbW11dGFiaWxpdHktaGVscGVyJztcclxuXHJcbmltcG9ydCB7IGdldEN1cnJlbmN5SXNvLCBnZXRDdXJyZW5jeUxvY2FsZSB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBGRVRDSF9DQVRBTE9HUyxcclxuICAgIEZFVENIX0NBVEFMT0dTX0ZBSUxVUkUsXHJcbiAgICBGRVRDSF9DQVRBTE9HU19TVUNDRVNTLFxyXG4gICAgRkVUQ0hfQ0hBUlRfQ1VSUkVOQ1ksXHJcbiAgICBGRVRDSF9DSEFSVF9XSURHRVRTLFxyXG4gICAgRkVUQ0hfQ0hBUlRfV0lER0VUU19GQUlMVVJFLFxyXG4gICAgRkVUQ0hfQ0hBUlRfV0lER0VUU19TVUNDRVNTLFxyXG4gICAgRkVUQ0hfSU1BR0VfR0FMTEVSWSxcclxuICAgIEZFVENIX0lNQUdFX0dBTExFUllfRkFJTFVSRSxcclxuICAgIEZFVENIX0lNQUdFX0dBTExFUllfU1VDQ0VTUyxcclxuICAgIEZFVENIX1RBQkxFX0NVUlJFTkNZLFxyXG4gICAgRkVUQ0hfVEFCTEVfV0lER0VUUyxcclxuICAgIEZFVENIX1RBQkxFX1dJREdFVFNfRkFJTFVSRSxcclxuICAgIEZFVENIX1RBQkxFX1dJREdFVFNfU1VDQ0VTUyxcclxuICAgIEZFVENIX1RBR1NfQ1VSUkVOQ1ksXHJcbiAgICBJTklUSUFMSVpFLFxyXG4gICAgSU5JVElBTElaRV9GQUlMVVJFLFxyXG4gICAgSU5JVElBTElaRV9TVUNDRVNTLFxyXG4gICAgUkVTRVQsXHJcbiAgICBTRVRfVEVNUExBVEVfQ09ORklHLFxyXG59IGZyb20gJy4vYWN0aW9uVHlwZXMnO1xyXG5cclxuY29uc3QgSU5JVElBTF9TVEFURSA9IHtcclxuICAgIGN1cnJlbmN5Q29uZmlnOiB7fSxcclxuICAgIHRva2VuOiBudWxsLFxyXG4gICAgYmFzZVVybDogbnVsbCxcclxuICAgIGxhbmd1YWdlOiBudWxsLFxyXG4gICAgZ29vZ2xlQXBpS2V5OiBudWxsLFxyXG4gICAgZnJvYWxhTGljZW5zZUtleTogJycsXHJcbiAgICBmZXRjaENhdGFsb2dzOiB7XHJcbiAgICAgICAgaXNGZXRjaGluZzogZmFsc2UsXHJcbiAgICAgICAgZGF0YTogW10sXHJcbiAgICAgICAgZXJyb3I6IG51bGwsXHJcbiAgICB9LFxyXG4gICAgZmV0Y2hUYWJsZVdpZGdldHM6IHtcclxuICAgICAgICBpc0ZldGNoaW5nOiB0cnVlLFxyXG4gICAgICAgIGRhdGE6IFtdLFxyXG4gICAgICAgIGVycm9yczogW10sXHJcbiAgICB9LFxyXG4gICAgZmV0Y2hDaGFydFdpZGdldHM6IHtcclxuICAgICAgICBpc0ZldGNoaW5nOiB0cnVlLFxyXG4gICAgICAgIGRhdGE6IFtdLFxyXG4gICAgICAgIGVycm9yczogW10sXHJcbiAgICB9LFxyXG4gICAgZmV0Y2hJbWFnZUdhbGxlcnk6IHtcclxuICAgICAgICBpc0ZldGNoaW5nOiBmYWxzZSxcclxuICAgICAgICBkYXRhOiBbXSxcclxuICAgICAgICBlcnJvcnM6IG51bGwsXHJcbiAgICB9LFxyXG4gICAgaW5pdGlhbGl6aW5nOiB0cnVlLFxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gKHN0YXRlID0gSU5JVElBTF9TVEFURSwgYWN0aW9uKSB7XHJcbiAgICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSBGRVRDSF9DQVRBTE9HUzpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZmV0Y2hDYXRhbG9nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZldGNoaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9DQVRBTE9HU19TVUNDRVNTOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBmZXRjaENhdGFsb2dzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgRkVUQ0hfQ0FUQUxPR1NfRkFJTFVSRTpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZmV0Y2hDYXRhbG9nczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZldGNoaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgRkVUQ0hfQ0hBUlRfQ1VSUkVOQ1k6IHtcclxuICAgICAgICAgICAgY29uc3QgY2hhcnRJc28gPSBnZXRDdXJyZW5jeUlzbyhhY3Rpb24ucGF5bG9hZCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGNoYXJ0TG9jYWxlID0gZ2V0Q3VycmVuY3lMb2NhbGUoYWN0aW9uLnBheWxvYWQpO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBjdXJyZW5jeUNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGFydElzbyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRMb2NhbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlIEZFVENIX0lNQUdFX0dBTExFUlk6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGZldGNoSW1hZ2VHYWxsZXJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogW10sXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIEZFVENIX0lNQUdFX0dBTExFUllfU1VDQ0VTUzpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZmV0Y2hJbWFnZUdhbGxlcnk6IHtcclxuICAgICAgICAgICAgICAgICAgICAkbWVyZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNGZXRjaGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9JTUFHRV9HQUxMRVJZX0ZBSUxVUkU6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGZldGNoSW1hZ2VHYWxsZXJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9UQUJMRV9DVVJSRU5DWToge1xyXG4gICAgICAgICAgICBjb25zdCB0YWJsZUlzbyA9IGdldEN1cnJlbmN5SXNvKGFjdGlvbi5wYXlsb2FkKTtcclxuICAgICAgICAgICAgY29uc3QgdGFibGVMb2NhbGUgPSBnZXRDdXJyZW5jeUxvY2FsZShhY3Rpb24ucGF5bG9hZCk7XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGN1cnJlbmN5Q29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlSXNvLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWJsZUxvY2FsZSxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIEZFVENIX1RBQkxFX1dJREdFVFM6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGZldGNoVGFibGVXaWRnZXRzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogW10sXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIEZFVENIX1RBQkxFX1dJREdFVFNfU1VDQ0VTUzpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZmV0Y2hUYWJsZVdpZGdldHM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkbWVyZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNGZXRjaGluZzogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9UQUJMRV9XSURHRVRTX0ZBSUxVUkU6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGZldGNoVGFibGVXaWRnZXRzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9UQUdTX0NVUlJFTkNZOiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhZ3NMb2NhbGUgPSBnZXRDdXJyZW5jeUxvY2FsZShhY3Rpb24ucGF5bG9hZCk7XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGN1cnJlbmN5Q29uZmlnOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9DSEFSVF9XSURHRVRTOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBmZXRjaENoYXJ0V2lkZ2V0czoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZldGNoaW5nOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBGRVRDSF9DSEFSVF9XSURHRVRTX1NVQ0NFU1M6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGZldGNoQ2hhcnRXaWRnZXRzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJG1lcmdlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmV0Y2hpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgRkVUQ0hfQ0hBUlRfV0lER0VUU19GQUlMVVJFOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBmZXRjaFRhYmxlV2lkZ2V0czoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZldGNoaW5nOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgSU5JVElBTElaRTpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgaW5pdGlhbGl6aW5nOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIGNhc2UgSU5JVElBTElaRV9GQUlMVVJFOlxyXG4gICAgICAgIGNhc2UgSU5JVElBTElaRV9TVUNDRVNTOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBpbml0aWFsaXppbmc6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIFJFU0VUOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICAkc2V0OiBJTklUSUFMX1NUQVRFLFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBTRVRfVEVNUExBVEVfQ09ORklHOiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHsgYmFzZVVybCwgZnJvYWxhTGljZW5zZUtleSwgZ29vZ2xlQXBpS2V5LCB0b2tlbiwgbGFuZ3VhZ2UgfSA9XHJcbiAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZDtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgYmFzZVVybDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IGJhc2VVcmwsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgdG9rZW46IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiB0b2tlbixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBnb29nbGVBcGlLZXk6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBnb29nbGVBcGlLZXksXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZnJvYWxhTGljZW5zZUtleToge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IGZyb2FsYUxpY2Vuc2VLZXksXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbGFuZ3VhZ2U6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBsYW5ndWFnZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==