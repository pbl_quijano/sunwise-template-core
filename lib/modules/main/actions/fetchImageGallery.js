"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _api = require("../../../api");

var _utils = require("../../../helpers/utils");

var _actionTypes = require("../actionTypes");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var _default = function _default() {
  return function (dispatch, getState) {
    dispatch({
      type: _actionTypes.FETCH_IMAGE_GALLERY
    });
    (0, _api.getImageGallery)(getState())().then(function (response) {
      dispatch({
        type: _actionTypes.FETCH_IMAGE_GALLERY_SUCCESS,
        payload: Object.keys(response.data.data).reduce(function (acc, key) {
          return [].concat(_toConsumableArray(acc), _toConsumableArray(response.data.data[key].map(function (_ref) {
            var id = _ref.id,
                name = _ref.name,
                url_file = _ref.url_file;
            return {
              id: id,
              name: name,
              urlFile: (0, _utils.handleFileURL)(url_file, process.env.REACT_APP_S3_MEDIA_PATH)
            };
          })));
        }, [])
      });
    }).catch(function (error) {
      return dispatch({
        type: _actionTypes.FETCH_IMAGE_GALLERY_FAILURE,
        payload: error.response.data.errors
      });
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9mZXRjaEltYWdlR2FsbGVyeS5qcyJdLCJuYW1lcyI6WyJkaXNwYXRjaCIsImdldFN0YXRlIiwidHlwZSIsIkZFVENIX0lNQUdFX0dBTExFUlkiLCJ0aGVuIiwicmVzcG9uc2UiLCJGRVRDSF9JTUFHRV9HQUxMRVJZX1NVQ0NFU1MiLCJwYXlsb2FkIiwiT2JqZWN0Iiwia2V5cyIsImRhdGEiLCJyZWR1Y2UiLCJhY2MiLCJrZXkiLCJtYXAiLCJpZCIsIm5hbWUiLCJ1cmxfZmlsZSIsInVybEZpbGUiLCJwcm9jZXNzIiwiZW52IiwiUkVBQ1RfQVBQX1MzX01FRElBX1BBVEgiLCJjYXRjaCIsImVycm9yIiwiRkVUQ0hfSU1BR0VfR0FMTEVSWV9GQUlMVVJFIiwiZXJyb3JzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O2VBTWU7QUFBQSxTQUFNLFVBQUNBLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUN6Q0QsSUFBQUEsUUFBUSxDQUFDO0FBQUVFLE1BQUFBLElBQUksRUFBRUM7QUFBUixLQUFELENBQVI7QUFDQSw4QkFBZ0JGLFFBQVEsRUFBeEIsSUFDS0csSUFETCxDQUNVLFVBQUNDLFFBQUQsRUFBYztBQUNoQkwsTUFBQUEsUUFBUSxDQUFDO0FBQ0xFLFFBQUFBLElBQUksRUFBRUksd0NBREQ7QUFFTEMsUUFBQUEsT0FBTyxFQUFFQyxNQUFNLENBQUNDLElBQVAsQ0FBWUosUUFBUSxDQUFDSyxJQUFULENBQWNBLElBQTFCLEVBQWdDQyxNQUFoQyxDQUNMLFVBQUNDLEdBQUQsRUFBTUMsR0FBTjtBQUFBLDhDQUNPRCxHQURQLHNCQUVPUCxRQUFRLENBQUNLLElBQVQsQ0FBY0EsSUFBZCxDQUFtQkcsR0FBbkIsRUFBd0JDLEdBQXhCLENBQ0M7QUFBQSxnQkFBR0MsRUFBSCxRQUFHQSxFQUFIO0FBQUEsZ0JBQU9DLElBQVAsUUFBT0EsSUFBUDtBQUFBLGdCQUFhQyxRQUFiLFFBQWFBLFFBQWI7QUFBQSxtQkFBNkI7QUFDekJGLGNBQUFBLEVBQUUsRUFBRkEsRUFEeUI7QUFFekJDLGNBQUFBLElBQUksRUFBSkEsSUFGeUI7QUFHekJFLGNBQUFBLE9BQU8sRUFBRSwwQkFDTEQsUUFESyxFQUVMRSxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsdUJBRlA7QUFIZ0IsYUFBN0I7QUFBQSxXQURELENBRlA7QUFBQSxTQURLLEVBY0wsRUFkSztBQUZKLE9BQUQsQ0FBUjtBQW1CSCxLQXJCTCxFQXNCS0MsS0F0QkwsQ0FzQlcsVUFBQ0MsS0FBRDtBQUFBLGFBQ0h2QixRQUFRLENBQUM7QUFDTEUsUUFBQUEsSUFBSSxFQUFFc0Isd0NBREQ7QUFFTGpCLFFBQUFBLE9BQU8sRUFBRWdCLEtBQUssQ0FBQ2xCLFFBQU4sQ0FBZUssSUFBZixDQUFvQmU7QUFGeEIsT0FBRCxDQURMO0FBQUEsS0F0Qlg7QUE0QkgsR0E5QmM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgZ2V0SW1hZ2VHYWxsZXJ5IH0gZnJvbSAnQGFwaSc7XG5cbmltcG9ydCB7IGhhbmRsZUZpbGVVUkwgfSBmcm9tICdAaGVscGVycy91dGlscyc7XG5cbmltcG9ydCB7XG4gICAgRkVUQ0hfSU1BR0VfR0FMTEVSWSxcbiAgICBGRVRDSF9JTUFHRV9HQUxMRVJZX0ZBSUxVUkUsXG4gICAgRkVUQ0hfSU1BR0VfR0FMTEVSWV9TVUNDRVNTLFxufSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0ICgpID0+IChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcbiAgICBkaXNwYXRjaCh7IHR5cGU6IEZFVENIX0lNQUdFX0dBTExFUlkgfSk7XG4gICAgZ2V0SW1hZ2VHYWxsZXJ5KGdldFN0YXRlKCkpKClcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBkaXNwYXRjaCh7XG4gICAgICAgICAgICAgICAgdHlwZTogRkVUQ0hfSU1BR0VfR0FMTEVSWV9TVUNDRVNTLFxuICAgICAgICAgICAgICAgIHBheWxvYWQ6IE9iamVjdC5rZXlzKHJlc3BvbnNlLmRhdGEuZGF0YSkucmVkdWNlKFxuICAgICAgICAgICAgICAgICAgICAoYWNjLCBrZXkpID0+IFtcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLmFjYyxcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLnJlc3BvbnNlLmRhdGEuZGF0YVtrZXldLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyBpZCwgbmFtZSwgdXJsX2ZpbGUgfSkgPT4gKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybEZpbGU6IGhhbmRsZUZpbGVVUkwoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmxfZmlsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3MuZW52LlJFQUNUX0FQUF9TM19NRURJQV9QQVRIXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgIFtdXG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycm9yKSA9PlxuICAgICAgICAgICAgZGlzcGF0Y2goe1xuICAgICAgICAgICAgICAgIHR5cGU6IEZFVENIX0lNQUdFX0dBTExFUllfRkFJTFVSRSxcbiAgICAgICAgICAgICAgICBwYXlsb2FkOiBlcnJvci5yZXNwb25zZS5kYXRhLmVycm9ycyxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG59O1xuIl19