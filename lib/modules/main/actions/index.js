"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "fetchTagsCurrency", {
  enumerable: true,
  get: function get() {
    return _fetchTagsCurrency.default;
  }
});
Object.defineProperty(exports, "getTemplateUpdatedData", {
  enumerable: true,
  get: function get() {
    return _getTemplateUpdatedData.default;
  }
});
Object.defineProperty(exports, "initialize", {
  enumerable: true,
  get: function get() {
    return _initialize.default;
  }
});
Object.defineProperty(exports, "reset", {
  enumerable: true,
  get: function get() {
    return _reset.default;
  }
});
Object.defineProperty(exports, "setTemplateConfig", {
  enumerable: true,
  get: function get() {
    return _setTemplateConfig.default;
  }
});

var _fetchTagsCurrency = _interopRequireDefault(require("./fetchTagsCurrency"));

var _getTemplateUpdatedData = _interopRequireDefault(require("./getTemplateUpdatedData"));

var _initialize = _interopRequireDefault(require("./initialize"));

var _reset = _interopRequireDefault(require("./reset"));

var _setTemplateConfig = _interopRequireDefault(require("./setTemplateConfig"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCB7IGRlZmF1bHQgYXMgZmV0Y2hUYWdzQ3VycmVuY3kgfSBmcm9tICcuL2ZldGNoVGFnc0N1cnJlbmN5JztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBnZXRUZW1wbGF0ZVVwZGF0ZWREYXRhIH0gZnJvbSAnLi9nZXRUZW1wbGF0ZVVwZGF0ZWREYXRhJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBpbml0aWFsaXplIH0gZnJvbSAnLi9pbml0aWFsaXplJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyByZXNldCB9IGZyb20gJy4vcmVzZXQnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHNldFRlbXBsYXRlQ29uZmlnIH0gZnJvbSAnLi9zZXRUZW1wbGF0ZUNvbmZpZyc7XHJcbiJdfQ==