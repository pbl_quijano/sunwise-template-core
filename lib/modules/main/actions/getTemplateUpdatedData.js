"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default() {
  return function (_, getState) {
    var templateData = templateCoreSelectors.getTemplateUpdatingContentData(getState());
    return templateData;
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9nZXRUZW1wbGF0ZVVwZGF0ZWREYXRhLmpzIl0sIm5hbWVzIjpbIl8iLCJnZXRTdGF0ZSIsInRlbXBsYXRlRGF0YSIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldFRlbXBsYXRlVXBkYXRpbmdDb250ZW50RGF0YSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7Ozs7OztlQUVlO0FBQUEsU0FBTSxVQUFDQSxDQUFELEVBQUlDLFFBQUosRUFBaUI7QUFDbEMsUUFBTUMsWUFBWSxHQUFHQyxxQkFBcUIsQ0FBQ0MsOEJBQXRCLENBQ2pCSCxRQUFRLEVBRFMsQ0FBckI7QUFHQSxXQUFPQyxZQUFQO0FBQ0gsR0FMYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xuXG5leHBvcnQgZGVmYXVsdCAoKSA9PiAoXywgZ2V0U3RhdGUpID0+IHtcbiAgICBjb25zdCB0ZW1wbGF0ZURhdGEgPSB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0VGVtcGxhdGVVcGRhdGluZ0NvbnRlbnREYXRhKFxuICAgICAgICBnZXRTdGF0ZSgpXG4gICAgKTtcbiAgICByZXR1cm4gdGVtcGxhdGVEYXRhO1xufTtcbiJdfQ==