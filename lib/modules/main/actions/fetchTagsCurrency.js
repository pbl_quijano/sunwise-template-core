"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _api = require("../../../api");

var _actionTypes = require("../actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default() {
  return function (dispatch, getState) {
    (0, _api.getCompaniesMe)(getState())().then(function (response) {
      var defaultCurrency = (0, _get.default)(response.data, 'currency', {});
      dispatch({
        type: _actionTypes.FETCH_TAGS_CURRENCY,
        payload: defaultCurrency
      });
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9mZXRjaFRhZ3NDdXJyZW5jeS5qcyJdLCJuYW1lcyI6WyJkaXNwYXRjaCIsImdldFN0YXRlIiwidGhlbiIsInJlc3BvbnNlIiwiZGVmYXVsdEN1cnJlbmN5IiwiZGF0YSIsInR5cGUiLCJGRVRDSF9UQUdTX0NVUlJFTkNZIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOztBQUVBOzs7O2VBRWU7QUFBQSxTQUFNLFVBQUNBLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUN6Qyw2QkFBZUEsUUFBUSxFQUF2QixJQUE2QkMsSUFBN0IsQ0FBa0MsVUFBQ0MsUUFBRCxFQUFjO0FBQzVDLFVBQU1DLGVBQWUsR0FBRyxrQkFBSUQsUUFBUSxDQUFDRSxJQUFiLEVBQW1CLFVBQW5CLEVBQStCLEVBQS9CLENBQXhCO0FBRUFMLE1BQUFBLFFBQVEsQ0FBQztBQUNMTSxRQUFBQSxJQUFJLEVBQUVDLGdDQUREO0FBRUxDLFFBQUFBLE9BQU8sRUFBRUo7QUFGSixPQUFELENBQVI7QUFJSCxLQVBEO0FBUUgsR0FUYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xuXG5pbXBvcnQgeyBnZXRDb21wYW5pZXNNZSB9IGZyb20gJ0BhcGknO1xuXG5pbXBvcnQgeyBGRVRDSF9UQUdTX0NVUlJFTkNZIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAoKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgZ2V0Q29tcGFuaWVzTWUoZ2V0U3RhdGUoKSkoKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICBjb25zdCBkZWZhdWx0Q3VycmVuY3kgPSBnZXQocmVzcG9uc2UuZGF0YSwgJ2N1cnJlbmN5Jywge30pO1xuXG4gICAgICAgIGRpc3BhdGNoKHtcbiAgICAgICAgICAgIHR5cGU6IEZFVENIX1RBR1NfQ1VSUkVOQ1ksXG4gICAgICAgICAgICBwYXlsb2FkOiBkZWZhdWx0Q3VycmVuY3ksXG4gICAgICAgIH0pO1xuICAgIH0pO1xufTtcbiJdfQ==