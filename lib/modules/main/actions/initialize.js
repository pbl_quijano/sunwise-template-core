"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tags = require("../../../constants/tags");

var _actionTypes = require("../actionTypes");

var selectors = _interopRequireWildcard(require("../selectors"));

var _fetchCatalogs = _interopRequireDefault(require("./fetchCatalogs"));

var _fetchChartCurrency = _interopRequireDefault(require("./fetchChartCurrency"));

var _fetchChartWidgets = _interopRequireDefault(require("./fetchChartWidgets"));

var _fetchImageGallery = _interopRequireDefault(require("./fetchImageGallery"));

var _fetchTableCurrency = _interopRequireDefault(require("./fetchTableCurrency"));

var _fetchTableWidgets = _interopRequireDefault(require("./fetchTableWidgets"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var getActions = function getActions(templateData, token, offerId, dispatch) {
  var actions = [dispatch((0, _fetchChartCurrency.default)()), dispatch((0, _fetchTableWidgets.default)(templateData.language)), dispatch((0, _fetchChartWidgets.default)(templateData.language))];

  if (offerId) {
    return [dispatch((0, _fetchTableCurrency.default)(offerId, token))].concat(actions);
  }

  if (token) {
    return [dispatch((0, _fetchImageGallery.default)()), dispatch((0, _fetchCatalogs.default)(_tags.PROPOSAL_SUMMARY_TYPE))].concat(actions);
  }

  return actions;
};

var _default = function _default(templateData, offerId) {
  return function (dispatch, getState) {
    dispatch({
      type: _actionTypes.INITIALIZE,
      payload: templateData
    });
    var token = selectors.getToken(getState());
    Promise.all(getActions(templateData, token, offerId, dispatch)).then(function () {
      dispatch({
        type: _actionTypes.INITIALIZE_SUCCESS
      });
    }).catch(function () {
      dispatch({
        type: _actionTypes.INITIALIZE_FAILURE
      });
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9pbml0aWFsaXplLmpzIl0sIm5hbWVzIjpbImdldEFjdGlvbnMiLCJ0ZW1wbGF0ZURhdGEiLCJ0b2tlbiIsIm9mZmVySWQiLCJkaXNwYXRjaCIsImFjdGlvbnMiLCJsYW5ndWFnZSIsIlBST1BPU0FMX1NVTU1BUllfVFlQRSIsImdldFN0YXRlIiwidHlwZSIsIklOSVRJQUxJWkUiLCJwYXlsb2FkIiwic2VsZWN0b3JzIiwiZ2V0VG9rZW4iLCJQcm9taXNlIiwiYWxsIiwidGhlbiIsIklOSVRJQUxJWkVfU1VDQ0VTUyIsImNhdGNoIiwiSU5JVElBTElaRV9GQUlMVVJFIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFLQTs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7QUFFQSxJQUFNQSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxZQUFELEVBQWVDLEtBQWYsRUFBc0JDLE9BQXRCLEVBQStCQyxRQUEvQixFQUE0QztBQUMzRCxNQUFJQyxPQUFPLEdBQUcsQ0FDVkQsUUFBUSxDQUFDLGtDQUFELENBREUsRUFFVkEsUUFBUSxDQUFDLGdDQUFrQkgsWUFBWSxDQUFDSyxRQUEvQixDQUFELENBRkUsRUFHVkYsUUFBUSxDQUFDLGdDQUFrQkgsWUFBWSxDQUFDSyxRQUEvQixDQUFELENBSEUsQ0FBZDs7QUFLQSxNQUFJSCxPQUFKLEVBQWE7QUFDVCxZQUFRQyxRQUFRLENBQUMsaUNBQW1CRCxPQUFuQixFQUE0QkQsS0FBNUIsQ0FBRCxDQUFoQixTQUF5REcsT0FBekQ7QUFDSDs7QUFDRCxNQUFJSCxLQUFKLEVBQVc7QUFDUCxZQUNJRSxRQUFRLENBQUMsaUNBQUQsQ0FEWixFQUVJQSxRQUFRLENBQUMsNEJBQWNHLDJCQUFkLENBQUQsQ0FGWixTQUdPRixPQUhQO0FBS0g7O0FBQ0QsU0FBT0EsT0FBUDtBQUNILENBakJEOztlQWtCZSxrQkFBQ0osWUFBRCxFQUFlRSxPQUFmO0FBQUEsU0FBMkIsVUFBQ0MsUUFBRCxFQUFXSSxRQUFYLEVBQXdCO0FBQzlESixJQUFBQSxRQUFRLENBQUM7QUFBRUssTUFBQUEsSUFBSSxFQUFFQyx1QkFBUjtBQUFvQkMsTUFBQUEsT0FBTyxFQUFFVjtBQUE3QixLQUFELENBQVI7QUFDQSxRQUFNQyxLQUFLLEdBQUdVLFNBQVMsQ0FBQ0MsUUFBVixDQUFtQkwsUUFBUSxFQUEzQixDQUFkO0FBQ0FNLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZZixVQUFVLENBQUNDLFlBQUQsRUFBZUMsS0FBZixFQUFzQkMsT0FBdEIsRUFBK0JDLFFBQS9CLENBQXRCLEVBQ0tZLElBREwsQ0FDVSxZQUFNO0FBQ1JaLE1BQUFBLFFBQVEsQ0FBQztBQUFFSyxRQUFBQSxJQUFJLEVBQUVRO0FBQVIsT0FBRCxDQUFSO0FBQ0gsS0FITCxFQUlLQyxLQUpMLENBSVcsWUFBTTtBQUNUZCxNQUFBQSxRQUFRLENBQUM7QUFBRUssUUFBQUEsSUFBSSxFQUFFVTtBQUFSLE9BQUQsQ0FBUjtBQUNILEtBTkw7QUFPSCxHQVZjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBST1BPU0FMX1NVTU1BUllfVFlQRSB9IGZyb20gJ0Bjb25zdGFudHMvdGFncyc7XG5cbmltcG9ydCB7XG4gICAgSU5JVElBTElaRSxcbiAgICBJTklUSUFMSVpFX1NVQ0NFU1MsXG4gICAgSU5JVElBTElaRV9GQUlMVVJFLFxufSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vc2VsZWN0b3JzJztcblxuaW1wb3J0IGZldGNoQ2F0YWxvZ3MgZnJvbSAnLi9mZXRjaENhdGFsb2dzJztcbmltcG9ydCBmZXRjaENoYXJ0Q3VycmVuY3kgZnJvbSAnLi9mZXRjaENoYXJ0Q3VycmVuY3knO1xuaW1wb3J0IGZldGNoQ2hhcnRXaWRnZXRzIGZyb20gJy4vZmV0Y2hDaGFydFdpZGdldHMnO1xuaW1wb3J0IGZldGNoQ29tcGFueUltYWdlR2FsbGVyeSBmcm9tICcuL2ZldGNoSW1hZ2VHYWxsZXJ5JztcbmltcG9ydCBmZXRjaFRhYmxlQ3VycmVuY3kgZnJvbSAnLi9mZXRjaFRhYmxlQ3VycmVuY3knO1xuaW1wb3J0IGZldGNoVGFibGVXaWRnZXRzIGZyb20gJy4vZmV0Y2hUYWJsZVdpZGdldHMnO1xuXG5jb25zdCBnZXRBY3Rpb25zID0gKHRlbXBsYXRlRGF0YSwgdG9rZW4sIG9mZmVySWQsIGRpc3BhdGNoKSA9PiB7XG4gICAgbGV0IGFjdGlvbnMgPSBbXG4gICAgICAgIGRpc3BhdGNoKGZldGNoQ2hhcnRDdXJyZW5jeSgpKSxcbiAgICAgICAgZGlzcGF0Y2goZmV0Y2hUYWJsZVdpZGdldHModGVtcGxhdGVEYXRhLmxhbmd1YWdlKSksXG4gICAgICAgIGRpc3BhdGNoKGZldGNoQ2hhcnRXaWRnZXRzKHRlbXBsYXRlRGF0YS5sYW5ndWFnZSkpLFxuICAgIF07XG4gICAgaWYgKG9mZmVySWQpIHtcbiAgICAgICAgcmV0dXJuIFtkaXNwYXRjaChmZXRjaFRhYmxlQ3VycmVuY3kob2ZmZXJJZCwgdG9rZW4pKSwgLi4uYWN0aW9uc107XG4gICAgfVxuICAgIGlmICh0b2tlbikge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgZGlzcGF0Y2goZmV0Y2hDb21wYW55SW1hZ2VHYWxsZXJ5KCkpLFxuICAgICAgICAgICAgZGlzcGF0Y2goZmV0Y2hDYXRhbG9ncyhQUk9QT1NBTF9TVU1NQVJZX1RZUEUpKSxcbiAgICAgICAgICAgIC4uLmFjdGlvbnMsXG4gICAgICAgIF07XG4gICAgfVxuICAgIHJldHVybiBhY3Rpb25zO1xufTtcbmV4cG9ydCBkZWZhdWx0ICh0ZW1wbGF0ZURhdGEsIG9mZmVySWQpID0+IChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcbiAgICBkaXNwYXRjaCh7IHR5cGU6IElOSVRJQUxJWkUsIHBheWxvYWQ6IHRlbXBsYXRlRGF0YSB9KTtcbiAgICBjb25zdCB0b2tlbiA9IHNlbGVjdG9ycy5nZXRUb2tlbihnZXRTdGF0ZSgpKTtcbiAgICBQcm9taXNlLmFsbChnZXRBY3Rpb25zKHRlbXBsYXRlRGF0YSwgdG9rZW4sIG9mZmVySWQsIGRpc3BhdGNoKSlcbiAgICAgICAgLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgZGlzcGF0Y2goeyB0eXBlOiBJTklUSUFMSVpFX1NVQ0NFU1MgfSk7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoKSA9PiB7XG4gICAgICAgICAgICBkaXNwYXRjaCh7IHR5cGU6IElOSVRJQUxJWkVfRkFJTFVSRSB9KTtcbiAgICAgICAgfSk7XG59O1xuIl19