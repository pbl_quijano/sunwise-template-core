"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(templateConfig) {
  return {
    type: _actionTypes.SET_TEMPLATE_CONFIG,
    payload: templateConfig
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9zZXRUZW1wbGF0ZUNvbmZpZy5qcyJdLCJuYW1lcyI6WyJ0ZW1wbGF0ZUNvbmZpZyIsInR5cGUiLCJTRVRfVEVNUExBVEVfQ09ORklHIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxjQUFEO0FBQUEsU0FBcUI7QUFDaENDLElBQUFBLElBQUksRUFBRUMsZ0NBRDBCO0FBRWhDQyxJQUFBQSxPQUFPLEVBQUVIO0FBRnVCLEdBQXJCO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9URU1QTEFURV9DT05GSUcgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0ICh0ZW1wbGF0ZUNvbmZpZykgPT4gKHtcbiAgICB0eXBlOiBTRVRfVEVNUExBVEVfQ09ORklHLFxuICAgIHBheWxvYWQ6IHRlbXBsYXRlQ29uZmlnLFxufSk7XG4iXX0=