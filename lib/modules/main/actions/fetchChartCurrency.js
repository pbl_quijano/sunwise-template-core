"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _api = require("../../../api");

var _actionTypes = require("../actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default() {
  return function (dispatch, getState) {
    (0, _api.getMe)(getState())().then(function (response) {
      dispatch({
        type: _actionTypes.FETCH_CHART_CURRENCY,
        payload: (0, _get.default)(response.data, 'currency_company_locale', {})
      });
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL21haW4vYWN0aW9ucy9mZXRjaENoYXJ0Q3VycmVuY3kuanMiXSwibmFtZXMiOlsiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInRoZW4iLCJyZXNwb25zZSIsInR5cGUiLCJGRVRDSF9DSEFSVF9DVVJSRU5DWSIsInBheWxvYWQiLCJkYXRhIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBRUE7Ozs7ZUFFZTtBQUFBLFNBQU0sVUFBQ0EsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQ3pDLG9CQUFNQSxRQUFRLEVBQWQsSUFBb0JDLElBQXBCLENBQXlCLFVBQUNDLFFBQUQsRUFBYztBQUNuQ0gsTUFBQUEsUUFBUSxDQUFDO0FBQ0xJLFFBQUFBLElBQUksRUFBRUMsaUNBREQ7QUFFTEMsUUFBQUEsT0FBTyxFQUFFLGtCQUFJSCxRQUFRLENBQUNJLElBQWIsRUFBbUIseUJBQW5CLEVBQThDLEVBQTlDO0FBRkosT0FBRCxDQUFSO0FBSUgsS0FMRDtBQU1ILEdBUGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldCBmcm9tICdsb2Rhc2gvZ2V0JztcblxuaW1wb3J0IHsgZ2V0TWUgfSBmcm9tICdAYXBpJztcblxuaW1wb3J0IHsgRkVUQ0hfQ0hBUlRfQ1VSUkVOQ1kgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0ICgpID0+IChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcbiAgICBnZXRNZShnZXRTdGF0ZSgpKSgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgIGRpc3BhdGNoKHtcbiAgICAgICAgICAgIHR5cGU6IEZFVENIX0NIQVJUX0NVUlJFTkNZLFxuICAgICAgICAgICAgcGF5bG9hZDogZ2V0KHJlc3BvbnNlLmRhdGEsICdjdXJyZW5jeV9jb21wYW55X2xvY2FsZScsIHt9KSxcbiAgICAgICAgfSk7XG4gICAgfSk7XG59O1xuIl19