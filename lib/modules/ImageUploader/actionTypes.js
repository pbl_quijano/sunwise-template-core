"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SAVE_IMAGE_SUCCESS = exports.SAVE_IMAGE = exports.INITIALIZE = void 0;
var NAME = 'imageUploader';
var INITIALIZE = "".concat(NAME, "/INITIALIZE");
exports.INITIALIZE = INITIALIZE;
var SAVE_IMAGE = "".concat(NAME, "/SAVE_IMAGE");
exports.SAVE_IMAGE = SAVE_IMAGE;
var SAVE_IMAGE_SUCCESS = "".concat(NAME, "/SAVE_IMAGE_SUCCESS");
exports.SAVE_IMAGE_SUCCESS = SAVE_IMAGE_SUCCESS;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL0ltYWdlVXBsb2FkZXIvYWN0aW9uVHlwZXMuanMiXSwibmFtZXMiOlsiTkFNRSIsIklOSVRJQUxJWkUiLCJTQVZFX0lNQUdFIiwiU0FWRV9JTUFHRV9TVUNDRVNTIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxJQUFNQSxJQUFJLEdBQUcsZUFBYjtBQUNPLElBQU1DLFVBQVUsYUFBTUQsSUFBTixnQkFBaEI7O0FBQ0EsSUFBTUUsVUFBVSxhQUFNRixJQUFOLGdCQUFoQjs7QUFDQSxJQUFNRyxrQkFBa0IsYUFBTUgsSUFBTix3QkFBeEIiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBOQU1FID0gJ2ltYWdlVXBsb2FkZXInO1xuZXhwb3J0IGNvbnN0IElOSVRJQUxJWkUgPSBgJHtOQU1FfS9JTklUSUFMSVpFYDtcbmV4cG9ydCBjb25zdCBTQVZFX0lNQUdFID0gYCR7TkFNRX0vU0FWRV9JTUFHRWA7XG5leHBvcnQgY29uc3QgU0FWRV9JTUFHRV9TVUNDRVNTID0gYCR7TkFNRX0vU0FWRV9JTUFHRV9TVUNDRVNTYDtcbiJdfQ==