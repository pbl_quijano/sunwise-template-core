"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _api = require("../../../api");

var mainSelectors = _interopRequireWildcard(require("../../main/selectors"));

var _actionTypes = require("../actionTypes");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var getImageUrl = function getImageUrl(url, state) {
  if (url.includes('https')) {
    return url;
  }

  var baseURL = mainSelectors.getBaseUrl(state);
  return "".concat(baseURL).concat(url);
};

var _default = function _default(image) {
  return function (dispatch, getState) {
    if (image) {
      var state = getState();
      dispatch({
        type: _actionTypes.SAVE_IMAGE
      });
      (0, _api.createImage)(state)({
        image: image
      }).then(function (response) {
        var imgSrc = getImageUrl(response.data.image, state);
        dispatch({
          type: _actionTypes.SAVE_IMAGE_SUCCESS,
          payload: imgSrc
        });
        dispatch({
          type: _actionTypes.INITIALIZE
        });
      });
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL0ltYWdlVXBsb2FkZXIvYWN0aW9ucy9zYXZlSW1hZ2UuanMiXSwibmFtZXMiOlsiZ2V0SW1hZ2VVcmwiLCJ1cmwiLCJzdGF0ZSIsImluY2x1ZGVzIiwiYmFzZVVSTCIsIm1haW5TZWxlY3RvcnMiLCJnZXRCYXNlVXJsIiwiaW1hZ2UiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwidHlwZSIsIlNBVkVfSU1BR0UiLCJ0aGVuIiwicmVzcG9uc2UiLCJpbWdTcmMiLCJkYXRhIiwiU0FWRV9JTUFHRV9TVUNDRVNTIiwicGF5bG9hZCIsIklOSVRJQUxJWkUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUVBOzs7Ozs7QUFFQSxJQUFNQSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFDQyxHQUFELEVBQU1DLEtBQU4sRUFBZ0I7QUFDaEMsTUFBSUQsR0FBRyxDQUFDRSxRQUFKLENBQWEsT0FBYixDQUFKLEVBQTJCO0FBQ3ZCLFdBQU9GLEdBQVA7QUFDSDs7QUFDRCxNQUFNRyxPQUFPLEdBQUdDLGFBQWEsQ0FBQ0MsVUFBZCxDQUF5QkosS0FBekIsQ0FBaEI7QUFDQSxtQkFBVUUsT0FBVixTQUFvQkgsR0FBcEI7QUFDSCxDQU5EOztlQVFlLGtCQUFDTSxLQUFEO0FBQUEsU0FBVyxVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDOUMsUUFBSUYsS0FBSixFQUFXO0FBQ1AsVUFBTUwsS0FBSyxHQUFHTyxRQUFRLEVBQXRCO0FBQ0FELE1BQUFBLFFBQVEsQ0FBQztBQUFFRSxRQUFBQSxJQUFJLEVBQUVDO0FBQVIsT0FBRCxDQUFSO0FBQ0EsNEJBQVlULEtBQVosRUFBbUI7QUFBRUssUUFBQUEsS0FBSyxFQUFMQTtBQUFGLE9BQW5CLEVBQThCSyxJQUE5QixDQUFtQyxVQUFDQyxRQUFELEVBQWM7QUFDN0MsWUFBTUMsTUFBTSxHQUFHZCxXQUFXLENBQUNhLFFBQVEsQ0FBQ0UsSUFBVCxDQUFjUixLQUFmLEVBQXNCTCxLQUF0QixDQUExQjtBQUNBTSxRQUFBQSxRQUFRLENBQUM7QUFBRUUsVUFBQUEsSUFBSSxFQUFFTSwrQkFBUjtBQUE0QkMsVUFBQUEsT0FBTyxFQUFFSDtBQUFyQyxTQUFELENBQVI7QUFDQU4sUUFBQUEsUUFBUSxDQUFDO0FBQUVFLFVBQUFBLElBQUksRUFBRVE7QUFBUixTQUFELENBQVI7QUFDSCxPQUpEO0FBS0g7QUFDSixHQVZjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZUltYWdlIH0gZnJvbSAnQGFwaSc7XG5cbmltcG9ydCAqIGFzIG1haW5TZWxlY3RvcnMgZnJvbSAnQG1haW4vc2VsZWN0b3JzJztcblxuaW1wb3J0IHsgSU5JVElBTElaRSwgU0FWRV9JTUFHRSwgU0FWRV9JTUFHRV9TVUNDRVNTIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5jb25zdCBnZXRJbWFnZVVybCA9ICh1cmwsIHN0YXRlKSA9PiB7XG4gICAgaWYgKHVybC5pbmNsdWRlcygnaHR0cHMnKSkge1xuICAgICAgICByZXR1cm4gdXJsO1xuICAgIH1cbiAgICBjb25zdCBiYXNlVVJMID0gbWFpblNlbGVjdG9ycy5nZXRCYXNlVXJsKHN0YXRlKTtcbiAgICByZXR1cm4gYCR7YmFzZVVSTH0ke3VybH1gO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgKGltYWdlKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgaWYgKGltYWdlKSB7XG4gICAgICAgIGNvbnN0IHN0YXRlID0gZ2V0U3RhdGUoKTtcbiAgICAgICAgZGlzcGF0Y2goeyB0eXBlOiBTQVZFX0lNQUdFIH0pO1xuICAgICAgICBjcmVhdGVJbWFnZShzdGF0ZSkoeyBpbWFnZSB9KS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgaW1nU3JjID0gZ2V0SW1hZ2VVcmwocmVzcG9uc2UuZGF0YS5pbWFnZSwgc3RhdGUpO1xuICAgICAgICAgICAgZGlzcGF0Y2goeyB0eXBlOiBTQVZFX0lNQUdFX1NVQ0NFU1MsIHBheWxvYWQ6IGltZ1NyYyB9KTtcbiAgICAgICAgICAgIGRpc3BhdGNoKHsgdHlwZTogSU5JVElBTElaRSB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxufTtcbiJdfQ==