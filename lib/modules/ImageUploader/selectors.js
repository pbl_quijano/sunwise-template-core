"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSavedImage = exports.getModel = exports.getIsSavingImage = void 0;

var _reselect = require("reselect");

var selectors = _interopRequireWildcard(require("../main/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var getModel = (0, _reselect.createSelector)(selectors.getLibState, function (sunwiseTemplateCore) {
  return sunwiseTemplateCore.imageUploader;
});
exports.getModel = getModel;
var getIsSavingImage = (0, _reselect.createSelector)(getModel, function (model) {
  return model.saveImage.isSaving;
});
exports.getIsSavingImage = getIsSavingImage;
var getSavedImage = (0, _reselect.createSelector)(getModel, function (model) {
  return model.saveImage.data;
});
exports.getSavedImage = getSavedImage;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL0ltYWdlVXBsb2FkZXIvc2VsZWN0b3JzLmpzIl0sIm5hbWVzIjpbImdldE1vZGVsIiwic2VsZWN0b3JzIiwiZ2V0TGliU3RhdGUiLCJzdW53aXNlVGVtcGxhdGVDb3JlIiwiaW1hZ2VVcGxvYWRlciIsImdldElzU2F2aW5nSW1hZ2UiLCJtb2RlbCIsInNhdmVJbWFnZSIsImlzU2F2aW5nIiwiZ2V0U2F2ZWRJbWFnZSIsImRhdGEiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUVBOzs7Ozs7QUFFTyxJQUFNQSxRQUFRLEdBQUcsOEJBQ3BCQyxTQUFTLENBQUNDLFdBRFUsRUFFcEIsVUFBQ0MsbUJBQUQ7QUFBQSxTQUF5QkEsbUJBQW1CLENBQUNDLGFBQTdDO0FBQUEsQ0FGb0IsQ0FBakI7O0FBS0EsSUFBTUMsZ0JBQWdCLEdBQUcsOEJBQzVCTCxRQUQ0QixFQUU1QixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDQyxTQUFOLENBQWdCQyxRQUEzQjtBQUFBLENBRjRCLENBQXpCOztBQUtBLElBQU1DLGFBQWEsR0FBRyw4QkFDekJULFFBRHlCLEVBRXpCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNDLFNBQU4sQ0FBZ0JHLElBQTNCO0FBQUEsQ0FGeUIsQ0FBdEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjcmVhdGVTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcblxuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XG5cbmV4cG9ydCBjb25zdCBnZXRNb2RlbCA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIHNlbGVjdG9ycy5nZXRMaWJTdGF0ZSxcbiAgICAoc3Vud2lzZVRlbXBsYXRlQ29yZSkgPT4gc3Vud2lzZVRlbXBsYXRlQ29yZS5pbWFnZVVwbG9hZGVyXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0SXNTYXZpbmdJbWFnZSA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuc2F2ZUltYWdlLmlzU2F2aW5nXG4pO1xuXG5leHBvcnQgY29uc3QgZ2V0U2F2ZWRJbWFnZSA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIGdldE1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuc2F2ZUltYWdlLmRhdGFcbik7XG4iXX0=