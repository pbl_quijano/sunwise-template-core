"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _values = require("../../constants/values");

var _showToast = _interopRequireDefault(require("../../helpers/showToast"));

var actions = _interopRequireWildcard(require("./actions"));

var selectors = _interopRequireWildcard(require("./selectors"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Content = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var UploadButton = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    background-color: #fff;\n    border-radius: 16px;\n    border: 1px solid #ccc;\n    color: #ff9a00;\n    cursor: pointer;\n    font-size: 14px;\n    font-weight: bold;\n    height: 32px;\n    line-height: 32px;\n    opacity: ", ";\n    pointer-events: ", ";\n    text-align: center;\n    transition: all 0.2s linear;\n    width: 164px;\n"])), function (_ref) {
  var isSavingImage = _ref.isSavingImage;
  return isSavingImage ? '0.5' : '1';
}, function (_ref2) {
  var isSavingImage = _ref2.isSavingImage;
  return isSavingImage ? 'none' : 'all';
});

var FileInput = _styledComponents.default.input(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    cursor: pointer;\n    display: block;\n    height: 0;\n    visibility: hidden;\n    width: 0;\n"])));

var ImageUploader = function ImageUploader(_ref3) {
  var currentValue = _ref3.currentValue,
      handleValueChange = _ref3.handleValueChange,
      isSavingImage = _ref3.isSavingImage,
      savedImage = _ref3.savedImage,
      saveImage = _ref3.saveImage;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var inputFileRef = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    if (!(0, _isNil.default)(savedImage) && savedImage !== currentValue) {
      (0, _showToast.default)({
        body: t('The image was uploaded successfully'),
        type: 'success'
      });
      handleValueChange(savedImage);
    }
  }, [savedImage]);

  var onChangeFile = function onChangeFile(event) {
    var files = event.target.files;

    if (files.length > 0) {
      var _files = _slicedToArray(files, 1),
          file = _files[0];

      if (!['image/jpeg', 'image/png'].includes(file.type)) {
        (0, _showToast.default)({
          body: t('Invalid file format'),
          type: 'danger'
        });
        return;
      }

      if (file.size >= _values.MAX_FILE_SIZE) {
        (0, _showToast.default)({
          body: t('The size is larger than allowed'),
          type: 'danger'
        });
        return;
      }

      saveImage(file);
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Content, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(UploadButton, {
      isSavingImage: isSavingImage,
      onClick: function onClick() {
        return inputFileRef.current.click();
      },
      children: isSavingImage ? t('Uploading image') : t('Upload image')
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(FileInput, {
      accept: "image/jpeg, image/png",
      type: "file",
      ref: inputFileRef,
      onChange: onChangeFile
    })]
  });
};

ImageUploader.propTypes = {
  currentValue: _propTypes.default.string,
  handleValueChange: _propTypes.default.func,
  isSavingImage: _propTypes.default.bool,
  savedImage: _propTypes.default.string,
  saveImage: _propTypes.default.func
};
var mapStateToProps = (0, _reselect.createStructuredSelector)({
  isSavingImage: selectors.getIsSavingImage,
  savedImage: selectors.getSavedImage
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    saveImage: function saveImage(image) {
      return dispatch(actions.saveImage(image));
    }
  };
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(ImageUploader);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL0ltYWdlVXBsb2FkZXIvaW5kZXguanMiXSwibmFtZXMiOlsiQ29udGVudCIsInN0eWxlZCIsImRpdiIsIlVwbG9hZEJ1dHRvbiIsImlzU2F2aW5nSW1hZ2UiLCJGaWxlSW5wdXQiLCJpbnB1dCIsIkltYWdlVXBsb2FkZXIiLCJjdXJyZW50VmFsdWUiLCJoYW5kbGVWYWx1ZUNoYW5nZSIsInNhdmVkSW1hZ2UiLCJzYXZlSW1hZ2UiLCJ0IiwiaW5wdXRGaWxlUmVmIiwiYm9keSIsInR5cGUiLCJvbkNoYW5nZUZpbGUiLCJldmVudCIsImZpbGVzIiwidGFyZ2V0IiwibGVuZ3RoIiwiZmlsZSIsImluY2x1ZGVzIiwic2l6ZSIsIk1BWF9GSUxFX1NJWkUiLCJjdXJyZW50IiwiY2xpY2siLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJmdW5jIiwiYm9vbCIsIm1hcFN0YXRlVG9Qcm9wcyIsInNlbGVjdG9ycyIsImdldElzU2F2aW5nSW1hZ2UiLCJnZXRTYXZlZEltYWdlIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJpbWFnZSIsImFjdGlvbnMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLE9BQU8sR0FBR0MsMEJBQU9DLEdBQVYsMElBQWI7O0FBTUEsSUFBTUMsWUFBWSxHQUFHRiwwQkFBT0MsR0FBViwrWkFVSDtBQUFBLE1BQUdFLGFBQUgsUUFBR0EsYUFBSDtBQUFBLFNBQXdCQSxhQUFhLEdBQUcsS0FBSCxHQUFXLEdBQWhEO0FBQUEsQ0FWRyxFQVdJO0FBQUEsTUFBR0EsYUFBSCxTQUFHQSxhQUFIO0FBQUEsU0FBd0JBLGFBQWEsR0FBRyxNQUFILEdBQVksS0FBakQ7QUFBQSxDQVhKLENBQWxCOztBQWlCQSxJQUFNQyxTQUFTLEdBQUdKLDBCQUFPSyxLQUFWLDRLQUFmOztBQVFBLElBQU1DLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsUUFNaEI7QUFBQSxNQUxGQyxZQUtFLFNBTEZBLFlBS0U7QUFBQSxNQUpGQyxpQkFJRSxTQUpGQSxpQkFJRTtBQUFBLE1BSEZMLGFBR0UsU0FIRkEsYUFHRTtBQUFBLE1BRkZNLFVBRUUsU0FGRkEsVUFFRTtBQUFBLE1BREZDLFNBQ0UsU0FERkEsU0FDRTs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EsTUFBTUMsWUFBWSxHQUFHLG1CQUFPLElBQVAsQ0FBckI7QUFFQSx3QkFBVSxZQUFNO0FBQ1osUUFBSSxDQUFDLG9CQUFNSCxVQUFOLENBQUQsSUFBc0JBLFVBQVUsS0FBS0YsWUFBekMsRUFBdUQ7QUFDbkQsOEJBQVU7QUFDTk0sUUFBQUEsSUFBSSxFQUFFRixDQUFDLENBQUMscUNBQUQsQ0FERDtBQUVORyxRQUFBQSxJQUFJLEVBQUU7QUFGQSxPQUFWO0FBSUFOLE1BQUFBLGlCQUFpQixDQUFDQyxVQUFELENBQWpCO0FBQ0g7QUFDSixHQVJELEVBUUcsQ0FBQ0EsVUFBRCxDQVJIOztBQVVBLE1BQU1NLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLEtBQUQsRUFBVztBQUM1QixRQUFRQyxLQUFSLEdBQWtCRCxLQUFLLENBQUNFLE1BQXhCLENBQVFELEtBQVI7O0FBQ0EsUUFBSUEsS0FBSyxDQUFDRSxNQUFOLEdBQWUsQ0FBbkIsRUFBc0I7QUFDbEIsa0NBQWVGLEtBQWY7QUFBQSxVQUFPRyxJQUFQOztBQUNBLFVBQUksQ0FBQyxDQUFDLFlBQUQsRUFBZSxXQUFmLEVBQTRCQyxRQUE1QixDQUFxQ0QsSUFBSSxDQUFDTixJQUExQyxDQUFMLEVBQXNEO0FBQ2xELGdDQUFVO0FBQ05ELFVBQUFBLElBQUksRUFBRUYsQ0FBQyxDQUFDLHFCQUFELENBREQ7QUFFTkcsVUFBQUEsSUFBSSxFQUFFO0FBRkEsU0FBVjtBQUlBO0FBQ0g7O0FBQ0QsVUFBSU0sSUFBSSxDQUFDRSxJQUFMLElBQWFDLHFCQUFqQixFQUFnQztBQUM1QixnQ0FBVTtBQUNOVixVQUFBQSxJQUFJLEVBQUVGLENBQUMsQ0FBQyxpQ0FBRCxDQUREO0FBRU5HLFVBQUFBLElBQUksRUFBRTtBQUZBLFNBQVY7QUFJQTtBQUNIOztBQUNESixNQUFBQSxTQUFTLENBQUNVLElBQUQsQ0FBVDtBQUNIO0FBQ0osR0FwQkQ7O0FBc0JBLHNCQUNJLHNCQUFDLE9BQUQ7QUFBQSw0QkFDSSxxQkFBQyxZQUFEO0FBQ0ksTUFBQSxhQUFhLEVBQUVqQixhQURuQjtBQUVJLE1BQUEsT0FBTyxFQUFFO0FBQUEsZUFBTVMsWUFBWSxDQUFDWSxPQUFiLENBQXFCQyxLQUFyQixFQUFOO0FBQUEsT0FGYjtBQUFBLGdCQUlLdEIsYUFBYSxHQUFHUSxDQUFDLENBQUMsaUJBQUQsQ0FBSixHQUEwQkEsQ0FBQyxDQUFDLGNBQUQ7QUFKN0MsTUFESixlQU9JLHFCQUFDLFNBQUQ7QUFDSSxNQUFBLE1BQU0sRUFBQyx1QkFEWDtBQUVJLE1BQUEsSUFBSSxFQUFDLE1BRlQ7QUFHSSxNQUFBLEdBQUcsRUFBRUMsWUFIVDtBQUlJLE1BQUEsUUFBUSxFQUFFRztBQUpkLE1BUEo7QUFBQSxJQURKO0FBZ0JILENBMUREOztBQTREQVQsYUFBYSxDQUFDb0IsU0FBZCxHQUEwQjtBQUN0Qm5CLEVBQUFBLFlBQVksRUFBRW9CLG1CQUFVQyxNQURGO0FBRXRCcEIsRUFBQUEsaUJBQWlCLEVBQUVtQixtQkFBVUUsSUFGUDtBQUd0QjFCLEVBQUFBLGFBQWEsRUFBRXdCLG1CQUFVRyxJQUhIO0FBSXRCckIsRUFBQUEsVUFBVSxFQUFFa0IsbUJBQVVDLE1BSkE7QUFLdEJsQixFQUFBQSxTQUFTLEVBQUVpQixtQkFBVUU7QUFMQyxDQUExQjtBQVFBLElBQU1FLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0M1QixFQUFBQSxhQUFhLEVBQUU2QixTQUFTLENBQUNDLGdCQURvQjtBQUU3Q3hCLEVBQUFBLFVBQVUsRUFBRXVCLFNBQVMsQ0FBQ0U7QUFGdUIsQ0FBekIsQ0FBeEI7O0FBS0EsSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0QzFCLElBQUFBLFNBQVMsRUFBRSxtQkFBQzJCLEtBQUQ7QUFBQSxhQUFXRCxRQUFRLENBQUNFLE9BQU8sQ0FBQzVCLFNBQVIsQ0FBa0IyQixLQUFsQixDQUFELENBQW5CO0FBQUE7QUFEMkIsR0FBZjtBQUFBLENBQTNCOztlQUllLHlCQUFRTixlQUFSLEVBQXlCSSxrQkFBekIsRUFBNkM3QixhQUE3QyxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VSZWYgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBNQVhfRklMRV9TSVpFIH0gZnJvbSAnQGNvbnN0YW50cy92YWx1ZXMnO1xuXG5pbXBvcnQgc2hvd1RvYXN0IGZyb20gJ0BoZWxwZXJzL3Nob3dUb2FzdCc7XG5cbmltcG9ydCAqIGFzIGFjdGlvbnMgZnJvbSAnLi9hY3Rpb25zJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuL3NlbGVjdG9ycyc7XG5cbmNvbnN0IENvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB3aWR0aDogMTAwJTtcbmA7XG5cbmNvbnN0IFVwbG9hZEJ1dHRvbiA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gICAgY29sb3I6ICNmZjlhMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBoZWlnaHQ6IDMycHg7XG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XG4gICAgb3BhY2l0eTogJHsoeyBpc1NhdmluZ0ltYWdlIH0pID0+IChpc1NhdmluZ0ltYWdlID8gJzAuNScgOiAnMScpfTtcbiAgICBwb2ludGVyLWV2ZW50czogJHsoeyBpc1NhdmluZ0ltYWdlIH0pID0+IChpc1NhdmluZ0ltYWdlID8gJ25vbmUnIDogJ2FsbCcpfTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMnMgbGluZWFyO1xuICAgIHdpZHRoOiAxNjRweDtcbmA7XG5cbmNvbnN0IEZpbGVJbnB1dCA9IHN0eWxlZC5pbnB1dGBcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgaGVpZ2h0OiAwO1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICB3aWR0aDogMDtcbmA7XG5cbmNvbnN0IEltYWdlVXBsb2FkZXIgPSAoe1xuICAgIGN1cnJlbnRWYWx1ZSxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZSxcbiAgICBpc1NhdmluZ0ltYWdlLFxuICAgIHNhdmVkSW1hZ2UsXG4gICAgc2F2ZUltYWdlLFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBpbnB1dEZpbGVSZWYgPSB1c2VSZWYobnVsbCk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoIWlzTmlsKHNhdmVkSW1hZ2UpICYmIHNhdmVkSW1hZ2UgIT09IGN1cnJlbnRWYWx1ZSkge1xuICAgICAgICAgICAgc2hvd1RvYXN0KHtcbiAgICAgICAgICAgICAgICBib2R5OiB0KCdUaGUgaW1hZ2Ugd2FzIHVwbG9hZGVkIHN1Y2Nlc3NmdWxseScpLFxuICAgICAgICAgICAgICAgIHR5cGU6ICdzdWNjZXNzJyxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2Uoc2F2ZWRJbWFnZSk7XG4gICAgICAgIH1cbiAgICB9LCBbc2F2ZWRJbWFnZV0pO1xuXG4gICAgY29uc3Qgb25DaGFuZ2VGaWxlID0gKGV2ZW50KSA9PiB7XG4gICAgICAgIGNvbnN0IHsgZmlsZXMgfSA9IGV2ZW50LnRhcmdldDtcbiAgICAgICAgaWYgKGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGNvbnN0IFtmaWxlXSA9IGZpbGVzO1xuICAgICAgICAgICAgaWYgKCFbJ2ltYWdlL2pwZWcnLCAnaW1hZ2UvcG5nJ10uaW5jbHVkZXMoZmlsZS50eXBlKSkge1xuICAgICAgICAgICAgICAgIHNob3dUb2FzdCh7XG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IHQoJ0ludmFsaWQgZmlsZSBmb3JtYXQnKSxcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2RhbmdlcicsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGZpbGUuc2l6ZSA+PSBNQVhfRklMRV9TSVpFKSB7XG4gICAgICAgICAgICAgICAgc2hvd1RvYXN0KHtcbiAgICAgICAgICAgICAgICAgICAgYm9keTogdCgnVGhlIHNpemUgaXMgbGFyZ2VyIHRoYW4gYWxsb3dlZCcpLFxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZGFuZ2VyJyxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzYXZlSW1hZ2UoZmlsZSk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRlbnQ+XG4gICAgICAgICAgICA8VXBsb2FkQnV0dG9uXG4gICAgICAgICAgICAgICAgaXNTYXZpbmdJbWFnZT17aXNTYXZpbmdJbWFnZX1cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBpbnB1dEZpbGVSZWYuY3VycmVudC5jbGljaygpfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtpc1NhdmluZ0ltYWdlID8gdCgnVXBsb2FkaW5nIGltYWdlJykgOiB0KCdVcGxvYWQgaW1hZ2UnKX1cbiAgICAgICAgICAgIDwvVXBsb2FkQnV0dG9uPlxuICAgICAgICAgICAgPEZpbGVJbnB1dFxuICAgICAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWcsIGltYWdlL3BuZ1wiXG4gICAgICAgICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgICAgICAgIHJlZj17aW5wdXRGaWxlUmVmfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZUZpbGV9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L0NvbnRlbnQ+XG4gICAgKTtcbn07XG5cbkltYWdlVXBsb2FkZXIucHJvcFR5cGVzID0ge1xuICAgIGN1cnJlbnRWYWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaXNTYXZpbmdJbWFnZTogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2F2ZWRJbWFnZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzYXZlSW1hZ2U6IFByb3BUeXBlcy5mdW5jLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcbiAgICBpc1NhdmluZ0ltYWdlOiBzZWxlY3RvcnMuZ2V0SXNTYXZpbmdJbWFnZSxcbiAgICBzYXZlZEltYWdlOiBzZWxlY3RvcnMuZ2V0U2F2ZWRJbWFnZSxcbn0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+ICh7XG4gICAgc2F2ZUltYWdlOiAoaW1hZ2UpID0+IGRpc3BhdGNoKGFjdGlvbnMuc2F2ZUltYWdlKGltYWdlKSksXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoSW1hZ2VVcGxvYWRlcik7XG4iXX0=