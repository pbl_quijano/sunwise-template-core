"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isInfiniteModeValid = exports.getStatePoolIndex = exports.getStatePoolDataLength = exports.getStatePoolData = exports.getStatePool = exports.getShowGuides = exports.getSelectedWidgetId = exports.getSelectedPageTheme = exports.getSelectedPageOrientation = exports.getSelectedPageIsBlocked = exports.getSelectedPageInfiniteMode = exports.getSelectedPageId = exports.getSelectedPage = exports.getModel = exports.getIsToolbarDisabled = exports.getDroppingItem = exports.getCopiedItem = void 0;

var _reselect = require("reselect");

var selectors = _interopRequireWildcard(require("../main/selectors"));

var templateCoreSelectors = _interopRequireWildcard(require("../TemplateCore/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getModel = (0, _reselect.createSelector)(selectors.getLibState, function (sunwiseTemplateCore) {
  return sunwiseTemplateCore.templateView;
});
exports.getModel = getModel;
var getCopiedItem = (0, _reselect.createSelector)(getModel, function (model) {
  return model.copiedItem;
});
exports.getCopiedItem = getCopiedItem;
var getDroppingItem = (0, _reselect.createSelector)(getModel, function (model) {
  return model.droppingItem;
});
exports.getDroppingItem = getDroppingItem;
var getIsToolbarDisabled = (0, _reselect.createSelector)(getModel, function (model) {
  return model.isToolbarDisabled;
});
exports.getIsToolbarDisabled = getIsToolbarDisabled;
var getSelectedWidgetId = (0, _reselect.createSelector)(getModel, function (model) {
  return model.selectedWidgetId;
});
exports.getSelectedWidgetId = getSelectedWidgetId;
var getSelectedPageId = (0, _reselect.createSelector)(getModel, function (model) {
  return model.selectedPageId;
});
exports.getSelectedPageId = getSelectedPageId;
var getSelectedPage = (0, _reselect.createSelector)(templateCoreSelectors.getEntitiesSession, getSelectedPageId, function (_ref, selectedPageId) {
  var Page = _ref.Page;

  if (selectedPageId === null || !Page.idExists(selectedPageId)) {
    return null;
  }

  var pageModel = Page.withId(selectedPageId);
  return _objectSpread(_objectSpread({}, pageModel.ref), {}, {
    widgets: pageModel.widgets.toRefArray()
  });
});
exports.getSelectedPage = getSelectedPage;
var getSelectedPageIsBlocked = (0, _reselect.createSelector)(getSelectedPage, function (selectedPage) {
  if (selectedPage === null) {
    return true;
  }

  return selectedPage.blocked === 1;
});
exports.getSelectedPageIsBlocked = getSelectedPageIsBlocked;
var getSelectedPageInfiniteMode = (0, _reselect.createSelector)(getSelectedPage, function (selectedPage) {
  if (selectedPage === null) {
    return false;
  }

  return selectedPage.infiniteMode;
});
exports.getSelectedPageInfiniteMode = getSelectedPageInfiniteMode;
var getSelectedPageOrientation = (0, _reselect.createSelector)(getSelectedPage, function (selectedPage) {
  if (selectedPage === null) {
    return '';
  }

  return selectedPage.orientation;
});
exports.getSelectedPageOrientation = getSelectedPageOrientation;
var getSelectedPageTheme = (0, _reselect.createSelector)(getSelectedPage, function (selectedPage) {
  if (selectedPage === null) {
    return 'defaultTheme';
  }

  return selectedPage.theme;
});
exports.getSelectedPageTheme = getSelectedPageTheme;
var isInfiniteModeValid = (0, _reselect.createSelector)(getSelectedPage, function (selectedPage) {
  if (selectedPage === null) {
    return false;
  }

  var tableCount = selectedPage.widgets.reduce(function (acc, widget) {
    if (widget.replaceInfoRequired === 'table') {
      return acc + 1;
    }

    return acc;
  }, 0);
  return tableCount === 1;
});
exports.isInfiniteModeValid = isInfiniteModeValid;
var getShowGuides = (0, _reselect.createSelector)(getModel, function (model) {
  return model.showGuides;
});
exports.getShowGuides = getShowGuides;
var getStatePool = (0, _reselect.createSelector)(getModel, function (model) {
  return model.statesPool;
});
exports.getStatePool = getStatePool;
var getStatePoolData = (0, _reselect.createSelector)(getStatePool, function (statesPool) {
  return statesPool.data;
});
exports.getStatePoolData = getStatePoolData;
var getStatePoolDataLength = (0, _reselect.createSelector)(getStatePoolData, function () {
  var statePoolData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return statePoolData.length;
});
exports.getStatePoolDataLength = getStatePoolDataLength;
var getStatePoolIndex = (0, _reselect.createSelector)(getStatePool, function (statesPool) {
  return statesPool.index;
});
exports.getStatePoolIndex = getStatePoolIndex;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9zZWxlY3RvcnMuanMiXSwibmFtZXMiOlsiZ2V0TW9kZWwiLCJzZWxlY3RvcnMiLCJnZXRMaWJTdGF0ZSIsInN1bndpc2VUZW1wbGF0ZUNvcmUiLCJ0ZW1wbGF0ZVZpZXciLCJnZXRDb3BpZWRJdGVtIiwibW9kZWwiLCJjb3BpZWRJdGVtIiwiZ2V0RHJvcHBpbmdJdGVtIiwiZHJvcHBpbmdJdGVtIiwiZ2V0SXNUb29sYmFyRGlzYWJsZWQiLCJpc1Rvb2xiYXJEaXNhYmxlZCIsImdldFNlbGVjdGVkV2lkZ2V0SWQiLCJzZWxlY3RlZFdpZGdldElkIiwiZ2V0U2VsZWN0ZWRQYWdlSWQiLCJzZWxlY3RlZFBhZ2VJZCIsImdldFNlbGVjdGVkUGFnZSIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEVudGl0aWVzU2Vzc2lvbiIsIlBhZ2UiLCJpZEV4aXN0cyIsInBhZ2VNb2RlbCIsIndpdGhJZCIsInJlZiIsIndpZGdldHMiLCJ0b1JlZkFycmF5IiwiZ2V0U2VsZWN0ZWRQYWdlSXNCbG9ja2VkIiwic2VsZWN0ZWRQYWdlIiwiYmxvY2tlZCIsImdldFNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSIsImluZmluaXRlTW9kZSIsImdldFNlbGVjdGVkUGFnZU9yaWVudGF0aW9uIiwib3JpZW50YXRpb24iLCJnZXRTZWxlY3RlZFBhZ2VUaGVtZSIsInRoZW1lIiwiaXNJbmZpbml0ZU1vZGVWYWxpZCIsInRhYmxlQ291bnQiLCJyZWR1Y2UiLCJhY2MiLCJ3aWRnZXQiLCJyZXBsYWNlSW5mb1JlcXVpcmVkIiwiZ2V0U2hvd0d1aWRlcyIsInNob3dHdWlkZXMiLCJnZXRTdGF0ZVBvb2wiLCJzdGF0ZXNQb29sIiwiZ2V0U3RhdGVQb29sRGF0YSIsImRhdGEiLCJnZXRTdGF0ZVBvb2xEYXRhTGVuZ3RoIiwic3RhdGVQb29sRGF0YSIsImxlbmd0aCIsImdldFN0YXRlUG9vbEluZGV4IiwiaW5kZXgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7QUFFTyxJQUFNQSxRQUFRLEdBQUcsOEJBQ3BCQyxTQUFTLENBQUNDLFdBRFUsRUFFcEIsVUFBQ0MsbUJBQUQ7QUFBQSxTQUF5QkEsbUJBQW1CLENBQUNDLFlBQTdDO0FBQUEsQ0FGb0IsQ0FBakI7O0FBS0EsSUFBTUMsYUFBYSxHQUFHLDhCQUN6QkwsUUFEeUIsRUFFekIsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ0MsVUFBakI7QUFBQSxDQUZ5QixDQUF0Qjs7QUFLQSxJQUFNQyxlQUFlLEdBQUcsOEJBQzNCUixRQUQyQixFQUUzQixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDRyxZQUFqQjtBQUFBLENBRjJCLENBQXhCOztBQUtBLElBQU1DLG9CQUFvQixHQUFHLDhCQUNoQ1YsUUFEZ0MsRUFFaEMsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ0ssaUJBQWpCO0FBQUEsQ0FGZ0MsQ0FBN0I7O0FBS0EsSUFBTUMsbUJBQW1CLEdBQUcsOEJBQy9CWixRQUQrQixFQUUvQixVQUFDTSxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDTyxnQkFBakI7QUFBQSxDQUYrQixDQUE1Qjs7QUFLQSxJQUFNQyxpQkFBaUIsR0FBRyw4QkFDN0JkLFFBRDZCLEVBRTdCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNTLGNBQWpCO0FBQUEsQ0FGNkIsQ0FBMUI7O0FBS0EsSUFBTUMsZUFBZSxHQUFHLDhCQUMzQkMscUJBQXFCLENBQUNDLGtCQURLLEVBRTNCSixpQkFGMkIsRUFHM0IsZ0JBQVdDLGNBQVgsRUFBOEI7QUFBQSxNQUEzQkksSUFBMkIsUUFBM0JBLElBQTJCOztBQUMxQixNQUFJSixjQUFjLEtBQUssSUFBbkIsSUFBMkIsQ0FBQ0ksSUFBSSxDQUFDQyxRQUFMLENBQWNMLGNBQWQsQ0FBaEMsRUFBK0Q7QUFDM0QsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsTUFBTU0sU0FBUyxHQUFHRixJQUFJLENBQUNHLE1BQUwsQ0FBWVAsY0FBWixDQUFsQjtBQUNBLHlDQUNPTSxTQUFTLENBQUNFLEdBRGpCO0FBRUlDLElBQUFBLE9BQU8sRUFBRUgsU0FBUyxDQUFDRyxPQUFWLENBQWtCQyxVQUFsQjtBQUZiO0FBSUgsQ0FaMEIsQ0FBeEI7O0FBZUEsSUFBTUMsd0JBQXdCLEdBQUcsOEJBQ3BDVixlQURvQyxFQUVwQyxVQUFDVyxZQUFELEVBQWtCO0FBQ2QsTUFBSUEsWUFBWSxLQUFLLElBQXJCLEVBQTJCO0FBQ3ZCLFdBQU8sSUFBUDtBQUNIOztBQUNELFNBQU9BLFlBQVksQ0FBQ0MsT0FBYixLQUF5QixDQUFoQztBQUNILENBUG1DLENBQWpDOztBQVVBLElBQU1DLDJCQUEyQixHQUFHLDhCQUN2Q2IsZUFEdUMsRUFFdkMsVUFBQ1csWUFBRCxFQUFrQjtBQUNkLE1BQUlBLFlBQVksS0FBSyxJQUFyQixFQUEyQjtBQUN2QixXQUFPLEtBQVA7QUFDSDs7QUFDRCxTQUFPQSxZQUFZLENBQUNHLFlBQXBCO0FBQ0gsQ0FQc0MsQ0FBcEM7O0FBVUEsSUFBTUMsMEJBQTBCLEdBQUcsOEJBQ3RDZixlQURzQyxFQUV0QyxVQUFDVyxZQUFELEVBQWtCO0FBQ2QsTUFBSUEsWUFBWSxLQUFLLElBQXJCLEVBQTJCO0FBQ3ZCLFdBQU8sRUFBUDtBQUNIOztBQUNELFNBQU9BLFlBQVksQ0FBQ0ssV0FBcEI7QUFDSCxDQVBxQyxDQUFuQzs7QUFVQSxJQUFNQyxvQkFBb0IsR0FBRyw4QkFDaENqQixlQURnQyxFQUVoQyxVQUFDVyxZQUFELEVBQWtCO0FBQ2QsTUFBSUEsWUFBWSxLQUFLLElBQXJCLEVBQTJCO0FBQ3ZCLFdBQU8sY0FBUDtBQUNIOztBQUNELFNBQU9BLFlBQVksQ0FBQ08sS0FBcEI7QUFDSCxDQVArQixDQUE3Qjs7QUFVQSxJQUFNQyxtQkFBbUIsR0FBRyw4QkFDL0JuQixlQUQrQixFQUUvQixVQUFDVyxZQUFELEVBQWtCO0FBQ2QsTUFBSUEsWUFBWSxLQUFLLElBQXJCLEVBQTJCO0FBQ3ZCLFdBQU8sS0FBUDtBQUNIOztBQUNELE1BQU1TLFVBQVUsR0FBR1QsWUFBWSxDQUFDSCxPQUFiLENBQXFCYSxNQUFyQixDQUE0QixVQUFDQyxHQUFELEVBQU1DLE1BQU4sRUFBaUI7QUFDNUQsUUFBSUEsTUFBTSxDQUFDQyxtQkFBUCxLQUErQixPQUFuQyxFQUE0QztBQUN4QyxhQUFPRixHQUFHLEdBQUcsQ0FBYjtBQUNIOztBQUNELFdBQU9BLEdBQVA7QUFDSCxHQUxrQixFQUtoQixDQUxnQixDQUFuQjtBQU1BLFNBQU9GLFVBQVUsS0FBSyxDQUF0QjtBQUNILENBYjhCLENBQTVCOztBQWdCQSxJQUFNSyxhQUFhLEdBQUcsOEJBQ3pCekMsUUFEeUIsRUFFekIsVUFBQ00sS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ29DLFVBQWpCO0FBQUEsQ0FGeUIsQ0FBdEI7O0FBS0EsSUFBTUMsWUFBWSxHQUFHLDhCQUN4QjNDLFFBRHdCLEVBRXhCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNzQyxVQUFqQjtBQUFBLENBRndCLENBQXJCOztBQUtBLElBQU1DLGdCQUFnQixHQUFHLDhCQUM1QkYsWUFENEIsRUFFNUIsVUFBQ0MsVUFBRDtBQUFBLFNBQWdCQSxVQUFVLENBQUNFLElBQTNCO0FBQUEsQ0FGNEIsQ0FBekI7O0FBS0EsSUFBTUMsc0JBQXNCLEdBQUcsOEJBQ2xDRixnQkFEa0MsRUFFbEM7QUFBQSxNQUFDRyxhQUFELHVFQUFpQixFQUFqQjtBQUFBLFNBQXdCQSxhQUFhLENBQUNDLE1BQXRDO0FBQUEsQ0FGa0MsQ0FBL0I7O0FBS0EsSUFBTUMsaUJBQWlCLEdBQUcsOEJBQzdCUCxZQUQ2QixFQUU3QixVQUFDQyxVQUFEO0FBQUEsU0FBZ0JBLFVBQVUsQ0FBQ08sS0FBM0I7QUFBQSxDQUY2QixDQUExQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZVNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xyXG5cclxuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldE1vZGVsID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBzZWxlY3RvcnMuZ2V0TGliU3RhdGUsXHJcbiAgICAoc3Vud2lzZVRlbXBsYXRlQ29yZSkgPT4gc3Vud2lzZVRlbXBsYXRlQ29yZS50ZW1wbGF0ZVZpZXdcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDb3BpZWRJdGVtID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRNb2RlbCxcclxuICAgIChtb2RlbCkgPT4gbW9kZWwuY29waWVkSXRlbVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldERyb3BwaW5nSXRlbSA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0TW9kZWwsXHJcbiAgICAobW9kZWwpID0+IG1vZGVsLmRyb3BwaW5nSXRlbVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldElzVG9vbGJhckRpc2FibGVkID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRNb2RlbCxcclxuICAgIChtb2RlbCkgPT4gbW9kZWwuaXNUb29sYmFyRGlzYWJsZWRcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTZWxlY3RlZFdpZGdldElkID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRNb2RlbCxcclxuICAgIChtb2RlbCkgPT4gbW9kZWwuc2VsZWN0ZWRXaWRnZXRJZFxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFNlbGVjdGVkUGFnZUlkID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRNb2RlbCxcclxuICAgIChtb2RlbCkgPT4gbW9kZWwuc2VsZWN0ZWRQYWdlSWRcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTZWxlY3RlZFBhZ2UgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIHRlbXBsYXRlQ29yZVNlbGVjdG9ycy5nZXRFbnRpdGllc1Nlc3Npb24sXHJcbiAgICBnZXRTZWxlY3RlZFBhZ2VJZCxcclxuICAgICh7IFBhZ2UgfSwgc2VsZWN0ZWRQYWdlSWQpID0+IHtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRQYWdlSWQgPT09IG51bGwgfHwgIVBhZ2UuaWRFeGlzdHMoc2VsZWN0ZWRQYWdlSWQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBwYWdlTW9kZWwgPSBQYWdlLndpdGhJZChzZWxlY3RlZFBhZ2VJZCk7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgLi4ucGFnZU1vZGVsLnJlZixcclxuICAgICAgICAgICAgd2lkZ2V0czogcGFnZU1vZGVsLndpZGdldHMudG9SZWZBcnJheSgpLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0U2VsZWN0ZWRQYWdlSXNCbG9ja2VkID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRTZWxlY3RlZFBhZ2UsXHJcbiAgICAoc2VsZWN0ZWRQYWdlKSA9PiB7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkUGFnZSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGVkUGFnZS5ibG9ja2VkID09PSAxO1xyXG4gICAgfVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0U2VsZWN0ZWRQYWdlLFxyXG4gICAgKHNlbGVjdGVkUGFnZSkgPT4ge1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2UgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRQYWdlLmluZmluaXRlTW9kZTtcclxuICAgIH1cclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTZWxlY3RlZFBhZ2VPcmllbnRhdGlvbiA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0U2VsZWN0ZWRQYWdlLFxyXG4gICAgKHNlbGVjdGVkUGFnZSkgPT4ge1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2UgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2VsZWN0ZWRQYWdlLm9yaWVudGF0aW9uO1xyXG4gICAgfVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFNlbGVjdGVkUGFnZVRoZW1lID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRTZWxlY3RlZFBhZ2UsXHJcbiAgICAoc2VsZWN0ZWRQYWdlKSA9PiB7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkUGFnZSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gJ2RlZmF1bHRUaGVtZSc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzZWxlY3RlZFBhZ2UudGhlbWU7XHJcbiAgICB9XHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgaXNJbmZpbml0ZU1vZGVWYWxpZCA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0U2VsZWN0ZWRQYWdlLFxyXG4gICAgKHNlbGVjdGVkUGFnZSkgPT4ge1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2UgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB0YWJsZUNvdW50ID0gc2VsZWN0ZWRQYWdlLndpZGdldHMucmVkdWNlKChhY2MsIHdpZGdldCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAod2lkZ2V0LnJlcGxhY2VJbmZvUmVxdWlyZWQgPT09ICd0YWJsZScpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhY2MgKyAxO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBhY2M7XHJcbiAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgcmV0dXJuIHRhYmxlQ291bnQgPT09IDE7XHJcbiAgICB9XHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0U2hvd0d1aWRlcyA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0TW9kZWwsXHJcbiAgICAobW9kZWwpID0+IG1vZGVsLnNob3dHdWlkZXNcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTdGF0ZVBvb2wgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldE1vZGVsLFxyXG4gICAgKG1vZGVsKSA9PiBtb2RlbC5zdGF0ZXNQb29sXHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0U3RhdGVQb29sRGF0YSA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0U3RhdGVQb29sLFxyXG4gICAgKHN0YXRlc1Bvb2wpID0+IHN0YXRlc1Bvb2wuZGF0YVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFN0YXRlUG9vbERhdGFMZW5ndGggPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldFN0YXRlUG9vbERhdGEsXHJcbiAgICAoc3RhdGVQb29sRGF0YSA9IFtdKSA9PiBzdGF0ZVBvb2xEYXRhLmxlbmd0aFxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFN0YXRlUG9vbEluZGV4ID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRTdGF0ZVBvb2wsXHJcbiAgICAoc3RhdGVzUG9vbCkgPT4gc3RhdGVzUG9vbC5pbmRleFxyXG4pO1xyXG4iXX0=