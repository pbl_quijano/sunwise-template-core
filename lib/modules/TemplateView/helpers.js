"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortingWidgetsByOrder = exports.selectedWidgetUseEffect = exports.isInfiniteModeEnabled = exports.hasSettingsPanel = exports.handleRightClickBuild = exports.handlePasteBuild = exports.handlePageKeyUp = exports.handlePageKeyDown = exports.handleOpenSettingsBuild = exports.handleOpenPageContextMenuBuild = exports.handleOpenItemContextMenuBuild = exports.handleOnStartResizeBuild = exports.handleOnStartDragBuild = exports.handleDropBuild = exports.handleCopyWidgetBuild = exports.handleCloseContextMenuBuild = exports.handleClickContainerBuild = exports.getSelectedWidgetValidated = exports.getPagePosition = exports.getOpenSettingsDisabled = exports.getIsLocationWidget = exports.getHasSummarySupport = exports.calculateInfinitePages = exports.calcPositionWithLimits = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _template = require("../../constants/template");

var _types = require("../../constants/types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var getScrollParent = function getScrollParent(node) {
  var regex = /(auto|scroll)/;

  var parents = function parents(_node, ps) {
    if (_node.parentNode === null) {
      return ps;
    }

    return parents(_node.parentNode, ps.concat([_node]));
  };

  var style = function style(_node, prop) {
    return getComputedStyle(_node, null).getPropertyValue(prop);
  };

  var overflow = function overflow(_node) {
    return style(_node, 'overflow') + style(_node, 'overflow-y') + style(_node, 'overflow-x');
  };

  var scroll = function scroll(_node) {
    return regex.test(overflow(_node));
  };
  /* eslint-disable consistent-return */


  var scrollParent = function scrollParent(_node) {
    if (!(_node instanceof HTMLElement || _node instanceof SVGElement)) {
      return;
    }

    var ps = parents(_node.parentNode, []);

    for (var i = 0; i < ps.length; i += 1) {
      if (scroll(ps[i])) {
        return ps[i];
      }
    }

    return document.scrollingElement || document.documentElement;
  };

  return scrollParent(node);
  /* eslint-enable consistent-return */
};

var getStartGridX = function getStartGridX(tempX, pageOrientation) {
  var resX = tempX % _template.GRID_WIDTH;
  var newX = resX >= 0 && resX < _template.GRID_WIDTH / 2 ? tempX - resX : tempX + (_template.GRID_WIDTH - resX);

  if (newX < 0) {
    return 0;
  }

  if (newX > (pageOrientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT)) {
    return tempX - resX;
  }

  return newX;
};

var getStartGridY = function getStartGridY(tempY, pageOrientation) {
  var resY = tempY % _template.GRID_HEIGHT;
  var newY = resY >= 0 && resY < _template.GRID_HEIGHT / 2 ? tempY - resY : tempY + (_template.GRID_HEIGHT - resY);

  if (newY < 0) {
    return 0;
  }

  if (newY > (pageOrientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH)) {
    return tempY - resY;
  }

  return newY;
};

var calculateInfinitePages = function calculateInfinitePages(_ref) {
  var pageId = _ref.pageId,
      rowDivs = _ref.rowDivs,
      tableContentHeight = _ref.tableContentHeight;
  var PADDING_BOTTOM_SPACE = 8;
  var TABLE_HEIGHT_LIMIT = tableContentHeight + PADDING_BOTTOM_SPACE;
  var initPosition = 0;
  var lastPosition = 0;
  var extraPagesData = [];
  var rowDivsLength = rowDivs.length;

  for (var index = 0; index < rowDivsLength; index++) {
    var divHeight = rowDivs[index].offsetHeight;
    var newTempPosition = lastPosition + divHeight;

    if (newTempPosition - initPosition > TABLE_HEIGHT_LIMIT || index + 1 === rowDivsLength) {
      extraPagesData.push({
        id: "".concat(pageId, "-clone-").concat(extraPagesData.length + 1),
        tablePosition: parseInt(initPosition),
        nextPosition: lastPosition
      });
      initPosition = lastPosition;
    }

    lastPosition = newTempPosition;
  }

  if (extraPagesData.length === 0) {
    return [];
  }

  return extraPagesData;
};

exports.calculateInfinitePages = calculateInfinitePages;

var getSelectedWidgetValidated = function getSelectedWidgetValidated(selectedWidgetId, pageId, pageWidgets) {
  if ((0, _isNil.default)(selectedWidgetId)) {
    return null;
  }

  var _selectedWidgetId$spl = selectedWidgetId.split('/'),
      _selectedWidgetId$spl2 = _slicedToArray(_selectedWidgetId$spl, 1),
      widgetPageId = _selectedWidgetId$spl2[0];

  if (widgetPageId !== pageId) {
    return null;
  }

  return pageWidgets.find(function (widget) {
    return widget.id === selectedWidgetId;
  });
};

exports.getSelectedWidgetValidated = getSelectedWidgetValidated;

var calcPositionWithLimits = function calcPositionWithLimits(x, y, width, height, pageOrientation, showGuides) {
  var pageWidth = pageOrientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
  var pageHeight = pageOrientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
  var limitX = pageWidth - width;
  var limitY = pageHeight - height;
  var newX = x > limitX ? limitX : x;
  var newY = y > limitY ? limitY : y;

  if (showGuides) {
    var fixedX = newX >= _template.GRID_WIDTH ? parseInt(newX / _template.GRID_WIDTH) * _template.GRID_WIDTH : 0;
    var fixedY = newY >= _template.GRID_HEIGHT ? parseInt(newY / _template.GRID_HEIGHT) * _template.GRID_HEIGHT : 0;
    return {
      x: fixedX,
      y: fixedY
    };
  }

  return {
    x: newX,
    y: newY
  };
};

exports.calcPositionWithLimits = calcPositionWithLimits;

var getHasSummarySupport = function getHasSummarySupport(selectedWidget) {
  return !(0, _isNil.default)(selectedWidget) && (0, _get.default)(selectedWidget, 'hasSummarySupport', false);
};

exports.getHasSummarySupport = getHasSummarySupport;

var getIsLocationWidget = function getIsLocationWidget(selectedWidget) {
  return !(0, _isNil.default)(selectedWidget) && ['map', 'segments-map'].includes(selectedWidget.name);
};

exports.getIsLocationWidget = getIsLocationWidget;

var getOpenSettingsDisabled = function getOpenSettingsDisabled(selectedWidget, templateType, chartWidgetsNames, tableWidgetsNames) {
  if (templateType === _types.MULTIPROPOSAL_TYPE && getHasSummarySupport(selectedWidget) && getIsLocationWidget(selectedWidget)) {
    return false;
  }

  if (!(0, _isNil.default)(selectedWidget)) {
    return !hasSettingsPanel([].concat(_toConsumableArray(chartWidgetsNames), _toConsumableArray(tableWidgetsNames)), selectedWidget.name);
  }

  return true;
};

exports.getOpenSettingsDisabled = getOpenSettingsDisabled;

var getPagePosition = function getPagePosition(elementRef) {
  if (!elementRef) {
    return null;
  }

  var boundingClientRect = elementRef.getBoundingClientRect();
  var scrolledParent = getScrollParent(elementRef);
  return {
    x: scrolledParent.scrollLeft + boundingClientRect.x,
    y: scrolledParent.scrollTop + boundingClientRect.y
  };
};

exports.getPagePosition = getPagePosition;

var hasSettingsPanel = function hasSettingsPanel(onlineWidgetsNames, type) {
  if ([].concat(_toConsumableArray(onlineWidgetsNames), ['clip-art', 'equipment-table', 'image', 'kwh-table', 'money-table', 'segments-map', 'separator']).includes(type)) {
    return true;
  }

  return false;
};
/* Handlers */


exports.hasSettingsPanel = hasSettingsPanel;

var handleClickContainerBuild = function handleClickContainerBuild(contextMenuData, handleCloseContextMenu, selectedWidgetId, selectWidget, wrapperRef) {
  return function (event) {
    if (contextMenuData.show) {
      handleCloseContextMenu();
    }

    if (event.target === wrapperRef.current && !(0, _isNil.default)(selectedWidgetId)) {
      selectWidget(null);
    }
  };
};

exports.handleClickContainerBuild = handleClickContainerBuild;

var handleCloseContextMenuBuild = function handleCloseContextMenuBuild(setContextMenuData) {
  return function (event) {
    if (event) event.preventDefault();
    setContextMenuData({
      x: 0,
      y: 0,
      pageX: 0,
      pageY: 0,
      show: false
    });
  };
};

exports.handleCloseContextMenuBuild = handleCloseContextMenuBuild;

var handleCopyWidgetBuild = function handleCopyWidgetBuild(setCopiedWidget) {
  return function (_ref2) {
    var field = _ref2.field,
        height = _ref2.height,
        name = _ref2.name,
        num = _ref2.num,
        replaceInfoRequired = _ref2.replaceInfoRequired,
        style = _ref2.style,
        value = _ref2.value,
        width = _ref2.width,
        x = _ref2.x,
        y = _ref2.y;
    setCopiedWidget({
      field: field,
      height: height,
      name: name,
      num: num,
      replaceInfoRequired: replaceInfoRequired,
      style: style,
      value: value,
      width: width,
      x: x,
      y: y
    });
  };
};

exports.handleCopyWidgetBuild = handleCopyWidgetBuild;

var handleDropBuild = function handleDropBuild(droppingItem, onAddWidget, setDroppingItem) {
  return function (event, placeholderPositionX, placeholderPositionY) {
    event.preventDefault();
    setDroppingItem(null);
    onAddWidget(droppingItem.id, {
      x: placeholderPositionX,
      y: placeholderPositionY
    });
  };
};

exports.handleDropBuild = handleDropBuild;

var handleOnStartDragBuild = function handleOnStartDragBuild(id, pageOrientation, setWidgetPosition, showGuides) {
  return function (_, _ref3) {
    var lastX = _ref3.lastX,
        lastY = _ref3.lastY;
    setWidgetPositionByAdjustment(id, pageOrientation, setWidgetPosition, showGuides, lastX, lastY);
  };
};

exports.handleOnStartDragBuild = handleOnStartDragBuild;

var handleOnStartResizeBuild = function handleOnStartResizeBuild(id, pageOrientation, setWidgetPosition, showGuides, x, y) {
  return function () {
    setWidgetPositionByAdjustment(id, pageOrientation, setWidgetPosition, showGuides, x, y);
  };
};

exports.handleOnStartResizeBuild = handleOnStartResizeBuild;

var handleOpenItemContextMenuBuild = function handleOpenItemContextMenuBuild(setContextMenuData) {
  return function (event) {
    event.preventDefault();
    setContextMenuData({
      x: event.clientX,
      y: event.clientY,
      pageX: event.pageX,
      pageY: event.pageY,
      show: true
    });
  };
};

exports.handleOpenItemContextMenuBuild = handleOpenItemContextMenuBuild;

var handleOpenPageContextMenuBuild = function handleOpenPageContextMenuBuild(_ref4) {
  var contextMenuData = _ref4.contextMenuData,
      editionLevel = _ref4.editionLevel,
      selectedWidgetId = _ref4.selectedWidgetId,
      selectWidget = _ref4.selectWidget,
      setContextMenuData = _ref4.setContextMenuData,
      wrapperRef = _ref4.wrapperRef;
  return function (event) {
    event.preventDefault();

    if (event.target === wrapperRef.current) {
      switch (editionLevel) {
        case _types.FULL_EDITION_MODE:
          if (!(0, _isNil.default)(selectedWidgetId)) {
            selectWidget(null);
          }

          setContextMenuData({
            x: event.clientX,
            y: event.clientY,
            pageX: event.pageX,
            pageY: event.pageY,
            show: true
          });
          break;

        case _types.PARTIAL_EDITION_MODE:
          if (!(0, _isNil.default)(selectedWidgetId)) {
            selectWidget(null);
          }

          if (contextMenuData.show) {
            setContextMenuData({
              x: 0,
              y: 0,
              pageX: 0,
              pageY: 0,
              show: false
            });
          }

          break;

        default:
      }
    }
  };
};

exports.handleOpenPageContextMenuBuild = handleOpenPageContextMenuBuild;

var handleOpenSettingsBuild = function handleOpenSettingsBuild(selectedWidget, settingsPanelRef) {
  return function (event) {
    settingsPanelRef.current.openSettingsPanel(selectedWidget, event);
  };
};

exports.handleOpenSettingsBuild = handleOpenSettingsBuild;

var handlePageKeyDown = function handlePageKeyDown(selectedWidgetName, moveWidgetByKey, interval) {
  if (!selectedWidgetName) {
    return null;
  }

  return function (e) {
    if ([_template.ARROW_UP_KEY, _template.ARROW_DOWN_KEY, _template.ARROW_RIGHT_KEY, _template.ARROW_LEFT_KEY].includes(e.keyCode) && interval.current === null && (selectedWidgetName !== 'text' || !e.target.className.includes('fr-element') && !e.target.className.includes('fr-view'))) {
      e.preventDefault();
      e.stopPropagation();
      interval.current = setInterval(function () {
        moveWidgetByKey(e.keyCode);
      }, _template.WIDGET_MOVE_BY_KEY_INTERVAL);
      moveWidgetByKey(e.keyCode);
    }

    if (interval.current !== null) {
      e.preventDefault();
      e.stopPropagation();
    }
  };
};

exports.handlePageKeyDown = handlePageKeyDown;

var handlePageKeyUp = function handlePageKeyUp(interval) {
  return function () {
    if (interval.current) {
      clearTimeout(interval.current);
      interval.current = null;
    }
  };
};

exports.handlePageKeyUp = handlePageKeyUp;

var handleRightClickBuild = function handleRightClickBuild(handleCloseContextMenu, handleOpenContextMenu, id, openSettingsDisabled, selectWidgetId, editionLevel) {
  return function (e) {
    e.preventDefault();
    selectWidgetId(id);

    switch (editionLevel) {
      case _types.FULL_EDITION_MODE:
        handleOpenContextMenu(e, true);
        break;

      case _types.PARTIAL_EDITION_MODE:
        if (openSettingsDisabled) {
          handleCloseContextMenu();
        } else {
          handleOpenContextMenu(e, true);
        }

        break;
    }
  };
};

exports.handleRightClickBuild = handleRightClickBuild;

var handlePasteBuild = function handlePasteBuild(contextMenuData, copiedWidget, hasSummarySupport, onChangeInPage, onPasteWidget, page, pagePosition, showGuides) {
  return function () {
    onPasteWidget(page.id, copiedWidget, showGuides, calcPositionWithLimits(contextMenuData.pageX - pagePosition.x, contextMenuData.pageY - pagePosition.y, copiedWidget.width, copiedWidget.height, page.orientation, showGuides), hasSummarySupport, onChangeInPage);
  };
};

exports.handlePasteBuild = handlePasteBuild;

var isInfiniteModeEnabled = function isInfiniteModeEnabled(widgets) {
  var tableCount = widgets.reduce(function (acc, widget) {
    if (widget.replaceInfoRequired === 'table') {
      return acc + 1;
    }

    return acc;
  }, 0);
  return tableCount === 1;
};

exports.isInfiniteModeEnabled = isInfiniteModeEnabled;

var selectedWidgetUseEffect = function selectedWidgetUseEffect(selectedWidgetName, moveWidgetByKey, interval) {
  return function () {
    var handlePageKeyUpBuild = handlePageKeyUp(interval);
    var handlePageKeyDownBuild = handlePageKeyDown(selectedWidgetName, moveWidgetByKey, interval);
    window.addEventListener('keyup', handlePageKeyUpBuild, true);
    window.addEventListener('keydown', handlePageKeyDownBuild, true);
    return function () {
      window.removeEventListener('keyup', handlePageKeyUpBuild, true);
      window.removeEventListener('keydown', handlePageKeyDownBuild, true);
    };
  };
};

exports.selectedWidgetUseEffect = selectedWidgetUseEffect;

var setWidgetPositionByAdjustment = function setWidgetPositionByAdjustment(id, pageOrientation, setWidgetPosition, showGuides, x, y) {
  if (showGuides && (x % _template.GRID_WIDTH > 0 || y % _template.GRID_HEIGHT > 0)) {
    setWidgetPosition(id, getStartGridX(x, pageOrientation), getStartGridY(y, pageOrientation));
  }
};

var sortingWidgetsByOrder = function sortingWidgetsByOrder(a, b) {
  if (a.order < b.order) {
    return -1;
  }

  if (a.order > b.order) {
    return 1;
  }

  return 0;
};

exports.sortingWidgetsByOrder = sortingWidgetsByOrder;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9oZWxwZXJzLmpzIl0sIm5hbWVzIjpbImdldFNjcm9sbFBhcmVudCIsIm5vZGUiLCJyZWdleCIsInBhcmVudHMiLCJfbm9kZSIsInBzIiwicGFyZW50Tm9kZSIsImNvbmNhdCIsInN0eWxlIiwicHJvcCIsImdldENvbXB1dGVkU3R5bGUiLCJnZXRQcm9wZXJ0eVZhbHVlIiwib3ZlcmZsb3ciLCJzY3JvbGwiLCJ0ZXN0Iiwic2Nyb2xsUGFyZW50IiwiSFRNTEVsZW1lbnQiLCJTVkdFbGVtZW50IiwiaSIsImxlbmd0aCIsImRvY3VtZW50Iiwic2Nyb2xsaW5nRWxlbWVudCIsImRvY3VtZW50RWxlbWVudCIsImdldFN0YXJ0R3JpZFgiLCJ0ZW1wWCIsInBhZ2VPcmllbnRhdGlvbiIsInJlc1giLCJHUklEX1dJRFRIIiwibmV3WCIsIlBBR0VfV0lEVEgiLCJQQUdFX0hFSUdIVCIsImdldFN0YXJ0R3JpZFkiLCJ0ZW1wWSIsInJlc1kiLCJHUklEX0hFSUdIVCIsIm5ld1kiLCJjYWxjdWxhdGVJbmZpbml0ZVBhZ2VzIiwicGFnZUlkIiwicm93RGl2cyIsInRhYmxlQ29udGVudEhlaWdodCIsIlBBRERJTkdfQk9UVE9NX1NQQUNFIiwiVEFCTEVfSEVJR0hUX0xJTUlUIiwiaW5pdFBvc2l0aW9uIiwibGFzdFBvc2l0aW9uIiwiZXh0cmFQYWdlc0RhdGEiLCJyb3dEaXZzTGVuZ3RoIiwiaW5kZXgiLCJkaXZIZWlnaHQiLCJvZmZzZXRIZWlnaHQiLCJuZXdUZW1wUG9zaXRpb24iLCJwdXNoIiwiaWQiLCJ0YWJsZVBvc2l0aW9uIiwicGFyc2VJbnQiLCJuZXh0UG9zaXRpb24iLCJnZXRTZWxlY3RlZFdpZGdldFZhbGlkYXRlZCIsInNlbGVjdGVkV2lkZ2V0SWQiLCJwYWdlV2lkZ2V0cyIsInNwbGl0Iiwid2lkZ2V0UGFnZUlkIiwiZmluZCIsIndpZGdldCIsImNhbGNQb3NpdGlvbldpdGhMaW1pdHMiLCJ4IiwieSIsIndpZHRoIiwiaGVpZ2h0Iiwic2hvd0d1aWRlcyIsInBhZ2VXaWR0aCIsInBhZ2VIZWlnaHQiLCJsaW1pdFgiLCJsaW1pdFkiLCJmaXhlZFgiLCJmaXhlZFkiLCJnZXRIYXNTdW1tYXJ5U3VwcG9ydCIsInNlbGVjdGVkV2lkZ2V0IiwiZ2V0SXNMb2NhdGlvbldpZGdldCIsImluY2x1ZGVzIiwibmFtZSIsImdldE9wZW5TZXR0aW5nc0Rpc2FibGVkIiwidGVtcGxhdGVUeXBlIiwiY2hhcnRXaWRnZXRzTmFtZXMiLCJ0YWJsZVdpZGdldHNOYW1lcyIsIk1VTFRJUFJPUE9TQUxfVFlQRSIsImhhc1NldHRpbmdzUGFuZWwiLCJnZXRQYWdlUG9zaXRpb24iLCJlbGVtZW50UmVmIiwiYm91bmRpbmdDbGllbnRSZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0Iiwic2Nyb2xsZWRQYXJlbnQiLCJzY3JvbGxMZWZ0Iiwic2Nyb2xsVG9wIiwib25saW5lV2lkZ2V0c05hbWVzIiwidHlwZSIsImhhbmRsZUNsaWNrQ29udGFpbmVyQnVpbGQiLCJjb250ZXh0TWVudURhdGEiLCJoYW5kbGVDbG9zZUNvbnRleHRNZW51Iiwic2VsZWN0V2lkZ2V0Iiwid3JhcHBlclJlZiIsImV2ZW50Iiwic2hvdyIsInRhcmdldCIsImN1cnJlbnQiLCJoYW5kbGVDbG9zZUNvbnRleHRNZW51QnVpbGQiLCJzZXRDb250ZXh0TWVudURhdGEiLCJwcmV2ZW50RGVmYXVsdCIsInBhZ2VYIiwicGFnZVkiLCJoYW5kbGVDb3B5V2lkZ2V0QnVpbGQiLCJzZXRDb3BpZWRXaWRnZXQiLCJmaWVsZCIsIm51bSIsInJlcGxhY2VJbmZvUmVxdWlyZWQiLCJ2YWx1ZSIsImhhbmRsZURyb3BCdWlsZCIsImRyb3BwaW5nSXRlbSIsIm9uQWRkV2lkZ2V0Iiwic2V0RHJvcHBpbmdJdGVtIiwicGxhY2Vob2xkZXJQb3NpdGlvblgiLCJwbGFjZWhvbGRlclBvc2l0aW9uWSIsImhhbmRsZU9uU3RhcnREcmFnQnVpbGQiLCJzZXRXaWRnZXRQb3NpdGlvbiIsIl8iLCJsYXN0WCIsImxhc3RZIiwic2V0V2lkZ2V0UG9zaXRpb25CeUFkanVzdG1lbnQiLCJoYW5kbGVPblN0YXJ0UmVzaXplQnVpbGQiLCJoYW5kbGVPcGVuSXRlbUNvbnRleHRNZW51QnVpbGQiLCJjbGllbnRYIiwiY2xpZW50WSIsImhhbmRsZU9wZW5QYWdlQ29udGV4dE1lbnVCdWlsZCIsImVkaXRpb25MZXZlbCIsIkZVTExfRURJVElPTl9NT0RFIiwiUEFSVElBTF9FRElUSU9OX01PREUiLCJoYW5kbGVPcGVuU2V0dGluZ3NCdWlsZCIsInNldHRpbmdzUGFuZWxSZWYiLCJvcGVuU2V0dGluZ3NQYW5lbCIsImhhbmRsZVBhZ2VLZXlEb3duIiwic2VsZWN0ZWRXaWRnZXROYW1lIiwibW92ZVdpZGdldEJ5S2V5IiwiaW50ZXJ2YWwiLCJlIiwiQVJST1dfVVBfS0VZIiwiQVJST1dfRE9XTl9LRVkiLCJBUlJPV19SSUdIVF9LRVkiLCJBUlJPV19MRUZUX0tFWSIsImtleUNvZGUiLCJjbGFzc05hbWUiLCJzdG9wUHJvcGFnYXRpb24iLCJzZXRJbnRlcnZhbCIsIldJREdFVF9NT1ZFX0JZX0tFWV9JTlRFUlZBTCIsImhhbmRsZVBhZ2VLZXlVcCIsImNsZWFyVGltZW91dCIsImhhbmRsZVJpZ2h0Q2xpY2tCdWlsZCIsImhhbmRsZU9wZW5Db250ZXh0TWVudSIsIm9wZW5TZXR0aW5nc0Rpc2FibGVkIiwic2VsZWN0V2lkZ2V0SWQiLCJoYW5kbGVQYXN0ZUJ1aWxkIiwiY29waWVkV2lkZ2V0IiwiaGFzU3VtbWFyeVN1cHBvcnQiLCJvbkNoYW5nZUluUGFnZSIsIm9uUGFzdGVXaWRnZXQiLCJwYWdlIiwicGFnZVBvc2l0aW9uIiwib3JpZW50YXRpb24iLCJpc0luZmluaXRlTW9kZUVuYWJsZWQiLCJ3aWRnZXRzIiwidGFibGVDb3VudCIsInJlZHVjZSIsImFjYyIsInNlbGVjdGVkV2lkZ2V0VXNlRWZmZWN0IiwiaGFuZGxlUGFnZUtleVVwQnVpbGQiLCJoYW5kbGVQYWdlS2V5RG93bkJ1aWxkIiwid2luZG93IiwiYWRkRXZlbnRMaXN0ZW5lciIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJzb3J0aW5nV2lkZ2V0c0J5T3JkZXIiLCJhIiwiYiIsIm9yZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBRUE7O0FBV0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQU1BLElBQU1BLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ0MsSUFBRCxFQUFVO0FBQzlCLE1BQU1DLEtBQUssR0FBRyxlQUFkOztBQUNBLE1BQU1DLE9BQU8sR0FBRyxTQUFWQSxPQUFVLENBQUNDLEtBQUQsRUFBUUMsRUFBUixFQUFlO0FBQzNCLFFBQUlELEtBQUssQ0FBQ0UsVUFBTixLQUFxQixJQUF6QixFQUErQjtBQUMzQixhQUFPRCxFQUFQO0FBQ0g7O0FBQ0QsV0FBT0YsT0FBTyxDQUFDQyxLQUFLLENBQUNFLFVBQVAsRUFBbUJELEVBQUUsQ0FBQ0UsTUFBSCxDQUFVLENBQUNILEtBQUQsQ0FBVixDQUFuQixDQUFkO0FBQ0gsR0FMRDs7QUFPQSxNQUFNSSxLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFDSixLQUFELEVBQVFLLElBQVI7QUFBQSxXQUNWQyxnQkFBZ0IsQ0FBQ04sS0FBRCxFQUFRLElBQVIsQ0FBaEIsQ0FBOEJPLGdCQUE5QixDQUErQ0YsSUFBL0MsQ0FEVTtBQUFBLEdBQWQ7O0FBRUEsTUFBTUcsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ1IsS0FBRDtBQUFBLFdBQ2JJLEtBQUssQ0FBQ0osS0FBRCxFQUFRLFVBQVIsQ0FBTCxHQUNBSSxLQUFLLENBQUNKLEtBQUQsRUFBUSxZQUFSLENBREwsR0FFQUksS0FBSyxDQUFDSixLQUFELEVBQVEsWUFBUixDQUhRO0FBQUEsR0FBakI7O0FBSUEsTUFBTVMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBQ1QsS0FBRDtBQUFBLFdBQVdGLEtBQUssQ0FBQ1ksSUFBTixDQUFXRixRQUFRLENBQUNSLEtBQUQsQ0FBbkIsQ0FBWDtBQUFBLEdBQWY7QUFFQTs7O0FBQ0EsTUFBTVcsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ1gsS0FBRCxFQUFXO0FBQzVCLFFBQUksRUFBRUEsS0FBSyxZQUFZWSxXQUFqQixJQUFnQ1osS0FBSyxZQUFZYSxVQUFuRCxDQUFKLEVBQW9FO0FBQ2hFO0FBQ0g7O0FBRUQsUUFBTVosRUFBRSxHQUFHRixPQUFPLENBQUNDLEtBQUssQ0FBQ0UsVUFBUCxFQUFtQixFQUFuQixDQUFsQjs7QUFFQSxTQUFLLElBQUlZLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdiLEVBQUUsQ0FBQ2MsTUFBdkIsRUFBK0JELENBQUMsSUFBSSxDQUFwQyxFQUF1QztBQUNuQyxVQUFJTCxNQUFNLENBQUNSLEVBQUUsQ0FBQ2EsQ0FBRCxDQUFILENBQVYsRUFBbUI7QUFDZixlQUFPYixFQUFFLENBQUNhLENBQUQsQ0FBVDtBQUNIO0FBQ0o7O0FBRUQsV0FBT0UsUUFBUSxDQUFDQyxnQkFBVCxJQUE2QkQsUUFBUSxDQUFDRSxlQUE3QztBQUNILEdBZEQ7O0FBZ0JBLFNBQU9QLFlBQVksQ0FBQ2QsSUFBRCxDQUFuQjtBQUNBO0FBQ0gsQ0FwQ0Q7O0FBc0NBLElBQU1zQixhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNDLEtBQUQsRUFBUUMsZUFBUixFQUE0QjtBQUM5QyxNQUFNQyxJQUFJLEdBQUdGLEtBQUssR0FBR0csb0JBQXJCO0FBQ0EsTUFBTUMsSUFBSSxHQUNORixJQUFJLElBQUksQ0FBUixJQUFhQSxJQUFJLEdBQUdDLHVCQUFhLENBQWpDLEdBQ01ILEtBQUssR0FBR0UsSUFEZCxHQUVNRixLQUFLLElBQUlHLHVCQUFhRCxJQUFqQixDQUhmOztBQUlBLE1BQUlFLElBQUksR0FBRyxDQUFYLEVBQWM7QUFDVixXQUFPLENBQVA7QUFDSDs7QUFDRCxNQUFJQSxJQUFJLElBQUlILGVBQWUsS0FBSyxVQUFwQixHQUFpQ0ksb0JBQWpDLEdBQThDQyxxQkFBbEQsQ0FBUixFQUF3RTtBQUNwRSxXQUFPTixLQUFLLEdBQUdFLElBQWY7QUFDSDs7QUFDRCxTQUFPRSxJQUFQO0FBQ0gsQ0FiRDs7QUFlQSxJQUFNRyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNDLEtBQUQsRUFBUVAsZUFBUixFQUE0QjtBQUM5QyxNQUFNUSxJQUFJLEdBQUdELEtBQUssR0FBR0UscUJBQXJCO0FBQ0EsTUFBTUMsSUFBSSxHQUNORixJQUFJLElBQUksQ0FBUixJQUFhQSxJQUFJLEdBQUdDLHdCQUFjLENBQWxDLEdBQ01GLEtBQUssR0FBR0MsSUFEZCxHQUVNRCxLQUFLLElBQUlFLHdCQUFjRCxJQUFsQixDQUhmOztBQUlBLE1BQUlFLElBQUksR0FBRyxDQUFYLEVBQWM7QUFDVixXQUFPLENBQVA7QUFDSDs7QUFDRCxNQUFJQSxJQUFJLElBQUlWLGVBQWUsS0FBSyxVQUFwQixHQUFpQ0sscUJBQWpDLEdBQStDRCxvQkFBbkQsQ0FBUixFQUF3RTtBQUNwRSxXQUFPRyxLQUFLLEdBQUdDLElBQWY7QUFDSDs7QUFDRCxTQUFPRSxJQUFQO0FBQ0gsQ0FiRDs7QUFlTyxJQUFNQyxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLE9BSWhDO0FBQUEsTUFIRkMsTUFHRSxRQUhGQSxNQUdFO0FBQUEsTUFGRkMsT0FFRSxRQUZGQSxPQUVFO0FBQUEsTUFERkMsa0JBQ0UsUUFERkEsa0JBQ0U7QUFDRixNQUFNQyxvQkFBb0IsR0FBRyxDQUE3QjtBQUNBLE1BQU1DLGtCQUFrQixHQUFHRixrQkFBa0IsR0FBR0Msb0JBQWhEO0FBQ0EsTUFBSUUsWUFBWSxHQUFHLENBQW5CO0FBQ0EsTUFBSUMsWUFBWSxHQUFHLENBQW5CO0FBQ0EsTUFBSUMsY0FBYyxHQUFHLEVBQXJCO0FBQ0EsTUFBSUMsYUFBYSxHQUFHUCxPQUFPLENBQUNuQixNQUE1Qjs7QUFFQSxPQUFLLElBQUkyQixLQUFLLEdBQUcsQ0FBakIsRUFBb0JBLEtBQUssR0FBR0QsYUFBNUIsRUFBMkNDLEtBQUssRUFBaEQsRUFBb0Q7QUFDaEQsUUFBTUMsU0FBUyxHQUFHVCxPQUFPLENBQUNRLEtBQUQsQ0FBUCxDQUFlRSxZQUFqQztBQUNBLFFBQU1DLGVBQWUsR0FBR04sWUFBWSxHQUFHSSxTQUF2Qzs7QUFDQSxRQUNJRSxlQUFlLEdBQUdQLFlBQWxCLEdBQWlDRCxrQkFBakMsSUFDQUssS0FBSyxHQUFHLENBQVIsS0FBY0QsYUFGbEIsRUFHRTtBQUNFRCxNQUFBQSxjQUFjLENBQUNNLElBQWYsQ0FBb0I7QUFDaEJDLFFBQUFBLEVBQUUsWUFBS2QsTUFBTCxvQkFBcUJPLGNBQWMsQ0FBQ3pCLE1BQWYsR0FBd0IsQ0FBN0MsQ0FEYztBQUVoQmlDLFFBQUFBLGFBQWEsRUFBRUMsUUFBUSxDQUFDWCxZQUFELENBRlA7QUFHaEJZLFFBQUFBLFlBQVksRUFBRVg7QUFIRSxPQUFwQjtBQUtBRCxNQUFBQSxZQUFZLEdBQUdDLFlBQWY7QUFDSDs7QUFDREEsSUFBQUEsWUFBWSxHQUFHTSxlQUFmO0FBQ0g7O0FBRUQsTUFBSUwsY0FBYyxDQUFDekIsTUFBZixLQUEwQixDQUE5QixFQUFpQztBQUM3QixXQUFPLEVBQVA7QUFDSDs7QUFDRCxTQUFPeUIsY0FBUDtBQUNILENBakNNOzs7O0FBbUNBLElBQU1XLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsQ0FDdENDLGdCQURzQyxFQUV0Q25CLE1BRnNDLEVBR3RDb0IsV0FIc0MsRUFJckM7QUFDRCxNQUFJLG9CQUFNRCxnQkFBTixDQUFKLEVBQTZCO0FBQ3pCLFdBQU8sSUFBUDtBQUNIOztBQUNELDhCQUF1QkEsZ0JBQWdCLENBQUNFLEtBQWpCLENBQXVCLEdBQXZCLENBQXZCO0FBQUE7QUFBQSxNQUFPQyxZQUFQOztBQUNBLE1BQUlBLFlBQVksS0FBS3RCLE1BQXJCLEVBQTZCO0FBQ3pCLFdBQU8sSUFBUDtBQUNIOztBQUNELFNBQU9vQixXQUFXLENBQUNHLElBQVosQ0FBaUIsVUFBQ0MsTUFBRDtBQUFBLFdBQVlBLE1BQU0sQ0FBQ1YsRUFBUCxLQUFjSyxnQkFBMUI7QUFBQSxHQUFqQixDQUFQO0FBQ0gsQ0FiTTs7OztBQWVBLElBQU1NLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FDbENDLENBRGtDLEVBRWxDQyxDQUZrQyxFQUdsQ0MsS0FIa0MsRUFJbENDLE1BSmtDLEVBS2xDekMsZUFMa0MsRUFNbEMwQyxVQU5rQyxFQU9qQztBQUNELE1BQU1DLFNBQVMsR0FBRzNDLGVBQWUsS0FBSyxVQUFwQixHQUFpQ0ksb0JBQWpDLEdBQThDQyxxQkFBaEU7QUFDQSxNQUFNdUMsVUFBVSxHQUNaNUMsZUFBZSxLQUFLLFVBQXBCLEdBQWlDSyxxQkFBakMsR0FBK0NELG9CQURuRDtBQUVBLE1BQU15QyxNQUFNLEdBQUdGLFNBQVMsR0FBR0gsS0FBM0I7QUFDQSxNQUFNTSxNQUFNLEdBQUdGLFVBQVUsR0FBR0gsTUFBNUI7QUFDQSxNQUFNdEMsSUFBSSxHQUFHbUMsQ0FBQyxHQUFHTyxNQUFKLEdBQWFBLE1BQWIsR0FBc0JQLENBQW5DO0FBQ0EsTUFBTTVCLElBQUksR0FBRzZCLENBQUMsR0FBR08sTUFBSixHQUFhQSxNQUFiLEdBQXNCUCxDQUFuQzs7QUFDQSxNQUFJRyxVQUFKLEVBQWdCO0FBQ1osUUFBTUssTUFBTSxHQUNSNUMsSUFBSSxJQUFJRCxvQkFBUixHQUFxQjBCLFFBQVEsQ0FBQ3pCLElBQUksR0FBR0Qsb0JBQVIsQ0FBUixHQUE4QkEsb0JBQW5ELEdBQWdFLENBRHBFO0FBRUEsUUFBTThDLE1BQU0sR0FDUnRDLElBQUksSUFBSUQscUJBQVIsR0FDTW1CLFFBQVEsQ0FBQ2xCLElBQUksR0FBR0QscUJBQVIsQ0FBUixHQUErQkEscUJBRHJDLEdBRU0sQ0FIVjtBQUtBLFdBQU87QUFBRTZCLE1BQUFBLENBQUMsRUFBRVMsTUFBTDtBQUFhUixNQUFBQSxDQUFDLEVBQUVTO0FBQWhCLEtBQVA7QUFDSDs7QUFDRCxTQUFPO0FBQ0hWLElBQUFBLENBQUMsRUFBRW5DLElBREE7QUFFSG9DLElBQUFBLENBQUMsRUFBRTdCO0FBRkEsR0FBUDtBQUlILENBN0JNOzs7O0FBK0JBLElBQU11QyxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNDLGNBQUQ7QUFBQSxTQUNoQyxDQUFDLG9CQUFNQSxjQUFOLENBQUQsSUFBMEIsa0JBQUlBLGNBQUosRUFBb0IsbUJBQXBCLEVBQXlDLEtBQXpDLENBRE07QUFBQSxDQUE3Qjs7OztBQUdBLElBQU1DLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsQ0FBQ0QsY0FBRDtBQUFBLFNBQy9CLENBQUMsb0JBQU1BLGNBQU4sQ0FBRCxJQUNBLENBQUMsS0FBRCxFQUFRLGNBQVIsRUFBd0JFLFFBQXhCLENBQWlDRixjQUFjLENBQUNHLElBQWhELENBRitCO0FBQUEsQ0FBNUI7Ozs7QUFJQSxJQUFNQyx1QkFBdUIsR0FBRyxTQUExQkEsdUJBQTBCLENBQ25DSixjQURtQyxFQUVuQ0ssWUFGbUMsRUFHbkNDLGlCQUhtQyxFQUluQ0MsaUJBSm1DLEVBS2xDO0FBQ0QsTUFDSUYsWUFBWSxLQUFLRyx5QkFBakIsSUFDQVQsb0JBQW9CLENBQUNDLGNBQUQsQ0FEcEIsSUFFQUMsbUJBQW1CLENBQUNELGNBQUQsQ0FIdkIsRUFJRTtBQUNFLFdBQU8sS0FBUDtBQUNIOztBQUNELE1BQUksQ0FBQyxvQkFBTUEsY0FBTixDQUFMLEVBQTRCO0FBQ3hCLFdBQU8sQ0FBQ1MsZ0JBQWdCLDhCQUNoQkgsaUJBRGdCLHNCQUNNQyxpQkFETixJQUVwQlAsY0FBYyxDQUFDRyxJQUZLLENBQXhCO0FBSUg7O0FBQ0QsU0FBTyxJQUFQO0FBQ0gsQ0FwQk07Ozs7QUFzQkEsSUFBTU8sZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxVQUFELEVBQWdCO0FBQzNDLE1BQUksQ0FBQ0EsVUFBTCxFQUFpQjtBQUNiLFdBQU8sSUFBUDtBQUNIOztBQUNELE1BQU1DLGtCQUFrQixHQUFHRCxVQUFVLENBQUNFLHFCQUFYLEVBQTNCO0FBQ0EsTUFBTUMsY0FBYyxHQUFHekYsZUFBZSxDQUFDc0YsVUFBRCxDQUF0QztBQUNBLFNBQU87QUFDSHZCLElBQUFBLENBQUMsRUFBRTBCLGNBQWMsQ0FBQ0MsVUFBZixHQUE0Qkgsa0JBQWtCLENBQUN4QixDQUQvQztBQUVIQyxJQUFBQSxDQUFDLEVBQUV5QixjQUFjLENBQUNFLFNBQWYsR0FBMkJKLGtCQUFrQixDQUFDdkI7QUFGOUMsR0FBUDtBQUlILENBVk07Ozs7QUFZQSxJQUFNb0IsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFDUSxrQkFBRCxFQUFxQkMsSUFBckIsRUFBOEI7QUFDMUQsTUFDSSw2QkFDT0Qsa0JBRFAsSUFFSSxVQUZKLEVBR0ksaUJBSEosRUFJSSxPQUpKLEVBS0ksV0FMSixFQU1JLGFBTkosRUFPSSxjQVBKLEVBUUksV0FSSixHQVNFZixRQVRGLENBU1dnQixJQVRYLENBREosRUFXRTtBQUNFLFdBQU8sSUFBUDtBQUNIOztBQUNELFNBQU8sS0FBUDtBQUNILENBaEJNO0FBa0JQOzs7OztBQUNPLElBQU1DLHlCQUF5QixHQUNsQyxTQURTQSx5QkFDVCxDQUNJQyxlQURKLEVBRUlDLHNCQUZKLEVBR0l4QyxnQkFISixFQUlJeUMsWUFKSixFQUtJQyxVQUxKO0FBQUEsU0FPQSxVQUFDQyxLQUFELEVBQVc7QUFDUCxRQUFJSixlQUFlLENBQUNLLElBQXBCLEVBQTBCO0FBQ3RCSixNQUFBQSxzQkFBc0I7QUFDekI7O0FBQ0QsUUFBSUcsS0FBSyxDQUFDRSxNQUFOLEtBQWlCSCxVQUFVLENBQUNJLE9BQTVCLElBQXVDLENBQUMsb0JBQU05QyxnQkFBTixDQUE1QyxFQUFxRTtBQUNqRXlDLE1BQUFBLFlBQVksQ0FBQyxJQUFELENBQVo7QUFDSDtBQUNKLEdBZEQ7QUFBQSxDQURHOzs7O0FBaUJBLElBQU1NLDJCQUEyQixHQUFHLFNBQTlCQSwyQkFBOEIsQ0FBQ0Msa0JBQUQ7QUFBQSxTQUF3QixVQUFDTCxLQUFELEVBQVc7QUFDMUUsUUFBSUEsS0FBSixFQUFXQSxLQUFLLENBQUNNLGNBQU47QUFDWEQsSUFBQUEsa0JBQWtCLENBQUM7QUFDZnpDLE1BQUFBLENBQUMsRUFBRSxDQURZO0FBRWZDLE1BQUFBLENBQUMsRUFBRSxDQUZZO0FBR2YwQyxNQUFBQSxLQUFLLEVBQUUsQ0FIUTtBQUlmQyxNQUFBQSxLQUFLLEVBQUUsQ0FKUTtBQUtmUCxNQUFBQSxJQUFJLEVBQUU7QUFMUyxLQUFELENBQWxCO0FBT0gsR0FUMEM7QUFBQSxDQUFwQzs7OztBQVdBLElBQU1RLHFCQUFxQixHQUM5QixTQURTQSxxQkFDVCxDQUFDQyxlQUFEO0FBQUEsU0FDQSxpQkFXTTtBQUFBLFFBVkZDLEtBVUUsU0FWRkEsS0FVRTtBQUFBLFFBVEY1QyxNQVNFLFNBVEZBLE1BU0U7QUFBQSxRQVJGWSxJQVFFLFNBUkZBLElBUUU7QUFBQSxRQVBGaUMsR0FPRSxTQVBGQSxHQU9FO0FBQUEsUUFORkMsbUJBTUUsU0FORkEsbUJBTUU7QUFBQSxRQUxGeEcsS0FLRSxTQUxGQSxLQUtFO0FBQUEsUUFKRnlHLEtBSUUsU0FKRkEsS0FJRTtBQUFBLFFBSEZoRCxLQUdFLFNBSEZBLEtBR0U7QUFBQSxRQUZGRixDQUVFLFNBRkZBLENBRUU7QUFBQSxRQURGQyxDQUNFLFNBREZBLENBQ0U7QUFDRjZDLElBQUFBLGVBQWUsQ0FBQztBQUNaQyxNQUFBQSxLQUFLLEVBQUxBLEtBRFk7QUFFWjVDLE1BQUFBLE1BQU0sRUFBTkEsTUFGWTtBQUdaWSxNQUFBQSxJQUFJLEVBQUpBLElBSFk7QUFJWmlDLE1BQUFBLEdBQUcsRUFBSEEsR0FKWTtBQUtaQyxNQUFBQSxtQkFBbUIsRUFBbkJBLG1CQUxZO0FBTVp4RyxNQUFBQSxLQUFLLEVBQUxBLEtBTlk7QUFPWnlHLE1BQUFBLEtBQUssRUFBTEEsS0FQWTtBQVFaaEQsTUFBQUEsS0FBSyxFQUFMQSxLQVJZO0FBU1pGLE1BQUFBLENBQUMsRUFBREEsQ0FUWTtBQVVaQyxNQUFBQSxDQUFDLEVBQURBO0FBVlksS0FBRCxDQUFmO0FBWUgsR0F6QkQ7QUFBQSxDQURHOzs7O0FBNEJBLElBQU1rRCxlQUFlLEdBQ3hCLFNBRFNBLGVBQ1QsQ0FBQ0MsWUFBRCxFQUFlQyxXQUFmLEVBQTRCQyxlQUE1QjtBQUFBLFNBQ0EsVUFBQ2xCLEtBQUQsRUFBUW1CLG9CQUFSLEVBQThCQyxvQkFBOUIsRUFBdUQ7QUFDbkRwQixJQUFBQSxLQUFLLENBQUNNLGNBQU47QUFDQVksSUFBQUEsZUFBZSxDQUFDLElBQUQsQ0FBZjtBQUNBRCxJQUFBQSxXQUFXLENBQUNELFlBQVksQ0FBQ2hFLEVBQWQsRUFBa0I7QUFDekJZLE1BQUFBLENBQUMsRUFBRXVELG9CQURzQjtBQUV6QnRELE1BQUFBLENBQUMsRUFBRXVEO0FBRnNCLEtBQWxCLENBQVg7QUFJSCxHQVJEO0FBQUEsQ0FERzs7OztBQVdBLElBQU1DLHNCQUFzQixHQUMvQixTQURTQSxzQkFDVCxDQUFDckUsRUFBRCxFQUFLMUIsZUFBTCxFQUFzQmdHLGlCQUF0QixFQUF5Q3RELFVBQXpDO0FBQUEsU0FDQSxVQUFDdUQsQ0FBRCxTQUF5QjtBQUFBLFFBQW5CQyxLQUFtQixTQUFuQkEsS0FBbUI7QUFBQSxRQUFaQyxLQUFZLFNBQVpBLEtBQVk7QUFDckJDLElBQUFBLDZCQUE2QixDQUN6QjFFLEVBRHlCLEVBRXpCMUIsZUFGeUIsRUFHekJnRyxpQkFIeUIsRUFJekJ0RCxVQUp5QixFQUt6QndELEtBTHlCLEVBTXpCQyxLQU55QixDQUE3QjtBQVFILEdBVkQ7QUFBQSxDQURHOzs7O0FBYUEsSUFBTUUsd0JBQXdCLEdBQ2pDLFNBRFNBLHdCQUNULENBQUMzRSxFQUFELEVBQUsxQixlQUFMLEVBQXNCZ0csaUJBQXRCLEVBQXlDdEQsVUFBekMsRUFBcURKLENBQXJELEVBQXdEQyxDQUF4RDtBQUFBLFNBQThELFlBQU07QUFDaEU2RCxJQUFBQSw2QkFBNkIsQ0FDekIxRSxFQUR5QixFQUV6QjFCLGVBRnlCLEVBR3pCZ0csaUJBSHlCLEVBSXpCdEQsVUFKeUIsRUFLekJKLENBTHlCLEVBTXpCQyxDQU55QixDQUE3QjtBQVFILEdBVEQ7QUFBQSxDQURHOzs7O0FBWUEsSUFBTStELDhCQUE4QixHQUN2QyxTQURTQSw4QkFDVCxDQUFDdkIsa0JBQUQ7QUFBQSxTQUF3QixVQUFDTCxLQUFELEVBQVc7QUFDL0JBLElBQUFBLEtBQUssQ0FBQ00sY0FBTjtBQUNBRCxJQUFBQSxrQkFBa0IsQ0FBQztBQUNmekMsTUFBQUEsQ0FBQyxFQUFFb0MsS0FBSyxDQUFDNkIsT0FETTtBQUVmaEUsTUFBQUEsQ0FBQyxFQUFFbUMsS0FBSyxDQUFDOEIsT0FGTTtBQUdmdkIsTUFBQUEsS0FBSyxFQUFFUCxLQUFLLENBQUNPLEtBSEU7QUFJZkMsTUFBQUEsS0FBSyxFQUFFUixLQUFLLENBQUNRLEtBSkU7QUFLZlAsTUFBQUEsSUFBSSxFQUFFO0FBTFMsS0FBRCxDQUFsQjtBQU9ILEdBVEQ7QUFBQSxDQURHOzs7O0FBWUEsSUFBTThCLDhCQUE4QixHQUN2QyxTQURTQSw4QkFDVDtBQUFBLE1BQ0luQyxlQURKLFNBQ0lBLGVBREo7QUFBQSxNQUVJb0MsWUFGSixTQUVJQSxZQUZKO0FBQUEsTUFHSTNFLGdCQUhKLFNBR0lBLGdCQUhKO0FBQUEsTUFJSXlDLFlBSkosU0FJSUEsWUFKSjtBQUFBLE1BS0lPLGtCQUxKLFNBS0lBLGtCQUxKO0FBQUEsTUFNSU4sVUFOSixTQU1JQSxVQU5KO0FBQUEsU0FRQSxVQUFDQyxLQUFELEVBQVc7QUFDUEEsSUFBQUEsS0FBSyxDQUFDTSxjQUFOOztBQUNBLFFBQUlOLEtBQUssQ0FBQ0UsTUFBTixLQUFpQkgsVUFBVSxDQUFDSSxPQUFoQyxFQUF5QztBQUNyQyxjQUFRNkIsWUFBUjtBQUNJLGFBQUtDLHdCQUFMO0FBQ0ksY0FBSSxDQUFDLG9CQUFNNUUsZ0JBQU4sQ0FBTCxFQUE4QjtBQUMxQnlDLFlBQUFBLFlBQVksQ0FBQyxJQUFELENBQVo7QUFDSDs7QUFDRE8sVUFBQUEsa0JBQWtCLENBQUM7QUFDZnpDLFlBQUFBLENBQUMsRUFBRW9DLEtBQUssQ0FBQzZCLE9BRE07QUFFZmhFLFlBQUFBLENBQUMsRUFBRW1DLEtBQUssQ0FBQzhCLE9BRk07QUFHZnZCLFlBQUFBLEtBQUssRUFBRVAsS0FBSyxDQUFDTyxLQUhFO0FBSWZDLFlBQUFBLEtBQUssRUFBRVIsS0FBSyxDQUFDUSxLQUpFO0FBS2ZQLFlBQUFBLElBQUksRUFBRTtBQUxTLFdBQUQsQ0FBbEI7QUFPQTs7QUFDSixhQUFLaUMsMkJBQUw7QUFDSSxjQUFJLENBQUMsb0JBQU03RSxnQkFBTixDQUFMLEVBQThCO0FBQzFCeUMsWUFBQUEsWUFBWSxDQUFDLElBQUQsQ0FBWjtBQUNIOztBQUNELGNBQUlGLGVBQWUsQ0FBQ0ssSUFBcEIsRUFBMEI7QUFDdEJJLFlBQUFBLGtCQUFrQixDQUFDO0FBQ2Z6QyxjQUFBQSxDQUFDLEVBQUUsQ0FEWTtBQUVmQyxjQUFBQSxDQUFDLEVBQUUsQ0FGWTtBQUdmMEMsY0FBQUEsS0FBSyxFQUFFLENBSFE7QUFJZkMsY0FBQUEsS0FBSyxFQUFFLENBSlE7QUFLZlAsY0FBQUEsSUFBSSxFQUFFO0FBTFMsYUFBRCxDQUFsQjtBQU9IOztBQUNEOztBQUNKO0FBM0JKO0FBNkJIO0FBQ0osR0F6Q0Q7QUFBQSxDQURHOzs7O0FBNENBLElBQU1rQyx1QkFBdUIsR0FDaEMsU0FEU0EsdUJBQ1QsQ0FBQzNELGNBQUQsRUFBaUI0RCxnQkFBakI7QUFBQSxTQUFzQyxVQUFDcEMsS0FBRCxFQUFXO0FBQzdDb0MsSUFBQUEsZ0JBQWdCLENBQUNqQyxPQUFqQixDQUF5QmtDLGlCQUF6QixDQUEyQzdELGNBQTNDLEVBQTJEd0IsS0FBM0Q7QUFDSCxHQUZEO0FBQUEsQ0FERzs7OztBQUtBLElBQU1zQyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQzdCQyxrQkFENkIsRUFFN0JDLGVBRjZCLEVBRzdCQyxRQUg2QixFQUk1QjtBQUNELE1BQUksQ0FBQ0Ysa0JBQUwsRUFBeUI7QUFDckIsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsU0FBTyxVQUFDRyxDQUFELEVBQU87QUFDVixRQUNJLENBQ0lDLHNCQURKLEVBRUlDLHdCQUZKLEVBR0lDLHlCQUhKLEVBSUlDLHdCQUpKLEVBS0VwRSxRQUxGLENBS1dnRSxDQUFDLENBQUNLLE9BTGIsS0FNQU4sUUFBUSxDQUFDdEMsT0FBVCxLQUFxQixJQU5yQixLQU9Db0Msa0JBQWtCLEtBQUssTUFBdkIsSUFDSSxDQUFDRyxDQUFDLENBQUN4QyxNQUFGLENBQVM4QyxTQUFULENBQW1CdEUsUUFBbkIsQ0FBNEIsWUFBNUIsQ0FBRCxJQUNHLENBQUNnRSxDQUFDLENBQUN4QyxNQUFGLENBQVM4QyxTQUFULENBQW1CdEUsUUFBbkIsQ0FBNEIsU0FBNUIsQ0FUVCxDQURKLEVBV0U7QUFDRWdFLE1BQUFBLENBQUMsQ0FBQ3BDLGNBQUY7QUFDQW9DLE1BQUFBLENBQUMsQ0FBQ08sZUFBRjtBQUNBUixNQUFBQSxRQUFRLENBQUN0QyxPQUFULEdBQW1CK0MsV0FBVyxDQUFDLFlBQU07QUFDakNWLFFBQUFBLGVBQWUsQ0FBQ0UsQ0FBQyxDQUFDSyxPQUFILENBQWY7QUFDSCxPQUY2QixFQUUzQkkscUNBRjJCLENBQTlCO0FBR0FYLE1BQUFBLGVBQWUsQ0FBQ0UsQ0FBQyxDQUFDSyxPQUFILENBQWY7QUFDSDs7QUFDRCxRQUFJTixRQUFRLENBQUN0QyxPQUFULEtBQXFCLElBQXpCLEVBQStCO0FBQzNCdUMsTUFBQUEsQ0FBQyxDQUFDcEMsY0FBRjtBQUNBb0MsTUFBQUEsQ0FBQyxDQUFDTyxlQUFGO0FBQ0g7QUFDSixHQXhCRDtBQXlCSCxDQWpDTTs7OztBQW1DQSxJQUFNRyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNYLFFBQUQ7QUFBQSxTQUFjLFlBQU07QUFDL0MsUUFBSUEsUUFBUSxDQUFDdEMsT0FBYixFQUFzQjtBQUNsQmtELE1BQUFBLFlBQVksQ0FBQ1osUUFBUSxDQUFDdEMsT0FBVixDQUFaO0FBQ0FzQyxNQUFBQSxRQUFRLENBQUN0QyxPQUFULEdBQW1CLElBQW5CO0FBQ0g7QUFDSixHQUw4QjtBQUFBLENBQXhCOzs7O0FBT0EsSUFBTW1ELHFCQUFxQixHQUM5QixTQURTQSxxQkFDVCxDQUNJekQsc0JBREosRUFFSTBELHFCQUZKLEVBR0l2RyxFQUhKLEVBSUl3RyxvQkFKSixFQUtJQyxjQUxKLEVBTUl6QixZQU5KO0FBQUEsU0FRQSxVQUFDVSxDQUFELEVBQU87QUFDSEEsSUFBQUEsQ0FBQyxDQUFDcEMsY0FBRjtBQUNBbUQsSUFBQUEsY0FBYyxDQUFDekcsRUFBRCxDQUFkOztBQUVBLFlBQVFnRixZQUFSO0FBQ0ksV0FBS0Msd0JBQUw7QUFDSXNCLFFBQUFBLHFCQUFxQixDQUFDYixDQUFELEVBQUksSUFBSixDQUFyQjtBQUNBOztBQUNKLFdBQUtSLDJCQUFMO0FBQ0ksWUFBSXNCLG9CQUFKLEVBQTBCO0FBQ3RCM0QsVUFBQUEsc0JBQXNCO0FBQ3pCLFNBRkQsTUFFTztBQUNIMEQsVUFBQUEscUJBQXFCLENBQUNiLENBQUQsRUFBSSxJQUFKLENBQXJCO0FBQ0g7O0FBQ0Q7QUFWUjtBQVlILEdBeEJEO0FBQUEsQ0FERzs7OztBQTJCQSxJQUFNZ0IsZ0JBQWdCLEdBQ3pCLFNBRFNBLGdCQUNULENBQ0k5RCxlQURKLEVBRUkrRCxZQUZKLEVBR0lDLGlCQUhKLEVBSUlDLGNBSkosRUFLSUMsYUFMSixFQU1JQyxJQU5KLEVBT0lDLFlBUEosRUFRSWhHLFVBUko7QUFBQSxTQVVBLFlBQU07QUFDRjhGLElBQUFBLGFBQWEsQ0FDVEMsSUFBSSxDQUFDL0csRUFESSxFQUVUMkcsWUFGUyxFQUdUM0YsVUFIUyxFQUlUTCxzQkFBc0IsQ0FDbEJpQyxlQUFlLENBQUNXLEtBQWhCLEdBQXdCeUQsWUFBWSxDQUFDcEcsQ0FEbkIsRUFFbEJnQyxlQUFlLENBQUNZLEtBQWhCLEdBQXdCd0QsWUFBWSxDQUFDbkcsQ0FGbkIsRUFHbEI4RixZQUFZLENBQUM3RixLQUhLLEVBSWxCNkYsWUFBWSxDQUFDNUYsTUFKSyxFQUtsQmdHLElBQUksQ0FBQ0UsV0FMYSxFQU1sQmpHLFVBTmtCLENBSmIsRUFZVDRGLGlCQVpTLEVBYVRDLGNBYlMsQ0FBYjtBQWVILEdBMUJEO0FBQUEsQ0FERzs7OztBQTZCQSxJQUFNSyxxQkFBcUIsR0FBRyxTQUF4QkEscUJBQXdCLENBQUNDLE9BQUQsRUFBYTtBQUM5QyxNQUFNQyxVQUFVLEdBQUdELE9BQU8sQ0FBQ0UsTUFBUixDQUFlLFVBQUNDLEdBQUQsRUFBTTVHLE1BQU4sRUFBaUI7QUFDL0MsUUFBSUEsTUFBTSxDQUFDbUQsbUJBQVAsS0FBK0IsT0FBbkMsRUFBNEM7QUFDeEMsYUFBT3lELEdBQUcsR0FBRyxDQUFiO0FBQ0g7O0FBQ0QsV0FBT0EsR0FBUDtBQUNILEdBTGtCLEVBS2hCLENBTGdCLENBQW5CO0FBTUEsU0FBT0YsVUFBVSxLQUFLLENBQXRCO0FBQ0gsQ0FSTTs7OztBQVVBLElBQU1HLHVCQUF1QixHQUFHLFNBQTFCQSx1QkFBMEIsQ0FDbkNoQyxrQkFEbUMsRUFFbkNDLGVBRm1DLEVBR25DQyxRQUhtQyxFQUlsQztBQUNELFNBQU8sWUFBTTtBQUNULFFBQU0rQixvQkFBb0IsR0FBR3BCLGVBQWUsQ0FBQ1gsUUFBRCxDQUE1QztBQUNBLFFBQU1nQyxzQkFBc0IsR0FBR25DLGlCQUFpQixDQUM1Q0Msa0JBRDRDLEVBRTVDQyxlQUY0QyxFQUc1Q0MsUUFINEMsQ0FBaEQ7QUFLQWlDLElBQUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsT0FBeEIsRUFBaUNILG9CQUFqQyxFQUF1RCxJQUF2RDtBQUNBRSxJQUFBQSxNQUFNLENBQUNDLGdCQUFQLENBQXdCLFNBQXhCLEVBQW1DRixzQkFBbkMsRUFBMkQsSUFBM0Q7QUFDQSxXQUFPLFlBQU07QUFDVEMsTUFBQUEsTUFBTSxDQUFDRSxtQkFBUCxDQUEyQixPQUEzQixFQUFvQ0osb0JBQXBDLEVBQTBELElBQTFEO0FBQ0FFLE1BQUFBLE1BQU0sQ0FBQ0UsbUJBQVAsQ0FBMkIsU0FBM0IsRUFBc0NILHNCQUF0QyxFQUE4RCxJQUE5RDtBQUNILEtBSEQ7QUFJSCxHQWJEO0FBY0gsQ0FuQk07Ozs7QUFxQlAsSUFBTS9DLDZCQUE2QixHQUFHLFNBQWhDQSw2QkFBZ0MsQ0FDbEMxRSxFQURrQyxFQUVsQzFCLGVBRmtDLEVBR2xDZ0csaUJBSGtDLEVBSWxDdEQsVUFKa0MsRUFLbENKLENBTGtDLEVBTWxDQyxDQU5rQyxFQU9qQztBQUNELE1BQUlHLFVBQVUsS0FBS0osQ0FBQyxHQUFHcEMsb0JBQUosR0FBaUIsQ0FBakIsSUFBc0JxQyxDQUFDLEdBQUc5QixxQkFBSixHQUFrQixDQUE3QyxDQUFkLEVBQStEO0FBQzNEdUYsSUFBQUEsaUJBQWlCLENBQ2J0RSxFQURhLEVBRWI1QixhQUFhLENBQUN3QyxDQUFELEVBQUl0QyxlQUFKLENBRkEsRUFHYk0sYUFBYSxDQUFDaUMsQ0FBRCxFQUFJdkMsZUFBSixDQUhBLENBQWpCO0FBS0g7QUFDSixDQWZEOztBQWlCTyxJQUFNdUoscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUMzQyxNQUFJRCxDQUFDLENBQUNFLEtBQUYsR0FBVUQsQ0FBQyxDQUFDQyxLQUFoQixFQUF1QjtBQUNuQixXQUFPLENBQUMsQ0FBUjtBQUNIOztBQUNELE1BQUlGLENBQUMsQ0FBQ0UsS0FBRixHQUFVRCxDQUFDLENBQUNDLEtBQWhCLEVBQXVCO0FBQ25CLFdBQU8sQ0FBUDtBQUNIOztBQUNELFNBQU8sQ0FBUDtBQUNILENBUk0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xyXG5pbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBBUlJPV19VUF9LRVksXHJcbiAgICBBUlJPV19ET1dOX0tFWSxcclxuICAgIEFSUk9XX1JJR0hUX0tFWSxcclxuICAgIEFSUk9XX0xFRlRfS0VZLFxyXG4gICAgR1JJRF9XSURUSCxcclxuICAgIEdSSURfSEVJR0hULFxyXG4gICAgUEFHRV9IRUlHSFQsXHJcbiAgICBQQUdFX1dJRFRILFxyXG4gICAgV0lER0VUX01PVkVfQllfS0VZX0lOVEVSVkFMLFxyXG59IGZyb20gJ0Bjb25zdGFudHMvdGVtcGxhdGUnO1xyXG5pbXBvcnQge1xyXG4gICAgTVVMVElQUk9QT1NBTF9UWVBFLFxyXG4gICAgRlVMTF9FRElUSU9OX01PREUsXHJcbiAgICBQQVJUSUFMX0VESVRJT05fTU9ERSxcclxufSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcclxuXHJcbmNvbnN0IGdldFNjcm9sbFBhcmVudCA9IChub2RlKSA9PiB7XHJcbiAgICBjb25zdCByZWdleCA9IC8oYXV0b3xzY3JvbGwpLztcclxuICAgIGNvbnN0IHBhcmVudHMgPSAoX25vZGUsIHBzKSA9PiB7XHJcbiAgICAgICAgaWYgKF9ub2RlLnBhcmVudE5vZGUgPT09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBzO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcGFyZW50cyhfbm9kZS5wYXJlbnROb2RlLCBwcy5jb25jYXQoW19ub2RlXSkpO1xyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdCBzdHlsZSA9IChfbm9kZSwgcHJvcCkgPT5cclxuICAgICAgICBnZXRDb21wdXRlZFN0eWxlKF9ub2RlLCBudWxsKS5nZXRQcm9wZXJ0eVZhbHVlKHByb3ApO1xyXG4gICAgY29uc3Qgb3ZlcmZsb3cgPSAoX25vZGUpID0+XHJcbiAgICAgICAgc3R5bGUoX25vZGUsICdvdmVyZmxvdycpICtcclxuICAgICAgICBzdHlsZShfbm9kZSwgJ292ZXJmbG93LXknKSArXHJcbiAgICAgICAgc3R5bGUoX25vZGUsICdvdmVyZmxvdy14Jyk7XHJcbiAgICBjb25zdCBzY3JvbGwgPSAoX25vZGUpID0+IHJlZ2V4LnRlc3Qob3ZlcmZsb3coX25vZGUpKTtcclxuXHJcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBjb25zaXN0ZW50LXJldHVybiAqL1xyXG4gICAgY29uc3Qgc2Nyb2xsUGFyZW50ID0gKF9ub2RlKSA9PiB7XHJcbiAgICAgICAgaWYgKCEoX25vZGUgaW5zdGFuY2VvZiBIVE1MRWxlbWVudCB8fCBfbm9kZSBpbnN0YW5jZW9mIFNWR0VsZW1lbnQpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBzID0gcGFyZW50cyhfbm9kZS5wYXJlbnROb2RlLCBbXSk7XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHMubGVuZ3RoOyBpICs9IDEpIHtcclxuICAgICAgICAgICAgaWYgKHNjcm9sbChwc1tpXSkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwc1tpXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LnNjcm9sbGluZ0VsZW1lbnQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gc2Nyb2xsUGFyZW50KG5vZGUpO1xyXG4gICAgLyogZXNsaW50LWVuYWJsZSBjb25zaXN0ZW50LXJldHVybiAqL1xyXG59O1xyXG5cclxuY29uc3QgZ2V0U3RhcnRHcmlkWCA9ICh0ZW1wWCwgcGFnZU9yaWVudGF0aW9uKSA9PiB7XHJcbiAgICBjb25zdCByZXNYID0gdGVtcFggJSBHUklEX1dJRFRIO1xyXG4gICAgY29uc3QgbmV3WCA9XHJcbiAgICAgICAgcmVzWCA+PSAwICYmIHJlc1ggPCBHUklEX1dJRFRIIC8gMlxyXG4gICAgICAgICAgICA/IHRlbXBYIC0gcmVzWFxyXG4gICAgICAgICAgICA6IHRlbXBYICsgKEdSSURfV0lEVEggLSByZXNYKTtcclxuICAgIGlmIChuZXdYIDwgMCkge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgfVxyXG4gICAgaWYgKG5ld1ggPiAocGFnZU9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUKSkge1xyXG4gICAgICAgIHJldHVybiB0ZW1wWCAtIHJlc1g7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbmV3WDtcclxufTtcclxuXHJcbmNvbnN0IGdldFN0YXJ0R3JpZFkgPSAodGVtcFksIHBhZ2VPcmllbnRhdGlvbikgPT4ge1xyXG4gICAgY29uc3QgcmVzWSA9IHRlbXBZICUgR1JJRF9IRUlHSFQ7XHJcbiAgICBjb25zdCBuZXdZID1cclxuICAgICAgICByZXNZID49IDAgJiYgcmVzWSA8IEdSSURfSEVJR0hUIC8gMlxyXG4gICAgICAgICAgICA/IHRlbXBZIC0gcmVzWVxyXG4gICAgICAgICAgICA6IHRlbXBZICsgKEdSSURfSEVJR0hUIC0gcmVzWSk7XHJcbiAgICBpZiAobmV3WSA8IDApIHtcclxuICAgICAgICByZXR1cm4gMDtcclxuICAgIH1cclxuICAgIGlmIChuZXdZID4gKHBhZ2VPcmllbnRhdGlvbiA9PT0gJ3BvcnRyYWl0JyA/IFBBR0VfSEVJR0hUIDogUEFHRV9XSURUSCkpIHtcclxuICAgICAgICByZXR1cm4gdGVtcFkgLSByZXNZO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG5ld1k7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY2FsY3VsYXRlSW5maW5pdGVQYWdlcyA9ICh7XHJcbiAgICBwYWdlSWQsXHJcbiAgICByb3dEaXZzLFxyXG4gICAgdGFibGVDb250ZW50SGVpZ2h0LFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCBQQURESU5HX0JPVFRPTV9TUEFDRSA9IDg7XHJcbiAgICBjb25zdCBUQUJMRV9IRUlHSFRfTElNSVQgPSB0YWJsZUNvbnRlbnRIZWlnaHQgKyBQQURESU5HX0JPVFRPTV9TUEFDRTtcclxuICAgIGxldCBpbml0UG9zaXRpb24gPSAwO1xyXG4gICAgbGV0IGxhc3RQb3NpdGlvbiA9IDA7XHJcbiAgICBsZXQgZXh0cmFQYWdlc0RhdGEgPSBbXTtcclxuICAgIGxldCByb3dEaXZzTGVuZ3RoID0gcm93RGl2cy5sZW5ndGg7XHJcblxyXG4gICAgZm9yICh2YXIgaW5kZXggPSAwOyBpbmRleCA8IHJvd0RpdnNMZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBjb25zdCBkaXZIZWlnaHQgPSByb3dEaXZzW2luZGV4XS5vZmZzZXRIZWlnaHQ7XHJcbiAgICAgICAgY29uc3QgbmV3VGVtcFBvc2l0aW9uID0gbGFzdFBvc2l0aW9uICsgZGl2SGVpZ2h0O1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgbmV3VGVtcFBvc2l0aW9uIC0gaW5pdFBvc2l0aW9uID4gVEFCTEVfSEVJR0hUX0xJTUlUIHx8XHJcbiAgICAgICAgICAgIGluZGV4ICsgMSA9PT0gcm93RGl2c0xlbmd0aFxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICBleHRyYVBhZ2VzRGF0YS5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGlkOiBgJHtwYWdlSWR9LWNsb25lLSR7ZXh0cmFQYWdlc0RhdGEubGVuZ3RoICsgMX1gLFxyXG4gICAgICAgICAgICAgICAgdGFibGVQb3NpdGlvbjogcGFyc2VJbnQoaW5pdFBvc2l0aW9uKSxcclxuICAgICAgICAgICAgICAgIG5leHRQb3NpdGlvbjogbGFzdFBvc2l0aW9uLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaW5pdFBvc2l0aW9uID0gbGFzdFBvc2l0aW9uO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsYXN0UG9zaXRpb24gPSBuZXdUZW1wUG9zaXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGV4dHJhUGFnZXNEYXRhLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuICAgIHJldHVybiBleHRyYVBhZ2VzRGF0YTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRTZWxlY3RlZFdpZGdldFZhbGlkYXRlZCA9IChcclxuICAgIHNlbGVjdGVkV2lkZ2V0SWQsXHJcbiAgICBwYWdlSWQsXHJcbiAgICBwYWdlV2lkZ2V0c1xyXG4pID0+IHtcclxuICAgIGlmIChpc05pbChzZWxlY3RlZFdpZGdldElkKSkge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gICAgY29uc3QgW3dpZGdldFBhZ2VJZF0gPSBzZWxlY3RlZFdpZGdldElkLnNwbGl0KCcvJyk7XHJcbiAgICBpZiAod2lkZ2V0UGFnZUlkICE9PSBwYWdlSWQpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIHJldHVybiBwYWdlV2lkZ2V0cy5maW5kKCh3aWRnZXQpID0+IHdpZGdldC5pZCA9PT0gc2VsZWN0ZWRXaWRnZXRJZCk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgY2FsY1Bvc2l0aW9uV2l0aExpbWl0cyA9IChcclxuICAgIHgsXHJcbiAgICB5LFxyXG4gICAgd2lkdGgsXHJcbiAgICBoZWlnaHQsXHJcbiAgICBwYWdlT3JpZW50YXRpb24sXHJcbiAgICBzaG93R3VpZGVzXHJcbikgPT4ge1xyXG4gICAgY29uc3QgcGFnZVdpZHRoID0gcGFnZU9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUO1xyXG4gICAgY29uc3QgcGFnZUhlaWdodCA9XHJcbiAgICAgICAgcGFnZU9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIO1xyXG4gICAgY29uc3QgbGltaXRYID0gcGFnZVdpZHRoIC0gd2lkdGg7XHJcbiAgICBjb25zdCBsaW1pdFkgPSBwYWdlSGVpZ2h0IC0gaGVpZ2h0O1xyXG4gICAgY29uc3QgbmV3WCA9IHggPiBsaW1pdFggPyBsaW1pdFggOiB4O1xyXG4gICAgY29uc3QgbmV3WSA9IHkgPiBsaW1pdFkgPyBsaW1pdFkgOiB5O1xyXG4gICAgaWYgKHNob3dHdWlkZXMpIHtcclxuICAgICAgICBjb25zdCBmaXhlZFggPVxyXG4gICAgICAgICAgICBuZXdYID49IEdSSURfV0lEVEggPyBwYXJzZUludChuZXdYIC8gR1JJRF9XSURUSCkgKiBHUklEX1dJRFRIIDogMDtcclxuICAgICAgICBjb25zdCBmaXhlZFkgPVxyXG4gICAgICAgICAgICBuZXdZID49IEdSSURfSEVJR0hUXHJcbiAgICAgICAgICAgICAgICA/IHBhcnNlSW50KG5ld1kgLyBHUklEX0hFSUdIVCkgKiBHUklEX0hFSUdIVFxyXG4gICAgICAgICAgICAgICAgOiAwO1xyXG5cclxuICAgICAgICByZXR1cm4geyB4OiBmaXhlZFgsIHk6IGZpeGVkWSB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB4OiBuZXdYLFxyXG4gICAgICAgIHk6IG5ld1ksXHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEhhc1N1bW1hcnlTdXBwb3J0ID0gKHNlbGVjdGVkV2lkZ2V0KSA9PlxyXG4gICAgIWlzTmlsKHNlbGVjdGVkV2lkZ2V0KSAmJiBnZXQoc2VsZWN0ZWRXaWRnZXQsICdoYXNTdW1tYXJ5U3VwcG9ydCcsIGZhbHNlKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRJc0xvY2F0aW9uV2lkZ2V0ID0gKHNlbGVjdGVkV2lkZ2V0KSA9PlxyXG4gICAgIWlzTmlsKHNlbGVjdGVkV2lkZ2V0KSAmJlxyXG4gICAgWydtYXAnLCAnc2VnbWVudHMtbWFwJ10uaW5jbHVkZXMoc2VsZWN0ZWRXaWRnZXQubmFtZSk7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0T3BlblNldHRpbmdzRGlzYWJsZWQgPSAoXHJcbiAgICBzZWxlY3RlZFdpZGdldCxcclxuICAgIHRlbXBsYXRlVHlwZSxcclxuICAgIGNoYXJ0V2lkZ2V0c05hbWVzLFxyXG4gICAgdGFibGVXaWRnZXRzTmFtZXNcclxuKSA9PiB7XHJcbiAgICBpZiAoXHJcbiAgICAgICAgdGVtcGxhdGVUeXBlID09PSBNVUxUSVBST1BPU0FMX1RZUEUgJiZcclxuICAgICAgICBnZXRIYXNTdW1tYXJ5U3VwcG9ydChzZWxlY3RlZFdpZGdldCkgJiZcclxuICAgICAgICBnZXRJc0xvY2F0aW9uV2lkZ2V0KHNlbGVjdGVkV2lkZ2V0KVxyXG4gICAgKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKCFpc05pbChzZWxlY3RlZFdpZGdldCkpIHtcclxuICAgICAgICByZXR1cm4gIWhhc1NldHRpbmdzUGFuZWwoXHJcbiAgICAgICAgICAgIFsuLi5jaGFydFdpZGdldHNOYW1lcywgLi4udGFibGVXaWRnZXRzTmFtZXNdLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFdpZGdldC5uYW1lXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFBhZ2VQb3NpdGlvbiA9IChlbGVtZW50UmVmKSA9PiB7XHJcbiAgICBpZiAoIWVsZW1lbnRSZWYpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIGNvbnN0IGJvdW5kaW5nQ2xpZW50UmVjdCA9IGVsZW1lbnRSZWYuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICBjb25zdCBzY3JvbGxlZFBhcmVudCA9IGdldFNjcm9sbFBhcmVudChlbGVtZW50UmVmKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgeDogc2Nyb2xsZWRQYXJlbnQuc2Nyb2xsTGVmdCArIGJvdW5kaW5nQ2xpZW50UmVjdC54LFxyXG4gICAgICAgIHk6IHNjcm9sbGVkUGFyZW50LnNjcm9sbFRvcCArIGJvdW5kaW5nQ2xpZW50UmVjdC55LFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYXNTZXR0aW5nc1BhbmVsID0gKG9ubGluZVdpZGdldHNOYW1lcywgdHlwZSkgPT4ge1xyXG4gICAgaWYgKFxyXG4gICAgICAgIFtcclxuICAgICAgICAgICAgLi4ub25saW5lV2lkZ2V0c05hbWVzLFxyXG4gICAgICAgICAgICAnY2xpcC1hcnQnLFxyXG4gICAgICAgICAgICAnZXF1aXBtZW50LXRhYmxlJyxcclxuICAgICAgICAgICAgJ2ltYWdlJyxcclxuICAgICAgICAgICAgJ2t3aC10YWJsZScsXHJcbiAgICAgICAgICAgICdtb25leS10YWJsZScsXHJcbiAgICAgICAgICAgICdzZWdtZW50cy1tYXAnLFxyXG4gICAgICAgICAgICAnc2VwYXJhdG9yJyxcclxuICAgICAgICBdLmluY2x1ZGVzKHR5cGUpXHJcbiAgICApIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxufTtcclxuXHJcbi8qIEhhbmRsZXJzICovXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVDbGlja0NvbnRhaW5lckJ1aWxkID1cclxuICAgIChcclxuICAgICAgICBjb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgaGFuZGxlQ2xvc2VDb250ZXh0TWVudSxcclxuICAgICAgICBzZWxlY3RlZFdpZGdldElkLFxyXG4gICAgICAgIHNlbGVjdFdpZGdldCxcclxuICAgICAgICB3cmFwcGVyUmVmXHJcbiAgICApID0+XHJcbiAgICAoZXZlbnQpID0+IHtcclxuICAgICAgICBpZiAoY29udGV4dE1lbnVEYXRhLnNob3cpIHtcclxuICAgICAgICAgICAgaGFuZGxlQ2xvc2VDb250ZXh0TWVudSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQudGFyZ2V0ID09PSB3cmFwcGVyUmVmLmN1cnJlbnQgJiYgIWlzTmlsKHNlbGVjdGVkV2lkZ2V0SWQpKSB7XHJcbiAgICAgICAgICAgIHNlbGVjdFdpZGdldChudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhhbmRsZUNsb3NlQ29udGV4dE1lbnVCdWlsZCA9IChzZXRDb250ZXh0TWVudURhdGEpID0+IChldmVudCkgPT4ge1xyXG4gICAgaWYgKGV2ZW50KSBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgc2V0Q29udGV4dE1lbnVEYXRhKHtcclxuICAgICAgICB4OiAwLFxyXG4gICAgICAgIHk6IDAsXHJcbiAgICAgICAgcGFnZVg6IDAsXHJcbiAgICAgICAgcGFnZVk6IDAsXHJcbiAgICAgICAgc2hvdzogZmFsc2UsXHJcbiAgICB9KTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVDb3B5V2lkZ2V0QnVpbGQgPVxyXG4gICAgKHNldENvcGllZFdpZGdldCkgPT5cclxuICAgICh7XHJcbiAgICAgICAgZmllbGQsXHJcbiAgICAgICAgaGVpZ2h0LFxyXG4gICAgICAgIG5hbWUsXHJcbiAgICAgICAgbnVtLFxyXG4gICAgICAgIHJlcGxhY2VJbmZvUmVxdWlyZWQsXHJcbiAgICAgICAgc3R5bGUsXHJcbiAgICAgICAgdmFsdWUsXHJcbiAgICAgICAgd2lkdGgsXHJcbiAgICAgICAgeCxcclxuICAgICAgICB5LFxyXG4gICAgfSkgPT4ge1xyXG4gICAgICAgIHNldENvcGllZFdpZGdldCh7XHJcbiAgICAgICAgICAgIGZpZWxkLFxyXG4gICAgICAgICAgICBoZWlnaHQsXHJcbiAgICAgICAgICAgIG5hbWUsXHJcbiAgICAgICAgICAgIG51bSxcclxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcclxuICAgICAgICAgICAgc3R5bGUsXHJcbiAgICAgICAgICAgIHZhbHVlLFxyXG4gICAgICAgICAgICB3aWR0aCxcclxuICAgICAgICAgICAgeCxcclxuICAgICAgICAgICAgeSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG5leHBvcnQgY29uc3QgaGFuZGxlRHJvcEJ1aWxkID1cclxuICAgIChkcm9wcGluZ0l0ZW0sIG9uQWRkV2lkZ2V0LCBzZXREcm9wcGluZ0l0ZW0pID0+XHJcbiAgICAoZXZlbnQsIHBsYWNlaG9sZGVyUG9zaXRpb25YLCBwbGFjZWhvbGRlclBvc2l0aW9uWSkgPT4ge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgc2V0RHJvcHBpbmdJdGVtKG51bGwpO1xyXG4gICAgICAgIG9uQWRkV2lkZ2V0KGRyb3BwaW5nSXRlbS5pZCwge1xyXG4gICAgICAgICAgICB4OiBwbGFjZWhvbGRlclBvc2l0aW9uWCxcclxuICAgICAgICAgICAgeTogcGxhY2Vob2xkZXJQb3NpdGlvblksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhhbmRsZU9uU3RhcnREcmFnQnVpbGQgPVxyXG4gICAgKGlkLCBwYWdlT3JpZW50YXRpb24sIHNldFdpZGdldFBvc2l0aW9uLCBzaG93R3VpZGVzKSA9PlxyXG4gICAgKF8sIHsgbGFzdFgsIGxhc3RZIH0pID0+IHtcclxuICAgICAgICBzZXRXaWRnZXRQb3NpdGlvbkJ5QWRqdXN0bWVudChcclxuICAgICAgICAgICAgaWQsXHJcbiAgICAgICAgICAgIHBhZ2VPcmllbnRhdGlvbixcclxuICAgICAgICAgICAgc2V0V2lkZ2V0UG9zaXRpb24sXHJcbiAgICAgICAgICAgIHNob3dHdWlkZXMsXHJcbiAgICAgICAgICAgIGxhc3RYLFxyXG4gICAgICAgICAgICBsYXN0WVxyXG4gICAgICAgICk7XHJcbiAgICB9O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhhbmRsZU9uU3RhcnRSZXNpemVCdWlsZCA9XHJcbiAgICAoaWQsIHBhZ2VPcmllbnRhdGlvbiwgc2V0V2lkZ2V0UG9zaXRpb24sIHNob3dHdWlkZXMsIHgsIHkpID0+ICgpID0+IHtcclxuICAgICAgICBzZXRXaWRnZXRQb3NpdGlvbkJ5QWRqdXN0bWVudChcclxuICAgICAgICAgICAgaWQsXHJcbiAgICAgICAgICAgIHBhZ2VPcmllbnRhdGlvbixcclxuICAgICAgICAgICAgc2V0V2lkZ2V0UG9zaXRpb24sXHJcbiAgICAgICAgICAgIHNob3dHdWlkZXMsXHJcbiAgICAgICAgICAgIHgsXHJcbiAgICAgICAgICAgIHlcclxuICAgICAgICApO1xyXG4gICAgfTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVPcGVuSXRlbUNvbnRleHRNZW51QnVpbGQgPVxyXG4gICAgKHNldENvbnRleHRNZW51RGF0YSkgPT4gKGV2ZW50KSA9PiB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBzZXRDb250ZXh0TWVudURhdGEoe1xyXG4gICAgICAgICAgICB4OiBldmVudC5jbGllbnRYLFxyXG4gICAgICAgICAgICB5OiBldmVudC5jbGllbnRZLFxyXG4gICAgICAgICAgICBwYWdlWDogZXZlbnQucGFnZVgsXHJcbiAgICAgICAgICAgIHBhZ2VZOiBldmVudC5wYWdlWSxcclxuICAgICAgICAgICAgc2hvdzogdHJ1ZSxcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG5leHBvcnQgY29uc3QgaGFuZGxlT3BlblBhZ2VDb250ZXh0TWVudUJ1aWxkID1cclxuICAgICh7XHJcbiAgICAgICAgY29udGV4dE1lbnVEYXRhLFxyXG4gICAgICAgIGVkaXRpb25MZXZlbCxcclxuICAgICAgICBzZWxlY3RlZFdpZGdldElkLFxyXG4gICAgICAgIHNlbGVjdFdpZGdldCxcclxuICAgICAgICBzZXRDb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgd3JhcHBlclJlZixcclxuICAgIH0pID0+XHJcbiAgICAoZXZlbnQpID0+IHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGlmIChldmVudC50YXJnZXQgPT09IHdyYXBwZXJSZWYuY3VycmVudCkge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGVkaXRpb25MZXZlbCkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGVUxMX0VESVRJT05fTU9ERTpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWlzTmlsKHNlbGVjdGVkV2lkZ2V0SWQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdFdpZGdldChudWxsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0Q29udGV4dE1lbnVEYXRhKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgeDogZXZlbnQuY2xpZW50WCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgeTogZXZlbnQuY2xpZW50WSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZVg6IGV2ZW50LnBhZ2VYLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlWTogZXZlbnQucGFnZVksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3c6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIFBBUlRJQUxfRURJVElPTl9NT0RFOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghaXNOaWwoc2VsZWN0ZWRXaWRnZXRJZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0V2lkZ2V0KG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29udGV4dE1lbnVEYXRhLnNob3cpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0Q29udGV4dE1lbnVEYXRhKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB5OiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZVg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWdlWTogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3c6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVPcGVuU2V0dGluZ3NCdWlsZCA9XHJcbiAgICAoc2VsZWN0ZWRXaWRnZXQsIHNldHRpbmdzUGFuZWxSZWYpID0+IChldmVudCkgPT4ge1xyXG4gICAgICAgIHNldHRpbmdzUGFuZWxSZWYuY3VycmVudC5vcGVuU2V0dGluZ3NQYW5lbChzZWxlY3RlZFdpZGdldCwgZXZlbnQpO1xyXG4gICAgfTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVQYWdlS2V5RG93biA9IChcclxuICAgIHNlbGVjdGVkV2lkZ2V0TmFtZSxcclxuICAgIG1vdmVXaWRnZXRCeUtleSxcclxuICAgIGludGVydmFsXHJcbikgPT4ge1xyXG4gICAgaWYgKCFzZWxlY3RlZFdpZGdldE5hbWUpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICAgIHJldHVybiAoZSkgPT4ge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgW1xyXG4gICAgICAgICAgICAgICAgQVJST1dfVVBfS0VZLFxyXG4gICAgICAgICAgICAgICAgQVJST1dfRE9XTl9LRVksXHJcbiAgICAgICAgICAgICAgICBBUlJPV19SSUdIVF9LRVksXHJcbiAgICAgICAgICAgICAgICBBUlJPV19MRUZUX0tFWSxcclxuICAgICAgICAgICAgXS5pbmNsdWRlcyhlLmtleUNvZGUpICYmXHJcbiAgICAgICAgICAgIGludGVydmFsLmN1cnJlbnQgPT09IG51bGwgJiZcclxuICAgICAgICAgICAgKHNlbGVjdGVkV2lkZ2V0TmFtZSAhPT0gJ3RleHQnIHx8XHJcbiAgICAgICAgICAgICAgICAoIWUudGFyZ2V0LmNsYXNzTmFtZS5pbmNsdWRlcygnZnItZWxlbWVudCcpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgIWUudGFyZ2V0LmNsYXNzTmFtZS5pbmNsdWRlcygnZnItdmlldycpKSlcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBpbnRlcnZhbC5jdXJyZW50ID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbW92ZVdpZGdldEJ5S2V5KGUua2V5Q29kZSk7XHJcbiAgICAgICAgICAgIH0sIFdJREdFVF9NT1ZFX0JZX0tFWV9JTlRFUlZBTCk7XHJcbiAgICAgICAgICAgIG1vdmVXaWRnZXRCeUtleShlLmtleUNvZGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaW50ZXJ2YWwuY3VycmVudCAhPT0gbnVsbCkge1xyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVQYWdlS2V5VXAgPSAoaW50ZXJ2YWwpID0+ICgpID0+IHtcclxuICAgIGlmIChpbnRlcnZhbC5jdXJyZW50KSB7XHJcbiAgICAgICAgY2xlYXJUaW1lb3V0KGludGVydmFsLmN1cnJlbnQpO1xyXG4gICAgICAgIGludGVydmFsLmN1cnJlbnQgPSBudWxsO1xyXG4gICAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhhbmRsZVJpZ2h0Q2xpY2tCdWlsZCA9XHJcbiAgICAoXHJcbiAgICAgICAgaGFuZGxlQ2xvc2VDb250ZXh0TWVudSxcclxuICAgICAgICBoYW5kbGVPcGVuQ29udGV4dE1lbnUsXHJcbiAgICAgICAgaWQsXHJcbiAgICAgICAgb3BlblNldHRpbmdzRGlzYWJsZWQsXHJcbiAgICAgICAgc2VsZWN0V2lkZ2V0SWQsXHJcbiAgICAgICAgZWRpdGlvbkxldmVsXHJcbiAgICApID0+XHJcbiAgICAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBzZWxlY3RXaWRnZXRJZChpZCk7XHJcblxyXG4gICAgICAgIHN3aXRjaCAoZWRpdGlvbkxldmVsKSB7XHJcbiAgICAgICAgICAgIGNhc2UgRlVMTF9FRElUSU9OX01PREU6XHJcbiAgICAgICAgICAgICAgICBoYW5kbGVPcGVuQ29udGV4dE1lbnUoZSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBQQVJUSUFMX0VESVRJT05fTU9ERTpcclxuICAgICAgICAgICAgICAgIGlmIChvcGVuU2V0dGluZ3NEaXNhYmxlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsb3NlQ29udGV4dE1lbnUoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlT3BlbkNvbnRleHRNZW51KGUsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYW5kbGVQYXN0ZUJ1aWxkID1cclxuICAgIChcclxuICAgICAgICBjb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgY29waWVkV2lkZ2V0LFxyXG4gICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgIG9uQ2hhbmdlSW5QYWdlLFxyXG4gICAgICAgIG9uUGFzdGVXaWRnZXQsXHJcbiAgICAgICAgcGFnZSxcclxuICAgICAgICBwYWdlUG9zaXRpb24sXHJcbiAgICAgICAgc2hvd0d1aWRlc1xyXG4gICAgKSA9PlxyXG4gICAgKCkgPT4ge1xyXG4gICAgICAgIG9uUGFzdGVXaWRnZXQoXHJcbiAgICAgICAgICAgIHBhZ2UuaWQsXHJcbiAgICAgICAgICAgIGNvcGllZFdpZGdldCxcclxuICAgICAgICAgICAgc2hvd0d1aWRlcyxcclxuICAgICAgICAgICAgY2FsY1Bvc2l0aW9uV2l0aExpbWl0cyhcclxuICAgICAgICAgICAgICAgIGNvbnRleHRNZW51RGF0YS5wYWdlWCAtIHBhZ2VQb3NpdGlvbi54LFxyXG4gICAgICAgICAgICAgICAgY29udGV4dE1lbnVEYXRhLnBhZ2VZIC0gcGFnZVBvc2l0aW9uLnksXHJcbiAgICAgICAgICAgICAgICBjb3BpZWRXaWRnZXQud2lkdGgsXHJcbiAgICAgICAgICAgICAgICBjb3BpZWRXaWRnZXQuaGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgcGFnZS5vcmllbnRhdGlvbixcclxuICAgICAgICAgICAgICAgIHNob3dHdWlkZXNcclxuICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXHJcbiAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXHJcbiAgICAgICAgKTtcclxuICAgIH07XHJcblxyXG5leHBvcnQgY29uc3QgaXNJbmZpbml0ZU1vZGVFbmFibGVkID0gKHdpZGdldHMpID0+IHtcclxuICAgIGNvbnN0IHRhYmxlQ291bnQgPSB3aWRnZXRzLnJlZHVjZSgoYWNjLCB3aWRnZXQpID0+IHtcclxuICAgICAgICBpZiAod2lkZ2V0LnJlcGxhY2VJbmZvUmVxdWlyZWQgPT09ICd0YWJsZScpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGFjYyArIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBhY2M7XHJcbiAgICB9LCAwKTtcclxuICAgIHJldHVybiB0YWJsZUNvdW50ID09PSAxO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHNlbGVjdGVkV2lkZ2V0VXNlRWZmZWN0ID0gKFxyXG4gICAgc2VsZWN0ZWRXaWRnZXROYW1lLFxyXG4gICAgbW92ZVdpZGdldEJ5S2V5LFxyXG4gICAgaW50ZXJ2YWxcclxuKSA9PiB7XHJcbiAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGhhbmRsZVBhZ2VLZXlVcEJ1aWxkID0gaGFuZGxlUGFnZUtleVVwKGludGVydmFsKTtcclxuICAgICAgICBjb25zdCBoYW5kbGVQYWdlS2V5RG93bkJ1aWxkID0gaGFuZGxlUGFnZUtleURvd24oXHJcbiAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0TmFtZSxcclxuICAgICAgICAgICAgbW92ZVdpZGdldEJ5S2V5LFxyXG4gICAgICAgICAgICBpbnRlcnZhbFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleXVwJywgaGFuZGxlUGFnZUtleVVwQnVpbGQsIHRydWUpO1xyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdrZXlkb3duJywgaGFuZGxlUGFnZUtleURvd25CdWlsZCwgdHJ1ZSk7XHJcbiAgICAgICAgcmV0dXJuICgpID0+IHtcclxuICAgICAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2tleXVwJywgaGFuZGxlUGFnZUtleVVwQnVpbGQsIHRydWUpO1xyXG4gICAgICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGhhbmRsZVBhZ2VLZXlEb3duQnVpbGQsIHRydWUpO1xyXG4gICAgICAgIH07XHJcbiAgICB9O1xyXG59O1xyXG5cclxuY29uc3Qgc2V0V2lkZ2V0UG9zaXRpb25CeUFkanVzdG1lbnQgPSAoXHJcbiAgICBpZCxcclxuICAgIHBhZ2VPcmllbnRhdGlvbixcclxuICAgIHNldFdpZGdldFBvc2l0aW9uLFxyXG4gICAgc2hvd0d1aWRlcyxcclxuICAgIHgsXHJcbiAgICB5XHJcbikgPT4ge1xyXG4gICAgaWYgKHNob3dHdWlkZXMgJiYgKHggJSBHUklEX1dJRFRIID4gMCB8fCB5ICUgR1JJRF9IRUlHSFQgPiAwKSkge1xyXG4gICAgICAgIHNldFdpZGdldFBvc2l0aW9uKFxyXG4gICAgICAgICAgICBpZCxcclxuICAgICAgICAgICAgZ2V0U3RhcnRHcmlkWCh4LCBwYWdlT3JpZW50YXRpb24pLFxyXG4gICAgICAgICAgICBnZXRTdGFydEdyaWRZKHksIHBhZ2VPcmllbnRhdGlvbilcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IHNvcnRpbmdXaWRnZXRzQnlPcmRlciA9IChhLCBiKSA9PiB7XHJcbiAgICBpZiAoYS5vcmRlciA8IGIub3JkZXIpIHtcclxuICAgICAgICByZXR1cm4gLTE7XHJcbiAgICB9XHJcbiAgICBpZiAoYS5vcmRlciA+IGIub3JkZXIpIHtcclxuICAgICAgICByZXR1cm4gMTtcclxuICAgIH1cclxuICAgIHJldHVybiAwO1xyXG59O1xyXG4iXX0=