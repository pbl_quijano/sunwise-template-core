"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(newState) {
  return function (dispatch) {
    dispatch({
      type: _actionTypes.INITIALIZE_STATE_POOL,
      payload: newState
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL2luaXRpYWxpemVTdGF0ZVBvb2wuanMiXSwibmFtZXMiOlsibmV3U3RhdGUiLCJkaXNwYXRjaCIsInR5cGUiLCJJTklUSUFMSVpFX1NUQVRFX1BPT0wiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O2VBRWUsa0JBQUNBLFFBQUQ7QUFBQSxTQUFjLFVBQUNDLFFBQUQsRUFBYztBQUN2Q0EsSUFBQUEsUUFBUSxDQUFDO0FBQUVDLE1BQUFBLElBQUksRUFBRUMsa0NBQVI7QUFBK0JDLE1BQUFBLE9BQU8sRUFBRUo7QUFBeEMsS0FBRCxDQUFSO0FBQ0gsR0FGYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJTklUSUFMSVpFX1NUQVRFX1BPT0wgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0IChuZXdTdGF0ZSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gICAgZGlzcGF0Y2goeyB0eXBlOiBJTklUSUFMSVpFX1NUQVRFX1BPT0wsIHBheWxvYWQ6IG5ld1N0YXRlIH0pO1xufTtcbiJdfQ==