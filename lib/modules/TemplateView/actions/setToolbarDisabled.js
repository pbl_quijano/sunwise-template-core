"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(isToolbarDisabled) {
  return function (dispatch) {
    return dispatch({
      type: _actionTypes.SET_TOOLBAR_DISABLED,
      payload: isToolbarDisabled
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NldFRvb2xiYXJEaXNhYmxlZC5qcyJdLCJuYW1lcyI6WyJpc1Rvb2xiYXJEaXNhYmxlZCIsImRpc3BhdGNoIiwidHlwZSIsIlNFVF9UT09MQkFSX0RJU0FCTEVEIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztlQUVlLGtCQUFDQSxpQkFBRDtBQUFBLFNBQXVCLFVBQUNDLFFBQUQ7QUFBQSxXQUNsQ0EsUUFBUSxDQUFDO0FBQUVDLE1BQUFBLElBQUksRUFBRUMsaUNBQVI7QUFBOEJDLE1BQUFBLE9BQU8sRUFBRUo7QUFBdkMsS0FBRCxDQUQwQjtBQUFBLEdBQXZCO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9UT09MQkFSX0RJU0FCTEVEIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAoaXNUb29sYmFyRGlzYWJsZWQpID0+IChkaXNwYXRjaCkgPT5cbiAgICBkaXNwYXRjaCh7IHR5cGU6IFNFVF9UT09MQkFSX0RJU0FCTEVELCBwYXlsb2FkOiBpc1Rvb2xiYXJEaXNhYmxlZCB9KTtcbiJdfQ==