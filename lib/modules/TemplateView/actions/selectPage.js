"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

var _actionTypes = require("../actionTypes");

var _initializeStatePool = _interopRequireDefault(require("./initializeStatePool"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(pageId) {
  return function (dispatch, getState) {
    if (pageId) {
      var _templateCoreSelector = templateCoreSelectors.getEntitiesSession(getState()),
          Page = _templateCoreSelector.Page;

      var pageModel = Page.withId(pageId);
      dispatch({
        type: _actionTypes.SELECT_PAGE,
        payload: pageId
      });
      dispatch((0, _initializeStatePool.default)(JSON.stringify(_objectSpread(_objectSpread({}, pageModel.ref), {}, {
        widgets: pageModel.widgets.toRefArray()
      }))));
    } else {
      dispatch({
        type: _actionTypes.SELECT_PAGE,
        payload: null
      });
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NlbGVjdFBhZ2UuanMiXSwibmFtZXMiOlsicGFnZUlkIiwiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEVudGl0aWVzU2Vzc2lvbiIsIlBhZ2UiLCJwYWdlTW9kZWwiLCJ3aXRoSWQiLCJ0eXBlIiwiU0VMRUNUX1BBR0UiLCJwYXlsb2FkIiwiSlNPTiIsInN0cmluZ2lmeSIsInJlZiIsIndpZGdldHMiLCJ0b1JlZkFycmF5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7ZUFDZSxrQkFBQ0EsTUFBRDtBQUFBLFNBQVksVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQy9DLFFBQUlGLE1BQUosRUFBWTtBQUNSLGtDQUFpQkcscUJBQXFCLENBQUNDLGtCQUF0QixDQUF5Q0YsUUFBUSxFQUFqRCxDQUFqQjtBQUFBLFVBQVFHLElBQVIseUJBQVFBLElBQVI7O0FBQ0EsVUFBTUMsU0FBUyxHQUFHRCxJQUFJLENBQUNFLE1BQUwsQ0FBWVAsTUFBWixDQUFsQjtBQUNBQyxNQUFBQSxRQUFRLENBQUM7QUFBRU8sUUFBQUEsSUFBSSxFQUFFQyx3QkFBUjtBQUFxQkMsUUFBQUEsT0FBTyxFQUFFVjtBQUE5QixPQUFELENBQVI7QUFDQUMsTUFBQUEsUUFBUSxDQUNKLGtDQUNJVSxJQUFJLENBQUNDLFNBQUwsaUNBQ09OLFNBQVMsQ0FBQ08sR0FEakI7QUFFSUMsUUFBQUEsT0FBTyxFQUFFUixTQUFTLENBQUNRLE9BQVYsQ0FBa0JDLFVBQWxCO0FBRmIsU0FESixDQURJLENBQVI7QUFRSCxLQVpELE1BWU87QUFDSGQsTUFBQUEsUUFBUSxDQUFDO0FBQUVPLFFBQUFBLElBQUksRUFBRUMsd0JBQVI7QUFBcUJDLFFBQUFBLE9BQU8sRUFBRTtBQUE5QixPQUFELENBQVI7QUFDSDtBQUNKLEdBaEJjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XG5cbmltcG9ydCB7IFNFTEVDVF9QQUdFIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5pbXBvcnQgaW5pdGlhbGl6ZVN0YXRlUG9vbCBmcm9tICcuL2luaXRpYWxpemVTdGF0ZVBvb2wnO1xuZXhwb3J0IGRlZmF1bHQgKHBhZ2VJZCkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xuICAgIGlmIChwYWdlSWQpIHtcbiAgICAgICAgY29uc3QgeyBQYWdlIH0gPSB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0RW50aXRpZXNTZXNzaW9uKGdldFN0YXRlKCkpO1xuICAgICAgICBjb25zdCBwYWdlTW9kZWwgPSBQYWdlLndpdGhJZChwYWdlSWQpO1xuICAgICAgICBkaXNwYXRjaCh7IHR5cGU6IFNFTEVDVF9QQUdFLCBwYXlsb2FkOiBwYWdlSWQgfSk7XG4gICAgICAgIGRpc3BhdGNoKFxuICAgICAgICAgICAgaW5pdGlhbGl6ZVN0YXRlUG9vbChcbiAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIC4uLnBhZ2VNb2RlbC5yZWYsXG4gICAgICAgICAgICAgICAgICAgIHdpZGdldHM6IHBhZ2VNb2RlbC53aWRnZXRzLnRvUmVmQXJyYXkoKSxcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKVxuICAgICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGRpc3BhdGNoKHsgdHlwZTogU0VMRUNUX1BBR0UsIHBheWxvYWQ6IG51bGwgfSk7XG4gICAgfVxufTtcbiJdfQ==