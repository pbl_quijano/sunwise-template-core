"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(copyData) {
  return function (dispatch) {
    return dispatch({
      type: _actionTypes.SET_COPIED_ITEM,
      payload: copyData
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NldENvcGllZEl0ZW0uanMiXSwibmFtZXMiOlsiY29weURhdGEiLCJkaXNwYXRjaCIsInR5cGUiLCJTRVRfQ09QSUVEX0lURU0iLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O2VBRWUsa0JBQUNBLFFBQUQ7QUFBQSxTQUFjLFVBQUNDLFFBQUQ7QUFBQSxXQUN6QkEsUUFBUSxDQUFDO0FBQUVDLE1BQUFBLElBQUksRUFBRUMsNEJBQVI7QUFBeUJDLE1BQUFBLE9BQU8sRUFBRUo7QUFBbEMsS0FBRCxDQURpQjtBQUFBLEdBQWQ7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU0VUX0NPUElFRF9JVEVNIH0gZnJvbSAnLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAoY29weURhdGEpID0+IChkaXNwYXRjaCkgPT5cbiAgICBkaXNwYXRjaCh7IHR5cGU6IFNFVF9DT1BJRURfSVRFTSwgcGF5bG9hZDogY29weURhdGEgfSk7XG4iXX0=