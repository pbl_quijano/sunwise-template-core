"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

var _actionTypes = require("../actionTypes");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default() {
  var onChangeInPage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
  var addStateDisabled = arguments.length > 1 ? arguments[1] : undefined;
  return function (dispatch, getState) {
    var state = getState();

    if (!addStateDisabled) {
      dispatch({
        type: _actionTypes.ADD_STATE_POOL,
        payload: JSON.stringify(selectors.getSelectedPage(state))
      });
    }

    var updatedData = templateCoreSelectors.getTemplateUpdatingContentData(state);
    onChangeInPage(updatedData);
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL29uVXBkYXRlUGFnZS5qcyJdLCJuYW1lcyI6WyJvbkNoYW5nZUluUGFnZSIsImFkZFN0YXRlRGlzYWJsZWQiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwic3RhdGUiLCJ0eXBlIiwiQUREX1NUQVRFX1BPT0wiLCJwYXlsb2FkIiwiSlNPTiIsInN0cmluZ2lmeSIsInNlbGVjdG9ycyIsImdldFNlbGVjdGVkUGFnZSIsInVwZGF0ZWREYXRhIiwidGVtcGxhdGVDb3JlU2VsZWN0b3JzIiwiZ2V0VGVtcGxhdGVVcGRhdGluZ0NvbnRlbnREYXRhIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFDQTs7Ozs7O2VBRWU7QUFBQSxNQUFDQSxjQUFELHVFQUFrQixZQUFNLENBQUUsQ0FBMUI7QUFBQSxNQUE0QkMsZ0JBQTVCO0FBQUEsU0FDWCxVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDcEIsUUFBTUMsS0FBSyxHQUFHRCxRQUFRLEVBQXRCOztBQUNBLFFBQUksQ0FBQ0YsZ0JBQUwsRUFBdUI7QUFDbkJDLE1BQUFBLFFBQVEsQ0FBQztBQUNMRyxRQUFBQSxJQUFJLEVBQUVDLDJCQUREO0FBRUxDLFFBQUFBLE9BQU8sRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWVDLFNBQVMsQ0FBQ0MsZUFBVixDQUEwQlAsS0FBMUIsQ0FBZjtBQUZKLE9BQUQsQ0FBUjtBQUlIOztBQUVELFFBQU1RLFdBQVcsR0FDYkMscUJBQXFCLENBQUNDLDhCQUF0QixDQUFxRFYsS0FBckQsQ0FESjtBQUdBSixJQUFBQSxjQUFjLENBQUNZLFdBQUQsQ0FBZDtBQUNILEdBZFU7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlU2VsZWN0b3JzIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvc2VsZWN0b3JzJztcblxuaW1wb3J0IHsgQUREX1NUQVRFX1BPT0wgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGRlZmF1bHQgKG9uQ2hhbmdlSW5QYWdlID0gKCkgPT4ge30sIGFkZFN0YXRlRGlzYWJsZWQpID0+XG4gICAgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xuICAgICAgICBjb25zdCBzdGF0ZSA9IGdldFN0YXRlKCk7XG4gICAgICAgIGlmICghYWRkU3RhdGVEaXNhYmxlZCkge1xuICAgICAgICAgICAgZGlzcGF0Y2goe1xuICAgICAgICAgICAgICAgIHR5cGU6IEFERF9TVEFURV9QT09MLFxuICAgICAgICAgICAgICAgIHBheWxvYWQ6IEpTT04uc3RyaW5naWZ5KHNlbGVjdG9ycy5nZXRTZWxlY3RlZFBhZ2Uoc3RhdGUpKSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgdXBkYXRlZERhdGEgPVxuICAgICAgICAgICAgdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldFRlbXBsYXRlVXBkYXRpbmdDb250ZW50RGF0YShzdGF0ZSk7XG5cbiAgICAgICAgb25DaGFuZ2VJblBhZ2UodXBkYXRlZERhdGEpO1xuICAgIH07XG4iXX0=