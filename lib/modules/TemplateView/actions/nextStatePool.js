"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _setPageData = _interopRequireDefault(require("../../TemplateCore/actions/setPageData"));

var _actionTypes = require("../actionTypes");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import * as templateCoreSelectors from '@templateCore/selectors';
var _default = function _default() {
  return function (dispatch, getState) {
    var state = getState();
    var statePoolData = selectors.getStatePoolData(state);
    var statePoolIndex = selectors.getStatePoolIndex(state);
    var newPrevIndex = statePoolIndex;

    if (statePoolIndex < statePoolData.length - 1 && statePoolData.length > 1) {
      var updateData = statePoolData[newPrevIndex + 1];
      dispatch((0, _setPageData.default)(JSON.parse(updateData)));
      dispatch({
        type: _actionTypes.NEXT_STATE_POOL,
        payload: newPrevIndex + 1
      }); // templateCoreSelectors.getTemplateUpdatingContentData(state);
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL25leHRTdGF0ZVBvb2wuanMiXSwibmFtZXMiOlsiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInN0YXRlIiwic3RhdGVQb29sRGF0YSIsInNlbGVjdG9ycyIsImdldFN0YXRlUG9vbERhdGEiLCJzdGF0ZVBvb2xJbmRleCIsImdldFN0YXRlUG9vbEluZGV4IiwibmV3UHJldkluZGV4IiwibGVuZ3RoIiwidXBkYXRlRGF0YSIsIkpTT04iLCJwYXJzZSIsInR5cGUiLCJORVhUX1NUQVRFX1BPT0wiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFHQTs7QUFDQTs7Ozs7Ozs7QUFIQTtlQUtlO0FBQUEsU0FBTSxVQUFDQSxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDekMsUUFBTUMsS0FBSyxHQUFHRCxRQUFRLEVBQXRCO0FBQ0EsUUFBTUUsYUFBYSxHQUFHQyxTQUFTLENBQUNDLGdCQUFWLENBQTJCSCxLQUEzQixDQUF0QjtBQUNBLFFBQU1JLGNBQWMsR0FBR0YsU0FBUyxDQUFDRyxpQkFBVixDQUE0QkwsS0FBNUIsQ0FBdkI7QUFDQSxRQUFJTSxZQUFZLEdBQUdGLGNBQW5COztBQUNBLFFBQUlBLGNBQWMsR0FBR0gsYUFBYSxDQUFDTSxNQUFkLEdBQXVCLENBQXhDLElBQTZDTixhQUFhLENBQUNNLE1BQWQsR0FBdUIsQ0FBeEUsRUFBMkU7QUFDdkUsVUFBTUMsVUFBVSxHQUFHUCxhQUFhLENBQUNLLFlBQVksR0FBRyxDQUFoQixDQUFoQztBQUNBUixNQUFBQSxRQUFRLENBQUMsMEJBQVlXLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixVQUFYLENBQVosQ0FBRCxDQUFSO0FBQ0FWLE1BQUFBLFFBQVEsQ0FBQztBQUNMYSxRQUFBQSxJQUFJLEVBQUVDLDRCQUREO0FBRUxDLFFBQUFBLE9BQU8sRUFBRVAsWUFBWSxHQUFHO0FBRm5CLE9BQUQsQ0FBUixDQUh1RSxDQU92RTtBQUNIO0FBQ0osR0FkYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc2V0UGFnZURhdGEgZnJvbSAnQHRlbXBsYXRlQ29yZS9hY3Rpb25zL3NldFBhZ2VEYXRhJztcbi8vIGltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XG5cbmltcG9ydCB7IE5FWFRfU1RBVEVfUE9PTCB9IGZyb20gJy4uL2FjdGlvblR5cGVzJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi9zZWxlY3RvcnMnO1xuXG5leHBvcnQgZGVmYXVsdCAoKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgY29uc3Qgc3RhdGUgPSBnZXRTdGF0ZSgpO1xuICAgIGNvbnN0IHN0YXRlUG9vbERhdGEgPSBzZWxlY3RvcnMuZ2V0U3RhdGVQb29sRGF0YShzdGF0ZSk7XG4gICAgY29uc3Qgc3RhdGVQb29sSW5kZXggPSBzZWxlY3RvcnMuZ2V0U3RhdGVQb29sSW5kZXgoc3RhdGUpO1xuICAgIGxldCBuZXdQcmV2SW5kZXggPSBzdGF0ZVBvb2xJbmRleDtcbiAgICBpZiAoc3RhdGVQb29sSW5kZXggPCBzdGF0ZVBvb2xEYXRhLmxlbmd0aCAtIDEgJiYgc3RhdGVQb29sRGF0YS5sZW5ndGggPiAxKSB7XG4gICAgICAgIGNvbnN0IHVwZGF0ZURhdGEgPSBzdGF0ZVBvb2xEYXRhW25ld1ByZXZJbmRleCArIDFdO1xuICAgICAgICBkaXNwYXRjaChzZXRQYWdlRGF0YShKU09OLnBhcnNlKHVwZGF0ZURhdGEpKSk7XG4gICAgICAgIGRpc3BhdGNoKHtcbiAgICAgICAgICAgIHR5cGU6IE5FWFRfU1RBVEVfUE9PTCxcbiAgICAgICAgICAgIHBheWxvYWQ6IG5ld1ByZXZJbmRleCArIDEsXG4gICAgICAgIH0pO1xuICAgICAgICAvLyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0VGVtcGxhdGVVcGRhdGluZ0NvbnRlbnREYXRhKHN0YXRlKTtcbiAgICB9XG59O1xuIl19