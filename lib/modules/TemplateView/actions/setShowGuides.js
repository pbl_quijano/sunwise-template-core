"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(showGuides) {
  return function (dispatch) {
    return dispatch({
      type: _actionTypes.SET_SHOW_GUIDES,
      payload: showGuides
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NldFNob3dHdWlkZXMuanMiXSwibmFtZXMiOlsic2hvd0d1aWRlcyIsImRpc3BhdGNoIiwidHlwZSIsIlNFVF9TSE9XX0dVSURFUyIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsVUFBRDtBQUFBLFNBQWdCLFVBQUNDLFFBQUQ7QUFBQSxXQUMzQkEsUUFBUSxDQUFDO0FBQUVDLE1BQUFBLElBQUksRUFBRUMsNEJBQVI7QUFBeUJDLE1BQUFBLE9BQU8sRUFBRUo7QUFBbEMsS0FBRCxDQURtQjtBQUFBLEdBQWhCO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9TSE9XX0dVSURFUyB9IGZyb20gJy4uL2FjdGlvblR5cGVzJztcblxuZXhwb3J0IGRlZmF1bHQgKHNob3dHdWlkZXMpID0+IChkaXNwYXRjaCkgPT5cbiAgICBkaXNwYXRjaCh7IHR5cGU6IFNFVF9TSE9XX0dVSURFUywgcGF5bG9hZDogc2hvd0d1aWRlcyB9KTtcbiJdfQ==