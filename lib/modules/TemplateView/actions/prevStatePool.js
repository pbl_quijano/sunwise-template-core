"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _setPageData = _interopRequireDefault(require("../../TemplateCore/actions/setPageData"));

var _actionTypes = require("../actionTypes");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import * as templateCoreSelectors from '@templateCore/selectors';
var _default = function _default() {
  return function (dispatch, getState) {
    var state = getState();
    var statePoolData = selectors.getStatePoolData(state);
    var statePoolIndex = selectors.getStatePoolIndex(state);
    var newPrevIndex = statePoolIndex;

    if (statePoolIndex > 0 && statePoolData.length > 1) {
      var updateData = statePoolData[newPrevIndex - 1];
      dispatch((0, _setPageData.default)(JSON.parse(updateData)));
      dispatch({
        type: _actionTypes.PREV_STATE_POOL,
        payload: newPrevIndex - 1
      }); // templateCoreSelectors.getTemplateUpdatingContentData(state);
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3ByZXZTdGF0ZVBvb2wuanMiXSwibmFtZXMiOlsiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInN0YXRlIiwic3RhdGVQb29sRGF0YSIsInNlbGVjdG9ycyIsImdldFN0YXRlUG9vbERhdGEiLCJzdGF0ZVBvb2xJbmRleCIsImdldFN0YXRlUG9vbEluZGV4IiwibmV3UHJldkluZGV4IiwibGVuZ3RoIiwidXBkYXRlRGF0YSIsIkpTT04iLCJwYXJzZSIsInR5cGUiLCJQUkVWX1NUQVRFX1BPT0wiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFHQTs7QUFDQTs7Ozs7Ozs7QUFIQTtlQUtlO0FBQUEsU0FBTSxVQUFDQSxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDekMsUUFBTUMsS0FBSyxHQUFHRCxRQUFRLEVBQXRCO0FBQ0EsUUFBTUUsYUFBYSxHQUFHQyxTQUFTLENBQUNDLGdCQUFWLENBQTJCSCxLQUEzQixDQUF0QjtBQUNBLFFBQU1JLGNBQWMsR0FBR0YsU0FBUyxDQUFDRyxpQkFBVixDQUE0QkwsS0FBNUIsQ0FBdkI7QUFDQSxRQUFJTSxZQUFZLEdBQUdGLGNBQW5COztBQUNBLFFBQUlBLGNBQWMsR0FBRyxDQUFqQixJQUFzQkgsYUFBYSxDQUFDTSxNQUFkLEdBQXVCLENBQWpELEVBQW9EO0FBQ2hELFVBQU1DLFVBQVUsR0FBR1AsYUFBYSxDQUFDSyxZQUFZLEdBQUcsQ0FBaEIsQ0FBaEM7QUFDQVIsTUFBQUEsUUFBUSxDQUFDLDBCQUFZVyxJQUFJLENBQUNDLEtBQUwsQ0FBV0YsVUFBWCxDQUFaLENBQUQsQ0FBUjtBQUNBVixNQUFBQSxRQUFRLENBQUM7QUFDTGEsUUFBQUEsSUFBSSxFQUFFQyw0QkFERDtBQUVMQyxRQUFBQSxPQUFPLEVBQUVQLFlBQVksR0FBRztBQUZuQixPQUFELENBQVIsQ0FIZ0QsQ0FPaEQ7QUFDSDtBQUNKLEdBZGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHNldFBhZ2VEYXRhIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvYWN0aW9ucy9zZXRQYWdlRGF0YSc7XG4vLyBpbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xuXG5pbXBvcnQgeyBQUkVWX1NUQVRFX1BPT0wgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGRlZmF1bHQgKCkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xuICAgIGNvbnN0IHN0YXRlID0gZ2V0U3RhdGUoKTtcbiAgICBjb25zdCBzdGF0ZVBvb2xEYXRhID0gc2VsZWN0b3JzLmdldFN0YXRlUG9vbERhdGEoc3RhdGUpO1xuICAgIGNvbnN0IHN0YXRlUG9vbEluZGV4ID0gc2VsZWN0b3JzLmdldFN0YXRlUG9vbEluZGV4KHN0YXRlKTtcbiAgICBsZXQgbmV3UHJldkluZGV4ID0gc3RhdGVQb29sSW5kZXg7XG4gICAgaWYgKHN0YXRlUG9vbEluZGV4ID4gMCAmJiBzdGF0ZVBvb2xEYXRhLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgY29uc3QgdXBkYXRlRGF0YSA9IHN0YXRlUG9vbERhdGFbbmV3UHJldkluZGV4IC0gMV07XG4gICAgICAgIGRpc3BhdGNoKHNldFBhZ2VEYXRhKEpTT04ucGFyc2UodXBkYXRlRGF0YSkpKTtcbiAgICAgICAgZGlzcGF0Y2goe1xuICAgICAgICAgICAgdHlwZTogUFJFVl9TVEFURV9QT09MLFxuICAgICAgICAgICAgcGF5bG9hZDogbmV3UHJldkluZGV4IC0gMSxcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIHRlbXBsYXRlQ29yZVNlbGVjdG9ycy5nZXRUZW1wbGF0ZVVwZGF0aW5nQ29udGVudERhdGEoc3RhdGUpO1xuICAgIH1cbn07XG4iXX0=