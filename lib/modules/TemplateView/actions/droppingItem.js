"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(data) {
  return function (dispatch) {
    return dispatch({
      type: _actionTypes.DROPPING_ITEM,
      payload: data
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL2Ryb3BwaW5nSXRlbS5qcyJdLCJuYW1lcyI6WyJkYXRhIiwiZGlzcGF0Y2giLCJ0eXBlIiwiRFJPUFBJTkdfSVRFTSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsSUFBRDtBQUFBLFNBQVUsVUFBQ0MsUUFBRDtBQUFBLFdBQ3JCQSxRQUFRLENBQUM7QUFBRUMsTUFBQUEsSUFBSSxFQUFFQywwQkFBUjtBQUF1QkMsTUFBQUEsT0FBTyxFQUFFSjtBQUFoQyxLQUFELENBRGE7QUFBQSxHQUFWO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERST1BQSU5HX0lURU0gfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0IChkYXRhKSA9PiAoZGlzcGF0Y2gpID0+XG4gICAgZGlzcGF0Y2goeyB0eXBlOiBEUk9QUElOR19JVEVNLCBwYXlsb2FkOiBkYXRhIH0pO1xuIl19