"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

var _selectPage = _interopRequireDefault(require("./selectPage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default() {
  return function (dispatch, getState) {
    var pages = templateCoreSelectors.getCurrentTemplatePages(getState());

    if (pages.length === 0) {
      return;
    }

    var firstElement = pages[0];

    if (firstElement.type === 'group') {
      if (firstElement.pages.length === 0) {
        return;
      }

      dispatch((0, _selectPage.default)(firstElement.pages[0].id));
    } else {
      dispatch((0, _selectPage.default)(firstElement.id));
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NlbGVjdERlZmF1bHRQYWdlLmpzIl0sIm5hbWVzIjpbImRpc3BhdGNoIiwiZ2V0U3RhdGUiLCJwYWdlcyIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzIiwibGVuZ3RoIiwiZmlyc3RFbGVtZW50IiwidHlwZSIsImlkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7Ozs7Ozs7ZUFDZTtBQUFBLFNBQU0sVUFBQ0EsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQ3pDLFFBQU1DLEtBQUssR0FBR0MscUJBQXFCLENBQUNDLHVCQUF0QixDQUE4Q0gsUUFBUSxFQUF0RCxDQUFkOztBQUNBLFFBQUlDLEtBQUssQ0FBQ0csTUFBTixLQUFpQixDQUFyQixFQUF3QjtBQUNwQjtBQUNIOztBQUNELFFBQU1DLFlBQVksR0FBR0osS0FBSyxDQUFDLENBQUQsQ0FBMUI7O0FBQ0EsUUFBSUksWUFBWSxDQUFDQyxJQUFiLEtBQXNCLE9BQTFCLEVBQW1DO0FBQy9CLFVBQUlELFlBQVksQ0FBQ0osS0FBYixDQUFtQkcsTUFBbkIsS0FBOEIsQ0FBbEMsRUFBcUM7QUFDakM7QUFDSDs7QUFDREwsTUFBQUEsUUFBUSxDQUFDLHlCQUFXTSxZQUFZLENBQUNKLEtBQWIsQ0FBbUIsQ0FBbkIsRUFBc0JNLEVBQWpDLENBQUQsQ0FBUjtBQUNILEtBTEQsTUFLTztBQUNIUixNQUFBQSxRQUFRLENBQUMseUJBQVdNLFlBQVksQ0FBQ0UsRUFBeEIsQ0FBRCxDQUFSO0FBQ0g7QUFDSixHQWRjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XG5cbmltcG9ydCBzZWxlY3RQYWdlIGZyb20gJy4vc2VsZWN0UGFnZSc7XG5leHBvcnQgZGVmYXVsdCAoKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgY29uc3QgcGFnZXMgPSB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0Q3VycmVudFRlbXBsYXRlUGFnZXMoZ2V0U3RhdGUoKSk7XG4gICAgaWYgKHBhZ2VzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGNvbnN0IGZpcnN0RWxlbWVudCA9IHBhZ2VzWzBdO1xuICAgIGlmIChmaXJzdEVsZW1lbnQudHlwZSA9PT0gJ2dyb3VwJykge1xuICAgICAgICBpZiAoZmlyc3RFbGVtZW50LnBhZ2VzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGRpc3BhdGNoKHNlbGVjdFBhZ2UoZmlyc3RFbGVtZW50LnBhZ2VzWzBdLmlkKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgZGlzcGF0Y2goc2VsZWN0UGFnZShmaXJzdEVsZW1lbnQuaWQpKTtcbiAgICB9XG59O1xuIl19