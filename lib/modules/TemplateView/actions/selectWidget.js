"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(widgetId) {
  return function (dispatch) {
    return dispatch({
      type: _actionTypes.SELECT_WIDGET,
      payload: widgetId
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NlbGVjdFdpZGdldC5qcyJdLCJuYW1lcyI6WyJ3aWRnZXRJZCIsImRpc3BhdGNoIiwidHlwZSIsIlNFTEVDVF9XSURHRVQiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O2VBRWUsa0JBQUNBLFFBQUQ7QUFBQSxTQUFjLFVBQUNDLFFBQUQ7QUFBQSxXQUN6QkEsUUFBUSxDQUFDO0FBQUVDLE1BQUFBLElBQUksRUFBRUMsMEJBQVI7QUFBdUJDLE1BQUFBLE9BQU8sRUFBRUo7QUFBaEMsS0FBRCxDQURpQjtBQUFBLEdBQWQ7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU0VMRUNUX1dJREdFVCB9IGZyb20gJy4uL2FjdGlvblR5cGVzJztcblxuZXhwb3J0IGRlZmF1bHQgKHdpZGdldElkKSA9PiAoZGlzcGF0Y2gpID0+XG4gICAgZGlzcGF0Y2goeyB0eXBlOiBTRUxFQ1RfV0lER0VULCBwYXlsb2FkOiB3aWRnZXRJZCB9KTtcbiJdfQ==