"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactWebfontLoader = _interopRequireDefault(require("@dr-kobros/react-webfont-loader"));

var _froalaEditor = _interopRequireDefault(require("froala-editor"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _froala = require("../../constants/froala");

var _types = require("../../constants/types");

var _froala2 = require("../../helpers/froala");

var mainSelectors = _interopRequireWildcard(require("../main/selectors"));

var _TagSystem = _interopRequireDefault(require("../TagSystem"));

var tagSystemActions = _interopRequireWildcard(require("../TagSystem/actions"));

var templateCoreSelectors = _interopRequireWildcard(require("../TemplateCore/selectors"));

var actions = _interopRequireWildcard(require("./actions"));

var _TemplateContent = _interopRequireDefault(require("./components/TemplateContent"));

var selectors = _interopRequireWildcard(require("./selectors"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TemplateView = function TemplateView(_ref) {
  var catalogs = _ref.catalogs,
      currentTemplateProposalNumber = _ref.currentTemplateProposalNumber,
      currentTemplateType = _ref.currentTemplateType,
      editionLevel = _ref.editionLevel,
      froalaLicenseKey = _ref.froalaLicenseKey,
      horizontalScrollMode = _ref.horizontalScrollMode,
      _ref$infinitePagesSup = _ref.infinitePagesSupportEnabled,
      infinitePagesSupportEnabled = _ref$infinitePagesSup === void 0 ? true : _ref$infinitePagesSup,
      isInitializing = _ref.isInitializing,
      language = _ref.language,
      pageDesignDisabled = _ref.pageDesignDisabled,
      prepareEditCustomTag = _ref.prepareEditCustomTag,
      prepareEditLibraryTag = _ref.prepareEditLibraryTag,
      prepareInsertLibraryTag = _ref.prepareInsertLibraryTag,
      setIsToolbarDisabled = _ref.setIsToolbarDisabled,
      showGuides = _ref.showGuides;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      currentFroalaEditorInstance = _useState2[0],
      setCurrentFroalaEditorInstance = _useState2[1];

  (0, _react.useEffect)(function () {
    var originalHelpers = _froalaEditor.default.MODULES.helpers;

    _froalaEditor.default.MODULES.helpers = function () {
      var helpers = originalHelpers.apply(this, arguments);

      helpers.screenSize = function () {
        if (window.innerWidth >= 1200) return _froalaEditor.default.LG;
        if (window.innerWidth >= 992) return _froalaEditor.default.MD;
        if (window.innerWidth >= 768) return _froalaEditor.default.SM;
        return _froalaEditor.default.XS;
      };

      return helpers;
    };

    _froalaEditor.default.DefineIcon('tagList', {
      NAME: 'tags',
      SVG_KEY: 'tags'
    });

    _froalaEditor.default.RegisterCommand('tagList', {
      title: t('Smart Tags'),
      undo: true,
      showOnMobile: true,
      callback: function callback() {
        this.selection.save();
        prepareInsertLibraryTag();
      }
    });
  }, []);

  if (isInitializing) {
    return null;
  }

  var froalaEditorConfig = (0, _froala2.getFroalaEditorConfig)({
    FroalaEditor: _froalaEditor.default,
    froalaLicenseKey: froalaLicenseKey,
    isFullEdition: editionLevel === _types.FULL_EDITION_MODE,
    language: language,
    prepareEditCustomTag: prepareEditCustomTag,
    prepareEditLibraryTag: prepareEditLibraryTag,
    setCurrentFroalaEditorInstance: setCurrentFroalaEditorInstance,
    setIsToolbarDisabled: setIsToolbarDisabled
  });
  var availableCatalogs = catalogs.filter(function (item) {
    return item.order <= currentTemplateProposalNumber;
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactWebfontLoader.default, {
    config: _froala.FONT_CONFIG,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_TemplateContent.default, {
        availableCatalogs: availableCatalogs,
        editionLevel: editionLevel,
        froalaEditorConfig: froalaEditorConfig,
        horizontalScrollMode: horizontalScrollMode,
        infinitePagesSupportEnabled: infinitePagesSupportEnabled,
        pageDesignDisabled: pageDesignDisabled,
        showGuides: showGuides,
        templateType: currentTemplateType
      }), editionLevel !== _types.NO_EDITION_MODE && /*#__PURE__*/(0, _jsxRuntime.jsx)(_TagSystem.default, {
        catalogs: availableCatalogs,
        editionLevel: editionLevel,
        froalaInstance: currentFroalaEditorInstance,
        templateType: currentTemplateType
      })]
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  currentTemplateProposalNumber: templateCoreSelectors.getCurrentTemplateProposalNumber,
  currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
  catalogs: mainSelectors.getFetchCatalogData,
  froalaLicenseKey: mainSelectors.getFroalaLicenseKey,
  googleApiKey: mainSelectors.getGoogleApiKey,
  isInitializing: mainSelectors.getIsInitializing,
  language: mainSelectors.getLanguage,
  showGuides: selectors.getShowGuides
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    prepareEditCustomTag: function prepareEditCustomTag(values) {
      return dispatch(tagSystemActions.prepareEditCustomTag(values));
    },
    prepareEditLibraryTag: function prepareEditLibraryTag(values) {
      return dispatch(tagSystemActions.prepareEditLibraryTag(values));
    },
    prepareInsertLibraryTag: function prepareInsertLibraryTag() {
      return dispatch(tagSystemActions.prepareInsertLibraryTag());
    },
    setIsToolbarDisabled: function setIsToolbarDisabled(isToolbarDisabled) {
      return dispatch(actions.setToolbarDisabled(isToolbarDisabled));
    },
    setShowGuides: function setShowGuides(showGuides) {
      return dispatch(actions.setShowGuides(showGuides));
    }
  };
};

TemplateView.propTypes = {
  catalogs: _propTypes.default.array,
  currentTemplateProposalNumber: _propTypes.default.number,
  currentTemplateType: _propTypes.default.number,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  froalaLicenseKey: _propTypes.default.string,
  horizontalScrollMode: _propTypes.default.bool,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  isInitializing: _propTypes.default.bool,
  language: _propTypes.default.string,
  pageDesignDisabled: _propTypes.default.bool,
  prepareEditCustomTag: _propTypes.default.func,
  prepareEditLibraryTag: _propTypes.default.func,
  prepareInsertLibraryTag: _propTypes.default.func,
  setIsToolbarDisabled: _propTypes.default.func,
  showGuides: _propTypes.default.bool
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TemplateView);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9pbmRleC5qcyJdLCJuYW1lcyI6WyJUZW1wbGF0ZVZpZXciLCJjYXRhbG9ncyIsImN1cnJlbnRUZW1wbGF0ZVByb3Bvc2FsTnVtYmVyIiwiY3VycmVudFRlbXBsYXRlVHlwZSIsImVkaXRpb25MZXZlbCIsImZyb2FsYUxpY2Vuc2VLZXkiLCJob3Jpem9udGFsU2Nyb2xsTW9kZSIsImluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZCIsImlzSW5pdGlhbGl6aW5nIiwibGFuZ3VhZ2UiLCJwYWdlRGVzaWduRGlzYWJsZWQiLCJwcmVwYXJlRWRpdEN1c3RvbVRhZyIsInByZXBhcmVFZGl0TGlicmFyeVRhZyIsInByZXBhcmVJbnNlcnRMaWJyYXJ5VGFnIiwic2V0SXNUb29sYmFyRGlzYWJsZWQiLCJzaG93R3VpZGVzIiwidCIsImN1cnJlbnRGcm9hbGFFZGl0b3JJbnN0YW5jZSIsInNldEN1cnJlbnRGcm9hbGFFZGl0b3JJbnN0YW5jZSIsIm9yaWdpbmFsSGVscGVycyIsIkZyb2FsYUVkaXRvciIsIk1PRFVMRVMiLCJoZWxwZXJzIiwiYXBwbHkiLCJhcmd1bWVudHMiLCJzY3JlZW5TaXplIiwid2luZG93IiwiaW5uZXJXaWR0aCIsIkxHIiwiTUQiLCJTTSIsIlhTIiwiRGVmaW5lSWNvbiIsIk5BTUUiLCJTVkdfS0VZIiwiUmVnaXN0ZXJDb21tYW5kIiwidGl0bGUiLCJ1bmRvIiwic2hvd09uTW9iaWxlIiwiY2FsbGJhY2siLCJzZWxlY3Rpb24iLCJzYXZlIiwiZnJvYWxhRWRpdG9yQ29uZmlnIiwiaXNGdWxsRWRpdGlvbiIsIkZVTExfRURJVElPTl9NT0RFIiwiYXZhaWxhYmxlQ2F0YWxvZ3MiLCJmaWx0ZXIiLCJpdGVtIiwib3JkZXIiLCJGT05UX0NPTkZJRyIsIk5PX0VESVRJT05fTU9ERSIsIm1hcFN0YXRlVG9Qcm9wcyIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVByb3Bvc2FsTnVtYmVyIiwiZ2V0Q3VycmVudFRlbXBsYXRlVHlwZSIsIm1haW5TZWxlY3RvcnMiLCJnZXRGZXRjaENhdGFsb2dEYXRhIiwiZ2V0RnJvYWxhTGljZW5zZUtleSIsImdvb2dsZUFwaUtleSIsImdldEdvb2dsZUFwaUtleSIsImdldElzSW5pdGlhbGl6aW5nIiwiZ2V0TGFuZ3VhZ2UiLCJzZWxlY3RvcnMiLCJnZXRTaG93R3VpZGVzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJ2YWx1ZXMiLCJ0YWdTeXN0ZW1BY3Rpb25zIiwiaXNUb29sYmFyRGlzYWJsZWQiLCJhY3Rpb25zIiwic2V0VG9vbGJhckRpc2FibGVkIiwic2V0U2hvd0d1aWRlcyIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5IiwibnVtYmVyIiwic3RyaW5nIiwib2JqZWN0IiwiYm9vbCIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHLFNBQWZBLFlBQWUsT0FnQmY7QUFBQSxNQWZGQyxRQWVFLFFBZkZBLFFBZUU7QUFBQSxNQWRGQyw2QkFjRSxRQWRGQSw2QkFjRTtBQUFBLE1BYkZDLG1CQWFFLFFBYkZBLG1CQWFFO0FBQUEsTUFaRkMsWUFZRSxRQVpGQSxZQVlFO0FBQUEsTUFYRkMsZ0JBV0UsUUFYRkEsZ0JBV0U7QUFBQSxNQVZGQyxvQkFVRSxRQVZGQSxvQkFVRTtBQUFBLG1DQVRGQywyQkFTRTtBQUFBLE1BVEZBLDJCQVNFLHNDQVQ0QixJQVM1QjtBQUFBLE1BUkZDLGNBUUUsUUFSRkEsY0FRRTtBQUFBLE1BUEZDLFFBT0UsUUFQRkEsUUFPRTtBQUFBLE1BTkZDLGtCQU1FLFFBTkZBLGtCQU1FO0FBQUEsTUFMRkMsb0JBS0UsUUFMRkEsb0JBS0U7QUFBQSxNQUpGQyxxQkFJRSxRQUpGQSxxQkFJRTtBQUFBLE1BSEZDLHVCQUdFLFFBSEZBLHVCQUdFO0FBQUEsTUFGRkMsb0JBRUUsUUFGRkEsb0JBRUU7QUFBQSxNQURGQyxVQUNFLFFBREZBLFVBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLGtCQUNJLHFCQUFTLElBQVQsQ0FESjtBQUFBO0FBQUEsTUFBT0MsMkJBQVA7QUFBQSxNQUFvQ0MsOEJBQXBDOztBQUdBLHdCQUFVLFlBQU07QUFDWixRQUFNQyxlQUFlLEdBQUdDLHNCQUFhQyxPQUFiLENBQXFCQyxPQUE3Qzs7QUFFQUYsMEJBQWFDLE9BQWIsQ0FBcUJDLE9BQXJCLEdBQStCLFlBQVk7QUFDdkMsVUFBTUEsT0FBTyxHQUFHSCxlQUFlLENBQUNJLEtBQWhCLENBQXNCLElBQXRCLEVBQTRCQyxTQUE1QixDQUFoQjs7QUFFQUYsTUFBQUEsT0FBTyxDQUFDRyxVQUFSLEdBQXFCLFlBQVk7QUFDN0IsWUFBSUMsTUFBTSxDQUFDQyxVQUFQLElBQXFCLElBQXpCLEVBQStCLE9BQU9QLHNCQUFhUSxFQUFwQjtBQUMvQixZQUFJRixNQUFNLENBQUNDLFVBQVAsSUFBcUIsR0FBekIsRUFBOEIsT0FBT1Asc0JBQWFTLEVBQXBCO0FBQzlCLFlBQUlILE1BQU0sQ0FBQ0MsVUFBUCxJQUFxQixHQUF6QixFQUE4QixPQUFPUCxzQkFBYVUsRUFBcEI7QUFDOUIsZUFBT1Ysc0JBQWFXLEVBQXBCO0FBQ0gsT0FMRDs7QUFPQSxhQUFPVCxPQUFQO0FBQ0gsS0FYRDs7QUFhQUYsMEJBQWFZLFVBQWIsQ0FBd0IsU0FBeEIsRUFBbUM7QUFBRUMsTUFBQUEsSUFBSSxFQUFFLE1BQVI7QUFBZ0JDLE1BQUFBLE9BQU8sRUFBRTtBQUF6QixLQUFuQzs7QUFFQWQsMEJBQWFlLGVBQWIsQ0FBNkIsU0FBN0IsRUFBd0M7QUFDcENDLE1BQUFBLEtBQUssRUFBRXBCLENBQUMsQ0FBQyxZQUFELENBRDRCO0FBRXBDcUIsTUFBQUEsSUFBSSxFQUFFLElBRjhCO0FBR3BDQyxNQUFBQSxZQUFZLEVBQUUsSUFIc0I7QUFJcENDLE1BQUFBLFFBQVEsRUFBRSxvQkFBWTtBQUNsQixhQUFLQyxTQUFMLENBQWVDLElBQWY7QUFDQTVCLFFBQUFBLHVCQUF1QjtBQUMxQjtBQVBtQyxLQUF4QztBQVNILEdBM0JELEVBMkJHLEVBM0JIOztBQTRCQSxNQUFJTCxjQUFKLEVBQW9CO0FBQ2hCLFdBQU8sSUFBUDtBQUNIOztBQUVELE1BQU1rQyxrQkFBa0IsR0FBRyxvQ0FBc0I7QUFDN0N0QixJQUFBQSxZQUFZLEVBQVpBLHFCQUQ2QztBQUU3Q2YsSUFBQUEsZ0JBQWdCLEVBQWhCQSxnQkFGNkM7QUFHN0NzQyxJQUFBQSxhQUFhLEVBQUV2QyxZQUFZLEtBQUt3Qyx3QkFIYTtBQUk3Q25DLElBQUFBLFFBQVEsRUFBUkEsUUFKNkM7QUFLN0NFLElBQUFBLG9CQUFvQixFQUFwQkEsb0JBTDZDO0FBTTdDQyxJQUFBQSxxQkFBcUIsRUFBckJBLHFCQU42QztBQU83Q00sSUFBQUEsOEJBQThCLEVBQTlCQSw4QkFQNkM7QUFRN0NKLElBQUFBLG9CQUFvQixFQUFwQkE7QUFSNkMsR0FBdEIsQ0FBM0I7QUFXQSxNQUFNK0IsaUJBQWlCLEdBQUc1QyxRQUFRLENBQUM2QyxNQUFULENBQ3RCLFVBQUNDLElBQUQ7QUFBQSxXQUFVQSxJQUFJLENBQUNDLEtBQUwsSUFBYzlDLDZCQUF4QjtBQUFBLEdBRHNCLENBQTFCO0FBR0Esc0JBQ0kscUJBQUMsMkJBQUQ7QUFBZSxJQUFBLE1BQU0sRUFBRStDLG1CQUF2QjtBQUFBLDJCQUNJO0FBQUEsOEJBQ0kscUJBQUMsd0JBQUQ7QUFDSSxRQUFBLGlCQUFpQixFQUFFSixpQkFEdkI7QUFFSSxRQUFBLFlBQVksRUFBRXpDLFlBRmxCO0FBR0ksUUFBQSxrQkFBa0IsRUFBRXNDLGtCQUh4QjtBQUlJLFFBQUEsb0JBQW9CLEVBQUVwQyxvQkFKMUI7QUFLSSxRQUFBLDJCQUEyQixFQUFFQywyQkFMakM7QUFNSSxRQUFBLGtCQUFrQixFQUFFRyxrQkFOeEI7QUFPSSxRQUFBLFVBQVUsRUFBRUssVUFQaEI7QUFRSSxRQUFBLFlBQVksRUFBRVo7QUFSbEIsUUFESixFQVdLQyxZQUFZLEtBQUs4QyxzQkFBakIsaUJBQ0cscUJBQUMsa0JBQUQ7QUFDSSxRQUFBLFFBQVEsRUFBRUwsaUJBRGQ7QUFFSSxRQUFBLFlBQVksRUFBRXpDLFlBRmxCO0FBR0ksUUFBQSxjQUFjLEVBQUVhLDJCQUhwQjtBQUlJLFFBQUEsWUFBWSxFQUFFZDtBQUpsQixRQVpSO0FBQUE7QUFESixJQURKO0FBd0JILENBM0ZEOztBQTZGQSxJQUFNZ0QsZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q2pELEVBQUFBLDZCQUE2QixFQUN6QmtELHFCQUFxQixDQUFDQyxnQ0FGbUI7QUFHN0NsRCxFQUFBQSxtQkFBbUIsRUFBRWlELHFCQUFxQixDQUFDRSxzQkFIRTtBQUk3Q3JELEVBQUFBLFFBQVEsRUFBRXNELGFBQWEsQ0FBQ0MsbUJBSnFCO0FBSzdDbkQsRUFBQUEsZ0JBQWdCLEVBQUVrRCxhQUFhLENBQUNFLG1CQUxhO0FBTTdDQyxFQUFBQSxZQUFZLEVBQUVILGFBQWEsQ0FBQ0ksZUFOaUI7QUFPN0NuRCxFQUFBQSxjQUFjLEVBQUUrQyxhQUFhLENBQUNLLGlCQVBlO0FBUTdDbkQsRUFBQUEsUUFBUSxFQUFFOEMsYUFBYSxDQUFDTSxXQVJxQjtBQVM3QzlDLEVBQUFBLFVBQVUsRUFBRStDLFNBQVMsQ0FBQ0M7QUFUdUIsQ0FBekIsQ0FBeEI7O0FBWUEsSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0Q3RELElBQUFBLG9CQUFvQixFQUFFLDhCQUFDdUQsTUFBRDtBQUFBLGFBQ2xCRCxRQUFRLENBQUNFLGdCQUFnQixDQUFDeEQsb0JBQWpCLENBQXNDdUQsTUFBdEMsQ0FBRCxDQURVO0FBQUEsS0FEZ0I7QUFHdEN0RCxJQUFBQSxxQkFBcUIsRUFBRSwrQkFBQ3NELE1BQUQ7QUFBQSxhQUNuQkQsUUFBUSxDQUFDRSxnQkFBZ0IsQ0FBQ3ZELHFCQUFqQixDQUF1Q3NELE1BQXZDLENBQUQsQ0FEVztBQUFBLEtBSGU7QUFLdENyRCxJQUFBQSx1QkFBdUIsRUFBRTtBQUFBLGFBQ3JCb0QsUUFBUSxDQUFDRSxnQkFBZ0IsQ0FBQ3RELHVCQUFqQixFQUFELENBRGE7QUFBQSxLQUxhO0FBT3RDQyxJQUFBQSxvQkFBb0IsRUFBRSw4QkFBQ3NELGlCQUFEO0FBQUEsYUFDbEJILFFBQVEsQ0FBQ0ksT0FBTyxDQUFDQyxrQkFBUixDQUEyQkYsaUJBQTNCLENBQUQsQ0FEVTtBQUFBLEtBUGdCO0FBU3RDRyxJQUFBQSxhQUFhLEVBQUUsdUJBQUN4RCxVQUFEO0FBQUEsYUFBZ0JrRCxRQUFRLENBQUNJLE9BQU8sQ0FBQ0UsYUFBUixDQUFzQnhELFVBQXRCLENBQUQsQ0FBeEI7QUFBQTtBQVR1QixHQUFmO0FBQUEsQ0FBM0I7O0FBWUFmLFlBQVksQ0FBQ3dFLFNBQWIsR0FBeUI7QUFDckJ2RSxFQUFBQSxRQUFRLEVBQUV3RSxtQkFBVUMsS0FEQztBQUVyQnhFLEVBQUFBLDZCQUE2QixFQUFFdUUsbUJBQVVFLE1BRnBCO0FBR3JCeEUsRUFBQUEsbUJBQW1CLEVBQUVzRSxtQkFBVUUsTUFIVjtBQUlyQnZFLEVBQUFBLFlBQVksRUFBRXFFLG1CQUFVRyxNQUpIO0FBS3JCbEMsRUFBQUEsa0JBQWtCLEVBQUUrQixtQkFBVUksTUFMVDtBQU1yQnhFLEVBQUFBLGdCQUFnQixFQUFFb0UsbUJBQVVHLE1BTlA7QUFPckJ0RSxFQUFBQSxvQkFBb0IsRUFBRW1FLG1CQUFVSyxJQVBYO0FBUXJCdkUsRUFBQUEsMkJBQTJCLEVBQUVrRSxtQkFBVUssSUFSbEI7QUFTckJ0RSxFQUFBQSxjQUFjLEVBQUVpRSxtQkFBVUssSUFUTDtBQVVyQnJFLEVBQUFBLFFBQVEsRUFBRWdFLG1CQUFVRyxNQVZDO0FBV3JCbEUsRUFBQUEsa0JBQWtCLEVBQUUrRCxtQkFBVUssSUFYVDtBQVlyQm5FLEVBQUFBLG9CQUFvQixFQUFFOEQsbUJBQVVNLElBWlg7QUFhckJuRSxFQUFBQSxxQkFBcUIsRUFBRTZELG1CQUFVTSxJQWJaO0FBY3JCbEUsRUFBQUEsdUJBQXVCLEVBQUU0RCxtQkFBVU0sSUFkZDtBQWVyQmpFLEVBQUFBLG9CQUFvQixFQUFFMkQsbUJBQVVNLElBZlg7QUFnQnJCaEUsRUFBQUEsVUFBVSxFQUFFMEQsbUJBQVVLO0FBaEJELENBQXpCOztlQW1CZSx5QkFBUTNCLGVBQVIsRUFBeUJhLGtCQUF6QixFQUE2Q2hFLFlBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgV2ViZm9udExvYWRlciBmcm9tICdAZHIta29icm9zL3JlYWN0LXdlYmZvbnQtbG9hZGVyJztcclxuaW1wb3J0IEZyb2FsYUVkaXRvciBmcm9tICdmcm9hbGEtZWRpdG9yJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcclxuXHJcbmltcG9ydCB7IEZPTlRfQ09ORklHIH0gZnJvbSAnQGNvbnN0YW50cy9mcm9hbGEnO1xyXG5pbXBvcnQgeyBGVUxMX0VESVRJT05fTU9ERSwgTk9fRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XHJcblxyXG5pbXBvcnQgeyBnZXRGcm9hbGFFZGl0b3JDb25maWcgfSBmcm9tICdAaGVscGVycy9mcm9hbGEnO1xyXG5cclxuaW1wb3J0ICogYXMgbWFpblNlbGVjdG9ycyBmcm9tICdAbWFpbi9zZWxlY3RvcnMnO1xyXG5cclxuaW1wb3J0IFRhZ1N5c3RlbSBmcm9tICdAbW9kdWxlcy9UYWdTeXN0ZW0nO1xyXG5pbXBvcnQgKiBhcyB0YWdTeXN0ZW1BY3Rpb25zIGZyb20gJ0Btb2R1bGVzL1RhZ1N5c3RlbS9hY3Rpb25zJztcclxuXHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gJy4vYWN0aW9ucyc7XHJcbmltcG9ydCBUZW1wbGF0ZUNvbnRlbnQgZnJvbSAnLi9jb21wb25lbnRzL1RlbXBsYXRlQ29udGVudCc7XHJcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuL3NlbGVjdG9ycyc7XHJcblxyXG5jb25zdCBUZW1wbGF0ZVZpZXcgPSAoe1xyXG4gICAgY2F0YWxvZ3MsXHJcbiAgICBjdXJyZW50VGVtcGxhdGVQcm9wb3NhbE51bWJlcixcclxuICAgIGN1cnJlbnRUZW1wbGF0ZVR5cGUsXHJcbiAgICBlZGl0aW9uTGV2ZWwsXHJcbiAgICBmcm9hbGFMaWNlbnNlS2V5LFxyXG4gICAgaG9yaXpvbnRhbFNjcm9sbE1vZGUsXHJcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQgPSB0cnVlLFxyXG4gICAgaXNJbml0aWFsaXppbmcsXHJcbiAgICBsYW5ndWFnZSxcclxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZCxcclxuICAgIHByZXBhcmVFZGl0Q3VzdG9tVGFnLFxyXG4gICAgcHJlcGFyZUVkaXRMaWJyYXJ5VGFnLFxyXG4gICAgcHJlcGFyZUluc2VydExpYnJhcnlUYWcsXHJcbiAgICBzZXRJc1Rvb2xiYXJEaXNhYmxlZCxcclxuICAgIHNob3dHdWlkZXMsXHJcbn0pID0+IHtcclxuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcclxuICAgIGNvbnN0IFtjdXJyZW50RnJvYWxhRWRpdG9ySW5zdGFuY2UsIHNldEN1cnJlbnRGcm9hbGFFZGl0b3JJbnN0YW5jZV0gPVxyXG4gICAgICAgIHVzZVN0YXRlKG51bGwpO1xyXG5cclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgICAgY29uc3Qgb3JpZ2luYWxIZWxwZXJzID0gRnJvYWxhRWRpdG9yLk1PRFVMRVMuaGVscGVycztcclxuXHJcbiAgICAgICAgRnJvYWxhRWRpdG9yLk1PRFVMRVMuaGVscGVycyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgY29uc3QgaGVscGVycyA9IG9yaWdpbmFsSGVscGVycy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG5cclxuICAgICAgICAgICAgaGVscGVycy5zY3JlZW5TaXplID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDEyMDApIHJldHVybiBGcm9hbGFFZGl0b3IuTEc7XHJcbiAgICAgICAgICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gOTkyKSByZXR1cm4gRnJvYWxhRWRpdG9yLk1EO1xyXG4gICAgICAgICAgICAgICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc2OCkgcmV0dXJuIEZyb2FsYUVkaXRvci5TTTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBGcm9hbGFFZGl0b3IuWFM7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gaGVscGVycztcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBGcm9hbGFFZGl0b3IuRGVmaW5lSWNvbigndGFnTGlzdCcsIHsgTkFNRTogJ3RhZ3MnLCBTVkdfS0VZOiAndGFncycgfSk7XHJcblxyXG4gICAgICAgIEZyb2FsYUVkaXRvci5SZWdpc3RlckNvbW1hbmQoJ3RhZ0xpc3QnLCB7XHJcbiAgICAgICAgICAgIHRpdGxlOiB0KCdTbWFydCBUYWdzJyksXHJcbiAgICAgICAgICAgIHVuZG86IHRydWUsXHJcbiAgICAgICAgICAgIHNob3dPbk1vYmlsZTogdHJ1ZSxcclxuICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uLnNhdmUoKTtcclxuICAgICAgICAgICAgICAgIHByZXBhcmVJbnNlcnRMaWJyYXJ5VGFnKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9LCBbXSk7XHJcbiAgICBpZiAoaXNJbml0aWFsaXppbmcpIHtcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBmcm9hbGFFZGl0b3JDb25maWcgPSBnZXRGcm9hbGFFZGl0b3JDb25maWcoe1xyXG4gICAgICAgIEZyb2FsYUVkaXRvcixcclxuICAgICAgICBmcm9hbGFMaWNlbnNlS2V5LFxyXG4gICAgICAgIGlzRnVsbEVkaXRpb246IGVkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUsXHJcbiAgICAgICAgbGFuZ3VhZ2UsXHJcbiAgICAgICAgcHJlcGFyZUVkaXRDdXN0b21UYWcsXHJcbiAgICAgICAgcHJlcGFyZUVkaXRMaWJyYXJ5VGFnLFxyXG4gICAgICAgIHNldEN1cnJlbnRGcm9hbGFFZGl0b3JJbnN0YW5jZSxcclxuICAgICAgICBzZXRJc1Rvb2xiYXJEaXNhYmxlZCxcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IGF2YWlsYWJsZUNhdGFsb2dzID0gY2F0YWxvZ3MuZmlsdGVyKFxyXG4gICAgICAgIChpdGVtKSA9PiBpdGVtLm9yZGVyIDw9IGN1cnJlbnRUZW1wbGF0ZVByb3Bvc2FsTnVtYmVyXHJcbiAgICApO1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8V2ViZm9udExvYWRlciBjb25maWc9e0ZPTlRfQ09ORklHfT5cclxuICAgICAgICAgICAgPD5cclxuICAgICAgICAgICAgICAgIDxUZW1wbGF0ZUNvbnRlbnRcclxuICAgICAgICAgICAgICAgICAgICBhdmFpbGFibGVDYXRhbG9ncz17YXZhaWxhYmxlQ2F0YWxvZ3N9XHJcbiAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XHJcbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbFNjcm9sbE1vZGU9e2hvcml6b250YWxTY3JvbGxNb2RlfVxyXG4gICAgICAgICAgICAgICAgICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZD17aW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VEZXNpZ25EaXNhYmxlZD17cGFnZURlc2lnbkRpc2FibGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dHdWlkZXM9e3Nob3dHdWlkZXN9XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVUeXBlPXtjdXJyZW50VGVtcGxhdGVUeXBlfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIHtlZGl0aW9uTGV2ZWwgIT09IE5PX0VESVRJT05fTU9ERSAmJiAoXHJcbiAgICAgICAgICAgICAgICAgICAgPFRhZ1N5c3RlbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRhbG9ncz17YXZhaWxhYmxlQ2F0YWxvZ3N9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbD17ZWRpdGlvbkxldmVsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmcm9hbGFJbnN0YW5jZT17Y3VycmVudEZyb2FsYUVkaXRvckluc3RhbmNlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVR5cGU9e2N1cnJlbnRUZW1wbGF0ZVR5cGV9XHJcbiAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgIDwvPlxyXG4gICAgICAgIDwvV2ViZm9udExvYWRlcj5cclxuICAgICk7XHJcbn07XHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xyXG4gICAgY3VycmVudFRlbXBsYXRlUHJvcG9zYWxOdW1iZXI6XHJcbiAgICAgICAgdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVByb3Bvc2FsTnVtYmVyLFxyXG4gICAgY3VycmVudFRlbXBsYXRlVHlwZTogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVR5cGUsXHJcbiAgICBjYXRhbG9nczogbWFpblNlbGVjdG9ycy5nZXRGZXRjaENhdGFsb2dEYXRhLFxyXG4gICAgZnJvYWxhTGljZW5zZUtleTogbWFpblNlbGVjdG9ycy5nZXRGcm9hbGFMaWNlbnNlS2V5LFxyXG4gICAgZ29vZ2xlQXBpS2V5OiBtYWluU2VsZWN0b3JzLmdldEdvb2dsZUFwaUtleSxcclxuICAgIGlzSW5pdGlhbGl6aW5nOiBtYWluU2VsZWN0b3JzLmdldElzSW5pdGlhbGl6aW5nLFxyXG4gICAgbGFuZ3VhZ2U6IG1haW5TZWxlY3RvcnMuZ2V0TGFuZ3VhZ2UsXHJcbiAgICBzaG93R3VpZGVzOiBzZWxlY3RvcnMuZ2V0U2hvd0d1aWRlcyxcclxufSk7XHJcblxyXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+ICh7XHJcbiAgICBwcmVwYXJlRWRpdEN1c3RvbVRhZzogKHZhbHVlcykgPT5cclxuICAgICAgICBkaXNwYXRjaCh0YWdTeXN0ZW1BY3Rpb25zLnByZXBhcmVFZGl0Q3VzdG9tVGFnKHZhbHVlcykpLFxyXG4gICAgcHJlcGFyZUVkaXRMaWJyYXJ5VGFnOiAodmFsdWVzKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKHRhZ1N5c3RlbUFjdGlvbnMucHJlcGFyZUVkaXRMaWJyYXJ5VGFnKHZhbHVlcykpLFxyXG4gICAgcHJlcGFyZUluc2VydExpYnJhcnlUYWc6ICgpID0+XHJcbiAgICAgICAgZGlzcGF0Y2godGFnU3lzdGVtQWN0aW9ucy5wcmVwYXJlSW5zZXJ0TGlicmFyeVRhZygpKSxcclxuICAgIHNldElzVG9vbGJhckRpc2FibGVkOiAoaXNUb29sYmFyRGlzYWJsZWQpID0+XHJcbiAgICAgICAgZGlzcGF0Y2goYWN0aW9ucy5zZXRUb29sYmFyRGlzYWJsZWQoaXNUb29sYmFyRGlzYWJsZWQpKSxcclxuICAgIHNldFNob3dHdWlkZXM6IChzaG93R3VpZGVzKSA9PiBkaXNwYXRjaChhY3Rpb25zLnNldFNob3dHdWlkZXMoc2hvd0d1aWRlcykpLFxyXG59KTtcclxuXHJcblRlbXBsYXRlVmlldy5wcm9wVHlwZXMgPSB7XHJcbiAgICBjYXRhbG9nczogUHJvcFR5cGVzLmFycmF5LFxyXG4gICAgY3VycmVudFRlbXBsYXRlUHJvcG9zYWxOdW1iZXI6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICBjdXJyZW50VGVtcGxhdGVUeXBlOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgZWRpdGlvbkxldmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgZnJvYWxhTGljZW5zZUtleTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGhvcml6b250YWxTY3JvbGxNb2RlOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBpc0luaXRpYWxpemluZzogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBsYW5ndWFnZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBwcmVwYXJlRWRpdEN1c3RvbVRhZzogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBwcmVwYXJlRWRpdExpYnJhcnlUYWc6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgcHJlcGFyZUluc2VydExpYnJhcnlUYWc6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgc2V0SXNUb29sYmFyRGlzYWJsZWQ6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgc2hvd0d1aWRlczogUHJvcFR5cGVzLmJvb2wsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShUZW1wbGF0ZVZpZXcpO1xyXG4iXX0=