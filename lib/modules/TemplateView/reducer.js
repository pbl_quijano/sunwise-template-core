"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _immutabilityHelper = _interopRequireDefault(require("immutability-helper"));

var _actionTypes = require("./actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var INITIAL_STATE = {
  copiedItem: null,
  droppingItem: null,
  isToolbarDisabled: false,
  selectedPageId: null,
  selectedWidgetId: null,
  showGuides: localStorage.getItem('showGuides') === 'true',
  statesPool: {
    data: [],
    index: 0
  }
};

function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actionTypes.ADD_STATE_POOL:
      {
        var _state$statesPool = state.statesPool,
            data = _state$statesPool.data,
            index = _state$statesPool.index;

        var tempData = _toConsumableArray(data);

        var tempIndex = index;

        if (index === tempData.length - 1) {
          tempData = [].concat(_toConsumableArray(tempData), [action.payload]);
        } else {
          tempData.splice(tempIndex + 1, tempData.length - (tempIndex + 1), action.payload);
        }

        ++tempIndex;
        return (0, _immutabilityHelper.default)(state, {
          statesPool: {
            $set: {
              data: tempData,
              index: tempIndex
            }
          }
        });
      }

    case _actionTypes.DROPPING_ITEM:
      return (0, _immutabilityHelper.default)(state, {
        droppingItem: {
          $set: action.payload
        }
      });

    case _actionTypes.INITIALIZE_STATE_POOL:
      return (0, _immutabilityHelper.default)(state, {
        statesPool: {
          $set: {
            data: [action.payload],
            index: 0
          }
        }
      });

    case _actionTypes.NEXT_STATE_POOL:
    case _actionTypes.PREV_STATE_POOL:
      return (0, _immutabilityHelper.default)(state, {
        statesPool: {
          $merge: {
            index: action.payload
          }
        }
      });

    case _actionTypes.RESET:
      return (0, _immutabilityHelper.default)(state, {
        $set: INITIAL_STATE
      });

    case _actionTypes.SELECT_PAGE:
      return (0, _immutabilityHelper.default)(state, {
        selectedPageId: {
          $set: action.payload
        }
      });

    case _actionTypes.SELECT_WIDGET:
      return (0, _immutabilityHelper.default)(state, {
        selectedWidgetId: {
          $set: action.payload
        }
      });

    case _actionTypes.SET_COPIED_ITEM:
      return (0, _immutabilityHelper.default)(state, {
        copiedItem: {
          $set: action.payload
        }
      });

    case _actionTypes.SET_SHOW_GUIDES:
      return (0, _immutabilityHelper.default)(state, {
        showGuides: {
          $set: action.payload
        }
      });

    case _actionTypes.SET_TOOLBAR_DISABLED:
      return (0, _immutabilityHelper.default)(state, {
        isToolbarDisabled: {
          $set: action.payload
        }
      });

    default:
      return state;
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9yZWR1Y2VyLmpzIl0sIm5hbWVzIjpbIklOSVRJQUxfU1RBVEUiLCJjb3BpZWRJdGVtIiwiZHJvcHBpbmdJdGVtIiwiaXNUb29sYmFyRGlzYWJsZWQiLCJzZWxlY3RlZFBhZ2VJZCIsInNlbGVjdGVkV2lkZ2V0SWQiLCJzaG93R3VpZGVzIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInN0YXRlc1Bvb2wiLCJkYXRhIiwiaW5kZXgiLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJBRERfU1RBVEVfUE9PTCIsInRlbXBEYXRhIiwidGVtcEluZGV4IiwibGVuZ3RoIiwicGF5bG9hZCIsInNwbGljZSIsIiRzZXQiLCJEUk9QUElOR19JVEVNIiwiSU5JVElBTElaRV9TVEFURV9QT09MIiwiTkVYVF9TVEFURV9QT09MIiwiUFJFVl9TVEFURV9QT09MIiwiJG1lcmdlIiwiUkVTRVQiLCJTRUxFQ1RfUEFHRSIsIlNFTEVDVF9XSURHRVQiLCJTRVRfQ09QSUVEX0lURU0iLCJTRVRfU0hPV19HVUlERVMiLCJTRVRfVE9PTEJBUl9ESVNBQkxFRCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBY0EsSUFBTUEsYUFBYSxHQUFHO0FBQ2xCQyxFQUFBQSxVQUFVLEVBQUUsSUFETTtBQUVsQkMsRUFBQUEsWUFBWSxFQUFFLElBRkk7QUFHbEJDLEVBQUFBLGlCQUFpQixFQUFFLEtBSEQ7QUFJbEJDLEVBQUFBLGNBQWMsRUFBRSxJQUpFO0FBS2xCQyxFQUFBQSxnQkFBZ0IsRUFBRSxJQUxBO0FBTWxCQyxFQUFBQSxVQUFVLEVBQUVDLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixZQUFyQixNQUF1QyxNQU5qQztBQU9sQkMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLElBQUksRUFBRSxFQURFO0FBRVJDLElBQUFBLEtBQUssRUFBRTtBQUZDO0FBUE0sQ0FBdEI7O0FBYWUsb0JBQXlDO0FBQUEsTUFBL0JDLEtBQStCLHVFQUF2QlosYUFBdUI7QUFBQSxNQUFSYSxNQUFROztBQUNwRCxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDSSxTQUFLQywyQkFBTDtBQUFxQjtBQUNqQixnQ0FBd0JILEtBQUssQ0FBQ0gsVUFBOUI7QUFBQSxZQUFRQyxJQUFSLHFCQUFRQSxJQUFSO0FBQUEsWUFBY0MsS0FBZCxxQkFBY0EsS0FBZDs7QUFDQSxZQUFJSyxRQUFRLHNCQUFPTixJQUFQLENBQVo7O0FBQ0EsWUFBSU8sU0FBUyxHQUFHTixLQUFoQjs7QUFDQSxZQUFJQSxLQUFLLEtBQUtLLFFBQVEsQ0FBQ0UsTUFBVCxHQUFrQixDQUFoQyxFQUFtQztBQUMvQkYsVUFBQUEsUUFBUSxnQ0FBT0EsUUFBUCxJQUFpQkgsTUFBTSxDQUFDTSxPQUF4QixFQUFSO0FBQ0gsU0FGRCxNQUVPO0FBQ0hILFVBQUFBLFFBQVEsQ0FBQ0ksTUFBVCxDQUNJSCxTQUFTLEdBQUcsQ0FEaEIsRUFFSUQsUUFBUSxDQUFDRSxNQUFULElBQW1CRCxTQUFTLEdBQUcsQ0FBL0IsQ0FGSixFQUdJSixNQUFNLENBQUNNLE9BSFg7QUFLSDs7QUFDRCxVQUFFRixTQUFGO0FBQ0EsZUFBTyxpQ0FBT0wsS0FBUCxFQUFjO0FBQ2pCSCxVQUFBQSxVQUFVLEVBQUU7QUFDUlksWUFBQUEsSUFBSSxFQUFFO0FBQ0ZYLGNBQUFBLElBQUksRUFBRU0sUUFESjtBQUVGTCxjQUFBQSxLQUFLLEVBQUVNO0FBRkw7QUFERTtBQURLLFNBQWQsQ0FBUDtBQVFIOztBQUVELFNBQUtLLDBCQUFMO0FBQ0ksYUFBTyxpQ0FBT1YsS0FBUCxFQUFjO0FBQ2pCVixRQUFBQSxZQUFZLEVBQUU7QUFDVm1CLFVBQUFBLElBQUksRUFBRVIsTUFBTSxDQUFDTTtBQURIO0FBREcsT0FBZCxDQUFQOztBQU1KLFNBQUtJLGtDQUFMO0FBQ0ksYUFBTyxpQ0FBT1gsS0FBUCxFQUFjO0FBQ2pCSCxRQUFBQSxVQUFVLEVBQUU7QUFDUlksVUFBQUEsSUFBSSxFQUFFO0FBQ0ZYLFlBQUFBLElBQUksRUFBRSxDQUFDRyxNQUFNLENBQUNNLE9BQVIsQ0FESjtBQUVGUixZQUFBQSxLQUFLLEVBQUU7QUFGTDtBQURFO0FBREssT0FBZCxDQUFQOztBQVNKLFNBQUthLDRCQUFMO0FBQ0EsU0FBS0MsNEJBQUw7QUFDSSxhQUFPLGlDQUFPYixLQUFQLEVBQWM7QUFDakJILFFBQUFBLFVBQVUsRUFBRTtBQUNSaUIsVUFBQUEsTUFBTSxFQUFFO0FBQ0pmLFlBQUFBLEtBQUssRUFBRUUsTUFBTSxDQUFDTTtBQURWO0FBREE7QUFESyxPQUFkLENBQVA7O0FBUUosU0FBS1Esa0JBQUw7QUFDSSxhQUFPLGlDQUFPZixLQUFQLEVBQWM7QUFDakJTLFFBQUFBLElBQUksRUFBRXJCO0FBRFcsT0FBZCxDQUFQOztBQUlKLFNBQUs0Qix3QkFBTDtBQUNJLGFBQU8saUNBQU9oQixLQUFQLEVBQWM7QUFDakJSLFFBQUFBLGNBQWMsRUFBRTtBQUNaaUIsVUFBQUEsSUFBSSxFQUFFUixNQUFNLENBQUNNO0FBREQ7QUFEQyxPQUFkLENBQVA7O0FBTUosU0FBS1UsMEJBQUw7QUFDSSxhQUFPLGlDQUFPakIsS0FBUCxFQUFjO0FBQ2pCUCxRQUFBQSxnQkFBZ0IsRUFBRTtBQUNkZ0IsVUFBQUEsSUFBSSxFQUFFUixNQUFNLENBQUNNO0FBREM7QUFERCxPQUFkLENBQVA7O0FBTUosU0FBS1csNEJBQUw7QUFDSSxhQUFPLGlDQUFPbEIsS0FBUCxFQUFjO0FBQ2pCWCxRQUFBQSxVQUFVLEVBQUU7QUFDUm9CLFVBQUFBLElBQUksRUFBRVIsTUFBTSxDQUFDTTtBQURMO0FBREssT0FBZCxDQUFQOztBQU1KLFNBQUtZLDRCQUFMO0FBQ0ksYUFBTyxpQ0FBT25CLEtBQVAsRUFBYztBQUNqQk4sUUFBQUEsVUFBVSxFQUFFO0FBQ1JlLFVBQUFBLElBQUksRUFBRVIsTUFBTSxDQUFDTTtBQURMO0FBREssT0FBZCxDQUFQOztBQU1KLFNBQUthLGlDQUFMO0FBQ0ksYUFBTyxpQ0FBT3BCLEtBQVAsRUFBYztBQUNqQlQsUUFBQUEsaUJBQWlCLEVBQUU7QUFDZmtCLFVBQUFBLElBQUksRUFBRVIsTUFBTSxDQUFDTTtBQURFO0FBREYsT0FBZCxDQUFQOztBQU1KO0FBQ0ksYUFBT1AsS0FBUDtBQTdGUjtBQStGSCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB1cGRhdGUgZnJvbSAnaW1tdXRhYmlsaXR5LWhlbHBlcic7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgQUREX1NUQVRFX1BPT0wsXHJcbiAgICBEUk9QUElOR19JVEVNLFxyXG4gICAgSU5JVElBTElaRV9TVEFURV9QT09MLFxyXG4gICAgTkVYVF9TVEFURV9QT09MLFxyXG4gICAgUFJFVl9TVEFURV9QT09MLFxyXG4gICAgUkVTRVQsXHJcbiAgICBTRUxFQ1RfUEFHRSxcclxuICAgIFNFTEVDVF9XSURHRVQsXHJcbiAgICBTRVRfQ09QSUVEX0lURU0sXHJcbiAgICBTRVRfU0hPV19HVUlERVMsXHJcbiAgICBTRVRfVE9PTEJBUl9ESVNBQkxFRCxcclxufSBmcm9tICcuL2FjdGlvblR5cGVzJztcclxuXHJcbmNvbnN0IElOSVRJQUxfU1RBVEUgPSB7XHJcbiAgICBjb3BpZWRJdGVtOiBudWxsLFxyXG4gICAgZHJvcHBpbmdJdGVtOiBudWxsLFxyXG4gICAgaXNUb29sYmFyRGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgc2VsZWN0ZWRQYWdlSWQ6IG51bGwsXHJcbiAgICBzZWxlY3RlZFdpZGdldElkOiBudWxsLFxyXG4gICAgc2hvd0d1aWRlczogbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nob3dHdWlkZXMnKSA9PT0gJ3RydWUnLFxyXG4gICAgc3RhdGVzUG9vbDoge1xyXG4gICAgICAgIGRhdGE6IFtdLFxyXG4gICAgICAgIGluZGV4OiAwLFxyXG4gICAgfSxcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIChzdGF0ZSA9IElOSVRJQUxfU1RBVEUsIGFjdGlvbikge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgQUREX1NUQVRFX1BPT0w6IHtcclxuICAgICAgICAgICAgY29uc3QgeyBkYXRhLCBpbmRleCB9ID0gc3RhdGUuc3RhdGVzUG9vbDtcclxuICAgICAgICAgICAgbGV0IHRlbXBEYXRhID0gWy4uLmRhdGFdO1xyXG4gICAgICAgICAgICBsZXQgdGVtcEluZGV4ID0gaW5kZXg7XHJcbiAgICAgICAgICAgIGlmIChpbmRleCA9PT0gdGVtcERhdGEubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgICAgICAgICAgdGVtcERhdGEgPSBbLi4udGVtcERhdGEsIGFjdGlvbi5wYXlsb2FkXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRlbXBEYXRhLnNwbGljZShcclxuICAgICAgICAgICAgICAgICAgICB0ZW1wSW5kZXggKyAxLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBEYXRhLmxlbmd0aCAtICh0ZW1wSW5kZXggKyAxKSxcclxuICAgICAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICArK3RlbXBJbmRleDtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgc3RhdGVzUG9vbDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogdGVtcERhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4OiB0ZW1wSW5kZXgsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FzZSBEUk9QUElOR19JVEVNOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBkcm9wcGluZ0l0ZW06IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIElOSVRJQUxJWkVfU1RBVEVfUE9PTDpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgc3RhdGVzUG9vbDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogW2FjdGlvbi5wYXlsb2FkXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIE5FWFRfU1RBVEVfUE9PTDpcclxuICAgICAgICBjYXNlIFBSRVZfU1RBVEVfUE9PTDpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgc3RhdGVzUG9vbDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRtZXJnZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmRleDogYWN0aW9uLnBheWxvYWQsXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIFJFU0VUOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICAkc2V0OiBJTklUSUFMX1NUQVRFLFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBTRUxFQ1RfUEFHRTpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRQYWdlSWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIFNFTEVDVF9XSURHRVQ6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0SWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIFNFVF9DT1BJRURfSVRFTTpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgY29waWVkSXRlbToge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgU0VUX1NIT1dfR1VJREVTOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBzaG93R3VpZGVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogYWN0aW9uLnBheWxvYWQsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBTRVRfVE9PTEJBUl9ESVNBQkxFRDpcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgaXNUb29sYmFyRGlzYWJsZWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBhY3Rpb24ucGF5bG9hZCxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gc3RhdGU7XHJcbiAgICB9XHJcbn1cclxuIl19