"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _types = require("../../../constants/types");

var mainSelectors = _interopRequireWildcard(require("../../main/selectors"));

var _OnePageView = _interopRequireDefault(require("./OnePageView"));

var _PageListView = _interopRequireDefault(require("./PageListView"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TemplateContent = function TemplateContent(_ref) {
  var availableCatalogs = _ref.availableCatalogs,
      chartWidgets = _ref.chartWidgets,
      editionLevel = _ref.editionLevel,
      froalaEditorConfig = _ref.froalaEditorConfig,
      horizontalScrollMode = _ref.horizontalScrollMode,
      infinitePagesSupportEnabled = _ref.infinitePagesSupportEnabled,
      pageDesignDisabled = _ref.pageDesignDisabled,
      showGuides = _ref.showGuides,
      tableWidgets = _ref.tableWidgets;
  var tableWidgetsNames = tableWidgets.map(function (tableWidget) {
    return tableWidget.id;
  });
  var chartWidgetsNames = chartWidgets.map(function (chartWidget) {
    return chartWidget.id;
  });

  if (editionLevel === _types.FULL_EDITION_MODE) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_OnePageView.default, {
      availableCatalogs: availableCatalogs,
      chartWidgetsNames: chartWidgetsNames,
      froalaEditorConfig: froalaEditorConfig,
      pageDesignDisabled: pageDesignDisabled,
      showGuides: showGuides,
      tableWidgetsNames: tableWidgetsNames
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_PageListView.default, {
    availableCatalogs: availableCatalogs,
    chartWidgetsNames: chartWidgetsNames,
    editionLevel: editionLevel,
    froalaEditorConfig: froalaEditorConfig,
    horizontalScrollMode: horizontalScrollMode,
    infinitePagesSupportEnabled: infinitePagesSupportEnabled,
    pageDesignDisabled: pageDesignDisabled,
    showGuides: showGuides,
    tableWidgetsNames: tableWidgetsNames
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  chartWidgets: mainSelectors.getDataFetchChartWidgets,
  tableWidgets: mainSelectors.getDataFetchTableWidgets
});
TemplateContent.propTypes = {
  availableCatalogs: _propTypes.default.array,
  chartWidgets: _propTypes.default.array,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  horizontalScrollMode: _propTypes.default.bool,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  pageDesignDisabled: _propTypes.default.bool,
  showGuides: _propTypes.default.bool,
  tableWidgets: _propTypes.default.array
};

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(TemplateContent);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1RlbXBsYXRlQ29udGVudC5qcyJdLCJuYW1lcyI6WyJUZW1wbGF0ZUNvbnRlbnQiLCJhdmFpbGFibGVDYXRhbG9ncyIsImNoYXJ0V2lkZ2V0cyIsImVkaXRpb25MZXZlbCIsImZyb2FsYUVkaXRvckNvbmZpZyIsImhvcml6b250YWxTY3JvbGxNb2RlIiwiaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkIiwicGFnZURlc2lnbkRpc2FibGVkIiwic2hvd0d1aWRlcyIsInRhYmxlV2lkZ2V0cyIsInRhYmxlV2lkZ2V0c05hbWVzIiwibWFwIiwidGFibGVXaWRnZXQiLCJpZCIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiY2hhcnRXaWRnZXQiLCJGVUxMX0VESVRJT05fTU9ERSIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1haW5TZWxlY3RvcnMiLCJnZXREYXRhRmV0Y2hDaGFydFdpZGdldHMiLCJnZXREYXRhRmV0Y2hUYWJsZVdpZGdldHMiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsInN0cmluZyIsIm9iamVjdCIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixPQVVsQjtBQUFBLE1BVEZDLGlCQVNFLFFBVEZBLGlCQVNFO0FBQUEsTUFSRkMsWUFRRSxRQVJGQSxZQVFFO0FBQUEsTUFQRkMsWUFPRSxRQVBGQSxZQU9FO0FBQUEsTUFORkMsa0JBTUUsUUFORkEsa0JBTUU7QUFBQSxNQUxGQyxvQkFLRSxRQUxGQSxvQkFLRTtBQUFBLE1BSkZDLDJCQUlFLFFBSkZBLDJCQUlFO0FBQUEsTUFIRkMsa0JBR0UsUUFIRkEsa0JBR0U7QUFBQSxNQUZGQyxVQUVFLFFBRkZBLFVBRUU7QUFBQSxNQURGQyxZQUNFLFFBREZBLFlBQ0U7QUFDRixNQUFNQyxpQkFBaUIsR0FBR0QsWUFBWSxDQUFDRSxHQUFiLENBQWlCLFVBQUNDLFdBQUQ7QUFBQSxXQUFpQkEsV0FBVyxDQUFDQyxFQUE3QjtBQUFBLEdBQWpCLENBQTFCO0FBQ0EsTUFBTUMsaUJBQWlCLEdBQUdaLFlBQVksQ0FBQ1MsR0FBYixDQUFpQixVQUFDSSxXQUFEO0FBQUEsV0FBaUJBLFdBQVcsQ0FBQ0YsRUFBN0I7QUFBQSxHQUFqQixDQUExQjs7QUFDQSxNQUFJVixZQUFZLEtBQUthLHdCQUFyQixFQUF3QztBQUNwQyx3QkFDSSxxQkFBQyxvQkFBRDtBQUNJLE1BQUEsaUJBQWlCLEVBQUVmLGlCQUR2QjtBQUVJLE1BQUEsaUJBQWlCLEVBQUVhLGlCQUZ2QjtBQUdJLE1BQUEsa0JBQWtCLEVBQUVWLGtCQUh4QjtBQUlJLE1BQUEsa0JBQWtCLEVBQUVHLGtCQUp4QjtBQUtJLE1BQUEsVUFBVSxFQUFFQyxVQUxoQjtBQU1JLE1BQUEsaUJBQWlCLEVBQUVFO0FBTnZCLE1BREo7QUFVSDs7QUFDRCxzQkFDSSxxQkFBQyxxQkFBRDtBQUNJLElBQUEsaUJBQWlCLEVBQUVULGlCQUR2QjtBQUVJLElBQUEsaUJBQWlCLEVBQUVhLGlCQUZ2QjtBQUdJLElBQUEsWUFBWSxFQUFFWCxZQUhsQjtBQUlJLElBQUEsa0JBQWtCLEVBQUVDLGtCQUp4QjtBQUtJLElBQUEsb0JBQW9CLEVBQUVDLG9CQUwxQjtBQU1JLElBQUEsMkJBQTJCLEVBQUVDLDJCQU5qQztBQU9JLElBQUEsa0JBQWtCLEVBQUVDLGtCQVB4QjtBQVFJLElBQUEsVUFBVSxFQUFFQyxVQVJoQjtBQVNJLElBQUEsaUJBQWlCLEVBQUVFO0FBVHZCLElBREo7QUFhSCxDQXRDRDs7QUF3Q0EsSUFBTU8sZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q2YsRUFBQUEsWUFBWSxFQUFFZ0IsYUFBYSxDQUFDQyx3QkFEaUI7QUFFN0NWLEVBQUFBLFlBQVksRUFBRVMsYUFBYSxDQUFDRTtBQUZpQixDQUF6QixDQUF4QjtBQUtBcEIsZUFBZSxDQUFDcUIsU0FBaEIsR0FBNEI7QUFDeEJwQixFQUFBQSxpQkFBaUIsRUFBRXFCLG1CQUFVQyxLQURMO0FBRXhCckIsRUFBQUEsWUFBWSxFQUFFb0IsbUJBQVVDLEtBRkE7QUFHeEJwQixFQUFBQSxZQUFZLEVBQUVtQixtQkFBVUUsTUFIQTtBQUl4QnBCLEVBQUFBLGtCQUFrQixFQUFFa0IsbUJBQVVHLE1BSk47QUFLeEJwQixFQUFBQSxvQkFBb0IsRUFBRWlCLG1CQUFVSSxJQUxSO0FBTXhCcEIsRUFBQUEsMkJBQTJCLEVBQUVnQixtQkFBVUksSUFOZjtBQU94Qm5CLEVBQUFBLGtCQUFrQixFQUFFZSxtQkFBVUksSUFQTjtBQVF4QmxCLEVBQUFBLFVBQVUsRUFBRWMsbUJBQVVJLElBUkU7QUFTeEJqQixFQUFBQSxZQUFZLEVBQUVhLG1CQUFVQztBQVRBLENBQTVCOztlQVllLHlCQUFRTixlQUFSLEVBQXlCLElBQXpCLEVBQStCakIsZUFBL0IsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcclxuXHJcbmltcG9ydCB7IEZVTExfRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XHJcblxyXG5pbXBvcnQgKiBhcyBtYWluU2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgT25lUGFnZVZpZXcgZnJvbSAnLi9PbmVQYWdlVmlldyc7XHJcbmltcG9ydCBQYWdlTGlzdFZpZXcgZnJvbSAnLi9QYWdlTGlzdFZpZXcnO1xyXG5cclxuY29uc3QgVGVtcGxhdGVDb250ZW50ID0gKHtcclxuICAgIGF2YWlsYWJsZUNhdGFsb2dzLFxyXG4gICAgY2hhcnRXaWRnZXRzLFxyXG4gICAgZWRpdGlvbkxldmVsLFxyXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnLFxyXG4gICAgaG9yaXpvbnRhbFNjcm9sbE1vZGUsXHJcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQsXHJcbiAgICBwYWdlRGVzaWduRGlzYWJsZWQsXHJcbiAgICBzaG93R3VpZGVzLFxyXG4gICAgdGFibGVXaWRnZXRzLFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCB0YWJsZVdpZGdldHNOYW1lcyA9IHRhYmxlV2lkZ2V0cy5tYXAoKHRhYmxlV2lkZ2V0KSA9PiB0YWJsZVdpZGdldC5pZCk7XHJcbiAgICBjb25zdCBjaGFydFdpZGdldHNOYW1lcyA9IGNoYXJ0V2lkZ2V0cy5tYXAoKGNoYXJ0V2lkZ2V0KSA9PiBjaGFydFdpZGdldC5pZCk7XHJcbiAgICBpZiAoZWRpdGlvbkxldmVsID09PSBGVUxMX0VESVRJT05fTU9ERSkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxPbmVQYWdlVmlld1xyXG4gICAgICAgICAgICAgICAgYXZhaWxhYmxlQ2F0YWxvZ3M9e2F2YWlsYWJsZUNhdGFsb2dzfVxyXG4gICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXM9e2NoYXJ0V2lkZ2V0c05hbWVzfVxyXG4gICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XHJcbiAgICAgICAgICAgICAgICBwYWdlRGVzaWduRGlzYWJsZWQ9e3BhZ2VEZXNpZ25EaXNhYmxlZH1cclxuICAgICAgICAgICAgICAgIHNob3dHdWlkZXM9e3Nob3dHdWlkZXN9XHJcbiAgICAgICAgICAgICAgICB0YWJsZVdpZGdldHNOYW1lcz17dGFibGVXaWRnZXRzTmFtZXN9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPFBhZ2VMaXN0Vmlld1xyXG4gICAgICAgICAgICBhdmFpbGFibGVDYXRhbG9ncz17YXZhaWxhYmxlQ2F0YWxvZ3N9XHJcbiAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XHJcbiAgICAgICAgICAgIGZyb2FsYUVkaXRvckNvbmZpZz17ZnJvYWxhRWRpdG9yQ29uZmlnfVxyXG4gICAgICAgICAgICBob3Jpem9udGFsU2Nyb2xsTW9kZT17aG9yaXpvbnRhbFNjcm9sbE1vZGV9XHJcbiAgICAgICAgICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZD17aW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkfVxyXG4gICAgICAgICAgICBwYWdlRGVzaWduRGlzYWJsZWQ9e3BhZ2VEZXNpZ25EaXNhYmxlZH1cclxuICAgICAgICAgICAgc2hvd0d1aWRlcz17c2hvd0d1aWRlc31cclxuICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxyXG4gICAgICAgIC8+XHJcbiAgICApO1xyXG59O1xyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcclxuICAgIGNoYXJ0V2lkZ2V0czogbWFpblNlbGVjdG9ycy5nZXREYXRhRmV0Y2hDaGFydFdpZGdldHMsXHJcbiAgICB0YWJsZVdpZGdldHM6IG1haW5TZWxlY3RvcnMuZ2V0RGF0YUZldGNoVGFibGVXaWRnZXRzLFxyXG59KTtcclxuXHJcblRlbXBsYXRlQ29udGVudC5wcm9wVHlwZXMgPSB7XHJcbiAgICBhdmFpbGFibGVDYXRhbG9nczogUHJvcFR5cGVzLmFycmF5LFxyXG4gICAgY2hhcnRXaWRnZXRzOiBQcm9wVHlwZXMuYXJyYXksXHJcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICBmcm9hbGFFZGl0b3JDb25maWc6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICBob3Jpem9udGFsU2Nyb2xsTW9kZTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgcGFnZURlc2lnbkRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIHNob3dHdWlkZXM6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgdGFibGVXaWRnZXRzOiBQcm9wVHlwZXMuYXJyYXksXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbnVsbCkoVGVtcGxhdGVDb250ZW50KTtcclxuIl19