"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _ChartPanel = _interopRequireDefault(require("./settingsPanels/ChartPanel"));

var _ClipArtPanel = _interopRequireDefault(require("./settingsPanels/ClipArtPanel"));

var _ImagePanel = _interopRequireDefault(require("./settingsPanels/ImagePanel"));

var _LocationPanel = _interopRequireDefault(require("./settingsPanels/LocationPanel"));

var _SegmentsMapPanel = _interopRequireDefault(require("./settingsPanels/SegmentsMapPanel"));

var _SeparatorPanel = _interopRequireDefault(require("./settingsPanels/SeparatorPanel"));

var _SupportTablePanel = _interopRequireDefault(require("./settingsPanels/SupportTablePanel"));

var _TablePanel = _interopRequireDefault(require("./settingsPanels/TablePanel"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SettingsPanelContent = function SettingsPanelContent(props) {
  var chartWidgetsNames = props.chartWidgetsNames,
      hasProposalSelector = props.hasProposalSelector,
      tableWidgetsNames = props.tableWidgetsNames,
      type = props.type;

  switch (type) {
    case 'clip-art':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ClipArtPanel.default, _objectSpread({}, props));

    case 'image':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ImagePanel.default, _objectSpread({}, props));

    case 'map':
      if (hasProposalSelector) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_LocationPanel.default, _objectSpread({}, props));
      }

      return null;

    case 'segments-map':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_SegmentsMapPanel.default, _objectSpread({}, props));

    case 'separator':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_SeparatorPanel.default, _objectSpread({}, props));

    case 'money-table':
    case 'kwh-table':
    case 'equipment-table':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_SupportTablePanel.default, _objectSpread({}, props));

    default:
      if (tableWidgetsNames.includes(type)) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_TablePanel.default, _objectSpread({}, props));
      }

      if (chartWidgetsNames.includes(type)) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ChartPanel.default, _objectSpread({}, props));
      }

      return null;
  }
};

SettingsPanelContent.propTypes = {
  chartWidgetsNames: _propTypes.default.array,
  hasProposalSelector: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  type: _propTypes.default.string
};
var _default = SettingsPanelContent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1NldHRpbmdzUGFuZWxDb250ZW50LmpzIl0sIm5hbWVzIjpbIlNldHRpbmdzUGFuZWxDb250ZW50IiwicHJvcHMiLCJjaGFydFdpZGdldHNOYW1lcyIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJ0YWJsZVdpZGdldHNOYW1lcyIsInR5cGUiLCJpbmNsdWRlcyIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5IiwiYm9vbCIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNDLEtBQUQsRUFBVztBQUNwQyxNQUFRQyxpQkFBUixHQUNJRCxLQURKLENBQVFDLGlCQUFSO0FBQUEsTUFBMkJDLG1CQUEzQixHQUNJRixLQURKLENBQTJCRSxtQkFBM0I7QUFBQSxNQUFnREMsaUJBQWhELEdBQ0lILEtBREosQ0FBZ0RHLGlCQUFoRDtBQUFBLE1BQW1FQyxJQUFuRSxHQUNJSixLQURKLENBQW1FSSxJQUFuRTs7QUFFQSxVQUFRQSxJQUFSO0FBQ0ksU0FBSyxVQUFMO0FBQ0ksMEJBQU8scUJBQUMscUJBQUQsb0JBQWtCSixLQUFsQixFQUFQOztBQUNKLFNBQUssT0FBTDtBQUNJLDBCQUFPLHFCQUFDLG1CQUFELG9CQUFnQkEsS0FBaEIsRUFBUDs7QUFDSixTQUFLLEtBQUw7QUFDSSxVQUFJRSxtQkFBSixFQUF5QjtBQUNyQiw0QkFBTyxxQkFBQyxzQkFBRCxvQkFBbUJGLEtBQW5CLEVBQVA7QUFDSDs7QUFDRCxhQUFPLElBQVA7O0FBQ0osU0FBSyxjQUFMO0FBQ0ksMEJBQU8scUJBQUMseUJBQUQsb0JBQXNCQSxLQUF0QixFQUFQOztBQUNKLFNBQUssV0FBTDtBQUNJLDBCQUFPLHFCQUFDLHVCQUFELG9CQUFvQkEsS0FBcEIsRUFBUDs7QUFDSixTQUFLLGFBQUw7QUFDQSxTQUFLLFdBQUw7QUFDQSxTQUFLLGlCQUFMO0FBQ0ksMEJBQU8scUJBQUMsMEJBQUQsb0JBQXVCQSxLQUF2QixFQUFQOztBQUNKO0FBQ0ksVUFBSUcsaUJBQWlCLENBQUNFLFFBQWxCLENBQTJCRCxJQUEzQixDQUFKLEVBQXNDO0FBQ2xDLDRCQUFPLHFCQUFDLG1CQUFELG9CQUFnQkosS0FBaEIsRUFBUDtBQUNIOztBQUNELFVBQUlDLGlCQUFpQixDQUFDSSxRQUFsQixDQUEyQkQsSUFBM0IsQ0FBSixFQUFzQztBQUNsQyw0QkFBTyxxQkFBQyxtQkFBRCxvQkFBZ0JKLEtBQWhCLEVBQVA7QUFDSDs7QUFDRCxhQUFPLElBQVA7QUF6QlI7QUEyQkgsQ0E5QkQ7O0FBZ0NBRCxvQkFBb0IsQ0FBQ08sU0FBckIsR0FBaUM7QUFDN0JMLEVBQUFBLGlCQUFpQixFQUFFTSxtQkFBVUMsS0FEQTtBQUU3Qk4sRUFBQUEsbUJBQW1CLEVBQUVLLG1CQUFVRSxJQUZGO0FBRzdCTixFQUFBQSxpQkFBaUIsRUFBRUksbUJBQVVDLEtBSEE7QUFJN0JKLEVBQUFBLElBQUksRUFBRUcsbUJBQVVHO0FBSmEsQ0FBakM7ZUFPZVgsb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgQ2hhcnRQYW5lbCBmcm9tICcuL3NldHRpbmdzUGFuZWxzL0NoYXJ0UGFuZWwnO1xuaW1wb3J0IENsaXBBcnRQYW5lbCBmcm9tICcuL3NldHRpbmdzUGFuZWxzL0NsaXBBcnRQYW5lbCc7XG5pbXBvcnQgSW1hZ2VQYW5lbCBmcm9tICcuL3NldHRpbmdzUGFuZWxzL0ltYWdlUGFuZWwnO1xuaW1wb3J0IExvY2F0aW9uUGFuZWwgZnJvbSAnLi9zZXR0aW5nc1BhbmVscy9Mb2NhdGlvblBhbmVsJztcbmltcG9ydCBTZWdtZW50c01hcFBhbmVsIGZyb20gJy4vc2V0dGluZ3NQYW5lbHMvU2VnbWVudHNNYXBQYW5lbCc7XG5pbXBvcnQgU2VwYXJhdG9yUGFuZWwgZnJvbSAnLi9zZXR0aW5nc1BhbmVscy9TZXBhcmF0b3JQYW5lbCc7XG5pbXBvcnQgU3VwcG9ydFRhYmxlUGFuZWwgZnJvbSAnLi9zZXR0aW5nc1BhbmVscy9TdXBwb3J0VGFibGVQYW5lbCc7XG5pbXBvcnQgVGFibGVQYW5lbCBmcm9tICcuL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwnO1xuXG5jb25zdCBTZXR0aW5nc1BhbmVsQ29udGVudCA9IChwcm9wcykgPT4ge1xuICAgIGNvbnN0IHsgY2hhcnRXaWRnZXRzTmFtZXMsIGhhc1Byb3Bvc2FsU2VsZWN0b3IsIHRhYmxlV2lkZ2V0c05hbWVzLCB0eXBlIH0gPVxuICAgICAgICBwcm9wcztcbiAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgY2FzZSAnY2xpcC1hcnQnOlxuICAgICAgICAgICAgcmV0dXJuIDxDbGlwQXJ0UGFuZWwgey4uLnByb3BzfSAvPjtcbiAgICAgICAgY2FzZSAnaW1hZ2UnOlxuICAgICAgICAgICAgcmV0dXJuIDxJbWFnZVBhbmVsIHsuLi5wcm9wc30gLz47XG4gICAgICAgIGNhc2UgJ21hcCc6XG4gICAgICAgICAgICBpZiAoaGFzUHJvcG9zYWxTZWxlY3Rvcikge1xuICAgICAgICAgICAgICAgIHJldHVybiA8TG9jYXRpb25QYW5lbCB7Li4ucHJvcHN9IC8+O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIGNhc2UgJ3NlZ21lbnRzLW1hcCc6XG4gICAgICAgICAgICByZXR1cm4gPFNlZ21lbnRzTWFwUGFuZWwgey4uLnByb3BzfSAvPjtcbiAgICAgICAgY2FzZSAnc2VwYXJhdG9yJzpcbiAgICAgICAgICAgIHJldHVybiA8U2VwYXJhdG9yUGFuZWwgey4uLnByb3BzfSAvPjtcbiAgICAgICAgY2FzZSAnbW9uZXktdGFibGUnOlxuICAgICAgICBjYXNlICdrd2gtdGFibGUnOlxuICAgICAgICBjYXNlICdlcXVpcG1lbnQtdGFibGUnOlxuICAgICAgICAgICAgcmV0dXJuIDxTdXBwb3J0VGFibGVQYW5lbCB7Li4ucHJvcHN9IC8+O1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgaWYgKHRhYmxlV2lkZ2V0c05hbWVzLmluY2x1ZGVzKHR5cGUpKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIDxUYWJsZVBhbmVsIHsuLi5wcm9wc30gLz47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoY2hhcnRXaWRnZXRzTmFtZXMuaW5jbHVkZXModHlwZSkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gPENoYXJ0UGFuZWwgey4uLnByb3BzfSAvPjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbn07XG5cblNldHRpbmdzUGFuZWxDb250ZW50LnByb3BUeXBlcyA9IHtcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I6IFByb3BUeXBlcy5ib29sLFxuICAgIHRhYmxlV2lkZ2V0c05hbWVzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFNldHRpbmdzUGFuZWxDb250ZW50O1xuIl19