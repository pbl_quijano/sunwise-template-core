"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styled = require("../../../components/styled");

var _template = require("../../../constants/template");

var _types = require("../../../constants/types");

var _contexts = require("../../../helpers/contexts");

var templateCoreActions = _interopRequireWildcard(require("../../TemplateCore/actions"));

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

var actions = _interopRequireWildcard(require("../actions"));

var selectors = _interopRequireWildcard(require("../selectors"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11, _templateObject12, _templateObject13, _templateObject14, _templateObject15;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var BringToBackIcon = function BringToBackIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fillRule: "nonzero",
      fill: "none",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M12.186.254H.873a.69.69 0 0 0-.689.689v11.313c0 .379.31.688.689.688h11.313a.69.69 0 0 0 .689-.688V.943a.687.687 0 0 0-.689-.689z",
        fill: "#D1D7EF"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M17.186 3.944H5.873a.69.69 0 0 0-.689.689v11.313c0 .379.31.688.689.688h11.313a.69.69 0 0 0 .689-.688V4.633a.687.687 0 0 0-.689-.689zm-.689 11.313H6.562V5.322h9.935v9.935z",
        fill: "#AAB4D9"
      })]
    })
  }));
};

BringToBackIcon.defaultProps = {
  viewBox: "0 0 18 17",
  xmlns: "http://www.w3.org/2000/svg"
};

var BringToFrontIcon = function BringToFrontIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fillRule: "nonzero",
      fill: "none",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M12.186.254H.873a.69.69 0 0 0-.689.689v11.313c0 .379.31.688.689.688h11.313a.69.69 0 0 0 .689-.688V.943a.687.687 0 0 0-.689-.689zm-.689 11.313H1.562V1.632h9.935v9.935z",
        fill: "#AAB4D9"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M19.001 5H7.688a.69.69 0 0 0-.689.689v11.313c0 .379.31.688.689.688h11.313a.69.69 0 0 0 .689-.688V5.689A.687.687 0 0 0 19 5z",
        fill: "#D1D7EF"
      })]
    })
  }));
};

BringToFrontIcon.defaultProps = {
  viewBox: "0 0 20 18",
  xmlns: "http://www.w3.org/2000/svg"
};

var GridsIcon = function GridsIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("g", {
      fill: "#D3D7EB",
      fillRule: "evenodd",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M20.277 17.231h2.698v-1.436h-4.134zM17.231 5.744V1.436h-1.436v4.308H8.79l1.436 1.436h5.569v5.57l1.436 1.436V7.18h5.744V5.744h-5.744zM7.18 1.436H5.744v1.262L7.18 4.134z"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "m0 1.015 4.729 4.729H0V7.18h5.744v8.615H0v1.436h5.744v4.308H7.18v-4.308h8.615v4.308h1.436v-3.293l4.729 4.729 1.015-1.015L1.015 0 0 1.015zm7.18 14.78v-7.6l7.6 7.6h-7.6z",
        fillRule: "nonzero"
      })]
    })
  }));
};

GridsIcon.defaultProps = {
  viewBox: "0 0 23 23",
  xmlns: "http://www.w3.org/2000/svg"
};

var PasteIcon = function PasteIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M128 184c0-30.879 25.122-56 56-56h136V56c0-13.255-10.745-24-24-24h-80.61C204.306 12.89 183.637 0 160 0s-44.306 12.89-55.39 32H24C10.745 32 0 42.745 0 56v336c0 13.255 10.745 24 24 24h104V184zm32-144c13.255 0 24 10.745 24 24s-10.745 24-24 24-24-10.745-24-24 10.745-24 24-24zm184 248h104v200c0 13.255-10.745 24-24 24H184c-13.255 0-24-10.745-24-24V184c0-13.255 10.745-24 24-24h136v104c0 13.2 10.8 24 24 24zm104-38.059V256h-96v-96h6.059a24 24 0 0 1 16.97 7.029l65.941 65.941a24.002 24.002 0 0 1 7.03 16.971z",
      fill: "currentColor"
    })
  }));
};

PasteIcon.defaultProps = {
  className: "svg-inline--fa fa-paste fa-w-14",
  'aria-hidden': "true",
  'data-icon': "paste",
  'data-prefix': "fas",
  viewBox: "0 0 448 512",
  xmlns: "http://www.w3.org/2000/svg"
};

var RedoIcon = function RedoIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
      d: "M.358 16.978A.447.447 0 0 1 0 16.542c0-7.253 8.61-8.798 10.61-9.059V4.155c0-.164.09-.314.235-.393a.451.451 0 0 1 .456.024l9.202 6.194a.442.442 0 0 1 .194.369.442.442 0 0 1-.194.366l-9.197 6.193a.446.446 0 0 1-.693-.371v-3.58c-1.447.009-2.645.073-3.642.193-4.785.567-6.064 3.44-6.116 3.563a.447.447 0 0 1-.41.271c-.03.002-.06 0-.087-.006z",
      fill: "#030104"
    })
  }));
};

RedoIcon.defaultProps = {
  viewBox: "0 0 20.697 20.697",
  xmlSpace: "preserve",
  xmlns: "http://www.w3.org/2000/svg"
};

var ToBackIcon = function ToBackIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("g", {
      fill: "#D2D7ED",
      fillRule: "nonzero",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M8.558 20.855a.696.696 0 1 0-.986.986l.789.789h-3.2a3.773 3.773 0 0 1-3.77-3.77v-2.338a.698.698 0 0 0-.695-.696.698.698 0 0 0-.696.696v2.338a5.165 5.165 0 0 0 5.16 5.162h3.2l-.788.789a.697.697 0 1 0 .986.986l1.977-1.978a.697.697 0 0 0 0-.986l-1.977-1.978zM12.294 0H.705C.317 0-.001.304-.001.675V11.76c0 .371.318.675.706.675h11.589c.388 0 .706-.304.706-.675V.675A.689.689 0 0 0 12.294 0zm-.706 11.085H1.41V1.35h10.178v9.735zM20.834 1.98h-3.2l.788-.79a.697.697 0 1 0-.986-.986l-1.97 1.978a.697.697 0 0 0 0 .986l1.976 1.978a.698.698 0 0 0 .986-.986l-.789-.789h3.2a3.773 3.773 0 0 1 3.77 3.77v2.337c0 .383.312.696.695.696A.698.698 0 0 0 26 9.478V7.141a5.17 5.17 0 0 0-5.166-5.162zM25.435 24.87c-.15 0-.297.061-.4.165s-.165.25-.165.4c0 .146.061.297.165.4s.25.165.4.165.297-.06.4-.164.165-.25.165-.4-.061-.298-.165-.401a.588.588 0 0 0-.4-.165zM22.609 24.87c-.31 0-.565.254-.565.565s.254.565.565.565c.31 0 .565-.254.565-.565s-.254-.565-.565-.565zM16.957 24.87c-.31 0-.565.254-.565.565s.254.565.565.565c.31 0 .565-.254.565-.565s-.25-.565-.565-.565zM19.783 24.87c-.31 0-.565.254-.565.565s.254.565.565.565.565-.254.565-.565-.254-.565-.565-.565zM14.13 24.87c-.15 0-.297.061-.4.165s-.165.25-.165.4c0 .146.061.297.165.4s.25.165.4.165c.146 0 .297-.06.4-.164s.165-.25.165-.4-.061-.298-.165-.401-.254-.165-.4-.165zM14.13 22.043a.567.567 0 0 0-.565.565c0 .311.254.565.565.565.31 0 .565-.254.565-.565a.567.567 0 0 0-.565-.565zM14.13 19.217c-.31 0-.565.254-.565.565s.254.565.565.565c.31 0 .565-.254.565-.565s-.254-.565-.565-.565zM14.13 16.391a.567.567 0 0 0-.565.565c0 .311.254.565.565.565.31 0 .565-.254.565-.565a.567.567 0 0 0-.565-.565zM14.13 13.565c-.15 0-.297.061-.4.165s-.165.25-.165.4c0 .15.061.297.165.4s.25.165.4.165c.146 0 .297-.06.4-.164s.165-.25.165-.4a.58.58 0 0 0-.165-.401.567.567 0 0 0-.4-.165zM22.609 13.565a.567.567 0 0 0-.565.565c0 .311.254.565.565.565.31 0 .565-.254.565-.565a.567.567 0 0 0-.565-.565zM19.783 13.565a.567.567 0 0 0-.565.565c0 .311.254.565.565.565s.565-.254.565-.565a.567.567 0 0 0-.565-.565zM16.957 13.565a.567.567 0 0 0-.565.565c0 .311.254.565.565.565a.564.564 0 1 0 0-1.13zM25.435 13.565c-.15 0-.297.061-.4.165s-.165.25-.165.4c0 .15.061.297.165.4s.25.165.4.165.297-.06.4-.164.165-.25.165-.4a.58.58 0 0 0-.165-.401.588.588 0 0 0-.4-.165zM25.435 16.391a.567.567 0 0 0-.565.565c0 .311.254.565.565.565s.565-.254.565-.565a.567.567 0 0 0-.565-.565zM25.435 22.043a.567.567 0 0 0-.565.565c0 .311.254.565.565.565s.565-.254.565-.565a.567.567 0 0 0-.565-.565zM25.435 19.217c-.31 0-.565.254-.565.565s.254.565.565.565.565-.254.565-.565-.254-.565-.565-.565z"
      })
    })
  }));
};

ToBackIcon.defaultProps = {
  viewBox: "0 0 26 26",
  xmlns: "http://www.w3.org/2000/svg"
};

var ToFrontIcon = function ToFrontIcon(props) {
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", _objectSpread(_objectSpread({}, props), {}, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("g", {
      fill: "#D1D7EF",
      fillRule: "nonzero",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("path", {
        d: "M20.812 1.959h-3.168l.78-.781a.69.69 0 1 0-.975-.976L15.497 2.16c-.132.132-.2.304-.2.487s.074.356.2.488l1.957 1.957a.691.691 0 0 0 .976-.975l-.78-.78h3.168c2.06 0 3.73 1.675 3.73 3.73V9.38c0 .379.31.689.69.689a.69.69 0 0 0 .688-.69V7.068a5.117 5.117 0 0 0-5.114-5.108zM25.186 13.254H13.873a.69.69 0 0 0-.689.689v11.313c0 .379.31.688.689.688h11.313a.69.69 0 0 0 .689-.688V13.943a.687.687 0 0 0-.689-.689zm-.689 11.313h-9.935v-9.935h9.935v9.935zM8.49 20.853a.69.69 0 1 0-.976.976l.78.78H5.126a3.734 3.734 0 0 1-3.73-3.73v-2.313a.69.69 0 0 0-.69-.69.69.69 0 0 0-.688.69v2.313a5.112 5.112 0 0 0 5.108 5.108h3.168l-.78.78a.69.69 0 1 0 .975.976l1.958-1.957a.69.69 0 0 0 0-.976l-1.958-1.957zM.758 1.379a.695.695 0 0 0 .488-.201.695.695 0 0 0 0-.976.695.695 0 0 0-.976 0 .695.695 0 0 0 0 .976c.132.126.305.2.488.2zM6.418 1.379A.69.69 0 0 0 7.106.69a.69.69 0 0 0-.688-.689.69.69 0 0 0-.69.69c0 .378.305.688.69.688zM3.588 1.379A.69.69 0 0 0 4.277.69.69.69 0 0 0 2.9.69c0 .379.31.689.688.689zM9.241 1.379A.69.69 0 0 0 9.93.69a.69.69 0 0 0-.689-.689.69.69 0 0 0-.688.69c0 .378.31.688.688.688zM12.071.001a.706.706 0 0 0-.488.201.695.695 0 0 0 0 .976.695.695 0 0 0 .976 0 .695.695 0 0 0 0-.976.695.695 0 0 0-.488-.2zM12.071 8.49a.69.69 0 0 0-.689.689c0 .379.31.689.689.689a.69.69 0 0 0 .689-.69.683.683 0 0 0-.689-.688zM12.071 5.66a.69.69 0 0 0 0 1.377.69.69 0 0 0 .689-.688.683.683 0 0 0-.689-.688zM12.071 2.831a.69.69 0 0 0-.689.689c0 .379.31.688.689.688a.69.69 0 0 0 .689-.688.687.687 0 0 0-.689-.689zM12.071 11.314a.695.695 0 0 0-.488.2.695.695 0 0 0 0 .977.695.695 0 0 0 .976 0 .695.695 0 0 0 0-.976.695.695 0 0 0-.488-.201zM9.241 12.692a.69.69 0 0 0 .689-.689.69.69 0 0 0-.689-.688.69.69 0 0 0-.688.688c0 .385.31.689.688.689zM6.418 12.692a.69.69 0 0 0 .688-.689.69.69 0 0 0-.688-.688.69.69 0 0 0-.69.688c0 .385.305.689.69.689zM3.588 12.692a.69.69 0 0 0 .689-.689.69.69 0 0 0-1.377 0c0 .385.31.689.688.689zM.758 12.692a.695.695 0 0 0 .488-.2.695.695 0 0 0 0-.977.695.695 0 0 0-.976 0 .695.695 0 0 0 0 .976c.132.126.305.201.488.201zM.758 9.868a.69.69 0 0 0 .689-.689.69.69 0 0 0-.689-.689.69.69 0 0 0-.688.689c0 .379.31.689.688.689zM.758 7.038c.38 0 .689-.31.689-.689s-.31-.688-.689-.688c-.379 0-.688.31-.688.688s.31.69.688.69zM.758 4.208a.69.69 0 0 0 .689-.688.69.69 0 0 0-.689-.689.69.69 0 0 0-.688.689c0 .379.31.688.688.688z"
      })
    })
  }));
};

ToFrontIcon.defaultProps = {
  viewBox: "0 0 26 26",
  xmlns: "http://www.w3.org/2000/svg"
};
var StyledIconGrids = (0, _styledComponents.default)(GridsIcon)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    svg g {\n        fill: ", ";\n    }\n"])), function (_ref) {
  var selected = _ref.selected;
  return selected ? '#fff' : '#002438';
});
var StyledRedoIcon = (0, _styledComponents.default)(RedoIcon)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    svg g path {\n        fill: #002438 !important;\n    }\n"])));
var StyledUndoIcon = (0, _styledComponents.default)(RedoIcon)(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    transform: scale(-1, 1);\n    svg g path {\n        fill: #002438 !important;\n    }\n"])));
var StyledPasteIcon = (0, _styledComponents.default)(PasteIcon)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    svg path {\n        fill: #002438;\n    }\n"])));
var StyledBringToFrontIcon = (0, _styledComponents.default)(BringToFrontIcon)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    display: inline-block;\n    margin-right: 9px;\n    svg path {\n        fill: #848bab;\n    }\n"])));
var StyledToFrontIcon = (0, _styledComponents.default)(ToFrontIcon)(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    display: inline-block;\n    margin-right: 9px;\n    svg path {\n        fill: #848bab;\n    }\n"])));
var StyledBringToBackIcon = (0, _styledComponents.default)(BringToBackIcon)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    display: inline-block;\n    margin-right: 9px;\n    svg path {\n        fill: #848bab;\n    }\n"])));
var StyledToBackIcon = (0, _styledComponents.default)(ToBackIcon)(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n    display: inline-block;\n    margin-right: 9px;\n    svg path {\n        fill: #848bab;\n    }\n"])));

var ToolbarWrapper = _styledComponents.default.div(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n    display: flex;\n    position: sticky;\n    top: 0;\n    width: 100%;\n    height: ", ";\n    min-height: ", ";\n    background-color: #fff;\n    box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);\n    justify-content: center;\n    z-index: 54;\n    overflow-y: ", ";\n    visibility: ", ";\n"])), function (_ref2) {
  var visible = _ref2.visible;
  return visible ? '48px' : '0';
}, function (_ref3) {
  var visible = _ref3.visible;
  return visible ? '48px' : '0';
}, function (_ref4) {
  var visible = _ref4.visible;
  return visible ? 'inherit' : 'hidden';
}, function (_ref5) {
  var visible = _ref5.visible;
  return visible ? 'visible' : 'hidden';
});

var ToolbarContainer = _styledComponents.default.div(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n    display: flex;\n    height: 100%;\n    width: ", "px;\n    border-bottom: 1px solid #ff9a00;\n"])), _template.PAGE_WIDTH);

var ToolbarReplace = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n    height: 100%;\n    .fr-toolbar {\n        border: 0 solid #cccccc !important;\n        background: transparent !important;\n        border-radius: 0 !important;\n        .fr-btn-grp {\n            margin: 0 6px;\n        }\n        .fr-newline {\n            background: transparent !important;\n        }\n        .fr-command.fr-btn svg path {\n            fill: #002438 !important;\n        }\n\n        .fr-command.fr-btn.fr-active svg path {\n            fill: #ff9a00 !important;\n        }\n    }\n    &.disabled {\n        display: none;\n    }\n"])));

var ToolbarOption = _styledComponents.default.div(_templateObject12 || (_templateObject12 = _taggedTemplateLiteral(["\n    display: flex;\n    position: relative;\n    width: 38px;\n    height: 40px;\n    justify-content: center;\n    align-items: center;\n    background-color: transparent;\n    color: #333333;\n    cursor: pointer;\n    margin: 4px 2px;\n    transition: all 0.5s;\n    border-radius: 4px;\n    ", "\n    &:hover {\n        background-color: #ccc;\n    }\n    &.selected {\n        background-color: #ff9a00;\n    }\n"])), function (_ref6) {
  var disabled = _ref6.disabled;
  return disabled && "\n        pointer-events: none;\n        cursor: default;\n\n        svg \n        {\n            opacity: 0.2;\n        }\n    ";
});

var DropdownToggle = (0, _styledComponents.default)(_reactBootstrap.Dropdown.Toggle)(_templateObject13 || (_templateObject13 = _taggedTemplateLiteral(["\n    display: flex;\n    position: relative;\n    width: 38px;\n    height: 40px;\n    justify-content: center;\n    align-items: center;\n    background-color: transparent !important;\n    border-color: transparent !important;\n    cursor: pointer;\n    margin: 4px 2px;\n    transition: all 0.5s;\n    border-radius: 4px;\n    padding: 0;\n    &::after {\n        display: none;\n    }\n\n    ", "\n    &:hover {\n        background-color: #ccc !important;\n    }\n"])), function (_ref7) {
  var disabled = _ref7.disabled;
  return disabled && "\n        pointer-events: none;\n        cursor: default;\n\n        i \n        {\n            opacity: 0.45;\n        }\n    ";
});
var DropdownItem = (0, _styledComponents.default)(_reactBootstrap.Dropdown.Item)(_templateObject14 || (_templateObject14 = _taggedTemplateLiteral(["\n    color: #848bab !important;\n    font-size: 12px;\n    line-height: 14px;\n    padding: 6px 16px 7px 16px;\n"])));

var StyledIcon = _styledComponents.default.i(_templateObject15 || (_templateObject15 = _taggedTemplateLiteral(["\n    font-size: 14px;\n    color: #002438;\n"])));

var PageToolbar = function PageToolbar(_ref8) {
  var copiedWidget = _ref8.copiedWidget,
      currentTemplateType = _ref8.currentTemplateType,
      _ref8$editionLevel = _ref8.editionLevel,
      editionLevel = _ref8$editionLevel === void 0 ? _types.FULL_EDITION_MODE : _ref8$editionLevel,
      isToolbarDisabled = _ref8.isToolbarDisabled,
      nextStatePool = _ref8.nextStatePool,
      pasteWidget = _ref8.pasteWidget,
      prevStatePool = _ref8.prevStatePool,
      selectedPageId = _ref8.selectedPageId,
      selectedWidgetId = _ref8.selectedWidgetId,
      setShowGuides = _ref8.setShowGuides,
      setWidgetOrder = _ref8.setWidgetOrder,
      showGuides = _ref8.showGuides,
      stateIndexSelected = _ref8.stateIndexSelected,
      statePoolLength = _ref8.statePoolLength,
      _ref8$visible = _ref8.visible,
      visible = _ref8$visible === void 0 ? true : _ref8$visible;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      onChangeInPage = _useContext.onChangeInPage;

  var hasSummarySupport = currentTemplateType === _types.MULTIPROPOSAL_TYPE;

  var handleSetShowGuides = function handleSetShowGuides(value) {
    localStorage.setItem('showGuides', value);
    setShowGuides(value);
  };

  var handlePasteElement = function handlePasteElement(options) {
    return pasteWidget(selectedPageId, options, showGuides, null, hasSummarySupport, onChangeInPage);
  };

  var handleRedo = function handleRedo() {
    return nextStatePool(onChangeInPage);
  };

  var handleUndo = function handleUndo() {
    return prevStatePool(onChangeInPage);
  };

  var onChangeItemOrder = function onChangeItemOrder(widgetId, moveType) {
    return setWidgetOrder(selectedPageId, widgetId, moveType, onChangeInPage);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarWrapper, {
    visible: visible,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(ToolbarContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarReplace, {
        id: "froala-content",
        className: isToolbarDisabled && 'disabled'
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.Spacer, {}), editionLevel === _types.FULL_EDITION_MODE && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Dropdown, {
          alignRight: true,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(DropdownToggle, {
            disabled: (0, _isNil.default)(selectedWidgetId),
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledIcon, {
              className: "fas fa-clone"
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Dropdown.Menu, {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
              onClick: function onClick() {
                return onChangeItemOrder(selectedWidgetId, _template.MOVE_STEP_FORWARD);
              },
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledToFrontIcon, {
                width: "12",
                height: "12"
              }), ' ', t('Bring forward')]
            }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
              onClick: function onClick() {
                return onChangeItemOrder(selectedWidgetId, _template.MOVE_STEP_BACK);
              },
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledToBackIcon, {
                width: "12",
                height: "12"
              }), ' ', t('Send backward')]
            }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
              onClick: function onClick() {
                return onChangeItemOrder(selectedWidgetId, _template.MOVE_FORWARD);
              },
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledBringToFrontIcon, {
                width: "12",
                height: "12"
              }), ' ', t('Bring to front')]
            }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
              onClick: function onClick() {
                return onChangeItemOrder(selectedWidgetId, _template.MOVE_BACK);
              },
              children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledBringToBackIcon, {
                width: "12",
                height: "12"
              }), ' ', t('Send to back')]
            })]
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarOption, {
          disabled: (0, _isNil.default)(copiedWidget),
          onClick: function onClick() {
            return handlePasteElement(copiedWidget);
          },
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledPasteIcon, {
            width: "16",
            height: "16"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarOption, {
          disabled: statePoolLength < 2 || stateIndexSelected === 0,
          onClick: handleUndo,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledUndoIcon, {
            width: "16",
            height: "16"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarOption, {
          disabled: stateIndexSelected === statePoolLength - 1,
          onClick: handleRedo,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledRedoIcon, {
            width: "16",
            height: "16"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ToolbarOption, {
          className: showGuides && 'selected',
          onClick: function onClick() {
            return handleSetShowGuides(!showGuides);
          },
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledIconGrids, {
            width: "16",
            height: "16",
            selected: showGuides
          })
        })]
      })]
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  copiedWidget: selectors.getCopiedItem,
  currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
  isToolbarDisabled: selectors.getIsToolbarDisabled,
  selectedPageId: selectors.getSelectedPageId,
  selectedWidgetId: selectors.getSelectedWidgetId,
  showGuides: selectors.getShowGuides,
  stateIndexSelected: selectors.getStatePoolIndex,
  statePoolLength: selectors.getStatePoolDataLength
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    nextStatePool: function nextStatePool(onUpdatePage) {
      return dispatch(actions.nextStatePool(onUpdatePage));
    },
    pasteWidget: function pasteWidget(pageId, options, showGuides, position, hasSummarySupport, onChangeInPage) {
      return dispatch(templateCoreActions.pasteWidget(pageId, options, showGuides, position, hasSummarySupport, onChangeInPage));
    },
    prevStatePool: function prevStatePool(onUpdatePage) {
      return dispatch(actions.prevStatePool(onUpdatePage));
    },
    setCopiedWidget: function setCopiedWidget(data) {
      return dispatch(actions.setCopiedItem(data));
    },
    setShowGuides: function setShowGuides(showGuides) {
      return dispatch(actions.setShowGuides(showGuides));
    },
    setWidgetOrder: function setWidgetOrder(pageId, widgetId, moveType, onChangeInPage) {
      return dispatch(templateCoreActions.setWidgetOrder(pageId, widgetId, moveType, onChangeInPage));
    }
  };
};

PageToolbar.propTypes = {
  copiedWidget: _propTypes.default.object,
  currentTemplateType: _propTypes.default.number,
  editionLevel: _propTypes.default.string,
  handleRedo: _propTypes.default.func,
  handleUndo: _propTypes.default.func,
  isToolbarDisabled: _propTypes.default.bool,
  nextStatePool: _propTypes.default.func,
  onChangeItemOrder: _propTypes.default.func,
  pasteWidget: _propTypes.default.func,
  prevStatePool: _propTypes.default.func,
  selectedPageId: _propTypes.default.string,
  selectedWidgetId: _propTypes.default.string,
  setShowGuides: _propTypes.default.func,
  setWidgetOrder: _propTypes.default.func,
  showGuides: _propTypes.default.bool,
  stateIndexSelected: _propTypes.default.number,
  statePoolLength: _propTypes.default.number,
  visible: _propTypes.default.bool
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PageToolbar);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1BhZ2VUb29sYmFyLmpzIl0sIm5hbWVzIjpbIkJyaW5nVG9CYWNrSWNvbiIsIkJyaW5nVG9Gcm9udEljb24iLCJHcmlkc0ljb24iLCJQYXN0ZUljb24iLCJSZWRvSWNvbiIsIlRvQmFja0ljb24iLCJUb0Zyb250SWNvbiIsIlN0eWxlZEljb25HcmlkcyIsInNlbGVjdGVkIiwiU3R5bGVkUmVkb0ljb24iLCJTdHlsZWRVbmRvSWNvbiIsIlN0eWxlZFBhc3RlSWNvbiIsIlN0eWxlZEJyaW5nVG9Gcm9udEljb24iLCJTdHlsZWRUb0Zyb250SWNvbiIsIlN0eWxlZEJyaW5nVG9CYWNrSWNvbiIsIlN0eWxlZFRvQmFja0ljb24iLCJUb29sYmFyV3JhcHBlciIsInN0eWxlZCIsImRpdiIsInZpc2libGUiLCJUb29sYmFyQ29udGFpbmVyIiwiUEFHRV9XSURUSCIsIlRvb2xiYXJSZXBsYWNlIiwiVG9vbGJhck9wdGlvbiIsImRpc2FibGVkIiwiRHJvcGRvd25Ub2dnbGUiLCJEcm9wZG93biIsIlRvZ2dsZSIsIkRyb3Bkb3duSXRlbSIsIkl0ZW0iLCJTdHlsZWRJY29uIiwiaSIsIlBhZ2VUb29sYmFyIiwiY29waWVkV2lkZ2V0IiwiY3VycmVudFRlbXBsYXRlVHlwZSIsImVkaXRpb25MZXZlbCIsIkZVTExfRURJVElPTl9NT0RFIiwiaXNUb29sYmFyRGlzYWJsZWQiLCJuZXh0U3RhdGVQb29sIiwicGFzdGVXaWRnZXQiLCJwcmV2U3RhdGVQb29sIiwic2VsZWN0ZWRQYWdlSWQiLCJzZWxlY3RlZFdpZGdldElkIiwic2V0U2hvd0d1aWRlcyIsInNldFdpZGdldE9yZGVyIiwic2hvd0d1aWRlcyIsInN0YXRlSW5kZXhTZWxlY3RlZCIsInN0YXRlUG9vbExlbmd0aCIsInQiLCJHZW5lcmFsQ29udGV4dCIsIm9uQ2hhbmdlSW5QYWdlIiwiaGFzU3VtbWFyeVN1cHBvcnQiLCJNVUxUSVBST1BPU0FMX1RZUEUiLCJoYW5kbGVTZXRTaG93R3VpZGVzIiwidmFsdWUiLCJsb2NhbFN0b3JhZ2UiLCJzZXRJdGVtIiwiaGFuZGxlUGFzdGVFbGVtZW50Iiwib3B0aW9ucyIsImhhbmRsZVJlZG8iLCJoYW5kbGVVbmRvIiwib25DaGFuZ2VJdGVtT3JkZXIiLCJ3aWRnZXRJZCIsIm1vdmVUeXBlIiwiTU9WRV9TVEVQX0ZPUldBUkQiLCJNT1ZFX1NURVBfQkFDSyIsIk1PVkVfRk9SV0FSRCIsIk1PVkVfQkFDSyIsIm1hcFN0YXRlVG9Qcm9wcyIsInNlbGVjdG9ycyIsImdldENvcGllZEl0ZW0iLCJ0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMiLCJnZXRDdXJyZW50VGVtcGxhdGVUeXBlIiwiZ2V0SXNUb29sYmFyRGlzYWJsZWQiLCJnZXRTZWxlY3RlZFBhZ2VJZCIsImdldFNlbGVjdGVkV2lkZ2V0SWQiLCJnZXRTaG93R3VpZGVzIiwiZ2V0U3RhdGVQb29sSW5kZXgiLCJnZXRTdGF0ZVBvb2xEYXRhTGVuZ3RoIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJvblVwZGF0ZVBhZ2UiLCJhY3Rpb25zIiwicGFnZUlkIiwicG9zaXRpb24iLCJ0ZW1wbGF0ZUNvcmVBY3Rpb25zIiwic2V0Q29waWVkV2lkZ2V0IiwiZGF0YSIsInNldENvcGllZEl0ZW0iLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJudW1iZXIiLCJzdHJpbmciLCJmdW5jIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBRUE7O0FBRkE7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBT0E7O0FBRUE7O0FBVUE7O0FBQ0E7O0FBRUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBWjJCQSxlLFlBQUFBLGU7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsZTs7Ozs7SUFDQUMsZ0IsWUFBQUEsZ0I7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsZ0I7Ozs7O0lBQ0FDLFMsWUFBQUEsUzs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLFM7Ozs7O0lBQ0FDLFMsWUFBQUEsUzs7Ozs7Ozs7O0FBQUFBLFM7Ozs7Ozs7OztJQUNBQyxRLFlBQUFBLFE7Ozs7Ozs7OztBQUFBQSxROzs7Ozs7SUFDQUMsVSxZQUFBQSxVOzs7Ozs7Ozs7Ozs7QUFBQUEsVTs7Ozs7SUFDQUMsVyxZQUFBQSxXOzs7Ozs7Ozs7Ozs7QUFBQUEsVzs7OztBQVEzQixJQUFNQyxlQUFlLEdBQUcsK0JBQU9MLFNBQVAsQ0FBSCxpSEFFTDtBQUFBLE1BQUdNLFFBQUgsUUFBR0EsUUFBSDtBQUFBLFNBQW1CQSxRQUFRLEdBQUcsTUFBSCxHQUFZLFNBQXZDO0FBQUEsQ0FGSyxDQUFyQjtBQU1BLElBQU1DLGNBQWMsR0FBRywrQkFBT0wsUUFBUCxDQUFILHFJQUFwQjtBQU1BLElBQU1NLGNBQWMsR0FBRywrQkFBT04sUUFBUCxDQUFILG1LQUFwQjtBQU9BLElBQU1PLGVBQWUsR0FBRywrQkFBT1IsU0FBUCxDQUFILHdIQUFyQjtBQU1BLElBQU1TLHNCQUFzQixHQUFHLCtCQUFPWCxnQkFBUCxDQUFILDRLQUE1QjtBQVFBLElBQU1ZLGlCQUFpQixHQUFHLCtCQUFPUCxXQUFQLENBQUgsNEtBQXZCO0FBUUEsSUFBTVEscUJBQXFCLEdBQUcsK0JBQU9kLGVBQVAsQ0FBSCw0S0FBM0I7QUFRQSxJQUFNZSxnQkFBZ0IsR0FBRywrQkFBT1YsVUFBUCxDQUFILDRLQUF0Qjs7QUFRQSxJQUFNVyxjQUFjLEdBQUdDLDBCQUFPQyxHQUFWLHFYQUtOO0FBQUEsTUFBR0MsT0FBSCxTQUFHQSxPQUFIO0FBQUEsU0FBa0JBLE9BQU8sR0FBRyxNQUFILEdBQVksR0FBckM7QUFBQSxDQUxNLEVBTUY7QUFBQSxNQUFHQSxPQUFILFNBQUdBLE9BQUg7QUFBQSxTQUFrQkEsT0FBTyxHQUFHLE1BQUgsR0FBWSxHQUFyQztBQUFBLENBTkUsRUFXRjtBQUFBLE1BQUdBLE9BQUgsU0FBR0EsT0FBSDtBQUFBLFNBQWtCQSxPQUFPLEdBQUcsU0FBSCxHQUFlLFFBQXhDO0FBQUEsQ0FYRSxFQVlGO0FBQUEsTUFBR0EsT0FBSCxTQUFHQSxPQUFIO0FBQUEsU0FBa0JBLE9BQU8sR0FBRyxTQUFILEdBQWUsUUFBeEM7QUFBQSxDQVpFLENBQXBCOztBQWVBLElBQU1DLGdCQUFnQixHQUFHSCwwQkFBT0MsR0FBViw4S0FHVEcsb0JBSFMsQ0FBdEI7O0FBT0EsSUFBTUMsY0FBYyxHQUFHTCwwQkFBT0MsR0FBVix3bkJBQXBCOztBQXlCQSxJQUFNSyxhQUFhLEdBQUdOLDBCQUFPQyxHQUFWLDZlQWFiO0FBQUEsTUFBR00sUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FDRUEsUUFBUSxzSUFEVjtBQUFBLENBYmEsQ0FBbkI7O0FBZ0NBLElBQU1DLGNBQWMsR0FBRywrQkFBT0MseUJBQVNDLE1BQWhCLENBQUgsOGhCQWtCZDtBQUFBLE1BQUdILFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQ0VBLFFBQVEscUlBRFY7QUFBQSxDQWxCYyxDQUFwQjtBQWtDQSxJQUFNSSxZQUFZLEdBQUcsK0JBQU9GLHlCQUFTRyxJQUFoQixDQUFILDBMQUFsQjs7QUFPQSxJQUFNQyxVQUFVLEdBQUdiLDBCQUFPYyxDQUFWLHNIQUFoQjs7QUFLQSxJQUFNQyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxRQWdCZDtBQUFBLE1BZkZDLFlBZUUsU0FmRkEsWUFlRTtBQUFBLE1BZEZDLG1CQWNFLFNBZEZBLG1CQWNFO0FBQUEsaUNBYkZDLFlBYUU7QUFBQSxNQWJGQSxZQWFFLG1DQWJhQyx3QkFhYjtBQUFBLE1BWkZDLGlCQVlFLFNBWkZBLGlCQVlFO0FBQUEsTUFYRkMsYUFXRSxTQVhGQSxhQVdFO0FBQUEsTUFWRkMsV0FVRSxTQVZGQSxXQVVFO0FBQUEsTUFURkMsYUFTRSxTQVRGQSxhQVNFO0FBQUEsTUFSRkMsY0FRRSxTQVJGQSxjQVFFO0FBQUEsTUFQRkMsZ0JBT0UsU0FQRkEsZ0JBT0U7QUFBQSxNQU5GQyxhQU1FLFNBTkZBLGFBTUU7QUFBQSxNQUxGQyxjQUtFLFNBTEZBLGNBS0U7QUFBQSxNQUpGQyxVQUlFLFNBSkZBLFVBSUU7QUFBQSxNQUhGQyxrQkFHRSxTQUhGQSxrQkFHRTtBQUFBLE1BRkZDLGVBRUUsU0FGRkEsZUFFRTtBQUFBLDRCQURGNUIsT0FDRTtBQUFBLE1BREZBLE9BQ0UsOEJBRFEsSUFDUjs7QUFDRix3QkFBYyxtQ0FBZDtBQUFBLE1BQVE2QixDQUFSLG1CQUFRQSxDQUFSOztBQUNBLG9CQUEyQix1QkFBV0Msd0JBQVgsQ0FBM0I7QUFBQSxNQUFRQyxjQUFSLGVBQVFBLGNBQVI7O0FBQ0EsTUFBTUMsaUJBQWlCLEdBQUdqQixtQkFBbUIsS0FBS2tCLHlCQUFsRDs7QUFFQSxNQUFNQyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLEtBQUQsRUFBVztBQUNuQ0MsSUFBQUEsWUFBWSxDQUFDQyxPQUFiLENBQXFCLFlBQXJCLEVBQW1DRixLQUFuQztBQUNBWCxJQUFBQSxhQUFhLENBQUNXLEtBQUQsQ0FBYjtBQUNILEdBSEQ7O0FBS0EsTUFBTUcsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxPQUFEO0FBQUEsV0FDdkJuQixXQUFXLENBQ1BFLGNBRE8sRUFFUGlCLE9BRk8sRUFHUGIsVUFITyxFQUlQLElBSk8sRUFLUE0saUJBTE8sRUFNUEQsY0FOTyxDQURZO0FBQUEsR0FBM0I7O0FBVUEsTUFBTVMsVUFBVSxHQUFHLFNBQWJBLFVBQWE7QUFBQSxXQUFNckIsYUFBYSxDQUFDWSxjQUFELENBQW5CO0FBQUEsR0FBbkI7O0FBQ0EsTUFBTVUsVUFBVSxHQUFHLFNBQWJBLFVBQWE7QUFBQSxXQUFNcEIsYUFBYSxDQUFDVSxjQUFELENBQW5CO0FBQUEsR0FBbkI7O0FBQ0EsTUFBTVcsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDQyxRQUFELEVBQVdDLFFBQVg7QUFBQSxXQUN0Qm5CLGNBQWMsQ0FBQ0gsY0FBRCxFQUFpQnFCLFFBQWpCLEVBQTJCQyxRQUEzQixFQUFxQ2IsY0FBckMsQ0FEUTtBQUFBLEdBQTFCOztBQUdBLHNCQUNJLHFCQUFDLGNBQUQ7QUFBZ0IsSUFBQSxPQUFPLEVBQUUvQixPQUF6QjtBQUFBLDJCQUNJLHNCQUFDLGdCQUFEO0FBQUEsOEJBQ0kscUJBQUMsY0FBRDtBQUNJLFFBQUEsRUFBRSxFQUFDLGdCQURQO0FBRUksUUFBQSxTQUFTLEVBQUVrQixpQkFBaUIsSUFBSTtBQUZwQyxRQURKLGVBS0kscUJBQUMsY0FBRCxLQUxKLEVBTUtGLFlBQVksS0FBS0Msd0JBQWpCLGlCQUNHO0FBQUEsZ0NBQ0ksc0JBQUMsd0JBQUQ7QUFBVSxVQUFBLFVBQVUsTUFBcEI7QUFBQSxrQ0FDSSxxQkFBQyxjQUFEO0FBQWdCLFlBQUEsUUFBUSxFQUFFLG9CQUFNTSxnQkFBTixDQUExQjtBQUFBLG1DQUNJLHFCQUFDLFVBQUQ7QUFBWSxjQUFBLFNBQVMsRUFBQztBQUF0QjtBQURKLFlBREosZUFJSSxzQkFBQyx3QkFBRCxDQUFVLElBQVY7QUFBQSxvQ0FDSSxzQkFBQyxZQUFEO0FBQ0ksY0FBQSxPQUFPLEVBQUU7QUFBQSx1QkFDTG1CLGlCQUFpQixDQUNibkIsZ0JBRGEsRUFFYnNCLDJCQUZhLENBRFo7QUFBQSxlQURiO0FBQUEsc0NBUUkscUJBQUMsaUJBQUQ7QUFBbUIsZ0JBQUEsS0FBSyxFQUFDLElBQXpCO0FBQThCLGdCQUFBLE1BQU0sRUFBQztBQUFyQyxnQkFSSixFQVFpRCxHQVJqRCxFQVNLaEIsQ0FBQyxDQUFDLGVBQUQsQ0FUTjtBQUFBLGNBREosZUFZSSxzQkFBQyxZQUFEO0FBQ0ksY0FBQSxPQUFPLEVBQUU7QUFBQSx1QkFDTGEsaUJBQWlCLENBQ2JuQixnQkFEYSxFQUVidUIsd0JBRmEsQ0FEWjtBQUFBLGVBRGI7QUFBQSxzQ0FRSSxxQkFBQyxnQkFBRDtBQUFrQixnQkFBQSxLQUFLLEVBQUMsSUFBeEI7QUFBNkIsZ0JBQUEsTUFBTSxFQUFDO0FBQXBDLGdCQVJKLEVBUWdELEdBUmhELEVBU0tqQixDQUFDLENBQUMsZUFBRCxDQVROO0FBQUEsY0FaSixlQXVCSSxzQkFBQyxZQUFEO0FBQ0ksY0FBQSxPQUFPLEVBQUU7QUFBQSx1QkFDTGEsaUJBQWlCLENBQ2JuQixnQkFEYSxFQUVid0Isc0JBRmEsQ0FEWjtBQUFBLGVBRGI7QUFBQSxzQ0FRSSxxQkFBQyxzQkFBRDtBQUNJLGdCQUFBLEtBQUssRUFBQyxJQURWO0FBRUksZ0JBQUEsTUFBTSxFQUFDO0FBRlgsZ0JBUkosRUFXTyxHQVhQLEVBWUtsQixDQUFDLENBQUMsZ0JBQUQsQ0FaTjtBQUFBLGNBdkJKLGVBcUNJLHNCQUFDLFlBQUQ7QUFDSSxjQUFBLE9BQU8sRUFBRTtBQUFBLHVCQUNMYSxpQkFBaUIsQ0FDYm5CLGdCQURhLEVBRWJ5QixtQkFGYSxDQURaO0FBQUEsZUFEYjtBQUFBLHNDQVFJLHFCQUFDLHFCQUFEO0FBQ0ksZ0JBQUEsS0FBSyxFQUFDLElBRFY7QUFFSSxnQkFBQSxNQUFNLEVBQUM7QUFGWCxnQkFSSixFQVdPLEdBWFAsRUFZS25CLENBQUMsQ0FBQyxjQUFELENBWk47QUFBQSxjQXJDSjtBQUFBLFlBSko7QUFBQSxVQURKLGVBMERJLHFCQUFDLGFBQUQ7QUFDSSxVQUFBLFFBQVEsRUFBRSxvQkFBTWYsWUFBTixDQURkO0FBRUksVUFBQSxPQUFPLEVBQUU7QUFBQSxtQkFBTXdCLGtCQUFrQixDQUFDeEIsWUFBRCxDQUF4QjtBQUFBLFdBRmI7QUFBQSxpQ0FJSSxxQkFBQyxlQUFEO0FBQWlCLFlBQUEsS0FBSyxFQUFDLElBQXZCO0FBQTRCLFlBQUEsTUFBTSxFQUFDO0FBQW5DO0FBSkosVUExREosZUFnRUkscUJBQUMsYUFBRDtBQUNJLFVBQUEsUUFBUSxFQUNKYyxlQUFlLEdBQUcsQ0FBbEIsSUFBdUJELGtCQUFrQixLQUFLLENBRnREO0FBSUksVUFBQSxPQUFPLEVBQUVjLFVBSmI7QUFBQSxpQ0FNSSxxQkFBQyxjQUFEO0FBQWdCLFlBQUEsS0FBSyxFQUFDLElBQXRCO0FBQTJCLFlBQUEsTUFBTSxFQUFDO0FBQWxDO0FBTkosVUFoRUosZUF3RUkscUJBQUMsYUFBRDtBQUNJLFVBQUEsUUFBUSxFQUNKZCxrQkFBa0IsS0FBS0MsZUFBZSxHQUFHLENBRmpEO0FBSUksVUFBQSxPQUFPLEVBQUVZLFVBSmI7QUFBQSxpQ0FNSSxxQkFBQyxjQUFEO0FBQWdCLFlBQUEsS0FBSyxFQUFDLElBQXRCO0FBQTJCLFlBQUEsTUFBTSxFQUFDO0FBQWxDO0FBTkosVUF4RUosZUFnRkkscUJBQUMsYUFBRDtBQUNJLFVBQUEsU0FBUyxFQUFFZCxVQUFVLElBQUksVUFEN0I7QUFFSSxVQUFBLE9BQU8sRUFBRTtBQUFBLG1CQUFNUSxtQkFBbUIsQ0FBQyxDQUFDUixVQUFGLENBQXpCO0FBQUEsV0FGYjtBQUFBLGlDQUlJLHFCQUFDLGVBQUQ7QUFDSSxZQUFBLEtBQUssRUFBQyxJQURWO0FBRUksWUFBQSxNQUFNLEVBQUMsSUFGWDtBQUdJLFlBQUEsUUFBUSxFQUFFQTtBQUhkO0FBSkosVUFoRko7QUFBQSxRQVBSO0FBQUE7QUFESixJQURKO0FBd0dILENBakpEOztBQW1KQSxJQUFNdUIsZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q25DLEVBQUFBLFlBQVksRUFBRW9DLFNBQVMsQ0FBQ0MsYUFEcUI7QUFFN0NwQyxFQUFBQSxtQkFBbUIsRUFBRXFDLHFCQUFxQixDQUFDQyxzQkFGRTtBQUc3Q25DLEVBQUFBLGlCQUFpQixFQUFFZ0MsU0FBUyxDQUFDSSxvQkFIZ0I7QUFJN0NoQyxFQUFBQSxjQUFjLEVBQUU0QixTQUFTLENBQUNLLGlCQUptQjtBQUs3Q2hDLEVBQUFBLGdCQUFnQixFQUFFMkIsU0FBUyxDQUFDTSxtQkFMaUI7QUFNN0M5QixFQUFBQSxVQUFVLEVBQUV3QixTQUFTLENBQUNPLGFBTnVCO0FBTzdDOUIsRUFBQUEsa0JBQWtCLEVBQUV1QixTQUFTLENBQUNRLGlCQVBlO0FBUTdDOUIsRUFBQUEsZUFBZSxFQUFFc0IsU0FBUyxDQUFDUztBQVJrQixDQUF6QixDQUF4Qjs7QUFXQSxJQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUFlO0FBQ3RDMUMsSUFBQUEsYUFBYSxFQUFFLHVCQUFDMkMsWUFBRDtBQUFBLGFBQ1hELFFBQVEsQ0FBQ0UsT0FBTyxDQUFDNUMsYUFBUixDQUFzQjJDLFlBQXRCLENBQUQsQ0FERztBQUFBLEtBRHVCO0FBR3RDMUMsSUFBQUEsV0FBVyxFQUFFLHFCQUNUNEMsTUFEUyxFQUVUekIsT0FGUyxFQUdUYixVQUhTLEVBSVR1QyxRQUpTLEVBS1RqQyxpQkFMUyxFQU1URCxjQU5TO0FBQUEsYUFRVDhCLFFBQVEsQ0FDSkssbUJBQW1CLENBQUM5QyxXQUFwQixDQUNJNEMsTUFESixFQUVJekIsT0FGSixFQUdJYixVQUhKLEVBSUl1QyxRQUpKLEVBS0lqQyxpQkFMSixFQU1JRCxjQU5KLENBREksQ0FSQztBQUFBLEtBSHlCO0FBcUJ0Q1YsSUFBQUEsYUFBYSxFQUFFLHVCQUFDeUMsWUFBRDtBQUFBLGFBQ1hELFFBQVEsQ0FBQ0UsT0FBTyxDQUFDMUMsYUFBUixDQUFzQnlDLFlBQXRCLENBQUQsQ0FERztBQUFBLEtBckJ1QjtBQXVCdENLLElBQUFBLGVBQWUsRUFBRSx5QkFBQ0MsSUFBRDtBQUFBLGFBQVVQLFFBQVEsQ0FBQ0UsT0FBTyxDQUFDTSxhQUFSLENBQXNCRCxJQUF0QixDQUFELENBQWxCO0FBQUEsS0F2QnFCO0FBd0J0QzVDLElBQUFBLGFBQWEsRUFBRSx1QkFBQ0UsVUFBRDtBQUFBLGFBQWdCbUMsUUFBUSxDQUFDRSxPQUFPLENBQUN2QyxhQUFSLENBQXNCRSxVQUF0QixDQUFELENBQXhCO0FBQUEsS0F4QnVCO0FBeUJ0Q0QsSUFBQUEsY0FBYyxFQUFFLHdCQUFDdUMsTUFBRCxFQUFTckIsUUFBVCxFQUFtQkMsUUFBbkIsRUFBNkJiLGNBQTdCO0FBQUEsYUFDWjhCLFFBQVEsQ0FDSkssbUJBQW1CLENBQUN6QyxjQUFwQixDQUNJdUMsTUFESixFQUVJckIsUUFGSixFQUdJQyxRQUhKLEVBSUliLGNBSkosQ0FESSxDQURJO0FBQUE7QUF6QnNCLEdBQWY7QUFBQSxDQUEzQjs7QUFvQ0FsQixXQUFXLENBQUN5RCxTQUFaLEdBQXdCO0FBQ3BCeEQsRUFBQUEsWUFBWSxFQUFFeUQsbUJBQVVDLE1BREo7QUFFcEJ6RCxFQUFBQSxtQkFBbUIsRUFBRXdELG1CQUFVRSxNQUZYO0FBR3BCekQsRUFBQUEsWUFBWSxFQUFFdUQsbUJBQVVHLE1BSEo7QUFJcEJsQyxFQUFBQSxVQUFVLEVBQUUrQixtQkFBVUksSUFKRjtBQUtwQmxDLEVBQUFBLFVBQVUsRUFBRThCLG1CQUFVSSxJQUxGO0FBTXBCekQsRUFBQUEsaUJBQWlCLEVBQUVxRCxtQkFBVUssSUFOVDtBQU9wQnpELEVBQUFBLGFBQWEsRUFBRW9ELG1CQUFVSSxJQVBMO0FBUXBCakMsRUFBQUEsaUJBQWlCLEVBQUU2QixtQkFBVUksSUFSVDtBQVNwQnZELEVBQUFBLFdBQVcsRUFBRW1ELG1CQUFVSSxJQVRIO0FBVXBCdEQsRUFBQUEsYUFBYSxFQUFFa0QsbUJBQVVJLElBVkw7QUFXcEJyRCxFQUFBQSxjQUFjLEVBQUVpRCxtQkFBVUcsTUFYTjtBQVlwQm5ELEVBQUFBLGdCQUFnQixFQUFFZ0QsbUJBQVVHLE1BWlI7QUFhcEJsRCxFQUFBQSxhQUFhLEVBQUUrQyxtQkFBVUksSUFiTDtBQWNwQmxELEVBQUFBLGNBQWMsRUFBRThDLG1CQUFVSSxJQWROO0FBZXBCakQsRUFBQUEsVUFBVSxFQUFFNkMsbUJBQVVLLElBZkY7QUFnQnBCakQsRUFBQUEsa0JBQWtCLEVBQUU0QyxtQkFBVUUsTUFoQlY7QUFpQnBCN0MsRUFBQUEsZUFBZSxFQUFFMkMsbUJBQVVFLE1BakJQO0FBa0JwQnpFLEVBQUFBLE9BQU8sRUFBRXVFLG1CQUFVSztBQWxCQyxDQUF4Qjs7ZUFvQmUseUJBQVEzQixlQUFSLEVBQXlCVyxrQkFBekIsRUFBNkMvQyxXQUE3QyxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ29udGV4dCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IERyb3Bkb3duIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHsgY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IFNwYWNlciB9IGZyb20gJ0Bjb21wb25lbnRzL3N0eWxlZCc7XG5cbmltcG9ydCB7XG4gICAgTU9WRV9CQUNLLFxuICAgIE1PVkVfRk9SV0FSRCxcbiAgICBNT1ZFX1NURVBfQkFDSyxcbiAgICBNT1ZFX1NURVBfRk9SV0FSRCxcbiAgICBQQUdFX1dJRFRILFxufSBmcm9tICdAY29uc3RhbnRzL3RlbXBsYXRlJztcbmltcG9ydCB7IEZVTExfRURJVElPTl9NT0RFLCBNVUxUSVBST1BPU0FMX1RZUEUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcblxuaW1wb3J0IHsgR2VuZXJhbENvbnRleHQgfSBmcm9tICdAaGVscGVycy9jb250ZXh0cyc7XG5cbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIEJyaW5nVG9CYWNrSWNvbiB9IGZyb20gJ0ByZXMvaWNvbnMvYnJpbmdUb0JhY2suc3ZnJztcbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIEJyaW5nVG9Gcm9udEljb24gfSBmcm9tICdAcmVzL2ljb25zL2JyaW5nVG9Gcm9udC5zdmcnO1xuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgR3JpZHNJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9ncmlkcy5zdmcnO1xuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgUGFzdGVJY29uIH0gZnJvbSAnQHJlcy9pY29ucy9wYXN0ZS5zdmcnO1xuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgUmVkb0ljb24gfSBmcm9tICdAcmVzL2ljb25zL3JlZG8uc3ZnJztcbmltcG9ydCB7IFJlYWN0Q29tcG9uZW50IGFzIFRvQmFja0ljb24gfSBmcm9tICdAcmVzL2ljb25zL3RvQmFjay5zdmcnO1xuaW1wb3J0IHsgUmVhY3RDb21wb25lbnQgYXMgVG9Gcm9udEljb24gfSBmcm9tICdAcmVzL2ljb25zL3RvRnJvbnQuc3ZnJztcblxuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlQWN0aW9ucyBmcm9tICdAdGVtcGxhdGVDb3JlL2FjdGlvbnMnO1xuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlU2VsZWN0b3JzIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvc2VsZWN0b3JzJztcblxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuLi9hY3Rpb25zJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi9zZWxlY3RvcnMnO1xuXG5jb25zdCBTdHlsZWRJY29uR3JpZHMgPSBzdHlsZWQoR3JpZHNJY29uKWBcbiAgICBzdmcgZyB7XG4gICAgICAgIGZpbGw6ICR7KHsgc2VsZWN0ZWQgfSkgPT4gKHNlbGVjdGVkID8gJyNmZmYnIDogJyMwMDI0MzgnKX07XG4gICAgfVxuYDtcblxuY29uc3QgU3R5bGVkUmVkb0ljb24gPSBzdHlsZWQoUmVkb0ljb24pYFxuICAgIHN2ZyBnIHBhdGgge1xuICAgICAgICBmaWxsOiAjMDAyNDM4ICFpbXBvcnRhbnQ7XG4gICAgfVxuYDtcblxuY29uc3QgU3R5bGVkVW5kb0ljb24gPSBzdHlsZWQoUmVkb0ljb24pYFxuICAgIHRyYW5zZm9ybTogc2NhbGUoLTEsIDEpO1xuICAgIHN2ZyBnIHBhdGgge1xuICAgICAgICBmaWxsOiAjMDAyNDM4ICFpbXBvcnRhbnQ7XG4gICAgfVxuYDtcblxuY29uc3QgU3R5bGVkUGFzdGVJY29uID0gc3R5bGVkKFBhc3RlSWNvbilgXG4gICAgc3ZnIHBhdGgge1xuICAgICAgICBmaWxsOiAjMDAyNDM4O1xuICAgIH1cbmA7XG5cbmNvbnN0IFN0eWxlZEJyaW5nVG9Gcm9udEljb24gPSBzdHlsZWQoQnJpbmdUb0Zyb250SWNvbilgXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgIHN2ZyBwYXRoIHtcbiAgICAgICAgZmlsbDogIzg0OGJhYjtcbiAgICB9XG5gO1xuXG5jb25zdCBTdHlsZWRUb0Zyb250SWNvbiA9IHN0eWxlZChUb0Zyb250SWNvbilgXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgIHN2ZyBwYXRoIHtcbiAgICAgICAgZmlsbDogIzg0OGJhYjtcbiAgICB9XG5gO1xuXG5jb25zdCBTdHlsZWRCcmluZ1RvQmFja0ljb24gPSBzdHlsZWQoQnJpbmdUb0JhY2tJY29uKWBcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWFyZ2luLXJpZ2h0OiA5cHg7XG4gICAgc3ZnIHBhdGgge1xuICAgICAgICBmaWxsOiAjODQ4YmFiO1xuICAgIH1cbmA7XG5cbmNvbnN0IFN0eWxlZFRvQmFja0ljb24gPSBzdHlsZWQoVG9CYWNrSWNvbilgXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgIHN2ZyBwYXRoIHtcbiAgICAgICAgZmlsbDogIzg0OGJhYjtcbiAgICB9XG5gO1xuXG5jb25zdCBUb29sYmFyV3JhcHBlciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6ICR7KHsgdmlzaWJsZSB9KSA9PiAodmlzaWJsZSA/ICc0OHB4JyA6ICcwJyl9O1xuICAgIG1pbi1oZWlnaHQ6ICR7KHsgdmlzaWJsZSB9KSA9PiAodmlzaWJsZSA/ICc0OHB4JyA6ICcwJyl9O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm94LXNoYWRvdzogMjBweCAycHggMTdweCAwIHJnYmEoMTIwLCAxMTIsIDE3OCwgMC4wOCk7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgei1pbmRleDogNTQ7XG4gICAgb3ZlcmZsb3cteTogJHsoeyB2aXNpYmxlIH0pID0+ICh2aXNpYmxlID8gJ2luaGVyaXQnIDogJ2hpZGRlbicpfTtcbiAgICB2aXNpYmlsaXR5OiAkeyh7IHZpc2libGUgfSkgPT4gKHZpc2libGUgPyAndmlzaWJsZScgOiAnaGlkZGVuJyl9O1xuYDtcblxuY29uc3QgVG9vbGJhckNvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6ICR7UEFHRV9XSURUSH1weDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2ZmOWEwMDtcbmA7XG5cbmNvbnN0IFRvb2xiYXJSZXBsYWNlID0gc3R5bGVkLmRpdmBcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgLmZyLXRvb2xiYXIge1xuICAgICAgICBib3JkZXI6IDAgc29saWQgI2NjY2NjYyAhaW1wb3J0YW50O1xuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIC5mci1idG4tZ3JwIHtcbiAgICAgICAgICAgIG1hcmdpbjogMCA2cHg7XG4gICAgICAgIH1cbiAgICAgICAgLmZyLW5ld2xpbmUge1xuICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgICAgICAuZnItY29tbWFuZC5mci1idG4gc3ZnIHBhdGgge1xuICAgICAgICAgICAgZmlsbDogIzAwMjQzOCAhaW1wb3J0YW50O1xuICAgICAgICB9XG5cbiAgICAgICAgLmZyLWNvbW1hbmQuZnItYnRuLmZyLWFjdGl2ZSBzdmcgcGF0aCB7XG4gICAgICAgICAgICBmaWxsOiAjZmY5YTAwICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgJi5kaXNhYmxlZCB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuYDtcblxuY29uc3QgVG9vbGJhck9wdGlvbiA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDM4cHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgY29sb3I6ICMzMzMzMzM7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbjogNHB4IDJweDtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC41cztcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgJHsoeyBkaXNhYmxlZCB9KSA9PlxuICAgICAgICBkaXNhYmxlZCAmJlxuICAgICAgICBgXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICAgICAgICBjdXJzb3I6IGRlZmF1bHQ7XG5cbiAgICAgICAgc3ZnIFxuICAgICAgICB7XG4gICAgICAgICAgICBvcGFjaXR5OiAwLjI7XG4gICAgICAgIH1cbiAgICBgfVxuICAgICY6aG92ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xuICAgIH1cbiAgICAmLnNlbGVjdGVkIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmOWEwMDtcbiAgICB9XG5gO1xuXG5jb25zdCBEcm9wZG93blRvZ2dsZSA9IHN0eWxlZChEcm9wZG93bi5Ub2dnbGUpYFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAzOHB4O1xuICAgIGhlaWdodDogNDBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW46IDRweCAycHg7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNXM7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgICR7KHsgZGlzYWJsZWQgfSkgPT5cbiAgICAgICAgZGlzYWJsZWQgJiZcbiAgICAgICAgYFxuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgICAgICAgY3Vyc29yOiBkZWZhdWx0O1xuXG4gICAgICAgIGkgXG4gICAgICAgIHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNDU7XG4gICAgICAgIH1cbiAgICBgfVxuICAgICY6aG92ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjICFpbXBvcnRhbnQ7XG4gICAgfVxuYDtcblxuY29uc3QgRHJvcGRvd25JdGVtID0gc3R5bGVkKERyb3Bkb3duLkl0ZW0pYFxuICAgIGNvbG9yOiAjODQ4YmFiICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIHBhZGRpbmc6IDZweCAxNnB4IDdweCAxNnB4O1xuYDtcblxuY29uc3QgU3R5bGVkSWNvbiA9IHN0eWxlZC5pYFxuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogIzAwMjQzODtcbmA7XG5cbmNvbnN0IFBhZ2VUb29sYmFyID0gKHtcbiAgICBjb3BpZWRXaWRnZXQsXG4gICAgY3VycmVudFRlbXBsYXRlVHlwZSxcbiAgICBlZGl0aW9uTGV2ZWwgPSBGVUxMX0VESVRJT05fTU9ERSxcbiAgICBpc1Rvb2xiYXJEaXNhYmxlZCxcbiAgICBuZXh0U3RhdGVQb29sLFxuICAgIHBhc3RlV2lkZ2V0LFxuICAgIHByZXZTdGF0ZVBvb2wsXG4gICAgc2VsZWN0ZWRQYWdlSWQsXG4gICAgc2VsZWN0ZWRXaWRnZXRJZCxcbiAgICBzZXRTaG93R3VpZGVzLFxuICAgIHNldFdpZGdldE9yZGVyLFxuICAgIHNob3dHdWlkZXMsXG4gICAgc3RhdGVJbmRleFNlbGVjdGVkLFxuICAgIHN0YXRlUG9vbExlbmd0aCxcbiAgICB2aXNpYmxlID0gdHJ1ZSxcbn0pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3QgeyBvbkNoYW5nZUluUGFnZSB9ID0gdXNlQ29udGV4dChHZW5lcmFsQ29udGV4dCk7XG4gICAgY29uc3QgaGFzU3VtbWFyeVN1cHBvcnQgPSBjdXJyZW50VGVtcGxhdGVUeXBlID09PSBNVUxUSVBST1BPU0FMX1RZUEU7XG5cbiAgICBjb25zdCBoYW5kbGVTZXRTaG93R3VpZGVzID0gKHZhbHVlKSA9PiB7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdzaG93R3VpZGVzJywgdmFsdWUpO1xuICAgICAgICBzZXRTaG93R3VpZGVzKHZhbHVlKTtcbiAgICB9O1xuXG4gICAgY29uc3QgaGFuZGxlUGFzdGVFbGVtZW50ID0gKG9wdGlvbnMpID0+XG4gICAgICAgIHBhc3RlV2lkZ2V0KFxuICAgICAgICAgICAgc2VsZWN0ZWRQYWdlSWQsXG4gICAgICAgICAgICBvcHRpb25zLFxuICAgICAgICAgICAgc2hvd0d1aWRlcyxcbiAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcbiAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXG4gICAgICAgICk7XG5cbiAgICBjb25zdCBoYW5kbGVSZWRvID0gKCkgPT4gbmV4dFN0YXRlUG9vbChvbkNoYW5nZUluUGFnZSk7XG4gICAgY29uc3QgaGFuZGxlVW5kbyA9ICgpID0+IHByZXZTdGF0ZVBvb2wob25DaGFuZ2VJblBhZ2UpO1xuICAgIGNvbnN0IG9uQ2hhbmdlSXRlbU9yZGVyID0gKHdpZGdldElkLCBtb3ZlVHlwZSkgPT5cbiAgICAgICAgc2V0V2lkZ2V0T3JkZXIoc2VsZWN0ZWRQYWdlSWQsIHdpZGdldElkLCBtb3ZlVHlwZSwgb25DaGFuZ2VJblBhZ2UpO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFRvb2xiYXJXcmFwcGVyIHZpc2libGU9e3Zpc2libGV9PlxuICAgICAgICAgICAgPFRvb2xiYXJDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPFRvb2xiYXJSZXBsYWNlXG4gICAgICAgICAgICAgICAgICAgIGlkPVwiZnJvYWxhLWNvbnRlbnRcIlxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2lzVG9vbGJhckRpc2FibGVkICYmICdkaXNhYmxlZCd9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8U3BhY2VyIC8+XG4gICAgICAgICAgICAgICAge2VkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duIGFsaWduUmlnaHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duVG9nZ2xlIGRpc2FibGVkPXtpc05pbChzZWxlY3RlZFdpZGdldElkKX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRJY29uIGNsYXNzTmFtZT1cImZhcyBmYS1jbG9uZVwiIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93blRvZ2dsZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd24uTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duSXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUl0ZW1PcmRlcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXRJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTU9WRV9TVEVQX0ZPUldBUkRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRUb0Zyb250SWNvbiB3aWR0aD1cIjEyXCIgaGVpZ2h0PVwiMTJcIiAvPnsnICd9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnQnJpbmcgZm9yd2FyZCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Ryb3Bkb3duSXRlbT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duSXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUl0ZW1PcmRlcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXRJZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTU9WRV9TVEVQX0JBQ0tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRUb0JhY2tJY29uIHdpZHRoPVwiMTJcIiBoZWlnaHQ9XCIxMlwiIC8+eycgJ31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0KCdTZW5kIGJhY2t3YXJkJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd25JdGVtPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd25JdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlSXRlbU9yZGVyKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFdpZGdldElkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBNT1ZFX0ZPUldBUkRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRCcmluZ1RvRnJvbnRJY29uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxMlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PVwiMTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLz57JyAnfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3QoJ0JyaW5nIHRvIGZyb250Jyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd25JdGVtPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd25JdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlSXRlbU9yZGVyKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFdpZGdldElkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBNT1ZFX0JBQ0tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRCcmluZ1RvQmFja0ljb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxMlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPnsnICd9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU2VuZCB0byBiYWNrJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd25JdGVtPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24uTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd24+XG4gICAgICAgICAgICAgICAgICAgICAgICA8VG9vbGJhck9wdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtpc05pbChjb3BpZWRXaWRnZXQpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGhhbmRsZVBhc3RlRWxlbWVudChjb3BpZWRXaWRnZXQpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRQYXN0ZUljb24gd2lkdGg9XCIxNlwiIGhlaWdodD1cIjE2XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVG9vbGJhck9wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxUb29sYmFyT3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZVBvb2xMZW5ndGggPCAyIHx8IHN0YXRlSW5kZXhTZWxlY3RlZCA9PT0gMFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVVbmRvfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRVbmRvSWNvbiB3aWR0aD1cIjE2XCIgaGVpZ2h0PVwiMTZcIiAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Ub29sYmFyT3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFRvb2xiYXJPcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRlSW5kZXhTZWxlY3RlZCA9PT0gc3RhdGVQb29sTGVuZ3RoIC0gMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVSZWRvfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRSZWRvSWNvbiB3aWR0aD1cIjE2XCIgaGVpZ2h0PVwiMTZcIiAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Ub29sYmFyT3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFRvb2xiYXJPcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Nob3dHdWlkZXMgJiYgJ3NlbGVjdGVkJ31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBoYW5kbGVTZXRTaG93R3VpZGVzKCFzaG93R3VpZGVzKX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3R5bGVkSWNvbkdyaWRzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkPXtzaG93R3VpZGVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1Rvb2xiYXJPcHRpb24+XG4gICAgICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L1Rvb2xiYXJDb250YWluZXI+XG4gICAgICAgIDwvVG9vbGJhcldyYXBwZXI+XG4gICAgKTtcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3Rvcih7XG4gICAgY29waWVkV2lkZ2V0OiBzZWxlY3RvcnMuZ2V0Q29waWVkSXRlbSxcbiAgICBjdXJyZW50VGVtcGxhdGVUeXBlOiB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0Q3VycmVudFRlbXBsYXRlVHlwZSxcbiAgICBpc1Rvb2xiYXJEaXNhYmxlZDogc2VsZWN0b3JzLmdldElzVG9vbGJhckRpc2FibGVkLFxuICAgIHNlbGVjdGVkUGFnZUlkOiBzZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlSWQsXG4gICAgc2VsZWN0ZWRXaWRnZXRJZDogc2VsZWN0b3JzLmdldFNlbGVjdGVkV2lkZ2V0SWQsXG4gICAgc2hvd0d1aWRlczogc2VsZWN0b3JzLmdldFNob3dHdWlkZXMsXG4gICAgc3RhdGVJbmRleFNlbGVjdGVkOiBzZWxlY3RvcnMuZ2V0U3RhdGVQb29sSW5kZXgsXG4gICAgc3RhdGVQb29sTGVuZ3RoOiBzZWxlY3RvcnMuZ2V0U3RhdGVQb29sRGF0YUxlbmd0aCxcbn0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+ICh7XG4gICAgbmV4dFN0YXRlUG9vbDogKG9uVXBkYXRlUGFnZSkgPT5cbiAgICAgICAgZGlzcGF0Y2goYWN0aW9ucy5uZXh0U3RhdGVQb29sKG9uVXBkYXRlUGFnZSkpLFxuICAgIHBhc3RlV2lkZ2V0OiAoXG4gICAgICAgIHBhZ2VJZCxcbiAgICAgICAgb3B0aW9ucyxcbiAgICAgICAgc2hvd0d1aWRlcyxcbiAgICAgICAgcG9zaXRpb24sXG4gICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxuICAgICAgICBvbkNoYW5nZUluUGFnZVxuICAgICkgPT5cbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnBhc3RlV2lkZ2V0KFxuICAgICAgICAgICAgICAgIHBhZ2VJZCxcbiAgICAgICAgICAgICAgICBvcHRpb25zLFxuICAgICAgICAgICAgICAgIHNob3dHdWlkZXMsXG4gICAgICAgICAgICAgICAgcG9zaXRpb24sXG4gICAgICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcbiAgICAgICAgICAgIClcbiAgICAgICAgKSxcbiAgICBwcmV2U3RhdGVQb29sOiAob25VcGRhdGVQYWdlKSA9PlxuICAgICAgICBkaXNwYXRjaChhY3Rpb25zLnByZXZTdGF0ZVBvb2wob25VcGRhdGVQYWdlKSksXG4gICAgc2V0Q29waWVkV2lkZ2V0OiAoZGF0YSkgPT4gZGlzcGF0Y2goYWN0aW9ucy5zZXRDb3BpZWRJdGVtKGRhdGEpKSxcbiAgICBzZXRTaG93R3VpZGVzOiAoc2hvd0d1aWRlcykgPT4gZGlzcGF0Y2goYWN0aW9ucy5zZXRTaG93R3VpZGVzKHNob3dHdWlkZXMpKSxcbiAgICBzZXRXaWRnZXRPcmRlcjogKHBhZ2VJZCwgd2lkZ2V0SWQsIG1vdmVUeXBlLCBvbkNoYW5nZUluUGFnZSkgPT5cbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldFdpZGdldE9yZGVyKFxuICAgICAgICAgICAgICAgIHBhZ2VJZCxcbiAgICAgICAgICAgICAgICB3aWRnZXRJZCxcbiAgICAgICAgICAgICAgICBtb3ZlVHlwZSxcbiAgICAgICAgICAgICAgICBvbkNoYW5nZUluUGFnZVxuICAgICAgICAgICAgKVxuICAgICAgICApLFxufSk7XG5cblBhZ2VUb29sYmFyLnByb3BUeXBlcyA9IHtcbiAgICBjb3BpZWRXaWRnZXQ6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgY3VycmVudFRlbXBsYXRlVHlwZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGFuZGxlUmVkbzogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlVW5kbzogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaXNUb29sYmFyRGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIG5leHRTdGF0ZVBvb2w6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uQ2hhbmdlSXRlbU9yZGVyOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBwYXN0ZVdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgcHJldlN0YXRlUG9vbDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2VsZWN0ZWRQYWdlSWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2VsZWN0ZWRXaWRnZXRJZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzZXRTaG93R3VpZGVzOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzZXRXaWRnZXRPcmRlcjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2hvd0d1aWRlczogUHJvcFR5cGVzLmJvb2wsXG4gICAgc3RhdGVJbmRleFNlbGVjdGVkOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHN0YXRlUG9vbExlbmd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB2aXNpYmxlOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShQYWdlVG9vbGJhcik7XG4iXX0=