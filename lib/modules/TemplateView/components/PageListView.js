"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../constants/template");

var _types = require("../../../constants/types");

var templateCoreSelectors = _interopRequireWildcard(require("../../TemplateCore/selectors"));

var _PageListItem = _interopRequireDefault(require("./pages/PageListItem"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: ", "px;\n    margin: 0 auto;\n    align-items: center;\n    &.page-design-disabled {\n        width: ", "px;\n        align-items: flex-start;\n        margin: 0;\n    }\n    ", "\n"])), function (_ref) {
  var pageWidth = _ref.pageWidth;
  return pageWidth + 40;
}, function (_ref2) {
  var pageWidth = _ref2.pageWidth;
  return pageWidth;
}, function (_ref3) {
  var horizontalScrollMode = _ref3.horizontalScrollMode;
  return horizontalScrollMode && 'flex-direction: row;';
});

var PageListView = function PageListView(_ref4) {
  var availableCatalogs = _ref4.availableCatalogs,
      chartWidgetsNames = _ref4.chartWidgetsNames,
      currentTemplatePages = _ref4.currentTemplatePages,
      currentTemplateType = _ref4.currentTemplateType,
      _ref4$editionLevel = _ref4.editionLevel,
      editionLevel = _ref4$editionLevel === void 0 ? _types.NO_EDITION_MODE : _ref4$editionLevel,
      froalaEditorConfig = _ref4.froalaEditorConfig,
      horizontalScrollMode = _ref4.horizontalScrollMode,
      infinitePagesSupportEnabled = _ref4.infinitePagesSupportEnabled,
      pageDesignDisabled = _ref4.pageDesignDisabled,
      showGuides = _ref4.showGuides,
      tableWidgetsNames = _ref4.tableWidgetsNames;
  var hasLandscapePage = (0, _react.useMemo)(function () {
    var hasOnlyPortraitPage = true;
    currentTemplatePages.every(function (page) {
      if (page.type === 'group') {
        page.pages.every(function (groupedPage) {
          if (groupedPage.orientation === 'landscape') {
            hasOnlyPortraitPage = false;
          }

          return hasOnlyPortraitPage;
        });
        return hasOnlyPortraitPage;
      }

      if (page.orientation === 'landscape') {
        hasOnlyPortraitPage = false;
      }

      return hasOnlyPortraitPage;
    });
    return !hasOnlyPortraitPage;
  }, [currentTemplatePages]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    className: "".concat(pageDesignDisabled && 'page-design-disabled'),
    pageWidth: hasLandscapePage ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH,
    horizontalScrollMode: horizontalScrollMode,
    children: currentTemplatePages.map(function (page) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_PageListItem.default, {
        availableCatalogs: editionLevel !== _types.NO_EDITION_MODE ? availableCatalogs : [],
        chartWidgetsNames: chartWidgetsNames,
        editionLevel: editionLevel,
        froalaEditorConfig: froalaEditorConfig,
        infinitePagesSupportEnabled: infinitePagesSupportEnabled,
        page: page,
        pageDesignDisabled: pageDesignDisabled,
        showGuides: showGuides,
        tableWidgetsNames: tableWidgetsNames,
        templateType: currentTemplateType
      }, "page-list-item-".concat(page.id));
    })
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  currentTemplatePages: templateCoreSelectors.getCurrentTemplatePages,
  currentTemplateType: templateCoreSelectors.getCurrentTemplateType
});
PageListView.propTypes = {
  availableCatalogs: _propTypes.default.array,
  chartWidgetsNames: _propTypes.default.array,
  currentTemplatePages: _propTypes.default.array,
  currentTemplateType: _propTypes.default.number,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  horizontalScrollMode: _propTypes.default.bool,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  pageDesignDisabled: _propTypes.default.bool,
  showGuides: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array
};

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(PageListView);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1BhZ2VMaXN0Vmlldy5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJwYWdlV2lkdGgiLCJob3Jpem9udGFsU2Nyb2xsTW9kZSIsIlBhZ2VMaXN0VmlldyIsImF2YWlsYWJsZUNhdGFsb2dzIiwiY2hhcnRXaWRnZXRzTmFtZXMiLCJjdXJyZW50VGVtcGxhdGVQYWdlcyIsImN1cnJlbnRUZW1wbGF0ZVR5cGUiLCJlZGl0aW9uTGV2ZWwiLCJOT19FRElUSU9OX01PREUiLCJmcm9hbGFFZGl0b3JDb25maWciLCJpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQiLCJwYWdlRGVzaWduRGlzYWJsZWQiLCJzaG93R3VpZGVzIiwidGFibGVXaWRnZXRzTmFtZXMiLCJoYXNMYW5kc2NhcGVQYWdlIiwiaGFzT25seVBvcnRyYWl0UGFnZSIsImV2ZXJ5IiwicGFnZSIsInR5cGUiLCJwYWdlcyIsImdyb3VwZWRQYWdlIiwib3JpZW50YXRpb24iLCJQQUdFX0hFSUdIVCIsIlBBR0VfV0lEVEgiLCJtYXAiLCJpZCIsIm1hcFN0YXRlVG9Qcm9wcyIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzIiwiZ2V0Q3VycmVudFRlbXBsYXRlVHlwZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5IiwibnVtYmVyIiwic3RyaW5nIiwib2JqZWN0IiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBVix5VEFHRjtBQUFBLE1BQUdDLFNBQUgsUUFBR0EsU0FBSDtBQUFBLFNBQW1CQSxTQUFTLEdBQUcsRUFBL0I7QUFBQSxDQUhFLEVBT0U7QUFBQSxNQUFHQSxTQUFILFNBQUdBLFNBQUg7QUFBQSxTQUFtQkEsU0FBbkI7QUFBQSxDQVBGLEVBV1Q7QUFBQSxNQUFHQyxvQkFBSCxTQUFHQSxvQkFBSDtBQUFBLFNBQ0VBLG9CQUFvQixJQUFJLHNCQUQxQjtBQUFBLENBWFMsQ0FBZjs7QUFlQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxRQVlmO0FBQUEsTUFYRkMsaUJBV0UsU0FYRkEsaUJBV0U7QUFBQSxNQVZGQyxpQkFVRSxTQVZGQSxpQkFVRTtBQUFBLE1BVEZDLG9CQVNFLFNBVEZBLG9CQVNFO0FBQUEsTUFSRkMsbUJBUUUsU0FSRkEsbUJBUUU7QUFBQSxpQ0FQRkMsWUFPRTtBQUFBLE1BUEZBLFlBT0UsbUNBUGFDLHNCQU9iO0FBQUEsTUFORkMsa0JBTUUsU0FORkEsa0JBTUU7QUFBQSxNQUxGUixvQkFLRSxTQUxGQSxvQkFLRTtBQUFBLE1BSkZTLDJCQUlFLFNBSkZBLDJCQUlFO0FBQUEsTUFIRkMsa0JBR0UsU0FIRkEsa0JBR0U7QUFBQSxNQUZGQyxVQUVFLFNBRkZBLFVBRUU7QUFBQSxNQURGQyxpQkFDRSxTQURGQSxpQkFDRTtBQUNGLE1BQU1DLGdCQUFnQixHQUFHLG9CQUFRLFlBQU07QUFDbkMsUUFBSUMsbUJBQW1CLEdBQUcsSUFBMUI7QUFDQVYsSUFBQUEsb0JBQW9CLENBQUNXLEtBQXJCLENBQTJCLFVBQUNDLElBQUQsRUFBVTtBQUNqQyxVQUFJQSxJQUFJLENBQUNDLElBQUwsS0FBYyxPQUFsQixFQUEyQjtBQUN2QkQsUUFBQUEsSUFBSSxDQUFDRSxLQUFMLENBQVdILEtBQVgsQ0FBaUIsVUFBQ0ksV0FBRCxFQUFpQjtBQUM5QixjQUFJQSxXQUFXLENBQUNDLFdBQVosS0FBNEIsV0FBaEMsRUFBNkM7QUFDekNOLFlBQUFBLG1CQUFtQixHQUFHLEtBQXRCO0FBQ0g7O0FBQ0QsaUJBQU9BLG1CQUFQO0FBQ0gsU0FMRDtBQU1BLGVBQU9BLG1CQUFQO0FBQ0g7O0FBQ0QsVUFBSUUsSUFBSSxDQUFDSSxXQUFMLEtBQXFCLFdBQXpCLEVBQXNDO0FBQ2xDTixRQUFBQSxtQkFBbUIsR0FBRyxLQUF0QjtBQUNIOztBQUNELGFBQU9BLG1CQUFQO0FBQ0gsS0FkRDtBQWVBLFdBQU8sQ0FBQ0EsbUJBQVI7QUFDSCxHQWxCd0IsRUFrQnRCLENBQUNWLG9CQUFELENBbEJzQixDQUF6QjtBQW9CQSxzQkFDSSxxQkFBQyxTQUFEO0FBQ0ksSUFBQSxTQUFTLFlBQUtNLGtCQUFrQixJQUFJLHNCQUEzQixDQURiO0FBRUksSUFBQSxTQUFTLEVBQUVHLGdCQUFnQixHQUFHUSxxQkFBSCxHQUFpQkMsb0JBRmhEO0FBR0ksSUFBQSxvQkFBb0IsRUFBRXRCLG9CQUgxQjtBQUFBLGNBS0tJLG9CQUFvQixDQUFDbUIsR0FBckIsQ0FBeUIsVUFBQ1AsSUFBRDtBQUFBLDBCQUN0QixxQkFBQyxxQkFBRDtBQUNJLFFBQUEsaUJBQWlCLEVBQ2JWLFlBQVksS0FBS0Msc0JBQWpCLEdBQ01MLGlCQUROLEdBRU0sRUFKZDtBQU1JLFFBQUEsaUJBQWlCLEVBQUVDLGlCQU52QjtBQU9JLFFBQUEsWUFBWSxFQUFFRyxZQVBsQjtBQVFJLFFBQUEsa0JBQWtCLEVBQUVFLGtCQVJ4QjtBQVNJLFFBQUEsMkJBQTJCLEVBQUVDLDJCQVRqQztBQVdJLFFBQUEsSUFBSSxFQUFFTyxJQVhWO0FBWUksUUFBQSxrQkFBa0IsRUFBRU4sa0JBWnhCO0FBYUksUUFBQSxVQUFVLEVBQUVDLFVBYmhCO0FBY0ksUUFBQSxpQkFBaUIsRUFBRUMsaUJBZHZCO0FBZUksUUFBQSxZQUFZLEVBQUVQO0FBZmxCLGtDQVUyQlcsSUFBSSxDQUFDUSxFQVZoQyxFQURzQjtBQUFBLEtBQXpCO0FBTEwsSUFESjtBQTJCSCxDQTVERDs7QUE4REEsSUFBTUMsZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q3JCLEVBQUFBLG9CQUFvQixFQUFFc0IscUJBQXFCLENBQUNDLHVCQURDO0FBRTdDdEIsRUFBQUEsbUJBQW1CLEVBQUVxQixxQkFBcUIsQ0FBQ0U7QUFGRSxDQUF6QixDQUF4QjtBQUtBM0IsWUFBWSxDQUFDNEIsU0FBYixHQUF5QjtBQUNyQjNCLEVBQUFBLGlCQUFpQixFQUFFNEIsbUJBQVVDLEtBRFI7QUFFckI1QixFQUFBQSxpQkFBaUIsRUFBRTJCLG1CQUFVQyxLQUZSO0FBR3JCM0IsRUFBQUEsb0JBQW9CLEVBQUUwQixtQkFBVUMsS0FIWDtBQUlyQjFCLEVBQUFBLG1CQUFtQixFQUFFeUIsbUJBQVVFLE1BSlY7QUFLckIxQixFQUFBQSxZQUFZLEVBQUV3QixtQkFBVUcsTUFMSDtBQU1yQnpCLEVBQUFBLGtCQUFrQixFQUFFc0IsbUJBQVVJLE1BTlQ7QUFPckJsQyxFQUFBQSxvQkFBb0IsRUFBRThCLG1CQUFVSyxJQVBYO0FBUXJCMUIsRUFBQUEsMkJBQTJCLEVBQUVxQixtQkFBVUssSUFSbEI7QUFTckJ6QixFQUFBQSxrQkFBa0IsRUFBRW9CLG1CQUFVSyxJQVRUO0FBVXJCeEIsRUFBQUEsVUFBVSxFQUFFbUIsbUJBQVVLLElBVkQ7QUFXckJ2QixFQUFBQSxpQkFBaUIsRUFBRWtCLG1CQUFVQztBQVhSLENBQXpCOztlQWNlLHlCQUFRTixlQUFSLEVBQXlCLElBQXpCLEVBQStCeEIsWUFBL0IsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7IHVzZU1lbW8gfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XHJcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcclxuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcblxyXG5pbXBvcnQgeyBQQUdFX0hFSUdIVCwgUEFHRV9XSURUSCB9IGZyb20gJ0Bjb25zdGFudHMvdGVtcGxhdGUnO1xyXG5pbXBvcnQgeyBOT19FRElUSU9OX01PREUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcclxuXHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgUGFnZUxpc3RJdGVtIGZyb20gJy4vcGFnZXMvUGFnZUxpc3RJdGVtJztcclxuXHJcbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHdpZHRoOiAkeyh7IHBhZ2VXaWR0aCB9KSA9PiBwYWdlV2lkdGggKyA0MH1weDtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICYucGFnZS1kZXNpZ24tZGlzYWJsZWQge1xyXG4gICAgICAgIHdpZHRoOiAkeyh7IHBhZ2VXaWR0aCB9KSA9PiBwYWdlV2lkdGh9cHg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgfVxyXG4gICAgJHsoeyBob3Jpem9udGFsU2Nyb2xsTW9kZSB9KSA9PlxyXG4gICAgICAgIGhvcml6b250YWxTY3JvbGxNb2RlICYmICdmbGV4LWRpcmVjdGlvbjogcm93Oyd9XHJcbmA7XHJcblxyXG5jb25zdCBQYWdlTGlzdFZpZXcgPSAoe1xyXG4gICAgYXZhaWxhYmxlQ2F0YWxvZ3MsXHJcbiAgICBjaGFydFdpZGdldHNOYW1lcyxcclxuICAgIGN1cnJlbnRUZW1wbGF0ZVBhZ2VzLFxyXG4gICAgY3VycmVudFRlbXBsYXRlVHlwZSxcclxuICAgIGVkaXRpb25MZXZlbCA9IE5PX0VESVRJT05fTU9ERSxcclxuICAgIGZyb2FsYUVkaXRvckNvbmZpZyxcclxuICAgIGhvcml6b250YWxTY3JvbGxNb2RlLFxyXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkLFxyXG4gICAgcGFnZURlc2lnbkRpc2FibGVkLFxyXG4gICAgc2hvd0d1aWRlcyxcclxuICAgIHRhYmxlV2lkZ2V0c05hbWVzLFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCBoYXNMYW5kc2NhcGVQYWdlID0gdXNlTWVtbygoKSA9PiB7XHJcbiAgICAgICAgbGV0IGhhc09ubHlQb3J0cmFpdFBhZ2UgPSB0cnVlO1xyXG4gICAgICAgIGN1cnJlbnRUZW1wbGF0ZVBhZ2VzLmV2ZXJ5KChwYWdlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwYWdlLnR5cGUgPT09ICdncm91cCcpIHtcclxuICAgICAgICAgICAgICAgIHBhZ2UucGFnZXMuZXZlcnkoKGdyb3VwZWRQYWdlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGdyb3VwZWRQYWdlLm9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNPbmx5UG9ydHJhaXRQYWdlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBoYXNPbmx5UG9ydHJhaXRQYWdlO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaGFzT25seVBvcnRyYWl0UGFnZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAocGFnZS5vcmllbnRhdGlvbiA9PT0gJ2xhbmRzY2FwZScpIHtcclxuICAgICAgICAgICAgICAgIGhhc09ubHlQb3J0cmFpdFBhZ2UgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gaGFzT25seVBvcnRyYWl0UGFnZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gIWhhc09ubHlQb3J0cmFpdFBhZ2U7XHJcbiAgICB9LCBbY3VycmVudFRlbXBsYXRlUGFnZXNdKTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxDb250YWluZXJcclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtwYWdlRGVzaWduRGlzYWJsZWQgJiYgJ3BhZ2UtZGVzaWduLWRpc2FibGVkJ31gfVxyXG4gICAgICAgICAgICBwYWdlV2lkdGg9e2hhc0xhbmRzY2FwZVBhZ2UgPyBQQUdFX0hFSUdIVCA6IFBBR0VfV0lEVEh9XHJcbiAgICAgICAgICAgIGhvcml6b250YWxTY3JvbGxNb2RlPXtob3Jpem9udGFsU2Nyb2xsTW9kZX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIHtjdXJyZW50VGVtcGxhdGVQYWdlcy5tYXAoKHBhZ2UpID0+IChcclxuICAgICAgICAgICAgICAgIDxQYWdlTGlzdEl0ZW1cclxuICAgICAgICAgICAgICAgICAgICBhdmFpbGFibGVDYXRhbG9ncz17XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCAhPT0gTk9fRURJVElPTl9NT0RFXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IGF2YWlsYWJsZUNhdGFsb2dzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cclxuICAgICAgICAgICAgICAgICAgICBmcm9hbGFFZGl0b3JDb25maWc9e2Zyb2FsYUVkaXRvckNvbmZpZ31cclxuICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ9e2luZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICBrZXk9e2BwYWdlLWxpc3QtaXRlbS0ke3BhZ2UuaWR9YH1cclxuICAgICAgICAgICAgICAgICAgICBwYWdlPXtwYWdlfVxyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VEZXNpZ25EaXNhYmxlZD17cGFnZURlc2lnbkRpc2FibGVkfVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dHdWlkZXM9e3Nob3dHdWlkZXN9XHJcbiAgICAgICAgICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVHlwZT17Y3VycmVudFRlbXBsYXRlVHlwZX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICkpfVxyXG4gICAgICAgIDwvQ29udGFpbmVyPlxyXG4gICAgKTtcclxufTtcclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3Rvcih7XHJcbiAgICBjdXJyZW50VGVtcGxhdGVQYWdlczogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzLFxyXG4gICAgY3VycmVudFRlbXBsYXRlVHlwZTogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVR5cGUsXHJcbn0pO1xyXG5cclxuUGFnZUxpc3RWaWV3LnByb3BUeXBlcyA9IHtcclxuICAgIGF2YWlsYWJsZUNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXHJcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxyXG4gICAgY3VycmVudFRlbXBsYXRlUGFnZXM6IFByb3BUeXBlcy5hcnJheSxcclxuICAgIGN1cnJlbnRUZW1wbGF0ZVR5cGU6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXHJcbiAgICBmcm9hbGFFZGl0b3JDb25maWc6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICBob3Jpem9udGFsU2Nyb2xsTW9kZTogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgcGFnZURlc2lnbkRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIHNob3dHdWlkZXM6IFByb3BUeXBlcy5ib29sLFxyXG4gICAgdGFibGVXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBudWxsKShQYWdlTGlzdFZpZXcpO1xyXG4iXX0=