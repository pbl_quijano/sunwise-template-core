"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Submenu = exports.StyledTabs = exports.StyledInputGroup = exports.Label = exports.InputContainer = exports.CheckboxInput = void 0;

var _reactBootstrap = require("react-bootstrap");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Label = _styledComponents.default.span(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

exports.Label = Label;
var StyledInputGroup = (0, _styledComponents.default)(_reactBootstrap.InputGroup)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n\n    & select {\n        background-color: #ffffff;\n        border-radius: 3px;\n        border: 1px solid #ecedf0;\n        height: 46px;\n        width: 133px;\n    }\n"])));
exports.StyledInputGroup = StyledInputGroup;

var InputContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    margin: 6px 0;\n"])));

exports.InputContainer = InputContainer;

var Submenu = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    border-bottom: 1px solid #4f51a1;\n    color: #4f51a1;\n    font-size: 13px;\n    line-height: 16px;\n    margin-bottom: 6px;\n    padding-bottom: 6px;\n    margin-top: 6px;\n    min-height: 16px;\n"])));

exports.Submenu = Submenu;
var StyledTabs = (0, _styledComponents.default)(_reactBootstrap.Tabs)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    .nav-link {\n        color: #aaa;\n        font-size: 12px;\n\n        &.active {\n            color: #202253;\n        }\n    }\n"])));
exports.StyledTabs = StyledTabs;

var CheckboxInput = _styledComponents.default.input.attrs(function () {
  return {
    type: 'checkbox'
  };
})(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    cursor: pointer;\n"])));

exports.CheckboxInput = CheckboxInput;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1N0eWxlcy5qcyJdLCJuYW1lcyI6WyJMYWJlbCIsInN0eWxlZCIsInNwYW4iLCJTdHlsZWRJbnB1dEdyb3VwIiwiSW5wdXRHcm91cCIsIklucHV0Q29udGFpbmVyIiwiZGl2IiwiU3VibWVudSIsIlN0eWxlZFRhYnMiLCJUYWJzIiwiQ2hlY2tib3hJbnB1dCIsImlucHV0IiwiYXR0cnMiLCJ0eXBlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7Ozs7O0FBRU8sSUFBTUEsS0FBSyxHQUFHQywwQkFBT0MsSUFBVixxT0FBWDs7O0FBVUEsSUFBTUMsZ0JBQWdCLEdBQUcsK0JBQU9DLDBCQUFQLENBQUgsMlJBQXRCOzs7QUFhQSxJQUFNQyxjQUFjLEdBQUdKLDBCQUFPSyxHQUFWLDJJQUFwQjs7OztBQU1BLElBQU1DLE9BQU8sR0FBR04sMEJBQU9DLElBQVYsbVJBQWI7OztBQVdBLElBQU1NLFVBQVUsR0FBRywrQkFBT0Msb0JBQVAsQ0FBSCwrTUFBaEI7OztBQVdBLElBQU1DLGFBQWEsR0FBR1QsMEJBQU9VLEtBQVAsQ0FBYUMsS0FBYixDQUFtQjtBQUFBLFNBQU87QUFBRUMsSUFBQUEsSUFBSSxFQUFFO0FBQVIsR0FBUDtBQUFBLENBQW5CLENBQUgsK0ZBQW5CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5wdXRHcm91cCwgVGFicyB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuZXhwb3J0IGNvbnN0IExhYmVsID0gc3R5bGVkLnNwYW5gXG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1yaWdodDogMjNweDtcbiAgICBtaW4taGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHdpZHRoOiAxMzVweDtcbmA7XG5cbmV4cG9ydCBjb25zdCBTdHlsZWRJbnB1dEdyb3VwID0gc3R5bGVkKElucHV0R3JvdXApYFxuICAgIGhlaWdodDogNDZweDtcbiAgICB3aWR0aDogMTMzcHg7XG5cbiAgICAmIHNlbGVjdCB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2VjZWRmMDtcbiAgICAgICAgaGVpZ2h0OiA0NnB4O1xuICAgICAgICB3aWR0aDogMTMzcHg7XG4gICAgfVxuYDtcblxuZXhwb3J0IGNvbnN0IElucHV0Q29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWFyZ2luOiA2cHggMDtcbmA7XG5cbmV4cG9ydCBjb25zdCBTdWJtZW51ID0gc3R5bGVkLnNwYW5gXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM0ZjUxYTE7XG4gICAgY29sb3I6ICM0ZjUxYTE7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1ib3R0b206IDZweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogNnB4O1xuICAgIG1hcmdpbi10b3A6IDZweDtcbiAgICBtaW4taGVpZ2h0OiAxNnB4O1xuYDtcblxuZXhwb3J0IGNvbnN0IFN0eWxlZFRhYnMgPSBzdHlsZWQoVGFicylgXG4gICAgLm5hdi1saW5rIHtcbiAgICAgICAgY29sb3I6ICNhYWE7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcblxuICAgICAgICAmLmFjdGl2ZSB7XG4gICAgICAgICAgICBjb2xvcjogIzIwMjI1MztcbiAgICAgICAgfVxuICAgIH1cbmA7XG5cbmV4cG9ydCBjb25zdCBDaGVja2JveElucHV0ID0gc3R5bGVkLmlucHV0LmF0dHJzKCgpID0+ICh7IHR5cGU6ICdjaGVja2JveCcgfSkpYFxuICAgIGN1cnNvcjogcG9pbnRlcjtcbmA7XG4iXX0=