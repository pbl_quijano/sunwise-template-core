"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.StyledLabel = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledLabel = _styledComponents.default.span(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    font-size: 13px;\n    font-style: normal;\n    font-weight: 600;\n    color: #848bab;\n    margin-bottom: 0.5rem;\n"])));

exports.StyledLabel = StyledLabel;

var ProposalDataFrom = function ProposalDataFrom() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      disabled = _ref.disabled,
      handleValueChange = _ref.handleValueChange,
      value = _ref.value,
      visible = _ref.visible;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if (!visible) return null;
  var proposal_number = value.proposal_number;

  var handleChange = function handleChange(proposalSourceId) {
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      proposal_number: proposalSourceId
    }));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Form.default.Group, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledLabel, {
      children: t('What proposal is the data taken from?')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Form.default.Control, {
      as: "select",
      disabled: disabled,
      name: "proposal_number",
      onChange: function onChange(e) {
        return handleChange(e.target.value);
      },
      value: proposal_number,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
        value: "",
        disabled: true,
        children: t('Select proposal')
      }, "proposal-option"), catalogs.map(function (_ref2) {
        var name = _ref2.name,
            id = _ref2.id;
        return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
          value: id,
          disabled: disabled,
          children: name
        }, "proposal-option-".concat(id));
      })]
    })]
  });
};

ProposalDataFrom.propTypes = {
  catalogs: _propTypes.default.array,
  disabled: _propTypes.default.bool,
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.object,
  visible: _propTypes.default.bool
};
var _default = ProposalDataFrom;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1Byb3Bvc2FsU2VsZWN0b3IuanMiXSwibmFtZXMiOlsiU3R5bGVkTGFiZWwiLCJzdHlsZWQiLCJzcGFuIiwiUHJvcG9zYWxEYXRhRnJvbSIsImNhdGFsb2dzIiwiZGlzYWJsZWQiLCJoYW5kbGVWYWx1ZUNoYW5nZSIsInZhbHVlIiwidmlzaWJsZSIsInQiLCJwcm9wb3NhbF9udW1iZXIiLCJoYW5kbGVDaGFuZ2UiLCJwcm9wb3NhbFNvdXJjZUlkIiwiZSIsInRhcmdldCIsIm1hcCIsIm5hbWUiLCJpZCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5IiwiYm9vbCIsImZ1bmMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQUVPLElBQU1BLFdBQVcsR0FBR0MsMEJBQU9DLElBQVYsOExBQWpCOzs7O0FBUVAsSUFBTUMsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixHQU1kO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BTFBDLFFBS08sUUFMUEEsUUFLTztBQUFBLE1BSlBDLFFBSU8sUUFKUEEsUUFJTztBQUFBLE1BSFBDLGlCQUdPLFFBSFBBLGlCQUdPO0FBQUEsTUFGUEMsS0FFTyxRQUZQQSxLQUVPO0FBQUEsTUFEUEMsT0FDTyxRQURQQSxPQUNPOztBQUNQLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFFQSxNQUFJLENBQUNELE9BQUwsRUFBYyxPQUFPLElBQVA7QUFFZCxNQUFRRSxlQUFSLEdBQTRCSCxLQUE1QixDQUFRRyxlQUFSOztBQUVBLE1BQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLGdCQUFELEVBQXNCO0FBQ3ZDTixJQUFBQSxpQkFBaUIsaUNBQ1ZDLEtBRFU7QUFFYkcsTUFBQUEsZUFBZSxFQUFFRTtBQUZKLE9BQWpCO0FBSUgsR0FMRDs7QUFPQSxzQkFDSSxzQkFBQyxhQUFELENBQU0sS0FBTjtBQUFBLDRCQUNJLHFCQUFDLFdBQUQ7QUFBQSxnQkFDS0gsQ0FBQyxDQUFDLHVDQUFEO0FBRE4sTUFESixlQUlJLHNCQUFDLGFBQUQsQ0FBTSxPQUFOO0FBQ0ksTUFBQSxFQUFFLEVBQUMsUUFEUDtBQUVJLE1BQUEsUUFBUSxFQUFFSixRQUZkO0FBR0ksTUFBQSxJQUFJLEVBQUMsaUJBSFQ7QUFJSSxNQUFBLFFBQVEsRUFBRSxrQkFBQ1EsQ0FBRDtBQUFBLGVBQU9GLFlBQVksQ0FBQ0UsQ0FBQyxDQUFDQyxNQUFGLENBQVNQLEtBQVYsQ0FBbkI7QUFBQSxPQUpkO0FBS0ksTUFBQSxLQUFLLEVBQUVHLGVBTFg7QUFBQSw4QkFPSTtBQUE4QixRQUFBLEtBQUssRUFBQyxFQUFwQztBQUF1QyxRQUFBLFFBQVEsTUFBL0M7QUFBQSxrQkFDS0QsQ0FBQyxDQUFDLGlCQUFEO0FBRE4sU0FBWSxpQkFBWixDQVBKLEVBVUtMLFFBQVEsQ0FBQ1csR0FBVCxDQUFhO0FBQUEsWUFBR0MsSUFBSCxTQUFHQSxJQUFIO0FBQUEsWUFBU0MsRUFBVCxTQUFTQSxFQUFUO0FBQUEsNEJBQ1Y7QUFFSSxVQUFBLEtBQUssRUFBRUEsRUFGWDtBQUdJLFVBQUEsUUFBUSxFQUFFWixRQUhkO0FBQUEsb0JBS0tXO0FBTEwscUNBQzRCQyxFQUQ1QixFQURVO0FBQUEsT0FBYixDQVZMO0FBQUEsTUFKSjtBQUFBLElBREo7QUEyQkgsQ0EvQ0Q7O0FBaURBZCxnQkFBZ0IsQ0FBQ2UsU0FBakIsR0FBNkI7QUFDekJkLEVBQUFBLFFBQVEsRUFBRWUsbUJBQVVDLEtBREs7QUFFekJmLEVBQUFBLFFBQVEsRUFBRWMsbUJBQVVFLElBRks7QUFHekJmLEVBQUFBLGlCQUFpQixFQUFFYSxtQkFBVUcsSUFISjtBQUl6QmYsRUFBQUEsS0FBSyxFQUFFWSxtQkFBVUksTUFKUTtBQUt6QmYsRUFBQUEsT0FBTyxFQUFFVyxtQkFBVUU7QUFMTSxDQUE3QjtlQVFlbEIsZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5leHBvcnQgY29uc3QgU3R5bGVkTGFiZWwgPSBzdHlsZWQuc3BhbmBcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgbWFyZ2luLWJvdHRvbTogMC41cmVtO1xuYDtcblxuY29uc3QgUHJvcG9zYWxEYXRhRnJvbSA9ICh7XG4gICAgY2F0YWxvZ3MsXG4gICAgZGlzYWJsZWQsXG4gICAgaGFuZGxlVmFsdWVDaGFuZ2UsXG4gICAgdmFsdWUsXG4gICAgdmlzaWJsZSxcbn0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcblxuICAgIGlmICghdmlzaWJsZSkgcmV0dXJuIG51bGw7XG5cbiAgICBjb25zdCB7IHByb3Bvc2FsX251bWJlciB9ID0gdmFsdWU7XG5cbiAgICBjb25zdCBoYW5kbGVDaGFuZ2UgPSAocHJvcG9zYWxTb3VyY2VJZCkgPT4ge1xuICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgIHByb3Bvc2FsX251bWJlcjogcHJvcG9zYWxTb3VyY2VJZCxcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxGb3JtLkdyb3VwPlxuICAgICAgICAgICAgPFN0eWxlZExhYmVsPlxuICAgICAgICAgICAgICAgIHt0KCdXaGF0IHByb3Bvc2FsIGlzIHRoZSBkYXRhIHRha2VuIGZyb20/Jyl9XG4gICAgICAgICAgICA8L1N0eWxlZExhYmVsPlxuICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcbiAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgbmFtZT1cInByb3Bvc2FsX251bWJlclwiXG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBoYW5kbGVDaGFuZ2UoZS50YXJnZXQudmFsdWUpfVxuICAgICAgICAgICAgICAgIHZhbHVlPXtwcm9wb3NhbF9udW1iZXJ9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPG9wdGlvbiBrZXk9XCJwcm9wb3NhbC1vcHRpb25cIiB2YWx1ZT1cIlwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgICAgICB7dCgnU2VsZWN0IHByb3Bvc2FsJyl9XG4gICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAge2NhdGFsb2dzLm1hcCgoeyBuYW1lLCBpZCB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHByb3Bvc2FsLW9wdGlvbi0ke2lkfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17aWR9XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtuYW1lfVxuICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgKTtcbn07XG5cblByb3Bvc2FsRGF0YUZyb20ucHJvcFR5cGVzID0ge1xuICAgIGNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB2aXNpYmxlOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFByb3Bvc2FsRGF0YUZyb207XG4iXX0=