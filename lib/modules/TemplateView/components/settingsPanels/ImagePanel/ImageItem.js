"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ImageContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    width: 88px;\n    flex-direction: column;\n    margin: 4px 6px;\n    cursor: pointer;\n    &.selected {\n        img {\n            border: 1px solid #ff9a00;\n        }\n        span {\n            font-weight: bold;\n            color: #ff9a00;\n        }\n    }\n"])));

var Image = _styledComponents.default.img(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    width: 88px;\n    height: 88px;\n    margin-bottom: 2px;\n    object-fit: contain;\n    border: 1px solid #ecedf0;\n"])));

var Name = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    height: 15px;\n    width: 88px;\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    font-size: 12px;\n    line-height: 15px;\n    color: #848bab;\n"])));

var ImageItem = function ImageItem(_ref) {
  var handleSelectImage = _ref.handleSelectImage,
      isSelected = _ref.isSelected,
      name = _ref.name,
      urlFile = _ref.urlFile;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(ImageContainer, {
    className: "image-container ".concat(isSelected && 'selected'),
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Image, {
      src: urlFile,
      onClick: handleSelectImage
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Name, {
      title: name,
      children: name
    })]
  });
};

ImageItem.propTypes = {
  handleSelectImage: _propTypes.default.func,
  isSelected: _propTypes.default.bool,
  name: _propTypes.default.string,
  urlFile: _propTypes.default.string
};
var _default = ImageItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0ltYWdlUGFuZWwvSW1hZ2VJdGVtLmpzIl0sIm5hbWVzIjpbIkltYWdlQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiSW1hZ2UiLCJpbWciLCJOYW1lIiwic3BhbiIsIkltYWdlSXRlbSIsImhhbmRsZVNlbGVjdEltYWdlIiwiaXNTZWxlY3RlZCIsIm5hbWUiLCJ1cmxGaWxlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLElBQU1BLGNBQWMsR0FBR0MsMEJBQU9DLEdBQVYseVdBQXBCOztBQWlCQSxJQUFNQyxLQUFLLEdBQUdGLDBCQUFPRyxHQUFWLGlNQUFYOztBQVFBLElBQU1DLElBQUksR0FBR0osMEJBQU9LLElBQVYsZ1FBQVY7O0FBV0EsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksT0FBc0Q7QUFBQSxNQUFuREMsaUJBQW1ELFFBQW5EQSxpQkFBbUQ7QUFBQSxNQUFoQ0MsVUFBZ0MsUUFBaENBLFVBQWdDO0FBQUEsTUFBcEJDLElBQW9CLFFBQXBCQSxJQUFvQjtBQUFBLE1BQWRDLE9BQWMsUUFBZEEsT0FBYztBQUNwRSxzQkFDSSxzQkFBQyxjQUFEO0FBQ0ksSUFBQSxTQUFTLDRCQUFxQkYsVUFBVSxJQUFJLFVBQW5DLENBRGI7QUFBQSw0QkFHSSxxQkFBQyxLQUFEO0FBQU8sTUFBQSxHQUFHLEVBQUVFLE9BQVo7QUFBcUIsTUFBQSxPQUFPLEVBQUVIO0FBQTlCLE1BSEosZUFJSSxxQkFBQyxJQUFEO0FBQU0sTUFBQSxLQUFLLEVBQUVFLElBQWI7QUFBQSxnQkFBb0JBO0FBQXBCLE1BSko7QUFBQSxJQURKO0FBUUgsQ0FURDs7QUFXQUgsU0FBUyxDQUFDSyxTQUFWLEdBQXNCO0FBQ2xCSixFQUFBQSxpQkFBaUIsRUFBRUssbUJBQVVDLElBRFg7QUFFbEJMLEVBQUFBLFVBQVUsRUFBRUksbUJBQVVFLElBRko7QUFHbEJMLEVBQUFBLElBQUksRUFBRUcsbUJBQVVHLE1BSEU7QUFJbEJMLEVBQUFBLE9BQU8sRUFBRUUsbUJBQVVHO0FBSkQsQ0FBdEI7ZUFPZVQsUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3QgSW1hZ2VDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDg4cHg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBtYXJnaW46IDRweCA2cHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICYuc2VsZWN0ZWQge1xuICAgICAgICBpbWcge1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2ZmOWEwMDtcbiAgICAgICAgfVxuICAgICAgICBzcGFuIHtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgY29sb3I6ICNmZjlhMDA7XG4gICAgICAgIH1cbiAgICB9XG5gO1xuXG5jb25zdCBJbWFnZSA9IHN0eWxlZC5pbWdgXG4gICAgd2lkdGg6IDg4cHg7XG4gICAgaGVpZ2h0OiA4OHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDJweDtcbiAgICBvYmplY3QtZml0OiBjb250YWluO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlY2VkZjA7XG5gO1xuXG5jb25zdCBOYW1lID0gc3R5bGVkLnNwYW5gXG4gICAgaGVpZ2h0OiAxNXB4O1xuICAgIHdpZHRoOiA4OHB4O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE1cHg7XG4gICAgY29sb3I6ICM4NDhiYWI7XG5gO1xuXG5jb25zdCBJbWFnZUl0ZW0gPSAoeyBoYW5kbGVTZWxlY3RJbWFnZSwgaXNTZWxlY3RlZCwgbmFtZSwgdXJsRmlsZSB9KSA9PiB7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPEltYWdlQ29udGFpbmVyXG4gICAgICAgICAgICBjbGFzc05hbWU9e2BpbWFnZS1jb250YWluZXIgJHtpc1NlbGVjdGVkICYmICdzZWxlY3RlZCd9YH1cbiAgICAgICAgPlxuICAgICAgICAgICAgPEltYWdlIHNyYz17dXJsRmlsZX0gb25DbGljaz17aGFuZGxlU2VsZWN0SW1hZ2V9IC8+XG4gICAgICAgICAgICA8TmFtZSB0aXRsZT17bmFtZX0+e25hbWV9PC9OYW1lPlxuICAgICAgICA8L0ltYWdlQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5JbWFnZUl0ZW0ucHJvcFR5cGVzID0ge1xuICAgIGhhbmRsZVNlbGVjdEltYWdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBpc1NlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHVybEZpbGU6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbWFnZUl0ZW07XG4iXX0=