"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _utils = require("../../../../../helpers/utils");

var _ImageUploader = _interopRequireDefault(require("../../../../ImageUploader"));

var _Styles = require("../Styles");

var _ImageItem = _interopRequireDefault(require("./ImageItem"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var ImageUploaderContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    width: 164px;\n"])));

var ImageLabel = (0, _styledComponents.default)(_Styles.Label)(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    white-space: nowrap;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    opacity: 0.5;\n    cursor: default;\n    ", "\n"])), function (_ref) {
  var isLoaded = _ref.isLoaded;
  return isLoaded && 'font-weight: 600;opacity: 1;';
});

var ImagesContainer = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-wrap: wrap;\n"])));

var SourceTabContent = function SourceTabContent() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref2$companyImageGal = _ref2.companyImageGallery,
      companyImageGallery = _ref2$companyImageGal === void 0 ? [] : _ref2$companyImageGal,
      handleValueChange = _ref2.handleValueChange,
      value = _ref2.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var isGalleryValue = companyImageGallery.map(function (_ref3) {
    var urlFile = _ref3.urlFile;
    return urlFile;
  }).includes(value);
  var fileName = value && !isGalleryValue ? (0, _utils.getFileNameByPath)(value) : t('File upload');
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('File')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ImageLabel, {
        title: fileName,
        isLoaded: value && !isGalleryValue,
        children: fileName
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ImageUploaderContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ImageUploader.default, {
          currentValue: value,
          handleValueChange: handleValueChange
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Default')
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ImagesContainer, {
      children: companyImageGallery.map(function (_ref4) {
        var id = _ref4.id,
            name = _ref4.name,
            urlFile = _ref4.urlFile;
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ImageItem.default, {
          handleSelectImage: function handleSelectImage() {
            return urlFile !== value && handleValueChange(urlFile);
          },
          isSelected: urlFile === value,
          name: name,
          urlFile: urlFile
        }, id);
      })
    })]
  });
};

SourceTabContent.propTypes = {
  companyImageGallery: _propTypes.default.array,
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.string
};
var _default = SourceTabContent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0ltYWdlUGFuZWwvU291cmNlVGFiQ29udGVudC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJJbWFnZVVwbG9hZGVyQ29udGFpbmVyIiwiSW1hZ2VMYWJlbCIsIkxhYmVsIiwiaXNMb2FkZWQiLCJJbWFnZXNDb250YWluZXIiLCJTb3VyY2VUYWJDb250ZW50IiwiY29tcGFueUltYWdlR2FsbGVyeSIsImhhbmRsZVZhbHVlQ2hhbmdlIiwidmFsdWUiLCJ0IiwiaXNHYWxsZXJ5VmFsdWUiLCJtYXAiLCJ1cmxGaWxlIiwiaW5jbHVkZXMiLCJmaWxlTmFtZSIsImlkIiwibmFtZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5IiwiZnVuYyIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViwwSUFBZjs7QUFNQSxJQUFNQyxzQkFBc0IsR0FBR0YsMEJBQU9DLEdBQVYsZ0hBQTVCOztBQUtBLElBQU1FLFVBQVUsR0FBRywrQkFBT0MsYUFBUCxDQUFILDRNQU1WO0FBQUEsTUFBR0MsUUFBSCxRQUFHQSxRQUFIO0FBQUEsU0FBa0JBLFFBQVEsSUFBSSw4QkFBOUI7QUFBQSxDQU5VLENBQWhCOztBQVNBLElBQU1DLGVBQWUsR0FBR04sMEJBQU9DLEdBQVYsbUhBQXJCOztBQUtBLElBQU1NLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FJZDtBQUFBLGtGQUFQLEVBQU87QUFBQSxvQ0FIUEMsbUJBR087QUFBQSxNQUhQQSxtQkFHTyxzQ0FIZSxFQUdmO0FBQUEsTUFGUEMsaUJBRU8sU0FGUEEsaUJBRU87QUFBQSxNQURQQyxLQUNPLFNBRFBBLEtBQ087O0FBQ1Asd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLGNBQWMsR0FBR0osbUJBQW1CLENBQ3JDSyxHQURrQixDQUNkO0FBQUEsUUFBR0MsT0FBSCxTQUFHQSxPQUFIO0FBQUEsV0FBaUJBLE9BQWpCO0FBQUEsR0FEYyxFQUVsQkMsUUFGa0IsQ0FFVEwsS0FGUyxDQUF2QjtBQUdBLE1BQU1NLFFBQVEsR0FDVk4sS0FBSyxJQUFJLENBQUNFLGNBQVYsR0FBMkIsOEJBQWtCRixLQUFsQixDQUEzQixHQUFzREMsQ0FBQyxDQUFDLGFBQUQsQ0FEM0Q7QUFFQSxzQkFDSSxzQkFBQyxTQUFEO0FBQUEsNEJBQ0kscUJBQUMsZUFBRDtBQUFBLGdCQUFVQSxDQUFDLENBQUMsTUFBRDtBQUFYLE1BREosZUFFSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLFVBQUQ7QUFDSSxRQUFBLEtBQUssRUFBRUssUUFEWDtBQUVJLFFBQUEsUUFBUSxFQUFFTixLQUFLLElBQUksQ0FBQ0UsY0FGeEI7QUFBQSxrQkFJS0k7QUFKTCxRQURKLGVBT0kscUJBQUMsc0JBQUQ7QUFBQSwrQkFDSSxxQkFBQyxzQkFBRDtBQUNJLFVBQUEsWUFBWSxFQUFFTixLQURsQjtBQUVJLFVBQUEsaUJBQWlCLEVBQUVEO0FBRnZCO0FBREosUUFQSjtBQUFBLE1BRkosZUFnQkkscUJBQUMsZUFBRDtBQUFBLGdCQUFVRSxDQUFDLENBQUMsU0FBRDtBQUFYLE1BaEJKLGVBaUJJLHFCQUFDLGVBQUQ7QUFBQSxnQkFDS0gsbUJBQW1CLENBQUNLLEdBQXBCLENBQXdCO0FBQUEsWUFBR0ksRUFBSCxTQUFHQSxFQUFIO0FBQUEsWUFBT0MsSUFBUCxTQUFPQSxJQUFQO0FBQUEsWUFBYUosT0FBYixTQUFhQSxPQUFiO0FBQUEsNEJBQ3JCLHFCQUFDLGtCQUFEO0FBQ0ksVUFBQSxpQkFBaUIsRUFBRTtBQUFBLG1CQUNmQSxPQUFPLEtBQUtKLEtBQVosSUFBcUJELGlCQUFpQixDQUFDSyxPQUFELENBRHZCO0FBQUEsV0FEdkI7QUFJSSxVQUFBLFVBQVUsRUFBRUEsT0FBTyxLQUFLSixLQUo1QjtBQU1JLFVBQUEsSUFBSSxFQUFFUSxJQU5WO0FBT0ksVUFBQSxPQUFPLEVBQUVKO0FBUGIsV0FLU0csRUFMVCxDQURxQjtBQUFBLE9BQXhCO0FBREwsTUFqQko7QUFBQSxJQURKO0FBaUNILENBNUNEOztBQThDQVYsZ0JBQWdCLENBQUNZLFNBQWpCLEdBQTZCO0FBQ3pCWCxFQUFBQSxtQkFBbUIsRUFBRVksbUJBQVVDLEtBRE47QUFFekJaLEVBQUFBLGlCQUFpQixFQUFFVyxtQkFBVUUsSUFGSjtBQUd6QlosRUFBQUEsS0FBSyxFQUFFVSxtQkFBVUc7QUFIUSxDQUE3QjtlQU1laEIsZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBnZXRGaWxlTmFtZUJ5UGF0aCB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcblxuaW1wb3J0IEltYWdlVXBsb2FkZXIgZnJvbSAnQG1vZHVsZXMvSW1hZ2VVcGxvYWRlcic7XG5cbmltcG9ydCB7IElucHV0Q29udGFpbmVyLCBMYWJlbCwgU3VibWVudSB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmltcG9ydCBJbWFnZUl0ZW0gZnJvbSAnLi9JbWFnZUl0ZW0nO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICB3aWR0aDogMTAwJTtcbmA7XG5cbmNvbnN0IEltYWdlVXBsb2FkZXJDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDE2NHB4O1xuYDtcblxuY29uc3QgSW1hZ2VMYWJlbCA9IHN0eWxlZChMYWJlbClgXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIG9wYWNpdHk6IDAuNTtcbiAgICBjdXJzb3I6IGRlZmF1bHQ7XG4gICAgJHsoeyBpc0xvYWRlZCB9KSA9PiBpc0xvYWRlZCAmJiAnZm9udC13ZWlnaHQ6IDYwMDtvcGFjaXR5OiAxOyd9XG5gO1xuXG5jb25zdCBJbWFnZXNDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuYDtcblxuY29uc3QgU291cmNlVGFiQ29udGVudCA9ICh7XG4gICAgY29tcGFueUltYWdlR2FsbGVyeSA9IFtdLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlLFxuICAgIHZhbHVlLFxufSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IGlzR2FsbGVyeVZhbHVlID0gY29tcGFueUltYWdlR2FsbGVyeVxuICAgICAgICAubWFwKCh7IHVybEZpbGUgfSkgPT4gdXJsRmlsZSlcbiAgICAgICAgLmluY2x1ZGVzKHZhbHVlKTtcbiAgICBjb25zdCBmaWxlTmFtZSA9XG4gICAgICAgIHZhbHVlICYmICFpc0dhbGxlcnlWYWx1ZSA/IGdldEZpbGVOYW1lQnlQYXRoKHZhbHVlKSA6IHQoJ0ZpbGUgdXBsb2FkJyk7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxTdWJtZW51Pnt0KCdGaWxlJyl9PC9TdWJtZW51PlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxJbWFnZUxhYmVsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXtmaWxlTmFtZX1cbiAgICAgICAgICAgICAgICAgICAgaXNMb2FkZWQ9e3ZhbHVlICYmICFpc0dhbGxlcnlWYWx1ZX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHtmaWxlTmFtZX1cbiAgICAgICAgICAgICAgICA8L0ltYWdlTGFiZWw+XG4gICAgICAgICAgICAgICAgPEltYWdlVXBsb2FkZXJDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZVVwbG9hZGVyXG4gICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW50VmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2U9e2hhbmRsZVZhbHVlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvSW1hZ2VVcGxvYWRlckNvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8U3VibWVudT57dCgnRGVmYXVsdCcpfTwvU3VibWVudT5cbiAgICAgICAgICAgIDxJbWFnZXNDb250YWluZXI+XG4gICAgICAgICAgICAgICAge2NvbXBhbnlJbWFnZUdhbGxlcnkubWFwKCh7IGlkLCBuYW1lLCB1cmxGaWxlIH0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgPEltYWdlSXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlU2VsZWN0SW1hZ2U9eygpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsRmlsZSAhPT0gdmFsdWUgJiYgaGFuZGxlVmFsdWVDaGFuZ2UodXJsRmlsZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWQ9e3VybEZpbGUgPT09IHZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtpZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9e25hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICB1cmxGaWxlPXt1cmxGaWxlfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9JbWFnZXNDb250YWluZXI+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5Tb3VyY2VUYWJDb250ZW50LnByb3BUeXBlcyA9IHtcbiAgICBjb21wYW55SW1hZ2VHYWxsZXJ5OiBQcm9wVHlwZXMuYXJyYXksXG4gICAgaGFuZGxlVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU291cmNlVGFiQ29udGVudDtcbiJdfQ==