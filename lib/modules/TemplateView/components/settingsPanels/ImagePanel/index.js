"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var mainSelectors = _interopRequireWildcard(require("../../../../main/selectors"));

var _Styles = require("../Styles");

var _SourceTabContent = _interopRequireDefault(require("./SourceTabContent"));

var _StyleTabContent = _interopRequireDefault(require("./StyleTabContent"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ImagePanel = function ImagePanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      imageGallery = _ref.imageGallery,
      handleStyleChange = _ref.handleStyleChange,
      handleValueChange = _ref.handleValueChange,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      value = _ref.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.StyledTabs, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
      className: "pr-3 pl-3",
      eventKey: "Source",
      title: t('Source'),
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_SourceTabContent.default, {
        companyImageGallery: imageGallery,
        handleValueChange: handleValueChange,
        value: value
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
      className: "pr-3 pl-3",
      eventKey: "Style",
      title: t('Style'),
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_StyleTabContent.default, {
        handleStyleChange: handleStyleChange,
        style: style
      })
    })]
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  imageGallery: mainSelectors.getFetchImageWidgetGalleryData
});
ImagePanel.propTypes = {
  imageGallery: _propTypes.default.array,
  handleStyleChange: _propTypes.default.func,
  handleValueChange: _propTypes.default.func,
  style: _propTypes.default.object,
  value: _propTypes.default.string
};

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(ImagePanel);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0ltYWdlUGFuZWwvaW5kZXguanMiXSwibmFtZXMiOlsiSW1hZ2VQYW5lbCIsImltYWdlR2FsbGVyeSIsImhhbmRsZVN0eWxlQ2hhbmdlIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJzdHlsZSIsInZhbHVlIiwidCIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1haW5TZWxlY3RvcnMiLCJnZXRGZXRjaEltYWdlV2lkZ2V0R2FsbGVyeURhdGEiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsImZ1bmMiLCJvYmplY3QiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FNUjtBQUFBLGlGQUFQLEVBQU87QUFBQSxNQUxQQyxZQUtPLFFBTFBBLFlBS087QUFBQSxNQUpQQyxpQkFJTyxRQUpQQSxpQkFJTztBQUFBLE1BSFBDLGlCQUdPLFFBSFBBLGlCQUdPO0FBQUEsd0JBRlBDLEtBRU87QUFBQSxNQUZQQSxLQUVPLDJCQUZDLEVBRUQ7QUFBQSxNQURQQyxLQUNPLFFBRFBBLEtBQ087O0FBQ1Asd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUNJLHNCQUFDLGtCQUFEO0FBQUEsNEJBQ0kscUJBQUMsbUJBQUQ7QUFBSyxNQUFBLFNBQVMsRUFBQyxXQUFmO0FBQTJCLE1BQUEsUUFBUSxFQUFDLFFBQXBDO0FBQTZDLE1BQUEsS0FBSyxFQUFFQSxDQUFDLENBQUMsUUFBRCxDQUFyRDtBQUFBLDZCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxtQkFBbUIsRUFBRUwsWUFEekI7QUFFSSxRQUFBLGlCQUFpQixFQUFFRSxpQkFGdkI7QUFHSSxRQUFBLEtBQUssRUFBRUU7QUFIWDtBQURKLE1BREosZUFRSSxxQkFBQyxtQkFBRDtBQUFLLE1BQUEsU0FBUyxFQUFDLFdBQWY7QUFBMkIsTUFBQSxRQUFRLEVBQUMsT0FBcEM7QUFBNEMsTUFBQSxLQUFLLEVBQUVDLENBQUMsQ0FBQyxPQUFELENBQXBEO0FBQUEsNkJBQ0kscUJBQUMsd0JBQUQ7QUFDSSxRQUFBLGlCQUFpQixFQUFFSixpQkFEdkI7QUFFSSxRQUFBLEtBQUssRUFBRUU7QUFGWDtBQURKLE1BUko7QUFBQSxJQURKO0FBaUJILENBekJEOztBQTJCQSxJQUFNRyxlQUFlLEdBQUcsd0NBQXlCO0FBQzdDTixFQUFBQSxZQUFZLEVBQUVPLGFBQWEsQ0FBQ0M7QUFEaUIsQ0FBekIsQ0FBeEI7QUFJQVQsVUFBVSxDQUFDVSxTQUFYLEdBQXVCO0FBQ25CVCxFQUFBQSxZQUFZLEVBQUVVLG1CQUFVQyxLQURMO0FBRW5CVixFQUFBQSxpQkFBaUIsRUFBRVMsbUJBQVVFLElBRlY7QUFHbkJWLEVBQUFBLGlCQUFpQixFQUFFUSxtQkFBVUUsSUFIVjtBQUluQlQsRUFBQUEsS0FBSyxFQUFFTyxtQkFBVUcsTUFKRTtBQUtuQlQsRUFBQUEsS0FBSyxFQUFFTSxtQkFBVUk7QUFMRSxDQUF2Qjs7ZUFRZSx5QkFBUVIsZUFBUixFQUF5QixJQUF6QixFQUErQlAsVUFBL0IsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBUYWIgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XG5cbmltcG9ydCAqIGFzIG1haW5TZWxlY3RvcnMgZnJvbSAnQG1haW4vc2VsZWN0b3JzJztcblxuaW1wb3J0IHsgU3R5bGVkVGFicyB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmltcG9ydCBTb3VyY2VUYWJDb250ZW50IGZyb20gJy4vU291cmNlVGFiQ29udGVudCc7XG5pbXBvcnQgU3R5bGVUYWJDb250ZW50IGZyb20gJy4vU3R5bGVUYWJDb250ZW50JztcblxuY29uc3QgSW1hZ2VQYW5lbCA9ICh7XG4gICAgaW1hZ2VHYWxsZXJ5LFxuICAgIGhhbmRsZVN0eWxlQ2hhbmdlLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlLFxuICAgIHN0eWxlID0ge30sXG4gICAgdmFsdWUsXG59ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPFN0eWxlZFRhYnM+XG4gICAgICAgICAgICA8VGFiIGNsYXNzTmFtZT1cInByLTMgcGwtM1wiIGV2ZW50S2V5PVwiU291cmNlXCIgdGl0bGU9e3QoJ1NvdXJjZScpfT5cbiAgICAgICAgICAgICAgICA8U291cmNlVGFiQ29udGVudFxuICAgICAgICAgICAgICAgICAgICBjb21wYW55SW1hZ2VHYWxsZXJ5PXtpbWFnZUdhbGxlcnl9XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlPXtoYW5kbGVWYWx1ZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgICAgIDxUYWIgY2xhc3NOYW1lPVwicHItMyBwbC0zXCIgZXZlbnRLZXk9XCJTdHlsZVwiIHRpdGxlPXt0KCdTdHlsZScpfT5cbiAgICAgICAgICAgICAgICA8U3R5bGVUYWJDb250ZW50XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlPXtoYW5kbGVTdHlsZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgPC9TdHlsZWRUYWJzPlxuICAgICk7XG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xuICAgIGltYWdlR2FsbGVyeTogbWFpblNlbGVjdG9ycy5nZXRGZXRjaEltYWdlV2lkZ2V0R2FsbGVyeURhdGEsXG59KTtcblxuSW1hZ2VQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgaW1hZ2VHYWxsZXJ5OiBQcm9wVHlwZXMuYXJyYXksXG4gICAgaGFuZGxlU3R5bGVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBudWxsKShJbWFnZVBhbmVsKTtcbiJdfQ==