"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _rcSlider = _interopRequireDefault(require("rc-slider"));

var _rcTooltip = _interopRequireDefault(require("rc-tooltip"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["value", "dragging", "index"];

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var SliderContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    width: 164px;\n    .rc-slider {\n        .rc-slider-tooltip {\n            z-index: 1000;\n        }\n        .rc-slider-track {\n            background-color: #f59300;\n        }\n        .rc-slider-handle {\n            border: solid 2px #ff9900;\n            &:hover {\n                border-color: #ff9a00;\n            }\n            &:active {\n                border-color: #ff9a00;\n                box-shadow: 0 0 5px #ff9a00;\n            }\n        }\n        .rc-slider-dot-active {\n            border-color: #ff9900;\n        }\n    }\n"])));

var StyleTabContent = function StyleTabContent(_ref) {
  var handleStyleChange = _ref.handleStyleChange,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$objectFit = style.objectFit,
      objectFit = _style$objectFit === void 0 ? 'cover' : _style$objectFit,
      _style$opacity = style.opacity,
      opacity = _style$opacity === void 0 ? 1 : _style$opacity;
  var options = [{
    value: 'cover',
    label: t('Fill')
  }, {
    value: 'contain',
    label: t('Adjust')
  }, {
    value: 'fill',
    label: t('Expand')
  }];

  var onAfterChange = function onAfterChange(value) {
    return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
      opacity: value / 100
    }));
  };

  var handle = function handle(_ref2) {
    var value = _ref2.value,
        dragging = _ref2.dragging,
        index = _ref2.index,
        rest = _objectWithoutProperties(_ref2, _excluded);

    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcTooltip.default, {
      arrowContent: null,
      placement: "top",
      prefixCls: "panel-image rc-tooltip",
      overlay: "".concat(value, "%"),
      visible: dragging,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcSlider.default.Handle, _objectSpread({
        value: value
      }, rest))
    }, index);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Adjust image')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, {
          as: "select",
          id: "select-theme",
          onChange: function onChange(e) {
            return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
              objectFit: e.target.value
            }));
          },
          value: objectFit,
          children: options.map(function (_ref3) {
            var label = _ref3.label,
                value = _ref3.value,
                optionDisabled = _ref3.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              disabled: optionDisabled,
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Opacity')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(SliderContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcSlider.default, {
          defaultValue: opacity * 100,
          handle: handle,
          marks: {
            0: '0%',
            100: '100%'
          },
          max: 100,
          min: 0,
          step: 10,
          onAfterChange: onAfterChange
        })
      })]
    })]
  });
};

StyleTabContent.propTypes = {
  handleStyleChange: _propTypes.default.func,
  style: _propTypes.default.object
};
var _default = StyleTabContent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0ltYWdlUGFuZWwvU3R5bGVUYWJDb250ZW50LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsIlNsaWRlckNvbnRhaW5lciIsIlN0eWxlVGFiQ29udGVudCIsImhhbmRsZVN0eWxlQ2hhbmdlIiwic3R5bGUiLCJ0Iiwib2JqZWN0Rml0Iiwib3BhY2l0eSIsIm9wdGlvbnMiLCJ2YWx1ZSIsImxhYmVsIiwib25BZnRlckNoYW5nZSIsImhhbmRsZSIsImRyYWdnaW5nIiwiaW5kZXgiLCJyZXN0IiwiZSIsInRhcmdldCIsIm1hcCIsIm9wdGlvbkRpc2FibGVkIiwiZGlzYWJsZWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDBJQUFmOztBQU1BLElBQU1DLGVBQWUsR0FBR0YsMEJBQU9DLEdBQVYsd29CQUFyQjs7QUEwQkEsSUFBTUUsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixPQUF1QztBQUFBLE1BQXBDQyxpQkFBb0MsUUFBcENBLGlCQUFvQztBQUFBLHdCQUFqQkMsS0FBaUI7QUFBQSxNQUFqQkEsS0FBaUIsMkJBQVQsRUFBUzs7QUFDM0Qsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHlCQUE2Q0QsS0FBN0MsQ0FBUUUsU0FBUjtBQUFBLE1BQVFBLFNBQVIsaUNBQW9CLE9BQXBCO0FBQUEsdUJBQTZDRixLQUE3QyxDQUE2QkcsT0FBN0I7QUFBQSxNQUE2QkEsT0FBN0IsK0JBQXVDLENBQXZDO0FBQ0EsTUFBTUMsT0FBTyxHQUFHLENBQ1o7QUFBRUMsSUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JDLElBQUFBLEtBQUssRUFBRUwsQ0FBQyxDQUFDLE1BQUQ7QUFBMUIsR0FEWSxFQUVaO0FBQUVJLElBQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CQyxJQUFBQSxLQUFLLEVBQUVMLENBQUMsQ0FBQyxRQUFEO0FBQTVCLEdBRlksRUFHWjtBQUFFSSxJQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQkMsSUFBQUEsS0FBSyxFQUFFTCxDQUFDLENBQUMsUUFBRDtBQUF6QixHQUhZLENBQWhCOztBQU1BLE1BQU1NLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBQ0YsS0FBRDtBQUFBLFdBQ2xCTixpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUcsTUFBQUEsT0FBTyxFQUFFRSxLQUFLLEdBQUc7QUFBOUIsT0FEQztBQUFBLEdBQXRCOztBQUdBLE1BQU1HLE1BQU0sR0FBRyxTQUFUQSxNQUFTLFFBQXlDO0FBQUEsUUFBdENILEtBQXNDLFNBQXRDQSxLQUFzQztBQUFBLFFBQS9CSSxRQUErQixTQUEvQkEsUUFBK0I7QUFBQSxRQUFyQkMsS0FBcUIsU0FBckJBLEtBQXFCO0FBQUEsUUFBWEMsSUFBVzs7QUFDcEQsd0JBQ0kscUJBQUMsa0JBQUQ7QUFDSSxNQUFBLFlBQVksRUFBRSxJQURsQjtBQUdJLE1BQUEsU0FBUyxFQUFDLEtBSGQ7QUFJSSxNQUFBLFNBQVMsRUFBQyx3QkFKZDtBQUtJLE1BQUEsT0FBTyxZQUFLTixLQUFMLE1BTFg7QUFNSSxNQUFBLE9BQU8sRUFBRUksUUFOYjtBQUFBLDZCQVFJLHFCQUFDLGlCQUFELENBQVEsTUFBUjtBQUFlLFFBQUEsS0FBSyxFQUFFSjtBQUF0QixTQUFpQ00sSUFBakM7QUFSSixPQUVTRCxLQUZULENBREo7QUFZSCxHQWJEOztBQWNBLHNCQUNJLHNCQUFDLFNBQUQ7QUFBQSw0QkFDSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUVQsQ0FBQyxDQUFDLGNBQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMsd0JBQUQ7QUFBQSwrQkFDSSxxQkFBQyxvQkFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksVUFBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLFVBQUEsUUFBUSxFQUFFLGtCQUFDVyxDQUFEO0FBQUEsbUJBQ05iLGlCQUFpQixpQ0FDVkMsS0FEVTtBQUViRSxjQUFBQSxTQUFTLEVBQUVVLENBQUMsQ0FBQ0MsTUFBRixDQUFTUjtBQUZQLGVBRFg7QUFBQSxXQUhkO0FBU0ksVUFBQSxLQUFLLEVBQUVILFNBVFg7QUFBQSxvQkFXS0UsT0FBTyxDQUFDVSxHQUFSLENBQ0c7QUFBQSxnQkFBR1IsS0FBSCxTQUFHQSxLQUFIO0FBQUEsZ0JBQVVELEtBQVYsU0FBVUEsS0FBVjtBQUFBLGdCQUEyQlUsY0FBM0IsU0FBaUJDLFFBQWpCO0FBQUEsZ0NBQ0k7QUFDSSxjQUFBLFFBQVEsRUFBRUQsY0FEZDtBQUdJLGNBQUEsS0FBSyxFQUFFVixLQUhYO0FBQUEsd0JBS0tDO0FBTEwsZ0NBRW1CRCxLQUZuQixjQUU0QkMsS0FGNUIsRUFESjtBQUFBLFdBREg7QUFYTDtBQURKLFFBRko7QUFBQSxNQURKLGVBNkJJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRTCxDQUFDLENBQUMsU0FBRDtBQUFULFFBREosZUFFSSxxQkFBQyxlQUFEO0FBQUEsK0JBQ0kscUJBQUMsaUJBQUQ7QUFDSSxVQUFBLFlBQVksRUFBRUUsT0FBTyxHQUFHLEdBRDVCO0FBRUksVUFBQSxNQUFNLEVBQUVLLE1BRlo7QUFHSSxVQUFBLEtBQUssRUFBRTtBQUFFLGVBQUcsSUFBTDtBQUFXLGlCQUFLO0FBQWhCLFdBSFg7QUFJSSxVQUFBLEdBQUcsRUFBRSxHQUpUO0FBS0ksVUFBQSxHQUFHLEVBQUUsQ0FMVDtBQU1JLFVBQUEsSUFBSSxFQUFFLEVBTlY7QUFPSSxVQUFBLGFBQWEsRUFBRUQ7QUFQbkI7QUFESixRQUZKO0FBQUEsTUE3Qko7QUFBQSxJQURKO0FBOENILENBeEVEOztBQTBFQVQsZUFBZSxDQUFDbUIsU0FBaEIsR0FBNEI7QUFDeEJsQixFQUFBQSxpQkFBaUIsRUFBRW1CLG1CQUFVQyxJQURMO0FBRXhCbkIsRUFBQUEsS0FBSyxFQUFFa0IsbUJBQVVFO0FBRk8sQ0FBNUI7ZUFLZXRCLGUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IFNsaWRlciBmcm9tICdyYy1zbGlkZXInO1xuaW1wb3J0IFRvb2x0aXAgZnJvbSAncmMtdG9vbHRpcCc7XG5pbXBvcnQgeyBGb3JtIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHsgSW5wdXRDb250YWluZXIsIExhYmVsLCBTdHlsZWRJbnB1dEdyb3VwIH0gZnJvbSAnLi4vU3R5bGVzJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBTbGlkZXJDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDE2NHB4O1xuICAgIC5yYy1zbGlkZXIge1xuICAgICAgICAucmMtc2xpZGVyLXRvb2x0aXAge1xuICAgICAgICAgICAgei1pbmRleDogMTAwMDtcbiAgICAgICAgfVxuICAgICAgICAucmMtc2xpZGVyLXRyYWNrIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNTkzMDA7XG4gICAgICAgIH1cbiAgICAgICAgLnJjLXNsaWRlci1oYW5kbGUge1xuICAgICAgICAgICAgYm9yZGVyOiBzb2xpZCAycHggI2ZmOTkwMDtcbiAgICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2ZmOWEwMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICY6YWN0aXZlIHtcbiAgICAgICAgICAgICAgICBib3JkZXItY29sb3I6ICNmZjlhMDA7XG4gICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDVweCAjZmY5YTAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC5yYy1zbGlkZXItZG90LWFjdGl2ZSB7XG4gICAgICAgICAgICBib3JkZXItY29sb3I6ICNmZjk5MDA7XG4gICAgICAgIH1cbiAgICB9XG5gO1xuXG5jb25zdCBTdHlsZVRhYkNvbnRlbnQgPSAoeyBoYW5kbGVTdHlsZUNoYW5nZSwgc3R5bGUgPSB7fSB9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHsgb2JqZWN0Rml0ID0gJ2NvdmVyJywgb3BhY2l0eSA9IDEgfSA9IHN0eWxlO1xuICAgIGNvbnN0IG9wdGlvbnMgPSBbXG4gICAgICAgIHsgdmFsdWU6ICdjb3ZlcicsIGxhYmVsOiB0KCdGaWxsJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2NvbnRhaW4nLCBsYWJlbDogdCgnQWRqdXN0JykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2ZpbGwnLCBsYWJlbDogdCgnRXhwYW5kJykgfSxcbiAgICBdO1xuXG4gICAgY29uc3Qgb25BZnRlckNoYW5nZSA9ICh2YWx1ZSkgPT5cbiAgICAgICAgaGFuZGxlU3R5bGVDaGFuZ2UoeyAuLi5zdHlsZSwgb3BhY2l0eTogdmFsdWUgLyAxMDAgfSk7XG5cbiAgICBjb25zdCBoYW5kbGUgPSAoeyB2YWx1ZSwgZHJhZ2dpbmcsIGluZGV4LCAuLi5yZXN0IH0pID0+IHtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgICAgYXJyb3dDb250ZW50PXtudWxsfVxuICAgICAgICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgICAgICAgcGxhY2VtZW50PVwidG9wXCJcbiAgICAgICAgICAgICAgICBwcmVmaXhDbHM9XCJwYW5lbC1pbWFnZSByYy10b29sdGlwXCJcbiAgICAgICAgICAgICAgICBvdmVybGF5PXtgJHt2YWx1ZX0lYH1cbiAgICAgICAgICAgICAgICB2aXNpYmxlPXtkcmFnZ2luZ31cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8U2xpZGVyLkhhbmRsZSB2YWx1ZT17dmFsdWV9IHsuLi5yZXN0fSAvPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICApO1xuICAgIH07XG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0FkanVzdCBpbWFnZScpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcbiAgICAgICAgICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2VsZWN0LXRoZW1lXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLnN0eWxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYmplY3RGaXQ6IGUudGFyZ2V0LnZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17b2JqZWN0Rml0fVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7b3B0aW9ucy5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHsgbGFiZWwsIHZhbHVlLCBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BzZWxlY3QtJHt2YWx1ZX0tJHtsYWJlbH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7bGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdPcGFjaXR5Jyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U2xpZGVyQ29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8U2xpZGVyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU9e29wYWNpdHkgKiAxMDB9XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGU9e2hhbmRsZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmtzPXt7IDA6ICcwJScsIDEwMDogJzEwMCUnIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXg9ezEwMH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbj17MH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN0ZXA9ezEwfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25BZnRlckNoYW5nZT17b25BZnRlckNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1NsaWRlckNvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5TdHlsZVRhYkNvbnRlbnQucHJvcFR5cGVzID0ge1xuICAgIGhhbmRsZVN0eWxlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFN0eWxlVGFiQ29udGVudDtcbiJdfQ==