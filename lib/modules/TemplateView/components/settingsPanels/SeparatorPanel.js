"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _styled = require("../../../../components/styled");

var _Styles = require("./Styles");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SeparatorPanel = function SeparatorPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleStyleChange = _ref.handleStyleChange,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$borderBottomSt = style.borderBottomStyle,
      borderBottomStyle = _style$borderBottomSt === void 0 ? 'solid' : _style$borderBottomSt,
      _style$borderBottomCo = style.borderBottomColor,
      borderBottomColor = _style$borderBottomCo === void 0 ? '#000' : _style$borderBottomCo;
  var options = [{
    value: 'solid',
    label: t('Normal')
  }, {
    value: 'dashed',
    label: t('Dashed')
  }, {
    value: 'dotted',
    label: t('Dotted')
  }];
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Type')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          as: "select",
          id: "select-theme",
          onChange: function onChange(e) {
            return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
              borderBottomStyle: e.target.value
            }));
          },
          value: borderBottomStyle,
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              disabled: optionDisabled,
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Color'),
        value: borderBottomColor,
        onChange: function onChange(tempColor) {
          return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
            borderBottomColor: tempColor
          }));
        }
      })
    })]
  });
};

SeparatorPanel.propTypes = {
  handleStyleChange: _propTypes.default.func,
  style: _propTypes.default.object
};
var _default = SeparatorPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1NlcGFyYXRvclBhbmVsLmpzIl0sIm5hbWVzIjpbIlNlcGFyYXRvclBhbmVsIiwiaGFuZGxlU3R5bGVDaGFuZ2UiLCJzdHlsZSIsInQiLCJib3JkZXJCb3R0b21TdHlsZSIsImJvcmRlckJvdHRvbUNvbG9yIiwib3B0aW9ucyIsInZhbHVlIiwibGFiZWwiLCJlIiwidGFyZ2V0IiwibWFwIiwib3B0aW9uRGlzYWJsZWQiLCJkaXNhYmxlZCIsInRlbXBDb2xvciIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixHQUE0QztBQUFBLGlGQUFQLEVBQU87QUFBQSxNQUF6Q0MsaUJBQXlDLFFBQXpDQSxpQkFBeUM7QUFBQSx3QkFBdEJDLEtBQXNCO0FBQUEsTUFBdEJBLEtBQXNCLDJCQUFkLEVBQWM7O0FBQy9ELHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSw4QkFBb0VELEtBQXBFLENBQVFFLGlCQUFSO0FBQUEsTUFBUUEsaUJBQVIsc0NBQTRCLE9BQTVCO0FBQUEsOEJBQW9FRixLQUFwRSxDQUFxQ0csaUJBQXJDO0FBQUEsTUFBcUNBLGlCQUFyQyxzQ0FBeUQsTUFBekQ7QUFDQSxNQUFNQyxPQUFPLEdBQUcsQ0FDWjtBQUFFQyxJQUFBQSxLQUFLLEVBQUUsT0FBVDtBQUFrQkMsSUFBQUEsS0FBSyxFQUFFTCxDQUFDLENBQUMsUUFBRDtBQUExQixHQURZLEVBRVo7QUFBRUksSUFBQUEsS0FBSyxFQUFFLFFBQVQ7QUFBbUJDLElBQUFBLEtBQUssRUFBRUwsQ0FBQyxDQUFDLFFBQUQ7QUFBM0IsR0FGWSxFQUdaO0FBQUVJLElBQUFBLEtBQUssRUFBRSxRQUFUO0FBQW1CQyxJQUFBQSxLQUFLLEVBQUVMLENBQUMsQ0FBQyxRQUFEO0FBQTNCLEdBSFksQ0FBaEI7QUFLQSxzQkFDSSxzQkFBQyxrQkFBRDtBQUFBLDRCQUNJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRQSxDQUFDLENBQUMsTUFBRDtBQUFULFFBREosZUFFSSxxQkFBQyx3QkFBRDtBQUFBLCtCQUNJLHFCQUFDLGFBQUQsQ0FBTSxPQUFOO0FBQ0ksVUFBQSxFQUFFLEVBQUMsUUFEUDtBQUVJLFVBQUEsRUFBRSxFQUFDLGNBRlA7QUFHSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ00sQ0FBRDtBQUFBLG1CQUNOUixpQkFBaUIsaUNBQ1ZDLEtBRFU7QUFFYkUsY0FBQUEsaUJBQWlCLEVBQUVLLENBQUMsQ0FBQ0MsTUFBRixDQUFTSDtBQUZmLGVBRFg7QUFBQSxXQUhkO0FBU0ksVUFBQSxLQUFLLEVBQUVILGlCQVRYO0FBQUEsb0JBV0tFLE9BQU8sQ0FBQ0ssR0FBUixDQUNHO0FBQUEsZ0JBQUdILEtBQUgsU0FBR0EsS0FBSDtBQUFBLGdCQUFVRCxLQUFWLFNBQVVBLEtBQVY7QUFBQSxnQkFBMkJLLGNBQTNCLFNBQWlCQyxRQUFqQjtBQUFBLGdDQUNJO0FBQ0ksY0FBQSxRQUFRLEVBQUVELGNBRGQ7QUFHSSxjQUFBLEtBQUssRUFBRUwsS0FIWDtBQUFBLHdCQUtLQztBQUxMLGdDQUVtQkQsS0FGbkIsY0FFNEJDLEtBRjVCLEVBREo7QUFBQSxXQURIO0FBWEw7QUFESixRQUZKO0FBQUEsTUFESixlQTZCSSxxQkFBQyxzQkFBRDtBQUFBLDZCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxLQUFLLEVBQUVMLENBQUMsQ0FBQyxPQUFELENBRFo7QUFFSSxRQUFBLEtBQUssRUFBRUUsaUJBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQ1MsU0FBRDtBQUFBLGlCQUNOYixpQkFBaUIsaUNBQ1ZDLEtBRFU7QUFFYkcsWUFBQUEsaUJBQWlCLEVBQUVTO0FBRk4sYUFEWDtBQUFBO0FBSGQ7QUFESixNQTdCSjtBQUFBLElBREo7QUE0Q0gsQ0FwREQ7O0FBc0RBZCxjQUFjLENBQUNlLFNBQWYsR0FBMkI7QUFDdkJkLEVBQUFBLGlCQUFpQixFQUFFZSxtQkFBVUMsSUFETjtBQUV2QmYsRUFBQUEsS0FBSyxFQUFFYyxtQkFBVUU7QUFGTSxDQUEzQjtlQUtlbEIsYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgRm9ybSBmcm9tICdyZWFjdC1ib290c3RyYXAvRm9ybSc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuXG5pbXBvcnQgQ29sb3JQaWNrZXJJbnB1dCBmcm9tICdAY29tcG9uZW50cy9Db2xvclBpY2tlcklucHV0JztcbmltcG9ydCB7IEZsZXhDb2x1bW4gfSBmcm9tICdAY29tcG9uZW50cy9zdHlsZWQnO1xuXG5pbXBvcnQgeyBJbnB1dENvbnRhaW5lciwgTGFiZWwsIFN0eWxlZElucHV0R3JvdXAgfSBmcm9tICcuL1N0eWxlcyc7XG5cbmNvbnN0IFNlcGFyYXRvclBhbmVsID0gKHsgaGFuZGxlU3R5bGVDaGFuZ2UsIHN0eWxlID0ge30gfSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHsgYm9yZGVyQm90dG9tU3R5bGUgPSAnc29saWQnLCBib3JkZXJCb3R0b21Db2xvciA9ICcjMDAwJyB9ID0gc3R5bGU7XG4gICAgY29uc3Qgb3B0aW9ucyA9IFtcbiAgICAgICAgeyB2YWx1ZTogJ3NvbGlkJywgbGFiZWw6IHQoJ05vcm1hbCcpIH0sXG4gICAgICAgIHsgdmFsdWU6ICdkYXNoZWQnLCBsYWJlbDogdCgnRGFzaGVkJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2RvdHRlZCcsIGxhYmVsOiB0KCdEb3R0ZWQnKSB9LFxuICAgIF07XG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdUeXBlJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtdGhlbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLi4uc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlckJvdHRvbVN0eWxlOiBlLnRhcmdldC52YWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvcmRlckJvdHRvbVN0eWxlfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7b3B0aW9ucy5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHsgbGFiZWwsIHZhbHVlLCBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BzZWxlY3QtJHt2YWx1ZX0tJHtsYWJlbH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7bGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e3QoJ0NvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtib3JkZXJCb3R0b21Db2xvcn1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLi4uc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyQm90dG9tQ29sb3I6IHRlbXBDb2xvcixcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgPC9GbGV4Q29sdW1uPlxuICAgICk7XG59O1xuXG5TZXBhcmF0b3JQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlU3R5bGVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU2VwYXJhdG9yUGFuZWw7XG4iXX0=