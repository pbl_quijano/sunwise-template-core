"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _ProposalSelector = _interopRequireDefault(require("./ProposalSelector"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocationPanel = function LocationPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      handleValueChange = _ref.handleValueChange,
      hasProposalSelector = _ref.hasProposalSelector,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_ProposalSelector.default, {
    catalogs: catalogs,
    disabled: proposalSelectorDisabled,
    handleValueChange: handleValueChange,
    value: value,
    visible: hasProposalSelector
  });
};

LocationPanel.propTypes = {
  catalogs: _propTypes.default.array,
  handleValueChange: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  proposalSelectorDisabled: _propTypes.default.string,
  value: _propTypes.default.object
};
var _default = LocationPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0xvY2F0aW9uUGFuZWwuanMiXSwibmFtZXMiOlsiTG9jYXRpb25QYW5lbCIsImNhdGFsb2dzIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJoYXNQcm9wb3NhbFNlbGVjdG9yIiwicHJvcG9zYWxTZWxlY3RvckRpc2FibGVkIiwidmFsdWUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsImZ1bmMiLCJib29sIiwic3RyaW5nIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7Ozs7OztBQUVBLElBQU1BLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0I7QUFBQSxpRkFNbEIsRUFOa0I7QUFBQSxNQUNsQkMsUUFEa0IsUUFDbEJBLFFBRGtCO0FBQUEsTUFFbEJDLGlCQUZrQixRQUVsQkEsaUJBRmtCO0FBQUEsTUFHbEJDLG1CQUhrQixRQUdsQkEsbUJBSGtCO0FBQUEsTUFJbEJDLHdCQUprQixRQUlsQkEsd0JBSmtCO0FBQUEsd0JBS2xCQyxLQUxrQjtBQUFBLE1BS2xCQSxLQUxrQiwyQkFLVixFQUxVOztBQUFBLHNCQU9sQixxQkFBQyx5QkFBRDtBQUNJLElBQUEsUUFBUSxFQUFFSixRQURkO0FBRUksSUFBQSxRQUFRLEVBQUVHLHdCQUZkO0FBR0ksSUFBQSxpQkFBaUIsRUFBRUYsaUJBSHZCO0FBSUksSUFBQSxLQUFLLEVBQUVHLEtBSlg7QUFLSSxJQUFBLE9BQU8sRUFBRUY7QUFMYixJQVBrQjtBQUFBLENBQXRCOztBQWdCQUgsYUFBYSxDQUFDTSxTQUFkLEdBQTBCO0FBQ3RCTCxFQUFBQSxRQUFRLEVBQUVNLG1CQUFVQyxLQURFO0FBRXRCTixFQUFBQSxpQkFBaUIsRUFBRUssbUJBQVVFLElBRlA7QUFHdEJOLEVBQUFBLG1CQUFtQixFQUFFSSxtQkFBVUcsSUFIVDtBQUl0Qk4sRUFBQUEsd0JBQXdCLEVBQUVHLG1CQUFVSSxNQUpkO0FBS3RCTixFQUFBQSxLQUFLLEVBQUVFLG1CQUFVSztBQUxLLENBQTFCO2VBUWVaLGEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgUHJvcG9zYWxTZWxlY3RvciBmcm9tICcuL1Byb3Bvc2FsU2VsZWN0b3InO1xuXG5jb25zdCBMb2NhdGlvblBhbmVsID0gKHtcbiAgICBjYXRhbG9ncyxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZSxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yLFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZCxcbiAgICB2YWx1ZSA9IHt9LFxufSA9IHt9KSA9PiAoXG4gICAgPFByb3Bvc2FsU2VsZWN0b3JcbiAgICAgICAgY2F0YWxvZ3M9e2NhdGFsb2dzfVxuICAgICAgICBkaXNhYmxlZD17cHJvcG9zYWxTZWxlY3RvckRpc2FibGVkfVxuICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZT17aGFuZGxlVmFsdWVDaGFuZ2V9XG4gICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgdmlzaWJsZT17aGFzUHJvcG9zYWxTZWxlY3Rvcn1cbiAgICAvPlxuKTtcblxuTG9jYXRpb25QYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFzUHJvcG9zYWxTZWxlY3RvcjogUHJvcFR5cGVzLmJvb2wsXG4gICAgcHJvcG9zYWxTZWxlY3RvckRpc2FibGVkOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgTG9jYXRpb25QYW5lbDtcbiJdfQ==