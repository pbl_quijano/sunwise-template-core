"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Styles = require("./Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var HelioscopePanel = function HelioscopePanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleStyleChange = _ref.handleStyleChange,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$objectFit = style.objectFit,
      objectFit = _style$objectFit === void 0 ? 'cover' : _style$objectFit;
  var options = [{
    value: 'cover',
    label: t('Fill')
  }, {
    value: 'contain',
    label: t('Adjust')
  }, {
    value: 'fill',
    label: t('Expand')
  }];
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Adjust image')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          as: "select",
          id: "select-theme",
          onChange: function onChange(e) {
            return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
              objectFit: e.target.value
            }));
          },
          value: objectFit,
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              disabled: optionDisabled,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    })
  });
};

HelioscopePanel.propTypes = {
  handleStyleChange: _propTypes.default.func,
  style: _propTypes.default.object
};
var _default = HelioscopePanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0hlbGlvc2NvcGVQYW5lbC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJIZWxpb3Njb3BlUGFuZWwiLCJoYW5kbGVTdHlsZUNoYW5nZSIsInN0eWxlIiwidCIsIm9iamVjdEZpdCIsIm9wdGlvbnMiLCJ2YWx1ZSIsImxhYmVsIiwiZSIsInRhcmdldCIsIm1hcCIsIm9wdGlvbkRpc2FibGVkIiwiZGlzYWJsZWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDBJQUFmOztBQU1BLElBQU1DLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsR0FBNEM7QUFBQSxpRkFBUCxFQUFPO0FBQUEsTUFBekNDLGlCQUF5QyxRQUF6Q0EsaUJBQXlDO0FBQUEsd0JBQXRCQyxLQUFzQjtBQUFBLE1BQXRCQSxLQUFzQiwyQkFBZCxFQUFjOztBQUNoRSx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EseUJBQWdDRCxLQUFoQyxDQUFRRSxTQUFSO0FBQUEsTUFBUUEsU0FBUixpQ0FBb0IsT0FBcEI7QUFDQSxNQUFNQyxPQUFPLEdBQUcsQ0FDWjtBQUFFQyxJQUFBQSxLQUFLLEVBQUUsT0FBVDtBQUFrQkMsSUFBQUEsS0FBSyxFQUFFSixDQUFDLENBQUMsTUFBRDtBQUExQixHQURZLEVBRVo7QUFBRUcsSUFBQUEsS0FBSyxFQUFFLFNBQVQ7QUFBb0JDLElBQUFBLEtBQUssRUFBRUosQ0FBQyxDQUFDLFFBQUQ7QUFBNUIsR0FGWSxFQUdaO0FBQUVHLElBQUFBLEtBQUssRUFBRSxNQUFUO0FBQWlCQyxJQUFBQSxLQUFLLEVBQUVKLENBQUMsQ0FBQyxRQUFEO0FBQXpCLEdBSFksQ0FBaEI7QUFNQSxzQkFDSSxxQkFBQyxTQUFEO0FBQUEsMkJBQ0ksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFBLENBQUMsQ0FBQyxjQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksVUFBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLFVBQUEsUUFBUSxFQUFFLGtCQUFDSyxDQUFEO0FBQUEsbUJBQ05QLGlCQUFpQixpQ0FDVkMsS0FEVTtBQUViRSxjQUFBQSxTQUFTLEVBQUVJLENBQUMsQ0FBQ0MsTUFBRixDQUFTSDtBQUZQLGVBRFg7QUFBQSxXQUhkO0FBU0ksVUFBQSxLQUFLLEVBQUVGLFNBVFg7QUFBQSxvQkFXS0MsT0FBTyxDQUFDSyxHQUFSLENBQ0c7QUFBQSxnQkFBR0gsS0FBSCxTQUFHQSxLQUFIO0FBQUEsZ0JBQVVELEtBQVYsU0FBVUEsS0FBVjtBQUFBLGdCQUEyQkssY0FBM0IsU0FBaUJDLFFBQWpCO0FBQUEsZ0NBQ0k7QUFFSSxjQUFBLEtBQUssRUFBRU4sS0FGWDtBQUdJLGNBQUEsUUFBUSxFQUFFSyxjQUhkO0FBQUEsd0JBS0tKO0FBTEwsZ0NBQ21CRCxLQURuQixjQUM0QkMsS0FENUIsRUFESjtBQUFBLFdBREg7QUFYTDtBQURKLFFBRko7QUFBQTtBQURKLElBREo7QUFnQ0gsQ0F6Q0Q7O0FBMkNBUCxlQUFlLENBQUNhLFNBQWhCLEdBQTRCO0FBQ3hCWixFQUFBQSxpQkFBaUIsRUFBRWEsbUJBQVVDLElBREw7QUFFeEJiLEVBQUFBLEtBQUssRUFBRVksbUJBQVVFO0FBRk8sQ0FBNUI7ZUFLZWhCLGUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBJbnB1dENvbnRhaW5lciwgTGFiZWwsIFN0eWxlZElucHV0R3JvdXAgfSBmcm9tICcuL1N0eWxlcyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHdpZHRoOiAxMDAlO1xuYDtcblxuY29uc3QgSGVsaW9zY29wZVBhbmVsID0gKHsgaGFuZGxlU3R5bGVDaGFuZ2UsIHN0eWxlID0ge30gfSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHsgb2JqZWN0Rml0ID0gJ2NvdmVyJyB9ID0gc3R5bGU7XG4gICAgY29uc3Qgb3B0aW9ucyA9IFtcbiAgICAgICAgeyB2YWx1ZTogJ2NvdmVyJywgbGFiZWw6IHQoJ0ZpbGwnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnY29udGFpbicsIGxhYmVsOiB0KCdBZGp1c3QnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnZmlsbCcsIGxhYmVsOiB0KCdFeHBhbmQnKSB9LFxuICAgIF07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnQWRqdXN0IGltYWdlJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtdGhlbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLi4uc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9iamVjdEZpdDogZS50YXJnZXQudmFsdWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtvYmplY3RGaXR9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtvcHRpb25zLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyBsYWJlbCwgdmFsdWUsIGRpc2FibGVkOiBvcHRpb25EaXNhYmxlZCB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cbkhlbGlvc2NvcGVQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlU3R5bGVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgSGVsaW9zY29wZVBhbmVsO1xuIl19