"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../../components/ColorPickerInput"));

var _styled = require("../../../../../components/styled");

var _utils = require("../../../../../helpers/utils");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var InputContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    margin: 4px 0;\n"])));

var StyledInputGroup = (0, _styledComponents.default)(_styled.FlexRow)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n"])));

var Label = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var StyledInput = _styledComponents.default.input(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    width: 92px;\n    font-size: 13px;\n    background-color: rgba(255, 255, 255, 0.02);\n    border-radius: 3px;\n    border: 1px solid #eff1fb;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    color: #848bab;\n    padding: 4px 0 4px 6px;\n    font-weight: 400;\n    line-height: 1.5;\n"])));

var ColEditableText = _styledComponents.default.span(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    font-size: 13px;\n    font-style: italic;\n    width: 100px;\n"])));

var IconButton = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    border-radius: 50%;\n    text-align: center;\n    cursor: pointer;\n    margin: 4px;\n    min-width: 14px;\n    i {\n        display: block;\n        font-size: 14px;\n        color: ", ";\n    }\n"])), function (_ref) {
  var color = _ref.color;
  return color;
});

var ValuesTab = function ValuesTab() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref2.handleValueChange,
      _ref2$value = _ref2.value,
      value = _ref2$value === void 0 ? {} : _ref2$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var series = value.series;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      editIndex = _useState2[0],
      setEditIndex = _useState2[1];

  var _useState3 = (0, _react.useState)(''),
      _useState4 = _slicedToArray(_useState3, 2),
      editableTextField = _useState4[0],
      setEditableTextField = _useState4[1];

  var onChangeEdtitableTextField = function onChangeEdtitableTextField(event) {
    setEditableTextField(event.target.value);
  };

  var cancelEditMode = function cancelEditMode() {
    setEditIndex(null);
    setEditableTextField('');
  };

  var handleSave = function handleSave() {
    var tempSeries = (0, _utils.cloneElement)(series);
    tempSeries[editIndex].editable_label = editableTextField;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      series: tempSeries
    }));
    setEditIndex(null);
    setEditableTextField('');
  };

  var onChangeValueVisibilty = function onChangeValueVisibilty(index, visible) {
    var tempSeries = (0, _utils.cloneElement)(series);
    tempSeries[index].visible = visible;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      series: tempSeries
    }));
  };

  var onChangeValueColor = function onChangeValueColor(index, color) {
    var tempSeries = (0, _utils.cloneElement)(series);
    tempSeries[index].color = color;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      series: tempSeries
    }));
  };

  var prepareEditMode = function prepareEditMode(id, label) {
    setEditIndex(id);
    setEditableTextField(label);
  };

  var renderEditText = function renderEditText(index, editable_label, name) {
    if (editIndex === index) {
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInput, {
          onChange: onChangeEdtitableTextField,
          value: editableTextField
        }), ' ', /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#008dff",
          onClick: handleSave,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-save"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#ff0000",
          onClick: cancelEditMode,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-times"
          })
        })]
      });
    }

    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ColEditableText, {
        children: editable_label || name
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
        color: "#008dff",
        onClick: function onClick() {
          return prepareEditMode(index, editable_label || name);
        },
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-edit"
        })
      })]
    });
  };

  var renderResult = function renderResult(index, serie) {
    var name = serie.name,
        visible = serie.visible,
        _serie$color = serie.color,
        color = _serie$color === void 0 ? '#ff0000' : _serie$color,
        editable_label = serie.editable_label;
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: name
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Text')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
          alignItems: "center",
          children: renderEditText(index, editable_label, name)
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Visible')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: "stacked-bar",
          onChange: function onChange(e) {
            return onChangeValueVisibilty(index, e.target.checked);
          },
          checked: visible
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          label: t('Color'),
          value: color,
          onChange: function onChange(tempColor) {
            return onChangeValueColor(index, tempColor);
          }
        })
      })]
    }, "row-data-".concat(name));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexColumn, {
    children: series.map(function (serie, index) {
      return renderResult(index, serie);
    })
  });
};

ValuesTab.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.object
};
var _default = ValuesTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0NoYXJ0UGFuZWwvVmFsdWVzVGFiLmpzIl0sIm5hbWVzIjpbIklucHV0Q29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiU3R5bGVkSW5wdXRHcm91cCIsIkZsZXhSb3ciLCJMYWJlbCIsInNwYW4iLCJTdHlsZWRJbnB1dCIsImlucHV0IiwiQ29sRWRpdGFibGVUZXh0IiwiSWNvbkJ1dHRvbiIsImNvbG9yIiwiVmFsdWVzVGFiIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJ2YWx1ZSIsInQiLCJzZXJpZXMiLCJlZGl0SW5kZXgiLCJzZXRFZGl0SW5kZXgiLCJlZGl0YWJsZVRleHRGaWVsZCIsInNldEVkaXRhYmxlVGV4dEZpZWxkIiwib25DaGFuZ2VFZHRpdGFibGVUZXh0RmllbGQiLCJldmVudCIsInRhcmdldCIsImNhbmNlbEVkaXRNb2RlIiwiaGFuZGxlU2F2ZSIsInRlbXBTZXJpZXMiLCJlZGl0YWJsZV9sYWJlbCIsIm9uQ2hhbmdlVmFsdWVWaXNpYmlsdHkiLCJpbmRleCIsInZpc2libGUiLCJvbkNoYW5nZVZhbHVlQ29sb3IiLCJwcmVwYXJlRWRpdE1vZGUiLCJpZCIsImxhYmVsIiwicmVuZGVyRWRpdFRleHQiLCJuYW1lIiwicmVuZGVyUmVzdWx0Iiwic2VyaWUiLCJlIiwiY2hlY2tlZCIsInRlbXBDb2xvciIsIm1hcCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLGNBQWMsR0FBR0MsMEJBQU9DLEdBQVYseUlBQXBCOztBQU1BLElBQU1DLGdCQUFnQixHQUFHLCtCQUFPQyxlQUFQLENBQUgsK0dBQXRCOztBQUtBLElBQU1DLEtBQUssR0FBR0osMEJBQU9LLElBQVYsdU9BQVg7O0FBVUEsSUFBTUMsV0FBVyxHQUFHTiwwQkFBT08sS0FBVixxWEFBakI7O0FBYUEsSUFBTUMsZUFBZSxHQUFHUiwwQkFBT0ssSUFBViwySUFBckI7O0FBTUEsSUFBTUksVUFBVSxHQUFHVCwwQkFBT0MsR0FBVixtUkFTQztBQUFBLE1BQUdTLEtBQUgsUUFBR0EsS0FBSDtBQUFBLFNBQWVBLEtBQWY7QUFBQSxDQVRELENBQWhCOztBQWFBLElBQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFZLEdBQTRDO0FBQUEsa0ZBQVAsRUFBTztBQUFBLE1BQXpDQyxpQkFBeUMsU0FBekNBLGlCQUF5QztBQUFBLDBCQUF0QkMsS0FBc0I7QUFBQSxNQUF0QkEsS0FBc0IsNEJBQWQsRUFBYzs7QUFDMUQsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQVFDLE1BQVIsR0FBbUJGLEtBQW5CLENBQVFFLE1BQVI7O0FBQ0Esa0JBQWtDLHFCQUFTLElBQVQsQ0FBbEM7QUFBQTtBQUFBLE1BQU9DLFNBQVA7QUFBQSxNQUFrQkMsWUFBbEI7O0FBQ0EsbUJBQWtELHFCQUFTLEVBQVQsQ0FBbEQ7QUFBQTtBQUFBLE1BQU9DLGlCQUFQO0FBQUEsTUFBMEJDLG9CQUExQjs7QUFFQSxNQUFNQywwQkFBMEIsR0FBRyxTQUE3QkEsMEJBQTZCLENBQUNDLEtBQUQsRUFBVztBQUMxQ0YsSUFBQUEsb0JBQW9CLENBQUNFLEtBQUssQ0FBQ0MsTUFBTixDQUFhVCxLQUFkLENBQXBCO0FBQ0gsR0FGRDs7QUFJQSxNQUFNVSxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLEdBQU07QUFDekJOLElBQUFBLFlBQVksQ0FBQyxJQUFELENBQVo7QUFDQUUsSUFBQUEsb0JBQW9CLENBQUMsRUFBRCxDQUFwQjtBQUNILEdBSEQ7O0FBS0EsTUFBTUssVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBTTtBQUNyQixRQUFNQyxVQUFVLEdBQUcseUJBQWFWLE1BQWIsQ0FBbkI7QUFDQVUsSUFBQUEsVUFBVSxDQUFDVCxTQUFELENBQVYsQ0FBc0JVLGNBQXRCLEdBQXVDUixpQkFBdkM7QUFDQU4sSUFBQUEsaUJBQWlCLGlDQUFNQyxLQUFOO0FBQWFFLE1BQUFBLE1BQU0sRUFBRVU7QUFBckIsT0FBakI7QUFDQVIsSUFBQUEsWUFBWSxDQUFDLElBQUQsQ0FBWjtBQUNBRSxJQUFBQSxvQkFBb0IsQ0FBQyxFQUFELENBQXBCO0FBQ0gsR0FORDs7QUFRQSxNQUFNUSxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNDLEtBQUQsRUFBUUMsT0FBUixFQUFvQjtBQUMvQyxRQUFNSixVQUFVLEdBQUcseUJBQWFWLE1BQWIsQ0FBbkI7QUFDQVUsSUFBQUEsVUFBVSxDQUFDRyxLQUFELENBQVYsQ0FBa0JDLE9BQWxCLEdBQTRCQSxPQUE1QjtBQUNBakIsSUFBQUEsaUJBQWlCLGlDQUFNQyxLQUFOO0FBQWFFLE1BQUFBLE1BQU0sRUFBRVU7QUFBckIsT0FBakI7QUFDSCxHQUpEOztBQU1BLE1BQU1LLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0YsS0FBRCxFQUFRbEIsS0FBUixFQUFrQjtBQUN6QyxRQUFNZSxVQUFVLEdBQUcseUJBQWFWLE1BQWIsQ0FBbkI7QUFDQVUsSUFBQUEsVUFBVSxDQUFDRyxLQUFELENBQVYsQ0FBa0JsQixLQUFsQixHQUEwQkEsS0FBMUI7QUFDQUUsSUFBQUEsaUJBQWlCLGlDQUFNQyxLQUFOO0FBQWFFLE1BQUFBLE1BQU0sRUFBRVU7QUFBckIsT0FBakI7QUFDSCxHQUpEOztBQU1BLE1BQU1NLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ0MsRUFBRCxFQUFLQyxLQUFMLEVBQWU7QUFDbkNoQixJQUFBQSxZQUFZLENBQUNlLEVBQUQsQ0FBWjtBQUNBYixJQUFBQSxvQkFBb0IsQ0FBQ2MsS0FBRCxDQUFwQjtBQUNILEdBSEQ7O0FBS0EsTUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDTixLQUFELEVBQVFGLGNBQVIsRUFBd0JTLElBQXhCLEVBQWlDO0FBQ3BELFFBQUluQixTQUFTLEtBQUtZLEtBQWxCLEVBQXlCO0FBQ3JCLDBCQUNJO0FBQUEsZ0NBQ0kscUJBQUMsV0FBRDtBQUNJLFVBQUEsUUFBUSxFQUFFUiwwQkFEZDtBQUVJLFVBQUEsS0FBSyxFQUFFRjtBQUZYLFVBREosRUFJTyxHQUpQLGVBS0kscUJBQUMsVUFBRDtBQUFZLFVBQUEsS0FBSyxFQUFDLFNBQWxCO0FBQTRCLFVBQUEsT0FBTyxFQUFFTSxVQUFyQztBQUFBLGlDQUNJO0FBQUcsWUFBQSxTQUFTLEVBQUM7QUFBYjtBQURKLFVBTEosZUFRSSxxQkFBQyxVQUFEO0FBQVksVUFBQSxLQUFLLEVBQUMsU0FBbEI7QUFBNEIsVUFBQSxPQUFPLEVBQUVELGNBQXJDO0FBQUEsaUNBQ0k7QUFBRyxZQUFBLFNBQVMsRUFBQztBQUFiO0FBREosVUFSSjtBQUFBLFFBREo7QUFjSDs7QUFDRCx3QkFDSTtBQUFBLDhCQUNJLHFCQUFDLGVBQUQ7QUFBQSxrQkFBa0JHLGNBQWMsSUFBSVM7QUFBcEMsUUFESixlQUVJLHFCQUFDLFVBQUQ7QUFDSSxRQUFBLEtBQUssRUFBQyxTQURWO0FBRUksUUFBQSxPQUFPLEVBQUU7QUFBQSxpQkFDTEosZUFBZSxDQUFDSCxLQUFELEVBQVFGLGNBQWMsSUFBSVMsSUFBMUIsQ0FEVjtBQUFBLFNBRmI7QUFBQSwrQkFNSTtBQUFHLFVBQUEsU0FBUyxFQUFDO0FBQWI7QUFOSixRQUZKO0FBQUEsTUFESjtBQWFILEdBOUJEOztBQWdDQSxNQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDUixLQUFELEVBQVFTLEtBQVIsRUFBa0I7QUFDbkMsUUFBUUYsSUFBUixHQUE2REUsS0FBN0QsQ0FBUUYsSUFBUjtBQUFBLFFBQWNOLE9BQWQsR0FBNkRRLEtBQTdELENBQWNSLE9BQWQ7QUFBQSx1QkFBNkRRLEtBQTdELENBQXVCM0IsS0FBdkI7QUFBQSxRQUF1QkEsS0FBdkIsNkJBQStCLFNBQS9CO0FBQUEsUUFBMENnQixjQUExQyxHQUE2RFcsS0FBN0QsQ0FBMENYLGNBQTFDO0FBQ0Esd0JBQ0ksc0JBQUMsa0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxlQUFEO0FBQUEsa0JBQVVTO0FBQVYsUUFESixlQUVJLHNCQUFDLGNBQUQ7QUFBQSxnQ0FDSSxxQkFBQyxLQUFEO0FBQUEsb0JBQVFyQixDQUFDLENBQUMsTUFBRDtBQUFULFVBREosZUFFSSxxQkFBQyxnQkFBRDtBQUFrQixVQUFBLFVBQVUsRUFBQyxRQUE3QjtBQUFBLG9CQUNLb0IsY0FBYyxDQUFDTixLQUFELEVBQVFGLGNBQVIsRUFBd0JTLElBQXhCO0FBRG5CLFVBRko7QUFBQSxRQUZKLGVBUUksc0JBQUMsY0FBRDtBQUFBLGdDQUNJLHFCQUFDLEtBQUQ7QUFBQSxvQkFBUXJCLENBQUMsQ0FBQyxTQUFEO0FBQVQsVUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksVUFBQSxFQUFFLEVBQUMsYUFEUDtBQUVJLFVBQUEsUUFBUSxFQUFFLGtCQUFDd0IsQ0FBRDtBQUFBLG1CQUNOWCxzQkFBc0IsQ0FBQ0MsS0FBRCxFQUFRVSxDQUFDLENBQUNoQixNQUFGLENBQVNpQixPQUFqQixDQURoQjtBQUFBLFdBRmQ7QUFLSSxVQUFBLE9BQU8sRUFBRVY7QUFMYixVQUZKO0FBQUEsUUFSSixlQWtCSSxxQkFBQyxjQUFEO0FBQUEsK0JBQ0kscUJBQUMseUJBQUQ7QUFDSSxVQUFBLEtBQUssRUFBRWYsQ0FBQyxDQUFDLE9BQUQsQ0FEWjtBQUVJLFVBQUEsS0FBSyxFQUFFSixLQUZYO0FBR0ksVUFBQSxRQUFRLEVBQUUsa0JBQUM4QixTQUFEO0FBQUEsbUJBQ05WLGtCQUFrQixDQUFDRixLQUFELEVBQVFZLFNBQVIsQ0FEWjtBQUFBO0FBSGQ7QUFESixRQWxCSjtBQUFBLDBCQUE2QkwsSUFBN0IsRUFESjtBQThCSCxHQWhDRDs7QUFrQ0Esc0JBQ0kscUJBQUMsa0JBQUQ7QUFBQSxjQUNLcEIsTUFBTSxDQUFDMEIsR0FBUCxDQUFXLFVBQUNKLEtBQUQsRUFBUVQsS0FBUjtBQUFBLGFBQWtCUSxZQUFZLENBQUNSLEtBQUQsRUFBUVMsS0FBUixDQUE5QjtBQUFBLEtBQVg7QUFETCxJQURKO0FBS0gsQ0EvR0Q7O0FBaUhBMUIsU0FBUyxDQUFDK0IsU0FBVixHQUFzQjtBQUNsQjlCLEVBQUFBLGlCQUFpQixFQUFFK0IsbUJBQVVDLElBRFg7QUFFbEIvQixFQUFBQSxLQUFLLEVBQUU4QixtQkFBVUU7QUFGQyxDQUF0QjtlQUtlbEMsUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IENvbG9yUGlja2VySW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvQ29sb3JQaWNrZXJJbnB1dCc7XG5pbXBvcnQgeyBGbGV4Q29sdW1uLCBGbGV4Um93IH0gZnJvbSAnQGNvbXBvbmVudHMvc3R5bGVkJztcblxuaW1wb3J0IHsgY2xvbmVFbGVtZW50IH0gZnJvbSAnQGhlbHBlcnMvdXRpbHMnO1xuXG5pbXBvcnQgeyBDaGVja2JveElucHV0LCBTdWJtZW51IH0gZnJvbSAnLi4vU3R5bGVzJztcblxuY29uc3QgSW5wdXRDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDRweCAwO1xuYDtcblxuY29uc3QgU3R5bGVkSW5wdXRHcm91cCA9IHN0eWxlZChGbGV4Um93KWBcbiAgICBoZWlnaHQ6IDQ2cHg7XG4gICAgd2lkdGg6IDEzM3B4O1xuYDtcblxuY29uc3QgTGFiZWwgPSBzdHlsZWQuc3BhbmBcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAyM3B4O1xuICAgIG1pbi1oZWlnaHQ6IDE2cHg7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgd2lkdGg6IDEzNXB4O1xuYDtcblxuY29uc3QgU3R5bGVkSW5wdXQgPSBzdHlsZWQuaW5wdXRgXG4gICAgd2lkdGg6IDkycHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4wMik7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmYxZmI7XG4gICAgYm94LXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMTI5LCAxNTgsIDIwMCwgMC4wNik7XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgcGFkZGluZzogNHB4IDAgNHB4IDZweDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG5gO1xuXG5jb25zdCBDb2xFZGl0YWJsZVRleHQgPSBzdHlsZWQuc3BhbmBcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC1zdHlsZTogaXRhbGljO1xuICAgIHdpZHRoOiAxMDBweDtcbmA7XG5cbmNvbnN0IEljb25CdXR0b24gPSBzdHlsZWQuZGl2YFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbjogNHB4O1xuICAgIG1pbi13aWR0aDogMTRweDtcbiAgICBpIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICR7KHsgY29sb3IgfSkgPT4gY29sb3J9O1xuICAgIH1cbmA7XG5cbmNvbnN0IFZhbHVlc1RhYiA9ICh7IGhhbmRsZVZhbHVlQ2hhbmdlLCB2YWx1ZSA9IHt9IH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCB7IHNlcmllcyB9ID0gdmFsdWU7XG4gICAgY29uc3QgW2VkaXRJbmRleCwgc2V0RWRpdEluZGV4XSA9IHVzZVN0YXRlKG51bGwpO1xuICAgIGNvbnN0IFtlZGl0YWJsZVRleHRGaWVsZCwgc2V0RWRpdGFibGVUZXh0RmllbGRdID0gdXNlU3RhdGUoJycpO1xuXG4gICAgY29uc3Qgb25DaGFuZ2VFZHRpdGFibGVUZXh0RmllbGQgPSAoZXZlbnQpID0+IHtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQoZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICB9O1xuXG4gICAgY29uc3QgY2FuY2VsRWRpdE1vZGUgPSAoKSA9PiB7XG4gICAgICAgIHNldEVkaXRJbmRleChudWxsKTtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQoJycpO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVTYXZlID0gKCkgPT4ge1xuICAgICAgICBjb25zdCB0ZW1wU2VyaWVzID0gY2xvbmVFbGVtZW50KHNlcmllcyk7XG4gICAgICAgIHRlbXBTZXJpZXNbZWRpdEluZGV4XS5lZGl0YWJsZV9sYWJlbCA9IGVkaXRhYmxlVGV4dEZpZWxkO1xuICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZSh7IC4uLnZhbHVlLCBzZXJpZXM6IHRlbXBTZXJpZXMgfSk7XG4gICAgICAgIHNldEVkaXRJbmRleChudWxsKTtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQoJycpO1xuICAgIH07XG5cbiAgICBjb25zdCBvbkNoYW5nZVZhbHVlVmlzaWJpbHR5ID0gKGluZGV4LCB2aXNpYmxlKSA9PiB7XG4gICAgICAgIGNvbnN0IHRlbXBTZXJpZXMgPSBjbG9uZUVsZW1lbnQoc2VyaWVzKTtcbiAgICAgICAgdGVtcFNlcmllc1tpbmRleF0udmlzaWJsZSA9IHZpc2libGU7XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHsgLi4udmFsdWUsIHNlcmllczogdGVtcFNlcmllcyB9KTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VWYWx1ZUNvbG9yID0gKGluZGV4LCBjb2xvcikgPT4ge1xuICAgICAgICBjb25zdCB0ZW1wU2VyaWVzID0gY2xvbmVFbGVtZW50KHNlcmllcyk7XG4gICAgICAgIHRlbXBTZXJpZXNbaW5kZXhdLmNvbG9yID0gY29sb3I7XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHsgLi4udmFsdWUsIHNlcmllczogdGVtcFNlcmllcyB9KTtcbiAgICB9O1xuXG4gICAgY29uc3QgcHJlcGFyZUVkaXRNb2RlID0gKGlkLCBsYWJlbCkgPT4ge1xuICAgICAgICBzZXRFZGl0SW5kZXgoaWQpO1xuICAgICAgICBzZXRFZGl0YWJsZVRleHRGaWVsZChsYWJlbCk7XG4gICAgfTtcblxuICAgIGNvbnN0IHJlbmRlckVkaXRUZXh0ID0gKGluZGV4LCBlZGl0YWJsZV9sYWJlbCwgbmFtZSkgPT4ge1xuICAgICAgICBpZiAoZWRpdEluZGV4ID09PSBpbmRleCkge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZUVkdGl0YWJsZVRleHRGaWVsZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtlZGl0YWJsZVRleHRGaWVsZH1cbiAgICAgICAgICAgICAgICAgICAgLz57JyAnfVxuICAgICAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBjb2xvcj1cIiMwMDhkZmZcIiBvbkNsaWNrPXtoYW5kbGVTYXZlfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1zYXZlXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBjb2xvcj1cIiNmZjAwMDBcIiBvbkNsaWNrPXtjYW5jZWxFZGl0TW9kZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtdGltZXNcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgIDxDb2xFZGl0YWJsZVRleHQ+e2VkaXRhYmxlX2xhYmVsIHx8IG5hbWV9PC9Db2xFZGl0YWJsZVRleHQ+XG4gICAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICAgICAgY29sb3I9XCIjMDA4ZGZmXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXBhcmVFZGl0TW9kZShpbmRleCwgZWRpdGFibGVfbGFiZWwgfHwgbmFtZSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWVkaXRcIiAvPlxuICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvPlxuICAgICAgICApO1xuICAgIH07XG5cbiAgICBjb25zdCByZW5kZXJSZXN1bHQgPSAoaW5kZXgsIHNlcmllKSA9PiB7XG4gICAgICAgIGNvbnN0IHsgbmFtZSwgdmlzaWJsZSwgY29sb3IgPSAnI2ZmMDAwMCcsIGVkaXRhYmxlX2xhYmVsIH0gPSBzZXJpZTtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxGbGV4Q29sdW1uIGtleT17YHJvdy1kYXRhLSR7bmFtZX1gfT5cbiAgICAgICAgICAgICAgICA8U3VibWVudT57bmFtZX08L1N1Ym1lbnU+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1RleHQnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cCBhbGlnbkl0ZW1zPVwiY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICB7cmVuZGVyRWRpdFRleHQoaW5kZXgsIGVkaXRhYmxlX2xhYmVsLCBuYW1lKX1cbiAgICAgICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1Zpc2libGUnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8Q2hlY2tib3hJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzdGFja2VkLWJhclwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2VWYWx1ZVZpc2liaWx0eShpbmRleCwgZS50YXJnZXQuY2hlY2tlZClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3Zpc2libGV9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQ29sb3InKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtjb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlVmFsdWVDb2xvcihpbmRleCwgdGVtcENvbG9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8L0ZsZXhDb2x1bW4+XG4gICAgICAgICk7XG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxGbGV4Q29sdW1uPlxuICAgICAgICAgICAge3Nlcmllcy5tYXAoKHNlcmllLCBpbmRleCkgPT4gcmVuZGVyUmVzdWx0KGluZGV4LCBzZXJpZSkpfVxuICAgICAgICA8L0ZsZXhDb2x1bW4+XG4gICAgKTtcbn07XG5cblZhbHVlc1RhYi5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgVmFsdWVzVGFiO1xuIl19