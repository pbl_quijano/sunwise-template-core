"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../../components/ColorPickerInput"));

var _styled = require("../../../../../components/styled");

var _utils = require("../../../../../helpers/utils");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var InputContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    margin: 4px 0;\n"])));

var StyledInputGroup = (0, _styledComponents.default)(_styled.FlexRow)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n"])));

var Label = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var StyledInput = _styledComponents.default.input(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    width: 92px;\n    font-size: 13px;\n    background-color: rgba(255, 255, 255, 0.02);\n    border-radius: 3px;\n    border: 1px solid #eff1fb;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    color: #848bab;\n    padding: 4px 0 4px 6px;\n    font-weight: 400;\n    line-height: 1.5;\n"])));

var ColEditableText = _styledComponents.default.span(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    font-size: 13px;\n    font-style: italic;\n    width: 100px;\n"])));

var IconButton = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    border-radius: 50%;\n    text-align: center;\n    cursor: pointer;\n    margin: 4px;\n    min-width: 14px;\n    i {\n        display: block;\n        font-size: 14px;\n        color: ", ";\n    }\n"])), function (_ref) {
  var color = _ref.color;
  return color;
});

var AnnotationsTab = function AnnotationsTab() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref2.handleValueChange,
      _ref2$value = _ref2.value,
      value = _ref2$value === void 0 ? {} : _ref2$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _value$annotations = value.annotations,
      annotations = _value$annotations === void 0 ? [] : _value$annotations;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      editIndex = _useState2[0],
      setEditIndex = _useState2[1];

  var _useState3 = (0, _react.useState)(''),
      _useState4 = _slicedToArray(_useState3, 2),
      editableTextField = _useState4[0],
      setEditableTextField = _useState4[1];

  var onChangeEdtitableTextField = function onChangeEdtitableTextField(event) {
    setEditableTextField(event.target.value);
  };

  var cancelEditMode = function cancelEditMode() {
    setEditIndex(null);
    setEditableTextField('');
  };

  var handleSave = function handleSave() {
    var tempAnnotations = (0, _utils.cloneElement)(annotations);
    tempAnnotations[editIndex].editable_label = editableTextField;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      annotations: tempAnnotations
    }));
    setEditIndex(null);
    setEditableTextField('');
  };

  var onChangeValueVisibilty = function onChangeValueVisibilty(index, visible) {
    var tempAnnotations = (0, _utils.cloneElement)(annotations);
    tempAnnotations[index].visible = visible;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      annotations: tempAnnotations
    }));
  };

  var onChangeValueColor = function onChangeValueColor(index, color) {
    var tempAnnotations = (0, _utils.cloneElement)(annotations);
    tempAnnotations[index].color = color;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      annotations: tempAnnotations
    }));
  };

  var prepareEditMode = function prepareEditMode(id, label) {
    setEditIndex(id);
    setEditableTextField(label);
  };

  var renderEditText = function renderEditText(index, editable_label, label) {
    if (editIndex === index) {
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInput, {
          onChange: onChangeEdtitableTextField,
          value: editableTextField
        }), ' ', /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#008dff",
          onClick: handleSave,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-save"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#ff0000",
          onClick: cancelEditMode,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-times"
          })
        })]
      });
    }

    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ColEditableText, {
        children: editable_label || label
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
        color: "#008dff",
        onClick: function onClick() {
          return prepareEditMode(index, editable_label || label);
        },
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-edit"
        })
      })]
    });
  };

  var renderResult = function renderResult(index, serie) {
    var label = serie.label,
        visible = serie.visible,
        _serie$color = serie.color,
        color = _serie$color === void 0 ? '#ff0000' : _serie$color,
        editable_label = serie.editable_label;
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: label
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Text')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
          alignItems: "center",
          children: renderEditText(index, editable_label, label)
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Visible')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: "stacked-bar",
          onChange: function onChange(e) {
            return onChangeValueVisibilty(index, e.target.checked);
          },
          checked: visible
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          label: t('Color'),
          value: color,
          onChange: function onChange(tempColor) {
            return onChangeValueColor(index, tempColor);
          }
        })
      })]
    }, "row-data-".concat(label));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexColumn, {
    children: annotations.map(function (annotation, index) {
      return renderResult(index, annotation);
    })
  });
};

AnnotationsTab.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.object
};
var _default = AnnotationsTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0NoYXJ0UGFuZWwvQW5ub3RhdGlvbnNUYWIuanMiXSwibmFtZXMiOlsiSW5wdXRDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJTdHlsZWRJbnB1dEdyb3VwIiwiRmxleFJvdyIsIkxhYmVsIiwic3BhbiIsIlN0eWxlZElucHV0IiwiaW5wdXQiLCJDb2xFZGl0YWJsZVRleHQiLCJJY29uQnV0dG9uIiwiY29sb3IiLCJBbm5vdGF0aW9uc1RhYiIsImhhbmRsZVZhbHVlQ2hhbmdlIiwidmFsdWUiLCJ0IiwiYW5ub3RhdGlvbnMiLCJlZGl0SW5kZXgiLCJzZXRFZGl0SW5kZXgiLCJlZGl0YWJsZVRleHRGaWVsZCIsInNldEVkaXRhYmxlVGV4dEZpZWxkIiwib25DaGFuZ2VFZHRpdGFibGVUZXh0RmllbGQiLCJldmVudCIsInRhcmdldCIsImNhbmNlbEVkaXRNb2RlIiwiaGFuZGxlU2F2ZSIsInRlbXBBbm5vdGF0aW9ucyIsImVkaXRhYmxlX2xhYmVsIiwib25DaGFuZ2VWYWx1ZVZpc2liaWx0eSIsImluZGV4IiwidmlzaWJsZSIsIm9uQ2hhbmdlVmFsdWVDb2xvciIsInByZXBhcmVFZGl0TW9kZSIsImlkIiwibGFiZWwiLCJyZW5kZXJFZGl0VGV4dCIsInJlbmRlclJlc3VsdCIsInNlcmllIiwiZSIsImNoZWNrZWQiLCJ0ZW1wQ29sb3IiLCJtYXAiLCJhbm5vdGF0aW9uIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsY0FBYyxHQUFHQywwQkFBT0MsR0FBVix5SUFBcEI7O0FBTUEsSUFBTUMsZ0JBQWdCLEdBQUcsK0JBQU9DLGVBQVAsQ0FBSCwrR0FBdEI7O0FBS0EsSUFBTUMsS0FBSyxHQUFHSiwwQkFBT0ssSUFBVix1T0FBWDs7QUFVQSxJQUFNQyxXQUFXLEdBQUdOLDBCQUFPTyxLQUFWLHFYQUFqQjs7QUFhQSxJQUFNQyxlQUFlLEdBQUdSLDBCQUFPSyxJQUFWLDJJQUFyQjs7QUFNQSxJQUFNSSxVQUFVLEdBQUdULDBCQUFPQyxHQUFWLG1SQVNDO0FBQUEsTUFBR1MsS0FBSCxRQUFHQSxLQUFIO0FBQUEsU0FBZUEsS0FBZjtBQUFBLENBVEQsQ0FBaEI7O0FBYUEsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixHQUE0QztBQUFBLGtGQUFQLEVBQU87QUFBQSxNQUF6Q0MsaUJBQXlDLFNBQXpDQSxpQkFBeUM7QUFBQSwwQkFBdEJDLEtBQXNCO0FBQUEsTUFBdEJBLEtBQXNCLDRCQUFkLEVBQWM7O0FBQy9ELHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSwyQkFBNkJELEtBQTdCLENBQVFFLFdBQVI7QUFBQSxNQUFRQSxXQUFSLG1DQUFzQixFQUF0Qjs7QUFDQSxrQkFBa0MscUJBQVMsSUFBVCxDQUFsQztBQUFBO0FBQUEsTUFBT0MsU0FBUDtBQUFBLE1BQWtCQyxZQUFsQjs7QUFDQSxtQkFBa0QscUJBQVMsRUFBVCxDQUFsRDtBQUFBO0FBQUEsTUFBT0MsaUJBQVA7QUFBQSxNQUEwQkMsb0JBQTFCOztBQUVBLE1BQU1DLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsQ0FBQ0MsS0FBRCxFQUFXO0FBQzFDRixJQUFBQSxvQkFBb0IsQ0FBQ0UsS0FBSyxDQUFDQyxNQUFOLENBQWFULEtBQWQsQ0FBcEI7QUFDSCxHQUZEOztBQUlBLE1BQU1VLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUN6Qk4sSUFBQUEsWUFBWSxDQUFDLElBQUQsQ0FBWjtBQUNBRSxJQUFBQSxvQkFBb0IsQ0FBQyxFQUFELENBQXBCO0FBQ0gsR0FIRDs7QUFLQSxNQUFNSyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFNO0FBQ3JCLFFBQU1DLGVBQWUsR0FBRyx5QkFBYVYsV0FBYixDQUF4QjtBQUNBVSxJQUFBQSxlQUFlLENBQUNULFNBQUQsQ0FBZixDQUEyQlUsY0FBM0IsR0FBNENSLGlCQUE1QztBQUNBTixJQUFBQSxpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUUsTUFBQUEsV0FBVyxFQUFFVTtBQUExQixPQUFqQjtBQUNBUixJQUFBQSxZQUFZLENBQUMsSUFBRCxDQUFaO0FBQ0FFLElBQUFBLG9CQUFvQixDQUFDLEVBQUQsQ0FBcEI7QUFDSCxHQU5EOztBQVFBLE1BQU1RLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBQ0MsS0FBRCxFQUFRQyxPQUFSLEVBQW9CO0FBQy9DLFFBQU1KLGVBQWUsR0FBRyx5QkFBYVYsV0FBYixDQUF4QjtBQUNBVSxJQUFBQSxlQUFlLENBQUNHLEtBQUQsQ0FBZixDQUF1QkMsT0FBdkIsR0FBaUNBLE9BQWpDO0FBQ0FqQixJQUFBQSxpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUUsTUFBQUEsV0FBVyxFQUFFVTtBQUExQixPQUFqQjtBQUNILEdBSkQ7O0FBTUEsTUFBTUssa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDRixLQUFELEVBQVFsQixLQUFSLEVBQWtCO0FBQ3pDLFFBQU1lLGVBQWUsR0FBRyx5QkFBYVYsV0FBYixDQUF4QjtBQUNBVSxJQUFBQSxlQUFlLENBQUNHLEtBQUQsQ0FBZixDQUF1QmxCLEtBQXZCLEdBQStCQSxLQUEvQjtBQUNBRSxJQUFBQSxpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUUsTUFBQUEsV0FBVyxFQUFFVTtBQUExQixPQUFqQjtBQUNILEdBSkQ7O0FBTUEsTUFBTU0sZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxFQUFELEVBQUtDLEtBQUwsRUFBZTtBQUNuQ2hCLElBQUFBLFlBQVksQ0FBQ2UsRUFBRCxDQUFaO0FBQ0FiLElBQUFBLG9CQUFvQixDQUFDYyxLQUFELENBQXBCO0FBQ0gsR0FIRDs7QUFLQSxNQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNOLEtBQUQsRUFBUUYsY0FBUixFQUF3Qk8sS0FBeEIsRUFBa0M7QUFDckQsUUFBSWpCLFNBQVMsS0FBS1ksS0FBbEIsRUFBeUI7QUFDckIsMEJBQ0k7QUFBQSxnQ0FDSSxxQkFBQyxXQUFEO0FBQ0ksVUFBQSxRQUFRLEVBQUVSLDBCQURkO0FBRUksVUFBQSxLQUFLLEVBQUVGO0FBRlgsVUFESixFQUlPLEdBSlAsZUFLSSxxQkFBQyxVQUFEO0FBQVksVUFBQSxLQUFLLEVBQUMsU0FBbEI7QUFBNEIsVUFBQSxPQUFPLEVBQUVNLFVBQXJDO0FBQUEsaUNBQ0k7QUFBRyxZQUFBLFNBQVMsRUFBQztBQUFiO0FBREosVUFMSixlQVFJLHFCQUFDLFVBQUQ7QUFBWSxVQUFBLEtBQUssRUFBQyxTQUFsQjtBQUE0QixVQUFBLE9BQU8sRUFBRUQsY0FBckM7QUFBQSxpQ0FDSTtBQUFHLFlBQUEsU0FBUyxFQUFDO0FBQWI7QUFESixVQVJKO0FBQUEsUUFESjtBQWNIOztBQUNELHdCQUNJO0FBQUEsOEJBQ0kscUJBQUMsZUFBRDtBQUFBLGtCQUFrQkcsY0FBYyxJQUFJTztBQUFwQyxRQURKLGVBRUkscUJBQUMsVUFBRDtBQUNJLFFBQUEsS0FBSyxFQUFDLFNBRFY7QUFFSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUNMRixlQUFlLENBQUNILEtBQUQsRUFBUUYsY0FBYyxJQUFJTyxLQUExQixDQURWO0FBQUEsU0FGYjtBQUFBLCtCQU1JO0FBQUcsVUFBQSxTQUFTLEVBQUM7QUFBYjtBQU5KLFFBRko7QUFBQSxNQURKO0FBYUgsR0E5QkQ7O0FBZ0NBLE1BQU1FLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNQLEtBQUQsRUFBUVEsS0FBUixFQUFrQjtBQUNuQyxRQUFRSCxLQUFSLEdBQThERyxLQUE5RCxDQUFRSCxLQUFSO0FBQUEsUUFBZUosT0FBZixHQUE4RE8sS0FBOUQsQ0FBZVAsT0FBZjtBQUFBLHVCQUE4RE8sS0FBOUQsQ0FBd0IxQixLQUF4QjtBQUFBLFFBQXdCQSxLQUF4Qiw2QkFBZ0MsU0FBaEM7QUFBQSxRQUEyQ2dCLGNBQTNDLEdBQThEVSxLQUE5RCxDQUEyQ1YsY0FBM0M7QUFDQSx3QkFDSSxzQkFBQyxrQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGVBQUQ7QUFBQSxrQkFBVU87QUFBVixRQURKLGVBRUksc0JBQUMsY0FBRDtBQUFBLGdDQUNJLHFCQUFDLEtBQUQ7QUFBQSxvQkFBUW5CLENBQUMsQ0FBQyxNQUFEO0FBQVQsVUFESixlQUVJLHFCQUFDLGdCQUFEO0FBQWtCLFVBQUEsVUFBVSxFQUFDLFFBQTdCO0FBQUEsb0JBQ0tvQixjQUFjLENBQUNOLEtBQUQsRUFBUUYsY0FBUixFQUF3Qk8sS0FBeEI7QUFEbkIsVUFGSjtBQUFBLFFBRkosZUFRSSxzQkFBQyxjQUFEO0FBQUEsZ0NBQ0kscUJBQUMsS0FBRDtBQUFBLG9CQUFRbkIsQ0FBQyxDQUFDLFNBQUQ7QUFBVCxVQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxVQUFBLEVBQUUsRUFBQyxhQURQO0FBRUksVUFBQSxRQUFRLEVBQUUsa0JBQUN1QixDQUFEO0FBQUEsbUJBQ05WLHNCQUFzQixDQUFDQyxLQUFELEVBQVFTLENBQUMsQ0FBQ2YsTUFBRixDQUFTZ0IsT0FBakIsQ0FEaEI7QUFBQSxXQUZkO0FBS0ksVUFBQSxPQUFPLEVBQUVUO0FBTGIsVUFGSjtBQUFBLFFBUkosZUFrQkkscUJBQUMsY0FBRDtBQUFBLCtCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksVUFBQSxLQUFLLEVBQUVmLENBQUMsQ0FBQyxPQUFELENBRFo7QUFFSSxVQUFBLEtBQUssRUFBRUosS0FGWDtBQUdJLFVBQUEsUUFBUSxFQUFFLGtCQUFDNkIsU0FBRDtBQUFBLG1CQUNOVCxrQkFBa0IsQ0FBQ0YsS0FBRCxFQUFRVyxTQUFSLENBRFo7QUFBQTtBQUhkO0FBREosUUFsQko7QUFBQSwwQkFBNkJOLEtBQTdCLEVBREo7QUE4QkgsR0FoQ0Q7O0FBa0NBLHNCQUNJLHFCQUFDLGtCQUFEO0FBQUEsY0FDS2xCLFdBQVcsQ0FBQ3lCLEdBQVosQ0FBZ0IsVUFBQ0MsVUFBRCxFQUFhYixLQUFiO0FBQUEsYUFDYk8sWUFBWSxDQUFDUCxLQUFELEVBQVFhLFVBQVIsQ0FEQztBQUFBLEtBQWhCO0FBREwsSUFESjtBQU9ILENBakhEOztBQW1IQTlCLGNBQWMsQ0FBQytCLFNBQWYsR0FBMkI7QUFDdkI5QixFQUFBQSxpQkFBaUIsRUFBRStCLG1CQUFVQyxJQUROO0FBRXZCL0IsRUFBQUEsS0FBSyxFQUFFOEIsbUJBQVVFO0FBRk0sQ0FBM0I7ZUFLZWxDLGMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBDb2xvclBpY2tlcklucHV0IGZyb20gJ0Bjb21wb25lbnRzL0NvbG9yUGlja2VySW5wdXQnO1xuaW1wb3J0IHsgRmxleENvbHVtbiwgRmxleFJvdyB9IGZyb20gJ0Bjb21wb25lbnRzL3N0eWxlZCc7XG5cbmltcG9ydCB7IGNsb25lRWxlbWVudCB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcblxuaW1wb3J0IHsgQ2hlY2tib3hJbnB1dCwgU3VibWVudSB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmNvbnN0IElucHV0Q29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWFyZ2luOiA0cHggMDtcbmA7XG5cbmNvbnN0IFN0eWxlZElucHV0R3JvdXAgPSBzdHlsZWQoRmxleFJvdylgXG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIHdpZHRoOiAxMzNweDtcbmA7XG5cbmNvbnN0IExhYmVsID0gc3R5bGVkLnNwYW5gXG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1yaWdodDogMjNweDtcbiAgICBtaW4taGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHdpZHRoOiAxMzVweDtcbmA7XG5cbmNvbnN0IFN0eWxlZElucHV0ID0gc3R5bGVkLmlucHV0YFxuICAgIHdpZHRoOiA5MnB4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMDIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWZmMWZiO1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDEycHggMCByZ2JhKDEyOSwgMTU4LCAyMDAsIDAuMDYpO1xuICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgIHBhZGRpbmc6IDRweCAwIDRweCA2cHg7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuYDtcblxuY29uc3QgQ29sRWRpdGFibGVUZXh0ID0gc3R5bGVkLnNwYW5gXG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgICB3aWR0aDogMTAwcHg7XG5gO1xuXG5jb25zdCBJY29uQnV0dG9uID0gc3R5bGVkLmRpdmBcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBtYXJnaW46IDRweDtcbiAgICBtaW4td2lkdGg6IDE0cHg7XG4gICAgaSB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGNvbG9yOiAkeyh7IGNvbG9yIH0pID0+IGNvbG9yfTtcbiAgICB9XG5gO1xuXG5jb25zdCBBbm5vdGF0aW9uc1RhYiA9ICh7IGhhbmRsZVZhbHVlQ2hhbmdlLCB2YWx1ZSA9IHt9IH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCB7IGFubm90YXRpb25zID0gW10gfSA9IHZhbHVlO1xuICAgIGNvbnN0IFtlZGl0SW5kZXgsIHNldEVkaXRJbmRleF0gPSB1c2VTdGF0ZShudWxsKTtcbiAgICBjb25zdCBbZWRpdGFibGVUZXh0RmllbGQsIHNldEVkaXRhYmxlVGV4dEZpZWxkXSA9IHVzZVN0YXRlKCcnKTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlRWR0aXRhYmxlVGV4dEZpZWxkID0gKGV2ZW50KSA9PiB7XG4gICAgICAgIHNldEVkaXRhYmxlVGV4dEZpZWxkKGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgfTtcblxuICAgIGNvbnN0IGNhbmNlbEVkaXRNb2RlID0gKCkgPT4ge1xuICAgICAgICBzZXRFZGl0SW5kZXgobnVsbCk7XG4gICAgICAgIHNldEVkaXRhYmxlVGV4dEZpZWxkKCcnKTtcbiAgICB9O1xuXG4gICAgY29uc3QgaGFuZGxlU2F2ZSA9ICgpID0+IHtcbiAgICAgICAgY29uc3QgdGVtcEFubm90YXRpb25zID0gY2xvbmVFbGVtZW50KGFubm90YXRpb25zKTtcbiAgICAgICAgdGVtcEFubm90YXRpb25zW2VkaXRJbmRleF0uZWRpdGFibGVfbGFiZWwgPSBlZGl0YWJsZVRleHRGaWVsZDtcbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2UoeyAuLi52YWx1ZSwgYW5ub3RhdGlvbnM6IHRlbXBBbm5vdGF0aW9ucyB9KTtcbiAgICAgICAgc2V0RWRpdEluZGV4KG51bGwpO1xuICAgICAgICBzZXRFZGl0YWJsZVRleHRGaWVsZCgnJyk7XG4gICAgfTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlVmFsdWVWaXNpYmlsdHkgPSAoaW5kZXgsIHZpc2libGUpID0+IHtcbiAgICAgICAgY29uc3QgdGVtcEFubm90YXRpb25zID0gY2xvbmVFbGVtZW50KGFubm90YXRpb25zKTtcbiAgICAgICAgdGVtcEFubm90YXRpb25zW2luZGV4XS52aXNpYmxlID0gdmlzaWJsZTtcbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2UoeyAuLi52YWx1ZSwgYW5ub3RhdGlvbnM6IHRlbXBBbm5vdGF0aW9ucyB9KTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VWYWx1ZUNvbG9yID0gKGluZGV4LCBjb2xvcikgPT4ge1xuICAgICAgICBjb25zdCB0ZW1wQW5ub3RhdGlvbnMgPSBjbG9uZUVsZW1lbnQoYW5ub3RhdGlvbnMpO1xuICAgICAgICB0ZW1wQW5ub3RhdGlvbnNbaW5kZXhdLmNvbG9yID0gY29sb3I7XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHsgLi4udmFsdWUsIGFubm90YXRpb25zOiB0ZW1wQW5ub3RhdGlvbnMgfSk7XG4gICAgfTtcblxuICAgIGNvbnN0IHByZXBhcmVFZGl0TW9kZSA9IChpZCwgbGFiZWwpID0+IHtcbiAgICAgICAgc2V0RWRpdEluZGV4KGlkKTtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQobGFiZWwpO1xuICAgIH07XG5cbiAgICBjb25zdCByZW5kZXJFZGl0VGV4dCA9IChpbmRleCwgZWRpdGFibGVfbGFiZWwsIGxhYmVsKSA9PiB7XG4gICAgICAgIGlmIChlZGl0SW5kZXggPT09IGluZGV4KSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlRWR0aXRhYmxlVGV4dEZpZWxkfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2VkaXRhYmxlVGV4dEZpZWxkfVxuICAgICAgICAgICAgICAgICAgICAvPnsnICd9XG4gICAgICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGNvbG9yPVwiIzAwOGRmZlwiIG9uQ2xpY2s9e2hhbmRsZVNhdmV9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLXNhdmVcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGNvbG9yPVwiI2ZmMDAwMFwiIG9uQ2xpY2s9e2NhbmNlbEVkaXRNb2RlfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS10aW1lc1wiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgICA8Lz5cbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgPENvbEVkaXRhYmxlVGV4dD57ZWRpdGFibGVfbGFiZWwgfHwgbGFiZWx9PC9Db2xFZGl0YWJsZVRleHQ+XG4gICAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICAgICAgY29sb3I9XCIjMDA4ZGZmXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXBhcmVFZGl0TW9kZShpbmRleCwgZWRpdGFibGVfbGFiZWwgfHwgbGFiZWwpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1lZGl0XCIgLz5cbiAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8Lz5cbiAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgY29uc3QgcmVuZGVyUmVzdWx0ID0gKGluZGV4LCBzZXJpZSkgPT4ge1xuICAgICAgICBjb25zdCB7IGxhYmVsLCB2aXNpYmxlLCBjb2xvciA9ICcjZmYwMDAwJywgZWRpdGFibGVfbGFiZWwgfSA9IHNlcmllO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEZsZXhDb2x1bW4ga2V5PXtgcm93LWRhdGEtJHtsYWJlbH1gfT5cbiAgICAgICAgICAgICAgICA8U3VibWVudT57bGFiZWx9PC9TdWJtZW51PlxuICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdUZXh0Jyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXAgYWxpZ25JdGVtcz1cImNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge3JlbmRlckVkaXRUZXh0KGluZGV4LCBlZGl0YWJsZV9sYWJlbCwgbGFiZWwpfVxuICAgICAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnVmlzaWJsZScpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInN0YWNrZWQtYmFyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZVZhbHVlVmlzaWJpbHR5KGluZGV4LCBlLnRhcmdldC5jaGVja2VkKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17dmlzaWJsZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdDb2xvcicpfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2NvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2VWYWx1ZUNvbG9yKGluZGV4LCB0ZW1wQ29sb3IpXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvRmxleENvbHVtbj5cbiAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICB7YW5ub3RhdGlvbnMubWFwKChhbm5vdGF0aW9uLCBpbmRleCkgPT5cbiAgICAgICAgICAgICAgICByZW5kZXJSZXN1bHQoaW5kZXgsIGFubm90YXRpb24pXG4gICAgICAgICAgICApfVxuICAgICAgICA8L0ZsZXhDb2x1bW4+XG4gICAgKTtcbn07XG5cbkFubm90YXRpb25zVGFiLnByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBBbm5vdGF0aW9uc1RhYjtcbiJdfQ==