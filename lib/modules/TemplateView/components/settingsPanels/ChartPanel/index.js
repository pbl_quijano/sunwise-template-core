"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ProposalSelector = _interopRequireDefault(require("../ProposalSelector"));

var _Styles = require("../Styles");

var _AnnotationsTab = _interopRequireDefault(require("./AnnotationsTab"));

var _GeneralTab = _interopRequireDefault(require("./GeneralTab"));

var _ValuesTab = _interopRequireDefault(require("./ValuesTab"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChartPanel = function ChartPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      handleValueChange = _ref.handleValueChange,
      hasProposalSelector = _ref.hasProposalSelector,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      type = _ref.type,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_ProposalSelector.default, {
      catalogs: catalogs,
      disabled: proposalSelectorDisabled,
      handleValueChange: handleValueChange,
      value: value,
      visible: hasProposalSelector
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.StyledTabs, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Properties",
        title: t('Properties'),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_GeneralTab.default, {
          value: value,
          handleValueChange: handleValueChange,
          proposalSelectorDisabled: proposalSelectorDisabled,
          typeChart: type
        })
      }), (!value.stacked_bar || !value.stacked_bar.visible) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Values",
        title: t('Value', {
          count: 2
        }),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ValuesTab.default, {
          value: value,
          handleValueChange: handleValueChange
        })
      }), value.annotations && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Indicators",
        title: t('Indicators'),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_AnnotationsTab.default, {
          value: value,
          handleValueChange: handleValueChange
        })
      })]
    })]
  });
};

ChartPanel.propTypes = {
  catalogs: _propTypes.default.array,
  handleValueChange: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  proposalSelectorDisabled: _propTypes.default.bool,
  type: _propTypes.default.string,
  value: _propTypes.default.object
};
var _default = ChartPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0NoYXJ0UGFuZWwvaW5kZXguanMiXSwibmFtZXMiOlsiQ2hhcnRQYW5lbCIsImNhdGFsb2dzIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJoYXNQcm9wb3NhbFNlbGVjdG9yIiwicHJvcG9zYWxTZWxlY3RvckRpc2FibGVkIiwidHlwZSIsInZhbHVlIiwidCIsInN0YWNrZWRfYmFyIiwidmlzaWJsZSIsImNvdW50IiwiYW5ub3RhdGlvbnMiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsImZ1bmMiLCJib29sIiwic3RyaW5nIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7OztBQUVBLElBQU1BLFVBQVUsR0FBRyxTQUFiQSxVQUFhLEdBT1I7QUFBQSxpRkFBUCxFQUFPO0FBQUEsTUFOUEMsUUFNTyxRQU5QQSxRQU1PO0FBQUEsTUFMUEMsaUJBS08sUUFMUEEsaUJBS087QUFBQSxNQUpQQyxtQkFJTyxRQUpQQSxtQkFJTztBQUFBLE1BSFBDLHdCQUdPLFFBSFBBLHdCQUdPO0FBQUEsTUFGUEMsSUFFTyxRQUZQQSxJQUVPO0FBQUEsd0JBRFBDLEtBQ087QUFBQSxNQURQQSxLQUNPLDJCQURDLEVBQ0Q7O0FBQ1Asd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUNJO0FBQUEsNEJBQ0kscUJBQUMseUJBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRU4sUUFEZDtBQUVJLE1BQUEsUUFBUSxFQUFFRyx3QkFGZDtBQUdJLE1BQUEsaUJBQWlCLEVBQUVGLGlCQUh2QjtBQUlJLE1BQUEsS0FBSyxFQUFFSSxLQUpYO0FBS0ksTUFBQSxPQUFPLEVBQUVIO0FBTGIsTUFESixlQVFJLHNCQUFDLGtCQUFEO0FBQUEsOEJBQ0kscUJBQUMsbUJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxXQURkO0FBRUksUUFBQSxRQUFRLEVBQUMsWUFGYjtBQUdJLFFBQUEsS0FBSyxFQUFFSSxDQUFDLENBQUMsWUFBRCxDQUhaO0FBQUEsK0JBS0kscUJBQUMsbUJBQUQ7QUFFUUQsVUFBQUEsS0FBSyxFQUFMQSxLQUZSO0FBR1FKLFVBQUFBLGlCQUFpQixFQUFqQkEsaUJBSFI7QUFJUUUsVUFBQUEsd0JBQXdCLEVBQXhCQSx3QkFKUjtBQU1JLFVBQUEsU0FBUyxFQUFFQztBQU5mO0FBTEosUUFESixFQWVLLENBQUMsQ0FBQ0MsS0FBSyxDQUFDRSxXQUFQLElBQXNCLENBQUNGLEtBQUssQ0FBQ0UsV0FBTixDQUFrQkMsT0FBMUMsa0JBQ0cscUJBQUMsbUJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxXQURkO0FBRUksUUFBQSxRQUFRLEVBQUMsUUFGYjtBQUdJLFFBQUEsS0FBSyxFQUFFRixDQUFDLENBQUMsT0FBRCxFQUFVO0FBQUVHLFVBQUFBLEtBQUssRUFBRTtBQUFULFNBQVYsQ0FIWjtBQUFBLCtCQUtJLHFCQUFDLGtCQUFEO0FBQWlCSixVQUFBQSxLQUFLLEVBQUxBLEtBQWpCO0FBQXdCSixVQUFBQSxpQkFBaUIsRUFBakJBO0FBQXhCO0FBTEosUUFoQlIsRUF3QktJLEtBQUssQ0FBQ0ssV0FBTixpQkFDRyxxQkFBQyxtQkFBRDtBQUNJLFFBQUEsU0FBUyxFQUFDLFdBRGQ7QUFFSSxRQUFBLFFBQVEsRUFBQyxZQUZiO0FBR0ksUUFBQSxLQUFLLEVBQUVKLENBQUMsQ0FBQyxZQUFELENBSFo7QUFBQSwrQkFLSSxxQkFBQyx1QkFBRDtBQUFzQkQsVUFBQUEsS0FBSyxFQUFMQSxLQUF0QjtBQUE2QkosVUFBQUEsaUJBQWlCLEVBQWpCQTtBQUE3QjtBQUxKLFFBekJSO0FBQUEsTUFSSjtBQUFBLElBREo7QUE2Q0gsQ0F0REQ7O0FBd0RBRixVQUFVLENBQUNZLFNBQVgsR0FBdUI7QUFDbkJYLEVBQUFBLFFBQVEsRUFBRVksbUJBQVVDLEtBREQ7QUFFbkJaLEVBQUFBLGlCQUFpQixFQUFFVyxtQkFBVUUsSUFGVjtBQUduQlosRUFBQUEsbUJBQW1CLEVBQUVVLG1CQUFVRyxJQUhaO0FBSW5CWixFQUFBQSx3QkFBd0IsRUFBRVMsbUJBQVVHLElBSmpCO0FBS25CWCxFQUFBQSxJQUFJLEVBQUVRLG1CQUFVSSxNQUxHO0FBTW5CWCxFQUFBQSxLQUFLLEVBQUVPLG1CQUFVSztBQU5FLENBQXZCO2VBU2VsQixVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IFRhYiB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuXG5pbXBvcnQgUHJvcG9zYWxTZWxlY3RvciBmcm9tICcuLi9Qcm9wb3NhbFNlbGVjdG9yJztcbmltcG9ydCB7IFN0eWxlZFRhYnMgfSBmcm9tICcuLi9TdHlsZXMnO1xuXG5pbXBvcnQgQW5ub3RhdGlvbnNUYWIgZnJvbSAnLi9Bbm5vdGF0aW9uc1RhYic7XG5pbXBvcnQgR2VuZXJhbFRhYiBmcm9tICcuL0dlbmVyYWxUYWInO1xuaW1wb3J0IFZhbHVlc1RhYiBmcm9tICcuL1ZhbHVlc1RhYic7XG5cbmNvbnN0IENoYXJ0UGFuZWwgPSAoe1xuICAgIGNhdGFsb2dzLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgcHJvcG9zYWxTZWxlY3RvckRpc2FibGVkLFxuICAgIHR5cGUsXG4gICAgdmFsdWUgPSB7fSxcbn0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICByZXR1cm4gKFxuICAgICAgICA8PlxuICAgICAgICAgICAgPFByb3Bvc2FsU2VsZWN0b3JcbiAgICAgICAgICAgICAgICBjYXRhbG9ncz17Y2F0YWxvZ3N9XG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3Byb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZH1cbiAgICAgICAgICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZT17aGFuZGxlVmFsdWVDaGFuZ2V9XG4gICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgIHZpc2libGU9e2hhc1Byb3Bvc2FsU2VsZWN0b3J9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPFN0eWxlZFRhYnM+XG4gICAgICAgICAgICAgICAgPFRhYlxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwci0zIHBsLTNcIlxuICAgICAgICAgICAgICAgICAgICBldmVudEtleT1cIlByb3BlcnRpZXNcIlxuICAgICAgICAgICAgICAgICAgICB0aXRsZT17dCgnUHJvcGVydGllcycpfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPEdlbmVyYWxUYWJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsuLi57XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxTZWxlY3RvckRpc2FibGVkLFxuICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGVDaGFydD17dHlwZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgICAgICAgICB7KCF2YWx1ZS5zdGFja2VkX2JhciB8fCAhdmFsdWUuc3RhY2tlZF9iYXIudmlzaWJsZSkgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8VGFiXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwci0zIHBsLTNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRLZXk9XCJWYWx1ZXNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3QoJ1ZhbHVlJywgeyBjb3VudDogMiB9KX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFZhbHVlc1RhYiB7Li4ueyB2YWx1ZSwgaGFuZGxlVmFsdWVDaGFuZ2UgfX0gLz5cbiAgICAgICAgICAgICAgICAgICAgPC9UYWI+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICB7dmFsdWUuYW5ub3RhdGlvbnMgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8VGFiXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwci0zIHBsLTNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnRLZXk9XCJJbmRpY2F0b3JzXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXt0KCdJbmRpY2F0b3JzJyl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxBbm5vdGF0aW9uc1RhYiB7Li4ueyB2YWx1ZSwgaGFuZGxlVmFsdWVDaGFuZ2UgfX0gLz5cbiAgICAgICAgICAgICAgICAgICAgPC9UYWI+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIDwvU3R5bGVkVGFicz5cbiAgICAgICAgPC8+XG4gICAgKTtcbn07XG5cbkNoYXJ0UGFuZWwucHJvcFR5cGVzID0ge1xuICAgIGNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgaGFuZGxlVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I6IFByb3BUeXBlcy5ib29sLFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IENoYXJ0UGFuZWw7XG4iXX0=