"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styled = require("../../../../../components/styled");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledInputGroup = (0, _styledComponents.default)(_reactBootstrap.InputGroup)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n\n    & select {\n        background-color: #ffffff;\n        border-radius: 3px;\n        border: 1px solid #ecedf0;\n        height: 46px;\n        width: 133px;\n    }\n"])));

var InputContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    margin: 4px 0;\n"])));

var Label = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var GeneralTab = function GeneralTab() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref.handleValueChange,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      typeChart = _ref.typeChart,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _value$type = value.type,
      type = _value$type === void 0 ? 'bar' : _value$type,
      stacked_bar = value.stacked_bar,
      categories = value.categories,
      _value$initCategoryIn = value.initCategoryIndex,
      initCategoryIndex = _value$initCategoryIn === void 0 ? '0' : _value$initCategoryIn,
      _value$finishCategory = value.finishCategoryIndex,
      finishCategoryIndex = _value$finishCategory === void 0 ? "".concat(value.categories.length - 1) : _value$finishCategory;

  var onGetChartOptions = function onGetChartOptions() {
    return [{
      value: 'bar',
      label: t('Bars'),
      disabled: typeChart === 'kwh-chart'
    }, {
      value: 'line',
      label: t('Line'),
      disabled: false
    }, {
      value: 'area',
      label: t('Area'),
      disabled: typeChart === 'kwh-chart'
    }];
  };

  var onChangeType = function onChangeType(e) {
    var tempValue = _objectSpread(_objectSpread({}, value), {}, {
      type: e.target.value
    });

    handleValueChange(_objectSpread({}, tempValue));
  };

  var onChangeStackedBarType = function onChangeStackedBarType(e) {
    if (e.target.value !== stacked_bar.selected) {
      var tempValue = _objectSpread(_objectSpread({}, value), {}, {
        stacked_bar: _objectSpread(_objectSpread({}, stacked_bar), {}, {
          selected: e.target.value
        })
      });

      handleValueChange(_objectSpread({}, tempValue));
    }
  };

  var onChangeStackedBarVisibilty = function onChangeStackedBarVisibilty(visible) {
    var tempValue = _objectSpread(_objectSpread({}, value), {}, {
      stacked_bar: _objectSpread(_objectSpread({}, stacked_bar), {}, {
        visible: visible
      })
    });

    handleValueChange(_objectSpread({}, tempValue));
  };

  var onChangeInitCategoryIndex = function onChangeInitCategoryIndex(e) {
    if (e.target.value !== initCategoryIndex) {
      handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
        initCategoryIndex: e.target.value
      }));
    }
  };

  var onChangeFinishCategoryIndex = function onChangeFinishCategoryIndex(e) {
    if (e.target.value !== finishCategoryIndex) {
      handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
        finishCategoryIndex: e.target.value
      }));
    }
  };

  var categoriesOptions = categories.map(function (cat, index) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
      value: index,
      children: cat
    }, "".concat(cat));
  });

  var renderStackedBarSection = function renderStackedBarSection() {
    if (!stacked_bar || type !== 'bar') {
      return null;
    }

    var visible = stacked_bar.visible,
        selected = stacked_bar.selected,
        label = stacked_bar.label;
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('View details')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: "stacked-bar",
          onChange: function onChange(e) {
            return onChangeStackedBarVisibilty(e.target.checked);
          },
          checked: visible
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Type')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
            value: selected,
            onChange: onChangeStackedBarType,
            as: "select",
            id: "select-stacked",
            disabled: !visible,
            children: Object.keys(label).map(function (key) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
                value: key,
                children: label[key]
              }, "select-".concat(key, "-").concat(label[key]));
            })
          })
        })]
      })]
    });
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
        children: t('Type')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: type,
          onChange: onChangeType,
          as: "select",
          id: "select-type",
          children: onGetChartOptions().map(function (_ref2) {
            var label = _ref2.label,
                optionValue = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: optionValue,
              disabled: optionDisabled,
              children: label
            }, "select-".concat(optionValue, "-").concat(label));
          })
        })
      })]
    }), proposalSelectorDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Initial category')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
            value: initCategoryIndex,
            onChange: onChangeInitCategoryIndex,
            as: "select",
            id: "select-init-category",
            children: categoriesOptions.filter(function (e, index) {
              return index < finishCategoryIndex;
            })
          })
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Final category')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
            value: finishCategoryIndex,
            onChange: onChangeFinishCategoryIndex,
            as: "select",
            id: "select-finish-category",
            children: categoriesOptions.filter(function (e, index) {
              return index > initCategoryIndex;
            })
          })
        })]
      })]
    }), renderStackedBarSection()]
  });
};

GeneralTab.propTypes = {
  handleValueChange: _propTypes.default.func,
  proposalSelectorDisabled: _propTypes.default.bool,
  typeChart: _propTypes.default.string,
  value: _propTypes.default.object
};
var _default = GeneralTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0NoYXJ0UGFuZWwvR2VuZXJhbFRhYi5qcyJdLCJuYW1lcyI6WyJTdHlsZWRJbnB1dEdyb3VwIiwiSW5wdXRHcm91cCIsIklucHV0Q29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiTGFiZWwiLCJzcGFuIiwiR2VuZXJhbFRhYiIsImhhbmRsZVZhbHVlQ2hhbmdlIiwicHJvcG9zYWxTZWxlY3RvckRpc2FibGVkIiwidHlwZUNoYXJ0IiwidmFsdWUiLCJ0IiwidHlwZSIsInN0YWNrZWRfYmFyIiwiY2F0ZWdvcmllcyIsImluaXRDYXRlZ29yeUluZGV4IiwiZmluaXNoQ2F0ZWdvcnlJbmRleCIsImxlbmd0aCIsIm9uR2V0Q2hhcnRPcHRpb25zIiwibGFiZWwiLCJkaXNhYmxlZCIsIm9uQ2hhbmdlVHlwZSIsImUiLCJ0ZW1wVmFsdWUiLCJ0YXJnZXQiLCJvbkNoYW5nZVN0YWNrZWRCYXJUeXBlIiwic2VsZWN0ZWQiLCJvbkNoYW5nZVN0YWNrZWRCYXJWaXNpYmlsdHkiLCJ2aXNpYmxlIiwib25DaGFuZ2VJbml0Q2F0ZWdvcnlJbmRleCIsIm9uQ2hhbmdlRmluaXNoQ2F0ZWdvcnlJbmRleCIsImNhdGVnb3JpZXNPcHRpb25zIiwibWFwIiwiY2F0IiwiaW5kZXgiLCJyZW5kZXJTdGFja2VkQmFyU2VjdGlvbiIsImNoZWNrZWQiLCJPYmplY3QiLCJrZXlzIiwia2V5Iiwib3B0aW9uVmFsdWUiLCJvcHRpb25EaXNhYmxlZCIsImZpbHRlciIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJib29sIiwic3RyaW5nIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxnQkFBZ0IsR0FBRywrQkFBT0MsMEJBQVAsQ0FBSCx5UkFBdEI7O0FBYUEsSUFBTUMsY0FBYyxHQUFHQywwQkFBT0MsR0FBViwySUFBcEI7O0FBTUEsSUFBTUMsS0FBSyxHQUFHRiwwQkFBT0csSUFBVix1T0FBWDs7QUFVQSxJQUFNQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUtSO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BSlBDLGlCQUlPLFFBSlBBLGlCQUlPO0FBQUEsTUFIUEMsd0JBR08sUUFIUEEsd0JBR087QUFBQSxNQUZQQyxTQUVPLFFBRlBBLFNBRU87QUFBQSx3QkFEUEMsS0FDTztBQUFBLE1BRFBBLEtBQ08sMkJBREMsRUFDRDs7QUFDUCx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0Esb0JBTUlELEtBTkosQ0FDSUUsSUFESjtBQUFBLE1BQ0lBLElBREosNEJBQ1csS0FEWDtBQUFBLE1BRUlDLFdBRkosR0FNSUgsS0FOSixDQUVJRyxXQUZKO0FBQUEsTUFHSUMsVUFISixHQU1JSixLQU5KLENBR0lJLFVBSEo7QUFBQSw4QkFNSUosS0FOSixDQUlJSyxpQkFKSjtBQUFBLE1BSUlBLGlCQUpKLHNDQUl3QixHQUp4QjtBQUFBLDhCQU1JTCxLQU5KLENBS0lNLG1CQUxKO0FBQUEsTUFLSUEsbUJBTEosZ0RBSzZCTixLQUFLLENBQUNJLFVBQU4sQ0FBaUJHLE1BQWpCLEdBQTBCLENBTHZEOztBQVFBLE1BQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUM1QixXQUFPLENBQ0g7QUFDSVIsTUFBQUEsS0FBSyxFQUFFLEtBRFg7QUFFSVMsTUFBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsTUFBRCxDQUZaO0FBR0lTLE1BQUFBLFFBQVEsRUFBRVgsU0FBUyxLQUFLO0FBSDVCLEtBREcsRUFNSDtBQUFFQyxNQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQlMsTUFBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsTUFBRCxDQUF6QjtBQUFtQ1MsTUFBQUEsUUFBUSxFQUFFO0FBQTdDLEtBTkcsRUFPSDtBQUNJVixNQUFBQSxLQUFLLEVBQUUsTUFEWDtBQUVJUyxNQUFBQSxLQUFLLEVBQUVSLENBQUMsQ0FBQyxNQUFELENBRlo7QUFHSVMsTUFBQUEsUUFBUSxFQUFFWCxTQUFTLEtBQUs7QUFINUIsS0FQRyxDQUFQO0FBYUgsR0FkRDs7QUFnQkEsTUFBTVksWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsQ0FBRCxFQUFPO0FBQ3hCLFFBQU1DLFNBQVMsbUNBQ1JiLEtBRFE7QUFFWEUsTUFBQUEsSUFBSSxFQUFFVSxDQUFDLENBQUNFLE1BQUYsQ0FBU2Q7QUFGSixNQUFmOztBQUlBSCxJQUFBQSxpQkFBaUIsbUJBQ1ZnQixTQURVLEVBQWpCO0FBR0gsR0FSRDs7QUFVQSxNQUFNRSxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNILENBQUQsRUFBTztBQUNsQyxRQUFJQSxDQUFDLENBQUNFLE1BQUYsQ0FBU2QsS0FBVCxLQUFtQkcsV0FBVyxDQUFDYSxRQUFuQyxFQUE2QztBQUN6QyxVQUFNSCxTQUFTLG1DQUNSYixLQURRO0FBRVhHLFFBQUFBLFdBQVcsa0NBQ0pBLFdBREk7QUFFUGEsVUFBQUEsUUFBUSxFQUFFSixDQUFDLENBQUNFLE1BQUYsQ0FBU2Q7QUFGWjtBQUZBLFFBQWY7O0FBT0FILE1BQUFBLGlCQUFpQixtQkFDVmdCLFNBRFUsRUFBakI7QUFHSDtBQUNKLEdBYkQ7O0FBZUEsTUFBTUksMkJBQTJCLEdBQUcsU0FBOUJBLDJCQUE4QixDQUFDQyxPQUFELEVBQWE7QUFDN0MsUUFBTUwsU0FBUyxtQ0FDUmIsS0FEUTtBQUVYRyxNQUFBQSxXQUFXLGtDQUNKQSxXQURJO0FBRVBlLFFBQUFBLE9BQU8sRUFBUEE7QUFGTztBQUZBLE1BQWY7O0FBT0FyQixJQUFBQSxpQkFBaUIsbUJBQ1ZnQixTQURVLEVBQWpCO0FBR0gsR0FYRDs7QUFhQSxNQUFNTSx5QkFBeUIsR0FBRyxTQUE1QkEseUJBQTRCLENBQUNQLENBQUQsRUFBTztBQUNyQyxRQUFJQSxDQUFDLENBQUNFLE1BQUYsQ0FBU2QsS0FBVCxLQUFtQkssaUJBQXZCLEVBQTBDO0FBQ3RDUixNQUFBQSxpQkFBaUIsaUNBQ1ZHLEtBRFU7QUFFYkssUUFBQUEsaUJBQWlCLEVBQUVPLENBQUMsQ0FBQ0UsTUFBRixDQUFTZDtBQUZmLFNBQWpCO0FBSUg7QUFDSixHQVBEOztBQVNBLE1BQU1vQiwyQkFBMkIsR0FBRyxTQUE5QkEsMkJBQThCLENBQUNSLENBQUQsRUFBTztBQUN2QyxRQUFJQSxDQUFDLENBQUNFLE1BQUYsQ0FBU2QsS0FBVCxLQUFtQk0sbUJBQXZCLEVBQTRDO0FBQ3hDVCxNQUFBQSxpQkFBaUIsaUNBQ1ZHLEtBRFU7QUFFYk0sUUFBQUEsbUJBQW1CLEVBQUVNLENBQUMsQ0FBQ0UsTUFBRixDQUFTZDtBQUZqQixTQUFqQjtBQUlIO0FBQ0osR0FQRDs7QUFTQSxNQUFNcUIsaUJBQWlCLEdBQUdqQixVQUFVLENBQUNrQixHQUFYLENBQWUsVUFBQ0MsR0FBRCxFQUFNQyxLQUFOO0FBQUEsd0JBQ3JDO0FBQXVCLE1BQUEsS0FBSyxFQUFFQSxLQUE5QjtBQUFBLGdCQUNLRDtBQURMLGlCQUFnQkEsR0FBaEIsRUFEcUM7QUFBQSxHQUFmLENBQTFCOztBQU1BLE1BQU1FLHVCQUF1QixHQUFHLFNBQTFCQSx1QkFBMEIsR0FBTTtBQUNsQyxRQUFJLENBQUN0QixXQUFELElBQWdCRCxJQUFJLEtBQUssS0FBN0IsRUFBb0M7QUFDaEMsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsUUFBUWdCLE9BQVIsR0FBcUNmLFdBQXJDLENBQVFlLE9BQVI7QUFBQSxRQUFpQkYsUUFBakIsR0FBcUNiLFdBQXJDLENBQWlCYSxRQUFqQjtBQUFBLFFBQTJCUCxLQUEzQixHQUFxQ04sV0FBckMsQ0FBMkJNLEtBQTNCO0FBQ0Esd0JBQ0ksc0JBQUMsa0JBQUQ7QUFBQSw4QkFDSSxzQkFBQyxjQUFEO0FBQUEsZ0NBQ0kscUJBQUMsS0FBRDtBQUFBLG9CQUFRUixDQUFDLENBQUMsY0FBRDtBQUFULFVBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFVBQUEsRUFBRSxFQUFDLGFBRFA7QUFFSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ1csQ0FBRDtBQUFBLG1CQUNOSywyQkFBMkIsQ0FBQ0wsQ0FBQyxDQUFDRSxNQUFGLENBQVNZLE9BQVYsQ0FEckI7QUFBQSxXQUZkO0FBS0ksVUFBQSxPQUFPLEVBQUVSO0FBTGIsVUFGSjtBQUFBLFFBREosZUFXSSxzQkFBQyxjQUFEO0FBQUEsZ0NBQ0kscUJBQUMsS0FBRDtBQUFBLG9CQUFRakIsQ0FBQyxDQUFDLE1BQUQ7QUFBVCxVQURKLGVBRUkscUJBQUMsZ0JBQUQ7QUFBQSxpQ0FDSSxxQkFBQyxhQUFELENBQU0sT0FBTjtBQUNJLFlBQUEsS0FBSyxFQUFFZSxRQURYO0FBRUksWUFBQSxRQUFRLEVBQUVELHNCQUZkO0FBR0ksWUFBQSxFQUFFLEVBQUMsUUFIUDtBQUlJLFlBQUEsRUFBRSxFQUFDLGdCQUpQO0FBS0ksWUFBQSxRQUFRLEVBQUUsQ0FBQ0csT0FMZjtBQUFBLHNCQU9LUyxNQUFNLENBQUNDLElBQVAsQ0FBWW5CLEtBQVosRUFBbUJhLEdBQW5CLENBQXVCLFVBQUNPLEdBQUQ7QUFBQSxrQ0FDcEI7QUFFSSxnQkFBQSxLQUFLLEVBQUVBLEdBRlg7QUFBQSwwQkFJS3BCLEtBQUssQ0FBQ29CLEdBQUQ7QUFKVixrQ0FDbUJBLEdBRG5CLGNBQzBCcEIsS0FBSyxDQUFDb0IsR0FBRCxDQUQvQixFQURvQjtBQUFBLGFBQXZCO0FBUEw7QUFESixVQUZKO0FBQUEsUUFYSjtBQUFBLE1BREo7QUFtQ0gsR0F4Q0Q7O0FBMENBLHNCQUNJLHNCQUFDLGtCQUFEO0FBQUEsNEJBQ0ksc0JBQUMsY0FBRDtBQUFBLDhCQUNJLHFCQUFDLEtBQUQ7QUFBQSxrQkFBUTVCLENBQUMsQ0FBQyxNQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLGdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEtBQUssRUFBRUMsSUFEWDtBQUVJLFVBQUEsUUFBUSxFQUFFUyxZQUZkO0FBR0ksVUFBQSxFQUFFLEVBQUMsUUFIUDtBQUlJLFVBQUEsRUFBRSxFQUFDLGFBSlA7QUFBQSxvQkFNS0gsaUJBQWlCLEdBQUdjLEdBQXBCLENBQ0c7QUFBQSxnQkFDSWIsS0FESixTQUNJQSxLQURKO0FBQUEsZ0JBRVdxQixXQUZYLFNBRUk5QixLQUZKO0FBQUEsZ0JBR2MrQixjQUhkLFNBR0lyQixRQUhKO0FBQUEsZ0NBS0k7QUFFSSxjQUFBLEtBQUssRUFBRW9CLFdBRlg7QUFHSSxjQUFBLFFBQVEsRUFBRUMsY0FIZDtBQUFBLHdCQUtLdEI7QUFMTCxnQ0FDbUJxQixXQURuQixjQUNrQ3JCLEtBRGxDLEVBTEo7QUFBQSxXQURIO0FBTkw7QUFESixRQUZKO0FBQUEsTUFESixFQTRCS1gsd0JBQXdCLGlCQUNyQjtBQUFBLDhCQUNJLHNCQUFDLGNBQUQ7QUFBQSxnQ0FDSSxxQkFBQyxLQUFEO0FBQUEsb0JBQVFHLENBQUMsQ0FBQyxrQkFBRDtBQUFULFVBREosZUFFSSxxQkFBQyxnQkFBRDtBQUFBLGlDQUNJLHFCQUFDLGFBQUQsQ0FBTSxPQUFOO0FBQ0ksWUFBQSxLQUFLLEVBQUVJLGlCQURYO0FBRUksWUFBQSxRQUFRLEVBQUVjLHlCQUZkO0FBR0ksWUFBQSxFQUFFLEVBQUMsUUFIUDtBQUlJLFlBQUEsRUFBRSxFQUFDLHNCQUpQO0FBQUEsc0JBTUtFLGlCQUFpQixDQUFDVyxNQUFsQixDQUNHLFVBQUNwQixDQUFELEVBQUlZLEtBQUo7QUFBQSxxQkFBY0EsS0FBSyxHQUFHbEIsbUJBQXRCO0FBQUEsYUFESDtBQU5MO0FBREosVUFGSjtBQUFBLFFBREosZUFnQkksc0JBQUMsY0FBRDtBQUFBLGdDQUNJLHFCQUFDLEtBQUQ7QUFBQSxvQkFBUUwsQ0FBQyxDQUFDLGdCQUFEO0FBQVQsVUFESixlQUVJLHFCQUFDLGdCQUFEO0FBQUEsaUNBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxZQUFBLEtBQUssRUFBRUssbUJBRFg7QUFFSSxZQUFBLFFBQVEsRUFBRWMsMkJBRmQ7QUFHSSxZQUFBLEVBQUUsRUFBQyxRQUhQO0FBSUksWUFBQSxFQUFFLEVBQUMsd0JBSlA7QUFBQSxzQkFNS0MsaUJBQWlCLENBQUNXLE1BQWxCLENBQ0csVUFBQ3BCLENBQUQsRUFBSVksS0FBSjtBQUFBLHFCQUFjQSxLQUFLLEdBQUduQixpQkFBdEI7QUFBQSxhQURIO0FBTkw7QUFESixVQUZKO0FBQUEsUUFoQko7QUFBQSxNQTdCUixFQStES29CLHVCQUF1QixFQS9ENUI7QUFBQSxJQURKO0FBbUVILENBMU1EOztBQTRNQTdCLFVBQVUsQ0FBQ3FDLFNBQVgsR0FBdUI7QUFDbkJwQyxFQUFBQSxpQkFBaUIsRUFBRXFDLG1CQUFVQyxJQURWO0FBRW5CckMsRUFBQUEsd0JBQXdCLEVBQUVvQyxtQkFBVUUsSUFGakI7QUFHbkJyQyxFQUFBQSxTQUFTLEVBQUVtQyxtQkFBVUcsTUFIRjtBQUluQnJDLEVBQUFBLEtBQUssRUFBRWtDLG1CQUFVSTtBQUpFLENBQXZCO2VBT2UxQyxVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IElucHV0R3JvdXAgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBGbGV4Q29sdW1uIH0gZnJvbSAnQGNvbXBvbmVudHMvc3R5bGVkJztcblxuaW1wb3J0IHsgQ2hlY2tib3hJbnB1dCB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmNvbnN0IFN0eWxlZElucHV0R3JvdXAgPSBzdHlsZWQoSW5wdXRHcm91cClgXG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIHdpZHRoOiAxMzNweDtcblxuICAgICYgc2VsZWN0IHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZWNlZGYwO1xuICAgICAgICBoZWlnaHQ6IDQ2cHg7XG4gICAgICAgIHdpZHRoOiAxMzNweDtcbiAgICB9XG5gO1xuXG5jb25zdCBJbnB1dENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbjogNHB4IDA7XG5gO1xuXG5jb25zdCBMYWJlbCA9IHN0eWxlZC5zcGFuYFxuICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIzcHg7XG4gICAgbWluLWhlaWdodDogMTZweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB3aWR0aDogMTM1cHg7XG5gO1xuXG5jb25zdCBHZW5lcmFsVGFiID0gKHtcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZSxcbiAgICBwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWQsXG4gICAgdHlwZUNoYXJ0LFxuICAgIHZhbHVlID0ge30sXG59ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3Qge1xuICAgICAgICB0eXBlID0gJ2JhcicsXG4gICAgICAgIHN0YWNrZWRfYmFyLFxuICAgICAgICBjYXRlZ29yaWVzLFxuICAgICAgICBpbml0Q2F0ZWdvcnlJbmRleCA9ICcwJyxcbiAgICAgICAgZmluaXNoQ2F0ZWdvcnlJbmRleCA9IGAke3ZhbHVlLmNhdGVnb3JpZXMubGVuZ3RoIC0gMX1gLFxuICAgIH0gPSB2YWx1ZTtcblxuICAgIGNvbnN0IG9uR2V0Q2hhcnRPcHRpb25zID0gKCkgPT4ge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHZhbHVlOiAnYmFyJyxcbiAgICAgICAgICAgICAgICBsYWJlbDogdCgnQmFycycpLFxuICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0eXBlQ2hhcnQgPT09ICdrd2gtY2hhcnQnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHsgdmFsdWU6ICdsaW5lJywgbGFiZWw6IHQoJ0xpbmUnKSwgZGlzYWJsZWQ6IGZhbHNlIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhcmVhJyxcbiAgICAgICAgICAgICAgICBsYWJlbDogdCgnQXJlYScpLFxuICAgICAgICAgICAgICAgIGRpc2FibGVkOiB0eXBlQ2hhcnQgPT09ICdrd2gtY2hhcnQnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgXTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VUeXBlID0gKGUpID0+IHtcbiAgICAgICAgY29uc3QgdGVtcFZhbHVlID0ge1xuICAgICAgICAgICAgLi4udmFsdWUsXG4gICAgICAgICAgICB0eXBlOiBlLnRhcmdldC52YWx1ZSxcbiAgICAgICAgfTtcbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2Uoe1xuICAgICAgICAgICAgLi4udGVtcFZhbHVlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VTdGFja2VkQmFyVHlwZSA9IChlKSA9PiB7XG4gICAgICAgIGlmIChlLnRhcmdldC52YWx1ZSAhPT0gc3RhY2tlZF9iYXIuc2VsZWN0ZWQpIHtcbiAgICAgICAgICAgIGNvbnN0IHRlbXBWYWx1ZSA9IHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgICAgICBzdGFja2VkX2Jhcjoge1xuICAgICAgICAgICAgICAgICAgICAuLi5zdGFja2VkX2JhcixcbiAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWQ6IGUudGFyZ2V0LnZhbHVlLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2Uoe1xuICAgICAgICAgICAgICAgIC4uLnRlbXBWYWx1ZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlU3RhY2tlZEJhclZpc2liaWx0eSA9ICh2aXNpYmxlKSA9PiB7XG4gICAgICAgIGNvbnN0IHRlbXBWYWx1ZSA9IHtcbiAgICAgICAgICAgIC4uLnZhbHVlLFxuICAgICAgICAgICAgc3RhY2tlZF9iYXI6IHtcbiAgICAgICAgICAgICAgICAuLi5zdGFja2VkX2JhcixcbiAgICAgICAgICAgICAgICB2aXNpYmxlLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2Uoe1xuICAgICAgICAgICAgLi4udGVtcFZhbHVlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VJbml0Q2F0ZWdvcnlJbmRleCA9IChlKSA9PiB7XG4gICAgICAgIGlmIChlLnRhcmdldC52YWx1ZSAhPT0gaW5pdENhdGVnb3J5SW5kZXgpIHtcbiAgICAgICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgICAgICBpbml0Q2F0ZWdvcnlJbmRleDogZS50YXJnZXQudmFsdWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBjb25zdCBvbkNoYW5nZUZpbmlzaENhdGVnb3J5SW5kZXggPSAoZSkgPT4ge1xuICAgICAgICBpZiAoZS50YXJnZXQudmFsdWUgIT09IGZpbmlzaENhdGVnb3J5SW5kZXgpIHtcbiAgICAgICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgICAgICBmaW5pc2hDYXRlZ29yeUluZGV4OiBlLnRhcmdldC52YWx1ZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIGNvbnN0IGNhdGVnb3JpZXNPcHRpb25zID0gY2F0ZWdvcmllcy5tYXAoKGNhdCwgaW5kZXgpID0+IChcbiAgICAgICAgPG9wdGlvbiBrZXk9e2Ake2NhdH1gfSB2YWx1ZT17aW5kZXh9PlxuICAgICAgICAgICAge2NhdH1cbiAgICAgICAgPC9vcHRpb24+XG4gICAgKSk7XG5cbiAgICBjb25zdCByZW5kZXJTdGFja2VkQmFyU2VjdGlvbiA9ICgpID0+IHtcbiAgICAgICAgaWYgKCFzdGFja2VkX2JhciB8fCB0eXBlICE9PSAnYmFyJykge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgeyB2aXNpYmxlLCBzZWxlY3RlZCwgbGFiZWwgfSA9IHN0YWNrZWRfYmFyO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1ZpZXcgZGV0YWlscycpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInN0YWNrZWQtYmFyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZVN0YWNrZWRCYXJWaXNpYmlsdHkoZS50YXJnZXQuY2hlY2tlZClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3Zpc2libGV9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnVHlwZScpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtzZWxlY3RlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2VTdGFja2VkQmFyVHlwZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3Qtc3RhY2tlZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyF2aXNpYmxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtPYmplY3Qua2V5cyhsYWJlbCkubWFwKChrZXkpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgc2VsZWN0LSR7a2V5fS0ke2xhYmVsW2tleV19YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtrZXl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbFtrZXldfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvRmxleENvbHVtbj5cbiAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdUeXBlJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3R5cGV9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2VUeXBlfVxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtdHlwZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtvbkdldENoYXJ0T3B0aW9ucygpLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG9wdGlvblZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BzZWxlY3QtJHtvcHRpb25WYWx1ZX0tJHtsYWJlbH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e29wdGlvblZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e29wdGlvbkRpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7bGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICB7cHJvcG9zYWxTZWxlY3RvckRpc2FibGVkICYmIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0luaXRpYWwgY2F0ZWdvcnknKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17aW5pdENhdGVnb3J5SW5kZXh9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZUluaXRDYXRlZ29yeUluZGV4fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2VsZWN0LWluaXQtY2F0ZWdvcnlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2NhdGVnb3JpZXNPcHRpb25zLmZpbHRlcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChlLCBpbmRleCkgPT4gaW5kZXggPCBmaW5pc2hDYXRlZ29yeUluZGV4XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnRmluYWwgY2F0ZWdvcnknKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17ZmluaXNoQ2F0ZWdvcnlJbmRleH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlRmluaXNoQ2F0ZWdvcnlJbmRleH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC1maW5pc2gtY2F0ZWdvcnlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2NhdGVnb3JpZXNPcHRpb25zLmZpbHRlcihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIChlLCBpbmRleCkgPT4gaW5kZXggPiBpbml0Q2F0ZWdvcnlJbmRleFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgKX1cblxuICAgICAgICAgICAge3JlbmRlclN0YWNrZWRCYXJTZWN0aW9uKCl9XG4gICAgICAgIDwvRmxleENvbHVtbj5cbiAgICApO1xufTtcblxuR2VuZXJhbFRhYi5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgdHlwZUNoYXJ0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgR2VuZXJhbFRhYjtcbiJdfQ==