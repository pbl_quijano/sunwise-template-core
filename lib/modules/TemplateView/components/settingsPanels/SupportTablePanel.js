"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _styled = require("../../../../components/styled");

var _Styles = require("./Styles");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SupportTablePanel = function SupportTablePanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      handleStyleChange = _ref.handleStyleChange;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$table = style.table,
      _style$table$borderSt = _style$table.borderStyle,
      borderStyle = _style$table$borderSt === void 0 ? 'hidden' : _style$table$borderSt,
      _style$table$borderCo = _style$table.borderColor,
      borderColor = _style$table$borderCo === void 0 ? '#000' : _style$table$borderCo;
  var _style$header = style.header,
      _style$header$color = _style$header.color,
      headerColor = _style$header$color === void 0 ? '#fff' : _style$header$color,
      _style$header$backgro = _style$header.backgroundColor,
      headerBackgroundColor = _style$header$backgro === void 0 ? '#000' : _style$header$backgro,
      headerFontBold = _style$header.fontBold,
      headerFontItalic = _style$header.fontItalic,
      roundedBorders = _style$header.roundedBorders;
  var _style$body = style.body,
      _style$body$color = _style$body.color,
      bodyColor = _style$body$color === void 0 ? '#000' : _style$body$color,
      _style$body$backgroun = _style$body.backgroundColor1,
      bodyBackgroundColor1 = _style$body$backgroun === void 0 ? '#fff' : _style$body$backgroun,
      _style$body$backgroun2 = _style$body.backgroundColor2,
      bodyBackgroundColor2 = _style$body$backgroun2 === void 0 ? '#ccc' : _style$body$backgroun2,
      bodyFontBold = _style$body.fontBold,
      bodyFontItalic = _style$body.fontItalic,
      isZebraStyle = _style$body.isZebraStyle;
  var _style$footer = style.footer,
      footerBorderTopColor = _style$footer.borderTopColor,
      footerBorderTopStyle = _style$footer.borderTopStyle,
      _style$footer$color = _style$footer.color,
      footerColor = _style$footer$color === void 0 ? '#000' : _style$footer$color,
      _style$footer$backgro = _style$footer.backgroundColor1,
      footerBackgroundColor1 = _style$footer$backgro === void 0 ? '#fff' : _style$footer$backgro,
      _style$footer$backgro2 = _style$footer.backgroundColor2,
      footerBackgroundColor2 = _style$footer$backgro2 === void 0 ? '#ccc' : _style$footer$backgro2,
      footerFontBold = _style$footer.fontBold,
      footerFontItalic = _style$footer.fontItalic,
      isFooterZebraStyle = _style$footer.isZebraStyle;
  var options = [{
    value: 'hidden',
    label: t('No border')
  }, {
    value: 'solid',
    label: t('Normal')
  }, {
    value: 'dashed',
    label: t('Dashed')
  }, {
    value: 'dotted',
    label: t('Dotted')
  }];

  var handleTableStyleChange = function handleTableStyleChange(type, field, value) {
    var newStyle = {};
    var tableStyle = Object.assign({}, style[type]);
    tableStyle[field] = value;
    newStyle[type] = tableStyle;
    handleStyleChange(_objectSpread(_objectSpread({}, style), newStyle));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tabs, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
      className: "pr-3 pl-3",
      eventKey: t('Properties'),
      title: t('Properties'),
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
          children: t('Table')
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Border')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, {
              as: "select",
              id: "select-theme",
              value: borderStyle,
              onChange: function onChange(e) {
                return handleTableStyleChange('table', 'borderStyle', e.target.value);
              },
              children: options.map(function (_ref2) {
                var label = _ref2.label,
                    value = _ref2.value,
                    optionDisabled = _ref2.disabled;
                return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
                  disabled: optionDisabled,
                  value: value,
                  children: label
                }, "select-".concat(value, "-").concat(label));
              })
            })
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Border color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('table', 'borderColor', tempColor);
            },
            value: borderColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
          children: t('Header')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Border color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('header', 'backgroundColor', tempColor);
            },
            value: headerBackgroundColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Font color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('header', 'color', tempColor);
            },
            value: headerColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Bold')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: headerFontBold,
            id: "headerFontBold",
            onChange: function onChange(e) {
              return handleTableStyleChange('header', 'fontBold', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Italics')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: headerFontItalic,
            id: "headerFontItalic",
            onChange: function onChange(e) {
              return handleTableStyleChange('header', 'fontItalic', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Rounded')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: roundedBorders,
            id: "roundedBorders",
            onChange: function onChange(e) {
              return handleTableStyleChange('header', 'roundedBorders', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
          children: t('Body')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Font color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('body', 'color', tempColor);
            },
            value: bodyColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Alternating colors')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: isZebraStyle,
            id: "isZebraStyle",
            onChange: function onChange(e) {
              return handleTableStyleChange('body', 'isZebraStyle', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: "".concat(t('Background color'), " ").concat(isZebraStyle ? ' 1' : ''),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('body', 'backgroundColor1', tempColor);
            },
            value: bodyBackgroundColor1
          })
        }), isZebraStyle && /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: "".concat(t('Background color'), " 2"),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('body', 'backgroundColor2', tempColor);
            },
            value: bodyBackgroundColor2
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Bold')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: bodyFontBold,
            id: "bodyFontBold",
            onChange: function onChange(e) {
              return handleTableStyleChange('body', 'fontBold', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Italics')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            id: "bodyFontItalic",
            onChange: function onChange(e) {
              return handleTableStyleChange('body', 'fontItalic', e.target.checked);
            },
            checked: bodyFontItalic
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
          children: t('Table footer')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Font color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('footer', 'color', tempColor);
            },
            value: footerColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Alternating colors')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: isFooterZebraStyle,
            id: "isFooterZebraStyle",
            onChange: function onChange(e) {
              return handleTableStyleChange('footer', 'isZebraStyle', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: "".concat(t('Background color'), " ").concat(isFooterZebraStyle ? ' 1' : ''),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('footer', 'backgroundColor1', tempColor);
            },
            value: footerBackgroundColor1
          })
        }), isFooterZebraStyle && /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: "".concat(t('Background color'), " 2"),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('footer', 'backgroundColor2', tempColor);
            },
            value: footerBackgroundColor2
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Border')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, {
              as: "select",
              id: "select-theme",
              onChange: function onChange(e) {
                return handleTableStyleChange('footer', 'borderTopStyle', e.target.value);
              },
              value: footerBorderTopStyle,
              children: options.map(function (_ref3) {
                var label = _ref3.label,
                    value = _ref3.value,
                    optionDisabled = _ref3.disabled;
                return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
                  disabled: optionDisabled,
                  value: value,
                  children: label
                }, "select-".concat(value, "-").concat(label));
              })
            })
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
            label: t('Border color'),
            onChange: function onChange(tempColor) {
              return handleTableStyleChange('footer', 'borderTopColor', tempColor);
            },
            value: footerBorderTopColor
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Bold')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: footerFontBold,
            id: "footerFontBold",
            onChange: function onChange(e) {
              return handleTableStyleChange('footer', 'fontBold', e.target.checked);
            }
          })]
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
            children: t('Italics')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
            checked: footerFontItalic,
            id: "footerFontItalic",
            onChange: function onChange(e) {
              return handleTableStyleChange('footer', 'fontItalic', e.target.checked);
            }
          })]
        })]
      })
    })
  });
};

SupportTablePanel.propTypes = {
  style: _propTypes.default.object,
  handleStyleChange: _propTypes.default.func
};
var _default = SupportTablePanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1N1cHBvcnRUYWJsZVBhbmVsLmpzIl0sIm5hbWVzIjpbIlN1cHBvcnRUYWJsZVBhbmVsIiwic3R5bGUiLCJoYW5kbGVTdHlsZUNoYW5nZSIsInQiLCJ0YWJsZSIsImJvcmRlclN0eWxlIiwiYm9yZGVyQ29sb3IiLCJoZWFkZXIiLCJjb2xvciIsImhlYWRlckNvbG9yIiwiYmFja2dyb3VuZENvbG9yIiwiaGVhZGVyQmFja2dyb3VuZENvbG9yIiwiaGVhZGVyRm9udEJvbGQiLCJmb250Qm9sZCIsImhlYWRlckZvbnRJdGFsaWMiLCJmb250SXRhbGljIiwicm91bmRlZEJvcmRlcnMiLCJib2R5IiwiYm9keUNvbG9yIiwiYmFja2dyb3VuZENvbG9yMSIsImJvZHlCYWNrZ3JvdW5kQ29sb3IxIiwiYmFja2dyb3VuZENvbG9yMiIsImJvZHlCYWNrZ3JvdW5kQ29sb3IyIiwiYm9keUZvbnRCb2xkIiwiYm9keUZvbnRJdGFsaWMiLCJpc1plYnJhU3R5bGUiLCJmb290ZXIiLCJmb290ZXJCb3JkZXJUb3BDb2xvciIsImJvcmRlclRvcENvbG9yIiwiZm9vdGVyQm9yZGVyVG9wU3R5bGUiLCJib3JkZXJUb3BTdHlsZSIsImZvb3RlckNvbG9yIiwiZm9vdGVyQmFja2dyb3VuZENvbG9yMSIsImZvb3RlckJhY2tncm91bmRDb2xvcjIiLCJmb290ZXJGb250Qm9sZCIsImZvb3RlckZvbnRJdGFsaWMiLCJpc0Zvb3RlclplYnJhU3R5bGUiLCJvcHRpb25zIiwidmFsdWUiLCJsYWJlbCIsImhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UiLCJ0eXBlIiwiZmllbGQiLCJuZXdTdHlsZSIsInRhYmxlU3R5bGUiLCJPYmplY3QiLCJhc3NpZ24iLCJlIiwidGFyZ2V0IiwibWFwIiwib3B0aW9uRGlzYWJsZWQiLCJkaXNhYmxlZCIsInRlbXBDb2xvciIsImNoZWNrZWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztBQVFBLElBQU1BLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FBNEM7QUFBQSxpRkFBUCxFQUFPO0FBQUEsd0JBQXpDQyxLQUF5QztBQUFBLE1BQXpDQSxLQUF5QywyQkFBakMsRUFBaUM7QUFBQSxNQUE3QkMsaUJBQTZCLFFBQTdCQSxpQkFBNkI7O0FBQ2xFLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxxQkFBeURGLEtBQUssQ0FBQ0csS0FBL0Q7QUFBQSwyQ0FBUUMsV0FBUjtBQUFBLE1BQVFBLFdBQVIsc0NBQXNCLFFBQXRCO0FBQUEsMkNBQWdDQyxXQUFoQztBQUFBLE1BQWdDQSxXQUFoQyxzQ0FBOEMsTUFBOUM7QUFDQSxzQkFNSUwsS0FBSyxDQUFDTSxNQU5WO0FBQUEsMENBQ0lDLEtBREo7QUFBQSxNQUNXQyxXQURYLG9DQUN5QixNQUR6QjtBQUFBLDRDQUVJQyxlQUZKO0FBQUEsTUFFcUJDLHFCQUZyQixzQ0FFNkMsTUFGN0M7QUFBQSxNQUdjQyxjQUhkLGlCQUdJQyxRQUhKO0FBQUEsTUFJZ0JDLGdCQUpoQixpQkFJSUMsVUFKSjtBQUFBLE1BS0lDLGNBTEosaUJBS0lBLGNBTEo7QUFPQSxvQkFPSWYsS0FBSyxDQUFDZ0IsSUFQVjtBQUFBLHNDQUNJVCxLQURKO0FBQUEsTUFDV1UsU0FEWCxrQ0FDdUIsTUFEdkI7QUFBQSwwQ0FFSUMsZ0JBRko7QUFBQSxNQUVzQkMsb0JBRnRCLHNDQUU2QyxNQUY3QztBQUFBLDJDQUdJQyxnQkFISjtBQUFBLE1BR3NCQyxvQkFIdEIsdUNBRzZDLE1BSDdDO0FBQUEsTUFJY0MsWUFKZCxlQUlJVixRQUpKO0FBQUEsTUFLZ0JXLGNBTGhCLGVBS0lULFVBTEo7QUFBQSxNQU1JVSxZQU5KLGVBTUlBLFlBTko7QUFRQSxzQkFTSXhCLEtBQUssQ0FBQ3lCLE1BVFY7QUFBQSxNQUNvQkMsb0JBRHBCLGlCQUNJQyxjQURKO0FBQUEsTUFFb0JDLG9CQUZwQixpQkFFSUMsY0FGSjtBQUFBLDBDQUdJdEIsS0FISjtBQUFBLE1BR1d1QixXQUhYLG9DQUd5QixNQUh6QjtBQUFBLDRDQUlJWixnQkFKSjtBQUFBLE1BSXNCYSxzQkFKdEIsc0NBSStDLE1BSi9DO0FBQUEsNkNBS0lYLGdCQUxKO0FBQUEsTUFLc0JZLHNCQUx0Qix1Q0FLK0MsTUFML0M7QUFBQSxNQU1jQyxjQU5kLGlCQU1JckIsUUFOSjtBQUFBLE1BT2dCc0IsZ0JBUGhCLGlCQU9JcEIsVUFQSjtBQUFBLE1BUWtCcUIsa0JBUmxCLGlCQVFJWCxZQVJKO0FBVUEsTUFBTVksT0FBTyxHQUFHLENBQ1o7QUFBRUMsSUFBQUEsS0FBSyxFQUFFLFFBQVQ7QUFBbUJDLElBQUFBLEtBQUssRUFBRXBDLENBQUMsQ0FBQyxXQUFEO0FBQTNCLEdBRFksRUFFWjtBQUFFbUMsSUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JDLElBQUFBLEtBQUssRUFBRXBDLENBQUMsQ0FBQyxRQUFEO0FBQTFCLEdBRlksRUFHWjtBQUFFbUMsSUFBQUEsS0FBSyxFQUFFLFFBQVQ7QUFBbUJDLElBQUFBLEtBQUssRUFBRXBDLENBQUMsQ0FBQyxRQUFEO0FBQTNCLEdBSFksRUFJWjtBQUFFbUMsSUFBQUEsS0FBSyxFQUFFLFFBQVQ7QUFBbUJDLElBQUFBLEtBQUssRUFBRXBDLENBQUMsQ0FBQyxRQUFEO0FBQTNCLEdBSlksQ0FBaEI7O0FBT0EsTUFBTXFDLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBQ0MsSUFBRCxFQUFPQyxLQUFQLEVBQWNKLEtBQWQsRUFBd0I7QUFDbkQsUUFBSUssUUFBUSxHQUFHLEVBQWY7QUFDQSxRQUFJQyxVQUFVLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0I3QyxLQUFLLENBQUN3QyxJQUFELENBQXZCLENBQWpCO0FBQ0FHLElBQUFBLFVBQVUsQ0FBQ0YsS0FBRCxDQUFWLEdBQW9CSixLQUFwQjtBQUNBSyxJQUFBQSxRQUFRLENBQUNGLElBQUQsQ0FBUixHQUFpQkcsVUFBakI7QUFDQTFDLElBQUFBLGlCQUFpQixpQ0FDVkQsS0FEVSxHQUVWMEMsUUFGVSxFQUFqQjtBQUlILEdBVEQ7O0FBV0Esc0JBQ0kscUJBQUMsb0JBQUQ7QUFBQSwyQkFDSSxxQkFBQyxtQkFBRDtBQUNJLE1BQUEsU0FBUyxFQUFDLFdBRGQ7QUFFSSxNQUFBLFFBQVEsRUFBRXhDLENBQUMsQ0FBQyxZQUFELENBRmY7QUFHSSxNQUFBLEtBQUssRUFBRUEsQ0FBQyxDQUFDLFlBQUQsQ0FIWjtBQUFBLDZCQUtJLHNCQUFDLGtCQUFEO0FBQUEsZ0NBQ0kscUJBQUMsZUFBRDtBQUFBLG9CQUFVQSxDQUFDLENBQUMsT0FBRDtBQUFYLFVBREosZUFFSSxzQkFBQyxzQkFBRDtBQUFBLGtDQUNJLHFCQUFDLGFBQUQ7QUFBQSxzQkFBUUEsQ0FBQyxDQUFDLFFBQUQ7QUFBVCxZQURKLGVBRUkscUJBQUMsd0JBQUQ7QUFBQSxtQ0FDSSxxQkFBQyxvQkFBRCxDQUFNLE9BQU47QUFDSSxjQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksY0FBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLGNBQUEsS0FBSyxFQUFFRSxXQUhYO0FBSUksY0FBQSxRQUFRLEVBQUUsa0JBQUMwQyxDQUFEO0FBQUEsdUJBQ05QLHNCQUFzQixDQUNsQixPQURrQixFQUVsQixhQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNWLEtBSFMsQ0FEaEI7QUFBQSxlQUpkO0FBQUEsd0JBWUtELE9BQU8sQ0FBQ1ksR0FBUixDQUNHO0FBQUEsb0JBQ0lWLEtBREosU0FDSUEsS0FESjtBQUFBLG9CQUVJRCxLQUZKLFNBRUlBLEtBRko7QUFBQSxvQkFHY1ksY0FIZCxTQUdJQyxRQUhKO0FBQUEsb0NBS0k7QUFDSSxrQkFBQSxRQUFRLEVBQUVELGNBRGQ7QUFHSSxrQkFBQSxLQUFLLEVBQUVaLEtBSFg7QUFBQSw0QkFLS0M7QUFMTCxvQ0FFbUJELEtBRm5CLGNBRTRCQyxLQUY1QixFQUxKO0FBQUEsZUFESDtBQVpMO0FBREosWUFGSjtBQUFBLFVBRkosZUFtQ0kscUJBQUMsc0JBQUQ7QUFBQSxpQ0FDSSxxQkFBQyx5QkFBRDtBQUNJLFlBQUEsS0FBSyxFQUFFcEMsQ0FBQyxDQUFDLGNBQUQsQ0FEWjtBQUVJLFlBQUEsUUFBUSxFQUFFLGtCQUFDaUQsU0FBRDtBQUFBLHFCQUNOWixzQkFBc0IsQ0FDbEIsT0FEa0IsRUFFbEIsYUFGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUEsYUFGZDtBQVNJLFlBQUEsS0FBSyxFQUFFOUM7QUFUWDtBQURKLFVBbkNKLGVBaURJLHFCQUFDLGVBQUQ7QUFBQSxvQkFBVUgsQ0FBQyxDQUFDLFFBQUQ7QUFBWCxVQWpESixlQWtESSxxQkFBQyxzQkFBRDtBQUFBLGlDQUNJLHFCQUFDLHlCQUFEO0FBQ0ksWUFBQSxLQUFLLEVBQUVBLENBQUMsQ0FBQyxjQUFELENBRFo7QUFFSSxZQUFBLFFBQVEsRUFBRSxrQkFBQ2lELFNBQUQ7QUFBQSxxQkFDTlosc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLGlCQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQSxhQUZkO0FBU0ksWUFBQSxLQUFLLEVBQUV6QztBQVRYO0FBREosVUFsREosZUErREkscUJBQUMsc0JBQUQ7QUFBQSxpQ0FDSSxxQkFBQyx5QkFBRDtBQUNJLFlBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsWUFBRCxDQURaO0FBRUksWUFBQSxRQUFRLEVBQUUsa0JBQUNpRCxTQUFEO0FBQUEscUJBQ05aLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixPQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQSxhQUZkO0FBU0ksWUFBQSxLQUFLLEVBQUUzQztBQVRYO0FBREosVUEvREosZUE0RUksc0JBQUMsc0JBQUQ7QUFBQSxrQ0FDSSxxQkFBQyxhQUFEO0FBQUEsc0JBQVFOLENBQUMsQ0FBQyxNQUFEO0FBQVQsWUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksWUFBQSxPQUFPLEVBQUVTLGNBRGI7QUFFSSxZQUFBLEVBQUUsRUFBQyxnQkFGUDtBQUdJLFlBQUEsUUFBUSxFQUFFLGtCQUFDbUMsQ0FBRDtBQUFBLHFCQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsVUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTSyxPQUhTLENBRGhCO0FBQUE7QUFIZCxZQUZKO0FBQUEsVUE1RUosZUEwRkksc0JBQUMsc0JBQUQ7QUFBQSxrQ0FDSSxxQkFBQyxhQUFEO0FBQUEsc0JBQVFsRCxDQUFDLENBQUMsU0FBRDtBQUFULFlBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFlBQUEsT0FBTyxFQUFFVyxnQkFEYjtBQUVJLFlBQUEsRUFBRSxFQUFDLGtCQUZQO0FBR0ksWUFBQSxRQUFRLEVBQUUsa0JBQUNpQyxDQUFEO0FBQUEscUJBQ05QLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixZQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQTtBQUhkLFlBRko7QUFBQSxVQTFGSixlQXdHSSxzQkFBQyxzQkFBRDtBQUFBLGtDQUNJLHFCQUFDLGFBQUQ7QUFBQSxzQkFBUWxELENBQUMsQ0FBQyxTQUFEO0FBQVQsWUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksWUFBQSxPQUFPLEVBQUVhLGNBRGI7QUFFSSxZQUFBLEVBQUUsRUFBQyxnQkFGUDtBQUdJLFlBQUEsUUFBUSxFQUFFLGtCQUFDK0IsQ0FBRDtBQUFBLHFCQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsZ0JBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBO0FBSGQsWUFGSjtBQUFBLFVBeEdKLGVBdUhJLHFCQUFDLGVBQUQ7QUFBQSxvQkFBVWxELENBQUMsQ0FBQyxNQUFEO0FBQVgsVUF2SEosZUF5SEkscUJBQUMsc0JBQUQ7QUFBQSxpQ0FDSSxxQkFBQyx5QkFBRDtBQUNJLFlBQUEsS0FBSyxFQUFFQSxDQUFDLENBQUMsWUFBRCxDQURaO0FBRUksWUFBQSxRQUFRLEVBQUUsa0JBQUNpRCxTQUFEO0FBQUEscUJBQ05aLHNCQUFzQixDQUNsQixNQURrQixFQUVsQixPQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQSxhQUZkO0FBU0ksWUFBQSxLQUFLLEVBQUVsQztBQVRYO0FBREosVUF6SEosZUFzSUksc0JBQUMsc0JBQUQ7QUFBQSxrQ0FDSSxxQkFBQyxhQUFEO0FBQUEsc0JBQVFmLENBQUMsQ0FBQyxvQkFBRDtBQUFULFlBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFlBQUEsT0FBTyxFQUFFc0IsWUFEYjtBQUVJLFlBQUEsRUFBRSxFQUFDLGNBRlA7QUFHSSxZQUFBLFFBQVEsRUFBRSxrQkFBQ3NCLENBQUQ7QUFBQSxxQkFDTlAsc0JBQXNCLENBQ2xCLE1BRGtCLEVBRWxCLGNBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBO0FBSGQsWUFGSjtBQUFBLFVBdElKLGVBb0pJLHFCQUFDLHNCQUFEO0FBQUEsaUNBQ0kscUJBQUMseUJBQUQ7QUFDSSxZQUFBLEtBQUssWUFBS2xELENBQUMsQ0FBQyxrQkFBRCxDQUFOLGNBQ0RzQixZQUFZLEdBQUcsSUFBSCxHQUFVLEVBRHJCLENBRFQ7QUFJSSxZQUFBLFFBQVEsRUFBRSxrQkFBQzJCLFNBQUQ7QUFBQSxxQkFDTlosc0JBQXNCLENBQ2xCLE1BRGtCLEVBRWxCLGtCQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQSxhQUpkO0FBV0ksWUFBQSxLQUFLLEVBQUVoQztBQVhYO0FBREosVUFwSkosRUFtS0tLLFlBQVksaUJBQ1QscUJBQUMsc0JBQUQ7QUFBQSxpQ0FDSSxxQkFBQyx5QkFBRDtBQUNJLFlBQUEsS0FBSyxZQUFLdEIsQ0FBQyxDQUFDLGtCQUFELENBQU4sT0FEVDtBQUVJLFlBQUEsUUFBUSxFQUFFLGtCQUFDaUQsU0FBRDtBQUFBLHFCQUNOWixzQkFBc0IsQ0FDbEIsTUFEa0IsRUFFbEIsa0JBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBLGFBRmQ7QUFTSSxZQUFBLEtBQUssRUFBRTlCO0FBVFg7QUFESixVQXBLUixlQWtMSSxzQkFBQyxzQkFBRDtBQUFBLGtDQUNJLHFCQUFDLGFBQUQ7QUFBQSxzQkFBUW5CLENBQUMsQ0FBQyxNQUFEO0FBQVQsWUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksWUFBQSxPQUFPLEVBQUVvQixZQURiO0FBRUksWUFBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLFlBQUEsUUFBUSxFQUFFLGtCQUFDd0IsQ0FBRDtBQUFBLHFCQUNOUCxzQkFBc0IsQ0FDbEIsTUFEa0IsRUFFbEIsVUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTSyxPQUhTLENBRGhCO0FBQUE7QUFIZCxZQUZKO0FBQUEsVUFsTEosZUFnTUksc0JBQUMsc0JBQUQ7QUFBQSxrQ0FDSSxxQkFBQyxhQUFEO0FBQUEsc0JBQVFsRCxDQUFDLENBQUMsU0FBRDtBQUFULFlBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFlBQUEsRUFBRSxFQUFDLGdCQURQO0FBRUksWUFBQSxRQUFRLEVBQUUsa0JBQUM0QyxDQUFEO0FBQUEscUJBQ05QLHNCQUFzQixDQUNsQixNQURrQixFQUVsQixZQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQSxhQUZkO0FBU0ksWUFBQSxPQUFPLEVBQUU3QjtBQVRiLFlBRko7QUFBQSxVQWhNSixlQStNSSxxQkFBQyxlQUFEO0FBQUEsb0JBQVVyQixDQUFDLENBQUMsY0FBRDtBQUFYLFVBL01KLGVBZ05JLHFCQUFDLHNCQUFEO0FBQUEsaUNBQ0kscUJBQUMseUJBQUQ7QUFDSSxZQUFBLEtBQUssRUFBRUEsQ0FBQyxDQUFDLFlBQUQsQ0FEWjtBQUVJLFlBQUEsUUFBUSxFQUFFLGtCQUFDaUQsU0FBRDtBQUFBLHFCQUNOWixzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsT0FGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUEsYUFGZDtBQVNJLFlBQUEsS0FBSyxFQUFFckI7QUFUWDtBQURKLFVBaE5KLGVBNk5JLHNCQUFDLHNCQUFEO0FBQUEsa0NBQ0kscUJBQUMsYUFBRDtBQUFBLHNCQUFRNUIsQ0FBQyxDQUFDLG9CQUFEO0FBQVQsWUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksWUFBQSxPQUFPLEVBQUVpQyxrQkFEYjtBQUVJLFlBQUEsRUFBRSxFQUFDLG9CQUZQO0FBR0ksWUFBQSxRQUFRLEVBQUUsa0JBQUNXLENBQUQ7QUFBQSxxQkFDTlAsc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLGNBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBO0FBSGQsWUFGSjtBQUFBLFVBN05KLGVBMk9JLHFCQUFDLHNCQUFEO0FBQUEsaUNBQ0kscUJBQUMseUJBQUQ7QUFDSSxZQUFBLEtBQUssWUFBS2xELENBQUMsQ0FBQyxrQkFBRCxDQUFOLGNBQ0RpQyxrQkFBa0IsR0FBRyxJQUFILEdBQVUsRUFEM0IsQ0FEVDtBQUlJLFlBQUEsUUFBUSxFQUFFLGtCQUFDZ0IsU0FBRDtBQUFBLHFCQUNOWixzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsa0JBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBLGFBSmQ7QUFXSSxZQUFBLEtBQUssRUFBRXBCO0FBWFg7QUFESixVQTNPSixFQTBQS0ksa0JBQWtCLGlCQUNmLHFCQUFDLHNCQUFEO0FBQUEsaUNBQ0kscUJBQUMseUJBQUQ7QUFDSSxZQUFBLEtBQUssWUFBS2pDLENBQUMsQ0FBQyxrQkFBRCxDQUFOLE9BRFQ7QUFFSSxZQUFBLFFBQVEsRUFBRSxrQkFBQ2lELFNBQUQ7QUFBQSxxQkFDTlosc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLGtCQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQSxhQUZkO0FBU0ksWUFBQSxLQUFLLEVBQUVuQjtBQVRYO0FBREosVUEzUFIsZUF5UUksc0JBQUMsc0JBQUQ7QUFBQSxrQ0FDSSxxQkFBQyxhQUFEO0FBQUEsc0JBQVE5QixDQUFDLENBQUMsUUFBRDtBQUFULFlBREosZUFFSSxxQkFBQyx3QkFBRDtBQUFBLG1DQUNJLHFCQUFDLG9CQUFELENBQU0sT0FBTjtBQUNJLGNBQUEsRUFBRSxFQUFDLFFBRFA7QUFFSSxjQUFBLEVBQUUsRUFBQyxjQUZQO0FBR0ksY0FBQSxRQUFRLEVBQUUsa0JBQUM0QyxDQUFEO0FBQUEsdUJBQ05QLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixnQkFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTVixLQUhTLENBRGhCO0FBQUEsZUFIZDtBQVVJLGNBQUEsS0FBSyxFQUFFVCxvQkFWWDtBQUFBLHdCQVlLUSxPQUFPLENBQUNZLEdBQVIsQ0FDRztBQUFBLG9CQUNJVixLQURKLFNBQ0lBLEtBREo7QUFBQSxvQkFFSUQsS0FGSixTQUVJQSxLQUZKO0FBQUEsb0JBR2NZLGNBSGQsU0FHSUMsUUFISjtBQUFBLG9DQUtJO0FBQ0ksa0JBQUEsUUFBUSxFQUFFRCxjQURkO0FBR0ksa0JBQUEsS0FBSyxFQUFFWixLQUhYO0FBQUEsNEJBS0tDO0FBTEwsb0NBRW1CRCxLQUZuQixjQUU0QkMsS0FGNUIsRUFMSjtBQUFBLGVBREg7QUFaTDtBQURKLFlBRko7QUFBQSxVQXpRSixlQTBTSSxxQkFBQyxzQkFBRDtBQUFBLGlDQUNJLHFCQUFDLHlCQUFEO0FBQ0ksWUFBQSxLQUFLLEVBQUVwQyxDQUFDLENBQUMsY0FBRCxDQURaO0FBRUksWUFBQSxRQUFRLEVBQUUsa0JBQUNpRCxTQUFEO0FBQUEscUJBQ05aLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixnQkFGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUEsYUFGZDtBQVNJLFlBQUEsS0FBSyxFQUFFekI7QUFUWDtBQURKLFVBMVNKLGVBdVRJLHNCQUFDLHNCQUFEO0FBQUEsa0NBQ0kscUJBQUMsYUFBRDtBQUFBLHNCQUFReEIsQ0FBQyxDQUFDLE1BQUQ7QUFBVCxZQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxZQUFBLE9BQU8sRUFBRStCLGNBRGI7QUFFSSxZQUFBLEVBQUUsRUFBQyxnQkFGUDtBQUdJLFlBQUEsUUFBUSxFQUFFLGtCQUFDYSxDQUFEO0FBQUEscUJBQ05QLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixVQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQTtBQUhkLFlBRko7QUFBQSxVQXZUSixlQXFVSSxzQkFBQyxzQkFBRDtBQUFBLGtDQUNJLHFCQUFDLGFBQUQ7QUFBQSxzQkFBUWxELENBQUMsQ0FBQyxTQUFEO0FBQVQsWUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksWUFBQSxPQUFPLEVBQUVnQyxnQkFEYjtBQUVJLFlBQUEsRUFBRSxFQUFDLGtCQUZQO0FBR0ksWUFBQSxRQUFRLEVBQUUsa0JBQUNZLENBQUQ7QUFBQSxxQkFDTlAsc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLFlBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBO0FBSGQsWUFGSjtBQUFBLFVBclVKO0FBQUE7QUFMSjtBQURKLElBREo7QUE4VkgsQ0E1WUQ7O0FBOFlBckQsaUJBQWlCLENBQUNzRCxTQUFsQixHQUE4QjtBQUMxQnJELEVBQUFBLEtBQUssRUFBRXNELG1CQUFVQyxNQURTO0FBRTFCdEQsRUFBQUEsaUJBQWlCLEVBQUVxRCxtQkFBVUU7QUFGSCxDQUE5QjtlQUtlekQsaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgRm9ybSwgVGFiLCBUYWJzIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCBDb2xvclBpY2tlcklucHV0IGZyb20gJ0Bjb21wb25lbnRzL0NvbG9yUGlja2VySW5wdXQnO1xuaW1wb3J0IHsgRmxleENvbHVtbiB9IGZyb20gJ0Bjb21wb25lbnRzL3N0eWxlZCc7XG5cbmltcG9ydCB7XG4gICAgQ2hlY2tib3hJbnB1dCxcbiAgICBJbnB1dENvbnRhaW5lcixcbiAgICBMYWJlbCxcbiAgICBTdHlsZWRJbnB1dEdyb3VwLFxuICAgIFN1Ym1lbnUsXG59IGZyb20gJy4vU3R5bGVzJztcblxuY29uc3QgU3VwcG9ydFRhYmxlUGFuZWwgPSAoeyBzdHlsZSA9IHt9LCBoYW5kbGVTdHlsZUNoYW5nZSB9ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3QgeyBib3JkZXJTdHlsZSA9ICdoaWRkZW4nLCBib3JkZXJDb2xvciA9ICcjMDAwJyB9ID0gc3R5bGUudGFibGU7XG4gICAgY29uc3Qge1xuICAgICAgICBjb2xvcjogaGVhZGVyQ29sb3IgPSAnI2ZmZicsXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogaGVhZGVyQmFja2dyb3VuZENvbG9yID0gJyMwMDAnLFxuICAgICAgICBmb250Qm9sZDogaGVhZGVyRm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWM6IGhlYWRlckZvbnRJdGFsaWMsXG4gICAgICAgIHJvdW5kZWRCb3JkZXJzLFxuICAgIH0gPSBzdHlsZS5oZWFkZXI7XG4gICAgY29uc3Qge1xuICAgICAgICBjb2xvcjogYm9keUNvbG9yID0gJyMwMDAnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IxOiBib2R5QmFja2dyb3VuZENvbG9yMSA9ICcjZmZmJyxcbiAgICAgICAgYmFja2dyb3VuZENvbG9yMjogYm9keUJhY2tncm91bmRDb2xvcjIgPSAnI2NjYycsXG4gICAgICAgIGZvbnRCb2xkOiBib2R5Rm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWM6IGJvZHlGb250SXRhbGljLFxuICAgICAgICBpc1plYnJhU3R5bGUsXG4gICAgfSA9IHN0eWxlLmJvZHk7XG4gICAgY29uc3Qge1xuICAgICAgICBib3JkZXJUb3BDb2xvcjogZm9vdGVyQm9yZGVyVG9wQ29sb3IsXG4gICAgICAgIGJvcmRlclRvcFN0eWxlOiBmb290ZXJCb3JkZXJUb3BTdHlsZSxcbiAgICAgICAgY29sb3I6IGZvb3RlckNvbG9yID0gJyMwMDAnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IxOiBmb290ZXJCYWNrZ3JvdW5kQ29sb3IxID0gJyNmZmYnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IyOiBmb290ZXJCYWNrZ3JvdW5kQ29sb3IyID0gJyNjY2MnLFxuICAgICAgICBmb250Qm9sZDogZm9vdGVyRm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWM6IGZvb3RlckZvbnRJdGFsaWMsXG4gICAgICAgIGlzWmVicmFTdHlsZTogaXNGb290ZXJaZWJyYVN0eWxlLFxuICAgIH0gPSBzdHlsZS5mb290ZXI7XG4gICAgY29uc3Qgb3B0aW9ucyA9IFtcbiAgICAgICAgeyB2YWx1ZTogJ2hpZGRlbicsIGxhYmVsOiB0KCdObyBib3JkZXInKSB9LFxuICAgICAgICB7IHZhbHVlOiAnc29saWQnLCBsYWJlbDogdCgnTm9ybWFsJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2Rhc2hlZCcsIGxhYmVsOiB0KCdEYXNoZWQnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnZG90dGVkJywgbGFiZWw6IHQoJ0RvdHRlZCcpIH0sXG4gICAgXTtcblxuICAgIGNvbnN0IGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UgPSAodHlwZSwgZmllbGQsIHZhbHVlKSA9PiB7XG4gICAgICAgIGxldCBuZXdTdHlsZSA9IHt9O1xuICAgICAgICBsZXQgdGFibGVTdHlsZSA9IE9iamVjdC5hc3NpZ24oe30sIHN0eWxlW3R5cGVdKTtcbiAgICAgICAgdGFibGVTdHlsZVtmaWVsZF0gPSB2YWx1ZTtcbiAgICAgICAgbmV3U3R5bGVbdHlwZV0gPSB0YWJsZVN0eWxlO1xuICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi5zdHlsZSxcbiAgICAgICAgICAgIC4uLm5ld1N0eWxlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFRhYnM+XG4gICAgICAgICAgICA8VGFiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHItMyBwbC0zXCJcbiAgICAgICAgICAgICAgICBldmVudEtleT17dCgnUHJvcGVydGllcycpfVxuICAgICAgICAgICAgICAgIHRpdGxlPXt0KCdQcm9wZXJ0aWVzJyl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICAgICAgICAgIDxTdWJtZW51Pnt0KCdUYWJsZScpfTwvU3VibWVudT5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb3JkZXInKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2VsZWN0LXRoZW1lXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvcmRlclN0eWxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0YWJsZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlclN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7b3B0aW9ucy5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBvcHRpb25EaXNhYmxlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtvcHRpb25EaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgc2VsZWN0LSR7dmFsdWV9LSR7bGFiZWx9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2xhYmVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCb3JkZXIgY29sb3InKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0YWJsZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyQ29sb3InLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvcmRlckNvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cblxuICAgICAgICAgICAgICAgICAgICA8U3VibWVudT57dCgnSGVhZGVyJyl9PC9TdWJtZW51PlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCb3JkZXIgY29sb3InKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoZWFkZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JhY2tncm91bmRDb2xvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17aGVhZGVyQmFja2dyb3VuZENvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnRm9udCBjb2xvcicpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2hlYWRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY29sb3InLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2hlYWRlckNvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb2xkJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGVhZGVyRm9udEJvbGR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJoZWFkZXJGb250Qm9sZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2hlYWRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9udEJvbGQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdJdGFsaWNzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGVhZGVyRm9udEl0YWxpY31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZD1cImhlYWRlckZvbnRJdGFsaWNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoZWFkZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZvbnRJdGFsaWMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdSb3VuZGVkJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17cm91bmRlZEJvcmRlcnN9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJyb3VuZGVkQm9yZGVyc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2hlYWRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAncm91bmRlZEJvcmRlcnMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cblxuICAgICAgICAgICAgICAgICAgICA8U3VibWVudT57dCgnQm9keScpfTwvU3VibWVudT5cblxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdGb250IGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY29sb3InLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvZHlDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnQWx0ZXJuYXRpbmcgY29sb3JzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aXNaZWJyYVN0eWxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwiaXNaZWJyYVN0eWxlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaXNaZWJyYVN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGUudGFyZ2V0LmNoZWNrZWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e2Ake3QoJ0JhY2tncm91bmQgY29sb3InKX0gJHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXNaZWJyYVN0eWxlID8gJyAxJyA6ICcnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZENvbG9yMScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Ym9keUJhY2tncm91bmRDb2xvcjF9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICB7aXNaZWJyYVN0eWxlICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17YCR7dCgnQmFja2dyb3VuZCBjb2xvcicpfSAyYH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib2R5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZENvbG9yMicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvZHlCYWNrZ3JvdW5kQ29sb3IyfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0JvbGQnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtib2R5Rm9udEJvbGR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJib2R5Rm9udEJvbGRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib2R5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250Qm9sZCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0l0YWxpY3MnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZD1cImJvZHlGb250SXRhbGljXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9udEl0YWxpYycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17Ym9keUZvbnRJdGFsaWN9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuXG4gICAgICAgICAgICAgICAgICAgIDxTdWJtZW51Pnt0KCdUYWJsZSBmb290ZXInKX08L1N1Ym1lbnU+XG4gICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e3QoJ0ZvbnQgY29sb3InKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2NvbG9yJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBDb2xvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtmb290ZXJDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnQWx0ZXJuYXRpbmcgY29sb3JzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aXNGb290ZXJaZWJyYVN0eWxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwiaXNGb290ZXJaZWJyYVN0eWxlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdpc1plYnJhU3R5bGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17YCR7dCgnQmFja2dyb3VuZCBjb2xvcicpfSAke1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0Zvb3RlclplYnJhU3R5bGUgPyAnIDEnIDogJydcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JhY2tncm91bmRDb2xvcjEnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2Zvb3RlckJhY2tncm91bmRDb2xvcjF9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICB7aXNGb290ZXJaZWJyYVN0eWxlICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17YCR7dCgnQmFja2dyb3VuZCBjb2xvcicpfSAyYH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kQ29sb3IyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Zm9vdGVyQmFja2dyb3VuZENvbG9yMn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb3JkZXInKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2VsZWN0LXRoZW1lXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyVG9wU3R5bGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGUudGFyZ2V0LnZhbHVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2Zvb3RlckJvcmRlclRvcFN0eWxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge29wdGlvbnMubWFwKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQm9yZGVyIGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib3JkZXJUb3BDb2xvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Zm9vdGVyQm9yZGVyVG9wQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0JvbGQnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtmb290ZXJGb250Qm9sZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZD1cImZvb3RlckZvbnRCb2xkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250Qm9sZCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0l0YWxpY3MnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtmb290ZXJGb250SXRhbGljfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwiZm9vdGVyRm9udEl0YWxpY1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2Zvb3RlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9udEl0YWxpYycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDwvRmxleENvbHVtbj5cbiAgICAgICAgICAgIDwvVGFiPlxuICAgICAgICA8L1RhYnM+XG4gICAgKTtcbn07XG5cblN1cHBvcnRUYWJsZVBhbmVsLnByb3BUeXBlcyA9IHtcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBoYW5kbGVTdHlsZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBTdXBwb3J0VGFibGVQYW5lbDtcbiJdfQ==