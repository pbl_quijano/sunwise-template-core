"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n"])));

var StyledInputGroup = (0, _styledComponents.default)(_reactBootstrap.InputGroup)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n    & select {\n        height: 46px;\n        width: 133px;\n        border: 1px solid #ecedf0;\n        border-radius: 3px;\n        background-color: #ffffff;\n    }\n"])));

var InputContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    display: flex;\n    align-items: center;\n    margin: 4px 0;\n"])));

var Label = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    min-height: 16px;\n    width: 135px;\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    text-align: right;\n    margin-right: 23px;\n"])));

var MoneyChartPanel = function MoneyChartPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref.handleValueChange,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _value$colors = value.colors,
      colors = _value$colors === void 0 ? ['#000', '#000'] : _value$colors,
      _value$type = value.type,
      type = _value$type === void 0 ? 'bar' : _value$type;
  var options = [{
    value: 'bar',
    label: t('Bars')
  }, {
    value: 'line',
    label: t('Line')
  }, {
    value: 'area',
    label: t('Area')
  }];

  var onChangeColor = function onChangeColor(tempColors) {
    return handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      colors: tempColors
    }));
  };

  var onChangeType = function onChangeType(e) {
    return handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      type: e.target.value
    }));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
        children: t('Type')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, {
          as: "select",
          id: "select-theme",
          onChange: onChangeType,
          value: type,
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              disabled: optionDisabled,
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Color'), " 1"),
        value: colors[0],
        onChange: function onChange(tempColor) {
          return onChangeColor([tempColor, colors[1]]);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Color'), " 2"),
        value: colors[1],
        onChange: function onChange(tempColor) {
          return onChangeColor([colors[0], tempColor]);
        }
      })
    })]
  });
};

MoneyChartPanel.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.string
};
var _default = MoneyChartPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL01vbmV5Q2hhcnRQYW5lbC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJTdHlsZWRJbnB1dEdyb3VwIiwiSW5wdXRHcm91cCIsIklucHV0Q29udGFpbmVyIiwiTGFiZWwiLCJzcGFuIiwiTW9uZXlDaGFydFBhbmVsIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJ2YWx1ZSIsInQiLCJjb2xvcnMiLCJ0eXBlIiwib3B0aW9ucyIsImxhYmVsIiwib25DaGFuZ2VDb2xvciIsInRlbXBDb2xvcnMiLCJvbkNoYW5nZVR5cGUiLCJlIiwidGFyZ2V0IiwibWFwIiwib3B0aW9uRGlzYWJsZWQiLCJkaXNhYmxlZCIsInRlbXBDb2xvciIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsd0hBQWY7O0FBS0EsSUFBTUMsZ0JBQWdCLEdBQUcsK0JBQU9DLDBCQUFQLENBQUgseVJBQXRCOztBQVlBLElBQU1DLGNBQWMsR0FBR0osMEJBQU9DLEdBQVYsMklBQXBCOztBQU1BLElBQU1JLEtBQUssR0FBR0wsMEJBQU9NLElBQVYsdU9BQVg7O0FBVUEsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixHQUE0QztBQUFBLGlGQUFQLEVBQU87QUFBQSxNQUF6Q0MsaUJBQXlDLFFBQXpDQSxpQkFBeUM7QUFBQSx3QkFBdEJDLEtBQXNCO0FBQUEsTUFBdEJBLEtBQXNCLDJCQUFkLEVBQWM7O0FBQ2hFLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxzQkFBb0RELEtBQXBELENBQVFFLE1BQVI7QUFBQSxNQUFRQSxNQUFSLDhCQUFpQixDQUFDLE1BQUQsRUFBUyxNQUFULENBQWpCO0FBQUEsb0JBQW9ERixLQUFwRCxDQUFtQ0csSUFBbkM7QUFBQSxNQUFtQ0EsSUFBbkMsNEJBQTBDLEtBQTFDO0FBQ0EsTUFBTUMsT0FBTyxHQUFHLENBQ1o7QUFBRUosSUFBQUEsS0FBSyxFQUFFLEtBQVQ7QUFBZ0JLLElBQUFBLEtBQUssRUFBRUosQ0FBQyxDQUFDLE1BQUQ7QUFBeEIsR0FEWSxFQUVaO0FBQUVELElBQUFBLEtBQUssRUFBRSxNQUFUO0FBQWlCSyxJQUFBQSxLQUFLLEVBQUVKLENBQUMsQ0FBQyxNQUFEO0FBQXpCLEdBRlksRUFHWjtBQUFFRCxJQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQkssSUFBQUEsS0FBSyxFQUFFSixDQUFDLENBQUMsTUFBRDtBQUF6QixHQUhZLENBQWhCOztBQU1BLE1BQU1LLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBQ0MsVUFBRDtBQUFBLFdBQ2xCUixpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUUsTUFBQUEsTUFBTSxFQUFFSztBQUFyQixPQURDO0FBQUEsR0FBdEI7O0FBR0EsTUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsQ0FBRDtBQUFBLFdBQ2pCVixpQkFBaUIsaUNBQU1DLEtBQU47QUFBYUcsTUFBQUEsSUFBSSxFQUFFTSxDQUFDLENBQUNDLE1BQUYsQ0FBU1Y7QUFBNUIsT0FEQTtBQUFBLEdBQXJCOztBQUdBLHNCQUNJLHNCQUFDLFNBQUQ7QUFBQSw0QkFDSSxzQkFBQyxjQUFEO0FBQUEsOEJBQ0kscUJBQUMsS0FBRDtBQUFBLGtCQUFRQyxDQUFDLENBQUMsTUFBRDtBQUFULFFBREosZUFFSSxxQkFBQyxnQkFBRDtBQUFBLCtCQUNJLHFCQUFDLG9CQUFELENBQU0sT0FBTjtBQUNJLFVBQUEsRUFBRSxFQUFDLFFBRFA7QUFFSSxVQUFBLEVBQUUsRUFBQyxjQUZQO0FBR0ksVUFBQSxRQUFRLEVBQUVPLFlBSGQ7QUFJSSxVQUFBLEtBQUssRUFBRUwsSUFKWDtBQUFBLG9CQU1LQyxPQUFPLENBQUNPLEdBQVIsQ0FDRztBQUFBLGdCQUFHTixLQUFILFNBQUdBLEtBQUg7QUFBQSxnQkFBVUwsS0FBVixTQUFVQSxLQUFWO0FBQUEsZ0JBQTJCWSxjQUEzQixTQUFpQkMsUUFBakI7QUFBQSxnQ0FDSTtBQUNJLGNBQUEsUUFBUSxFQUFFRCxjQURkO0FBR0ksY0FBQSxLQUFLLEVBQUVaLEtBSFg7QUFBQSx3QkFLS0s7QUFMTCxnQ0FFbUJMLEtBRm5CLGNBRTRCSyxLQUY1QixFQURKO0FBQUEsV0FESDtBQU5MO0FBREosUUFGSjtBQUFBLE1BREosZUF3QkkscUJBQUMsY0FBRDtBQUFBLDZCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxLQUFLLFlBQUtKLENBQUMsQ0FBQyxPQUFELENBQU4sT0FEVDtBQUVJLFFBQUEsS0FBSyxFQUFFQyxNQUFNLENBQUMsQ0FBRCxDQUZqQjtBQUdJLFFBQUEsUUFBUSxFQUFFLGtCQUFDWSxTQUFEO0FBQUEsaUJBQ05SLGFBQWEsQ0FBQyxDQUFDUSxTQUFELEVBQVlaLE1BQU0sQ0FBQyxDQUFELENBQWxCLENBQUQsQ0FEUDtBQUFBO0FBSGQ7QUFESixNQXhCSixlQWlDSSxxQkFBQyxjQUFEO0FBQUEsNkJBQ0kscUJBQUMseUJBQUQ7QUFDSSxRQUFBLEtBQUssWUFBS0QsQ0FBQyxDQUFDLE9BQUQsQ0FBTixPQURUO0FBRUksUUFBQSxLQUFLLEVBQUVDLE1BQU0sQ0FBQyxDQUFELENBRmpCO0FBR0ksUUFBQSxRQUFRLEVBQUUsa0JBQUNZLFNBQUQ7QUFBQSxpQkFDTlIsYUFBYSxDQUFDLENBQUNKLE1BQU0sQ0FBQyxDQUFELENBQVAsRUFBWVksU0FBWixDQUFELENBRFA7QUFBQTtBQUhkO0FBREosTUFqQ0o7QUFBQSxJQURKO0FBNkNILENBNUREOztBQThEQWhCLGVBQWUsQ0FBQ2lCLFNBQWhCLEdBQTRCO0FBQ3hCaEIsRUFBQUEsaUJBQWlCLEVBQUVpQixtQkFBVUMsSUFETDtBQUV4QmpCLEVBQUFBLEtBQUssRUFBRWdCLG1CQUFVRTtBQUZPLENBQTVCO2VBS2VwQixlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IEZvcm0sIElucHV0R3JvdXAgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgQ29sb3JQaWNrZXJJbnB1dCBmcm9tICdAY29tcG9uZW50cy9Db2xvclBpY2tlcklucHV0JztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5gO1xuXG5jb25zdCBTdHlsZWRJbnB1dEdyb3VwID0gc3R5bGVkKElucHV0R3JvdXApYFxuICAgIGhlaWdodDogNDZweDtcbiAgICB3aWR0aDogMTMzcHg7XG4gICAgJiBzZWxlY3Qge1xuICAgICAgICBoZWlnaHQ6IDQ2cHg7XG4gICAgICAgIHdpZHRoOiAxMzNweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2VjZWRmMDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIH1cbmA7XG5cbmNvbnN0IElucHV0Q29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luOiA0cHggMDtcbmA7XG5cbmNvbnN0IExhYmVsID0gc3R5bGVkLnNwYW5gXG4gICAgbWluLWhlaWdodDogMTZweDtcbiAgICB3aWR0aDogMTM1cHg7XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIG1hcmdpbi1yaWdodDogMjNweDtcbmA7XG5cbmNvbnN0IE1vbmV5Q2hhcnRQYW5lbCA9ICh7IGhhbmRsZVZhbHVlQ2hhbmdlLCB2YWx1ZSA9IHt9IH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCB7IGNvbG9ycyA9IFsnIzAwMCcsICcjMDAwJ10sIHR5cGUgPSAnYmFyJyB9ID0gdmFsdWU7XG4gICAgY29uc3Qgb3B0aW9ucyA9IFtcbiAgICAgICAgeyB2YWx1ZTogJ2JhcicsIGxhYmVsOiB0KCdCYXJzJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2xpbmUnLCBsYWJlbDogdCgnTGluZScpIH0sXG4gICAgICAgIHsgdmFsdWU6ICdhcmVhJywgbGFiZWw6IHQoJ0FyZWEnKSB9LFxuICAgIF07XG5cbiAgICBjb25zdCBvbkNoYW5nZUNvbG9yID0gKHRlbXBDb2xvcnMpID0+XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHsgLi4udmFsdWUsIGNvbG9yczogdGVtcENvbG9ycyB9KTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlVHlwZSA9IChlKSA9PlxuICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZSh7IC4uLnZhbHVlLCB0eXBlOiBlLnRhcmdldC52YWx1ZSB9KTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdUeXBlJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtdGhlbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlVHlwZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0eXBlfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7b3B0aW9ucy5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHsgbGFiZWwsIHZhbHVlLCBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BzZWxlY3QtJHt2YWx1ZX0tJHtsYWJlbH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7bGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e2Ake3QoJ0NvbG9yJyl9IDFgfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Y29sb3JzWzBdfVxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlQ29sb3IoW3RlbXBDb2xvciwgY29sb3JzWzFdXSlcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXtgJHt0KCdDb2xvcicpfSAyYH1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2NvbG9yc1sxXX1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUNvbG9yKFtjb2xvcnNbMF0sIHRlbXBDb2xvcl0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cbk1vbmV5Q2hhcnRQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlVmFsdWVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgTW9uZXlDaGFydFBhbmVsO1xuIl19