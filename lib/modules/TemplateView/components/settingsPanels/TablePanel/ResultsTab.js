"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styled = require("../../../../../components/styled");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ResultsTable = _styledComponents.default.table(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    th {\n        color: #4f51a1;\n        font-size: 13px;\n        padding: 0 2px;\n\n        &.name-column {\n            width: 145px;\n        }\n\n        &.ref-column {\n            width: 136px;\n        }\n\n        &.visible-column {\n            text-align: center;\n        }\n    }\n\n    td {\n        font-size: 12px;\n        padding: 4px 2px;\n\n        &.visible-column {\n            text-align: center;\n        }\n    }\n"])));

var orderSort = function orderSort(a, b) {
  if (a.order > b.order) {
    return 1;
  }

  if (b.order > a.order) {
    return -1;
  }

  return 0;
};

var ResultsTab = function ResultsTab() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref.handleValueChange,
      value = _ref.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var preparedCols = Object.keys(value.cols).map(function (key) {
    return _objectSpread(_objectSpread({}, value.cols[key]), {}, {
      id: key
    });
  }).sort(orderSort);
  var _value$results = value.results,
      results = _value$results === void 0 ? {} : _value$results;

  var handleChangeFooterColumn = function handleChangeFooterColumn(key, elementKey, toAttr, newValue) {
    var tempResults = _objectSpread({}, value.results);

    tempResults[key].columns[elementKey][toAttr] = newValue;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      results: tempResults
    }));
  };

  var handleChangeVisibleFooterRow = function handleChangeVisibleFooterRow(key, visible) {
    var tempResults = _objectSpread({}, value.results);

    tempResults[key].visible = visible;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      results: tempResults
    }));
  };

  var renderFooterElement = function renderFooterElement(key, elementKey, refsUsed) {
    var element = results[key].columns[elementKey];
    var id = "check-".concat(key, "-").concat(elementKey);
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)("tr", {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("td", {
        children: element.label
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("td", {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          as: "select",
          value: element.ref,
          onChange: function onChange(e) {
            return handleChangeFooterColumn(key, elementKey, 'ref', e.target.value);
          },
          children: preparedCols.filter(function (col, index) {
            return index !== 0 && (col.id === element.ref || (0, _isNil.default)(refsUsed[col.id]));
          }).map(function (col) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: col.id,
              children: col.label
            }, col.id);
          })
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("td", {
        className: "visible-column",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: id,
          onChange: function onChange(e) {
            return handleChangeFooterColumn(key, elementKey, 'visible', e.target.checked);
          },
          checked: element.visible
        })
      })]
    }, id);
  };

  var renderResult = function renderResult(key) {
    var refsUsed = Object.keys(results[key].columns).reduce(function (acc, currentKey) {
      acc[results[key].columns[currentKey].ref] = true;
      return acc;
    }, {});
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: results[key].label
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(ResultsTable, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("thead", {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)("tr", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("th", {
              className: "name-column",
              children: t('Name')
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("th", {
              className: "ref-column",
              children: t('Column')
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("th", {
              className: "visible-column",
              children: t('Watch')
            })]
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)("tbody", {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)("tr", {
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("td", {
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)("strong", {
                children: t('Whole row')
              })
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)("td", {}), /*#__PURE__*/(0, _jsxRuntime.jsx)("td", {
              className: "visible-column",
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
                id: "headerFontBold",
                onChange: function onChange(e) {
                  return handleChangeVisibleFooterRow(key, e.target.checked);
                },
                checked: results[key].visible
              })
            })]
          }), results[key].visible && Object.keys(results[key].columns).map(function (elementKey) {
            return renderFooterElement(key, elementKey, refsUsed);
          })]
        })]
      })]
    }, "row-data-".concat(key));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexColumn, {
    children: Object.keys(results).map(function (key) {
      return renderResult(key);
    })
  });
};

ResultsTab.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.object
};
var _default = ResultsTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvUmVzdWx0c1RhYi5qcyJdLCJuYW1lcyI6WyJSZXN1bHRzVGFibGUiLCJzdHlsZWQiLCJ0YWJsZSIsIm9yZGVyU29ydCIsImEiLCJiIiwib3JkZXIiLCJSZXN1bHRzVGFiIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJ2YWx1ZSIsInQiLCJwcmVwYXJlZENvbHMiLCJPYmplY3QiLCJrZXlzIiwiY29scyIsIm1hcCIsImtleSIsImlkIiwic29ydCIsInJlc3VsdHMiLCJoYW5kbGVDaGFuZ2VGb290ZXJDb2x1bW4iLCJlbGVtZW50S2V5IiwidG9BdHRyIiwibmV3VmFsdWUiLCJ0ZW1wUmVzdWx0cyIsImNvbHVtbnMiLCJoYW5kbGVDaGFuZ2VWaXNpYmxlRm9vdGVyUm93IiwidmlzaWJsZSIsInJlbmRlckZvb3RlckVsZW1lbnQiLCJyZWZzVXNlZCIsImVsZW1lbnQiLCJsYWJlbCIsInJlZiIsImUiLCJ0YXJnZXQiLCJmaWx0ZXIiLCJjb2wiLCJpbmRleCIsImNoZWNrZWQiLCJyZW5kZXJSZXN1bHQiLCJyZWR1Y2UiLCJhY2MiLCJjdXJyZW50S2V5IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHQywwQkFBT0MsS0FBVixpZ0JBQWxCOztBQTZCQSxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUN4QixNQUFJRCxDQUFDLENBQUNFLEtBQUYsR0FBVUQsQ0FBQyxDQUFDQyxLQUFoQixFQUF1QjtBQUNuQixXQUFPLENBQVA7QUFDSDs7QUFDRCxNQUFJRCxDQUFDLENBQUNDLEtBQUYsR0FBVUYsQ0FBQyxDQUFDRSxLQUFoQixFQUF1QjtBQUNuQixXQUFPLENBQUMsQ0FBUjtBQUNIOztBQUNELFNBQU8sQ0FBUDtBQUNILENBUkQ7O0FBVUEsSUFBTUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBdUM7QUFBQSxpRkFBUCxFQUFPO0FBQUEsTUFBcENDLGlCQUFvQyxRQUFwQ0EsaUJBQW9DO0FBQUEsTUFBakJDLEtBQWlCLFFBQWpCQSxLQUFpQjs7QUFDdEQsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLFlBQVksR0FBR0MsTUFBTSxDQUFDQyxJQUFQLENBQVlKLEtBQUssQ0FBQ0ssSUFBbEIsRUFDaEJDLEdBRGdCLENBQ1osVUFBQ0MsR0FBRDtBQUFBLDJDQUFlUCxLQUFLLENBQUNLLElBQU4sQ0FBV0UsR0FBWCxDQUFmO0FBQWdDQyxNQUFBQSxFQUFFLEVBQUVEO0FBQXBDO0FBQUEsR0FEWSxFQUVoQkUsSUFGZ0IsQ0FFWGYsU0FGVyxDQUFyQjtBQUlBLHVCQUF5Qk0sS0FBekIsQ0FBUVUsT0FBUjtBQUFBLE1BQVFBLE9BQVIsK0JBQWtCLEVBQWxCOztBQUVBLE1BQU1DLHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsQ0FBQ0osR0FBRCxFQUFNSyxVQUFOLEVBQWtCQyxNQUFsQixFQUEwQkMsUUFBMUIsRUFBdUM7QUFDcEUsUUFBSUMsV0FBVyxxQkFBUWYsS0FBSyxDQUFDVSxPQUFkLENBQWY7O0FBQ0FLLElBQUFBLFdBQVcsQ0FBQ1IsR0FBRCxDQUFYLENBQWlCUyxPQUFqQixDQUF5QkosVUFBekIsRUFBcUNDLE1BQXJDLElBQStDQyxRQUEvQztBQUNBZixJQUFBQSxpQkFBaUIsaUNBQ1ZDLEtBRFU7QUFFYlUsTUFBQUEsT0FBTyxFQUFFSztBQUZJLE9BQWpCO0FBSUgsR0FQRDs7QUFTQSxNQUFNRSw0QkFBNEIsR0FBRyxTQUEvQkEsNEJBQStCLENBQUNWLEdBQUQsRUFBTVcsT0FBTixFQUFrQjtBQUNuRCxRQUFJSCxXQUFXLHFCQUFRZixLQUFLLENBQUNVLE9BQWQsQ0FBZjs7QUFDQUssSUFBQUEsV0FBVyxDQUFDUixHQUFELENBQVgsQ0FBaUJXLE9BQWpCLEdBQTJCQSxPQUEzQjtBQUNBbkIsSUFBQUEsaUJBQWlCLGlDQUNWQyxLQURVO0FBRWJVLE1BQUFBLE9BQU8sRUFBRUs7QUFGSSxPQUFqQjtBQUlILEdBUEQ7O0FBU0EsTUFBTUksbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDWixHQUFELEVBQU1LLFVBQU4sRUFBa0JRLFFBQWxCLEVBQStCO0FBQ3ZELFFBQU1DLE9BQU8sR0FBR1gsT0FBTyxDQUFDSCxHQUFELENBQVAsQ0FBYVMsT0FBYixDQUFxQkosVUFBckIsQ0FBaEI7QUFDQSxRQUFNSixFQUFFLG1CQUFZRCxHQUFaLGNBQW1CSyxVQUFuQixDQUFSO0FBRUEsd0JBQ0k7QUFBQSw4QkFDSTtBQUFBLGtCQUFLUyxPQUFPLENBQUNDO0FBQWIsUUFESixlQUVJO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksVUFBQSxLQUFLLEVBQUVELE9BQU8sQ0FBQ0UsR0FGbkI7QUFHSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ0MsQ0FBRDtBQUFBLG1CQUNOYix3QkFBd0IsQ0FDcEJKLEdBRG9CLEVBRXBCSyxVQUZvQixFQUdwQixLQUhvQixFQUlwQlksQ0FBQyxDQUFDQyxNQUFGLENBQVN6QixLQUpXLENBRGxCO0FBQUEsV0FIZDtBQUFBLG9CQVlLRSxZQUFZLENBQ1J3QixNQURKLENBRU8sVUFBQ0MsR0FBRCxFQUFNQyxLQUFOO0FBQUEsbUJBQ0lBLEtBQUssS0FBSyxDQUFWLEtBQ0NELEdBQUcsQ0FBQ25CLEVBQUosS0FBV2EsT0FBTyxDQUFDRSxHQUFuQixJQUNHLG9CQUFNSCxRQUFRLENBQUNPLEdBQUcsQ0FBQ25CLEVBQUwsQ0FBZCxDQUZKLENBREo7QUFBQSxXQUZQLEVBT0lGLEdBUEosQ0FPUSxVQUFDcUIsR0FBRDtBQUFBLGdDQUNEO0FBQXFCLGNBQUEsS0FBSyxFQUFFQSxHQUFHLENBQUNuQixFQUFoQztBQUFBLHdCQUNLbUIsR0FBRyxDQUFDTDtBQURULGVBQWFLLEdBQUcsQ0FBQ25CLEVBQWpCLENBREM7QUFBQSxXQVBSO0FBWkw7QUFESixRQUZKLGVBNkJJO0FBQUksUUFBQSxTQUFTLEVBQUMsZ0JBQWQ7QUFBQSwrQkFDSSxxQkFBQyxxQkFBRDtBQUNJLFVBQUEsRUFBRSxFQUFFQSxFQURSO0FBRUksVUFBQSxRQUFRLEVBQUUsa0JBQUNnQixDQUFEO0FBQUEsbUJBQ05iLHdCQUF3QixDQUNwQkosR0FEb0IsRUFFcEJLLFVBRm9CLEVBR3BCLFNBSG9CLEVBSXBCWSxDQUFDLENBQUNDLE1BQUYsQ0FBU0ksT0FKVyxDQURsQjtBQUFBLFdBRmQ7QUFVSSxVQUFBLE9BQU8sRUFBRVIsT0FBTyxDQUFDSDtBQVZyQjtBQURKLFFBN0JKO0FBQUEsT0FBU1YsRUFBVCxDQURKO0FBOENILEdBbEREOztBQW1EQSxNQUFNc0IsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ3ZCLEdBQUQsRUFBUztBQUMxQixRQUFNYSxRQUFRLEdBQUdqQixNQUFNLENBQUNDLElBQVAsQ0FBWU0sT0FBTyxDQUFDSCxHQUFELENBQVAsQ0FBYVMsT0FBekIsRUFBa0NlLE1BQWxDLENBQ2IsVUFBQ0MsR0FBRCxFQUFNQyxVQUFOLEVBQXFCO0FBQ2pCRCxNQUFBQSxHQUFHLENBQUN0QixPQUFPLENBQUNILEdBQUQsQ0FBUCxDQUFhUyxPQUFiLENBQXFCaUIsVUFBckIsRUFBaUNWLEdBQWxDLENBQUgsR0FBNEMsSUFBNUM7QUFDQSxhQUFPUyxHQUFQO0FBQ0gsS0FKWSxFQUtiLEVBTGEsQ0FBakI7QUFPQSx3QkFDSSxzQkFBQyxrQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGVBQUQ7QUFBQSxrQkFBVXRCLE9BQU8sQ0FBQ0gsR0FBRCxDQUFQLENBQWFlO0FBQXZCLFFBREosZUFFSSxzQkFBQyxZQUFEO0FBQUEsZ0NBQ0k7QUFBQSxpQ0FDSTtBQUFBLG9DQUNJO0FBQUksY0FBQSxTQUFTLEVBQUMsYUFBZDtBQUFBLHdCQUE2QnJCLENBQUMsQ0FBQyxNQUFEO0FBQTlCLGNBREosZUFFSTtBQUFJLGNBQUEsU0FBUyxFQUFDLFlBQWQ7QUFBQSx3QkFBNEJBLENBQUMsQ0FBQyxRQUFEO0FBQTdCLGNBRkosZUFHSTtBQUFJLGNBQUEsU0FBUyxFQUFDLGdCQUFkO0FBQUEsd0JBQWdDQSxDQUFDLENBQUMsT0FBRDtBQUFqQyxjQUhKO0FBQUE7QUFESixVQURKLGVBUUk7QUFBQSxrQ0FDSTtBQUFBLG9DQUNJO0FBQUEscUNBQ0k7QUFBQSwwQkFBU0EsQ0FBQyxDQUFDLFdBQUQ7QUFBVjtBQURKLGNBREosZUFJSSw4QkFKSixlQUtJO0FBQUksY0FBQSxTQUFTLEVBQUMsZ0JBQWQ7QUFBQSxxQ0FDSSxxQkFBQyxxQkFBRDtBQUNJLGdCQUFBLEVBQUUsRUFBQyxnQkFEUDtBQUVJLGdCQUFBLFFBQVEsRUFBRSxrQkFBQ3VCLENBQUQ7QUFBQSx5QkFDTlAsNEJBQTRCLENBQ3hCVixHQUR3QixFQUV4QmlCLENBQUMsQ0FBQ0MsTUFBRixDQUFTSSxPQUZlLENBRHRCO0FBQUEsaUJBRmQ7QUFRSSxnQkFBQSxPQUFPLEVBQUVuQixPQUFPLENBQUNILEdBQUQsQ0FBUCxDQUFhVztBQVIxQjtBQURKLGNBTEo7QUFBQSxZQURKLEVBbUJLUixPQUFPLENBQUNILEdBQUQsQ0FBUCxDQUFhVyxPQUFiLElBQ0dmLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZTSxPQUFPLENBQUNILEdBQUQsQ0FBUCxDQUFhUyxPQUF6QixFQUFrQ1YsR0FBbEMsQ0FDSSxVQUFDTSxVQUFEO0FBQUEsbUJBQ0lPLG1CQUFtQixDQUNmWixHQURlLEVBRWZLLFVBRmUsRUFHZlEsUUFIZSxDQUR2QjtBQUFBLFdBREosQ0FwQlI7QUFBQSxVQVJKO0FBQUEsUUFGSjtBQUFBLDBCQUE2QmIsR0FBN0IsRUFESjtBQTJDSCxHQW5ERDs7QUFxREEsc0JBQ0kscUJBQUMsa0JBQUQ7QUFBQSxjQUNLSixNQUFNLENBQUNDLElBQVAsQ0FBWU0sT0FBWixFQUFxQkosR0FBckIsQ0FBeUIsVUFBQ0MsR0FBRDtBQUFBLGFBQVN1QixZQUFZLENBQUN2QixHQUFELENBQXJCO0FBQUEsS0FBekI7QUFETCxJQURKO0FBS0gsQ0F2SUQ7O0FBeUlBVCxVQUFVLENBQUNvQyxTQUFYLEdBQXVCO0FBQ25CbkMsRUFBQUEsaUJBQWlCLEVBQUVvQyxtQkFBVUMsSUFEVjtBQUVuQnBDLEVBQUFBLEtBQUssRUFBRW1DLG1CQUFVRTtBQUZFLENBQXZCO2VBS2V2QyxVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgeyBGbGV4Q29sdW1uIH0gZnJvbSAnQGNvbXBvbmVudHMvc3R5bGVkJztcblxuaW1wb3J0IHsgQ2hlY2tib3hJbnB1dCwgU3VibWVudSB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmNvbnN0IFJlc3VsdHNUYWJsZSA9IHN0eWxlZC50YWJsZWBcbiAgICB0aCB7XG4gICAgICAgIGNvbG9yOiAjNGY1MWExO1xuICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgMnB4O1xuXG4gICAgICAgICYubmFtZS1jb2x1bW4ge1xuICAgICAgICAgICAgd2lkdGg6IDE0NXB4O1xuICAgICAgICB9XG5cbiAgICAgICAgJi5yZWYtY29sdW1uIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMzZweDtcbiAgICAgICAgfVxuXG4gICAgICAgICYudmlzaWJsZS1jb2x1bW4ge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgdGQge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIHBhZGRpbmc6IDRweCAycHg7XG5cbiAgICAgICAgJi52aXNpYmxlLWNvbHVtbiB7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICB9XG5gO1xuXG5jb25zdCBvcmRlclNvcnQgPSAoYSwgYikgPT4ge1xuICAgIGlmIChhLm9yZGVyID4gYi5vcmRlcikge1xuICAgICAgICByZXR1cm4gMTtcbiAgICB9XG4gICAgaWYgKGIub3JkZXIgPiBhLm9yZGVyKSB7XG4gICAgICAgIHJldHVybiAtMTtcbiAgICB9XG4gICAgcmV0dXJuIDA7XG59O1xuXG5jb25zdCBSZXN1bHRzVGFiID0gKHsgaGFuZGxlVmFsdWVDaGFuZ2UsIHZhbHVlIH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBwcmVwYXJlZENvbHMgPSBPYmplY3Qua2V5cyh2YWx1ZS5jb2xzKVxuICAgICAgICAubWFwKChrZXkpID0+ICh7IC4uLnZhbHVlLmNvbHNba2V5XSwgaWQ6IGtleSB9KSlcbiAgICAgICAgLnNvcnQob3JkZXJTb3J0KTtcblxuICAgIGNvbnN0IHsgcmVzdWx0cyA9IHt9IH0gPSB2YWx1ZTtcblxuICAgIGNvbnN0IGhhbmRsZUNoYW5nZUZvb3RlckNvbHVtbiA9IChrZXksIGVsZW1lbnRLZXksIHRvQXR0ciwgbmV3VmFsdWUpID0+IHtcbiAgICAgICAgbGV0IHRlbXBSZXN1bHRzID0geyAuLi52YWx1ZS5yZXN1bHRzIH07XG4gICAgICAgIHRlbXBSZXN1bHRzW2tleV0uY29sdW1uc1tlbGVtZW50S2V5XVt0b0F0dHJdID0gbmV3VmFsdWU7XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHtcbiAgICAgICAgICAgIC4uLnZhbHVlLFxuICAgICAgICAgICAgcmVzdWx0czogdGVtcFJlc3VsdHMsXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVDaGFuZ2VWaXNpYmxlRm9vdGVyUm93ID0gKGtleSwgdmlzaWJsZSkgPT4ge1xuICAgICAgICBsZXQgdGVtcFJlc3VsdHMgPSB7IC4uLnZhbHVlLnJlc3VsdHMgfTtcbiAgICAgICAgdGVtcFJlc3VsdHNba2V5XS52aXNpYmxlID0gdmlzaWJsZTtcbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2Uoe1xuICAgICAgICAgICAgLi4udmFsdWUsXG4gICAgICAgICAgICByZXN1bHRzOiB0ZW1wUmVzdWx0cyxcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIGNvbnN0IHJlbmRlckZvb3RlckVsZW1lbnQgPSAoa2V5LCBlbGVtZW50S2V5LCByZWZzVXNlZCkgPT4ge1xuICAgICAgICBjb25zdCBlbGVtZW50ID0gcmVzdWx0c1trZXldLmNvbHVtbnNbZWxlbWVudEtleV07XG4gICAgICAgIGNvbnN0IGlkID0gYGNoZWNrLSR7a2V5fS0ke2VsZW1lbnRLZXl9YDtcblxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPHRyIGtleT17aWR9PlxuICAgICAgICAgICAgICAgIDx0ZD57ZWxlbWVudC5sYWJlbH08L3RkPlxuICAgICAgICAgICAgICAgIDx0ZD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2VsZW1lbnQucmVmfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNoYW5nZUZvb3RlckNvbHVtbihcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50S2V5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAncmVmJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtwcmVwYXJlZENvbHNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY29sLCBpbmRleCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4ICE9PSAwICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY29sLmlkID09PSBlbGVtZW50LnJlZiB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzTmlsKHJlZnNVc2VkW2NvbC5pZF0pKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAubWFwKChjb2wpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiBrZXk9e2NvbC5pZH0gdmFsdWU9e2NvbC5pZH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Y29sLmxhYmVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgICAgICAgICA8dGQgY2xhc3NOYW1lPVwidmlzaWJsZS1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPXtpZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VGb290ZXJDb2x1bW4oXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudEtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3Zpc2libGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17ZWxlbWVudC52aXNpYmxlfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvdGQ+XG4gICAgICAgICAgICA8L3RyPlxuICAgICAgICApO1xuICAgIH07XG4gICAgY29uc3QgcmVuZGVyUmVzdWx0ID0gKGtleSkgPT4ge1xuICAgICAgICBjb25zdCByZWZzVXNlZCA9IE9iamVjdC5rZXlzKHJlc3VsdHNba2V5XS5jb2x1bW5zKS5yZWR1Y2UoXG4gICAgICAgICAgICAoYWNjLCBjdXJyZW50S2V5KSA9PiB7XG4gICAgICAgICAgICAgICAgYWNjW3Jlc3VsdHNba2V5XS5jb2x1bW5zW2N1cnJlbnRLZXldLnJlZl0gPSB0cnVlO1xuICAgICAgICAgICAgICAgIHJldHVybiBhY2M7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge31cbiAgICAgICAgKTtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxGbGV4Q29sdW1uIGtleT17YHJvdy1kYXRhLSR7a2V5fWB9PlxuICAgICAgICAgICAgICAgIDxTdWJtZW51PntyZXN1bHRzW2tleV0ubGFiZWx9PC9TdWJtZW51PlxuICAgICAgICAgICAgICAgIDxSZXN1bHRzVGFibGU+XG4gICAgICAgICAgICAgICAgICAgIDx0aGVhZD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3NOYW1lPVwibmFtZS1jb2x1bW5cIj57dCgnTmFtZScpfTwvdGg+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRoIGNsYXNzTmFtZT1cInJlZi1jb2x1bW5cIj57dCgnQ29sdW1uJyl9PC90aD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGggY2xhc3NOYW1lPVwidmlzaWJsZS1jb2x1bW5cIj57dCgnV2F0Y2gnKX08L3RoPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgPC90aGVhZD5cbiAgICAgICAgICAgICAgICAgICAgPHRib2R5PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHN0cm9uZz57dCgnV2hvbGUgcm93Jyl9PC9zdHJvbmc+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQ+PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dGQgY2xhc3NOYW1lPVwidmlzaWJsZS1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwiaGVhZGVyRm9udEJvbGRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNoYW5nZVZpc2libGVGb290ZXJSb3coXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3Jlc3VsdHNba2V5XS52aXNpYmxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICAgICAge3Jlc3VsdHNba2V5XS52aXNpYmxlICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMocmVzdWx0c1trZXldLmNvbHVtbnMpLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKGVsZW1lbnRLZXkpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZW5kZXJGb290ZXJFbGVtZW50KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50S2V5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZnNVc2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICAgICAgICA8L1Jlc3VsdHNUYWJsZT5cbiAgICAgICAgICAgIDwvRmxleENvbHVtbj5cbiAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICB7T2JqZWN0LmtleXMocmVzdWx0cykubWFwKChrZXkpID0+IHJlbmRlclJlc3VsdChrZXkpKX1cbiAgICAgICAgPC9GbGV4Q29sdW1uPlxuICAgICk7XG59O1xuXG5SZXN1bHRzVGFiLnByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBSZXN1bHRzVGFiO1xuIl19