"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _ProposalSelector = _interopRequireDefault(require("../ProposalSelector"));

var _Styles = require("../Styles");

var _ColumnsTab = _interopRequireDefault(require("./ColumnsTab"));

var _ResultsTab = _interopRequireDefault(require("./ResultsTab"));

var _StylesTab = _interopRequireDefault(require("./StylesTab"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TablePanel = function TablePanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      handleStyleChange = _ref.handleStyleChange,
      handleValueChange = _ref.handleValueChange,
      hasProposalSelector = _ref.hasProposalSelector,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      value = _ref.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_ProposalSelector.default, {
      catalogs: catalogs,
      disabled: proposalSelectorDisabled,
      handleValueChange: handleValueChange,
      value: value,
      visible: hasProposalSelector
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.StyledTabs, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Properties",
        title: t('Properties'),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_StylesTab.default, {
          handleStyleChange: handleStyleChange,
          style: style
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Columns",
        title: t('Column', {
          count: 2
        }),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColumnsTab.default, {
          handleValueChange: handleValueChange,
          value: value
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Tab, {
        className: "pr-3 pl-3",
        eventKey: "Footer",
        title: t('Footer'),
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ResultsTab.default, {
          handleValueChange: handleValueChange,
          value: value
        })
      })]
    })]
  });
};

TablePanel.propTypes = {
  catalogs: _propTypes.default.array,
  handleStyleChange: _propTypes.default.func,
  handleValueChange: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  proposalSelectorDisabled: _propTypes.default.string,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = TablePanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvaW5kZXguanMiXSwibmFtZXMiOlsiVGFibGVQYW5lbCIsImNhdGFsb2dzIiwiaGFuZGxlU3R5bGVDaGFuZ2UiLCJoYW5kbGVWYWx1ZUNoYW5nZSIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWQiLCJzdHlsZSIsInZhbHVlIiwidCIsImNvdW50IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJmdW5jIiwiYm9vbCIsInN0cmluZyIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7QUFFQSxJQUFNQSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQVFSO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BUFBDLFFBT08sUUFQUEEsUUFPTztBQUFBLE1BTlBDLGlCQU1PLFFBTlBBLGlCQU1PO0FBQUEsTUFMUEMsaUJBS08sUUFMUEEsaUJBS087QUFBQSxNQUpQQyxtQkFJTyxRQUpQQSxtQkFJTztBQUFBLE1BSFBDLHdCQUdPLFFBSFBBLHdCQUdPO0FBQUEsd0JBRlBDLEtBRU87QUFBQSxNQUZQQSxLQUVPLDJCQUZDLEVBRUQ7QUFBQSxNQURQQyxLQUNPLFFBRFBBLEtBQ087O0FBQ1Asd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUNJO0FBQUEsNEJBQ0kscUJBQUMseUJBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRVAsUUFEZDtBQUVJLE1BQUEsUUFBUSxFQUFFSSx3QkFGZDtBQUdJLE1BQUEsaUJBQWlCLEVBQUVGLGlCQUh2QjtBQUlJLE1BQUEsS0FBSyxFQUFFSSxLQUpYO0FBS0ksTUFBQSxPQUFPLEVBQUVIO0FBTGIsTUFESixlQVFJLHNCQUFDLGtCQUFEO0FBQUEsOEJBQ0kscUJBQUMsbUJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxXQURkO0FBRUksUUFBQSxRQUFRLEVBQUMsWUFGYjtBQUdJLFFBQUEsS0FBSyxFQUFFSSxDQUFDLENBQUMsWUFBRCxDQUhaO0FBQUEsK0JBS0kscUJBQUMsa0JBQUQ7QUFDSSxVQUFBLGlCQUFpQixFQUFFTixpQkFEdkI7QUFFSSxVQUFBLEtBQUssRUFBRUk7QUFGWDtBQUxKLFFBREosZUFXSSxxQkFBQyxtQkFBRDtBQUNJLFFBQUEsU0FBUyxFQUFDLFdBRGQ7QUFFSSxRQUFBLFFBQVEsRUFBQyxTQUZiO0FBR0ksUUFBQSxLQUFLLEVBQUVFLENBQUMsQ0FBQyxRQUFELEVBQVc7QUFBRUMsVUFBQUEsS0FBSyxFQUFFO0FBQVQsU0FBWCxDQUhaO0FBQUEsK0JBS0kscUJBQUMsbUJBQUQ7QUFDSSxVQUFBLGlCQUFpQixFQUFFTixpQkFEdkI7QUFFSSxVQUFBLEtBQUssRUFBRUk7QUFGWDtBQUxKLFFBWEosZUFxQkkscUJBQUMsbUJBQUQ7QUFDSSxRQUFBLFNBQVMsRUFBQyxXQURkO0FBRUksUUFBQSxRQUFRLEVBQUMsUUFGYjtBQUdJLFFBQUEsS0FBSyxFQUFFQyxDQUFDLENBQUMsUUFBRCxDQUhaO0FBQUEsK0JBS0kscUJBQUMsbUJBQUQ7QUFDSSxVQUFBLGlCQUFpQixFQUFFTCxpQkFEdkI7QUFFSSxVQUFBLEtBQUssRUFBRUk7QUFGWDtBQUxKLFFBckJKO0FBQUEsTUFSSjtBQUFBLElBREo7QUEyQ0gsQ0FyREQ7O0FBdURBUCxVQUFVLENBQUNVLFNBQVgsR0FBdUI7QUFDbkJULEVBQUFBLFFBQVEsRUFBRVUsbUJBQVVDLEtBREQ7QUFFbkJWLEVBQUFBLGlCQUFpQixFQUFFUyxtQkFBVUUsSUFGVjtBQUduQlYsRUFBQUEsaUJBQWlCLEVBQUVRLG1CQUFVRSxJQUhWO0FBSW5CVCxFQUFBQSxtQkFBbUIsRUFBRU8sbUJBQVVHLElBSlo7QUFLbkJULEVBQUFBLHdCQUF3QixFQUFFTSxtQkFBVUksTUFMakI7QUFNbkJULEVBQUFBLEtBQUssRUFBRUssbUJBQVVLLE1BTkU7QUFPbkJULEVBQUFBLEtBQUssRUFBRUksbUJBQVVLO0FBUEUsQ0FBdkI7ZUFVZWhCLFUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgVGFiIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5cbmltcG9ydCBQcm9wb3NhbFNlbGVjdG9yIGZyb20gJy4uL1Byb3Bvc2FsU2VsZWN0b3InO1xuaW1wb3J0IHsgU3R5bGVkVGFicyB9IGZyb20gJy4uL1N0eWxlcyc7XG5cbmltcG9ydCBDb2x1bW5zVGFiIGZyb20gJy4vQ29sdW1uc1RhYic7XG5pbXBvcnQgUmVzdWx0c1RhYiBmcm9tICcuL1Jlc3VsdHNUYWInO1xuaW1wb3J0IFN0eWxlc1RhYiBmcm9tICcuL1N0eWxlc1RhYic7XG5cbmNvbnN0IFRhYmxlUGFuZWwgPSAoe1xuICAgIGNhdGFsb2dzLFxuICAgIGhhbmRsZVN0eWxlQ2hhbmdlLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3IsXG4gICAgcHJvcG9zYWxTZWxlY3RvckRpc2FibGVkLFxuICAgIHN0eWxlID0ge30sXG4gICAgdmFsdWUsXG59ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPD5cbiAgICAgICAgICAgIDxQcm9wb3NhbFNlbGVjdG9yXG4gICAgICAgICAgICAgICAgY2F0YWxvZ3M9e2NhdGFsb2dzfVxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXtwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2U9e2hhbmRsZVZhbHVlQ2hhbmdlfVxuICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICB2aXNpYmxlPXtoYXNQcm9wb3NhbFNlbGVjdG9yfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxTdHlsZWRUYWJzPlxuICAgICAgICAgICAgICAgIDxUYWJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHItMyBwbC0zXCJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnRLZXk9XCJQcm9wZXJ0aWVzXCJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3QoJ1Byb3BlcnRpZXMnKX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxTdHlsZXNUYWJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlPXtoYW5kbGVTdHlsZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgICAgICAgICA8VGFiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByLTMgcGwtM1wiXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50S2V5PVwiQ29sdW1uc1wiXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlPXt0KCdDb2x1bW4nLCB7IGNvdW50OiAyIH0pfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPENvbHVtbnNUYWJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlPXtoYW5kbGVWYWx1ZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgICAgICAgICA8VGFiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByLTMgcGwtM1wiXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50S2V5PVwiRm9vdGVyXCJcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU9e3QoJ0Zvb3RlcicpfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPFJlc3VsdHNUYWJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlPXtoYW5kbGVWYWx1ZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1RhYj5cbiAgICAgICAgICAgIDwvU3R5bGVkVGFicz5cbiAgICAgICAgPC8+XG4gICAgKTtcbn07XG5cblRhYmxlUGFuZWwucHJvcFR5cGVzID0ge1xuICAgIGNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgaGFuZGxlU3R5bGVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZVZhbHVlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBUYWJsZVBhbmVsO1xuIl19