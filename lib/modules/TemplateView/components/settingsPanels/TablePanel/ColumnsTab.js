"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _arrayMove = _interopRequireDefault(require("array-move"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styled = require("../../../../../components/styled");

var _Styles = require("../Styles");

var _SortableList = _interopRequireDefault(require("./SortableList"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = (0, _styledComponents.default)(_styled.FlexColumn)(_templateObject || (_templateObject = _taggedTemplateLiteral([""])));
var Header = (0, _styledComponents.default)(_styled.FlexRow)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    .name-column {\n        flex-basis: 0;\n        flex-grow: 1;\n        padding: 0 4px;\n    }\n\n    .text-column {\n        width: 110px;\n        padding: 0 4px;\n    }\n\n    .actions-column {\n        width: 50px;\n        padding: 0 4px;\n    }\n    span {\n        color: #4f51a1;\n        font-size: 13px;\n    }\n"])));

var getNextID = function getNextID(arraySelected, currentID) {
  var selectedIndex = arraySelected.map(function (col) {
    return col.id;
  }).indexOf(currentID);

  if (selectedIndex === 0) {
    ++selectedIndex;
  } else {
    --selectedIndex;
  }

  return arraySelected[selectedIndex].id;
};

var orderSort = function orderSort(a, b) {
  if (a.order > b.order) {
    return 1;
  }

  if (b.order > a.order) {
    return -1;
  }

  return 0;
};

var getArrayCols = function getArrayCols(cols) {
  return Object.keys(cols).map(function (colKey) {
    return _objectSpread(_objectSpread({}, cols[colKey]), {}, {
      id: colKey
    });
  }).sort(orderSort).filter(function (col) {
    return col.visible;
  });
};

var ColumnsTab = function ColumnsTab() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref.handleValueChange,
      value = _ref.value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var preparedCols = Object.keys(value.cols).map(function (key) {
    return _objectSpread(_objectSpread({}, value.cols[key]), {}, {
      id: key
    });
  }).sort(orderSort);

  var onDragEnd = function onDragEnd(result) {
    var destination = result.destination,
        source = result.source;

    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    var items = (0, _arrayMove.default)(preparedCols, source.index, destination.index).map(function (item, index) {
      return _objectSpread(_objectSpread({}, item), {}, {
        order: index
      });
    });
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      cols: items.reduce(function (acc, currentValue) {
        acc[currentValue.id] = currentValue;
        return acc;
      }, {})
    }));
  };

  var handleChangeCheckbox = function handleChangeCheckbox(id, bool) {
    var tempCols = _objectSpread({}, value.cols);

    var beforeArrayCols = getArrayCols(_objectSpread({}, tempCols));
    tempCols[id].visible = bool;
    var arrayCols = getArrayCols(_objectSpread({}, tempCols));
    var elementsWithoutWidth = arrayCols.reduce(function (acc, current) {
      if ((0, _isNil.default)(current.width)) {
        ++acc;
      }

      return acc;
    }, 0);

    if (elementsWithoutWidth > 1) {
      arrayCols.forEach(function (element) {
        tempCols[element.id].width = 100 / arrayCols.length;
      });
    } else {
      if (bool) {
        if (arrayCols.length < 2) {
          tempCols[id].width = 100;
        } else {
          var nextId = getNextID(arrayCols, id);
          tempCols[id].width = tempCols[nextId].width / 2;
          tempCols[nextId].width = tempCols[nextId].width / 2;
        }
      } else if (arrayCols.length > 0) {
        var _nextId = getNextID(beforeArrayCols, id);

        tempCols[_nextId].width += tempCols[id].width;
      }
    }

    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      cols: tempCols
    }));
  };

  var handleChangeEditableText = function handleChangeEditableText(id, text) {
    var tempCols = _objectSpread({}, value.cols);

    tempCols[id].editable_label = text;
    handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      cols: tempCols
    }));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Data')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(Header, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "name-column",
        children: t('Column')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "text-column",
        children: t('Text')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        className: "actions-column",
        children: t('Action')
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_SortableList.default, {
      id: "column-options",
      items: preparedCols,
      onChangeCheckbox: handleChangeCheckbox,
      onChangeEditableText: handleChangeEditableText,
      onDragEnd: onDragEnd
    })]
  });
};

ColumnsTab.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.object
};
var _default = ColumnsTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvQ29sdW1uc1RhYi5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJGbGV4Q29sdW1uIiwiSGVhZGVyIiwiRmxleFJvdyIsImdldE5leHRJRCIsImFycmF5U2VsZWN0ZWQiLCJjdXJyZW50SUQiLCJzZWxlY3RlZEluZGV4IiwibWFwIiwiY29sIiwiaWQiLCJpbmRleE9mIiwib3JkZXJTb3J0IiwiYSIsImIiLCJvcmRlciIsImdldEFycmF5Q29scyIsImNvbHMiLCJPYmplY3QiLCJrZXlzIiwiY29sS2V5Iiwic29ydCIsImZpbHRlciIsInZpc2libGUiLCJDb2x1bW5zVGFiIiwiaGFuZGxlVmFsdWVDaGFuZ2UiLCJ2YWx1ZSIsInQiLCJwcmVwYXJlZENvbHMiLCJrZXkiLCJvbkRyYWdFbmQiLCJyZXN1bHQiLCJkZXN0aW5hdGlvbiIsInNvdXJjZSIsImRyb3BwYWJsZUlkIiwiaW5kZXgiLCJpdGVtcyIsIml0ZW0iLCJyZWR1Y2UiLCJhY2MiLCJjdXJyZW50VmFsdWUiLCJoYW5kbGVDaGFuZ2VDaGVja2JveCIsImJvb2wiLCJ0ZW1wQ29scyIsImJlZm9yZUFycmF5Q29scyIsImFycmF5Q29scyIsImVsZW1lbnRzV2l0aG91dFdpZHRoIiwiY3VycmVudCIsIndpZHRoIiwiZm9yRWFjaCIsImVsZW1lbnQiLCJsZW5ndGgiLCJuZXh0SWQiLCJoYW5kbGVDaGFuZ2VFZGl0YWJsZVRleHQiLCJ0ZXh0IiwiZWRpdGFibGVfbGFiZWwiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUcsK0JBQU9DLGtCQUFQLENBQUgscUVBQWY7QUFFQSxJQUFNQyxNQUFNLEdBQUcsK0JBQU9DLGVBQVAsQ0FBSCw4WUFBWjs7QUFzQkEsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBQ0MsYUFBRCxFQUFnQkMsU0FBaEIsRUFBOEI7QUFDNUMsTUFBSUMsYUFBYSxHQUFHRixhQUFhLENBQUNHLEdBQWQsQ0FBa0IsVUFBQ0MsR0FBRDtBQUFBLFdBQVNBLEdBQUcsQ0FBQ0MsRUFBYjtBQUFBLEdBQWxCLEVBQW1DQyxPQUFuQyxDQUEyQ0wsU0FBM0MsQ0FBcEI7O0FBQ0EsTUFBSUMsYUFBYSxLQUFLLENBQXRCLEVBQXlCO0FBQ3JCLE1BQUVBLGFBQUY7QUFDSCxHQUZELE1BRU87QUFDSCxNQUFFQSxhQUFGO0FBQ0g7O0FBQ0QsU0FBT0YsYUFBYSxDQUFDRSxhQUFELENBQWIsQ0FBNkJHLEVBQXBDO0FBQ0gsQ0FSRDs7QUFVQSxJQUFNRSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUN4QixNQUFJRCxDQUFDLENBQUNFLEtBQUYsR0FBVUQsQ0FBQyxDQUFDQyxLQUFoQixFQUF1QjtBQUNuQixXQUFPLENBQVA7QUFDSDs7QUFDRCxNQUFJRCxDQUFDLENBQUNDLEtBQUYsR0FBVUYsQ0FBQyxDQUFDRSxLQUFoQixFQUF1QjtBQUNuQixXQUFPLENBQUMsQ0FBUjtBQUNIOztBQUNELFNBQU8sQ0FBUDtBQUNILENBUkQ7O0FBVUEsSUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsSUFBRCxFQUFVO0FBQzNCLFNBQU9DLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZRixJQUFaLEVBQ0ZULEdBREUsQ0FDRSxVQUFDWSxNQUFEO0FBQUEsMkNBQWtCSCxJQUFJLENBQUNHLE1BQUQsQ0FBdEI7QUFBZ0NWLE1BQUFBLEVBQUUsRUFBRVU7QUFBcEM7QUFBQSxHQURGLEVBRUZDLElBRkUsQ0FFR1QsU0FGSCxFQUdGVSxNQUhFLENBR0ssVUFBQ2IsR0FBRDtBQUFBLFdBQVNBLEdBQUcsQ0FBQ2MsT0FBYjtBQUFBLEdBSEwsQ0FBUDtBQUlILENBTEQ7O0FBT0EsSUFBTUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBdUM7QUFBQSxpRkFBUCxFQUFPO0FBQUEsTUFBcENDLGlCQUFvQyxRQUFwQ0EsaUJBQW9DO0FBQUEsTUFBakJDLEtBQWlCLFFBQWpCQSxLQUFpQjs7QUFDdEQsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLFlBQVksR0FBR1YsTUFBTSxDQUFDQyxJQUFQLENBQVlPLEtBQUssQ0FBQ1QsSUFBbEIsRUFDaEJULEdBRGdCLENBQ1osVUFBQ3FCLEdBQUQ7QUFBQSwyQ0FBZUgsS0FBSyxDQUFDVCxJQUFOLENBQVdZLEdBQVgsQ0FBZjtBQUFnQ25CLE1BQUFBLEVBQUUsRUFBRW1CO0FBQXBDO0FBQUEsR0FEWSxFQUVoQlIsSUFGZ0IsQ0FFWFQsU0FGVyxDQUFyQjs7QUFJQSxNQUFNa0IsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBQ0MsTUFBRCxFQUFZO0FBQzFCLFFBQVFDLFdBQVIsR0FBZ0NELE1BQWhDLENBQVFDLFdBQVI7QUFBQSxRQUFxQkMsTUFBckIsR0FBZ0NGLE1BQWhDLENBQXFCRSxNQUFyQjs7QUFFQSxRQUFJLENBQUNELFdBQUwsRUFBa0I7QUFDZDtBQUNIOztBQUVELFFBQ0lBLFdBQVcsQ0FBQ0UsV0FBWixLQUE0QkQsTUFBTSxDQUFDQyxXQUFuQyxJQUNBRixXQUFXLENBQUNHLEtBQVosS0FBc0JGLE1BQU0sQ0FBQ0UsS0FGakMsRUFHRTtBQUNFO0FBQ0g7O0FBRUQsUUFBTUMsS0FBSyxHQUFHLHdCQUNWUixZQURVLEVBRVZLLE1BQU0sQ0FBQ0UsS0FGRyxFQUdWSCxXQUFXLENBQUNHLEtBSEYsRUFJWjNCLEdBSlksQ0FJUixVQUFDNkIsSUFBRCxFQUFPRixLQUFQO0FBQUEsNkNBQXVCRSxJQUF2QjtBQUE2QnRCLFFBQUFBLEtBQUssRUFBRW9CO0FBQXBDO0FBQUEsS0FKUSxDQUFkO0FBS0FWLElBQUFBLGlCQUFpQixpQ0FDVkMsS0FEVTtBQUViVCxNQUFBQSxJQUFJLEVBQUVtQixLQUFLLENBQUNFLE1BQU4sQ0FBYSxVQUFDQyxHQUFELEVBQU1DLFlBQU4sRUFBdUI7QUFDdENELFFBQUFBLEdBQUcsQ0FBQ0MsWUFBWSxDQUFDOUIsRUFBZCxDQUFILEdBQXVCOEIsWUFBdkI7QUFDQSxlQUFPRCxHQUFQO0FBQ0gsT0FISyxFQUdILEVBSEc7QUFGTyxPQUFqQjtBQU9ILEdBMUJEOztBQTRCQSxNQUFNRSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUMvQixFQUFELEVBQUtnQyxJQUFMLEVBQWM7QUFDdkMsUUFBSUMsUUFBUSxxQkFBUWpCLEtBQUssQ0FBQ1QsSUFBZCxDQUFaOztBQUNBLFFBQU0yQixlQUFlLEdBQUc1QixZQUFZLG1CQUFNMkIsUUFBTixFQUFwQztBQUNBQSxJQUFBQSxRQUFRLENBQUNqQyxFQUFELENBQVIsQ0FBYWEsT0FBYixHQUF1Qm1CLElBQXZCO0FBQ0EsUUFBTUcsU0FBUyxHQUFHN0IsWUFBWSxtQkFBTTJCLFFBQU4sRUFBOUI7QUFDQSxRQUFNRyxvQkFBb0IsR0FBR0QsU0FBUyxDQUFDUCxNQUFWLENBQWlCLFVBQUNDLEdBQUQsRUFBTVEsT0FBTixFQUFrQjtBQUM1RCxVQUFJLG9CQUFNQSxPQUFPLENBQUNDLEtBQWQsQ0FBSixFQUEwQjtBQUN0QixVQUFFVCxHQUFGO0FBQ0g7O0FBQ0QsYUFBT0EsR0FBUDtBQUNILEtBTDRCLEVBSzFCLENBTDBCLENBQTdCOztBQU9BLFFBQUlPLG9CQUFvQixHQUFHLENBQTNCLEVBQThCO0FBQzFCRCxNQUFBQSxTQUFTLENBQUNJLE9BQVYsQ0FBa0IsVUFBQ0MsT0FBRCxFQUFhO0FBQzNCUCxRQUFBQSxRQUFRLENBQUNPLE9BQU8sQ0FBQ3hDLEVBQVQsQ0FBUixDQUFxQnNDLEtBQXJCLEdBQTZCLE1BQU1ILFNBQVMsQ0FBQ00sTUFBN0M7QUFDSCxPQUZEO0FBR0gsS0FKRCxNQUlPO0FBQ0gsVUFBSVQsSUFBSixFQUFVO0FBQ04sWUFBSUcsU0FBUyxDQUFDTSxNQUFWLEdBQW1CLENBQXZCLEVBQTBCO0FBQ3RCUixVQUFBQSxRQUFRLENBQUNqQyxFQUFELENBQVIsQ0FBYXNDLEtBQWIsR0FBcUIsR0FBckI7QUFDSCxTQUZELE1BRU87QUFDSCxjQUFNSSxNQUFNLEdBQUdoRCxTQUFTLENBQUN5QyxTQUFELEVBQVluQyxFQUFaLENBQXhCO0FBQ0FpQyxVQUFBQSxRQUFRLENBQUNqQyxFQUFELENBQVIsQ0FBYXNDLEtBQWIsR0FBcUJMLFFBQVEsQ0FBQ1MsTUFBRCxDQUFSLENBQWlCSixLQUFqQixHQUF5QixDQUE5QztBQUNBTCxVQUFBQSxRQUFRLENBQUNTLE1BQUQsQ0FBUixDQUFpQkosS0FBakIsR0FBeUJMLFFBQVEsQ0FBQ1MsTUFBRCxDQUFSLENBQWlCSixLQUFqQixHQUF5QixDQUFsRDtBQUNIO0FBQ0osT0FSRCxNQVFPLElBQUlILFNBQVMsQ0FBQ00sTUFBVixHQUFtQixDQUF2QixFQUEwQjtBQUM3QixZQUFNQyxPQUFNLEdBQUdoRCxTQUFTLENBQUN3QyxlQUFELEVBQWtCbEMsRUFBbEIsQ0FBeEI7O0FBQ0FpQyxRQUFBQSxRQUFRLENBQUNTLE9BQUQsQ0FBUixDQUFpQkosS0FBakIsSUFBMEJMLFFBQVEsQ0FBQ2pDLEVBQUQsQ0FBUixDQUFhc0MsS0FBdkM7QUFDSDtBQUNKOztBQUVEdkIsSUFBQUEsaUJBQWlCLGlDQUNWQyxLQURVO0FBRWJULE1BQUFBLElBQUksRUFBRTBCO0FBRk8sT0FBakI7QUFJSCxHQW5DRDs7QUFxQ0EsTUFBTVUsd0JBQXdCLEdBQUcsU0FBM0JBLHdCQUEyQixDQUFDM0MsRUFBRCxFQUFLNEMsSUFBTCxFQUFjO0FBQzNDLFFBQUlYLFFBQVEscUJBQVFqQixLQUFLLENBQUNULElBQWQsQ0FBWjs7QUFDQTBCLElBQUFBLFFBQVEsQ0FBQ2pDLEVBQUQsQ0FBUixDQUFhNkMsY0FBYixHQUE4QkQsSUFBOUI7QUFFQTdCLElBQUFBLGlCQUFpQixpQ0FDVkMsS0FEVTtBQUViVCxNQUFBQSxJQUFJLEVBQUUwQjtBQUZPLE9BQWpCO0FBSUgsR0FSRDs7QUFVQSxzQkFDSSxzQkFBQyxTQUFEO0FBQUEsNEJBQ0kscUJBQUMsZUFBRDtBQUFBLGdCQUFVaEIsQ0FBQyxDQUFDLE1BQUQ7QUFBWCxNQURKLGVBRUksc0JBQUMsTUFBRDtBQUFBLDhCQUNJO0FBQU0sUUFBQSxTQUFTLEVBQUMsYUFBaEI7QUFBQSxrQkFBK0JBLENBQUMsQ0FBQyxRQUFEO0FBQWhDLFFBREosZUFFSTtBQUFNLFFBQUEsU0FBUyxFQUFDLGFBQWhCO0FBQUEsa0JBQStCQSxDQUFDLENBQUMsTUFBRDtBQUFoQyxRQUZKLGVBR0k7QUFBTSxRQUFBLFNBQVMsRUFBQyxnQkFBaEI7QUFBQSxrQkFBa0NBLENBQUMsQ0FBQyxRQUFEO0FBQW5DLFFBSEo7QUFBQSxNQUZKLGVBT0kscUJBQUMscUJBQUQ7QUFDSSxNQUFBLEVBQUUsRUFBQyxnQkFEUDtBQUVJLE1BQUEsS0FBSyxFQUFFQyxZQUZYO0FBR0ksTUFBQSxnQkFBZ0IsRUFBRWEsb0JBSHRCO0FBSUksTUFBQSxvQkFBb0IsRUFBRVksd0JBSjFCO0FBS0ksTUFBQSxTQUFTLEVBQUV2QjtBQUxmLE1BUEo7QUFBQSxJQURKO0FBaUJILENBbEdEOztBQW9HQU4sVUFBVSxDQUFDZ0MsU0FBWCxHQUF1QjtBQUNuQi9CLEVBQUFBLGlCQUFpQixFQUFFZ0MsbUJBQVVDLElBRFY7QUFFbkJoQyxFQUFBQSxLQUFLLEVBQUUrQixtQkFBVUU7QUFGRSxDQUF2QjtlQUtlbkMsVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBhcnJheU1vdmUgZnJvbSAnYXJyYXktbW92ZSc7XG5pbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IEZsZXhDb2x1bW4sIEZsZXhSb3cgfSBmcm9tICdAY29tcG9uZW50cy9zdHlsZWQnO1xuXG5pbXBvcnQgeyBTdWJtZW51IH0gZnJvbSAnLi4vU3R5bGVzJztcblxuaW1wb3J0IFNvcnRhYmxlTGlzdCBmcm9tICcuL1NvcnRhYmxlTGlzdCc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZChGbGV4Q29sdW1uKWBgO1xuXG5jb25zdCBIZWFkZXIgPSBzdHlsZWQoRmxleFJvdylgXG4gICAgLm5hbWUtY29sdW1uIHtcbiAgICAgICAgZmxleC1iYXNpczogMDtcbiAgICAgICAgZmxleC1ncm93OiAxO1xuICAgICAgICBwYWRkaW5nOiAwIDRweDtcbiAgICB9XG5cbiAgICAudGV4dC1jb2x1bW4ge1xuICAgICAgICB3aWR0aDogMTEwcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgNHB4O1xuICAgIH1cblxuICAgIC5hY3Rpb25zLWNvbHVtbiB7XG4gICAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgICBwYWRkaW5nOiAwIDRweDtcbiAgICB9XG4gICAgc3BhbiB7XG4gICAgICAgIGNvbG9yOiAjNGY1MWExO1xuICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgfVxuYDtcblxuY29uc3QgZ2V0TmV4dElEID0gKGFycmF5U2VsZWN0ZWQsIGN1cnJlbnRJRCkgPT4ge1xuICAgIGxldCBzZWxlY3RlZEluZGV4ID0gYXJyYXlTZWxlY3RlZC5tYXAoKGNvbCkgPT4gY29sLmlkKS5pbmRleE9mKGN1cnJlbnRJRCk7XG4gICAgaWYgKHNlbGVjdGVkSW5kZXggPT09IDApIHtcbiAgICAgICAgKytzZWxlY3RlZEluZGV4O1xuICAgIH0gZWxzZSB7XG4gICAgICAgIC0tc2VsZWN0ZWRJbmRleDtcbiAgICB9XG4gICAgcmV0dXJuIGFycmF5U2VsZWN0ZWRbc2VsZWN0ZWRJbmRleF0uaWQ7XG59O1xuXG5jb25zdCBvcmRlclNvcnQgPSAoYSwgYikgPT4ge1xuICAgIGlmIChhLm9yZGVyID4gYi5vcmRlcikge1xuICAgICAgICByZXR1cm4gMTtcbiAgICB9XG4gICAgaWYgKGIub3JkZXIgPiBhLm9yZGVyKSB7XG4gICAgICAgIHJldHVybiAtMTtcbiAgICB9XG4gICAgcmV0dXJuIDA7XG59O1xuXG5jb25zdCBnZXRBcnJheUNvbHMgPSAoY29scykgPT4ge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyhjb2xzKVxuICAgICAgICAubWFwKChjb2xLZXkpID0+ICh7IC4uLmNvbHNbY29sS2V5XSwgaWQ6IGNvbEtleSB9KSlcbiAgICAgICAgLnNvcnQob3JkZXJTb3J0KVxuICAgICAgICAuZmlsdGVyKChjb2wpID0+IGNvbC52aXNpYmxlKTtcbn07XG5cbmNvbnN0IENvbHVtbnNUYWIgPSAoeyBoYW5kbGVWYWx1ZUNoYW5nZSwgdmFsdWUgfSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHByZXBhcmVkQ29scyA9IE9iamVjdC5rZXlzKHZhbHVlLmNvbHMpXG4gICAgICAgIC5tYXAoKGtleSkgPT4gKHsgLi4udmFsdWUuY29sc1trZXldLCBpZDoga2V5IH0pKVxuICAgICAgICAuc29ydChvcmRlclNvcnQpO1xuXG4gICAgY29uc3Qgb25EcmFnRW5kID0gKHJlc3VsdCkgPT4ge1xuICAgICAgICBjb25zdCB7IGRlc3RpbmF0aW9uLCBzb3VyY2UgfSA9IHJlc3VsdDtcblxuICAgICAgICBpZiAoIWRlc3RpbmF0aW9uKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoXG4gICAgICAgICAgICBkZXN0aW5hdGlvbi5kcm9wcGFibGVJZCA9PT0gc291cmNlLmRyb3BwYWJsZUlkICYmXG4gICAgICAgICAgICBkZXN0aW5hdGlvbi5pbmRleCA9PT0gc291cmNlLmluZGV4XG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgaXRlbXMgPSBhcnJheU1vdmUoXG4gICAgICAgICAgICBwcmVwYXJlZENvbHMsXG4gICAgICAgICAgICBzb3VyY2UuaW5kZXgsXG4gICAgICAgICAgICBkZXN0aW5hdGlvbi5pbmRleFxuICAgICAgICApLm1hcCgoaXRlbSwgaW5kZXgpID0+ICh7IC4uLml0ZW0sIG9yZGVyOiBpbmRleCB9KSk7XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHtcbiAgICAgICAgICAgIC4uLnZhbHVlLFxuICAgICAgICAgICAgY29sczogaXRlbXMucmVkdWNlKChhY2MsIGN1cnJlbnRWYWx1ZSkgPT4ge1xuICAgICAgICAgICAgICAgIGFjY1tjdXJyZW50VmFsdWUuaWRdID0gY3VycmVudFZhbHVlO1xuICAgICAgICAgICAgICAgIHJldHVybiBhY2M7XG4gICAgICAgICAgICB9LCB7fSksXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVDaGFuZ2VDaGVja2JveCA9IChpZCwgYm9vbCkgPT4ge1xuICAgICAgICBsZXQgdGVtcENvbHMgPSB7IC4uLnZhbHVlLmNvbHMgfTtcbiAgICAgICAgY29uc3QgYmVmb3JlQXJyYXlDb2xzID0gZ2V0QXJyYXlDb2xzKHsgLi4udGVtcENvbHMgfSk7XG4gICAgICAgIHRlbXBDb2xzW2lkXS52aXNpYmxlID0gYm9vbDtcbiAgICAgICAgY29uc3QgYXJyYXlDb2xzID0gZ2V0QXJyYXlDb2xzKHsgLi4udGVtcENvbHMgfSk7XG4gICAgICAgIGNvbnN0IGVsZW1lbnRzV2l0aG91dFdpZHRoID0gYXJyYXlDb2xzLnJlZHVjZSgoYWNjLCBjdXJyZW50KSA9PiB7XG4gICAgICAgICAgICBpZiAoaXNOaWwoY3VycmVudC53aWR0aCkpIHtcbiAgICAgICAgICAgICAgICArK2FjYztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBhY2M7XG4gICAgICAgIH0sIDApO1xuXG4gICAgICAgIGlmIChlbGVtZW50c1dpdGhvdXRXaWR0aCA+IDEpIHtcbiAgICAgICAgICAgIGFycmF5Q29scy5mb3JFYWNoKChlbGVtZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgdGVtcENvbHNbZWxlbWVudC5pZF0ud2lkdGggPSAxMDAgLyBhcnJheUNvbHMubGVuZ3RoO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoYm9vbCkge1xuICAgICAgICAgICAgICAgIGlmIChhcnJheUNvbHMubGVuZ3RoIDwgMikge1xuICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sc1tpZF0ud2lkdGggPSAxMDA7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgbmV4dElkID0gZ2V0TmV4dElEKGFycmF5Q29scywgaWQpO1xuICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sc1tpZF0ud2lkdGggPSB0ZW1wQ29sc1tuZXh0SWRdLndpZHRoIC8gMjtcbiAgICAgICAgICAgICAgICAgICAgdGVtcENvbHNbbmV4dElkXS53aWR0aCA9IHRlbXBDb2xzW25leHRJZF0ud2lkdGggLyAyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYXJyYXlDb2xzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICBjb25zdCBuZXh0SWQgPSBnZXROZXh0SUQoYmVmb3JlQXJyYXlDb2xzLCBpZCk7XG4gICAgICAgICAgICAgICAgdGVtcENvbHNbbmV4dElkXS53aWR0aCArPSB0ZW1wQ29sc1tpZF0ud2lkdGg7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBoYW5kbGVWYWx1ZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgIGNvbHM6IHRlbXBDb2xzLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3QgaGFuZGxlQ2hhbmdlRWRpdGFibGVUZXh0ID0gKGlkLCB0ZXh0KSA9PiB7XG4gICAgICAgIGxldCB0ZW1wQ29scyA9IHsgLi4udmFsdWUuY29scyB9O1xuICAgICAgICB0ZW1wQ29sc1tpZF0uZWRpdGFibGVfbGFiZWwgPSB0ZXh0O1xuXG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHtcbiAgICAgICAgICAgIC4uLnZhbHVlLFxuICAgICAgICAgICAgY29sczogdGVtcENvbHMsXG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgPFN1Ym1lbnU+e3QoJ0RhdGEnKX08L1N1Ym1lbnU+XG4gICAgICAgICAgICA8SGVhZGVyPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cIm5hbWUtY29sdW1uXCI+e3QoJ0NvbHVtbicpfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0ZXh0LWNvbHVtblwiPnt0KCdUZXh0Jyl9PC9zcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cImFjdGlvbnMtY29sdW1uXCI+e3QoJ0FjdGlvbicpfTwvc3Bhbj5cbiAgICAgICAgICAgIDwvSGVhZGVyPlxuICAgICAgICAgICAgPFNvcnRhYmxlTGlzdFxuICAgICAgICAgICAgICAgIGlkPVwiY29sdW1uLW9wdGlvbnNcIlxuICAgICAgICAgICAgICAgIGl0ZW1zPXtwcmVwYXJlZENvbHN9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2VDaGVja2JveD17aGFuZGxlQ2hhbmdlQ2hlY2tib3h9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2VFZGl0YWJsZVRleHQ9e2hhbmRsZUNoYW5nZUVkaXRhYmxlVGV4dH1cbiAgICAgICAgICAgICAgICBvbkRyYWdFbmQ9e29uRHJhZ0VuZH1cbiAgICAgICAgICAgIC8+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5Db2x1bW5zVGFiLnByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDb2x1bW5zVGFiO1xuIl19