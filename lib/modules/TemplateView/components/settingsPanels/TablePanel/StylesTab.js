"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _ColorPickerInput = _interopRequireDefault(require("../../../../../components/ColorPickerInput"));

var _styled = require("../../../../../components/styled");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var FONT_SIZES = [{
  label: '8',
  value: '8px'
}, {
  label: '9',
  value: '9px'
}, {
  label: '10',
  value: '10px'
}, {
  label: '11',
  value: '11px'
}, {
  label: '12',
  value: '12px'
}, {
  label: '13',
  value: '13px'
}, {
  label: '14',
  value: '14px'
}, {
  label: '16',
  value: '16px'
}, {
  label: '18',
  value: '18px'
}, {
  label: '20',
  value: '20px'
}, {
  label: '24',
  value: '24px'
}, {
  label: '30',
  value: '30px'
}, {
  label: '36',
  value: '36px'
}, {
  label: '48',
  value: '48px'
}, {
  label: '60',
  value: '60px'
}, {
  label: '72',
  value: '72px'
}, {
  label: '96',
  value: '96px'
}];

var StylesTab = function StylesTab() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleStyleChange = _ref.handleStyleChange,
      style = _ref.style;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$table = style.table,
      _style$table$borderSt = _style$table.borderStyle,
      borderStyle = _style$table$borderSt === void 0 ? 'hidden' : _style$table$borderSt,
      _style$table$borderCo = _style$table.borderColor,
      borderColor = _style$table$borderCo === void 0 ? '#000' : _style$table$borderCo;
  var _style$header = style.header,
      _style$header$color = _style$header.color,
      headerColor = _style$header$color === void 0 ? '#fff' : _style$header$color,
      _style$header$backgro = _style$header.backgroundColor,
      headerBackgroundColor = _style$header$backgro === void 0 ? '#000' : _style$header$backgro,
      _style$header$fontSiz = _style$header.fontSize,
      headerFontSize = _style$header$fontSiz === void 0 ? '13px' : _style$header$fontSiz,
      headerFontBold = _style$header.fontBold,
      headerFontItalic = _style$header.fontItalic,
      roundedBorders = _style$header.roundedBorders;
  var _style$body = style.body,
      _style$body$color = _style$body.color,
      bodyColor = _style$body$color === void 0 ? '#000' : _style$body$color,
      _style$body$backgroun = _style$body.backgroundColor1,
      bodyBackgroundColor1 = _style$body$backgroun === void 0 ? '#fff' : _style$body$backgroun,
      _style$body$backgroun2 = _style$body.backgroundColor2,
      bodyBackgroundColor2 = _style$body$backgroun2 === void 0 ? '#ccc' : _style$body$backgroun2,
      _style$body$fontSize = _style$body.fontSize,
      bodyFontSize = _style$body$fontSize === void 0 ? '13px' : _style$body$fontSize,
      bodyFontBold = _style$body.fontBold,
      bodyFontItalic = _style$body.fontItalic,
      isZebraStyle = _style$body.isZebraStyle;
  var _style$footer = style.footer,
      footerBorderTopColor = _style$footer.borderTopColor,
      footerBorderTopStyle = _style$footer.borderTopStyle,
      _style$footer$color = _style$footer.color,
      footerColor = _style$footer$color === void 0 ? '#000' : _style$footer$color,
      _style$footer$backgro = _style$footer.backgroundColor1,
      footerBackgroundColor1 = _style$footer$backgro === void 0 ? '#fff' : _style$footer$backgro,
      _style$footer$backgro2 = _style$footer.backgroundColor2,
      footerBackgroundColor2 = _style$footer$backgro2 === void 0 ? '#ccc' : _style$footer$backgro2,
      _style$footer$fontSiz = _style$footer.fontSize,
      footerFontSize = _style$footer$fontSiz === void 0 ? '13px' : _style$footer$fontSiz,
      footerFontBold = _style$footer.fontBold,
      footerFontItalic = _style$footer.fontItalic,
      isFooterZebraStyle = _style$footer.isZebraStyle;
  var options = [{
    value: 'hidden',
    label: t('No border')
  }, {
    value: 'solid',
    label: t('Normal')
  }, {
    value: 'dashed',
    label: t('Lined')
  }, {
    value: 'dotted',
    label: t('Dotted')
  }];

  var handleTableStyleChange = function handleTableStyleChange(type, field, value) {
    var newStyle = {};
    var tableStyle = Object.assign({}, style[type]);
    tableStyle[field] = value;
    newStyle[type] = tableStyle;
    handleStyleChange(_objectSpread(_objectSpread({}, style), newStyle));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Table')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Border')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: borderStyle,
          onChange: function onChange(e) {
            return handleTableStyleChange('table', 'borderStyle', e.target.value);
          },
          as: "select",
          id: "select-theme",
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              disabled: optionDisabled,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Border color'),
        value: borderColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('table', 'borderColor', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Header')
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Background color'),
        value: headerBackgroundColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('header', 'backgroundColor', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Font color'),
        value: headerColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('header', 'color', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Font size')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: headerFontSize,
          onChange: function onChange(e) {
            return handleTableStyleChange('header', 'fontSize', e.target.value);
          },
          as: "select",
          id: "select-header-fontsize",
          children: FONT_SIZES.map(function (_ref3) {
            var label = _ref3.label,
                value = _ref3.value;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Bold')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "headerFontBold",
        onChange: function onChange(e) {
          return handleTableStyleChange('header', 'fontBold', e.target.checked);
        },
        checked: headerFontBold
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Italics')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "headerFontItalic",
        onChange: function onChange(e) {
          return handleTableStyleChange('header', 'fontItalic', e.target.checked);
        },
        checked: headerFontItalic
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Rounded')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "roundedBorders",
        onChange: function onChange(e) {
          return handleTableStyleChange('header', 'roundedBorders', e.target.checked);
        },
        checked: roundedBorders
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Body')
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Font color'),
        value: bodyColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('body', 'color', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Alternating colors')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "isZebraStyle",
        onChange: function onChange(e) {
          return handleTableStyleChange('body', 'isZebraStyle', e.target.checked);
        },
        checked: isZebraStyle
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Background color')).concat(isZebraStyle ? ' 1' : ''),
        value: bodyBackgroundColor1,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('body', 'backgroundColor1', tempColor);
        }
      })
    }), isZebraStyle && /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Background color'), " 2"),
        value: bodyBackgroundColor2,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('body', 'backgroundColor2', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Font size')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: bodyFontSize,
          onChange: function onChange(e) {
            return handleTableStyleChange('body', 'fontSize', e.target.value);
          },
          as: "select",
          id: "select-body-fontsize",
          children: FONT_SIZES.map(function (_ref4) {
            var label = _ref4.label,
                value = _ref4.value;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Bold')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "bodyFontBold",
        onChange: function onChange(e) {
          return handleTableStyleChange('body', 'fontBold', e.target.checked);
        },
        checked: bodyFontBold
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Italics')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "bodyFontItalic",
        onChange: function onChange(e) {
          return handleTableStyleChange('body', 'fontItalic', e.target.checked);
        },
        checked: bodyFontItalic
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
      children: t('Footer')
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Font color'),
        value: footerColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('footer', 'color', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Alternating colors')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "isFooterZebraStyle",
        onChange: function onChange(e) {
          return handleTableStyleChange('footer', 'isZebraStyle', e.target.checked);
        },
        checked: isFooterZebraStyle
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Background color')).concat(isFooterZebraStyle ? ' 1' : ''),
        value: footerBackgroundColor1,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('footer', 'backgroundColor1', tempColor);
        }
      })
    }), isFooterZebraStyle && /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Background color'), " 2"),
        value: footerBackgroundColor2,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('footer', 'backgroundColor2', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Border')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: footerBorderTopStyle,
          onChange: function onChange(e) {
            return handleTableStyleChange('footer', 'borderTopStyle', e.target.value);
          },
          as: "select",
          id: "select-theme",
          children: options.map(function (_ref5) {
            var label = _ref5.label,
                value = _ref5.value,
                optionDisabled = _ref5.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              disabled: optionDisabled,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Border color'),
        value: footerBorderTopColor,
        onChange: function onChange(tempColor) {
          return handleTableStyleChange('footer', 'borderTopColor', tempColor);
        }
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Font size')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: footerFontSize,
          onChange: function onChange(e) {
            return handleTableStyleChange('footer', 'fontSize', e.target.value);
          },
          as: "select",
          id: "select-footer-fontsize",
          children: FONT_SIZES.map(function (_ref6) {
            var label = _ref6.label,
                value = _ref6.value;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Bold')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "footerFontBold",
        onChange: function onChange(e) {
          return handleTableStyleChange('footer', 'fontBold', e.target.checked);
        },
        checked: footerFontBold
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Italics')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "footerFontItalic",
        onChange: function onChange(e) {
          return handleTableStyleChange('footer', 'fontItalic', e.target.checked);
        },
        checked: footerFontItalic
      })]
    })]
  });
};

StylesTab.propTypes = {
  handleStyleChange: _propTypes.default.func,
  style: _propTypes.default.object
};
var _default = StylesTab;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvU3R5bGVzVGFiLmpzIl0sIm5hbWVzIjpbIkZPTlRfU0laRVMiLCJsYWJlbCIsInZhbHVlIiwiU3R5bGVzVGFiIiwiaGFuZGxlU3R5bGVDaGFuZ2UiLCJzdHlsZSIsInQiLCJ0YWJsZSIsImJvcmRlclN0eWxlIiwiYm9yZGVyQ29sb3IiLCJoZWFkZXIiLCJjb2xvciIsImhlYWRlckNvbG9yIiwiYmFja2dyb3VuZENvbG9yIiwiaGVhZGVyQmFja2dyb3VuZENvbG9yIiwiZm9udFNpemUiLCJoZWFkZXJGb250U2l6ZSIsImhlYWRlckZvbnRCb2xkIiwiZm9udEJvbGQiLCJoZWFkZXJGb250SXRhbGljIiwiZm9udEl0YWxpYyIsInJvdW5kZWRCb3JkZXJzIiwiYm9keSIsImJvZHlDb2xvciIsImJhY2tncm91bmRDb2xvcjEiLCJib2R5QmFja2dyb3VuZENvbG9yMSIsImJhY2tncm91bmRDb2xvcjIiLCJib2R5QmFja2dyb3VuZENvbG9yMiIsImJvZHlGb250U2l6ZSIsImJvZHlGb250Qm9sZCIsImJvZHlGb250SXRhbGljIiwiaXNaZWJyYVN0eWxlIiwiZm9vdGVyIiwiZm9vdGVyQm9yZGVyVG9wQ29sb3IiLCJib3JkZXJUb3BDb2xvciIsImZvb3RlckJvcmRlclRvcFN0eWxlIiwiYm9yZGVyVG9wU3R5bGUiLCJmb290ZXJDb2xvciIsImZvb3RlckJhY2tncm91bmRDb2xvcjEiLCJmb290ZXJCYWNrZ3JvdW5kQ29sb3IyIiwiZm9vdGVyRm9udFNpemUiLCJmb290ZXJGb250Qm9sZCIsImZvb3RlckZvbnRJdGFsaWMiLCJpc0Zvb3RlclplYnJhU3R5bGUiLCJvcHRpb25zIiwiaGFuZGxlVGFibGVTdHlsZUNoYW5nZSIsInR5cGUiLCJmaWVsZCIsIm5ld1N0eWxlIiwidGFibGVTdHlsZSIsIk9iamVjdCIsImFzc2lnbiIsImUiLCJ0YXJnZXQiLCJtYXAiLCJvcHRpb25EaXNhYmxlZCIsImRpc2FibGVkIiwidGVtcENvbG9yIiwiY2hlY2tlZCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7O0FBUUEsSUFBTUEsVUFBVSxHQUFHLENBQ2Y7QUFBRUMsRUFBQUEsS0FBSyxFQUFFLEdBQVQ7QUFBY0MsRUFBQUEsS0FBSyxFQUFFO0FBQXJCLENBRGUsRUFFZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsR0FBVDtBQUFjQyxFQUFBQSxLQUFLLEVBQUU7QUFBckIsQ0FGZSxFQUdmO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQUhlLEVBSWY7QUFBRUQsRUFBQUEsS0FBSyxFQUFFLElBQVQ7QUFBZUMsRUFBQUEsS0FBSyxFQUFFO0FBQXRCLENBSmUsRUFLZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsSUFBVDtBQUFlQyxFQUFBQSxLQUFLLEVBQUU7QUFBdEIsQ0FMZSxFQU1mO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQU5lLEVBT2Y7QUFBRUQsRUFBQUEsS0FBSyxFQUFFLElBQVQ7QUFBZUMsRUFBQUEsS0FBSyxFQUFFO0FBQXRCLENBUGUsRUFRZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsSUFBVDtBQUFlQyxFQUFBQSxLQUFLLEVBQUU7QUFBdEIsQ0FSZSxFQVNmO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQVRlLEVBVWY7QUFBRUQsRUFBQUEsS0FBSyxFQUFFLElBQVQ7QUFBZUMsRUFBQUEsS0FBSyxFQUFFO0FBQXRCLENBVmUsRUFXZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsSUFBVDtBQUFlQyxFQUFBQSxLQUFLLEVBQUU7QUFBdEIsQ0FYZSxFQVlmO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQVplLEVBYWY7QUFBRUQsRUFBQUEsS0FBSyxFQUFFLElBQVQ7QUFBZUMsRUFBQUEsS0FBSyxFQUFFO0FBQXRCLENBYmUsRUFjZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsSUFBVDtBQUFlQyxFQUFBQSxLQUFLLEVBQUU7QUFBdEIsQ0FkZSxFQWVmO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQWZlLEVBZ0JmO0FBQUVELEVBQUFBLEtBQUssRUFBRSxJQUFUO0FBQWVDLEVBQUFBLEtBQUssRUFBRTtBQUF0QixDQWhCZSxFQWlCZjtBQUFFRCxFQUFBQSxLQUFLLEVBQUUsSUFBVDtBQUFlQyxFQUFBQSxLQUFLLEVBQUU7QUFBdEIsQ0FqQmUsQ0FBbkI7O0FBb0JBLElBQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFZLEdBQXVDO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BQXBDQyxpQkFBb0MsUUFBcENBLGlCQUFvQztBQUFBLE1BQWpCQyxLQUFpQixRQUFqQkEsS0FBaUI7O0FBQ3JELHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxxQkFBeURELEtBQUssQ0FBQ0UsS0FBL0Q7QUFBQSwyQ0FBUUMsV0FBUjtBQUFBLE1BQVFBLFdBQVIsc0NBQXNCLFFBQXRCO0FBQUEsMkNBQWdDQyxXQUFoQztBQUFBLE1BQWdDQSxXQUFoQyxzQ0FBOEMsTUFBOUM7QUFDQSxzQkFPSUosS0FBSyxDQUFDSyxNQVBWO0FBQUEsMENBQ0lDLEtBREo7QUFBQSxNQUNXQyxXQURYLG9DQUN5QixNQUR6QjtBQUFBLDRDQUVJQyxlQUZKO0FBQUEsTUFFcUJDLHFCQUZyQixzQ0FFNkMsTUFGN0M7QUFBQSw0Q0FHSUMsUUFISjtBQUFBLE1BR2NDLGNBSGQsc0NBRytCLE1BSC9CO0FBQUEsTUFJY0MsY0FKZCxpQkFJSUMsUUFKSjtBQUFBLE1BS2dCQyxnQkFMaEIsaUJBS0lDLFVBTEo7QUFBQSxNQU1JQyxjQU5KLGlCQU1JQSxjQU5KO0FBUUEsb0JBUUloQixLQUFLLENBQUNpQixJQVJWO0FBQUEsc0NBQ0lYLEtBREo7QUFBQSxNQUNXWSxTQURYLGtDQUN1QixNQUR2QjtBQUFBLDBDQUVJQyxnQkFGSjtBQUFBLE1BRXNCQyxvQkFGdEIsc0NBRTZDLE1BRjdDO0FBQUEsMkNBR0lDLGdCQUhKO0FBQUEsTUFHc0JDLG9CQUh0Qix1Q0FHNkMsTUFIN0M7QUFBQSx5Q0FJSVosUUFKSjtBQUFBLE1BSWNhLFlBSmQscUNBSTZCLE1BSjdCO0FBQUEsTUFLY0MsWUFMZCxlQUtJWCxRQUxKO0FBQUEsTUFNZ0JZLGNBTmhCLGVBTUlWLFVBTko7QUFBQSxNQU9JVyxZQVBKLGVBT0lBLFlBUEo7QUFTQSxzQkFVSTFCLEtBQUssQ0FBQzJCLE1BVlY7QUFBQSxNQUNvQkMsb0JBRHBCLGlCQUNJQyxjQURKO0FBQUEsTUFFb0JDLG9CQUZwQixpQkFFSUMsY0FGSjtBQUFBLDBDQUdJekIsS0FISjtBQUFBLE1BR1cwQixXQUhYLG9DQUd5QixNQUh6QjtBQUFBLDRDQUlJYixnQkFKSjtBQUFBLE1BSXNCYyxzQkFKdEIsc0NBSStDLE1BSi9DO0FBQUEsNkNBS0laLGdCQUxKO0FBQUEsTUFLc0JhLHNCQUx0Qix1Q0FLK0MsTUFML0M7QUFBQSw0Q0FNSXhCLFFBTko7QUFBQSxNQU1jeUIsY0FOZCxzQ0FNK0IsTUFOL0I7QUFBQSxNQU9jQyxjQVBkLGlCQU9JdkIsUUFQSjtBQUFBLE1BUWdCd0IsZ0JBUmhCLGlCQVFJdEIsVUFSSjtBQUFBLE1BU2tCdUIsa0JBVGxCLGlCQVNJWixZQVRKO0FBWUEsTUFBTWEsT0FBTyxHQUFHLENBQ1o7QUFBRTFDLElBQUFBLEtBQUssRUFBRSxRQUFUO0FBQW1CRCxJQUFBQSxLQUFLLEVBQUVLLENBQUMsQ0FBQyxXQUFEO0FBQTNCLEdBRFksRUFFWjtBQUFFSixJQUFBQSxLQUFLLEVBQUUsT0FBVDtBQUFrQkQsSUFBQUEsS0FBSyxFQUFFSyxDQUFDLENBQUMsUUFBRDtBQUExQixHQUZZLEVBR1o7QUFBRUosSUFBQUEsS0FBSyxFQUFFLFFBQVQ7QUFBbUJELElBQUFBLEtBQUssRUFBRUssQ0FBQyxDQUFDLE9BQUQ7QUFBM0IsR0FIWSxFQUlaO0FBQUVKLElBQUFBLEtBQUssRUFBRSxRQUFUO0FBQW1CRCxJQUFBQSxLQUFLLEVBQUVLLENBQUMsQ0FBQyxRQUFEO0FBQTNCLEdBSlksQ0FBaEI7O0FBT0EsTUFBTXVDLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBQ0MsSUFBRCxFQUFPQyxLQUFQLEVBQWM3QyxLQUFkLEVBQXdCO0FBQ25ELFFBQUk4QyxRQUFRLEdBQUcsRUFBZjtBQUNBLFFBQUlDLFVBQVUsR0FBR0MsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQjlDLEtBQUssQ0FBQ3lDLElBQUQsQ0FBdkIsQ0FBakI7QUFDQUcsSUFBQUEsVUFBVSxDQUFDRixLQUFELENBQVYsR0FBb0I3QyxLQUFwQjtBQUNBOEMsSUFBQUEsUUFBUSxDQUFDRixJQUFELENBQVIsR0FBaUJHLFVBQWpCO0FBQ0E3QyxJQUFBQSxpQkFBaUIsaUNBQ1ZDLEtBRFUsR0FFVjJDLFFBRlUsRUFBakI7QUFJSCxHQVREOztBQVdBLHNCQUNJLHNCQUFDLGtCQUFEO0FBQUEsNEJBQ0kscUJBQUMsZUFBRDtBQUFBLGdCQUFVMUMsQ0FBQyxDQUFDLE9BQUQ7QUFBWCxNQURKLGVBRUksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFBLENBQUMsQ0FBQyxRQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEtBQUssRUFBRUUsV0FEWDtBQUVJLFVBQUEsUUFBUSxFQUFFLGtCQUFDNEMsQ0FBRDtBQUFBLG1CQUNOUCxzQkFBc0IsQ0FDbEIsT0FEa0IsRUFFbEIsYUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTbkQsS0FIUyxDQURoQjtBQUFBLFdBRmQ7QUFTSSxVQUFBLEVBQUUsRUFBQyxRQVRQO0FBVUksVUFBQSxFQUFFLEVBQUMsY0FWUDtBQUFBLG9CQVlLMEMsT0FBTyxDQUFDVSxHQUFSLENBQ0c7QUFBQSxnQkFBR3JELEtBQUgsU0FBR0EsS0FBSDtBQUFBLGdCQUFVQyxLQUFWLFNBQVVBLEtBQVY7QUFBQSxnQkFBMkJxRCxjQUEzQixTQUFpQkMsUUFBakI7QUFBQSxnQ0FDSTtBQUVJLGNBQUEsS0FBSyxFQUFFdEQsS0FGWDtBQUdJLGNBQUEsUUFBUSxFQUFFcUQsY0FIZDtBQUFBLHdCQUtLdEQ7QUFMTCxnQ0FDbUJDLEtBRG5CLGNBQzRCRCxLQUQ1QixFQURKO0FBQUEsV0FESDtBQVpMO0FBREosUUFGSjtBQUFBLE1BRkosZUErQkkscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFSyxDQUFDLENBQUMsY0FBRCxDQURaO0FBRUksUUFBQSxLQUFLLEVBQUVHLFdBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQ2dELFNBQUQ7QUFBQSxpQkFDTlosc0JBQXNCLENBQ2xCLE9BRGtCLEVBRWxCLGFBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBO0FBSGQ7QUFESixNQS9CSixlQTZDSSxxQkFBQyxlQUFEO0FBQUEsZ0JBQVVuRCxDQUFDLENBQUMsUUFBRDtBQUFYLE1BN0NKLGVBOENJLHFCQUFDLHNCQUFEO0FBQUEsNkJBQ0kscUJBQUMseUJBQUQ7QUFDSSxRQUFBLEtBQUssRUFBRUEsQ0FBQyxDQUFDLGtCQUFELENBRFo7QUFFSSxRQUFBLEtBQUssRUFBRVEscUJBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzJDLFNBQUQ7QUFBQSxpQkFDTlosc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLGlCQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQTtBQUhkO0FBREosTUE5Q0osZUEyREkscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFbkQsQ0FBQyxDQUFDLFlBQUQsQ0FEWjtBQUVJLFFBQUEsS0FBSyxFQUFFTSxXQUZYO0FBR0ksUUFBQSxRQUFRLEVBQUUsa0JBQUM2QyxTQUFEO0FBQUEsaUJBQ05aLHNCQUFzQixDQUFDLFFBQUQsRUFBVyxPQUFYLEVBQW9CWSxTQUFwQixDQURoQjtBQUFBO0FBSGQ7QUFESixNQTNESixlQW9FSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUW5ELENBQUMsQ0FBQyxXQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEtBQUssRUFBRVUsY0FEWDtBQUVJLFVBQUEsUUFBUSxFQUFFLGtCQUFDb0MsQ0FBRDtBQUFBLG1CQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsVUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTbkQsS0FIUyxDQURoQjtBQUFBLFdBRmQ7QUFTSSxVQUFBLEVBQUUsRUFBQyxRQVRQO0FBVUksVUFBQSxFQUFFLEVBQUMsd0JBVlA7QUFBQSxvQkFZS0YsVUFBVSxDQUFDc0QsR0FBWCxDQUFlO0FBQUEsZ0JBQUdyRCxLQUFILFNBQUdBLEtBQUg7QUFBQSxnQkFBVUMsS0FBVixTQUFVQSxLQUFWO0FBQUEsZ0NBQ1o7QUFFSSxjQUFBLEtBQUssRUFBRUEsS0FGWDtBQUFBLHdCQUlLRDtBQUpMLGdDQUNtQkMsS0FEbkIsY0FDNEJELEtBRDVCLEVBRFk7QUFBQSxXQUFmO0FBWkw7QUFESixRQUZKO0FBQUEsTUFwRUosZUE4Rkksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFLLENBQUMsQ0FBQyxNQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksUUFBQSxFQUFFLEVBQUMsZ0JBRFA7QUFFSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzhDLENBQUQ7QUFBQSxpQkFDTlAsc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLFVBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBLFNBRmQ7QUFTSSxRQUFBLE9BQU8sRUFBRXpDO0FBVGIsUUFGSjtBQUFBLE1BOUZKLGVBNEdJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRWCxDQUFDLENBQUMsU0FBRDtBQUFULFFBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFFBQUEsRUFBRSxFQUFDLGtCQURQO0FBRUksUUFBQSxRQUFRLEVBQUUsa0JBQUM4QyxDQUFEO0FBQUEsaUJBQ05QLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixZQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQSxTQUZkO0FBU0ksUUFBQSxPQUFPLEVBQUV2QztBQVRiLFFBRko7QUFBQSxNQTVHSixlQTBISSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUWIsQ0FBQyxDQUFDLFNBQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxRQUFBLEVBQUUsRUFBQyxnQkFEUDtBQUVJLFFBQUEsUUFBUSxFQUFFLGtCQUFDOEMsQ0FBRDtBQUFBLGlCQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsZ0JBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBLFNBRmQ7QUFTSSxRQUFBLE9BQU8sRUFBRXJDO0FBVGIsUUFGSjtBQUFBLE1BMUhKLGVBeUlJLHFCQUFDLGVBQUQ7QUFBQSxnQkFBVWYsQ0FBQyxDQUFDLE1BQUQ7QUFBWCxNQXpJSixlQTBJSSxxQkFBQyxzQkFBRDtBQUFBLDZCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxLQUFLLEVBQUVBLENBQUMsQ0FBQyxZQUFELENBRFo7QUFFSSxRQUFBLEtBQUssRUFBRWlCLFNBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQ2tDLFNBQUQ7QUFBQSxpQkFDTlosc0JBQXNCLENBQUMsTUFBRCxFQUFTLE9BQVQsRUFBa0JZLFNBQWxCLENBRGhCO0FBQUE7QUFIZDtBQURKLE1BMUlKLGVBbUpJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRbkQsQ0FBQyxDQUFDLG9CQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksUUFBQSxFQUFFLEVBQUMsY0FEUDtBQUVJLFFBQUEsUUFBUSxFQUFFLGtCQUFDOEMsQ0FBRDtBQUFBLGlCQUNOUCxzQkFBc0IsQ0FDbEIsTUFEa0IsRUFFbEIsY0FGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTSyxPQUhTLENBRGhCO0FBQUEsU0FGZDtBQVNJLFFBQUEsT0FBTyxFQUFFM0I7QUFUYixRQUZKO0FBQUEsTUFuSkosZUFpS0kscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxZQUFLekIsQ0FBQyxDQUFDLGtCQUFELENBQU4sU0FDRHlCLFlBQVksR0FBRyxJQUFILEdBQVUsRUFEckIsQ0FEVDtBQUlJLFFBQUEsS0FBSyxFQUFFTixvQkFKWDtBQUtJLFFBQUEsUUFBUSxFQUFFLGtCQUFDZ0MsU0FBRDtBQUFBLGlCQUNOWixzQkFBc0IsQ0FDbEIsTUFEa0IsRUFFbEIsa0JBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBO0FBTGQ7QUFESixNQWpLSixFQWdMSzFCLFlBQVksaUJBQ1QscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxZQUFLekIsQ0FBQyxDQUFDLGtCQUFELENBQU4sT0FEVDtBQUVJLFFBQUEsS0FBSyxFQUFFcUIsb0JBRlg7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzhCLFNBQUQ7QUFBQSxpQkFDTlosc0JBQXNCLENBQ2xCLE1BRGtCLEVBRWxCLGtCQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQTtBQUhkO0FBREosTUFqTFIsZUErTEksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFuRCxDQUFDLENBQUMsV0FBRDtBQUFULFFBREosZUFFSSxxQkFBQyx3QkFBRDtBQUFBLCtCQUNJLHFCQUFDLGFBQUQsQ0FBTSxPQUFOO0FBQ0ksVUFBQSxLQUFLLEVBQUVzQixZQURYO0FBRUksVUFBQSxRQUFRLEVBQUUsa0JBQUN3QixDQUFEO0FBQUEsbUJBQ05QLHNCQUFzQixDQUNsQixNQURrQixFQUVsQixVQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNuRCxLQUhTLENBRGhCO0FBQUEsV0FGZDtBQVNJLFVBQUEsRUFBRSxFQUFDLFFBVFA7QUFVSSxVQUFBLEVBQUUsRUFBQyxzQkFWUDtBQUFBLG9CQVlLRixVQUFVLENBQUNzRCxHQUFYLENBQWU7QUFBQSxnQkFBR3JELEtBQUgsU0FBR0EsS0FBSDtBQUFBLGdCQUFVQyxLQUFWLFNBQVVBLEtBQVY7QUFBQSxnQ0FDWjtBQUVJLGNBQUEsS0FBSyxFQUFFQSxLQUZYO0FBQUEsd0JBSUtEO0FBSkwsZ0NBQ21CQyxLQURuQixjQUM0QkQsS0FENUIsRUFEWTtBQUFBLFdBQWY7QUFaTDtBQURKLFFBRko7QUFBQSxNQS9MSixlQXlOSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUUssQ0FBQyxDQUFDLE1BQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxRQUFBLEVBQUUsRUFBQyxjQURQO0FBRUksUUFBQSxRQUFRLEVBQUUsa0JBQUM4QyxDQUFEO0FBQUEsaUJBQ05QLHNCQUFzQixDQUNsQixNQURrQixFQUVsQixVQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQSxTQUZkO0FBU0ksUUFBQSxPQUFPLEVBQUU3QjtBQVRiLFFBRko7QUFBQSxNQXpOSixlQXVPSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUXZCLENBQUMsQ0FBQyxTQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksUUFBQSxFQUFFLEVBQUMsZ0JBRFA7QUFFSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzhDLENBQUQ7QUFBQSxpQkFDTlAsc0JBQXNCLENBQ2xCLE1BRGtCLEVBRWxCLFlBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBLFNBRmQ7QUFTSSxRQUFBLE9BQU8sRUFBRTVCO0FBVGIsUUFGSjtBQUFBLE1Bdk9KLGVBc1BJLHFCQUFDLGVBQUQ7QUFBQSxnQkFBVXhCLENBQUMsQ0FBQyxRQUFEO0FBQVgsTUF0UEosZUF1UEkscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFQSxDQUFDLENBQUMsWUFBRCxDQURaO0FBRUksUUFBQSxLQUFLLEVBQUUrQixXQUZYO0FBR0ksUUFBQSxRQUFRLEVBQUUsa0JBQUNvQixTQUFEO0FBQUEsaUJBQ05aLHNCQUFzQixDQUFDLFFBQUQsRUFBVyxPQUFYLEVBQW9CWSxTQUFwQixDQURoQjtBQUFBO0FBSGQ7QUFESixNQXZQSixlQWdRSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUW5ELENBQUMsQ0FBQyxvQkFBRDtBQUFULFFBREosZUFFSSxxQkFBQyxxQkFBRDtBQUNJLFFBQUEsRUFBRSxFQUFDLG9CQURQO0FBRUksUUFBQSxRQUFRLEVBQUUsa0JBQUM4QyxDQUFEO0FBQUEsaUJBQ05QLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixjQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNLLE9BSFMsQ0FEaEI7QUFBQSxTQUZkO0FBU0ksUUFBQSxPQUFPLEVBQUVmO0FBVGIsUUFGSjtBQUFBLE1BaFFKLGVBOFFJLHFCQUFDLHNCQUFEO0FBQUEsNkJBQ0kscUJBQUMseUJBQUQ7QUFDSSxRQUFBLEtBQUssWUFBS3JDLENBQUMsQ0FBQyxrQkFBRCxDQUFOLFNBQ0RxQyxrQkFBa0IsR0FBRyxJQUFILEdBQVUsRUFEM0IsQ0FEVDtBQUlJLFFBQUEsS0FBSyxFQUFFTCxzQkFKWDtBQUtJLFFBQUEsUUFBUSxFQUFFLGtCQUFDbUIsU0FBRDtBQUFBLGlCQUNOWixzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsa0JBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBO0FBTGQ7QUFESixNQTlRSixFQTZSS2Qsa0JBQWtCLGlCQUNmLHFCQUFDLHNCQUFEO0FBQUEsNkJBQ0kscUJBQUMseUJBQUQ7QUFDSSxRQUFBLEtBQUssWUFBS3JDLENBQUMsQ0FBQyxrQkFBRCxDQUFOLE9BRFQ7QUFFSSxRQUFBLEtBQUssRUFBRWlDLHNCQUZYO0FBR0ksUUFBQSxRQUFRLEVBQUUsa0JBQUNrQixTQUFEO0FBQUEsaUJBQ05aLHNCQUFzQixDQUNsQixRQURrQixFQUVsQixrQkFGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUE7QUFIZDtBQURKLE1BOVJSLGVBNFNJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRbkQsQ0FBQyxDQUFDLFFBQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMsd0JBQUQ7QUFBQSwrQkFDSSxxQkFBQyxhQUFELENBQU0sT0FBTjtBQUNJLFVBQUEsS0FBSyxFQUFFNkIsb0JBRFg7QUFFSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ2lCLENBQUQ7QUFBQSxtQkFDTlAsc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLGdCQUZrQixFQUdsQk8sQ0FBQyxDQUFDQyxNQUFGLENBQVNuRCxLQUhTLENBRGhCO0FBQUEsV0FGZDtBQVNJLFVBQUEsRUFBRSxFQUFDLFFBVFA7QUFVSSxVQUFBLEVBQUUsRUFBQyxjQVZQO0FBQUEsb0JBWUswQyxPQUFPLENBQUNVLEdBQVIsQ0FDRztBQUFBLGdCQUFHckQsS0FBSCxTQUFHQSxLQUFIO0FBQUEsZ0JBQVVDLEtBQVYsU0FBVUEsS0FBVjtBQUFBLGdCQUEyQnFELGNBQTNCLFNBQWlCQyxRQUFqQjtBQUFBLGdDQUNJO0FBRUksY0FBQSxLQUFLLEVBQUV0RCxLQUZYO0FBR0ksY0FBQSxRQUFRLEVBQUVxRCxjQUhkO0FBQUEsd0JBS0t0RDtBQUxMLGdDQUNtQkMsS0FEbkIsY0FDNEJELEtBRDVCLEVBREo7QUFBQSxXQURIO0FBWkw7QUFESixRQUZKO0FBQUEsTUE1U0osZUF5VUkscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxFQUFFSyxDQUFDLENBQUMsY0FBRCxDQURaO0FBRUksUUFBQSxLQUFLLEVBQUUyQixvQkFGWDtBQUdJLFFBQUEsUUFBUSxFQUFFLGtCQUFDd0IsU0FBRDtBQUFBLGlCQUNOWixzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsZ0JBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBO0FBSGQ7QUFESixNQXpVSixlQXNWSSxzQkFBQyxzQkFBRDtBQUFBLDhCQUNJLHFCQUFDLGFBQUQ7QUFBQSxrQkFBUW5ELENBQUMsQ0FBQyxXQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEtBQUssRUFBRWtDLGNBRFg7QUFFSSxVQUFBLFFBQVEsRUFBRSxrQkFBQ1ksQ0FBRDtBQUFBLG1CQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsVUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTbkQsS0FIUyxDQURoQjtBQUFBLFdBRmQ7QUFTSSxVQUFBLEVBQUUsRUFBQyxRQVRQO0FBVUksVUFBQSxFQUFFLEVBQUMsd0JBVlA7QUFBQSxvQkFZS0YsVUFBVSxDQUFDc0QsR0FBWCxDQUFlO0FBQUEsZ0JBQUdyRCxLQUFILFNBQUdBLEtBQUg7QUFBQSxnQkFBVUMsS0FBVixTQUFVQSxLQUFWO0FBQUEsZ0NBQ1o7QUFFSSxjQUFBLEtBQUssRUFBRUEsS0FGWDtBQUFBLHdCQUlLRDtBQUpMLGdDQUNtQkMsS0FEbkIsY0FDNEJELEtBRDVCLEVBRFk7QUFBQSxXQUFmO0FBWkw7QUFESixRQUZKO0FBQUEsTUF0VkosZUFnWEksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFLLENBQUMsQ0FBQyxNQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksUUFBQSxFQUFFLEVBQUMsZ0JBRFA7QUFFSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzhDLENBQUQ7QUFBQSxpQkFDTlAsc0JBQXNCLENBQ2xCLFFBRGtCLEVBRWxCLFVBRmtCLEVBR2xCTyxDQUFDLENBQUNDLE1BQUYsQ0FBU0ssT0FIUyxDQURoQjtBQUFBLFNBRmQ7QUFTSSxRQUFBLE9BQU8sRUFBRWpCO0FBVGIsUUFGSjtBQUFBLE1BaFhKLGVBOFhJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRbkMsQ0FBQyxDQUFDLFNBQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxRQUFBLEVBQUUsRUFBQyxrQkFEUDtBQUVJLFFBQUEsUUFBUSxFQUFFLGtCQUFDOEMsQ0FBRDtBQUFBLGlCQUNOUCxzQkFBc0IsQ0FDbEIsUUFEa0IsRUFFbEIsWUFGa0IsRUFHbEJPLENBQUMsQ0FBQ0MsTUFBRixDQUFTSyxPQUhTLENBRGhCO0FBQUEsU0FGZDtBQVNJLFFBQUEsT0FBTyxFQUFFaEI7QUFUYixRQUZKO0FBQUEsTUE5WEo7QUFBQSxJQURKO0FBK1lILENBamNEOztBQW1jQXZDLFNBQVMsQ0FBQ3dELFNBQVYsR0FBc0I7QUFDbEJ2RCxFQUFBQSxpQkFBaUIsRUFBRXdELG1CQUFVQyxJQURYO0FBRWxCeEQsRUFBQUEsS0FBSyxFQUFFdUQsbUJBQVVFO0FBRkMsQ0FBdEI7ZUFLZTNELFMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcblxuaW1wb3J0IENvbG9yUGlja2VySW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvQ29sb3JQaWNrZXJJbnB1dCc7XG5pbXBvcnQgeyBGbGV4Q29sdW1uIH0gZnJvbSAnQGNvbXBvbmVudHMvc3R5bGVkJztcblxuaW1wb3J0IHtcbiAgICBDaGVja2JveElucHV0LFxuICAgIElucHV0Q29udGFpbmVyLFxuICAgIExhYmVsLFxuICAgIFN0eWxlZElucHV0R3JvdXAsXG4gICAgU3VibWVudSxcbn0gZnJvbSAnLi4vU3R5bGVzJztcblxuY29uc3QgRk9OVF9TSVpFUyA9IFtcbiAgICB7IGxhYmVsOiAnOCcsIHZhbHVlOiAnOHB4JyB9LFxuICAgIHsgbGFiZWw6ICc5JywgdmFsdWU6ICc5cHgnIH0sXG4gICAgeyBsYWJlbDogJzEwJywgdmFsdWU6ICcxMHB4JyB9LFxuICAgIHsgbGFiZWw6ICcxMScsIHZhbHVlOiAnMTFweCcgfSxcbiAgICB7IGxhYmVsOiAnMTInLCB2YWx1ZTogJzEycHgnIH0sXG4gICAgeyBsYWJlbDogJzEzJywgdmFsdWU6ICcxM3B4JyB9LFxuICAgIHsgbGFiZWw6ICcxNCcsIHZhbHVlOiAnMTRweCcgfSxcbiAgICB7IGxhYmVsOiAnMTYnLCB2YWx1ZTogJzE2cHgnIH0sXG4gICAgeyBsYWJlbDogJzE4JywgdmFsdWU6ICcxOHB4JyB9LFxuICAgIHsgbGFiZWw6ICcyMCcsIHZhbHVlOiAnMjBweCcgfSxcbiAgICB7IGxhYmVsOiAnMjQnLCB2YWx1ZTogJzI0cHgnIH0sXG4gICAgeyBsYWJlbDogJzMwJywgdmFsdWU6ICczMHB4JyB9LFxuICAgIHsgbGFiZWw6ICczNicsIHZhbHVlOiAnMzZweCcgfSxcbiAgICB7IGxhYmVsOiAnNDgnLCB2YWx1ZTogJzQ4cHgnIH0sXG4gICAgeyBsYWJlbDogJzYwJywgdmFsdWU6ICc2MHB4JyB9LFxuICAgIHsgbGFiZWw6ICc3MicsIHZhbHVlOiAnNzJweCcgfSxcbiAgICB7IGxhYmVsOiAnOTYnLCB2YWx1ZTogJzk2cHgnIH0sXG5dO1xuXG5jb25zdCBTdHlsZXNUYWIgPSAoeyBoYW5kbGVTdHlsZUNoYW5nZSwgc3R5bGUgfSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHsgYm9yZGVyU3R5bGUgPSAnaGlkZGVuJywgYm9yZGVyQ29sb3IgPSAnIzAwMCcgfSA9IHN0eWxlLnRhYmxlO1xuICAgIGNvbnN0IHtcbiAgICAgICAgY29sb3I6IGhlYWRlckNvbG9yID0gJyNmZmYnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IGhlYWRlckJhY2tncm91bmRDb2xvciA9ICcjMDAwJyxcbiAgICAgICAgZm9udFNpemU6IGhlYWRlckZvbnRTaXplID0gJzEzcHgnLFxuICAgICAgICBmb250Qm9sZDogaGVhZGVyRm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWM6IGhlYWRlckZvbnRJdGFsaWMsXG4gICAgICAgIHJvdW5kZWRCb3JkZXJzLFxuICAgIH0gPSBzdHlsZS5oZWFkZXI7XG4gICAgY29uc3Qge1xuICAgICAgICBjb2xvcjogYm9keUNvbG9yID0gJyMwMDAnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IxOiBib2R5QmFja2dyb3VuZENvbG9yMSA9ICcjZmZmJyxcbiAgICAgICAgYmFja2dyb3VuZENvbG9yMjogYm9keUJhY2tncm91bmRDb2xvcjIgPSAnI2NjYycsXG4gICAgICAgIGZvbnRTaXplOiBib2R5Rm9udFNpemUgPSAnMTNweCcsXG4gICAgICAgIGZvbnRCb2xkOiBib2R5Rm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWM6IGJvZHlGb250SXRhbGljLFxuICAgICAgICBpc1plYnJhU3R5bGUsXG4gICAgfSA9IHN0eWxlLmJvZHk7XG4gICAgY29uc3Qge1xuICAgICAgICBib3JkZXJUb3BDb2xvcjogZm9vdGVyQm9yZGVyVG9wQ29sb3IsXG4gICAgICAgIGJvcmRlclRvcFN0eWxlOiBmb290ZXJCb3JkZXJUb3BTdHlsZSxcbiAgICAgICAgY29sb3I6IGZvb3RlckNvbG9yID0gJyMwMDAnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IxOiBmb290ZXJCYWNrZ3JvdW5kQ29sb3IxID0gJyNmZmYnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3IyOiBmb290ZXJCYWNrZ3JvdW5kQ29sb3IyID0gJyNjY2MnLFxuICAgICAgICBmb250U2l6ZTogZm9vdGVyRm9udFNpemUgPSAnMTNweCcsXG4gICAgICAgIGZvbnRCb2xkOiBmb290ZXJGb250Qm9sZCxcbiAgICAgICAgZm9udEl0YWxpYzogZm9vdGVyRm9udEl0YWxpYyxcbiAgICAgICAgaXNaZWJyYVN0eWxlOiBpc0Zvb3RlclplYnJhU3R5bGUsXG4gICAgfSA9IHN0eWxlLmZvb3RlcjtcblxuICAgIGNvbnN0IG9wdGlvbnMgPSBbXG4gICAgICAgIHsgdmFsdWU6ICdoaWRkZW4nLCBsYWJlbDogdCgnTm8gYm9yZGVyJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ3NvbGlkJywgbGFiZWw6IHQoJ05vcm1hbCcpIH0sXG4gICAgICAgIHsgdmFsdWU6ICdkYXNoZWQnLCBsYWJlbDogdCgnTGluZWQnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnZG90dGVkJywgbGFiZWw6IHQoJ0RvdHRlZCcpIH0sXG4gICAgXTtcblxuICAgIGNvbnN0IGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UgPSAodHlwZSwgZmllbGQsIHZhbHVlKSA9PiB7XG4gICAgICAgIGxldCBuZXdTdHlsZSA9IHt9O1xuICAgICAgICBsZXQgdGFibGVTdHlsZSA9IE9iamVjdC5hc3NpZ24oe30sIHN0eWxlW3R5cGVdKTtcbiAgICAgICAgdGFibGVTdHlsZVtmaWVsZF0gPSB2YWx1ZTtcbiAgICAgICAgbmV3U3R5bGVbdHlwZV0gPSB0YWJsZVN0eWxlO1xuICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi5zdHlsZSxcbiAgICAgICAgICAgIC4uLm5ld1N0eWxlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPEZsZXhDb2x1bW4+XG4gICAgICAgICAgICA8U3VibWVudT57dCgnVGFibGUnKX08L1N1Ym1lbnU+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb3JkZXInKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Ym9yZGVyU3R5bGV9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3RhYmxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlclN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC10aGVtZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtvcHRpb25zLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyBsYWJlbCwgdmFsdWUsIGRpc2FibGVkOiBvcHRpb25EaXNhYmxlZCB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQm9yZGVyIGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtib3JkZXJDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0YWJsZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlckNvbG9yJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuXG4gICAgICAgICAgICA8U3VibWVudT57dCgnSGVhZGVyJyl9PC9TdWJtZW51PlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCYWNrZ3JvdW5kIGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtoZWFkZXJCYWNrZ3JvdW5kQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaGVhZGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZENvbG9yJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdGb250IGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtoZWFkZXJDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKCdoZWFkZXInLCAnY29sb3InLCB0ZW1wQ29sb3IpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0ZvbnQgc2l6ZScpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtoZWFkZXJGb250U2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaGVhZGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZvbnRTaXplJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC1oZWFkZXItZm9udHNpemVcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Rk9OVF9TSVpFUy5tYXAoKHsgbGFiZWwsIHZhbHVlIH0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cbiAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnQm9sZCcpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJoZWFkZXJGb250Qm9sZFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2hlYWRlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZvbnRCb2xkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGVhZGVyRm9udEJvbGR9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdJdGFsaWNzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8Q2hlY2tib3hJbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD1cImhlYWRlckZvbnRJdGFsaWNcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoZWFkZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250SXRhbGljJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aGVhZGVyRm9udEl0YWxpY31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1JvdW5kZWQnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgIGlkPVwicm91bmRlZEJvcmRlcnNcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdoZWFkZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdyb3VuZGVkQm9yZGVycycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3JvdW5kZWRCb3JkZXJzfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuXG4gICAgICAgICAgICA8U3VibWVudT57dCgnQm9keScpfTwvU3VibWVudT5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnRm9udCBjb2xvcicpfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Ym9keUNvbG9yfVxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoJ2JvZHknLCAnY29sb3InLCB0ZW1wQ29sb3IpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0FsdGVybmF0aW5nIGNvbG9ycycpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJpc1plYnJhU3R5bGVcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib2R5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnaXNaZWJyYVN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aXNaZWJyYVN0eWxlfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXtgJHt0KCdCYWNrZ3JvdW5kIGNvbG9yJyl9JHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzWmVicmFTdHlsZSA/ICcgMScgOiAnJ1xuICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvZHlCYWNrZ3JvdW5kQ29sb3IxfVxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvZHknLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kQ29sb3IxJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAge2lzWmVicmFTdHlsZSAmJiAoXG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e2Ake3QoJ0JhY2tncm91bmQgY29sb3InKX0gMmB9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Ym9keUJhY2tncm91bmRDb2xvcjJ9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kQ29sb3IyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICApfVxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnRm9udCBzaXplJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgICAgICAgICAgPEZvcm0uQ29udHJvbFxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JvZHlGb250U2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250U2l6ZScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGUudGFyZ2V0LnZhbHVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtYm9keS1mb250c2l6ZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtGT05UX1NJWkVTLm1hcCgoeyBsYWJlbCwgdmFsdWUgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgc2VsZWN0LSR7dmFsdWV9LSR7bGFiZWx9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2xhYmVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb2xkJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8Q2hlY2tib3hJbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD1cImJvZHlGb250Qm9sZFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvZHknLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250Qm9sZCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2JvZHlGb250Qm9sZH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0l0YWxpY3MnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxDaGVja2JveElucHV0XG4gICAgICAgICAgICAgICAgICAgIGlkPVwiYm9keUZvbnRJdGFsaWNcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib2R5JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9udEl0YWxpYycsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e2JvZHlGb250SXRhbGljfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuXG4gICAgICAgICAgICA8U3VibWVudT57dCgnRm9vdGVyJyl9PC9TdWJtZW51PlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdGb250IGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtmb290ZXJDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKCdmb290ZXInLCAnY29sb3InLCB0ZW1wQ29sb3IpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0FsdGVybmF0aW5nIGNvbG9ycycpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJpc0Zvb3RlclplYnJhU3R5bGVcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdpc1plYnJhU3R5bGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGUudGFyZ2V0LmNoZWNrZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjaGVja2VkPXtpc0Zvb3RlclplYnJhU3R5bGV9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e2Ake3QoJ0JhY2tncm91bmQgY29sb3InKX0ke1xuICAgICAgICAgICAgICAgICAgICAgICAgaXNGb290ZXJaZWJyYVN0eWxlID8gJyAxJyA6ICcnXG4gICAgICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Zm9vdGVyQmFja2dyb3VuZENvbG9yMX1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kQ29sb3IxJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAge2lzRm9vdGVyWmVicmFTdHlsZSAmJiAoXG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9e2Ake3QoJ0JhY2tncm91bmQgY29sb3InKX0gMmB9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17Zm9vdGVyQmFja2dyb3VuZENvbG9yMn1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmFja2dyb3VuZENvbG9yMicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBDb2xvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0JvcmRlcicpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtmb290ZXJCb3JkZXJUb3BTdHlsZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvcmRlclRvcFN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC10aGVtZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtvcHRpb25zLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyBsYWJlbCwgdmFsdWUsIGRpc2FibGVkOiBvcHRpb25EaXNhYmxlZCB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQm9yZGVyIGNvbG9yJyl9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtmb290ZXJCb3JkZXJUb3BDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib3JkZXJUb3BDb2xvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ0ZvbnQgc2l6ZScpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtmb290ZXJGb250U2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9vdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZvbnRTaXplJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC1mb290ZXItZm9udHNpemVcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Rk9OVF9TSVpFUy5tYXAoKHsgbGFiZWwsIHZhbHVlIH0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cbiAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbD57dCgnQm9sZCcpfTwvTGFiZWw+XG4gICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaWQ9XCJmb290ZXJGb250Qm9sZFwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2Zvb3RlcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZvbnRCb2xkJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17Zm9vdGVyRm9udEJvbGR9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdJdGFsaWNzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICA8Q2hlY2tib3hJbnB1dFxuICAgICAgICAgICAgICAgICAgICBpZD1cImZvb3RlckZvbnRJdGFsaWNcIlxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb290ZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdmb250SXRhbGljJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17Zm9vdGVyRm9udEl0YWxpY31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgPC9GbGV4Q29sdW1uPlxuICAgICk7XG59O1xuXG5TdHlsZXNUYWIucHJvcFR5cGVzID0ge1xuICAgIGhhbmRsZVN0eWxlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFN0eWxlc1RhYjtcbiJdfQ==