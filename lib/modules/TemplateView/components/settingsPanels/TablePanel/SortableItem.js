"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _reactDom = require("react-dom");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _styled = require("../../../../../components/styled");

var _Styles = require("../Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ListItemContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    background: #fff;\n    border-bottom: 1px solid #ccc;\n    color: #848bab;\n    display: flex;\n    padding: 8px 0;\n    .drag-handle {\n        height: 16px;\n        i {\n            display: block;\n        }\n    }\n    .name-column {\n        flex-basis: 0;\n        flex-grow: 1;\n        padding: 0 4px;\n        align-items: center;\n    }\n\n    .text-column {\n        width: 110px;\n        padding: 0 4px;\n    }\n\n    .actions-column {\n        width: 50px;\n        padding: 0 4px;\n    }\n"])));

var ColNameText = _styledComponents.default.span(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    font-size: 13px;\n    font-weight: bold;\n    margin-left: 8px;\n"])));

var ColEditableText = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    font-size: 13px;\n"])));

var StyledInput = _styledComponents.default.input(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    width: 100%;\n    font-size: 13px;\n    background-color: rgba(255, 255, 255, 0.02);\n    border-radius: 3px;\n    border: 1px solid #eff1fb;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    color: #848bab;\n    padding: 4px 0 4px 6px;\n    font-weight: 400;\n    line-height: 1.5;\n"])));

var IconButton = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    border-radius: 50%;\n    text-align: center;\n    cursor: pointer;\n    margin-right: 4px;\n    min-width: 14px;\n    i {\n        display: block;\n        font-size: 14px;\n        color: ", ";\n    }\n"])), function (_ref) {
  var color = _ref.color;
  return color;
});

var _dragEl = document.getElementById('fixdraggable');

var optionalPortal = function optionalPortal(styles, element) {
  if (styles.position === 'fixed') {
    return /*#__PURE__*/(0, _reactDom.createPortal)(element, _dragEl);
  }

  return element;
};

var SortableItem = function SortableItem(_ref2) {
  var col = _ref2.col,
      editableTextField = _ref2.editableTextField,
      editMode = _ref2.editMode,
      handleSave = _ref2.handleSave,
      id = _ref2.id,
      index = _ref2.index,
      onChangeCheckbox = _ref2.onChangeCheckbox,
      setEditableTextField = _ref2.setEditableTextField,
      setEditMode = _ref2.setEditMode;
  var colId = col.id,
      editable_label = col.editable_label,
      label = col.label,
      visible = col.visible;

  var prepareEditMode = function prepareEditMode() {
    setEditMode(colId);
    setEditableTextField(editable_label || label);
  };

  var cancelEditMode = function cancelEditMode() {
    setEditMode('');
    setEditableTextField('');
  };

  var onChangeEdtitableTextField = function onChangeEdtitableTextField(event) {
    setEditableTextField(event.target.value);
  };

  var renderEditButtons = function renderEditButtons() {
    if (editMode) {
      return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#008dff",
          onClick: handleSave,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-save"
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
          color: "#ff0000",
          onClick: cancelEditMode,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
            className: "fas fa-times"
          })
        })]
      });
    }

    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(IconButton, {
        color: "#008dff",
        onClick: prepareEditMode,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
          className: "fas fa-edit"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
        id: "check-".concat(colId),
        onChange: function onChange(e) {
          return onChangeCheckbox(colId, e.target.checked);
        },
        checked: visible
      })]
    });
  };

  var renderEditText = function renderEditText() {
    if (editMode) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInput, {
        onChange: onChangeEdtitableTextField,
        value: editableTextField
      });
    }

    return /*#__PURE__*/(0, _jsxRuntime.jsx)(ColEditableText, {
      children: editable_label || label
    });
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.Draggable, {
    draggableId: id,
    index: index,
    children: function children(provided) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: optionalPortal(provided.draggableProps.style, /*#__PURE__*/(0, _jsxRuntime.jsxs)(ListItemContainer, _objectSpread(_objectSpread(_objectSpread({
          ref: provided.innerRef
        }, provided.draggableProps), provided.dragHandleProps), {}, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexRow, {
            className: "name-column",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              className: "drag-handle",
              children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
                className: "fas fa-sort"
              })
            }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ColNameText, {
              children: label
            })]
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexRow, {
            className: "text-column",
            children: renderEditText()
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexRow, {
            className: "actions-column",
            children: renderEditButtons()
          })]
        })))
      });
    }
  });
};

SortableItem.propTypes = {
  col: _propTypes.default.object,
  editableTextField: _propTypes.default.string,
  editMode: _propTypes.default.bool,
  handleSave: _propTypes.default.func,
  id: _propTypes.default.string,
  index: _propTypes.default.number,
  onChangeCheckbox: _propTypes.default.func,
  setEditableTextField: _propTypes.default.func,
  setEditMode: _propTypes.default.func
};
var _default = SortableItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvU29ydGFibGVJdGVtLmpzIl0sIm5hbWVzIjpbIkxpc3RJdGVtQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiQ29sTmFtZVRleHQiLCJzcGFuIiwiQ29sRWRpdGFibGVUZXh0IiwiU3R5bGVkSW5wdXQiLCJpbnB1dCIsIkljb25CdXR0b24iLCJjb2xvciIsIl9kcmFnRWwiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwib3B0aW9uYWxQb3J0YWwiLCJzdHlsZXMiLCJlbGVtZW50IiwicG9zaXRpb24iLCJTb3J0YWJsZUl0ZW0iLCJjb2wiLCJlZGl0YWJsZVRleHRGaWVsZCIsImVkaXRNb2RlIiwiaGFuZGxlU2F2ZSIsImlkIiwiaW5kZXgiLCJvbkNoYW5nZUNoZWNrYm94Iiwic2V0RWRpdGFibGVUZXh0RmllbGQiLCJzZXRFZGl0TW9kZSIsImNvbElkIiwiZWRpdGFibGVfbGFiZWwiLCJsYWJlbCIsInZpc2libGUiLCJwcmVwYXJlRWRpdE1vZGUiLCJjYW5jZWxFZGl0TW9kZSIsIm9uQ2hhbmdlRWR0aXRhYmxlVGV4dEZpZWxkIiwiZXZlbnQiLCJ0YXJnZXQiLCJ2YWx1ZSIsInJlbmRlckVkaXRCdXR0b25zIiwiZSIsImNoZWNrZWQiLCJyZW5kZXJFZGl0VGV4dCIsInByb3ZpZGVkIiwiZHJhZ2dhYmxlUHJvcHMiLCJzdHlsZSIsImlubmVyUmVmIiwiZHJhZ0hhbmRsZVByb3BzIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0Iiwic3RyaW5nIiwiYm9vbCIsImZ1bmMiLCJudW1iZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLGlCQUFpQixHQUFHQywwQkFBT0MsR0FBViw4bEJBQXZCOztBQStCQSxJQUFNQyxXQUFXLEdBQUdGLDBCQUFPRyxJQUFWLDhJQUFqQjs7QUFNQSxJQUFNQyxlQUFlLEdBQUdKLDBCQUFPRyxJQUFWLCtGQUFyQjs7QUFJQSxJQUFNRSxXQUFXLEdBQUdMLDBCQUFPTSxLQUFWLHFYQUFqQjs7QUFhQSxJQUFNQyxVQUFVLEdBQUdQLDBCQUFPQyxHQUFWLHlSQVNDO0FBQUEsTUFBR08sS0FBSCxRQUFHQSxLQUFIO0FBQUEsU0FBZUEsS0FBZjtBQUFBLENBVEQsQ0FBaEI7O0FBYUEsSUFBTUMsT0FBTyxHQUFHQyxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsY0FBeEIsQ0FBaEI7O0FBRUEsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxNQUFELEVBQVNDLE9BQVQsRUFBcUI7QUFDeEMsTUFBSUQsTUFBTSxDQUFDRSxRQUFQLEtBQW9CLE9BQXhCLEVBQWlDO0FBQzdCLHdCQUFPLDRCQUFhRCxPQUFiLEVBQXNCTCxPQUF0QixDQUFQO0FBQ0g7O0FBRUQsU0FBT0ssT0FBUDtBQUNILENBTkQ7O0FBUUEsSUFBTUUsWUFBWSxHQUFHLFNBQWZBLFlBQWUsUUFVZjtBQUFBLE1BVEZDLEdBU0UsU0FURkEsR0FTRTtBQUFBLE1BUkZDLGlCQVFFLFNBUkZBLGlCQVFFO0FBQUEsTUFQRkMsUUFPRSxTQVBGQSxRQU9FO0FBQUEsTUFORkMsVUFNRSxTQU5GQSxVQU1FO0FBQUEsTUFMRkMsRUFLRSxTQUxGQSxFQUtFO0FBQUEsTUFKRkMsS0FJRSxTQUpGQSxLQUlFO0FBQUEsTUFIRkMsZ0JBR0UsU0FIRkEsZ0JBR0U7QUFBQSxNQUZGQyxvQkFFRSxTQUZGQSxvQkFFRTtBQUFBLE1BREZDLFdBQ0UsU0FERkEsV0FDRTtBQUNGLE1BQVlDLEtBQVosR0FBc0RULEdBQXRELENBQVFJLEVBQVI7QUFBQSxNQUFtQk0sY0FBbkIsR0FBc0RWLEdBQXRELENBQW1CVSxjQUFuQjtBQUFBLE1BQW1DQyxLQUFuQyxHQUFzRFgsR0FBdEQsQ0FBbUNXLEtBQW5DO0FBQUEsTUFBMENDLE9BQTFDLEdBQXNEWixHQUF0RCxDQUEwQ1ksT0FBMUM7O0FBRUEsTUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixHQUFNO0FBQzFCTCxJQUFBQSxXQUFXLENBQUNDLEtBQUQsQ0FBWDtBQUNBRixJQUFBQSxvQkFBb0IsQ0FBQ0csY0FBYyxJQUFJQyxLQUFuQixDQUFwQjtBQUNILEdBSEQ7O0FBS0EsTUFBTUcsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixHQUFNO0FBQ3pCTixJQUFBQSxXQUFXLENBQUMsRUFBRCxDQUFYO0FBQ0FELElBQUFBLG9CQUFvQixDQUFDLEVBQUQsQ0FBcEI7QUFDSCxHQUhEOztBQUtBLE1BQU1RLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsQ0FBQ0MsS0FBRCxFQUFXO0FBQzFDVCxJQUFBQSxvQkFBb0IsQ0FBQ1MsS0FBSyxDQUFDQyxNQUFOLENBQWFDLEtBQWQsQ0FBcEI7QUFDSCxHQUZEOztBQUlBLE1BQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FBTTtBQUM1QixRQUFJakIsUUFBSixFQUFjO0FBQ1YsMEJBQ0k7QUFBQSxnQ0FDSSxxQkFBQyxVQUFEO0FBQVksVUFBQSxLQUFLLEVBQUMsU0FBbEI7QUFBNEIsVUFBQSxPQUFPLEVBQUVDLFVBQXJDO0FBQUEsaUNBQ0k7QUFBRyxZQUFBLFNBQVMsRUFBQztBQUFiO0FBREosVUFESixlQUlJLHFCQUFDLFVBQUQ7QUFBWSxVQUFBLEtBQUssRUFBQyxTQUFsQjtBQUE0QixVQUFBLE9BQU8sRUFBRVcsY0FBckM7QUFBQSxpQ0FDSTtBQUFHLFlBQUEsU0FBUyxFQUFDO0FBQWI7QUFESixVQUpKO0FBQUEsUUFESjtBQVVIOztBQUNELHdCQUNJO0FBQUEsOEJBQ0kscUJBQUMsVUFBRDtBQUFZLFFBQUEsS0FBSyxFQUFDLFNBQWxCO0FBQTRCLFFBQUEsT0FBTyxFQUFFRCxlQUFyQztBQUFBLCtCQUNJO0FBQUcsVUFBQSxTQUFTLEVBQUM7QUFBYjtBQURKLFFBREosZUFLSSxxQkFBQyxxQkFBRDtBQUNJLFFBQUEsRUFBRSxrQkFBV0osS0FBWCxDQUROO0FBRUksUUFBQSxRQUFRLEVBQUUsa0JBQUNXLENBQUQ7QUFBQSxpQkFBT2QsZ0JBQWdCLENBQUNHLEtBQUQsRUFBUVcsQ0FBQyxDQUFDSCxNQUFGLENBQVNJLE9BQWpCLENBQXZCO0FBQUEsU0FGZDtBQUdJLFFBQUEsT0FBTyxFQUFFVDtBQUhiLFFBTEo7QUFBQSxNQURKO0FBYUgsR0ExQkQ7O0FBNEJBLE1BQU1VLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUN6QixRQUFJcEIsUUFBSixFQUFjO0FBQ1YsMEJBQ0kscUJBQUMsV0FBRDtBQUNJLFFBQUEsUUFBUSxFQUFFYSwwQkFEZDtBQUVJLFFBQUEsS0FBSyxFQUFFZDtBQUZYLFFBREo7QUFNSDs7QUFDRCx3QkFBTyxxQkFBQyxlQUFEO0FBQUEsZ0JBQWtCUyxjQUFjLElBQUlDO0FBQXBDLE1BQVA7QUFDSCxHQVZEOztBQVlBLHNCQUNJLHFCQUFDLDRCQUFEO0FBQVcsSUFBQSxXQUFXLEVBQUVQLEVBQXhCO0FBQTRCLElBQUEsS0FBSyxFQUFFQyxLQUFuQztBQUFBLGNBQ0ssa0JBQUNrQixRQUFEO0FBQUEsMEJBQ0c7QUFBQSxrQkFDSzVCLGNBQWMsQ0FDWDRCLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QkMsS0FEYixlQUVYLHNCQUFDLGlCQUFEO0FBQ0ksVUFBQSxHQUFHLEVBQUVGLFFBQVEsQ0FBQ0c7QUFEbEIsV0FFUUgsUUFBUSxDQUFDQyxjQUZqQixHQUdRRCxRQUFRLENBQUNJLGVBSGpCO0FBQUEsa0NBS0ksc0JBQUMsZUFBRDtBQUFTLFlBQUEsU0FBUyxFQUFDLGFBQW5CO0FBQUEsb0NBQ0k7QUFBTSxjQUFBLFNBQVMsRUFBQyxhQUFoQjtBQUFBLHFDQUNJO0FBQUcsZ0JBQUEsU0FBUyxFQUFDO0FBQWI7QUFESixjQURKLGVBS0kscUJBQUMsV0FBRDtBQUFBLHdCQUFjaEI7QUFBZCxjQUxKO0FBQUEsWUFMSixlQVlJLHFCQUFDLGVBQUQ7QUFBUyxZQUFBLFNBQVMsRUFBQyxhQUFuQjtBQUFBLHNCQUNLVyxjQUFjO0FBRG5CLFlBWkosZUFlSSxxQkFBQyxlQUFEO0FBQVMsWUFBQSxTQUFTLEVBQUMsZ0JBQW5CO0FBQUEsc0JBQ0tILGlCQUFpQjtBQUR0QixZQWZKO0FBQUEsV0FGVztBQURuQixRQURIO0FBQUE7QUFETCxJQURKO0FBOEJILENBakdEOztBQW1HQXBCLFlBQVksQ0FBQzZCLFNBQWIsR0FBeUI7QUFDckI1QixFQUFBQSxHQUFHLEVBQUU2QixtQkFBVUMsTUFETTtBQUVyQjdCLEVBQUFBLGlCQUFpQixFQUFFNEIsbUJBQVVFLE1BRlI7QUFHckI3QixFQUFBQSxRQUFRLEVBQUUyQixtQkFBVUcsSUFIQztBQUlyQjdCLEVBQUFBLFVBQVUsRUFBRTBCLG1CQUFVSSxJQUpEO0FBS3JCN0IsRUFBQUEsRUFBRSxFQUFFeUIsbUJBQVVFLE1BTE87QUFNckIxQixFQUFBQSxLQUFLLEVBQUV3QixtQkFBVUssTUFOSTtBQU9yQjVCLEVBQUFBLGdCQUFnQixFQUFFdUIsbUJBQVVJLElBUFA7QUFRckIxQixFQUFBQSxvQkFBb0IsRUFBRXNCLG1CQUFVSSxJQVJYO0FBU3JCekIsRUFBQUEsV0FBVyxFQUFFcUIsbUJBQVVJO0FBVEYsQ0FBekI7ZUFZZWxDLFkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgRHJhZ2dhYmxlIH0gZnJvbSAncmVhY3QtYmVhdXRpZnVsLWRuZCc7XG5pbXBvcnQgeyBjcmVhdGVQb3J0YWwgfSBmcm9tICdyZWFjdC1kb20nO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IEZsZXhSb3cgfSBmcm9tICdAY29tcG9uZW50cy9zdHlsZWQnO1xuXG5pbXBvcnQgeyBDaGVja2JveElucHV0IH0gZnJvbSAnLi4vU3R5bGVzJztcblxuY29uc3QgTGlzdEl0ZW1Db250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBhZGRpbmc6IDhweCAwO1xuICAgIC5kcmFnLWhhbmRsZSB7XG4gICAgICAgIGhlaWdodDogMTZweDtcbiAgICAgICAgaSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgfVxuICAgIH1cbiAgICAubmFtZS1jb2x1bW4ge1xuICAgICAgICBmbGV4LWJhc2lzOiAwO1xuICAgICAgICBmbGV4LWdyb3c6IDE7XG4gICAgICAgIHBhZGRpbmc6IDAgNHB4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cblxuICAgIC50ZXh0LWNvbHVtbiB7XG4gICAgICAgIHdpZHRoOiAxMTBweDtcbiAgICAgICAgcGFkZGluZzogMCA0cHg7XG4gICAgfVxuXG4gICAgLmFjdGlvbnMtY29sdW1uIHtcbiAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgNHB4O1xuICAgIH1cbmA7XG5cbmNvbnN0IENvbE5hbWVUZXh0ID0gc3R5bGVkLnNwYW5gXG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIG1hcmdpbi1sZWZ0OiA4cHg7XG5gO1xuXG5jb25zdCBDb2xFZGl0YWJsZVRleHQgPSBzdHlsZWQuc3BhbmBcbiAgICBmb250LXNpemU6IDEzcHg7XG5gO1xuXG5jb25zdCBTdHlsZWRJbnB1dCA9IHN0eWxlZC5pbnB1dGBcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjAyKTtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2VmZjFmYjtcbiAgICBib3gtc2hhZG93OiAwIDJweCAxMnB4IDAgcmdiYSgxMjksIDE1OCwgMjAwLCAwLjA2KTtcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBwYWRkaW5nOiA0cHggMCA0cHggNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcbmA7XG5cbmNvbnN0IEljb25CdXR0b24gPSBzdHlsZWQuZGl2YFxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIG1hcmdpbi1yaWdodDogNHB4O1xuICAgIG1pbi13aWR0aDogMTRweDtcbiAgICBpIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICR7KHsgY29sb3IgfSkgPT4gY29sb3J9O1xuICAgIH1cbmA7XG5cbmNvbnN0IF9kcmFnRWwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZml4ZHJhZ2dhYmxlJyk7XG5cbmNvbnN0IG9wdGlvbmFsUG9ydGFsID0gKHN0eWxlcywgZWxlbWVudCkgPT4ge1xuICAgIGlmIChzdHlsZXMucG9zaXRpb24gPT09ICdmaXhlZCcpIHtcbiAgICAgICAgcmV0dXJuIGNyZWF0ZVBvcnRhbChlbGVtZW50LCBfZHJhZ0VsKTtcbiAgICB9XG5cbiAgICByZXR1cm4gZWxlbWVudDtcbn07XG5cbmNvbnN0IFNvcnRhYmxlSXRlbSA9ICh7XG4gICAgY29sLFxuICAgIGVkaXRhYmxlVGV4dEZpZWxkLFxuICAgIGVkaXRNb2RlLFxuICAgIGhhbmRsZVNhdmUsXG4gICAgaWQsXG4gICAgaW5kZXgsXG4gICAgb25DaGFuZ2VDaGVja2JveCxcbiAgICBzZXRFZGl0YWJsZVRleHRGaWVsZCxcbiAgICBzZXRFZGl0TW9kZSxcbn0pID0+IHtcbiAgICBjb25zdCB7IGlkOiBjb2xJZCwgZWRpdGFibGVfbGFiZWwsIGxhYmVsLCB2aXNpYmxlIH0gPSBjb2w7XG5cbiAgICBjb25zdCBwcmVwYXJlRWRpdE1vZGUgPSAoKSA9PiB7XG4gICAgICAgIHNldEVkaXRNb2RlKGNvbElkKTtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQoZWRpdGFibGVfbGFiZWwgfHwgbGFiZWwpO1xuICAgIH07XG5cbiAgICBjb25zdCBjYW5jZWxFZGl0TW9kZSA9ICgpID0+IHtcbiAgICAgICAgc2V0RWRpdE1vZGUoJycpO1xuICAgICAgICBzZXRFZGl0YWJsZVRleHRGaWVsZCgnJyk7XG4gICAgfTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlRWR0aXRhYmxlVGV4dEZpZWxkID0gKGV2ZW50KSA9PiB7XG4gICAgICAgIHNldEVkaXRhYmxlVGV4dEZpZWxkKGV2ZW50LnRhcmdldC52YWx1ZSk7XG4gICAgfTtcblxuICAgIGNvbnN0IHJlbmRlckVkaXRCdXR0b25zID0gKCkgPT4ge1xuICAgICAgICBpZiAoZWRpdE1vZGUpIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICAgICAgPEljb25CdXR0b24gY29sb3I9XCIjMDA4ZGZmXCIgb25DbGljaz17aGFuZGxlU2F2ZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtc2F2ZVwiIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPEljb25CdXR0b24gY29sb3I9XCIjZmYwMDAwXCIgb25DbGljaz17Y2FuY2VsRWRpdE1vZGV9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLXRpbWVzXCIgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBjb2xvcj1cIiMwMDhkZmZcIiBvbkNsaWNrPXtwcmVwYXJlRWRpdE1vZGV9PlxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtZWRpdFwiIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuXG4gICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaWQ9e2BjaGVjay0ke2NvbElkfWB9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gb25DaGFuZ2VDaGVja2JveChjb2xJZCwgZS50YXJnZXQuY2hlY2tlZCl9XG4gICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3Zpc2libGV9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvPlxuICAgICAgICApO1xuICAgIH07XG5cbiAgICBjb25zdCByZW5kZXJFZGl0VGV4dCA9ICgpID0+IHtcbiAgICAgICAgaWYgKGVkaXRNb2RlKSB7XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dFxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2VFZHRpdGFibGVUZXh0RmllbGR9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtlZGl0YWJsZVRleHRGaWVsZH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gPENvbEVkaXRhYmxlVGV4dD57ZWRpdGFibGVfbGFiZWwgfHwgbGFiZWx9PC9Db2xFZGl0YWJsZVRleHQ+O1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8RHJhZ2dhYmxlIGRyYWdnYWJsZUlkPXtpZH0gaW5kZXg9e2luZGV4fT5cbiAgICAgICAgICAgIHsocHJvdmlkZWQpID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICB7b3B0aW9uYWxQb3J0YWwoXG4gICAgICAgICAgICAgICAgICAgICAgICBwcm92aWRlZC5kcmFnZ2FibGVQcm9wcy5zdHlsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxMaXN0SXRlbUNvbnRhaW5lclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlZj17cHJvdmlkZWQuaW5uZXJSZWZ9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLnByb3ZpZGVkLmRyYWdnYWJsZVByb3BzfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5wcm92aWRlZC5kcmFnSGFuZGxlUHJvcHN9XG4gICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZsZXhSb3cgY2xhc3NOYW1lPVwibmFtZS1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZHJhZy1oYW5kbGVcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhcyBmYS1zb3J0XCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NwYW4+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPENvbE5hbWVUZXh0PntsYWJlbH08L0NvbE5hbWVUZXh0PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRmxleFJvdz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RmxleFJvdyBjbGFzc05hbWU9XCJ0ZXh0LWNvbHVtblwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7cmVuZGVyRWRpdFRleHQoKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0ZsZXhSb3c+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZsZXhSb3cgY2xhc3NOYW1lPVwiYWN0aW9ucy1jb2x1bW5cIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3JlbmRlckVkaXRCdXR0b25zKCl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9GbGV4Um93PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9MaXN0SXRlbUNvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICl9XG4gICAgICAgIDwvRHJhZ2dhYmxlPlxuICAgICk7XG59O1xuXG5Tb3J0YWJsZUl0ZW0ucHJvcFR5cGVzID0ge1xuICAgIGNvbDogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBlZGl0YWJsZVRleHRGaWVsZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBlZGl0TW9kZTogUHJvcFR5cGVzLmJvb2wsXG4gICAgaGFuZGxlU2F2ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaW5kZXg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgb25DaGFuZ2VDaGVja2JveDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2V0RWRpdGFibGVUZXh0RmllbGQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldEVkaXRNb2RlOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFNvcnRhYmxlSXRlbTtcbiJdfQ==