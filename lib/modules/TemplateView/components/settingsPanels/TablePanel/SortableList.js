"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _SortableItem = _interopRequireDefault(require("./SortableItem"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var SortableList = function SortableList(_ref) {
  var id = _ref.id,
      items = _ref.items,
      onChangeCheckbox = _ref.onChangeCheckbox,
      onChangeEditableText = _ref.onChangeEditableText,
      onDragEnd = _ref.onDragEnd;

  var _useState = (0, _react.useState)(''),
      _useState2 = _slicedToArray(_useState, 2),
      editItd = _useState2[0],
      setEditId = _useState2[1];

  var _useState3 = (0, _react.useState)(''),
      _useState4 = _slicedToArray(_useState3, 2),
      editableTextField = _useState4[0],
      setEditableTextField = _useState4[1];

  var handleSave = function handleSave() {
    onChangeEditableText(editItd, editableTextField);
    setEditId('');
    setEditableTextField('');
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.DragDropContext, {
    onDragEnd: onDragEnd,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBeautifulDnd.Droppable, {
      droppableId: id,
      children: function children(provided) {
        return /*#__PURE__*/(0, _jsxRuntime.jsxs)("div", _objectSpread(_objectSpread({
          ref: provided.innerRef
        }, provided.droppableProps), {}, {
          children: [items.map(function (value, index) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(_SortableItem.default, {
              col: value,
              id: "".concat(id, "-item-").concat(index),
              index: index,
              handleSave: handleSave,
              editMode: editItd === value.id,
              setEditableTextField: setEditableTextField,
              editableTextField: editableTextField,
              setEditMode: setEditId,
              onChangeCheckbox: onChangeCheckbox
            }, "".concat(id, "-item-").concat(index));
          }), provided.placeholder]
        }));
      }
    })
  });
};

SortableList.propTypes = {
  id: _propTypes.default.string,
  items: _propTypes.default.array,
  onChangeCheckbox: _propTypes.default.func,
  onChangeEditableText: _propTypes.default.func,
  onDragEnd: _propTypes.default.func
};
var _default = SortableList;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1RhYmxlUGFuZWwvU29ydGFibGVMaXN0LmpzIl0sIm5hbWVzIjpbIlNvcnRhYmxlTGlzdCIsImlkIiwiaXRlbXMiLCJvbkNoYW5nZUNoZWNrYm94Iiwib25DaGFuZ2VFZGl0YWJsZVRleHQiLCJvbkRyYWdFbmQiLCJlZGl0SXRkIiwic2V0RWRpdElkIiwiZWRpdGFibGVUZXh0RmllbGQiLCJzZXRFZGl0YWJsZVRleHRGaWVsZCIsImhhbmRsZVNhdmUiLCJwcm92aWRlZCIsImlubmVyUmVmIiwiZHJvcHBhYmxlUHJvcHMiLCJtYXAiLCJ2YWx1ZSIsImluZGV4IiwicGxhY2Vob2xkZXIiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJhcnJheSIsImZ1bmMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHLFNBQWZBLFlBQWUsT0FNZjtBQUFBLE1BTEZDLEVBS0UsUUFMRkEsRUFLRTtBQUFBLE1BSkZDLEtBSUUsUUFKRkEsS0FJRTtBQUFBLE1BSEZDLGdCQUdFLFFBSEZBLGdCQUdFO0FBQUEsTUFGRkMsb0JBRUUsUUFGRkEsb0JBRUU7QUFBQSxNQURGQyxTQUNFLFFBREZBLFNBQ0U7O0FBQ0Ysa0JBQTZCLHFCQUFTLEVBQVQsQ0FBN0I7QUFBQTtBQUFBLE1BQU9DLE9BQVA7QUFBQSxNQUFnQkMsU0FBaEI7O0FBQ0EsbUJBQWtELHFCQUFTLEVBQVQsQ0FBbEQ7QUFBQTtBQUFBLE1BQU9DLGlCQUFQO0FBQUEsTUFBMEJDLG9CQUExQjs7QUFFQSxNQUFNQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFNO0FBQ3JCTixJQUFBQSxvQkFBb0IsQ0FBQ0UsT0FBRCxFQUFVRSxpQkFBVixDQUFwQjtBQUNBRCxJQUFBQSxTQUFTLENBQUMsRUFBRCxDQUFUO0FBQ0FFLElBQUFBLG9CQUFvQixDQUFDLEVBQUQsQ0FBcEI7QUFDSCxHQUpEOztBQU1BLHNCQUNJLHFCQUFDLGtDQUFEO0FBQWlCLElBQUEsU0FBUyxFQUFFSixTQUE1QjtBQUFBLDJCQUNJLHFCQUFDLDRCQUFEO0FBQVcsTUFBQSxXQUFXLEVBQUVKLEVBQXhCO0FBQUEsZ0JBQ0ssa0JBQUNVLFFBQUQ7QUFBQSw0QkFDRztBQUFLLFVBQUEsR0FBRyxFQUFFQSxRQUFRLENBQUNDO0FBQW5CLFdBQWlDRCxRQUFRLENBQUNFLGNBQTFDO0FBQUEscUJBQ0tYLEtBQUssQ0FBQ1ksR0FBTixDQUFVLFVBQUNDLEtBQUQsRUFBUUMsS0FBUjtBQUFBLGdDQUNQLHFCQUFDLHFCQUFEO0FBQ0ksY0FBQSxHQUFHLEVBQUVELEtBRFQ7QUFFSSxjQUFBLEVBQUUsWUFBS2QsRUFBTCxtQkFBZ0JlLEtBQWhCLENBRk47QUFHSSxjQUFBLEtBQUssRUFBRUEsS0FIWDtBQUtJLGNBQUEsVUFBVSxFQUFFTixVQUxoQjtBQU1JLGNBQUEsUUFBUSxFQUFFSixPQUFPLEtBQUtTLEtBQUssQ0FBQ2QsRUFOaEM7QUFPSSxjQUFBLG9CQUFvQixFQUFFUSxvQkFQMUI7QUFRSSxjQUFBLGlCQUFpQixFQUFFRCxpQkFSdkI7QUFTSSxjQUFBLFdBQVcsRUFBRUQsU0FUakI7QUFVSSxjQUFBLGdCQUFnQixFQUFFSjtBQVZ0Qix5QkFJWUYsRUFKWixtQkFJdUJlLEtBSnZCLEVBRE87QUFBQSxXQUFWLENBREwsRUFlS0wsUUFBUSxDQUFDTSxXQWZkO0FBQUEsV0FESDtBQUFBO0FBREw7QUFESixJQURKO0FBeUJILENBekNEOztBQTJDQWpCLFlBQVksQ0FBQ2tCLFNBQWIsR0FBeUI7QUFDckJqQixFQUFBQSxFQUFFLEVBQUVrQixtQkFBVUMsTUFETztBQUVyQmxCLEVBQUFBLEtBQUssRUFBRWlCLG1CQUFVRSxLQUZJO0FBR3JCbEIsRUFBQUEsZ0JBQWdCLEVBQUVnQixtQkFBVUcsSUFIUDtBQUlyQmxCLEVBQUFBLG9CQUFvQixFQUFFZSxtQkFBVUcsSUFKWDtBQUtyQmpCLEVBQUFBLFNBQVMsRUFBRWMsbUJBQVVHO0FBTEEsQ0FBekI7ZUFRZXRCLFkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBEcmFnRHJvcENvbnRleHQsIERyb3BwYWJsZSB9IGZyb20gJ3JlYWN0LWJlYXV0aWZ1bC1kbmQnO1xuXG5pbXBvcnQgU29ydGFibGVJdGVtIGZyb20gJy4vU29ydGFibGVJdGVtJztcblxuY29uc3QgU29ydGFibGVMaXN0ID0gKHtcbiAgICBpZCxcbiAgICBpdGVtcyxcbiAgICBvbkNoYW5nZUNoZWNrYm94LFxuICAgIG9uQ2hhbmdlRWRpdGFibGVUZXh0LFxuICAgIG9uRHJhZ0VuZCxcbn0pID0+IHtcbiAgICBjb25zdCBbZWRpdEl0ZCwgc2V0RWRpdElkXSA9IHVzZVN0YXRlKCcnKTtcbiAgICBjb25zdCBbZWRpdGFibGVUZXh0RmllbGQsIHNldEVkaXRhYmxlVGV4dEZpZWxkXSA9IHVzZVN0YXRlKCcnKTtcblxuICAgIGNvbnN0IGhhbmRsZVNhdmUgPSAoKSA9PiB7XG4gICAgICAgIG9uQ2hhbmdlRWRpdGFibGVUZXh0KGVkaXRJdGQsIGVkaXRhYmxlVGV4dEZpZWxkKTtcbiAgICAgICAgc2V0RWRpdElkKCcnKTtcbiAgICAgICAgc2V0RWRpdGFibGVUZXh0RmllbGQoJycpO1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8RHJhZ0Ryb3BDb250ZXh0IG9uRHJhZ0VuZD17b25EcmFnRW5kfT5cbiAgICAgICAgICAgIDxEcm9wcGFibGUgZHJvcHBhYmxlSWQ9e2lkfT5cbiAgICAgICAgICAgICAgICB7KHByb3ZpZGVkKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgcmVmPXtwcm92aWRlZC5pbm5lclJlZn0gey4uLnByb3ZpZGVkLmRyb3BwYWJsZVByb3BzfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtpdGVtcy5tYXAoKHZhbHVlLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTb3J0YWJsZUl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2Ake2lkfS1pdGVtLSR7aW5kZXh9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg9e2luZGV4fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2Ake2lkfS1pdGVtLSR7aW5kZXh9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlU2F2ZT17aGFuZGxlU2F2ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWRpdE1vZGU9e2VkaXRJdGQgPT09IHZhbHVlLmlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRFZGl0YWJsZVRleHRGaWVsZD17c2V0RWRpdGFibGVUZXh0RmllbGR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRhYmxlVGV4dEZpZWxkPXtlZGl0YWJsZVRleHRGaWVsZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0RWRpdE1vZGU9e3NldEVkaXRJZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2VDaGVja2JveD17b25DaGFuZ2VDaGVja2JveH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgICAgICB7cHJvdmlkZWQucGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L0Ryb3BwYWJsZT5cbiAgICAgICAgPC9EcmFnRHJvcENvbnRleHQ+XG4gICAgKTtcbn07XG5cblNvcnRhYmxlTGlzdC5wcm9wVHlwZXMgPSB7XG4gICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaXRlbXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBvbkNoYW5nZUNoZWNrYm94OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbkNoYW5nZUVkaXRhYmxlVGV4dDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgb25EcmFnRW5kOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFNvcnRhYmxlTGlzdDtcbiJdfQ==