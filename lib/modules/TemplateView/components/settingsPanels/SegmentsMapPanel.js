"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _maps = require("../../../../constants/maps");

var _ProposalSelector = _interopRequireDefault(require("./ProposalSelector"));

var _Styles = require("./Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var Label = _styledComponents.default.span(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var SegmentsMapPanel = function SegmentsMapPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      catalogs = _ref.catalogs,
      handleStyleChange = _ref.handleStyleChange,
      handleValueChange = _ref.handleValueChange,
      hasProposalSelector = _ref.hasProposalSelector,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$segment = style.segment,
      segment = _style$segment === void 0 ? {} : _style$segment,
      _style$solarModule = style.solarModule,
      solarModule = _style$solarModule === void 0 ? {} : _style$solarModule,
      _style$showLabels = style.showLabels,
      showLabels = _style$showLabels === void 0 ? true : _style$showLabels,
      _style$showProjectMar = style.showProjectMarker,
      showProjectMarker = _style$showProjectMar === void 0 ? true : _style$showProjectMar;
  var _segment$fillColor = segment.fillColor,
      segmentFillColor = _segment$fillColor === void 0 ? _maps.SEGMENT_COLOR : _segment$fillColor,
      _segment$strokeColor = segment.strokeColor,
      segmentStrokeColor = _segment$strokeColor === void 0 ? _maps.SEGMENT_STROKE_COLOR : _segment$strokeColor;
  var _solarModule$fillColo = solarModule.fillColor,
      solarModuleFillColor = _solarModule$fillColo === void 0 ? _maps.SOLAR_MODULE_COLOR : _solarModule$fillColo,
      _solarModule$strokeCo = solarModule.strokeColor,
      solarModuleStrokeColor = _solarModule$strokeCo === void 0 ? _maps.SOLAR_MODULE_STROKE_COLOR : _solarModule$strokeCo;

  var handleTableStyleChange = function handleTableStyleChange(type, field, value) {
    var newStyle = {};
    var segmentStyle = Object.assign({}, style[type]);
    segmentStyle[field] = value;
    newStyle[type] = segmentStyle;
    handleStyleChange(_objectSpread(_objectSpread({}, style), newStyle));
  };

  var onChangeValueVisibilty = function onChangeValueVisibilty(key, value) {
    handleStyleChange(_objectSpread(_objectSpread({}, style), {}, _defineProperty({}, key, value)));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_ProposalSelector.default, {
      catalogs: catalogs,
      disabled: proposalSelectorDisabled,
      handleValueChange: handleValueChange,
      value: value,
      visible: hasProposalSelector
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: t('Project location')
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Show')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: "show-project-marker",
          onChange: function onChange(e) {
            return onChangeValueVisibilty('showProjectMarker', e.target.checked);
          },
          checked: showProjectMarker
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: t('Segment')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          hasAlphaChanel: true,
          label: t('Background'),
          value: segmentFillColor,
          onChange: function onChange(tempColor) {
            return handleTableStyleChange('segment', 'fillColor', tempColor);
          }
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          label: t('Border'),
          hasAlphaChanel: true,
          value: segmentStrokeColor,
          onChange: function onChange(tempColor) {
            return handleTableStyleChange('segment', 'strokeColor', tempColor);
          }
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
          children: t('Show measurements')
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.CheckboxInput, {
          id: "show-labels",
          onChange: function onChange(e) {
            return onChangeValueVisibilty('showLabels', e.target.checked);
          },
          checked: showLabels
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Submenu, {
        children: t('Panel')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          hasAlphaChanel: true,
          label: t('Background'),
          value: solarModuleFillColor,
          onChange: function onChange(tempColor) {
            return handleTableStyleChange('solarModule', 'fillColor', tempColor);
          }
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
          label: t('Border'),
          hasAlphaChanel: true,
          value: solarModuleStrokeColor,
          onChange: function onChange(tempColor) {
            return handleTableStyleChange('solarModule', 'strokeColor', tempColor);
          }
        })
      })]
    })]
  });
};

SegmentsMapPanel.propTypes = {
  catalogs: _propTypes.default.array,
  handleValueChange: _propTypes.default.func,
  handleStyleChange: _propTypes.default.func,
  hasProposalSelector: _propTypes.default.bool,
  proposalSelectorDisabled: _propTypes.default.bool,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = SegmentsMapPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL1NlZ21lbnRzTWFwUGFuZWwuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiTGFiZWwiLCJzcGFuIiwiU2VnbWVudHNNYXBQYW5lbCIsImNhdGFsb2dzIiwiaGFuZGxlU3R5bGVDaGFuZ2UiLCJoYW5kbGVWYWx1ZUNoYW5nZSIsImhhc1Byb3Bvc2FsU2VsZWN0b3IiLCJwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWQiLCJzdHlsZSIsInZhbHVlIiwidCIsInNlZ21lbnQiLCJzb2xhck1vZHVsZSIsInNob3dMYWJlbHMiLCJzaG93UHJvamVjdE1hcmtlciIsImZpbGxDb2xvciIsInNlZ21lbnRGaWxsQ29sb3IiLCJTRUdNRU5UX0NPTE9SIiwic3Ryb2tlQ29sb3IiLCJzZWdtZW50U3Ryb2tlQ29sb3IiLCJTRUdNRU5UX1NUUk9LRV9DT0xPUiIsInNvbGFyTW9kdWxlRmlsbENvbG9yIiwiU09MQVJfTU9EVUxFX0NPTE9SIiwic29sYXJNb2R1bGVTdHJva2VDb2xvciIsIlNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IiLCJoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlIiwidHlwZSIsImZpZWxkIiwibmV3U3R5bGUiLCJzZWdtZW50U3R5bGUiLCJPYmplY3QiLCJhc3NpZ24iLCJvbkNoYW5nZVZhbHVlVmlzaWJpbHR5Iiwia2V5IiwiZSIsInRhcmdldCIsImNoZWNrZWQiLCJ0ZW1wQ29sb3IiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsImZ1bmMiLCJib29sIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBT0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDBJQUFmOztBQU1BLElBQU1DLEtBQUssR0FBR0YsMEJBQU9HLElBQVYsdU9BQVg7O0FBVUEsSUFBTUMsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixHQVFkO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BUFBDLFFBT08sUUFQUEEsUUFPTztBQUFBLE1BTlBDLGlCQU1PLFFBTlBBLGlCQU1PO0FBQUEsTUFMUEMsaUJBS08sUUFMUEEsaUJBS087QUFBQSxNQUpQQyxtQkFJTyxRQUpQQSxtQkFJTztBQUFBLE1BSFBDLHdCQUdPLFFBSFBBLHdCQUdPO0FBQUEsd0JBRlBDLEtBRU87QUFBQSxNQUZQQSxLQUVPLDJCQUZDLEVBRUQ7QUFBQSx3QkFEUEMsS0FDTztBQUFBLE1BRFBBLEtBQ08sMkJBREMsRUFDRDs7QUFDUCx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EsdUJBS0lGLEtBTEosQ0FDSUcsT0FESjtBQUFBLE1BQ0lBLE9BREosK0JBQ2MsRUFEZDtBQUFBLDJCQUtJSCxLQUxKLENBRUlJLFdBRko7QUFBQSxNQUVJQSxXQUZKLG1DQUVrQixFQUZsQjtBQUFBLDBCQUtJSixLQUxKLENBR0lLLFVBSEo7QUFBQSxNQUdJQSxVQUhKLGtDQUdpQixJQUhqQjtBQUFBLDhCQUtJTCxLQUxKLENBSUlNLGlCQUpKO0FBQUEsTUFJSUEsaUJBSkosc0NBSXdCLElBSnhCO0FBTUEsMkJBR0lILE9BSEosQ0FDSUksU0FESjtBQUFBLE1BQ2VDLGdCQURmLG1DQUNrQ0MsbUJBRGxDO0FBQUEsNkJBR0lOLE9BSEosQ0FFSU8sV0FGSjtBQUFBLE1BRWlCQyxrQkFGakIscUNBRXNDQywwQkFGdEM7QUFJQSw4QkFHSVIsV0FISixDQUNJRyxTQURKO0FBQUEsTUFDZU0sb0JBRGYsc0NBQ3NDQyx3QkFEdEM7QUFBQSw4QkFHSVYsV0FISixDQUVJTSxXQUZKO0FBQUEsTUFFaUJLLHNCQUZqQixzQ0FFMENDLCtCQUYxQzs7QUFLQSxNQUFNQyxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNDLElBQUQsRUFBT0MsS0FBUCxFQUFjbEIsS0FBZCxFQUF3QjtBQUNuRCxRQUFJbUIsUUFBUSxHQUFHLEVBQWY7QUFDQSxRQUFJQyxZQUFZLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0J2QixLQUFLLENBQUNrQixJQUFELENBQXZCLENBQW5CO0FBQ0FHLElBQUFBLFlBQVksQ0FBQ0YsS0FBRCxDQUFaLEdBQXNCbEIsS0FBdEI7QUFDQW1CLElBQUFBLFFBQVEsQ0FBQ0YsSUFBRCxDQUFSLEdBQWlCRyxZQUFqQjtBQUNBekIsSUFBQUEsaUJBQWlCLGlDQUNWSSxLQURVLEdBRVZvQixRQUZVLEVBQWpCO0FBSUgsR0FURDs7QUFXQSxNQUFNSSxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNDLEdBQUQsRUFBTXhCLEtBQU4sRUFBZ0I7QUFDM0NMLElBQUFBLGlCQUFpQixpQ0FDVkksS0FEVSwyQkFFWnlCLEdBRlksRUFFTnhCLEtBRk0sR0FBakI7QUFJSCxHQUxEOztBQU9BLHNCQUNJO0FBQUEsNEJBQ0kscUJBQUMseUJBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRU4sUUFEZDtBQUVJLE1BQUEsUUFBUSxFQUFFSSx3QkFGZDtBQUdJLE1BQUEsaUJBQWlCLEVBQUVGLGlCQUh2QjtBQUlJLE1BQUEsS0FBSyxFQUFFSSxLQUpYO0FBS0ksTUFBQSxPQUFPLEVBQUVIO0FBTGIsTUFESixlQVFJLHNCQUFDLFNBQUQ7QUFBQSw4QkFDSSxxQkFBQyxlQUFEO0FBQUEsa0JBQVVJLENBQUMsQ0FBQyxrQkFBRDtBQUFYLFFBREosZUFFSSxzQkFBQyxzQkFBRDtBQUFBLGdDQUNJLHFCQUFDLEtBQUQ7QUFBQSxvQkFBUUEsQ0FBQyxDQUFDLE1BQUQ7QUFBVCxVQURKLGVBRUkscUJBQUMscUJBQUQ7QUFDSSxVQUFBLEVBQUUsRUFBQyxxQkFEUDtBQUVJLFVBQUEsUUFBUSxFQUFFLGtCQUFDd0IsQ0FBRDtBQUFBLG1CQUNORixzQkFBc0IsQ0FDbEIsbUJBRGtCLEVBRWxCRSxDQUFDLENBQUNDLE1BQUYsQ0FBU0MsT0FGUyxDQURoQjtBQUFBLFdBRmQ7QUFRSSxVQUFBLE9BQU8sRUFBRXRCO0FBUmIsVUFGSjtBQUFBLFFBRkosZUFlSSxxQkFBQyxlQUFEO0FBQUEsa0JBQVVKLENBQUMsQ0FBQyxTQUFEO0FBQVgsUUFmSixlQWdCSSxxQkFBQyxzQkFBRDtBQUFBLCtCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksVUFBQSxjQUFjLEVBQUUsSUFEcEI7QUFFSSxVQUFBLEtBQUssRUFBRUEsQ0FBQyxDQUFDLFlBQUQsQ0FGWjtBQUdJLFVBQUEsS0FBSyxFQUFFTSxnQkFIWDtBQUlJLFVBQUEsUUFBUSxFQUFFLGtCQUFDcUIsU0FBRDtBQUFBLG1CQUNOWixzQkFBc0IsQ0FDbEIsU0FEa0IsRUFFbEIsV0FGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUE7QUFKZDtBQURKLFFBaEJKLGVBOEJJLHFCQUFDLHNCQUFEO0FBQUEsK0JBQ0kscUJBQUMseUJBQUQ7QUFDSSxVQUFBLEtBQUssRUFBRTNCLENBQUMsQ0FBQyxRQUFELENBRFo7QUFFSSxVQUFBLGNBQWMsRUFBRSxJQUZwQjtBQUdJLFVBQUEsS0FBSyxFQUFFUyxrQkFIWDtBQUlJLFVBQUEsUUFBUSxFQUFFLGtCQUFDa0IsU0FBRDtBQUFBLG1CQUNOWixzQkFBc0IsQ0FDbEIsU0FEa0IsRUFFbEIsYUFGa0IsRUFHbEJZLFNBSGtCLENBRGhCO0FBQUE7QUFKZDtBQURKLFFBOUJKLGVBNENJLHNCQUFDLHNCQUFEO0FBQUEsZ0NBQ0kscUJBQUMsS0FBRDtBQUFBLG9CQUFRM0IsQ0FBQyxDQUFDLG1CQUFEO0FBQVQsVUFESixlQUVJLHFCQUFDLHFCQUFEO0FBQ0ksVUFBQSxFQUFFLEVBQUMsYUFEUDtBQUVJLFVBQUEsUUFBUSxFQUFFLGtCQUFDd0IsQ0FBRDtBQUFBLG1CQUNORixzQkFBc0IsQ0FDbEIsWUFEa0IsRUFFbEJFLENBQUMsQ0FBQ0MsTUFBRixDQUFTQyxPQUZTLENBRGhCO0FBQUEsV0FGZDtBQVFJLFVBQUEsT0FBTyxFQUFFdkI7QUFSYixVQUZKO0FBQUEsUUE1Q0osZUF5REkscUJBQUMsZUFBRDtBQUFBLGtCQUFVSCxDQUFDLENBQUMsT0FBRDtBQUFYLFFBekRKLGVBMERJLHFCQUFDLHNCQUFEO0FBQUEsK0JBQ0kscUJBQUMseUJBQUQ7QUFDSSxVQUFBLGNBQWMsRUFBRSxJQURwQjtBQUVJLFVBQUEsS0FBSyxFQUFFQSxDQUFDLENBQUMsWUFBRCxDQUZaO0FBR0ksVUFBQSxLQUFLLEVBQUVXLG9CQUhYO0FBSUksVUFBQSxRQUFRLEVBQUUsa0JBQUNnQixTQUFEO0FBQUEsbUJBQ05aLHNCQUFzQixDQUNsQixhQURrQixFQUVsQixXQUZrQixFQUdsQlksU0FIa0IsQ0FEaEI7QUFBQTtBQUpkO0FBREosUUExREosZUF3RUkscUJBQUMsc0JBQUQ7QUFBQSwrQkFDSSxxQkFBQyx5QkFBRDtBQUNJLFVBQUEsS0FBSyxFQUFFM0IsQ0FBQyxDQUFDLFFBQUQsQ0FEWjtBQUVJLFVBQUEsY0FBYyxFQUFFLElBRnBCO0FBR0ksVUFBQSxLQUFLLEVBQUVhLHNCQUhYO0FBSUksVUFBQSxRQUFRLEVBQUUsa0JBQUNjLFNBQUQ7QUFBQSxtQkFDTlosc0JBQXNCLENBQ2xCLGFBRGtCLEVBRWxCLGFBRmtCLEVBR2xCWSxTQUhrQixDQURoQjtBQUFBO0FBSmQ7QUFESixRQXhFSjtBQUFBLE1BUko7QUFBQSxJQURKO0FBa0dILENBN0lEOztBQStJQW5DLGdCQUFnQixDQUFDb0MsU0FBakIsR0FBNkI7QUFDekJuQyxFQUFBQSxRQUFRLEVBQUVvQyxtQkFBVUMsS0FESztBQUV6Qm5DLEVBQUFBLGlCQUFpQixFQUFFa0MsbUJBQVVFLElBRko7QUFHekJyQyxFQUFBQSxpQkFBaUIsRUFBRW1DLG1CQUFVRSxJQUhKO0FBSXpCbkMsRUFBQUEsbUJBQW1CLEVBQUVpQyxtQkFBVUcsSUFKTjtBQUt6Qm5DLEVBQUFBLHdCQUF3QixFQUFFZ0MsbUJBQVVHLElBTFg7QUFNekJsQyxFQUFBQSxLQUFLLEVBQUUrQixtQkFBVUksTUFOUTtBQU96QmxDLEVBQUFBLEtBQUssRUFBRThCLG1CQUFVSTtBQVBRLENBQTdCO2VBVWV6QyxnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBDb2xvclBpY2tlcklucHV0IGZyb20gJ0Bjb21wb25lbnRzL0NvbG9yUGlja2VySW5wdXQnO1xuXG5pbXBvcnQge1xuICAgIFNFR01FTlRfQ09MT1IsXG4gICAgU0VHTUVOVF9TVFJPS0VfQ09MT1IsXG4gICAgU09MQVJfTU9EVUxFX0NPTE9SLFxuICAgIFNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IsXG59IGZyb20gJ0Bjb25zdGFudHMvbWFwcyc7XG5cbmltcG9ydCBQcm9wb3NhbFNlbGVjdG9yIGZyb20gJy4vUHJvcG9zYWxTZWxlY3Rvcic7XG5pbXBvcnQgeyBDaGVja2JveElucHV0LCBJbnB1dENvbnRhaW5lciwgU3VibWVudSB9IGZyb20gJy4vU3R5bGVzJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBMYWJlbCA9IHN0eWxlZC5zcGFuYFxuICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIzcHg7XG4gICAgbWluLWhlaWdodDogMTZweDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICB3aWR0aDogMTM1cHg7XG5gO1xuXG5jb25zdCBTZWdtZW50c01hcFBhbmVsID0gKHtcbiAgICBjYXRhbG9ncyxcbiAgICBoYW5kbGVTdHlsZUNoYW5nZSxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZSxcbiAgICBoYXNQcm9wb3NhbFNlbGVjdG9yLFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZCxcbiAgICBzdHlsZSA9IHt9LFxuICAgIHZhbHVlID0ge30sXG59ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3Qge1xuICAgICAgICBzZWdtZW50ID0ge30sXG4gICAgICAgIHNvbGFyTW9kdWxlID0ge30sXG4gICAgICAgIHNob3dMYWJlbHMgPSB0cnVlLFxuICAgICAgICBzaG93UHJvamVjdE1hcmtlciA9IHRydWUsXG4gICAgfSA9IHN0eWxlO1xuICAgIGNvbnN0IHtcbiAgICAgICAgZmlsbENvbG9yOiBzZWdtZW50RmlsbENvbG9yID0gU0VHTUVOVF9DT0xPUixcbiAgICAgICAgc3Ryb2tlQ29sb3I6IHNlZ21lbnRTdHJva2VDb2xvciA9IFNFR01FTlRfU1RST0tFX0NPTE9SLFxuICAgIH0gPSBzZWdtZW50O1xuICAgIGNvbnN0IHtcbiAgICAgICAgZmlsbENvbG9yOiBzb2xhck1vZHVsZUZpbGxDb2xvciA9IFNPTEFSX01PRFVMRV9DT0xPUixcbiAgICAgICAgc3Ryb2tlQ29sb3I6IHNvbGFyTW9kdWxlU3Ryb2tlQ29sb3IgPSBTT0xBUl9NT0RVTEVfU1RST0tFX0NPTE9SLFxuICAgIH0gPSBzb2xhck1vZHVsZTtcblxuICAgIGNvbnN0IGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UgPSAodHlwZSwgZmllbGQsIHZhbHVlKSA9PiB7XG4gICAgICAgIGxldCBuZXdTdHlsZSA9IHt9O1xuICAgICAgICBsZXQgc2VnbWVudFN0eWxlID0gT2JqZWN0LmFzc2lnbih7fSwgc3R5bGVbdHlwZV0pO1xuICAgICAgICBzZWdtZW50U3R5bGVbZmllbGRdID0gdmFsdWU7XG4gICAgICAgIG5ld1N0eWxlW3R5cGVdID0gc2VnbWVudFN0eWxlO1xuICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi5zdHlsZSxcbiAgICAgICAgICAgIC4uLm5ld1N0eWxlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VWYWx1ZVZpc2liaWx0eSA9IChrZXksIHZhbHVlKSA9PiB7XG4gICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlKHtcbiAgICAgICAgICAgIC4uLnN0eWxlLFxuICAgICAgICAgICAgW2tleV06IHZhbHVlLFxuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPD5cbiAgICAgICAgICAgIDxQcm9wb3NhbFNlbGVjdG9yXG4gICAgICAgICAgICAgICAgY2F0YWxvZ3M9e2NhdGFsb2dzfVxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXtwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2U9e2hhbmRsZVZhbHVlQ2hhbmdlfVxuICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICB2aXNpYmxlPXtoYXNQcm9wb3NhbFNlbGVjdG9yfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPFN1Ym1lbnU+e3QoJ1Byb2plY3QgbG9jYXRpb24nKX08L1N1Ym1lbnU+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1Nob3cnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgICAgICA8Q2hlY2tib3hJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzaG93LXByb2plY3QtbWFya2VyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZVZhbHVlVmlzaWJpbHR5KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnc2hvd1Byb2plY3RNYXJrZXInLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlLnRhcmdldC5jaGVja2VkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17c2hvd1Byb2plY3RNYXJrZXJ9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8U3VibWVudT57dCgnU2VnbWVudCcpfTwvU3VibWVudT5cbiAgICAgICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxDb2xvclBpY2tlcklucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNBbHBoYUNoYW5lbD17dHJ1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCYWNrZ3JvdW5kJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17c2VnbWVudEZpbGxDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdzZWdtZW50JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZpbGxDb2xvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBDb2xvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCb3JkZXInKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0FscGhhQ2hhbmVsPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3NlZ21lbnRTdHJva2VDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVRhYmxlU3R5bGVDaGFuZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdzZWdtZW50JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3N0cm9rZUNvbG9yJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcENvbG9yXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1Nob3cgbWVhc3VyZW1lbnRzJyl9PC9MYWJlbD5cbiAgICAgICAgICAgICAgICAgICAgPENoZWNrYm94SW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2hvdy1sYWJlbHNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlVmFsdWVWaXNpYmlsdHkoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdzaG93TGFiZWxzJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZS50YXJnZXQuY2hlY2tlZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrZWQ9e3Nob3dMYWJlbHN9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8U3VibWVudT57dCgnUGFuZWwnKX08L1N1Ym1lbnU+XG4gICAgICAgICAgICAgICAgPElucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzQWxwaGFDaGFuZWw9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbD17dCgnQmFja2dyb3VuZCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3NvbGFyTW9kdWxlRmlsbENvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyh0ZW1wQ29sb3IpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVGFibGVTdHlsZUNoYW5nZShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3NvbGFyTW9kdWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2ZpbGxDb2xvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBDb2xvclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCb3JkZXInKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0FscGhhQ2hhbmVsPXt0cnVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3NvbGFyTW9kdWxlU3Ryb2tlQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVUYWJsZVN0eWxlQ2hhbmdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnc29sYXJNb2R1bGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnc3Ryb2tlQ29sb3InLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ29sb3JcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDwvQ29udGFpbmVyPlxuICAgICAgICA8Lz5cbiAgICApO1xufTtcblxuU2VnbWVudHNNYXBQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlU3R5bGVDaGFuZ2U6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I6IFByb3BUeXBlcy5ib29sLFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBTZWdtZW50c01hcFBhbmVsO1xuIl19