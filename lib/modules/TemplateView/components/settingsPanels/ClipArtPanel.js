"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toNumber = _interopRequireDefault(require("lodash/toNumber"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _rcSlider = _interopRequireDefault(require("rc-slider"));

var _rcTooltip = _interopRequireDefault(require("rc-tooltip"));

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _styled = require("../../../../components/styled");

var _Styles = require("./Styles");

var _jsxRuntime = require("react/jsx-runtime");

var _excluded = ["value", "dragging", "index"];

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledInput = _styledComponents.default.input(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background-color: #ffffff;\n    border-radius: 3px;\n    border: 1px solid #ecedf0;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    color: #848bab;\n    height: 46px;\n    padding: 6px 8px;\n    width: 133px;\n"])));

var SliderContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    width: 164px;\n\n    .rc-slider {\n        .rc-slider-tooltip {\n            z-index: 1000;\n        }\n\n        .rc-slider-track {\n            background-color: #f59300;\n        }\n\n        .rc-slider-handle {\n            border: solid 2px #ff9900;\n\n            &:hover {\n                border-color: #ff9a00;\n            }\n\n            &:active {\n                border-color: #ff9a00;\n                box-shadow: 0 0 5px #ff9a00;\n            }\n        }\n\n        .rc-slider-dot-active {\n            border-color: #ff9900;\n        }\n    }\n"])));

var ClipArtPanel = function ClipArtPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleStyleChange = _ref.handleStyleChange,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$type = style.type,
      type = _style$type === void 0 ? 'circle' : _style$type,
      _style$background = style.background,
      background = _style$background === void 0 ? '#000' : _style$background,
      _style$corner = style.corner,
      corner = _style$corner === void 0 ? 0 : _style$corner,
      _style$stroke = style.stroke,
      stroke = _style$stroke === void 0 ? '#000' : _style$stroke,
      _style$strokeWidth = style.strokeWidth,
      strokeWidth = _style$strokeWidth === void 0 ? 0 : _style$strokeWidth;
  var options = [{
    value: 'circle',
    label: t('Circle')
  }, {
    value: 'ellipse',
    label: t('Ellipse')
  }, {
    value: 'square',
    label: t('Square')
  }, {
    value: 'rectangle',
    label: t('Rectangle')
  }, {
    value: 'triangle',
    label: t('Triangle')
  }, {
    value: 'hexagon',
    label: t('Hexagon')
  }];

  var onAfterChange = function onAfterChange(value) {
    return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
      corner: value
    }));
  };

  var onChangeStrokeWidth = function onChangeStrokeWidth(event) {
    var numberValue = 0;

    if (!isNaN(event.target.value)) {
      numberValue = (0, _toNumber.default)(event.target.value);
    }

    handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
      strokeWidth: numberValue
    }));
  };

  var preventNumberInput = function preventNumberInput(e) {
    return e.preventDefault();
  };

  var handle = function handle(_ref2) {
    var value = _ref2.value,
        dragging = _ref2.dragging,
        index = _ref2.index,
        rest = _objectWithoutProperties(_ref2, _excluded);

    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcTooltip.default, {
      arrowContent: null,
      overlay: "".concat(value, "%"),
      placement: "top",
      prefixCls: "panel-image rc-tooltip",
      visible: dragging,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcSlider.default.Handle, _objectSpread({
        value: value
      }, rest))
    }, index);
  };

  var renderRoundedCornersSlider = function renderRoundedCornersSlider() {
    if (type !== 'square' && type !== 'rectangle') return null;
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Corner')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(SliderContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_rcSlider.default, {
          defaultValue: corner,
          handle: handle,
          marks: {
            0: '0%',
            100: '100%'
          },
          max: 100,
          min: 0,
          onAfterChange: onAfterChange,
          step: 10
        })
      })]
    });
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_styled.FlexColumn, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Type')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          as: "select",
          id: "select-theme",
          onChange: function onChange(e) {
            return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
              type: e.target.value
            }));
          },
          value: type,
          children: options.map(function (_ref3) {
            var label = _ref3.label,
                value = _ref3.value,
                optionDisabled = _ref3.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              disabled: optionDisabled,
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        hasAlphaChanel: true,
        label: t('Background'),
        onChange: function onChange(tempColor) {
          return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
            background: tempColor
          }));
        },
        value: background
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Styles.InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.Label, {
        children: t('Border')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInput, {
        min: "0",
        onChange: onChangeStrokeWidth,
        onKeyPress: preventNumberInput,
        type: "number",
        step: "any",
        value: strokeWidth
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Styles.InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: t('Border color'),
        onChange: function onChange(tempColor) {
          return handleStyleChange(_objectSpread(_objectSpread({}, style), {}, {
            stroke: tempColor
          }));
        },
        value: stroke
      })
    }), renderRoundedCornersSlider()]
  });
};

ClipArtPanel.propTypes = {
  handleStyleChange: _propTypes.default.func,
  style: _propTypes.default.object
};
var _default = ClipArtPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0NsaXBBcnRQYW5lbC5qcyJdLCJuYW1lcyI6WyJTdHlsZWRJbnB1dCIsInN0eWxlZCIsImlucHV0IiwiU2xpZGVyQ29udGFpbmVyIiwiZGl2IiwiQ2xpcEFydFBhbmVsIiwiaGFuZGxlU3R5bGVDaGFuZ2UiLCJzdHlsZSIsInQiLCJ0eXBlIiwiYmFja2dyb3VuZCIsImNvcm5lciIsInN0cm9rZSIsInN0cm9rZVdpZHRoIiwib3B0aW9ucyIsInZhbHVlIiwibGFiZWwiLCJvbkFmdGVyQ2hhbmdlIiwib25DaGFuZ2VTdHJva2VXaWR0aCIsImV2ZW50IiwibnVtYmVyVmFsdWUiLCJpc05hTiIsInRhcmdldCIsInByZXZlbnROdW1iZXJJbnB1dCIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImhhbmRsZSIsImRyYWdnaW5nIiwiaW5kZXgiLCJyZXN0IiwicmVuZGVyUm91bmRlZENvcm5lcnNTbGlkZXIiLCJtYXAiLCJvcHRpb25EaXNhYmxlZCIsImRpc2FibGVkIiwidGVtcENvbG9yIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsV0FBVyxHQUFHQywwQkFBT0MsS0FBViwyU0FBakI7O0FBV0EsSUFBTUMsZUFBZSxHQUFHRiwwQkFBT0csR0FBVixvcEJBQXJCOztBQWdDQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxHQUE0QztBQUFBLGlGQUFQLEVBQU87QUFBQSxNQUF6Q0MsaUJBQXlDLFFBQXpDQSxpQkFBeUM7QUFBQSx3QkFBdEJDLEtBQXNCO0FBQUEsTUFBdEJBLEtBQXNCLDJCQUFkLEVBQWM7O0FBQzdELHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxvQkFNSUQsS0FOSixDQUNJRSxJQURKO0FBQUEsTUFDSUEsSUFESiw0QkFDVyxRQURYO0FBQUEsMEJBTUlGLEtBTkosQ0FFSUcsVUFGSjtBQUFBLE1BRUlBLFVBRkosa0NBRWlCLE1BRmpCO0FBQUEsc0JBTUlILEtBTkosQ0FHSUksTUFISjtBQUFBLE1BR0lBLE1BSEosOEJBR2EsQ0FIYjtBQUFBLHNCQU1JSixLQU5KLENBSUlLLE1BSko7QUFBQSxNQUlJQSxNQUpKLDhCQUlhLE1BSmI7QUFBQSwyQkFNSUwsS0FOSixDQUtJTSxXQUxKO0FBQUEsTUFLSUEsV0FMSixtQ0FLa0IsQ0FMbEI7QUFPQSxNQUFNQyxPQUFPLEdBQUcsQ0FDWjtBQUFFQyxJQUFBQSxLQUFLLEVBQUUsUUFBVDtBQUFtQkMsSUFBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsUUFBRDtBQUEzQixHQURZLEVBRVo7QUFBRU8sSUFBQUEsS0FBSyxFQUFFLFNBQVQ7QUFBb0JDLElBQUFBLEtBQUssRUFBRVIsQ0FBQyxDQUFDLFNBQUQ7QUFBNUIsR0FGWSxFQUdaO0FBQUVPLElBQUFBLEtBQUssRUFBRSxRQUFUO0FBQW1CQyxJQUFBQSxLQUFLLEVBQUVSLENBQUMsQ0FBQyxRQUFEO0FBQTNCLEdBSFksRUFJWjtBQUFFTyxJQUFBQSxLQUFLLEVBQUUsV0FBVDtBQUFzQkMsSUFBQUEsS0FBSyxFQUFFUixDQUFDLENBQUMsV0FBRDtBQUE5QixHQUpZLEVBS1o7QUFBRU8sSUFBQUEsS0FBSyxFQUFFLFVBQVQ7QUFBcUJDLElBQUFBLEtBQUssRUFBRVIsQ0FBQyxDQUFDLFVBQUQ7QUFBN0IsR0FMWSxFQU1aO0FBQUVPLElBQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CQyxJQUFBQSxLQUFLLEVBQUVSLENBQUMsQ0FBQyxTQUFEO0FBQTVCLEdBTlksQ0FBaEI7O0FBU0EsTUFBTVMsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDRixLQUFEO0FBQUEsV0FDbEJULGlCQUFpQixpQ0FBTUMsS0FBTjtBQUFhSSxNQUFBQSxNQUFNLEVBQUVJO0FBQXJCLE9BREM7QUFBQSxHQUF0Qjs7QUFHQSxNQUFNRyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLEtBQUQsRUFBVztBQUNuQyxRQUFJQyxXQUFXLEdBQUcsQ0FBbEI7O0FBQ0EsUUFBSSxDQUFDQyxLQUFLLENBQUNGLEtBQUssQ0FBQ0csTUFBTixDQUFhUCxLQUFkLENBQVYsRUFBZ0M7QUFDNUJLLE1BQUFBLFdBQVcsR0FBRyx1QkFBU0QsS0FBSyxDQUFDRyxNQUFOLENBQWFQLEtBQXRCLENBQWQ7QUFDSDs7QUFDRFQsSUFBQUEsaUJBQWlCLGlDQUNWQyxLQURVO0FBRWJNLE1BQUFBLFdBQVcsRUFBRU87QUFGQSxPQUFqQjtBQUlILEdBVEQ7O0FBV0EsTUFBTUcsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxDQUFEO0FBQUEsV0FBT0EsQ0FBQyxDQUFDQyxjQUFGLEVBQVA7QUFBQSxHQUEzQjs7QUFFQSxNQUFNQyxNQUFNLEdBQUcsU0FBVEEsTUFBUztBQUFBLFFBQUdYLEtBQUgsU0FBR0EsS0FBSDtBQUFBLFFBQVVZLFFBQVYsU0FBVUEsUUFBVjtBQUFBLFFBQW9CQyxLQUFwQixTQUFvQkEsS0FBcEI7QUFBQSxRQUE4QkMsSUFBOUI7O0FBQUEsd0JBQ1gscUJBQUMsa0JBQUQ7QUFDSSxNQUFBLFlBQVksRUFBRSxJQURsQjtBQUdJLE1BQUEsT0FBTyxZQUFLZCxLQUFMLE1BSFg7QUFJSSxNQUFBLFNBQVMsRUFBQyxLQUpkO0FBS0ksTUFBQSxTQUFTLEVBQUMsd0JBTGQ7QUFNSSxNQUFBLE9BQU8sRUFBRVksUUFOYjtBQUFBLDZCQVFJLHFCQUFDLGlCQUFELENBQVEsTUFBUjtBQUFlLFFBQUEsS0FBSyxFQUFFWjtBQUF0QixTQUFpQ2MsSUFBakM7QUFSSixPQUVTRCxLQUZULENBRFc7QUFBQSxHQUFmOztBQWFBLE1BQU1FLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsR0FBTTtBQUNyQyxRQUFJckIsSUFBSSxLQUFLLFFBQVQsSUFBcUJBLElBQUksS0FBSyxXQUFsQyxFQUErQyxPQUFPLElBQVA7QUFFL0Msd0JBQ0ksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFELENBQUMsQ0FBQyxRQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLGVBQUQ7QUFBQSwrQkFDSSxxQkFBQyxpQkFBRDtBQUNJLFVBQUEsWUFBWSxFQUFFRyxNQURsQjtBQUVJLFVBQUEsTUFBTSxFQUFFZSxNQUZaO0FBR0ksVUFBQSxLQUFLLEVBQUU7QUFBRSxlQUFHLElBQUw7QUFBVyxpQkFBSztBQUFoQixXQUhYO0FBSUksVUFBQSxHQUFHLEVBQUUsR0FKVDtBQUtJLFVBQUEsR0FBRyxFQUFFLENBTFQ7QUFNSSxVQUFBLGFBQWEsRUFBRVQsYUFObkI7QUFPSSxVQUFBLElBQUksRUFBRTtBQVBWO0FBREosUUFGSjtBQUFBLE1BREo7QUFnQkgsR0FuQkQ7O0FBcUJBLHNCQUNJLHNCQUFDLGtCQUFEO0FBQUEsNEJBQ0ksc0JBQUMsc0JBQUQ7QUFBQSw4QkFDSSxxQkFBQyxhQUFEO0FBQUEsa0JBQVFULENBQUMsQ0FBQyxNQUFEO0FBQVQsUUFESixlQUVJLHFCQUFDLHdCQUFEO0FBQUEsK0JBQ0kscUJBQUMsYUFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksVUFBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLFVBQUEsUUFBUSxFQUFFLGtCQUFDZ0IsQ0FBRDtBQUFBLG1CQUNObEIsaUJBQWlCLGlDQUNWQyxLQURVO0FBRWJFLGNBQUFBLElBQUksRUFBRWUsQ0FBQyxDQUFDRixNQUFGLENBQVNQO0FBRkYsZUFEWDtBQUFBLFdBSGQ7QUFTSSxVQUFBLEtBQUssRUFBRU4sSUFUWDtBQUFBLG9CQVdLSyxPQUFPLENBQUNpQixHQUFSLENBQ0c7QUFBQSxnQkFBR2YsS0FBSCxTQUFHQSxLQUFIO0FBQUEsZ0JBQVVELEtBQVYsU0FBVUEsS0FBVjtBQUFBLGdCQUEyQmlCLGNBQTNCLFNBQWlCQyxRQUFqQjtBQUFBLGdDQUNJO0FBQ0ksY0FBQSxRQUFRLEVBQUVELGNBRGQ7QUFHSSxjQUFBLEtBQUssRUFBRWpCLEtBSFg7QUFBQSx3QkFLS0M7QUFMTCxnQ0FFbUJELEtBRm5CLGNBRTRCQyxLQUY1QixFQURKO0FBQUEsV0FESDtBQVhMO0FBREosUUFGSjtBQUFBLE1BREosZUE2QkkscUJBQUMsc0JBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsY0FBYyxFQUFFLElBRHBCO0FBRUksUUFBQSxLQUFLLEVBQUVSLENBQUMsQ0FBQyxZQUFELENBRlo7QUFHSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzBCLFNBQUQ7QUFBQSxpQkFDTjVCLGlCQUFpQixpQ0FBTUMsS0FBTjtBQUFhRyxZQUFBQSxVQUFVLEVBQUV3QjtBQUF6QixhQURYO0FBQUEsU0FIZDtBQU1JLFFBQUEsS0FBSyxFQUFFeEI7QUFOWDtBQURKLE1BN0JKLGVBdUNJLHNCQUFDLHNCQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUFBLGtCQUFRRixDQUFDLENBQUMsUUFBRDtBQUFULFFBREosZUFFSSxxQkFBQyxXQUFEO0FBQ0ksUUFBQSxHQUFHLEVBQUMsR0FEUjtBQUVJLFFBQUEsUUFBUSxFQUFFVSxtQkFGZDtBQUdJLFFBQUEsVUFBVSxFQUFFSyxrQkFIaEI7QUFJSSxRQUFBLElBQUksRUFBQyxRQUpUO0FBS0ksUUFBQSxJQUFJLEVBQUMsS0FMVDtBQU1JLFFBQUEsS0FBSyxFQUFFVjtBQU5YLFFBRko7QUFBQSxNQXZDSixlQWtESSxxQkFBQyxzQkFBRDtBQUFBLDZCQUNJLHFCQUFDLHlCQUFEO0FBQ0ksUUFBQSxLQUFLLEVBQUVMLENBQUMsQ0FBQyxjQUFELENBRFo7QUFFSSxRQUFBLFFBQVEsRUFBRSxrQkFBQzBCLFNBQUQ7QUFBQSxpQkFDTjVCLGlCQUFpQixpQ0FBTUMsS0FBTjtBQUFhSyxZQUFBQSxNQUFNLEVBQUVzQjtBQUFyQixhQURYO0FBQUEsU0FGZDtBQUtJLFFBQUEsS0FBSyxFQUFFdEI7QUFMWDtBQURKLE1BbERKLEVBNERLa0IsMEJBQTBCLEVBNUQvQjtBQUFBLElBREo7QUFnRUgsQ0FwSUQ7O0FBc0lBekIsWUFBWSxDQUFDOEIsU0FBYixHQUF5QjtBQUNyQjdCLEVBQUFBLGlCQUFpQixFQUFFOEIsbUJBQVVDLElBRFI7QUFFckI5QixFQUFBQSxLQUFLLEVBQUU2QixtQkFBVUU7QUFGSSxDQUF6QjtlQUtlakMsWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB0b051bWJlciBmcm9tICdsb2Rhc2gvdG9OdW1iZXInO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBTbGlkZXIgZnJvbSAncmMtc2xpZGVyJztcbmltcG9ydCBUb29sdGlwIGZyb20gJ3JjLXRvb2x0aXAnO1xuaW1wb3J0IEZvcm0gZnJvbSAncmVhY3QtYm9vdHN0cmFwL0Zvcm0nO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgQ29sb3JQaWNrZXJJbnB1dCBmcm9tICdAY29tcG9uZW50cy9Db2xvclBpY2tlcklucHV0JztcbmltcG9ydCB7IEZsZXhDb2x1bW4gfSBmcm9tICdAY29tcG9uZW50cy9zdHlsZWQnO1xuXG5pbXBvcnQgeyBJbnB1dENvbnRhaW5lciwgTGFiZWwsIFN0eWxlZElucHV0R3JvdXAgfSBmcm9tICcuL1N0eWxlcyc7XG5cbmNvbnN0IFN0eWxlZElucHV0ID0gc3R5bGVkLmlucHV0YFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlY2VkZjA7XG4gICAgYm94LXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMTI5LCAxNTgsIDIwMCwgMC4wNik7XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIHBhZGRpbmc6IDZweCA4cHg7XG4gICAgd2lkdGg6IDEzM3B4O1xuYDtcblxuY29uc3QgU2xpZGVyQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxNjRweDtcblxuICAgIC5yYy1zbGlkZXIge1xuICAgICAgICAucmMtc2xpZGVyLXRvb2x0aXAge1xuICAgICAgICAgICAgei1pbmRleDogMTAwMDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5yYy1zbGlkZXItdHJhY2sge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2Y1OTMwMDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5yYy1zbGlkZXItaGFuZGxlIHtcbiAgICAgICAgICAgIGJvcmRlcjogc29saWQgMnB4ICNmZjk5MDA7XG5cbiAgICAgICAgICAgICY6aG92ZXIge1xuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2ZmOWEwMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJjphY3RpdmUge1xuICAgICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2ZmOWEwMDtcbiAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAwIDAgNXB4ICNmZjlhMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAucmMtc2xpZGVyLWRvdC1hY3RpdmUge1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAjZmY5OTAwO1xuICAgICAgICB9XG4gICAgfVxuYDtcblxuY29uc3QgQ2xpcEFydFBhbmVsID0gKHsgaGFuZGxlU3R5bGVDaGFuZ2UsIHN0eWxlID0ge30gfSA9IHt9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHtcbiAgICAgICAgdHlwZSA9ICdjaXJjbGUnLFxuICAgICAgICBiYWNrZ3JvdW5kID0gJyMwMDAnLFxuICAgICAgICBjb3JuZXIgPSAwLFxuICAgICAgICBzdHJva2UgPSAnIzAwMCcsXG4gICAgICAgIHN0cm9rZVdpZHRoID0gMCxcbiAgICB9ID0gc3R5bGU7XG4gICAgY29uc3Qgb3B0aW9ucyA9IFtcbiAgICAgICAgeyB2YWx1ZTogJ2NpcmNsZScsIGxhYmVsOiB0KCdDaXJjbGUnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnZWxsaXBzZScsIGxhYmVsOiB0KCdFbGxpcHNlJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ3NxdWFyZScsIGxhYmVsOiB0KCdTcXVhcmUnKSB9LFxuICAgICAgICB7IHZhbHVlOiAncmVjdGFuZ2xlJywgbGFiZWw6IHQoJ1JlY3RhbmdsZScpIH0sXG4gICAgICAgIHsgdmFsdWU6ICd0cmlhbmdsZScsIGxhYmVsOiB0KCdUcmlhbmdsZScpIH0sXG4gICAgICAgIHsgdmFsdWU6ICdoZXhhZ29uJywgbGFiZWw6IHQoJ0hleGFnb24nKSB9LFxuICAgIF07XG5cbiAgICBjb25zdCBvbkFmdGVyQ2hhbmdlID0gKHZhbHVlKSA9PlxuICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7IC4uLnN0eWxlLCBjb3JuZXI6IHZhbHVlIH0pO1xuXG4gICAgY29uc3Qgb25DaGFuZ2VTdHJva2VXaWR0aCA9IChldmVudCkgPT4ge1xuICAgICAgICBsZXQgbnVtYmVyVmFsdWUgPSAwO1xuICAgICAgICBpZiAoIWlzTmFOKGV2ZW50LnRhcmdldC52YWx1ZSkpIHtcbiAgICAgICAgICAgIG51bWJlclZhbHVlID0gdG9OdW1iZXIoZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZSh7XG4gICAgICAgICAgICAuLi5zdHlsZSxcbiAgICAgICAgICAgIHN0cm9rZVdpZHRoOiBudW1iZXJWYWx1ZSxcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIGNvbnN0IHByZXZlbnROdW1iZXJJbnB1dCA9IChlKSA9PiBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBjb25zdCBoYW5kbGUgPSAoeyB2YWx1ZSwgZHJhZ2dpbmcsIGluZGV4LCAuLi5yZXN0IH0pID0+IChcbiAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgIGFycm93Q29udGVudD17bnVsbH1cbiAgICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgICBvdmVybGF5PXtgJHt2YWx1ZX0lYH1cbiAgICAgICAgICAgIHBsYWNlbWVudD1cInRvcFwiXG4gICAgICAgICAgICBwcmVmaXhDbHM9XCJwYW5lbC1pbWFnZSByYy10b29sdGlwXCJcbiAgICAgICAgICAgIHZpc2libGU9e2RyYWdnaW5nfVxuICAgICAgICA+XG4gICAgICAgICAgICA8U2xpZGVyLkhhbmRsZSB2YWx1ZT17dmFsdWV9IHsuLi5yZXN0fSAvPlxuICAgICAgICA8L1Rvb2x0aXA+XG4gICAgKTtcblxuICAgIGNvbnN0IHJlbmRlclJvdW5kZWRDb3JuZXJzU2xpZGVyID0gKCkgPT4ge1xuICAgICAgICBpZiAodHlwZSAhPT0gJ3NxdWFyZScgJiYgdHlwZSAhPT0gJ3JlY3RhbmdsZScpIHJldHVybiBudWxsO1xuXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdDb3JuZXInKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxTbGlkZXJDb250YWluZXI+XG4gICAgICAgICAgICAgICAgICAgIDxTbGlkZXJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17Y29ybmVyfVxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlPXtoYW5kbGV9XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrcz17eyAwOiAnMCUnLCAxMDA6ICcxMDAlJyB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4PXsxMDB9XG4gICAgICAgICAgICAgICAgICAgICAgICBtaW49ezB9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkFmdGVyQ2hhbmdlPXtvbkFmdGVyQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgc3RlcD17MTB9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9TbGlkZXJDb250YWluZXI+XG4gICAgICAgICAgICA8L0lucHV0Q29udGFpbmVyPlxuICAgICAgICApO1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8RmxleENvbHVtbj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1R5cGUnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC10aGVtZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlU3R5bGVDaGFuZ2Uoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi5zdHlsZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogZS50YXJnZXQudmFsdWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0eXBlfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7b3B0aW9ucy5tYXAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKHsgbGFiZWwsIHZhbHVlLCBkaXNhYmxlZDogb3B0aW9uRGlzYWJsZWQgfSkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17b3B0aW9uRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BzZWxlY3QtJHt2YWx1ZX0tJHtsYWJlbH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7bGFiZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgIDwvRm9ybS5Db250cm9sPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgaGFzQWxwaGFDaGFuZWw9e3RydWV9XG4gICAgICAgICAgICAgICAgICAgIGxhYmVsPXt0KCdCYWNrZ3JvdW5kJyl9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlU3R5bGVDaGFuZ2UoeyAuLi5zdHlsZSwgYmFja2dyb3VuZDogdGVtcENvbG9yIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e2JhY2tncm91bmR9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPExhYmVsPnt0KCdCb3JkZXInKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dFxuICAgICAgICAgICAgICAgICAgICBtaW49XCIwXCJcbiAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlU3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAgICAgICAgIG9uS2V5UHJlc3M9e3ByZXZlbnROdW1iZXJJbnB1dH1cbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cIm51bWJlclwiXG4gICAgICAgICAgICAgICAgICAgIHN0ZXA9XCJhbnlcIlxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17c3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICA8SW5wdXRDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPENvbG9yUGlja2VySW5wdXRcbiAgICAgICAgICAgICAgICAgICAgbGFiZWw9e3QoJ0JvcmRlciBjb2xvcicpfVxuICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KHRlbXBDb2xvcikgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlQ2hhbmdlKHsgLi4uc3R5bGUsIHN0cm9rZTogdGVtcENvbG9yIH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3N0cm9rZX1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cblxuICAgICAgICAgICAge3JlbmRlclJvdW5kZWRDb3JuZXJzU2xpZGVyKCl9XG4gICAgICAgIDwvRmxleENvbHVtbj5cbiAgICApO1xufTtcblxuQ2xpcEFydFBhbmVsLnByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVTdHlsZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDbGlwQXJ0UGFuZWw7XG4iXX0=