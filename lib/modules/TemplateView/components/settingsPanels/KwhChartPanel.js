"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ColorPickerInput = _interopRequireDefault(require("../../../../components/ColorPickerInput"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n"])));

var StyledInputGroup = (0, _styledComponents.default)(_reactBootstrap.InputGroup)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 46px;\n    width: 133px;\n\n    & select {\n        background-color: #ffffff;\n        border-radius: 3px;\n        border: 1px solid #ecedf0;\n        height: 46px;\n        width: 133px;\n    }\n"])));

var InputContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    margin: 4px 0;\n"])));

var Label = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var KwhChartPanel = function KwhChartPanel() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      handleValueChange = _ref.handleValueChange,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _value$colors = value.colors,
      colors = _value$colors === void 0 ? ['#000'] : _value$colors,
      _value$type = value.type,
      type = _value$type === void 0 ? 'bar' : _value$type;
  var options = [{
    value: 'bar',
    label: t('Bars')
  }, {
    value: 'line',
    label: t('Line')
  }, {
    value: 'area',
    label: t('Area')
  }];

  var onChangeColor = function onChangeColor(tempColors) {
    return handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      colors: tempColors
    }));
  };

  var onChangeType = function onChangeType(e) {
    return handleValueChange(_objectSpread(_objectSpread({}, value), {}, {
      type: e.target.value
    }));
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(InputContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
        children: t('Type')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, {
          as: "select",
          id: "select-theme",
          onChange: onChangeType,
          value: type,
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              disabled: optionDisabled,
              value: value,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InputContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ColorPickerInput.default, {
        label: "".concat(t('Color'), " 1"),
        onChange: function onChange(tempColor) {
          return onChangeColor([tempColor, colors[1]]);
        },
        value: colors[0]
      })
    })]
  });
};

KwhChartPanel.propTypes = {
  handleValueChange: _propTypes.default.func,
  value: _propTypes.default.string
};
var _default = KwhChartPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3NldHRpbmdzUGFuZWxzL0t3aENoYXJ0UGFuZWwuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiU3R5bGVkSW5wdXRHcm91cCIsIklucHV0R3JvdXAiLCJJbnB1dENvbnRhaW5lciIsIkxhYmVsIiwic3BhbiIsIkt3aENoYXJ0UGFuZWwiLCJoYW5kbGVWYWx1ZUNoYW5nZSIsInZhbHVlIiwidCIsImNvbG9ycyIsInR5cGUiLCJvcHRpb25zIiwibGFiZWwiLCJvbkNoYW5nZUNvbG9yIiwidGVtcENvbG9ycyIsIm9uQ2hhbmdlVHlwZSIsImUiLCJ0YXJnZXQiLCJtYXAiLCJvcHRpb25EaXNhYmxlZCIsImRpc2FibGVkIiwidGVtcENvbG9yIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiZnVuYyIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBVix3SEFBZjs7QUFLQSxJQUFNQyxnQkFBZ0IsR0FBRywrQkFBT0MsMEJBQVAsQ0FBSCwyUkFBdEI7O0FBYUEsSUFBTUMsY0FBYyxHQUFHSiwwQkFBT0MsR0FBViwySUFBcEI7O0FBTUEsSUFBTUksS0FBSyxHQUFHTCwwQkFBT00sSUFBVix1T0FBWDs7QUFVQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQTRDO0FBQUEsaUZBQVAsRUFBTztBQUFBLE1BQXpDQyxpQkFBeUMsUUFBekNBLGlCQUF5QztBQUFBLHdCQUF0QkMsS0FBc0I7QUFBQSxNQUF0QkEsS0FBc0IsMkJBQWQsRUFBYzs7QUFDOUQsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUE0Q0QsS0FBNUMsQ0FBUUUsTUFBUjtBQUFBLE1BQVFBLE1BQVIsOEJBQWlCLENBQUMsTUFBRCxDQUFqQjtBQUFBLG9CQUE0Q0YsS0FBNUMsQ0FBMkJHLElBQTNCO0FBQUEsTUFBMkJBLElBQTNCLDRCQUFrQyxLQUFsQztBQUNBLE1BQU1DLE9BQU8sR0FBRyxDQUNaO0FBQUVKLElBQUFBLEtBQUssRUFBRSxLQUFUO0FBQWdCSyxJQUFBQSxLQUFLLEVBQUVKLENBQUMsQ0FBQyxNQUFEO0FBQXhCLEdBRFksRUFFWjtBQUFFRCxJQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQkssSUFBQUEsS0FBSyxFQUFFSixDQUFDLENBQUMsTUFBRDtBQUF6QixHQUZZLEVBR1o7QUFBRUQsSUFBQUEsS0FBSyxFQUFFLE1BQVQ7QUFBaUJLLElBQUFBLEtBQUssRUFBRUosQ0FBQyxDQUFDLE1BQUQ7QUFBekIsR0FIWSxDQUFoQjs7QUFNQSxNQUFNSyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNDLFVBQUQ7QUFBQSxXQUNsQlIsaUJBQWlCLGlDQUFNQyxLQUFOO0FBQWFFLE1BQUFBLE1BQU0sRUFBRUs7QUFBckIsT0FEQztBQUFBLEdBQXRCOztBQUdBLE1BQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLENBQUQ7QUFBQSxXQUNqQlYsaUJBQWlCLGlDQUFNQyxLQUFOO0FBQWFHLE1BQUFBLElBQUksRUFBRU0sQ0FBQyxDQUFDQyxNQUFGLENBQVNWO0FBQTVCLE9BREE7QUFBQSxHQUFyQjs7QUFHQSxzQkFDSSxzQkFBQyxTQUFEO0FBQUEsNEJBQ0ksc0JBQUMsY0FBRDtBQUFBLDhCQUNJLHFCQUFDLEtBQUQ7QUFBQSxrQkFBUUMsQ0FBQyxDQUFDLE1BQUQ7QUFBVCxRQURKLGVBRUkscUJBQUMsZ0JBQUQ7QUFBQSwrQkFDSSxxQkFBQyxvQkFBRCxDQUFNLE9BQU47QUFDSSxVQUFBLEVBQUUsRUFBQyxRQURQO0FBRUksVUFBQSxFQUFFLEVBQUMsY0FGUDtBQUdJLFVBQUEsUUFBUSxFQUFFTyxZQUhkO0FBSUksVUFBQSxLQUFLLEVBQUVMLElBSlg7QUFBQSxvQkFNS0MsT0FBTyxDQUFDTyxHQUFSLENBQ0c7QUFBQSxnQkFBR04sS0FBSCxTQUFHQSxLQUFIO0FBQUEsZ0JBQVVMLEtBQVYsU0FBVUEsS0FBVjtBQUFBLGdCQUEyQlksY0FBM0IsU0FBaUJDLFFBQWpCO0FBQUEsZ0NBQ0k7QUFDSSxjQUFBLFFBQVEsRUFBRUQsY0FEZDtBQUdJLGNBQUEsS0FBSyxFQUFFWixLQUhYO0FBQUEsd0JBS0tLO0FBTEwsZ0NBRW1CTCxLQUZuQixjQUU0QkssS0FGNUIsRUFESjtBQUFBLFdBREg7QUFOTDtBQURKLFFBRko7QUFBQSxNQURKLGVBd0JJLHFCQUFDLGNBQUQ7QUFBQSw2QkFDSSxxQkFBQyx5QkFBRDtBQUNJLFFBQUEsS0FBSyxZQUFLSixDQUFDLENBQUMsT0FBRCxDQUFOLE9BRFQ7QUFFSSxRQUFBLFFBQVEsRUFBRSxrQkFBQ2EsU0FBRDtBQUFBLGlCQUNOUixhQUFhLENBQUMsQ0FBQ1EsU0FBRCxFQUFZWixNQUFNLENBQUMsQ0FBRCxDQUFsQixDQUFELENBRFA7QUFBQSxTQUZkO0FBS0ksUUFBQSxLQUFLLEVBQUVBLE1BQU0sQ0FBQyxDQUFEO0FBTGpCO0FBREosTUF4Qko7QUFBQSxJQURKO0FBb0NILENBbkREOztBQXFEQUosYUFBYSxDQUFDaUIsU0FBZCxHQUEwQjtBQUN0QmhCLEVBQUFBLGlCQUFpQixFQUFFaUIsbUJBQVVDLElBRFA7QUFFdEJqQixFQUFBQSxLQUFLLEVBQUVnQixtQkFBVUU7QUFGSyxDQUExQjtlQUtlcEIsYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBGb3JtLCBJbnB1dEdyb3VwIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IENvbG9yUGlja2VySW5wdXQgZnJvbSAnQGNvbXBvbmVudHMvQ29sb3JQaWNrZXJJbnB1dCc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuYDtcblxuY29uc3QgU3R5bGVkSW5wdXRHcm91cCA9IHN0eWxlZChJbnB1dEdyb3VwKWBcbiAgICBoZWlnaHQ6IDQ2cHg7XG4gICAgd2lkdGg6IDEzM3B4O1xuXG4gICAgJiBzZWxlY3Qge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlY2VkZjA7XG4gICAgICAgIGhlaWdodDogNDZweDtcbiAgICAgICAgd2lkdGg6IDEzM3B4O1xuICAgIH1cbmA7XG5cbmNvbnN0IElucHV0Q29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWFyZ2luOiA0cHggMDtcbmA7XG5cbmNvbnN0IExhYmVsID0gc3R5bGVkLnNwYW5gXG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1yaWdodDogMjNweDtcbiAgICBtaW4taGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHdpZHRoOiAxMzVweDtcbmA7XG5cbmNvbnN0IEt3aENoYXJ0UGFuZWwgPSAoeyBoYW5kbGVWYWx1ZUNoYW5nZSwgdmFsdWUgPSB7fSB9ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3QgeyBjb2xvcnMgPSBbJyMwMDAnXSwgdHlwZSA9ICdiYXInIH0gPSB2YWx1ZTtcbiAgICBjb25zdCBvcHRpb25zID0gW1xuICAgICAgICB7IHZhbHVlOiAnYmFyJywgbGFiZWw6IHQoJ0JhcnMnKSB9LFxuICAgICAgICB7IHZhbHVlOiAnbGluZScsIGxhYmVsOiB0KCdMaW5lJykgfSxcbiAgICAgICAgeyB2YWx1ZTogJ2FyZWEnLCBsYWJlbDogdCgnQXJlYScpIH0sXG4gICAgXTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlQ29sb3IgPSAodGVtcENvbG9ycykgPT5cbiAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2UoeyAuLi52YWx1ZSwgY29sb3JzOiB0ZW1wQ29sb3JzIH0pO1xuXG4gICAgY29uc3Qgb25DaGFuZ2VUeXBlID0gKGUpID0+XG4gICAgICAgIGhhbmRsZVZhbHVlQ2hhbmdlKHsgLi4udmFsdWUsIHR5cGU6IGUudGFyZ2V0LnZhbHVlIH0pO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8TGFiZWw+e3QoJ1R5cGUnKX08L0xhYmVsPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInNlbGVjdC10aGVtZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2VUeXBlfVxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e3R5cGV9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIHtvcHRpb25zLm1hcChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoeyBsYWJlbCwgdmFsdWUsIGRpc2FibGVkOiBvcHRpb25EaXNhYmxlZCB9KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtvcHRpb25EaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke3ZhbHVlfS0ke2xhYmVsfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtsYWJlbH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgPC9JbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxJbnB1dENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8Q29sb3JQaWNrZXJJbnB1dFxuICAgICAgICAgICAgICAgICAgICBsYWJlbD17YCR7dCgnQ29sb3InKX0gMWB9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsodGVtcENvbG9yKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2VDb2xvcihbdGVtcENvbG9yLCBjb2xvcnNbMV1dKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHZhbHVlPXtjb2xvcnNbMF19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvSW5wdXRDb250YWluZXI+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5Ld2hDaGFydFBhbmVsLnByb3BUeXBlcyA9IHtcbiAgICBoYW5kbGVWYWx1ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBLd2hDaGFydFBhbmVsO1xuIl19