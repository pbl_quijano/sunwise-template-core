"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactDraggable = _interopRequireDefault(require("react-draggable"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _TitleWithDetail = _interopRequireDefault(require("../../../components/TitleWithDetail"));

var _types = require("../../../constants/types");

var _helpers = require("../helpers");

var _SettingsPanelContent = _interopRequireDefault(require("./SettingsPanelContent"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    flex-basis: 0;\n    flex-grow: 1;\n    z-index: 2;\n"])));

var Content = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-basis: 0;\n    flex-direction: column;\n    flex-grow: 1;\n    overflow-y: auto;\n    padding: 16px 24px;\n    width: 100%;\n"])));

var HeadSection = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    border-bottom: 1px solid rgba(0, 0, 0, 0.05);\n    cursor: move;\n    display: flex;\n    flex-direction: column;\n    padding: 24px 24px 16px 24px;\n"])));

var BackDrop = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    cursor: pointer;\n    height: 100%;\n    left: 0;\n    position: absolute;\n    top: 0;\n    width: 100%;\n    z-index: 1;\n"])));

var SettingsContainer = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    height: 100%;\n    left: 0;\n    position: fixed;\n    top: 0;\n    width: 100%;\n    z-index: 999;\n"])));

var SettingsSection = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    background-color: #ffffff;\n    border-radius: 4px;\n    border: 1px solid #ecedf0;\n    box-shadow: 10px 12px 32px 0 rgba(0, 0, 0, 0.3);\n    display: flex;\n    flex-direction: column;\n    height: 500px;\n    width: 396px;\n    z-index: 2;\n    position: absolute;\n"])));

var StyledTitleWithDetail = (0, _styledComponents.default)(_TitleWithDetail.default)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    text-transform: capitalize;\n"])));

var CloseButton = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n    background-color: rgba(0, 0, 0, 0.8);\n    border-radius: 50%;\n    color: #fff;\n    cursor: pointer;\n    font-size: 12px;\n    height: 20px;\n    line-height: 20px;\n    position: absolute;\n    right: -10px;\n    text-align: center;\n    top: -10px;\n    width: 20px;\n    z-index: 3;\n"])));

var SettingsPanel = /*#__PURE__*/(0, _react.forwardRef)(function (_ref, ref) {
  var catalogs = _ref.catalogs,
      chartWidgetsNames = _ref.chartWidgetsNames,
      proposalSelectorDisabled = _ref.proposalSelectorDisabled,
      setWidgetStyle = _ref.setWidgetStyle,
      setWidgetValue = _ref.setWidgetValue,
      tableWidgetsNames = _ref.tableWidgetsNames,
      templateType = _ref.templateType;

  var _useState = (0, _react.useState)({
    show: false
  }),
      _useState2 = _slicedToArray(_useState, 2),
      SettingsData = _useState2[0],
      setSettingsData = _useState2[1];

  (0, _react.useImperativeHandle)(ref, function () {
    return {
      openSettingsPanel: function openSettingsPanel(widget, event) {
        var limitY = window.innerHeight - 500;
        var limitX = window.innerWidth - 396;
        setSettingsData({
          currentValue: widget.value,
          currentStyle: widget.style,
          hasSummarySupport: (0, _helpers.getHasSummarySupport)(widget),
          id: widget.id,
          name: widget.field,
          type: widget.name,
          position: {
            x: event.clientX > limitX ? limitX : event.clientX,
            y: event.clientY > limitY ? limitY : event.clientY
          },
          show: true
        });
      }
    };
  });

  var _handleValueChange = function _handleValueChange(tempValue) {
    if (SettingsData.id) {
      setSettingsData(_objectSpread(_objectSpread({}, SettingsData), {}, {
        currentValue: tempValue
      }));
      setWidgetValue(SettingsData.id, tempValue);
    }
  };

  var _handleStyleChange = function _handleStyleChange(tempStyle) {
    if (SettingsData.id) {
      setSettingsData(_objectSpread(_objectSpread({}, SettingsData), {}, {
        currentStyle: tempStyle
      }));
      setWidgetStyle(SettingsData.id, tempStyle);
    }
  };

  var closeSettingsPanel = function closeSettingsPanel() {
    setSettingsData({
      show: false
    });
  };

  if (!SettingsData.show) return null;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(SettingsContainer, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(BackDrop, {
      onClick: function onClick() {
        return closeSettingsPanel();
      }
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactDraggable.default, {
      defaultPosition: SettingsData.position,
      bounds: "parent",
      handle: ".handle-section",
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(SettingsSection, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(CloseButton, {
          onClick: function onClick() {
            return closeSettingsPanel();
          },
          children: "x"
        }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(Wrapper, {
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(HeadSection, {
            className: "handle-section",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledTitleWithDetail, {
              children: SettingsData.name
            })
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Content, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_SettingsPanelContent.default, {
              catalogs: catalogs,
              chartWidgetsNames: chartWidgetsNames,
              handleStyleChange: _handleStyleChange,
              handleValueChange: _handleValueChange,
              hasProposalSelector: templateType === _types.MULTIPROPOSAL_TYPE && SettingsData.hasSummarySupport,
              name: SettingsData.name,
              proposalSelectorDisabled: proposalSelectorDisabled,
              style: SettingsData.currentStyle,
              tableWidgetsNames: tableWidgetsNames,
              type: SettingsData.type,
              value: SettingsData.currentValue
            })
          })]
        })]
      })
    })]
  });
});
SettingsPanel.propTypes = {
  catalogs: _propTypes.default.array,
  chartWidgetsNames: _propTypes.default.array,
  proposalSelectorDisabled: _propTypes.default.bool,
  setWidgetStyle: _propTypes.default.func,
  setWidgetValue: _propTypes.default.func,
  tableWidgetsNames: _propTypes.default.array,
  templateType: _propTypes.default.number
};
SettingsPanel.displayName = 'SettingsPanel';
var _default = SettingsPanel;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1NldHRpbmdzUGFuZWwuanMiXSwibmFtZXMiOlsiV3JhcHBlciIsInN0eWxlZCIsImRpdiIsIkNvbnRlbnQiLCJIZWFkU2VjdGlvbiIsIkJhY2tEcm9wIiwiU2V0dGluZ3NDb250YWluZXIiLCJTZXR0aW5nc1NlY3Rpb24iLCJTdHlsZWRUaXRsZVdpdGhEZXRhaWwiLCJUaXRsZVdpdGhEZXRhaWwiLCJDbG9zZUJ1dHRvbiIsIlNldHRpbmdzUGFuZWwiLCJyZWYiLCJjYXRhbG9ncyIsImNoYXJ0V2lkZ2V0c05hbWVzIiwicHJvcG9zYWxTZWxlY3RvckRpc2FibGVkIiwic2V0V2lkZ2V0U3R5bGUiLCJzZXRXaWRnZXRWYWx1ZSIsInRhYmxlV2lkZ2V0c05hbWVzIiwidGVtcGxhdGVUeXBlIiwic2hvdyIsIlNldHRpbmdzRGF0YSIsInNldFNldHRpbmdzRGF0YSIsIm9wZW5TZXR0aW5nc1BhbmVsIiwid2lkZ2V0IiwiZXZlbnQiLCJsaW1pdFkiLCJ3aW5kb3ciLCJpbm5lckhlaWdodCIsImxpbWl0WCIsImlubmVyV2lkdGgiLCJjdXJyZW50VmFsdWUiLCJ2YWx1ZSIsImN1cnJlbnRTdHlsZSIsInN0eWxlIiwiaGFzU3VtbWFyeVN1cHBvcnQiLCJpZCIsIm5hbWUiLCJmaWVsZCIsInR5cGUiLCJwb3NpdGlvbiIsIngiLCJjbGllbnRYIiwieSIsImNsaWVudFkiLCJfaGFuZGxlVmFsdWVDaGFuZ2UiLCJ0ZW1wVmFsdWUiLCJfaGFuZGxlU3R5bGVDaGFuZ2UiLCJ0ZW1wU3R5bGUiLCJjbG9zZVNldHRpbmdzUGFuZWwiLCJNVUxUSVBST1BPU0FMX1RZUEUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsImJvb2wiLCJmdW5jIiwibnVtYmVyIiwiZGlzcGxheU5hbWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLE9BQU8sR0FBR0MsMEJBQU9DLEdBQVYsZ0xBQWI7O0FBUUEsSUFBTUMsT0FBTyxHQUFHRiwwQkFBT0MsR0FBVixtT0FBYjs7QUFVQSxJQUFNRSxXQUFXLEdBQUdILDBCQUFPQyxHQUFWLG1PQUFqQjs7QUFRQSxJQUFNRyxRQUFRLEdBQUdKLDBCQUFPQyxHQUFWLHlNQUFkOztBQVVBLElBQU1JLGlCQUFpQixHQUFHTCwwQkFBT0MsR0FBVixrTEFBdkI7O0FBU0EsSUFBTUssZUFBZSxHQUFHTiwwQkFBT0MsR0FBViwwVkFBckI7O0FBYUEsSUFBTU0scUJBQXFCLEdBQUcsK0JBQU9DLHdCQUFQLENBQUgsMEdBQTNCOztBQUlBLElBQU1DLFdBQVcsR0FBR1QsMEJBQU9DLEdBQVYsK1dBQWpCOztBQWdCQSxJQUFNUyxhQUFhLGdCQUFHLHVCQUNsQixnQkFVSUMsR0FWSixFQVdLO0FBQUEsTUFUR0MsUUFTSCxRQVRHQSxRQVNIO0FBQUEsTUFSR0MsaUJBUUgsUUFSR0EsaUJBUUg7QUFBQSxNQVBHQyx3QkFPSCxRQVBHQSx3QkFPSDtBQUFBLE1BTkdDLGNBTUgsUUFOR0EsY0FNSDtBQUFBLE1BTEdDLGNBS0gsUUFMR0EsY0FLSDtBQUFBLE1BSkdDLGlCQUlILFFBSkdBLGlCQUlIO0FBQUEsTUFIR0MsWUFHSCxRQUhHQSxZQUdIOztBQUNELGtCQUF3QyxxQkFBUztBQUM3Q0MsSUFBQUEsSUFBSSxFQUFFO0FBRHVDLEdBQVQsQ0FBeEM7QUFBQTtBQUFBLE1BQU9DLFlBQVA7QUFBQSxNQUFxQkMsZUFBckI7O0FBSUEsa0NBQW9CVixHQUFwQixFQUF5QjtBQUFBLFdBQU87QUFDNUJXLE1BQUFBLGlCQUQ0Qiw2QkFDVkMsTUFEVSxFQUNGQyxLQURFLEVBQ0s7QUFDN0IsWUFBTUMsTUFBTSxHQUFHQyxNQUFNLENBQUNDLFdBQVAsR0FBcUIsR0FBcEM7QUFDQSxZQUFNQyxNQUFNLEdBQUdGLE1BQU0sQ0FBQ0csVUFBUCxHQUFvQixHQUFuQztBQUNBUixRQUFBQSxlQUFlLENBQUM7QUFDWlMsVUFBQUEsWUFBWSxFQUFFUCxNQUFNLENBQUNRLEtBRFQ7QUFFWkMsVUFBQUEsWUFBWSxFQUFFVCxNQUFNLENBQUNVLEtBRlQ7QUFHWkMsVUFBQUEsaUJBQWlCLEVBQUUsbUNBQXFCWCxNQUFyQixDQUhQO0FBSVpZLFVBQUFBLEVBQUUsRUFBRVosTUFBTSxDQUFDWSxFQUpDO0FBS1pDLFVBQUFBLElBQUksRUFBRWIsTUFBTSxDQUFDYyxLQUxEO0FBTVpDLFVBQUFBLElBQUksRUFBRWYsTUFBTSxDQUFDYSxJQU5EO0FBT1pHLFVBQUFBLFFBQVEsRUFBRTtBQUNOQyxZQUFBQSxDQUFDLEVBQUVoQixLQUFLLENBQUNpQixPQUFOLEdBQWdCYixNQUFoQixHQUF5QkEsTUFBekIsR0FBa0NKLEtBQUssQ0FBQ2lCLE9BRHJDO0FBRU5DLFlBQUFBLENBQUMsRUFBRWxCLEtBQUssQ0FBQ21CLE9BQU4sR0FBZ0JsQixNQUFoQixHQUF5QkEsTUFBekIsR0FBa0NELEtBQUssQ0FBQ21CO0FBRnJDLFdBUEU7QUFXWnhCLFVBQUFBLElBQUksRUFBRTtBQVhNLFNBQUQsQ0FBZjtBQWFIO0FBakIyQixLQUFQO0FBQUEsR0FBekI7O0FBb0JBLE1BQU15QixrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFNBQUQsRUFBZTtBQUN0QyxRQUFJekIsWUFBWSxDQUFDZSxFQUFqQixFQUFxQjtBQUNqQmQsTUFBQUEsZUFBZSxpQ0FBTUQsWUFBTjtBQUFvQlUsUUFBQUEsWUFBWSxFQUFFZTtBQUFsQyxTQUFmO0FBQ0E3QixNQUFBQSxjQUFjLENBQUNJLFlBQVksQ0FBQ2UsRUFBZCxFQUFrQlUsU0FBbEIsQ0FBZDtBQUNIO0FBQ0osR0FMRDs7QUFPQSxNQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFNBQUQsRUFBZTtBQUN0QyxRQUFJM0IsWUFBWSxDQUFDZSxFQUFqQixFQUFxQjtBQUNqQmQsTUFBQUEsZUFBZSxpQ0FBTUQsWUFBTjtBQUFvQlksUUFBQUEsWUFBWSxFQUFFZTtBQUFsQyxTQUFmO0FBQ0FoQyxNQUFBQSxjQUFjLENBQUNLLFlBQVksQ0FBQ2UsRUFBZCxFQUFrQlksU0FBbEIsQ0FBZDtBQUNIO0FBQ0osR0FMRDs7QUFPQSxNQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLEdBQU07QUFDN0IzQixJQUFBQSxlQUFlLENBQUM7QUFBRUYsTUFBQUEsSUFBSSxFQUFFO0FBQVIsS0FBRCxDQUFmO0FBQ0gsR0FGRDs7QUFJQSxNQUFJLENBQUNDLFlBQVksQ0FBQ0QsSUFBbEIsRUFBd0IsT0FBTyxJQUFQO0FBRXhCLHNCQUNJLHNCQUFDLGlCQUFEO0FBQUEsNEJBQ0kscUJBQUMsUUFBRDtBQUFVLE1BQUEsT0FBTyxFQUFFO0FBQUEsZUFBTTZCLGtCQUFrQixFQUF4QjtBQUFBO0FBQW5CLE1BREosZUFFSSxxQkFBQyx1QkFBRDtBQUNJLE1BQUEsZUFBZSxFQUFFNUIsWUFBWSxDQUFDbUIsUUFEbEM7QUFFSSxNQUFBLE1BQU0sRUFBQyxRQUZYO0FBR0ksTUFBQSxNQUFNLEVBQUMsaUJBSFg7QUFBQSw2QkFLSSxzQkFBQyxlQUFEO0FBQUEsZ0NBQ0kscUJBQUMsV0FBRDtBQUFhLFVBQUEsT0FBTyxFQUFFO0FBQUEsbUJBQU1TLGtCQUFrQixFQUF4QjtBQUFBLFdBQXRCO0FBQUE7QUFBQSxVQURKLGVBSUksc0JBQUMsT0FBRDtBQUFBLGtDQUNJLHFCQUFDLFdBQUQ7QUFBYSxZQUFBLFNBQVMsRUFBQyxnQkFBdkI7QUFBQSxtQ0FDSSxxQkFBQyxxQkFBRDtBQUFBLHdCQUNLNUIsWUFBWSxDQUFDZ0I7QUFEbEI7QUFESixZQURKLGVBT0kscUJBQUMsT0FBRDtBQUFBLG1DQUNJLHFCQUFDLDZCQUFEO0FBQ0ksY0FBQSxRQUFRLEVBQUV4QixRQURkO0FBRUksY0FBQSxpQkFBaUIsRUFBRUMsaUJBRnZCO0FBR0ksY0FBQSxpQkFBaUIsRUFBRWlDLGtCQUh2QjtBQUlJLGNBQUEsaUJBQWlCLEVBQUVGLGtCQUp2QjtBQUtJLGNBQUEsbUJBQW1CLEVBQ2YxQixZQUFZLEtBQUsrQix5QkFBakIsSUFDQTdCLFlBQVksQ0FBQ2MsaUJBUHJCO0FBU0ksY0FBQSxJQUFJLEVBQUVkLFlBQVksQ0FBQ2dCLElBVHZCO0FBVUksY0FBQSx3QkFBd0IsRUFDcEJ0Qix3QkFYUjtBQWFJLGNBQUEsS0FBSyxFQUFFTSxZQUFZLENBQUNZLFlBYnhCO0FBY0ksY0FBQSxpQkFBaUIsRUFBRWYsaUJBZHZCO0FBZUksY0FBQSxJQUFJLEVBQUVHLFlBQVksQ0FBQ2tCLElBZnZCO0FBZ0JJLGNBQUEsS0FBSyxFQUFFbEIsWUFBWSxDQUFDVTtBQWhCeEI7QUFESixZQVBKO0FBQUEsVUFKSjtBQUFBO0FBTEosTUFGSjtBQUFBLElBREo7QUE0Q0gsQ0FyR2lCLENBQXRCO0FBd0dBcEIsYUFBYSxDQUFDd0MsU0FBZCxHQUEwQjtBQUN0QnRDLEVBQUFBLFFBQVEsRUFBRXVDLG1CQUFVQyxLQURFO0FBRXRCdkMsRUFBQUEsaUJBQWlCLEVBQUVzQyxtQkFBVUMsS0FGUDtBQUd0QnRDLEVBQUFBLHdCQUF3QixFQUFFcUMsbUJBQVVFLElBSGQ7QUFJdEJ0QyxFQUFBQSxjQUFjLEVBQUVvQyxtQkFBVUcsSUFKSjtBQUt0QnRDLEVBQUFBLGNBQWMsRUFBRW1DLG1CQUFVRyxJQUxKO0FBTXRCckMsRUFBQUEsaUJBQWlCLEVBQUVrQyxtQkFBVUMsS0FOUDtBQU90QmxDLEVBQUFBLFlBQVksRUFBRWlDLG1CQUFVSTtBQVBGLENBQTFCO0FBVUE3QyxhQUFhLENBQUM4QyxXQUFkLEdBQTRCLGVBQTVCO2VBRWU5QyxhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVN0YXRlLCBmb3J3YXJkUmVmLCB1c2VJbXBlcmF0aXZlSGFuZGxlIH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IERyYWdnYWJsZSBmcm9tICdyZWFjdC1kcmFnZ2FibGUnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBUaXRsZVdpdGhEZXRhaWwgZnJvbSAnQGNvbXBvbmVudHMvVGl0bGVXaXRoRGV0YWlsJztcblxuaW1wb3J0IHsgTVVMVElQUk9QT1NBTF9UWVBFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmltcG9ydCB7IGdldEhhc1N1bW1hcnlTdXBwb3J0IH0gZnJvbSAnLi4vaGVscGVycyc7XG5cbmltcG9ydCBTZXR0aW5nc1BhbmVsQ29udGVudCBmcm9tICcuL1NldHRpbmdzUGFuZWxDb250ZW50JztcblxuY29uc3QgV3JhcHBlciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGZsZXgtYmFzaXM6IDA7XG4gICAgZmxleC1ncm93OiAxO1xuICAgIHotaW5kZXg6IDI7XG5gO1xuXG5jb25zdCBDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtYmFzaXM6IDA7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmbGV4LWdyb3c6IDE7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBwYWRkaW5nOiAxNnB4IDI0cHg7XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBIZWFkU2VjdGlvbiA9IHN0eWxlZC5kaXZgXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4wNSk7XG4gICAgY3Vyc29yOiBtb3ZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBwYWRkaW5nOiAyNHB4IDI0cHggMTZweCAyNHB4O1xuYDtcblxuY29uc3QgQmFja0Ryb3AgPSBzdHlsZWQuZGl2YFxuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbGVmdDogMDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHotaW5kZXg6IDE7XG5gO1xuXG5jb25zdCBTZXR0aW5nc0NvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGxlZnQ6IDA7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB6LWluZGV4OiA5OTk7XG5gO1xuXG5jb25zdCBTZXR0aW5nc1NlY3Rpb24gPSBzdHlsZWQuZGl2YFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlY2VkZjA7XG4gICAgYm94LXNoYWRvdzogMTBweCAxMnB4IDMycHggMCByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGhlaWdodDogNTAwcHg7XG4gICAgd2lkdGg6IDM5NnB4O1xuICAgIHotaW5kZXg6IDI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuYDtcblxuY29uc3QgU3R5bGVkVGl0bGVXaXRoRGV0YWlsID0gc3R5bGVkKFRpdGxlV2l0aERldGFpbClgXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG5gO1xuXG5jb25zdCBDbG9zZUJ1dHRvbiA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLCAwLCAwLCAwLjgpO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IC0xMHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB0b3A6IC0xMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIHotaW5kZXg6IDM7XG5gO1xuXG5jb25zdCBTZXR0aW5nc1BhbmVsID0gZm9yd2FyZFJlZihcbiAgICAoXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNhdGFsb2dzLFxuICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXMsXG4gICAgICAgICAgICBwcm9wb3NhbFNlbGVjdG9yRGlzYWJsZWQsXG4gICAgICAgICAgICBzZXRXaWRnZXRTdHlsZSxcbiAgICAgICAgICAgIHNldFdpZGdldFZhbHVlLFxuICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXMsXG4gICAgICAgICAgICB0ZW1wbGF0ZVR5cGUsXG4gICAgICAgIH0sXG4gICAgICAgIHJlZlxuICAgICkgPT4ge1xuICAgICAgICBjb25zdCBbU2V0dGluZ3NEYXRhLCBzZXRTZXR0aW5nc0RhdGFdID0gdXNlU3RhdGUoe1xuICAgICAgICAgICAgc2hvdzogZmFsc2UsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHVzZUltcGVyYXRpdmVIYW5kbGUocmVmLCAoKSA9PiAoe1xuICAgICAgICAgICAgb3BlblNldHRpbmdzUGFuZWwod2lkZ2V0LCBldmVudCkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGxpbWl0WSA9IHdpbmRvdy5pbm5lckhlaWdodCAtIDUwMDtcbiAgICAgICAgICAgICAgICBjb25zdCBsaW1pdFggPSB3aW5kb3cuaW5uZXJXaWR0aCAtIDM5NjtcbiAgICAgICAgICAgICAgICBzZXRTZXR0aW5nc0RhdGEoe1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VmFsdWU6IHdpZGdldC52YWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFN0eWxlOiB3aWRnZXQuc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0OiBnZXRIYXNTdW1tYXJ5U3VwcG9ydCh3aWRnZXQpLFxuICAgICAgICAgICAgICAgICAgICBpZDogd2lkZ2V0LmlkLFxuICAgICAgICAgICAgICAgICAgICBuYW1lOiB3aWRnZXQuZmllbGQsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IHdpZGdldC5uYW1lLFxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgeDogZXZlbnQuY2xpZW50WCA+IGxpbWl0WCA/IGxpbWl0WCA6IGV2ZW50LmNsaWVudFgsXG4gICAgICAgICAgICAgICAgICAgICAgICB5OiBldmVudC5jbGllbnRZID4gbGltaXRZID8gbGltaXRZIDogZXZlbnQuY2xpZW50WSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgc2hvdzogdHJ1ZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH0pKTtcblxuICAgICAgICBjb25zdCBfaGFuZGxlVmFsdWVDaGFuZ2UgPSAodGVtcFZhbHVlKSA9PiB7XG4gICAgICAgICAgICBpZiAoU2V0dGluZ3NEYXRhLmlkKSB7XG4gICAgICAgICAgICAgICAgc2V0U2V0dGluZ3NEYXRhKHsgLi4uU2V0dGluZ3NEYXRhLCBjdXJyZW50VmFsdWU6IHRlbXBWYWx1ZSB9KTtcbiAgICAgICAgICAgICAgICBzZXRXaWRnZXRWYWx1ZShTZXR0aW5nc0RhdGEuaWQsIHRlbXBWYWx1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgY29uc3QgX2hhbmRsZVN0eWxlQ2hhbmdlID0gKHRlbXBTdHlsZSkgPT4ge1xuICAgICAgICAgICAgaWYgKFNldHRpbmdzRGF0YS5pZCkge1xuICAgICAgICAgICAgICAgIHNldFNldHRpbmdzRGF0YSh7IC4uLlNldHRpbmdzRGF0YSwgY3VycmVudFN0eWxlOiB0ZW1wU3R5bGUgfSk7XG4gICAgICAgICAgICAgICAgc2V0V2lkZ2V0U3R5bGUoU2V0dGluZ3NEYXRhLmlkLCB0ZW1wU3R5bGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIGNvbnN0IGNsb3NlU2V0dGluZ3NQYW5lbCA9ICgpID0+IHtcbiAgICAgICAgICAgIHNldFNldHRpbmdzRGF0YSh7IHNob3c6IGZhbHNlIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIGlmICghU2V0dGluZ3NEYXRhLnNob3cpIHJldHVybiBudWxsO1xuXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8U2V0dGluZ3NDb250YWluZXI+XG4gICAgICAgICAgICAgICAgPEJhY2tEcm9wIG9uQ2xpY2s9eygpID0+IGNsb3NlU2V0dGluZ3NQYW5lbCgpfSAvPlxuICAgICAgICAgICAgICAgIDxEcmFnZ2FibGVcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFBvc2l0aW9uPXtTZXR0aW5nc0RhdGEucG9zaXRpb259XG4gICAgICAgICAgICAgICAgICAgIGJvdW5kcz1cInBhcmVudFwiXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZT1cIi5oYW5kbGUtc2VjdGlvblwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8U2V0dGluZ3NTZWN0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENsb3NlQnV0dG9uIG9uQ2xpY2s9eygpID0+IGNsb3NlU2V0dGluZ3NQYW5lbCgpfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB4XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0Nsb3NlQnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFdyYXBwZXI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEhlYWRTZWN0aW9uIGNsYXNzTmFtZT1cImhhbmRsZS1zZWN0aW9uXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdHlsZWRUaXRsZVdpdGhEZXRhaWw+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7U2V0dGluZ3NEYXRhLm5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3R5bGVkVGl0bGVXaXRoRGV0YWlsPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvSGVhZFNlY3Rpb24+XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Q29udGVudD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFNldHRpbmdzUGFuZWxDb250ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRhbG9ncz17Y2F0YWxvZ3N9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFydFdpZGdldHNOYW1lcz17Y2hhcnRXaWRnZXRzTmFtZXN9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVTdHlsZUNoYW5nZT17X2hhbmRsZVN0eWxlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlVmFsdWVDaGFuZ2U9e19oYW5kbGVWYWx1ZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc1Byb3Bvc2FsU2VsZWN0b3I9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVHlwZSA9PT0gTVVMVElQUk9QT1NBTF9UWVBFICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgU2V0dGluZ3NEYXRhLmhhc1N1bW1hcnlTdXBwb3J0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPXtTZXR0aW5nc0RhdGEubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZD17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxTZWxlY3RvckRpc2FibGVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17U2V0dGluZ3NEYXRhLmN1cnJlbnRTdHlsZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzPXt0YWJsZVdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9e1NldHRpbmdzRGF0YS50eXBlfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU9e1NldHRpbmdzRGF0YS5jdXJyZW50VmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Db250ZW50PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9XcmFwcGVyPlxuICAgICAgICAgICAgICAgICAgICA8L1NldHRpbmdzU2VjdGlvbj5cbiAgICAgICAgICAgICAgICA8L0RyYWdnYWJsZT5cbiAgICAgICAgICAgIDwvU2V0dGluZ3NDb250YWluZXI+XG4gICAgICAgICk7XG4gICAgfVxuKTtcblxuU2V0dGluZ3NQYW5lbC5wcm9wVHlwZXMgPSB7XG4gICAgY2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2V0V2lkZ2V0U3R5bGU6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldFdpZGdldFZhbHVlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICB0YWJsZVdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHRlbXBsYXRlVHlwZTogUHJvcFR5cGVzLm51bWJlcixcbn07XG5cblNldHRpbmdzUGFuZWwuZGlzcGxheU5hbWUgPSAnU2V0dGluZ3NQYW5lbCc7XG5cbmV4cG9ydCBkZWZhdWx0IFNldHRpbmdzUGFuZWw7XG4iXX0=