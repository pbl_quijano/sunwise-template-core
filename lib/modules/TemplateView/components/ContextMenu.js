"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _types = require("../../../constants/types");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var MenuContainer = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background-color: #ffffff;\n    border-radius: 3px;\n    border: 0.5px solid #d3d7eb;\n    box-shadow: 0 2px 24px 0 rgba(13, 13, 54, 0.06);\n    left: 0;\n    padding: 5px;\n    position: fixed;\n    top: 0;\n    transform: translate(\n        ", "px,\n        ", "px\n    );\n    z-index: 999;\n"])), function (_ref) {
  var _ref$posX = _ref.posX,
      posX = _ref$posX === void 0 ? 0 : _ref$posX;
  return posX;
}, function (_ref2) {
  var _ref2$posY = _ref2.posY,
      posY = _ref2$posY === void 0 ? 0 : _ref2$posY;
  return posY;
});

var MenuItemContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n"])));

var MenuItem = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    background-color: transparent;\n    border-radius: 15px;\n    border: 0;\n    color: #848bab;\n    cursor: pointer;\n    display: block;\n    font-size: 12px;\n    line-height: 14px;\n    padding: 6px 16px 7px 16px;\n    white-space: nowrap;\n    width: 100%;\n\n    &:hover {\n        background-color: #f5f5f5;\n    }\n    &.disabled {\n        opacity: 0.5;\n        cursor: default;\n        pointer-events: none;\n    }\n"])));

var OptionIcon = _styledComponents.default.i(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 12px;\n    line-height: 12px;\n    margin-right: 9px;\n    min-height: 10px;\n"])));

var ContextMenu = function ContextMenu(_ref3) {
  var copiedWidget = _ref3.copiedWidget,
      editionLevel = _ref3.editionLevel,
      handleOpenSettings = _ref3.handleOpenSettings,
      handleRemove = _ref3.handleRemove,
      isSelectedItem = _ref3.isSelectedItem,
      onClose = _ref3.onClose,
      handleCopyWidget = _ref3.handleCopyWidget,
      handlePasteElement = _ref3.handlePasteElement,
      openSettingsDisabled = _ref3.openSettingsDisabled,
      x = _ref3.x,
      y = _ref3.y;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var onSelectOption = function onSelectOption(action) {
    action();
    onClose();
  };

  var handleCut = function handleCut() {
    handleCopyWidget();
    handleRemove();
  };

  var renderMenu = function renderMenu() {
    var deleteAndCopyDisabled = editionLevel === _types.PARTIAL_EDITION_MODE;

    if (!isSelectedItem) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(MenuItemContainer, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
          className: (deleteAndCopyDisabled || (0, _isNil.default)(copiedWidget)) && 'disabled',
          onClick: function onClick() {
            return onSelectOption(handlePasteElement);
          },
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
            className: "fas fa-paste"
          }), " ", t('Paste')]
        })
      });
    }

    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItemContainer, {
      children: [!openSettingsDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
        onClick: function onClick(e) {
          return onSelectOption(function () {
            return handleOpenSettings(e);
          });
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
          className: "fa fa-cog"
        }), " ", t('Set up')]
      }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
        onClick: function onClick() {
          return onSelectOption(handleCopyWidget);
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
          className: "fas fa-copy"
        }), " ", t('Copy')]
      }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
        onClick: function onClick() {
          return onSelectOption(handleCut);
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
          className: "fas fa-cut"
        }), " ", t('Cut')]
      }), !deleteAndCopyDisabled && !(0, _isNil.default)(copiedWidget) && /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
        onClick: function onClick() {
          return onSelectOption(handlePasteElement);
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
          className: "fas fa-paste"
        }), " ", t('Paste')]
      }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuItem, {
        onClick: function onClick() {
          return onSelectOption(handleRemove);
        },
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
          className: "fas fa-trash"
        }), " ", t('Delete')]
      })]
    });
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(MenuContainer, {
    posX: x,
    posY: y,
    onContextMenu: function onContextMenu(e) {
      return e.preventDefault();
    },
    children: renderMenu()
  });
};

ContextMenu.propTypes = {
  copiedWidget: _propTypes.default.object,
  editionLevel: _propTypes.default.string,
  handleOpenSettings: _propTypes.default.func,
  handleRemove: _propTypes.default.func,
  isSelectedItem: _propTypes.default.bool,
  onClose: _propTypes.default.func,
  handleCopyWidget: _propTypes.default.func,
  handlePasteElement: _propTypes.default.func,
  openSettingsDisabled: _propTypes.default.bool,
  x: _propTypes.default.number,
  y: _propTypes.default.number
};
var _default = ContextMenu;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0NvbnRleHRNZW51LmpzIl0sIm5hbWVzIjpbIk1lbnVDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJwb3NYIiwicG9zWSIsIk1lbnVJdGVtQ29udGFpbmVyIiwiTWVudUl0ZW0iLCJPcHRpb25JY29uIiwiaSIsIkNvbnRleHRNZW51IiwiY29waWVkV2lkZ2V0IiwiZWRpdGlvbkxldmVsIiwiaGFuZGxlT3BlblNldHRpbmdzIiwiaGFuZGxlUmVtb3ZlIiwiaXNTZWxlY3RlZEl0ZW0iLCJvbkNsb3NlIiwiaGFuZGxlQ29weVdpZGdldCIsImhhbmRsZVBhc3RlRWxlbWVudCIsIm9wZW5TZXR0aW5nc0Rpc2FibGVkIiwieCIsInkiLCJ0Iiwib25TZWxlY3RPcHRpb24iLCJhY3Rpb24iLCJoYW5kbGVDdXQiLCJyZW5kZXJNZW51IiwiZGVsZXRlQW5kQ29weURpc2FibGVkIiwiUEFSVElBTF9FRElUSU9OX01PREUiLCJlIiwicHJldmVudERlZmF1bHQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJzdHJpbmciLCJmdW5jIiwiYm9vbCIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsYUFBYSxHQUFHQywwQkFBT0MsR0FBVixvWEFVVDtBQUFBLHVCQUFHQyxJQUFIO0FBQUEsTUFBR0EsSUFBSCwwQkFBVSxDQUFWO0FBQUEsU0FBa0JBLElBQWxCO0FBQUEsQ0FWUyxFQVdUO0FBQUEseUJBQUdDLElBQUg7QUFBQSxNQUFHQSxJQUFILDJCQUFVLENBQVY7QUFBQSxTQUFrQkEsSUFBbEI7QUFBQSxDQVhTLENBQW5COztBQWVBLElBQU1DLGlCQUFpQixHQUFHSiwwQkFBT0MsR0FBViw0SUFBdkI7O0FBS0EsSUFBTUksUUFBUSxHQUFHTCwwQkFBT0MsR0FBVix1ZkFBZDs7QUF1QkEsSUFBTUssVUFBVSxHQUFHTiwwQkFBT08sQ0FBViwyTEFBaEI7O0FBUUEsSUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsUUFZZDtBQUFBLE1BWEZDLFlBV0UsU0FYRkEsWUFXRTtBQUFBLE1BVkZDLFlBVUUsU0FWRkEsWUFVRTtBQUFBLE1BVEZDLGtCQVNFLFNBVEZBLGtCQVNFO0FBQUEsTUFSRkMsWUFRRSxTQVJGQSxZQVFFO0FBQUEsTUFQRkMsY0FPRSxTQVBGQSxjQU9FO0FBQUEsTUFORkMsT0FNRSxTQU5GQSxPQU1FO0FBQUEsTUFMRkMsZ0JBS0UsU0FMRkEsZ0JBS0U7QUFBQSxNQUpGQyxrQkFJRSxTQUpGQSxrQkFJRTtBQUFBLE1BSEZDLG9CQUdFLFNBSEZBLG9CQUdFO0FBQUEsTUFGRkMsQ0FFRSxTQUZGQSxDQUVFO0FBQUEsTUFERkMsQ0FDRSxTQURGQSxDQUNFOztBQUNGLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxNQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLE1BQUQsRUFBWTtBQUMvQkEsSUFBQUEsTUFBTTtBQUNOUixJQUFBQSxPQUFPO0FBQ1YsR0FIRDs7QUFLQSxNQUFNUyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQUFNO0FBQ3BCUixJQUFBQSxnQkFBZ0I7QUFDaEJILElBQUFBLFlBQVk7QUFDZixHQUhEOztBQUtBLE1BQU1ZLFVBQVUsR0FBRyxTQUFiQSxVQUFhLEdBQU07QUFDckIsUUFBTUMscUJBQXFCLEdBQUdmLFlBQVksS0FBS2dCLDJCQUEvQzs7QUFDQSxRQUFJLENBQUNiLGNBQUwsRUFBcUI7QUFDakIsMEJBQ0kscUJBQUMsaUJBQUQ7QUFBQSwrQkFDSSxzQkFBQyxRQUFEO0FBQ0ksVUFBQSxTQUFTLEVBQ0wsQ0FBQ1kscUJBQXFCLElBQUksb0JBQU1oQixZQUFOLENBQTFCLEtBQ0EsVUFIUjtBQUtJLFVBQUEsT0FBTyxFQUFFO0FBQUEsbUJBQU1ZLGNBQWMsQ0FBQ0wsa0JBQUQsQ0FBcEI7QUFBQSxXQUxiO0FBQUEsa0NBT0kscUJBQUMsVUFBRDtBQUFZLFlBQUEsU0FBUyxFQUFDO0FBQXRCLFlBUEosT0FPNkNJLENBQUMsQ0FBQyxPQUFELENBUDlDO0FBQUE7QUFESixRQURKO0FBYUg7O0FBQ0Qsd0JBQ0ksc0JBQUMsaUJBQUQ7QUFBQSxpQkFDSyxDQUFDSCxvQkFBRCxpQkFDRyxzQkFBQyxRQUFEO0FBQ0ksUUFBQSxPQUFPLEVBQUUsaUJBQUNVLENBQUQ7QUFBQSxpQkFDTE4sY0FBYyxDQUFDO0FBQUEsbUJBQU1WLGtCQUFrQixDQUFDZ0IsQ0FBRCxDQUF4QjtBQUFBLFdBQUQsQ0FEVDtBQUFBLFNBRGI7QUFBQSxnQ0FLSSxxQkFBQyxVQUFEO0FBQVksVUFBQSxTQUFTLEVBQUM7QUFBdEIsVUFMSixPQUswQ1AsQ0FBQyxDQUFDLFFBQUQsQ0FMM0M7QUFBQSxRQUZSLEVBV0ssQ0FBQ0sscUJBQUQsaUJBQ0csc0JBQUMsUUFBRDtBQUFVLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQU1KLGNBQWMsQ0FBQ04sZ0JBQUQsQ0FBcEI7QUFBQSxTQUFuQjtBQUFBLGdDQUNJLHFCQUFDLFVBQUQ7QUFBWSxVQUFBLFNBQVMsRUFBQztBQUF0QixVQURKLE9BQzRDSyxDQUFDLENBQUMsTUFBRCxDQUQ3QztBQUFBLFFBWlIsRUFnQkssQ0FBQ0sscUJBQUQsaUJBQ0csc0JBQUMsUUFBRDtBQUFVLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQU1KLGNBQWMsQ0FBQ0UsU0FBRCxDQUFwQjtBQUFBLFNBQW5CO0FBQUEsZ0NBQ0kscUJBQUMsVUFBRDtBQUFZLFVBQUEsU0FBUyxFQUFDO0FBQXRCLFVBREosT0FDMkNILENBQUMsQ0FBQyxLQUFELENBRDVDO0FBQUEsUUFqQlIsRUFxQkssQ0FBQ0sscUJBQUQsSUFBMEIsQ0FBQyxvQkFBTWhCLFlBQU4sQ0FBM0IsaUJBQ0csc0JBQUMsUUFBRDtBQUNJLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQU1ZLGNBQWMsQ0FBQ0wsa0JBQUQsQ0FBcEI7QUFBQSxTQURiO0FBQUEsZ0NBR0kscUJBQUMsVUFBRDtBQUFZLFVBQUEsU0FBUyxFQUFDO0FBQXRCLFVBSEosT0FHNkNJLENBQUMsQ0FBQyxPQUFELENBSDlDO0FBQUEsUUF0QlIsRUE0QkssQ0FBQ0sscUJBQUQsaUJBQ0csc0JBQUMsUUFBRDtBQUFVLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQU1KLGNBQWMsQ0FBQ1QsWUFBRCxDQUFwQjtBQUFBLFNBQW5CO0FBQUEsZ0NBQ0kscUJBQUMsVUFBRDtBQUFZLFVBQUEsU0FBUyxFQUFDO0FBQXRCLFVBREosT0FDNkNRLENBQUMsQ0FBQyxRQUFELENBRDlDO0FBQUEsUUE3QlI7QUFBQSxNQURKO0FBb0NILEdBckREOztBQXVEQSxzQkFDSSxxQkFBQyxhQUFEO0FBQ0ksSUFBQSxJQUFJLEVBQUVGLENBRFY7QUFFSSxJQUFBLElBQUksRUFBRUMsQ0FGVjtBQUdJLElBQUEsYUFBYSxFQUFFLHVCQUFDUSxDQUFEO0FBQUEsYUFBT0EsQ0FBQyxDQUFDQyxjQUFGLEVBQVA7QUFBQSxLQUhuQjtBQUFBLGNBS0tKLFVBQVU7QUFMZixJQURKO0FBU0gsQ0F4RkQ7O0FBMEZBaEIsV0FBVyxDQUFDcUIsU0FBWixHQUF3QjtBQUNwQnBCLEVBQUFBLFlBQVksRUFBRXFCLG1CQUFVQyxNQURKO0FBRXBCckIsRUFBQUEsWUFBWSxFQUFFb0IsbUJBQVVFLE1BRko7QUFHcEJyQixFQUFBQSxrQkFBa0IsRUFBRW1CLG1CQUFVRyxJQUhWO0FBSXBCckIsRUFBQUEsWUFBWSxFQUFFa0IsbUJBQVVHLElBSko7QUFLcEJwQixFQUFBQSxjQUFjLEVBQUVpQixtQkFBVUksSUFMTjtBQU1wQnBCLEVBQUFBLE9BQU8sRUFBRWdCLG1CQUFVRyxJQU5DO0FBT3BCbEIsRUFBQUEsZ0JBQWdCLEVBQUVlLG1CQUFVRyxJQVBSO0FBUXBCakIsRUFBQUEsa0JBQWtCLEVBQUVjLG1CQUFVRyxJQVJWO0FBU3BCaEIsRUFBQUEsb0JBQW9CLEVBQUVhLG1CQUFVSSxJQVRaO0FBVXBCaEIsRUFBQUEsQ0FBQyxFQUFFWSxtQkFBVUssTUFWTztBQVdwQmhCLEVBQUFBLENBQUMsRUFBRVcsbUJBQVVLO0FBWE8sQ0FBeEI7ZUFjZTNCLFciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IFBBUlRJQUxfRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmNvbnN0IE1lbnVDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIGJvcmRlcjogMC41cHggc29saWQgI2QzZDdlYjtcbiAgICBib3gtc2hhZG93OiAwIDJweCAyNHB4IDAgcmdiYSgxMywgMTMsIDU0LCAwLjA2KTtcbiAgICBsZWZ0OiAwO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKFxuICAgICAgICAkeyh7IHBvc1ggPSAwIH0pID0+IHBvc1h9cHgsXG4gICAgICAgICR7KHsgcG9zWSA9IDAgfSkgPT4gcG9zWX1weFxuICAgICk7XG4gICAgei1pbmRleDogOTk5O1xuYDtcbmNvbnN0IE1lbnVJdGVtQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuY29uc3QgTWVudUl0ZW0gPSBzdHlsZWQuZGl2YFxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiAwO1xuICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgcGFkZGluZzogNnB4IDE2cHggN3B4IDE2cHg7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICB3aWR0aDogMTAwJTtcblxuICAgICY6aG92ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xuICAgIH1cbiAgICAmLmRpc2FibGVkIHtcbiAgICAgICAgb3BhY2l0eTogMC41O1xuICAgICAgICBjdXJzb3I6IGRlZmF1bHQ7XG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICAgIH1cbmA7XG5cbmNvbnN0IE9wdGlvbkljb24gPSBzdHlsZWQuaWBcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDEycHg7XG4gICAgbWFyZ2luLXJpZ2h0OiA5cHg7XG4gICAgbWluLWhlaWdodDogMTBweDtcbmA7XG5cbmNvbnN0IENvbnRleHRNZW51ID0gKHtcbiAgICBjb3BpZWRXaWRnZXQsXG4gICAgZWRpdGlvbkxldmVsLFxuICAgIGhhbmRsZU9wZW5TZXR0aW5ncyxcbiAgICBoYW5kbGVSZW1vdmUsXG4gICAgaXNTZWxlY3RlZEl0ZW0sXG4gICAgb25DbG9zZSxcbiAgICBoYW5kbGVDb3B5V2lkZ2V0LFxuICAgIGhhbmRsZVBhc3RlRWxlbWVudCxcbiAgICBvcGVuU2V0dGluZ3NEaXNhYmxlZCxcbiAgICB4LFxuICAgIHksXG59KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IG9uU2VsZWN0T3B0aW9uID0gKGFjdGlvbikgPT4ge1xuICAgICAgICBhY3Rpb24oKTtcbiAgICAgICAgb25DbG9zZSgpO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVDdXQgPSAoKSA9PiB7XG4gICAgICAgIGhhbmRsZUNvcHlXaWRnZXQoKTtcbiAgICAgICAgaGFuZGxlUmVtb3ZlKCk7XG4gICAgfTtcblxuICAgIGNvbnN0IHJlbmRlck1lbnUgPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGRlbGV0ZUFuZENvcHlEaXNhYmxlZCA9IGVkaXRpb25MZXZlbCA9PT0gUEFSVElBTF9FRElUSU9OX01PREU7XG4gICAgICAgIGlmICghaXNTZWxlY3RlZEl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgPE1lbnVJdGVtQ29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8TWVudUl0ZW1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGRlbGV0ZUFuZENvcHlEaXNhYmxlZCB8fCBpc05pbChjb3BpZWRXaWRnZXQpKSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdkaXNhYmxlZCdcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IG9uU2VsZWN0T3B0aW9uKGhhbmRsZVBhc3RlRWxlbWVudCl9XG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxPcHRpb25JY29uIGNsYXNzTmFtZT1cImZhcyBmYS1wYXN0ZVwiIC8+IHt0KCdQYXN0ZScpfVxuICAgICAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxuICAgICAgICAgICAgICAgIDwvTWVudUl0ZW1Db250YWluZXI+XG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8TWVudUl0ZW1Db250YWluZXI+XG4gICAgICAgICAgICAgICAgeyFvcGVuU2V0dGluZ3NEaXNhYmxlZCAmJiAoXG4gICAgICAgICAgICAgICAgICAgIDxNZW51SXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGUpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25TZWxlY3RPcHRpb24oKCkgPT4gaGFuZGxlT3BlblNldHRpbmdzKGUpKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8T3B0aW9uSWNvbiBjbGFzc05hbWU9XCJmYSBmYS1jb2dcIiAvPiB7dCgnU2V0IHVwJyl9XG4gICAgICAgICAgICAgICAgICAgIDwvTWVudUl0ZW0+XG4gICAgICAgICAgICAgICAgKX1cblxuICAgICAgICAgICAgICAgIHshZGVsZXRlQW5kQ29weURpc2FibGVkICYmIChcbiAgICAgICAgICAgICAgICAgICAgPE1lbnVJdGVtIG9uQ2xpY2s9eygpID0+IG9uU2VsZWN0T3B0aW9uKGhhbmRsZUNvcHlXaWRnZXQpfT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxPcHRpb25JY29uIGNsYXNzTmFtZT1cImZhcyBmYS1jb3B5XCIgLz4ge3QoJ0NvcHknKX1cbiAgICAgICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHshZGVsZXRlQW5kQ29weURpc2FibGVkICYmIChcbiAgICAgICAgICAgICAgICAgICAgPE1lbnVJdGVtIG9uQ2xpY2s9eygpID0+IG9uU2VsZWN0T3B0aW9uKGhhbmRsZUN1dCl9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPE9wdGlvbkljb24gY2xhc3NOYW1lPVwiZmFzIGZhLWN1dFwiIC8+IHt0KCdDdXQnKX1cbiAgICAgICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHshZGVsZXRlQW5kQ29weURpc2FibGVkICYmICFpc05pbChjb3BpZWRXaWRnZXQpICYmIChcbiAgICAgICAgICAgICAgICAgICAgPE1lbnVJdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBvblNlbGVjdE9wdGlvbihoYW5kbGVQYXN0ZUVsZW1lbnQpfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8T3B0aW9uSWNvbiBjbGFzc05hbWU9XCJmYXMgZmEtcGFzdGVcIiAvPiB7dCgnUGFzdGUnKX1cbiAgICAgICAgICAgICAgICAgICAgPC9NZW51SXRlbT5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHshZGVsZXRlQW5kQ29weURpc2FibGVkICYmIChcbiAgICAgICAgICAgICAgICAgICAgPE1lbnVJdGVtIG9uQ2xpY2s9eygpID0+IG9uU2VsZWN0T3B0aW9uKGhhbmRsZVJlbW92ZSl9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPE9wdGlvbkljb24gY2xhc3NOYW1lPVwiZmFzIGZhLXRyYXNoXCIgLz4ge3QoJ0RlbGV0ZScpfVxuICAgICAgICAgICAgICAgICAgICA8L01lbnVJdGVtPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L01lbnVJdGVtQ29udGFpbmVyPlxuICAgICAgICApO1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8TWVudUNvbnRhaW5lclxuICAgICAgICAgICAgcG9zWD17eH1cbiAgICAgICAgICAgIHBvc1k9e3l9XG4gICAgICAgICAgICBvbkNvbnRleHRNZW51PXsoZSkgPT4gZS5wcmV2ZW50RGVmYXVsdCgpfVxuICAgICAgICA+XG4gICAgICAgICAgICB7cmVuZGVyTWVudSgpfVxuICAgICAgICA8L01lbnVDb250YWluZXI+XG4gICAgKTtcbn07XG5cbkNvbnRleHRNZW51LnByb3BUeXBlcyA9IHtcbiAgICBjb3BpZWRXaWRnZXQ6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgZWRpdGlvbkxldmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGhhbmRsZU9wZW5TZXR0aW5nczogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlUmVtb3ZlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBpc1NlbGVjdGVkSXRlbTogUHJvcFR5cGVzLmJvb2wsXG4gICAgb25DbG9zZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlQ29weVdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlUGFzdGVFbGVtZW50OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvcGVuU2V0dGluZ3NEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgeDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB5OiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQ29udGV4dE1lbnU7XG4iXX0=