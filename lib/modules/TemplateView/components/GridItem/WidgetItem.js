"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _types = require("../../../../constants/types");

var _ChartWidget = _interopRequireDefault(require("../widgets/ChartWidget"));

var _ClipArtWidget = _interopRequireDefault(require("../widgets/ClipArtWidget"));

var _ImageWidget = _interopRequireDefault(require("../widgets/ImageWidget"));

var _MapWidget = _interopRequireDefault(require("../widgets/MapWidget"));

var _SegmentsMapWidget = _interopRequireDefault(require("../widgets/SegmentsMapWidget"));

var _SeparatorWidget = _interopRequireDefault(require("../widgets/SeparatorWidget"));

var _SignatureWidget = _interopRequireDefault(require("../widgets/SignatureWidget"));

var _SupportChartWidget = _interopRequireDefault(require("../widgets/SupportChartWidget"));

var _SupportTableWidget = _interopRequireDefault(require("../widgets/SupportTableWidget"));

var _TableWidget = _interopRequireDefault(require("../widgets/TableWidget"));

var _TextWidget = _interopRequireDefault(require("../widgets/TextWidget"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WidgetItem = function WidgetItem(_ref) {
  var chartWidgetsNames = _ref.chartWidgetsNames,
      currencyConfig = _ref.currencyConfig,
      editionLevel = _ref.editionLevel,
      froalaEditorConfig = _ref.froalaEditorConfig,
      _ref$handleChangeValu = _ref.handleChangeValue,
      handleChangeValue = _ref$handleChangeValu === void 0 ? function () {} : _ref$handleChangeValu,
      innerHeight = _ref.height,
      id = _ref.id,
      infinitePagesSupportEnabled = _ref.infinitePagesSupportEnabled,
      isSelected = _ref.isSelected,
      replaceInfoRequired = _ref.replaceInfoRequired,
      style = _ref.style,
      tableWidgetsNames = _ref.tableWidgetsNames,
      type = _ref.type,
      value = _ref.value,
      innerWidth = _ref.width;

  switch (type) {
    case 'clip-art':
      return (0, _ClipArtWidget.default)({
        innerHeight: innerHeight,
        innerWidth: innerWidth,
        style: style
      });

    case 'image':
      return (0, _ImageWidget.default)({
        value: value,
        style: style
      });

    case 'map':
      return (0, _MapWidget.default)({
        keyName: id,
        editionLevel: editionLevel,
        handleChangeValue: handleChangeValue,
        value: value,
        innerHeight: innerHeight,
        innerWidth: innerWidth
      });

    case 'segments-map':
      return (0, _SegmentsMapWidget.default)({
        keyName: id,
        editionLevel: editionLevel,
        handleChangeValue: handleChangeValue,
        style: style,
        value: value,
        innerHeight: innerHeight
      });

    case 'separator':
      return (0, _SeparatorWidget.default)({
        innerHeight: innerHeight,
        style: style
      });

    case 'signature':
      return (0, _SignatureWidget.default)({
        innerHeight: innerHeight,
        innerWidth: innerWidth,
        value: value
      });

    case 'money-table':
    case 'kwh-table':
    case 'equipment-table':
      return (0, _SupportTableWidget.default)({
        editionDisabled: editionLevel === _types.NO_EDITION_MODE,
        style: style,
        value: value
      });

    case 'text':
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_TextWidget.default, {
        editionDisabled: editionLevel === _types.NO_EDITION_MODE,
        froalaEditorConfig: froalaEditorConfig,
        handleChangeValue: handleChangeValue,
        isSelected: isSelected,
        keyName: id,
        value: value
      });

    default:
      {
        if (tableWidgetsNames.includes(type)) {
          var tableIso = currencyConfig.tableIso,
              tableLocale = currencyConfig.tableLocale;
          return (0, _TableWidget.default)({
            currencyIso: tableIso,
            currencyLocale: tableLocale,
            editionLevel: editionLevel,
            handleChangeValue: handleChangeValue,
            infinitePagesSupportEnabled: infinitePagesSupportEnabled,
            innerWidth: innerWidth,
            isSelected: isSelected,
            keyName: id,
            style: style,
            value: value
          });
        }

        if (chartWidgetsNames.includes(type)) {
          var chartIso = currencyConfig.chartIso,
              chartLocale = currencyConfig.chartLocale;

          if (replaceInfoRequired !== 'graph') {
            return (0, _SupportChartWidget.default)({
              currencyIso: chartIso,
              currencyLocale: chartLocale,
              editionDisabled: editionLevel === _types.NO_EDITION_MODE,
              innerHeight: innerHeight,
              innerWidth: innerWidth,
              value: value
            });
          }

          return (0, _ChartWidget.default)({
            currencyIso: chartIso,
            currencyLocale: chartLocale,
            innerHeight: innerHeight,
            innerWidth: innerWidth,
            value: value
          });
        }

        return null;
      }
  }
};

WidgetItem.propTypes = {
  chartWidgetsNames: _propTypes.default.array,
  currencyConfig: _propTypes.default.object,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  handleChangeValue: _propTypes.default.func,
  height: _propTypes.default.number,
  id: _propTypes.default.string,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  isSelected: _propTypes.default.bool,
  replaceInfoRequired: _propTypes.default.string,
  style: _propTypes.default.object,
  tableWidgetsNames: _propTypes.default.array,
  type: _propTypes.default.string,
  value: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
  width: _propTypes.default.number
};
var _default = WidgetItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL1dpZGdldEl0ZW0uanMiXSwibmFtZXMiOlsiV2lkZ2V0SXRlbSIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiY3VycmVuY3lDb25maWciLCJlZGl0aW9uTGV2ZWwiLCJmcm9hbGFFZGl0b3JDb25maWciLCJoYW5kbGVDaGFuZ2VWYWx1ZSIsImlubmVySGVpZ2h0IiwiaGVpZ2h0IiwiaWQiLCJpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQiLCJpc1NlbGVjdGVkIiwicmVwbGFjZUluZm9SZXF1aXJlZCIsInN0eWxlIiwidGFibGVXaWRnZXRzTmFtZXMiLCJ0eXBlIiwidmFsdWUiLCJpbm5lcldpZHRoIiwid2lkdGgiLCJrZXlOYW1lIiwiZWRpdGlvbkRpc2FibGVkIiwiTk9fRURJVElPTl9NT0RFIiwiaW5jbHVkZXMiLCJ0YWJsZUlzbyIsInRhYmxlTG9jYWxlIiwiY3VycmVuY3lJc28iLCJjdXJyZW5jeUxvY2FsZSIsImNoYXJ0SXNvIiwiY2hhcnRMb2NhbGUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsIm9iamVjdCIsInN0cmluZyIsImZ1bmMiLCJudW1iZXIiLCJib29sIiwib25lT2ZUeXBlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7OztBQUVBLElBQU1BLFVBQVUsR0FBRyxTQUFiQSxVQUFhLE9BZ0JiO0FBQUEsTUFmRkMsaUJBZUUsUUFmRkEsaUJBZUU7QUFBQSxNQWRGQyxjQWNFLFFBZEZBLGNBY0U7QUFBQSxNQWJGQyxZQWFFLFFBYkZBLFlBYUU7QUFBQSxNQVpGQyxrQkFZRSxRQVpGQSxrQkFZRTtBQUFBLG1DQVhGQyxpQkFXRTtBQUFBLE1BWEZBLGlCQVdFLHNDQVhrQixZQUFNLENBQUUsQ0FXMUI7QUFBQSxNQVZNQyxXQVVOLFFBVkZDLE1BVUU7QUFBQSxNQVRGQyxFQVNFLFFBVEZBLEVBU0U7QUFBQSxNQVJGQywyQkFRRSxRQVJGQSwyQkFRRTtBQUFBLE1BUEZDLFVBT0UsUUFQRkEsVUFPRTtBQUFBLE1BTkZDLG1CQU1FLFFBTkZBLG1CQU1FO0FBQUEsTUFMRkMsS0FLRSxRQUxGQSxLQUtFO0FBQUEsTUFKRkMsaUJBSUUsUUFKRkEsaUJBSUU7QUFBQSxNQUhGQyxJQUdFLFFBSEZBLElBR0U7QUFBQSxNQUZGQyxLQUVFLFFBRkZBLEtBRUU7QUFBQSxNQURLQyxVQUNMLFFBREZDLEtBQ0U7O0FBQ0YsVUFBUUgsSUFBUjtBQUNJLFNBQUssVUFBTDtBQUNJLGFBQU8sNEJBQWM7QUFDakJSLFFBQUFBLFdBQVcsRUFBWEEsV0FEaUI7QUFFakJVLFFBQUFBLFVBQVUsRUFBVkEsVUFGaUI7QUFHakJKLFFBQUFBLEtBQUssRUFBTEE7QUFIaUIsT0FBZCxDQUFQOztBQUtKLFNBQUssT0FBTDtBQUNJLGFBQU8sMEJBQVk7QUFBRUcsUUFBQUEsS0FBSyxFQUFMQSxLQUFGO0FBQVNILFFBQUFBLEtBQUssRUFBTEE7QUFBVCxPQUFaLENBQVA7O0FBQ0osU0FBSyxLQUFMO0FBQ0ksYUFBTyx3QkFBVTtBQUNiTSxRQUFBQSxPQUFPLEVBQUVWLEVBREk7QUFFYkwsUUFBQUEsWUFBWSxFQUFaQSxZQUZhO0FBR2JFLFFBQUFBLGlCQUFpQixFQUFqQkEsaUJBSGE7QUFJYlUsUUFBQUEsS0FBSyxFQUFMQSxLQUphO0FBS2JULFFBQUFBLFdBQVcsRUFBWEEsV0FMYTtBQU1iVSxRQUFBQSxVQUFVLEVBQVZBO0FBTmEsT0FBVixDQUFQOztBQVFKLFNBQUssY0FBTDtBQUNJLGFBQU8sZ0NBQWtCO0FBQ3JCRSxRQUFBQSxPQUFPLEVBQUVWLEVBRFk7QUFFckJMLFFBQUFBLFlBQVksRUFBWkEsWUFGcUI7QUFHckJFLFFBQUFBLGlCQUFpQixFQUFqQkEsaUJBSHFCO0FBSXJCTyxRQUFBQSxLQUFLLEVBQUxBLEtBSnFCO0FBS3JCRyxRQUFBQSxLQUFLLEVBQUxBLEtBTHFCO0FBTXJCVCxRQUFBQSxXQUFXLEVBQVhBO0FBTnFCLE9BQWxCLENBQVA7O0FBUUosU0FBSyxXQUFMO0FBQ0ksYUFBTyw4QkFBZ0I7QUFBRUEsUUFBQUEsV0FBVyxFQUFYQSxXQUFGO0FBQWVNLFFBQUFBLEtBQUssRUFBTEE7QUFBZixPQUFoQixDQUFQOztBQUNKLFNBQUssV0FBTDtBQUNJLGFBQU8sOEJBQWdCO0FBQUVOLFFBQUFBLFdBQVcsRUFBWEEsV0FBRjtBQUFlVSxRQUFBQSxVQUFVLEVBQVZBLFVBQWY7QUFBMkJELFFBQUFBLEtBQUssRUFBTEE7QUFBM0IsT0FBaEIsQ0FBUDs7QUFDSixTQUFLLGFBQUw7QUFDQSxTQUFLLFdBQUw7QUFDQSxTQUFLLGlCQUFMO0FBQ0ksYUFBTyxpQ0FBbUI7QUFDdEJJLFFBQUFBLGVBQWUsRUFBRWhCLFlBQVksS0FBS2lCLHNCQURaO0FBRXRCUixRQUFBQSxLQUFLLEVBQUxBLEtBRnNCO0FBR3RCRyxRQUFBQSxLQUFLLEVBQUxBO0FBSHNCLE9BQW5CLENBQVA7O0FBS0osU0FBSyxNQUFMO0FBQ0ksMEJBQ0kscUJBQUMsbUJBQUQ7QUFDSSxRQUFBLGVBQWUsRUFBRVosWUFBWSxLQUFLaUIsc0JBRHRDO0FBRUksUUFBQSxrQkFBa0IsRUFBRWhCLGtCQUZ4QjtBQUdJLFFBQUEsaUJBQWlCLEVBQUVDLGlCQUh2QjtBQUlJLFFBQUEsVUFBVSxFQUFFSyxVQUpoQjtBQUtJLFFBQUEsT0FBTyxFQUFFRixFQUxiO0FBTUksUUFBQSxLQUFLLEVBQUVPO0FBTlgsUUFESjs7QUFVSjtBQUFTO0FBQ0wsWUFBSUYsaUJBQWlCLENBQUNRLFFBQWxCLENBQTJCUCxJQUEzQixDQUFKLEVBQXNDO0FBQ2xDLGNBQVFRLFFBQVIsR0FBa0NwQixjQUFsQyxDQUFRb0IsUUFBUjtBQUFBLGNBQWtCQyxXQUFsQixHQUFrQ3JCLGNBQWxDLENBQWtCcUIsV0FBbEI7QUFDQSxpQkFBTywwQkFBWTtBQUNmQyxZQUFBQSxXQUFXLEVBQUVGLFFBREU7QUFFZkcsWUFBQUEsY0FBYyxFQUFFRixXQUZEO0FBR2ZwQixZQUFBQSxZQUFZLEVBQVpBLFlBSGU7QUFJZkUsWUFBQUEsaUJBQWlCLEVBQWpCQSxpQkFKZTtBQUtmSSxZQUFBQSwyQkFBMkIsRUFBM0JBLDJCQUxlO0FBTWZPLFlBQUFBLFVBQVUsRUFBVkEsVUFOZTtBQU9mTixZQUFBQSxVQUFVLEVBQVZBLFVBUGU7QUFRZlEsWUFBQUEsT0FBTyxFQUFFVixFQVJNO0FBU2ZJLFlBQUFBLEtBQUssRUFBTEEsS0FUZTtBQVVmRyxZQUFBQSxLQUFLLEVBQUxBO0FBVmUsV0FBWixDQUFQO0FBWUg7O0FBQ0QsWUFBSWQsaUJBQWlCLENBQUNvQixRQUFsQixDQUEyQlAsSUFBM0IsQ0FBSixFQUFzQztBQUNsQyxjQUFRWSxRQUFSLEdBQWtDeEIsY0FBbEMsQ0FBUXdCLFFBQVI7QUFBQSxjQUFrQkMsV0FBbEIsR0FBa0N6QixjQUFsQyxDQUFrQnlCLFdBQWxCOztBQUNBLGNBQUloQixtQkFBbUIsS0FBSyxPQUE1QixFQUFxQztBQUNqQyxtQkFBTyxpQ0FBbUI7QUFDdEJhLGNBQUFBLFdBQVcsRUFBRUUsUUFEUztBQUV0QkQsY0FBQUEsY0FBYyxFQUFFRSxXQUZNO0FBR3RCUixjQUFBQSxlQUFlLEVBQUVoQixZQUFZLEtBQUtpQixzQkFIWjtBQUl0QmQsY0FBQUEsV0FBVyxFQUFYQSxXQUpzQjtBQUt0QlUsY0FBQUEsVUFBVSxFQUFWQSxVQUxzQjtBQU10QkQsY0FBQUEsS0FBSyxFQUFMQTtBQU5zQixhQUFuQixDQUFQO0FBUUg7O0FBQ0QsaUJBQU8sMEJBQVk7QUFDZlMsWUFBQUEsV0FBVyxFQUFFRSxRQURFO0FBRWZELFlBQUFBLGNBQWMsRUFBRUUsV0FGRDtBQUdmckIsWUFBQUEsV0FBVyxFQUFYQSxXQUhlO0FBSWZVLFlBQUFBLFVBQVUsRUFBVkEsVUFKZTtBQUtmRCxZQUFBQSxLQUFLLEVBQUxBO0FBTGUsV0FBWixDQUFQO0FBT0g7O0FBQ0QsZUFBTyxJQUFQO0FBQ0g7QUF2Rkw7QUF5RkgsQ0ExR0Q7O0FBNEdBZixVQUFVLENBQUM0QixTQUFYLEdBQXVCO0FBQ25CM0IsRUFBQUEsaUJBQWlCLEVBQUU0QixtQkFBVUMsS0FEVjtBQUVuQjVCLEVBQUFBLGNBQWMsRUFBRTJCLG1CQUFVRSxNQUZQO0FBR25CNUIsRUFBQUEsWUFBWSxFQUFFMEIsbUJBQVVHLE1BSEw7QUFJbkI1QixFQUFBQSxrQkFBa0IsRUFBRXlCLG1CQUFVRSxNQUpYO0FBS25CMUIsRUFBQUEsaUJBQWlCLEVBQUV3QixtQkFBVUksSUFMVjtBQU1uQjFCLEVBQUFBLE1BQU0sRUFBRXNCLG1CQUFVSyxNQU5DO0FBT25CMUIsRUFBQUEsRUFBRSxFQUFFcUIsbUJBQVVHLE1BUEs7QUFRbkJ2QixFQUFBQSwyQkFBMkIsRUFBRW9CLG1CQUFVTSxJQVJwQjtBQVNuQnpCLEVBQUFBLFVBQVUsRUFBRW1CLG1CQUFVTSxJQVRIO0FBVW5CeEIsRUFBQUEsbUJBQW1CLEVBQUVrQixtQkFBVUcsTUFWWjtBQVduQnBCLEVBQUFBLEtBQUssRUFBRWlCLG1CQUFVRSxNQVhFO0FBWW5CbEIsRUFBQUEsaUJBQWlCLEVBQUVnQixtQkFBVUMsS0FaVjtBQWFuQmhCLEVBQUFBLElBQUksRUFBRWUsbUJBQVVHLE1BYkc7QUFjbkJqQixFQUFBQSxLQUFLLEVBQUVjLG1CQUFVTyxTQUFWLENBQW9CLENBQUNQLG1CQUFVRyxNQUFYLEVBQW1CSCxtQkFBVUUsTUFBN0IsQ0FBcEIsQ0FkWTtBQWVuQmQsRUFBQUEsS0FBSyxFQUFFWSxtQkFBVUs7QUFmRSxDQUF2QjtlQWtCZWxDLFUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBOT19FRElUSU9OX01PREUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcblxuaW1wb3J0IENoYXJ0V2lkZ2V0IGZyb20gJy4uL3dpZGdldHMvQ2hhcnRXaWRnZXQnO1xuaW1wb3J0IENsaXBBcnRXaWRnZXQgZnJvbSAnLi4vd2lkZ2V0cy9DbGlwQXJ0V2lkZ2V0JztcbmltcG9ydCBJbWFnZVdpZGdldCBmcm9tICcuLi93aWRnZXRzL0ltYWdlV2lkZ2V0JztcbmltcG9ydCBNYXBXaWRnZXQgZnJvbSAnLi4vd2lkZ2V0cy9NYXBXaWRnZXQnO1xuaW1wb3J0IFNlZ21lbnRzTWFwV2lkZ2V0IGZyb20gJy4uL3dpZGdldHMvU2VnbWVudHNNYXBXaWRnZXQnO1xuaW1wb3J0IFNlcGFyYXRvcldpZGdldCBmcm9tICcuLi93aWRnZXRzL1NlcGFyYXRvcldpZGdldCc7XG5pbXBvcnQgU2lnbmF0dXJlV2lkZ2V0IGZyb20gJy4uL3dpZGdldHMvU2lnbmF0dXJlV2lkZ2V0JztcbmltcG9ydCBTdXBwb3J0Q2hhcnRXaWRnZXQgZnJvbSAnLi4vd2lkZ2V0cy9TdXBwb3J0Q2hhcnRXaWRnZXQnO1xuaW1wb3J0IFN1cHBvcnRUYWJsZVdpZGdldCBmcm9tICcuLi93aWRnZXRzL1N1cHBvcnRUYWJsZVdpZGdldCc7XG5pbXBvcnQgVGFibGVXaWRnZXQgZnJvbSAnLi4vd2lkZ2V0cy9UYWJsZVdpZGdldCc7XG5pbXBvcnQgVGV4dFdpZGdldCBmcm9tICcuLi93aWRnZXRzL1RleHRXaWRnZXQnO1xuXG5jb25zdCBXaWRnZXRJdGVtID0gKHtcbiAgICBjaGFydFdpZGdldHNOYW1lcyxcbiAgICBjdXJyZW5jeUNvbmZpZyxcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnLFxuICAgIGhhbmRsZUNoYW5nZVZhbHVlID0gKCkgPT4ge30sXG4gICAgaGVpZ2h0OiBpbm5lckhlaWdodCxcbiAgICBpZCxcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQsXG4gICAgaXNTZWxlY3RlZCxcbiAgICByZXBsYWNlSW5mb1JlcXVpcmVkLFxuICAgIHN0eWxlLFxuICAgIHRhYmxlV2lkZ2V0c05hbWVzLFxuICAgIHR5cGUsXG4gICAgdmFsdWUsXG4gICAgd2lkdGg6IGlubmVyV2lkdGgsXG59KSA9PiB7XG4gICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgIGNhc2UgJ2NsaXAtYXJ0JzpcbiAgICAgICAgICAgIHJldHVybiBDbGlwQXJ0V2lkZ2V0KHtcbiAgICAgICAgICAgICAgICBpbm5lckhlaWdodCxcbiAgICAgICAgICAgICAgICBpbm5lcldpZHRoLFxuICAgICAgICAgICAgICAgIHN0eWxlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIGNhc2UgJ2ltYWdlJzpcbiAgICAgICAgICAgIHJldHVybiBJbWFnZVdpZGdldCh7IHZhbHVlLCBzdHlsZSB9KTtcbiAgICAgICAgY2FzZSAnbWFwJzpcbiAgICAgICAgICAgIHJldHVybiBNYXBXaWRnZXQoe1xuICAgICAgICAgICAgICAgIGtleU5hbWU6IGlkLFxuICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCxcbiAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSxcbiAgICAgICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgICAgICBpbm5lckhlaWdodCxcbiAgICAgICAgICAgICAgICBpbm5lcldpZHRoLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIGNhc2UgJ3NlZ21lbnRzLW1hcCc6XG4gICAgICAgICAgICByZXR1cm4gU2VnbWVudHNNYXBXaWRnZXQoe1xuICAgICAgICAgICAgICAgIGtleU5hbWU6IGlkLFxuICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCxcbiAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSxcbiAgICAgICAgICAgICAgICBzdHlsZSxcbiAgICAgICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgICAgICBpbm5lckhlaWdodCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICBjYXNlICdzZXBhcmF0b3InOlxuICAgICAgICAgICAgcmV0dXJuIFNlcGFyYXRvcldpZGdldCh7IGlubmVySGVpZ2h0LCBzdHlsZSB9KTtcbiAgICAgICAgY2FzZSAnc2lnbmF0dXJlJzpcbiAgICAgICAgICAgIHJldHVybiBTaWduYXR1cmVXaWRnZXQoeyBpbm5lckhlaWdodCwgaW5uZXJXaWR0aCwgdmFsdWUgfSk7XG4gICAgICAgIGNhc2UgJ21vbmV5LXRhYmxlJzpcbiAgICAgICAgY2FzZSAna3doLXRhYmxlJzpcbiAgICAgICAgY2FzZSAnZXF1aXBtZW50LXRhYmxlJzpcbiAgICAgICAgICAgIHJldHVybiBTdXBwb3J0VGFibGVXaWRnZXQoe1xuICAgICAgICAgICAgICAgIGVkaXRpb25EaXNhYmxlZDogZWRpdGlvbkxldmVsID09PSBOT19FRElUSU9OX01PREUsXG4gICAgICAgICAgICAgICAgc3R5bGUsXG4gICAgICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgY2FzZSAndGV4dCc6XG4gICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgIDxUZXh0V2lkZ2V0XG4gICAgICAgICAgICAgICAgICAgIGVkaXRpb25EaXNhYmxlZD17ZWRpdGlvbkxldmVsID09PSBOT19FRElUSU9OX01PREV9XG4gICAgICAgICAgICAgICAgICAgIGZyb2FsYUVkaXRvckNvbmZpZz17ZnJvYWxhRWRpdG9yQ29uZmlnfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZT17aGFuZGxlQ2hhbmdlVmFsdWV9XG4gICAgICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWQ9e2lzU2VsZWN0ZWR9XG4gICAgICAgICAgICAgICAgICAgIGtleU5hbWU9e2lkfVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICk7XG4gICAgICAgIGRlZmF1bHQ6IHtcbiAgICAgICAgICAgIGlmICh0YWJsZVdpZGdldHNOYW1lcy5pbmNsdWRlcyh0eXBlKSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHsgdGFibGVJc28sIHRhYmxlTG9jYWxlIH0gPSBjdXJyZW5jeUNvbmZpZztcbiAgICAgICAgICAgICAgICByZXR1cm4gVGFibGVXaWRnZXQoe1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW5jeUlzbzogdGFibGVJc28sXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbmN5TG9jYWxlOiB0YWJsZUxvY2FsZSxcbiAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsLFxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkLFxuICAgICAgICAgICAgICAgICAgICBpbm5lcldpZHRoLFxuICAgICAgICAgICAgICAgICAgICBpc1NlbGVjdGVkLFxuICAgICAgICAgICAgICAgICAgICBrZXlOYW1lOiBpZCxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNoYXJ0V2lkZ2V0c05hbWVzLmluY2x1ZGVzKHR5cGUpKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgeyBjaGFydElzbywgY2hhcnRMb2NhbGUgfSA9IGN1cnJlbmN5Q29uZmlnO1xuICAgICAgICAgICAgICAgIGlmIChyZXBsYWNlSW5mb1JlcXVpcmVkICE9PSAnZ3JhcGgnKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBTdXBwb3J0Q2hhcnRXaWRnZXQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY3lJc286IGNoYXJ0SXNvLFxuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY3lMb2NhbGU6IGNoYXJ0TG9jYWxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkRpc2FibGVkOiBlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGlubmVySGVpZ2h0LFxuICAgICAgICAgICAgICAgICAgICAgICAgaW5uZXJXaWR0aCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIENoYXJ0V2lkZ2V0KHtcbiAgICAgICAgICAgICAgICAgICAgY3VycmVuY3lJc286IGNoYXJ0SXNvLFxuICAgICAgICAgICAgICAgICAgICBjdXJyZW5jeUxvY2FsZTogY2hhcnRMb2NhbGUsXG4gICAgICAgICAgICAgICAgICAgIGlubmVySGVpZ2h0LFxuICAgICAgICAgICAgICAgICAgICBpbm5lcldpZHRoLFxuICAgICAgICAgICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgfVxufTtcblxuV2lkZ2V0SXRlbS5wcm9wVHlwZXMgPSB7XG4gICAgY2hhcnRXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjdXJyZW5jeUNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGhhbmRsZUNoYW5nZVZhbHVlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoZWlnaHQ6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpc1NlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICByZXBsYWNlSW5mb1JlcXVpcmVkOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHRhYmxlV2lkZ2V0c05hbWVzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm9iamVjdF0pLFxuICAgIHdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgV2lkZ2V0SXRlbTtcbiJdfQ==