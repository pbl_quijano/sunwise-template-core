"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    content: '';\n    height: 10px;\n    opacity: 0;\n    pointer-events: none;\n    position: absolute;\n    transition: opacity 0.2s linear;\n    width: 10px;\n    z-index: 1;\n\n    ", "\n\n    ", "\n"])), function (_ref) {
  var verticalAlign = _ref.verticalAlign;

  if (verticalAlign === 'top') {
    return 'border-bottom: 2px solid rgba(0, 0, 0, 0.4);top: -10px;';
  } else {
    return 'border-top: 2px solid rgba(0, 0, 0, 0.4);bottom: -10px;';
  }
}, function (_ref2) {
  var horizontalAlign = _ref2.horizontalAlign;

  if (horizontalAlign === 'left') {
    return 'border-right: 2px solid rgba(0, 0, 0, 0.4);left: -10px;';
  } else {
    return 'border-left: 2px solid rgba(0, 0, 0, 0.4);right: -10px;';
  }
});

var CornerMarker = function CornerMarker(_ref3) {
  var hide = _ref3.hide,
      horizontalAlign = _ref3.horizontalAlign,
      verticalAlign = _ref3.verticalAlign;
  if (hide) return null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    className: "corner-marker",
    verticalAlign: verticalAlign,
    horizontalAlign: horizontalAlign
  });
};

CornerMarker.propTypes = {
  hide: _propTypes.default.bool,
  horizontalAlign: _propTypes.default.string,
  verticalAlign: _propTypes.default.string
};
var _default = CornerMarker;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL0Nvcm5lck1hcmtlci5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJ2ZXJ0aWNhbEFsaWduIiwiaG9yaXpvbnRhbEFsaWduIiwiQ29ybmVyTWFya2VyIiwiaGlkZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsbVJBVVQsZ0JBQXVCO0FBQUEsTUFBcEJDLGFBQW9CLFFBQXBCQSxhQUFvQjs7QUFDckIsTUFBSUEsYUFBYSxLQUFLLEtBQXRCLEVBQTZCO0FBQ3pCLFdBQU8seURBQVA7QUFDSCxHQUZELE1BRU87QUFDSCxXQUFPLHlEQUFQO0FBQ0g7QUFDSixDQWhCVSxFQWtCVCxpQkFBeUI7QUFBQSxNQUF0QkMsZUFBc0IsU0FBdEJBLGVBQXNCOztBQUN2QixNQUFJQSxlQUFlLEtBQUssTUFBeEIsRUFBZ0M7QUFDNUIsV0FBTyx5REFBUDtBQUNILEdBRkQsTUFFTztBQUNILFdBQU8seURBQVA7QUFDSDtBQUNKLENBeEJVLENBQWY7O0FBMkJBLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLFFBQThDO0FBQUEsTUFBM0NDLElBQTJDLFNBQTNDQSxJQUEyQztBQUFBLE1BQXJDRixlQUFxQyxTQUFyQ0EsZUFBcUM7QUFBQSxNQUFwQkQsYUFBb0IsU0FBcEJBLGFBQW9CO0FBQy9ELE1BQUlHLElBQUosRUFBVSxPQUFPLElBQVA7QUFDVixzQkFDSSxxQkFBQyxTQUFEO0FBQ0ksSUFBQSxTQUFTLEVBQUMsZUFEZDtBQUVVSCxJQUFBQSxhQUFhLEVBQWJBLGFBRlY7QUFFeUJDLElBQUFBLGVBQWUsRUFBZkE7QUFGekIsSUFESjtBQU1ILENBUkQ7O0FBVUFDLFlBQVksQ0FBQ0UsU0FBYixHQUF5QjtBQUNyQkQsRUFBQUEsSUFBSSxFQUFFRSxtQkFBVUMsSUFESztBQUVyQkwsRUFBQUEsZUFBZSxFQUFFSSxtQkFBVUUsTUFGTjtBQUdyQlAsRUFBQUEsYUFBYSxFQUFFSyxtQkFBVUU7QUFISixDQUF6QjtlQU1lTCxZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGNvbnRlbnQ6ICcnO1xuICAgIGhlaWdodDogMTBweDtcbiAgICBvcGFjaXR5OiAwO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuMnMgbGluZWFyO1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIHotaW5kZXg6IDE7XG5cbiAgICAkeyh7IHZlcnRpY2FsQWxpZ24gfSkgPT4ge1xuICAgICAgICBpZiAodmVydGljYWxBbGlnbiA9PT0gJ3RvcCcpIHtcbiAgICAgICAgICAgIHJldHVybiAnYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTt0b3A6IC0xMHB4Oyc7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gJ2JvcmRlci10b3A6IDJweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuNCk7Ym90dG9tOiAtMTBweDsnO1xuICAgICAgICB9XG4gICAgfX1cblxuICAgICR7KHsgaG9yaXpvbnRhbEFsaWduIH0pID0+IHtcbiAgICAgICAgaWYgKGhvcml6b250YWxBbGlnbiA9PT0gJ2xlZnQnKSB7XG4gICAgICAgICAgICByZXR1cm4gJ2JvcmRlci1yaWdodDogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTtsZWZ0OiAtMTBweDsnO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuICdib3JkZXItbGVmdDogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTtyaWdodDogLTEwcHg7JztcbiAgICAgICAgfVxuICAgIH19XG5gO1xuXG5jb25zdCBDb3JuZXJNYXJrZXIgPSAoeyBoaWRlLCBob3Jpem9udGFsQWxpZ24sIHZlcnRpY2FsQWxpZ24gfSkgPT4ge1xuICAgIGlmIChoaWRlKSByZXR1cm4gbnVsbDtcbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJjb3JuZXItbWFya2VyXCJcbiAgICAgICAgICAgIHsuLi57IHZlcnRpY2FsQWxpZ24sIGhvcml6b250YWxBbGlnbiB9fVxuICAgICAgICAvPlxuICAgICk7XG59O1xuXG5Db3JuZXJNYXJrZXIucHJvcFR5cGVzID0ge1xuICAgIGhpZGU6IFByb3BUeXBlcy5ib29sLFxuICAgIGhvcml6b250YWxBbGlnbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2ZXJ0aWNhbEFsaWduOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQ29ybmVyTWFya2VyO1xuIl19