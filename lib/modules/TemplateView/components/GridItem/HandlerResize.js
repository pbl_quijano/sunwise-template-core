"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    border-bottom: 2px solid rgba(0, 0, 0, 0.4);\n    border-right: 2px solid rgba(0, 0, 0, 0.4);\n    bottom: 3px;\n    content: '';\n    height: 5px;\n    pointer-events: none;\n    position: absolute;\n    right: 3px;\n    width: 5px;\n    z-index: 9999;\n"])));

var HandlerResize = function HandlerResize(_ref) {
  var show = _ref.show;
  if (!show) return null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {});
};

HandlerResize.propTypes = {
  show: _propTypes.default.bool
};
var _default = HandlerResize;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL0hhbmRsZXJSZXNpemUuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiSGFuZGxlclJlc2l6ZSIsInNob3ciLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJib29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDBVQUFmOztBQWFBLElBQU1DLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsT0FBYztBQUFBLE1BQVhDLElBQVcsUUFBWEEsSUFBVztBQUNoQyxNQUFJLENBQUNBLElBQUwsRUFBVyxPQUFPLElBQVA7QUFDWCxzQkFBTyxxQkFBQyxTQUFELEtBQVA7QUFDSCxDQUhEOztBQUtBRCxhQUFhLENBQUNFLFNBQWQsR0FBMEI7QUFDdEJELEVBQUFBLElBQUksRUFBRUUsbUJBQVVDO0FBRE0sQ0FBMUI7ZUFJZUosYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjQpO1xuICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICBib3R0b206IDNweDtcbiAgICBjb250ZW50OiAnJztcbiAgICBoZWlnaHQ6IDVweDtcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDNweDtcbiAgICB3aWR0aDogNXB4O1xuICAgIHotaW5kZXg6IDk5OTk7XG5gO1xuXG5jb25zdCBIYW5kbGVyUmVzaXplID0gKHsgc2hvdyB9KSA9PiB7XG4gICAgaWYgKCFzaG93KSByZXR1cm4gbnVsbDtcbiAgICByZXR1cm4gPENvbnRhaW5lciAvPjtcbn07XG5cbkhhbmRsZXJSZXNpemUucHJvcFR5cGVzID0ge1xuICAgIHNob3c6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgSGFuZGxlclJlc2l6ZTtcbiJdfQ==