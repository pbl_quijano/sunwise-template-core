"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _types = require("../../../../constants/types");

var _WidgetItem = _interopRequireDefault(require("./WidgetItem"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    position: absolute;\n    width: ", ";\n    height: ", ";\n    top: ", ";\n    left: ", ";\n    overflow: hidden;\n    z-index: 2;\n    pointer-events: none;\n"])), function (_ref) {
  var size = _ref.size;
  return "".concat(size.width, "px");
}, function (_ref2) {
  var size = _ref2.size;
  return "".concat(size.height, "px");
}, function (_ref3) {
  var position = _ref3.position;
  return "".concat(position.y, "px");
}, function (_ref4) {
  var position = _ref4.position;
  return "".concat(position.x, "px");
});

var WidgetPreview = function WidgetPreview(_ref5) {
  var chartWidgetsNames = _ref5.chartWidgetsNames,
      currencyConfig = _ref5.currencyConfig,
      froalaEditorConfig = _ref5.froalaEditorConfig,
      infinitePagesSupportEnabled = _ref5.infinitePagesSupportEnabled,
      tableWidgetsNames = _ref5.tableWidgetsNames,
      widgetObject = _ref5.widgetObject;
  var height = widgetObject.height,
      id = widgetObject.id,
      name = widgetObject.name,
      replaceInfoRequired = widgetObject.replaceInfoRequired,
      style = widgetObject.style,
      value = widgetObject.value,
      width = widgetObject.width,
      x = widgetObject.x,
      y = widgetObject.y;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    size: {
      height: height,
      width: width
    },
    position: {
      x: x,
      y: y
    },
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetItem.default, {
      chartWidgetsNames: chartWidgetsNames,
      currencyConfig: currencyConfig,
      editionLevel: _types.NO_EDITION_MODE,
      froalaEditorConfig: froalaEditorConfig,
      height: height,
      id: id,
      infinitePagesSupportEnabled: infinitePagesSupportEnabled,
      replaceInfoRequired: replaceInfoRequired,
      style: style,
      tableWidgetsNames: tableWidgetsNames,
      type: name,
      value: value,
      width: width
    })
  });
};

WidgetPreview.propTypes = {
  chartWidgetsNames: _propTypes.default.array,
  currencyConfig: _propTypes.default.object,
  froalaEditorConfig: _propTypes.default.object,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  widgetObject: _propTypes.default.object
};
var _default = WidgetPreview;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL1dpZGdldFByZXZpZXcuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2Iiwic2l6ZSIsIndpZHRoIiwiaGVpZ2h0IiwicG9zaXRpb24iLCJ5IiwieCIsIldpZGdldFByZXZpZXciLCJjaGFydFdpZGdldHNOYW1lcyIsImN1cnJlbmN5Q29uZmlnIiwiZnJvYWxhRWRpdG9yQ29uZmlnIiwiaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkIiwidGFibGVXaWRnZXRzTmFtZXMiLCJ3aWRnZXRPYmplY3QiLCJpZCIsIm5hbWUiLCJyZXBsYWNlSW5mb1JlcXVpcmVkIiwic3R5bGUiLCJ2YWx1ZSIsIk5PX0VESVRJT05fTU9ERSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImFycmF5Iiwib2JqZWN0IiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViwwT0FFRjtBQUFBLE1BQUdDLElBQUgsUUFBR0EsSUFBSDtBQUFBLG1CQUFpQkEsSUFBSSxDQUFDQyxLQUF0QjtBQUFBLENBRkUsRUFHRDtBQUFBLE1BQUdELElBQUgsU0FBR0EsSUFBSDtBQUFBLG1CQUFpQkEsSUFBSSxDQUFDRSxNQUF0QjtBQUFBLENBSEMsRUFJSjtBQUFBLE1BQUdDLFFBQUgsU0FBR0EsUUFBSDtBQUFBLG1CQUFxQkEsUUFBUSxDQUFDQyxDQUE5QjtBQUFBLENBSkksRUFLSDtBQUFBLE1BQUdELFFBQUgsU0FBR0EsUUFBSDtBQUFBLG1CQUFxQkEsUUFBUSxDQUFDRSxDQUE5QjtBQUFBLENBTEcsQ0FBZjs7QUFXQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLFFBT2hCO0FBQUEsTUFORkMsaUJBTUUsU0FORkEsaUJBTUU7QUFBQSxNQUxGQyxjQUtFLFNBTEZBLGNBS0U7QUFBQSxNQUpGQyxrQkFJRSxTQUpGQSxrQkFJRTtBQUFBLE1BSEZDLDJCQUdFLFNBSEZBLDJCQUdFO0FBQUEsTUFGRkMsaUJBRUUsU0FGRkEsaUJBRUU7QUFBQSxNQURGQyxZQUNFLFNBREZBLFlBQ0U7QUFDRixNQUFRVixNQUFSLEdBQ0lVLFlBREosQ0FBUVYsTUFBUjtBQUFBLE1BQWdCVyxFQUFoQixHQUNJRCxZQURKLENBQWdCQyxFQUFoQjtBQUFBLE1BQW9CQyxJQUFwQixHQUNJRixZQURKLENBQW9CRSxJQUFwQjtBQUFBLE1BQTBCQyxtQkFBMUIsR0FDSUgsWUFESixDQUEwQkcsbUJBQTFCO0FBQUEsTUFBK0NDLEtBQS9DLEdBQ0lKLFlBREosQ0FBK0NJLEtBQS9DO0FBQUEsTUFBc0RDLEtBQXRELEdBQ0lMLFlBREosQ0FBc0RLLEtBQXREO0FBQUEsTUFBNkRoQixLQUE3RCxHQUNJVyxZQURKLENBQTZEWCxLQUE3RDtBQUFBLE1BQW9FSSxDQUFwRSxHQUNJTyxZQURKLENBQW9FUCxDQUFwRTtBQUFBLE1BQXVFRCxDQUF2RSxHQUNJUSxZQURKLENBQXVFUixDQUF2RTtBQUdBLHNCQUNJLHFCQUFDLFNBQUQ7QUFBVyxJQUFBLElBQUksRUFBRTtBQUFFRixNQUFBQSxNQUFNLEVBQU5BLE1BQUY7QUFBVUQsTUFBQUEsS0FBSyxFQUFMQTtBQUFWLEtBQWpCO0FBQW9DLElBQUEsUUFBUSxFQUFFO0FBQUVJLE1BQUFBLENBQUMsRUFBREEsQ0FBRjtBQUFLRCxNQUFBQSxDQUFDLEVBQURBO0FBQUwsS0FBOUM7QUFBQSwyQkFDSSxxQkFBQyxtQkFBRDtBQUNJLE1BQUEsaUJBQWlCLEVBQUVHLGlCQUR2QjtBQUVJLE1BQUEsY0FBYyxFQUFFQyxjQUZwQjtBQUdJLE1BQUEsWUFBWSxFQUFFVSxzQkFIbEI7QUFJSSxNQUFBLGtCQUFrQixFQUFFVCxrQkFKeEI7QUFLSSxNQUFBLE1BQU0sRUFBRVAsTUFMWjtBQU1JLE1BQUEsRUFBRSxFQUFFVyxFQU5SO0FBT0ksTUFBQSwyQkFBMkIsRUFBRUgsMkJBUGpDO0FBUUksTUFBQSxtQkFBbUIsRUFBRUssbUJBUnpCO0FBU0ksTUFBQSxLQUFLLEVBQUVDLEtBVFg7QUFVSSxNQUFBLGlCQUFpQixFQUFFTCxpQkFWdkI7QUFXSSxNQUFBLElBQUksRUFBRUcsSUFYVjtBQVlJLE1BQUEsS0FBSyxFQUFFRyxLQVpYO0FBYUksTUFBQSxLQUFLLEVBQUVoQjtBQWJYO0FBREosSUFESjtBQW1CSCxDQTlCRDs7QUFnQ0FLLGFBQWEsQ0FBQ2EsU0FBZCxHQUEwQjtBQUN0QlosRUFBQUEsaUJBQWlCLEVBQUVhLG1CQUFVQyxLQURQO0FBRXRCYixFQUFBQSxjQUFjLEVBQUVZLG1CQUFVRSxNQUZKO0FBR3RCYixFQUFBQSxrQkFBa0IsRUFBRVcsbUJBQVVFLE1BSFI7QUFJdEJaLEVBQUFBLDJCQUEyQixFQUFFVSxtQkFBVUcsSUFKakI7QUFLdEJaLEVBQUFBLGlCQUFpQixFQUFFUyxtQkFBVUMsS0FMUDtBQU10QlQsRUFBQUEsWUFBWSxFQUFFUSxtQkFBVUU7QUFORixDQUExQjtlQVNlaEIsYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHsgTk9fRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmltcG9ydCBXaWRnZXRJdGVtIGZyb20gJy4vV2lkZ2V0SXRlbSc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHdpZHRoOiAkeyh7IHNpemUgfSkgPT4gYCR7c2l6ZS53aWR0aH1weGB9O1xuICAgIGhlaWdodDogJHsoeyBzaXplIH0pID0+IGAke3NpemUuaGVpZ2h0fXB4YH07XG4gICAgdG9wOiAkeyh7IHBvc2l0aW9uIH0pID0+IGAke3Bvc2l0aW9uLnl9cHhgfTtcbiAgICBsZWZ0OiAkeyh7IHBvc2l0aW9uIH0pID0+IGAke3Bvc2l0aW9uLnh9cHhgfTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHotaW5kZXg6IDI7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG5gO1xuXG5jb25zdCBXaWRnZXRQcmV2aWV3ID0gKHtcbiAgICBjaGFydFdpZGdldHNOYW1lcyxcbiAgICBjdXJyZW5jeUNvbmZpZyxcbiAgICBmcm9hbGFFZGl0b3JDb25maWcsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkLFxuICAgIHRhYmxlV2lkZ2V0c05hbWVzLFxuICAgIHdpZGdldE9iamVjdCxcbn0pID0+IHtcbiAgICBjb25zdCB7IGhlaWdodCwgaWQsIG5hbWUsIHJlcGxhY2VJbmZvUmVxdWlyZWQsIHN0eWxlLCB2YWx1ZSwgd2lkdGgsIHgsIHkgfSA9XG4gICAgICAgIHdpZGdldE9iamVjdDtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXIgc2l6ZT17eyBoZWlnaHQsIHdpZHRoIH19IHBvc2l0aW9uPXt7IHgsIHkgfX0+XG4gICAgICAgICAgICA8V2lkZ2V0SXRlbVxuICAgICAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICBjdXJyZW5jeUNvbmZpZz17Y3VycmVuY3lDb25maWd9XG4gICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtOT19FRElUSU9OX01PREV9XG4gICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XG4gICAgICAgICAgICAgICAgaGVpZ2h0PXtoZWlnaHR9XG4gICAgICAgICAgICAgICAgaWQ9e2lkfVxuICAgICAgICAgICAgICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZD17aW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkfVxuICAgICAgICAgICAgICAgIHJlcGxhY2VJbmZvUmVxdWlyZWQ9e3JlcGxhY2VJbmZvUmVxdWlyZWR9XG4gICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlfVxuICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzPXt0YWJsZVdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICB0eXBlPXtuYW1lfVxuICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICB3aWR0aD17d2lkdGh9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICApO1xufTtcblxuV2lkZ2V0UHJldmlldy5wcm9wVHlwZXMgPSB7XG4gICAgY2hhcnRXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjdXJyZW5jeUNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBmcm9hbGFFZGl0b3JDb25maWc6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0YWJsZVdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHdpZGdldE9iamVjdDogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFdpZGdldFByZXZpZXc7XG4iXX0=