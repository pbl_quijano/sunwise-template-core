"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reResizable = require("re-resizable");

var _react = require("react");

var _reactDraggable = _interopRequireDefault(require("react-draggable"));

var _reactMousetrap = _interopRequireDefault(require("react-mousetrap"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _types = require("../../../../constants/types");

var _helpers = require("../../helpers");

var _CornerMarker = _interopRequireDefault(require("./CornerMarker"));

var _HandlerResize = _interopRequireDefault(require("./HandlerResize"));

var _Toolbar = _interopRequireDefault(require("./Toolbar"));

var _WidgetItem = _interopRequireDefault(require("./WidgetItem"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DraggableBody = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    height: 100%;\n    overflow: hidden;\n    ", "\n    position: relative;\n    transition: all 0.2s linear;\n    width: 100%;\n    z-index: 2;\n\n    &.design-mode {\n        outline: 1px dashed #bbb;\n    }\n\n    &.selected {\n        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19),\n            0 6px 6px rgba(0, 0, 0, 0.23);\n        outline: 1px solid rgb(255, 154, 0);\n    }\n"])), function (_ref) {
  var editionDisabled = _ref.editionDisabled;
  return editionDisabled && 'pointer-events: none;';
});

var GridItem = function GridItem(_ref2) {
  var bindShortcut = _ref2.bindShortcut,
      chartWidgetsNames = _ref2.chartWidgetsNames,
      currencyConfig = _ref2.currencyConfig,
      editionLevel = _ref2.editionLevel,
      froalaEditorConfig = _ref2.froalaEditorConfig,
      _ref2$handleCloseCont = _ref2.handleCloseContextMenu,
      handleCloseContextMenu = _ref2$handleCloseCont === void 0 ? function () {} : _ref2$handleCloseCont,
      _ref2$handleCopyWidge = _ref2.handleCopyWidget,
      handleCopyWidget = _ref2$handleCopyWidge === void 0 ? function () {} : _ref2$handleCopyWidge,
      _ref2$handleOpenConte = _ref2.handleOpenContextMenu,
      handleOpenContextMenu = _ref2$handleOpenConte === void 0 ? function () {} : _ref2$handleOpenConte,
      _ref2$handleOpenSetti = _ref2.handleOpenSettings,
      handleOpenSettings = _ref2$handleOpenSetti === void 0 ? function () {} : _ref2$handleOpenSetti,
      _ref2$handleRemove = _ref2.handleRemove,
      handleRemove = _ref2$handleRemove === void 0 ? function () {} : _ref2$handleRemove,
      infinitePagesSupportEnabled = _ref2.infinitePagesSupportEnabled,
      pageOrientation = _ref2.pageOrientation,
      selected = _ref2.selected,
      selectWidgetId = _ref2.selectWidgetId,
      setWidgetPosition = _ref2.setWidgetPosition,
      setWidgetSize = _ref2.setWidgetSize,
      _ref2$setWidgetValue = _ref2.setWidgetValue,
      setWidgetValue = _ref2$setWidgetValue === void 0 ? function () {} : _ref2$setWidgetValue,
      showGuides = _ref2.showGuides,
      tableWidgetsNames = _ref2.tableWidgetsNames,
      templateType = _ref2.templateType,
      unbindShortcut = _ref2.unbindShortcut,
      widgetObject = _ref2.widgetObject;
  var field = widgetObject.field,
      height = widgetObject.height,
      id = widgetObject.id,
      name = widgetObject.name,
      replaceInfoRequired = widgetObject.replaceInfoRequired,
      style = widgetObject.style,
      value = widgetObject.value,
      width = widgetObject.width,
      x = widgetObject.x,
      y = widgetObject.y;
  var generalGrid = [!showGuides ? 1 : _template.GRID_WIDTH, !showGuides ? 1 : _template.GRID_HEIGHT];
  var openSettingsDisabled = (0, _helpers.getOpenSettingsDisabled)(widgetObject, templateType, chartWidgetsNames, tableWidgetsNames);

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showMenu = _useState2[0],
      setShowMenu = _useState2[1];

  (0, _react.useEffect)(function () {
    if (!selected && showMenu) {
      setShowMenu(false);
    }

    var handleShortcuts = function handleShortcuts(e, combo) {
      if (combo === 'command+c' || combo === 'ctrl+c') {
        handleCopyWidget();
      } else {
        handleCut();
      }
    };

    if (selected) {
      bindShortcut(['command+c', 'ctrl+c', 'command+x', 'ctrl+x'], handleShortcuts);
    } else {
      unbindShortcut(['command+c', 'ctrl+c', 'command+x', 'ctrl+x']);
    }

    return function () {
      unbindShortcut(['command+c', 'ctrl+c', 'command+x', 'ctrl+x']);
    };
  }, [selected]);

  var handleCut = function handleCut() {
    handleCopyWidget();
    handleRemove();
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactDraggable.default, {
    grid: generalGrid,
    defaultPosition: {
      x: x,
      y: y
    },
    position: {
      x: x,
      y: y
    },
    handle: ".handler-section",
    bounds: "parent",
    onStart: (0, _helpers.handleOnStartDragBuild)(id, pageOrientation, setWidgetPosition, showGuides),
    onStop: function onStop(e, _ref3) {
      var lastX = _ref3.lastX,
          lastY = _ref3.lastY;
      return setWidgetPosition(id, lastX, lastY);
    },
    disabled: editionLevel === _types.NO_EDITION_MODE,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reResizable.Resizable, {
      size: {
        width: width,
        height: height
      },
      bounds: "parent",
      enable: {
        bottomRight: editionLevel !== _types.NO_EDITION_MODE
      },
      grid: generalGrid,
      minWidth: 1,
      minHeight: 1,
      handleStyles: {
        bottomRight: {
          zIndex: 3
        }
      },
      onResizeStart: (0, _helpers.handleOnStartResizeBuild)(id, pageOrientation, setWidgetPosition, showGuides, x, y),
      onResizeStop: function onResizeStop(e, dir, r, d) {
        return setWidgetSize(id, d.width, d.height);
      },
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(DraggableBody, {
        className: "draggable-body ".concat(selected && 'selected', " ").concat((showGuides || editionLevel !== _types.NO_EDITION_MODE) && 'design-mode'),
        editionDisabled: editionLevel === _types.NO_EDITION_MODE,
        onClick: function onClick() {
          return selectWidgetId(id);
        },
        onContextMenu: (0, _helpers.handleRightClickBuild)(handleCloseContextMenu, handleOpenContextMenu, id, openSettingsDisabled, selectWidgetId, editionLevel),
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetItem.default, {
          chartWidgetsNames: chartWidgetsNames,
          currencyConfig: currencyConfig,
          froalaEditorConfig: froalaEditorConfig,
          handleChangeValue: function handleChangeValue(newValue) {
            return setWidgetValue(id, newValue);
          },
          height: height,
          id: id,
          infinitePagesSupportEnabled: infinitePagesSupportEnabled,
          isSelected: selected,
          editionLevel: editionLevel,
          replaceInfoRequired: replaceInfoRequired,
          style: style,
          tableWidgetsNames: tableWidgetsNames,
          type: name,
          value: value,
          width: width
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_HandlerResize.default, {
          show: editionLevel !== _types.NO_EDITION_MODE
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_Toolbar.default, {
        editionLevel: editionLevel,
        handleCopyWidget: handleCopyWidget,
        handleCut: handleCut,
        handleOpenSettings: handleOpenSettings,
        handleRemove: handleRemove,
        keyName: field,
        openSettingsDisabled: openSettingsDisabled,
        show: selected,
        showMenu: showMenu
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_CornerMarker.default, {
        hide: !selected || !showGuides || editionLevel === _types.NO_EDITION_MODE,
        horizontalAlign: "left",
        verticalAlign: "bottom"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_CornerMarker.default, {
        hide: !selected || !showGuides || editionLevel === _types.NO_EDITION_MODE,
        horizontalAlign: "right",
        verticalAlign: "bottom"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_CornerMarker.default, {
        hide: !selected || !showGuides || editionLevel === _types.NO_EDITION_MODE,
        horizontalAlign: "left",
        verticalAlign: "top"
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_CornerMarker.default, {
        hide: !selected || !showGuides || editionLevel === _types.NO_EDITION_MODE,
        horizontalAlign: "right",
        verticalAlign: "top"
      })]
    })
  });
};

GridItem.propTypes = {
  bindShortcut: _propTypes.default.func,
  chartWidgetsNames: _propTypes.default.array,
  currencyConfig: _propTypes.default.object,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  handleCloseContextMenu: _propTypes.default.func,
  handleCopyWidget: _propTypes.default.func,
  handleOpenContextMenu: _propTypes.default.func,
  handleOpenSettings: _propTypes.default.func,
  handleRemove: _propTypes.default.func,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  pageOrientation: _propTypes.default.string,
  selected: _propTypes.default.bool,
  selectWidgetId: _propTypes.default.func,
  setWidgetPosition: _propTypes.default.func,
  setWidgetSize: _propTypes.default.func,
  setWidgetValue: _propTypes.default.func,
  showGuides: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  templateType: _propTypes.default.number,
  unbindShortcut: _propTypes.default.func,
  widgetObject: _propTypes.default.object
};

var _default = (0, _reactMousetrap.default)(GridItem);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL2luZGV4LmpzIl0sIm5hbWVzIjpbIkRyYWdnYWJsZUJvZHkiLCJzdHlsZWQiLCJkaXYiLCJlZGl0aW9uRGlzYWJsZWQiLCJHcmlkSXRlbSIsImJpbmRTaG9ydGN1dCIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiY3VycmVuY3lDb25maWciLCJlZGl0aW9uTGV2ZWwiLCJmcm9hbGFFZGl0b3JDb25maWciLCJoYW5kbGVDbG9zZUNvbnRleHRNZW51IiwiaGFuZGxlQ29weVdpZGdldCIsImhhbmRsZU9wZW5Db250ZXh0TWVudSIsImhhbmRsZU9wZW5TZXR0aW5ncyIsImhhbmRsZVJlbW92ZSIsImluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZCIsInBhZ2VPcmllbnRhdGlvbiIsInNlbGVjdGVkIiwic2VsZWN0V2lkZ2V0SWQiLCJzZXRXaWRnZXRQb3NpdGlvbiIsInNldFdpZGdldFNpemUiLCJzZXRXaWRnZXRWYWx1ZSIsInNob3dHdWlkZXMiLCJ0YWJsZVdpZGdldHNOYW1lcyIsInRlbXBsYXRlVHlwZSIsInVuYmluZFNob3J0Y3V0Iiwid2lkZ2V0T2JqZWN0IiwiZmllbGQiLCJoZWlnaHQiLCJpZCIsIm5hbWUiLCJyZXBsYWNlSW5mb1JlcXVpcmVkIiwic3R5bGUiLCJ2YWx1ZSIsIndpZHRoIiwieCIsInkiLCJnZW5lcmFsR3JpZCIsIkdSSURfV0lEVEgiLCJHUklEX0hFSUdIVCIsIm9wZW5TZXR0aW5nc0Rpc2FibGVkIiwic2hvd01lbnUiLCJzZXRTaG93TWVudSIsImhhbmRsZVNob3J0Y3V0cyIsImUiLCJjb21ibyIsImhhbmRsZUN1dCIsImxhc3RYIiwibGFzdFkiLCJOT19FRElUSU9OX01PREUiLCJib3R0b21SaWdodCIsInpJbmRleCIsImRpciIsInIiLCJkIiwibmV3VmFsdWUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwiYXJyYXkiLCJvYmplY3QiLCJzdHJpbmciLCJib29sIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBT0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxhQUFhLEdBQUdDLDBCQUFPQyxHQUFWLHNjQUdiO0FBQUEsTUFBR0MsZUFBSCxRQUFHQSxlQUFIO0FBQUEsU0FBeUJBLGVBQWUsSUFBSSx1QkFBNUM7QUFBQSxDQUhhLENBQW5COztBQW9CQSxJQUFNQyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxRQXVCWDtBQUFBLE1BdEJGQyxZQXNCRSxTQXRCRkEsWUFzQkU7QUFBQSxNQXJCRkMsaUJBcUJFLFNBckJGQSxpQkFxQkU7QUFBQSxNQXBCRkMsY0FvQkUsU0FwQkZBLGNBb0JFO0FBQUEsTUFuQkZDLFlBbUJFLFNBbkJGQSxZQW1CRTtBQUFBLE1BbEJGQyxrQkFrQkUsU0FsQkZBLGtCQWtCRTtBQUFBLG9DQWpCRkMsc0JBaUJFO0FBQUEsTUFqQkZBLHNCQWlCRSxzQ0FqQnVCLFlBQU0sQ0FBRSxDQWlCL0I7QUFBQSxvQ0FoQkZDLGdCQWdCRTtBQUFBLE1BaEJGQSxnQkFnQkUsc0NBaEJpQixZQUFNLENBQUUsQ0FnQnpCO0FBQUEsb0NBZkZDLHFCQWVFO0FBQUEsTUFmRkEscUJBZUUsc0NBZnNCLFlBQU0sQ0FBRSxDQWU5QjtBQUFBLG9DQWRGQyxrQkFjRTtBQUFBLE1BZEZBLGtCQWNFLHNDQWRtQixZQUFNLENBQUUsQ0FjM0I7QUFBQSxpQ0FiRkMsWUFhRTtBQUFBLE1BYkZBLFlBYUUsbUNBYmEsWUFBTSxDQUFFLENBYXJCO0FBQUEsTUFaRkMsMkJBWUUsU0FaRkEsMkJBWUU7QUFBQSxNQVhGQyxlQVdFLFNBWEZBLGVBV0U7QUFBQSxNQVZGQyxRQVVFLFNBVkZBLFFBVUU7QUFBQSxNQVRGQyxjQVNFLFNBVEZBLGNBU0U7QUFBQSxNQVJGQyxpQkFRRSxTQVJGQSxpQkFRRTtBQUFBLE1BUEZDLGFBT0UsU0FQRkEsYUFPRTtBQUFBLG1DQU5GQyxjQU1FO0FBQUEsTUFORkEsY0FNRSxxQ0FOZSxZQUFNLENBQUUsQ0FNdkI7QUFBQSxNQUxGQyxVQUtFLFNBTEZBLFVBS0U7QUFBQSxNQUpGQyxpQkFJRSxTQUpGQSxpQkFJRTtBQUFBLE1BSEZDLFlBR0UsU0FIRkEsWUFHRTtBQUFBLE1BRkZDLGNBRUUsU0FGRkEsY0FFRTtBQUFBLE1BREZDLFlBQ0UsU0FERkEsWUFDRTtBQUNGLE1BQ0lDLEtBREosR0FXSUQsWUFYSixDQUNJQyxLQURKO0FBQUEsTUFFSUMsTUFGSixHQVdJRixZQVhKLENBRUlFLE1BRko7QUFBQSxNQUdJQyxFQUhKLEdBV0lILFlBWEosQ0FHSUcsRUFISjtBQUFBLE1BSUlDLElBSkosR0FXSUosWUFYSixDQUlJSSxJQUpKO0FBQUEsTUFLSUMsbUJBTEosR0FXSUwsWUFYSixDQUtJSyxtQkFMSjtBQUFBLE1BTUlDLEtBTkosR0FXSU4sWUFYSixDQU1JTSxLQU5KO0FBQUEsTUFPSUMsS0FQSixHQVdJUCxZQVhKLENBT0lPLEtBUEo7QUFBQSxNQVFJQyxLQVJKLEdBV0lSLFlBWEosQ0FRSVEsS0FSSjtBQUFBLE1BU0lDLENBVEosR0FXSVQsWUFYSixDQVNJUyxDQVRKO0FBQUEsTUFVSUMsQ0FWSixHQVdJVixZQVhKLENBVUlVLENBVko7QUFhQSxNQUFNQyxXQUFXLEdBQUcsQ0FDaEIsQ0FBQ2YsVUFBRCxHQUFjLENBQWQsR0FBa0JnQixvQkFERixFQUVoQixDQUFDaEIsVUFBRCxHQUFjLENBQWQsR0FBa0JpQixxQkFGRixDQUFwQjtBQUtBLE1BQU1DLG9CQUFvQixHQUFHLHNDQUN6QmQsWUFEeUIsRUFFekJGLFlBRnlCLEVBR3pCbEIsaUJBSHlCLEVBSXpCaUIsaUJBSnlCLENBQTdCOztBQU1BLGtCQUFnQyxxQkFBUyxLQUFULENBQWhDO0FBQUE7QUFBQSxNQUFPa0IsUUFBUDtBQUFBLE1BQWlCQyxXQUFqQjs7QUFFQSx3QkFBVSxZQUFNO0FBQ1osUUFBSSxDQUFDekIsUUFBRCxJQUFhd0IsUUFBakIsRUFBMkI7QUFDdkJDLE1BQUFBLFdBQVcsQ0FBQyxLQUFELENBQVg7QUFDSDs7QUFDRCxRQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLENBQUQsRUFBSUMsS0FBSixFQUFjO0FBQ2xDLFVBQUlBLEtBQUssS0FBSyxXQUFWLElBQXlCQSxLQUFLLEtBQUssUUFBdkMsRUFBaUQ7QUFDN0NsQyxRQUFBQSxnQkFBZ0I7QUFDbkIsT0FGRCxNQUVPO0FBQ0htQyxRQUFBQSxTQUFTO0FBQ1o7QUFDSixLQU5EOztBQU9BLFFBQUk3QixRQUFKLEVBQWM7QUFDVlosTUFBQUEsWUFBWSxDQUNSLENBQUMsV0FBRCxFQUFjLFFBQWQsRUFBd0IsV0FBeEIsRUFBcUMsUUFBckMsQ0FEUSxFQUVSc0MsZUFGUSxDQUFaO0FBSUgsS0FMRCxNQUtPO0FBQ0hsQixNQUFBQSxjQUFjLENBQUMsQ0FBQyxXQUFELEVBQWMsUUFBZCxFQUF3QixXQUF4QixFQUFxQyxRQUFyQyxDQUFELENBQWQ7QUFDSDs7QUFDRCxXQUFPLFlBQU07QUFDVEEsTUFBQUEsY0FBYyxDQUFDLENBQUMsV0FBRCxFQUFjLFFBQWQsRUFBd0IsV0FBeEIsRUFBcUMsUUFBckMsQ0FBRCxDQUFkO0FBQ0gsS0FGRDtBQUdILEdBdEJELEVBc0JHLENBQUNSLFFBQUQsQ0F0Qkg7O0FBd0JBLE1BQU02QixTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQUFNO0FBQ3BCbkMsSUFBQUEsZ0JBQWdCO0FBQ2hCRyxJQUFBQSxZQUFZO0FBQ2YsR0FIRDs7QUFLQSxzQkFDSSxxQkFBQyx1QkFBRDtBQUNJLElBQUEsSUFBSSxFQUFFdUIsV0FEVjtBQUVJLElBQUEsZUFBZSxFQUFFO0FBQUVGLE1BQUFBLENBQUMsRUFBREEsQ0FBRjtBQUFLQyxNQUFBQSxDQUFDLEVBQURBO0FBQUwsS0FGckI7QUFHSSxJQUFBLFFBQVEsRUFBRTtBQUFFRCxNQUFBQSxDQUFDLEVBQURBLENBQUY7QUFBS0MsTUFBQUEsQ0FBQyxFQUFEQTtBQUFMLEtBSGQ7QUFJSSxJQUFBLE1BQU0sRUFBQyxrQkFKWDtBQUtJLElBQUEsTUFBTSxFQUFDLFFBTFg7QUFNSSxJQUFBLE9BQU8sRUFBRSxxQ0FDTFAsRUFESyxFQUVMYixlQUZLLEVBR0xHLGlCQUhLLEVBSUxHLFVBSkssQ0FOYjtBQVlJLElBQUEsTUFBTSxFQUFFLGdCQUFDc0IsQ0FBRDtBQUFBLFVBQU1HLEtBQU4sU0FBTUEsS0FBTjtBQUFBLFVBQWFDLEtBQWIsU0FBYUEsS0FBYjtBQUFBLGFBQ0o3QixpQkFBaUIsQ0FBQ1UsRUFBRCxFQUFLa0IsS0FBTCxFQUFZQyxLQUFaLENBRGI7QUFBQSxLQVpaO0FBZUksSUFBQSxRQUFRLEVBQUV4QyxZQUFZLEtBQUt5QyxzQkFmL0I7QUFBQSwyQkFpQkksc0JBQUMsc0JBQUQ7QUFDSSxNQUFBLElBQUksRUFBRTtBQUFFZixRQUFBQSxLQUFLLEVBQUxBLEtBQUY7QUFBU04sUUFBQUEsTUFBTSxFQUFOQTtBQUFULE9BRFY7QUFFSSxNQUFBLE1BQU0sRUFBQyxRQUZYO0FBR0ksTUFBQSxNQUFNLEVBQUU7QUFBRXNCLFFBQUFBLFdBQVcsRUFBRTFDLFlBQVksS0FBS3lDO0FBQWhDLE9BSFo7QUFJSSxNQUFBLElBQUksRUFBRVosV0FKVjtBQUtJLE1BQUEsUUFBUSxFQUFFLENBTGQ7QUFNSSxNQUFBLFNBQVMsRUFBRSxDQU5mO0FBT0ksTUFBQSxZQUFZLEVBQUU7QUFBRWEsUUFBQUEsV0FBVyxFQUFFO0FBQUVDLFVBQUFBLE1BQU0sRUFBRTtBQUFWO0FBQWYsT0FQbEI7QUFRSSxNQUFBLGFBQWEsRUFBRSx1Q0FDWHRCLEVBRFcsRUFFWGIsZUFGVyxFQUdYRyxpQkFIVyxFQUlYRyxVQUpXLEVBS1hhLENBTFcsRUFNWEMsQ0FOVyxDQVJuQjtBQWdCSSxNQUFBLFlBQVksRUFBRSxzQkFBQ1EsQ0FBRCxFQUFJUSxHQUFKLEVBQVNDLENBQVQsRUFBWUMsQ0FBWjtBQUFBLGVBQ1ZsQyxhQUFhLENBQUNTLEVBQUQsRUFBS3lCLENBQUMsQ0FBQ3BCLEtBQVAsRUFBY29CLENBQUMsQ0FBQzFCLE1BQWhCLENBREg7QUFBQSxPQWhCbEI7QUFBQSw4QkFvQkksc0JBQUMsYUFBRDtBQUNJLFFBQUEsU0FBUywyQkFBb0JYLFFBQVEsSUFBSSxVQUFoQyxjQUNMLENBQUNLLFVBQVUsSUFBSWQsWUFBWSxLQUFLeUMsc0JBQWhDLEtBQ0EsYUFGSyxDQURiO0FBS0ksUUFBQSxlQUFlLEVBQUV6QyxZQUFZLEtBQUt5QyxzQkFMdEM7QUFNSSxRQUFBLE9BQU8sRUFBRTtBQUFBLGlCQUFNL0IsY0FBYyxDQUFDVyxFQUFELENBQXBCO0FBQUEsU0FOYjtBQU9JLFFBQUEsYUFBYSxFQUFFLG9DQUNYbkIsc0JBRFcsRUFFWEUscUJBRlcsRUFHWGlCLEVBSFcsRUFJWFcsb0JBSlcsRUFLWHRCLGNBTFcsRUFNWFYsWUFOVyxDQVBuQjtBQUFBLGdDQWdCSSxxQkFBQyxtQkFBRDtBQUNJLFVBQUEsaUJBQWlCLEVBQUVGLGlCQUR2QjtBQUVJLFVBQUEsY0FBYyxFQUFFQyxjQUZwQjtBQUdJLFVBQUEsa0JBQWtCLEVBQUVFLGtCQUh4QjtBQUlJLFVBQUEsaUJBQWlCLEVBQUUsMkJBQUM4QyxRQUFEO0FBQUEsbUJBQ2ZsQyxjQUFjLENBQUNRLEVBQUQsRUFBSzBCLFFBQUwsQ0FEQztBQUFBLFdBSnZCO0FBT0ksVUFBQSxNQUFNLEVBQUUzQixNQVBaO0FBUUksVUFBQSxFQUFFLEVBQUVDLEVBUlI7QUFTSSxVQUFBLDJCQUEyQixFQUN2QmQsMkJBVlI7QUFZSSxVQUFBLFVBQVUsRUFBRUUsUUFaaEI7QUFhSSxVQUFBLFlBQVksRUFBRVQsWUFibEI7QUFjSSxVQUFBLG1CQUFtQixFQUFFdUIsbUJBZHpCO0FBZUksVUFBQSxLQUFLLEVBQUVDLEtBZlg7QUFnQkksVUFBQSxpQkFBaUIsRUFBRVQsaUJBaEJ2QjtBQWlCSSxVQUFBLElBQUksRUFBRU8sSUFqQlY7QUFrQkksVUFBQSxLQUFLLEVBQUVHLEtBbEJYO0FBbUJJLFVBQUEsS0FBSyxFQUFFQztBQW5CWCxVQWhCSixlQXFDSSxxQkFBQyxzQkFBRDtBQUFlLFVBQUEsSUFBSSxFQUFFMUIsWUFBWSxLQUFLeUM7QUFBdEMsVUFyQ0o7QUFBQSxRQXBCSixlQTJESSxxQkFBQyxnQkFBRDtBQUNJLFFBQUEsWUFBWSxFQUFFekMsWUFEbEI7QUFFSSxRQUFBLGdCQUFnQixFQUFFRyxnQkFGdEI7QUFHSSxRQUFBLFNBQVMsRUFBRW1DLFNBSGY7QUFJSSxRQUFBLGtCQUFrQixFQUFFakMsa0JBSnhCO0FBS0ksUUFBQSxZQUFZLEVBQUVDLFlBTGxCO0FBTUksUUFBQSxPQUFPLEVBQUVhLEtBTmI7QUFPSSxRQUFBLG9CQUFvQixFQUFFYSxvQkFQMUI7QUFRSSxRQUFBLElBQUksRUFBRXZCLFFBUlY7QUFTSSxRQUFBLFFBQVEsRUFBRXdCO0FBVGQsUUEzREosZUFzRUkscUJBQUMscUJBQUQ7QUFDSSxRQUFBLElBQUksRUFDQSxDQUFDeEIsUUFBRCxJQUNBLENBQUNLLFVBREQsSUFFQWQsWUFBWSxLQUFLeUMsc0JBSnpCO0FBTUksUUFBQSxlQUFlLEVBQUMsTUFOcEI7QUFPSSxRQUFBLGFBQWEsRUFBQztBQVBsQixRQXRFSixlQStFSSxxQkFBQyxxQkFBRDtBQUNJLFFBQUEsSUFBSSxFQUNBLENBQUNoQyxRQUFELElBQ0EsQ0FBQ0ssVUFERCxJQUVBZCxZQUFZLEtBQUt5QyxzQkFKekI7QUFNSSxRQUFBLGVBQWUsRUFBQyxPQU5wQjtBQU9JLFFBQUEsYUFBYSxFQUFDO0FBUGxCLFFBL0VKLGVBd0ZJLHFCQUFDLHFCQUFEO0FBQ0ksUUFBQSxJQUFJLEVBQ0EsQ0FBQ2hDLFFBQUQsSUFDQSxDQUFDSyxVQURELElBRUFkLFlBQVksS0FBS3lDLHNCQUp6QjtBQU1JLFFBQUEsZUFBZSxFQUFDLE1BTnBCO0FBT0ksUUFBQSxhQUFhLEVBQUM7QUFQbEIsUUF4RkosZUFpR0kscUJBQUMscUJBQUQ7QUFDSSxRQUFBLElBQUksRUFDQSxDQUFDaEMsUUFBRCxJQUNBLENBQUNLLFVBREQsSUFFQWQsWUFBWSxLQUFLeUMsc0JBSnpCO0FBTUksUUFBQSxlQUFlLEVBQUMsT0FOcEI7QUFPSSxRQUFBLGFBQWEsRUFBQztBQVBsQixRQWpHSjtBQUFBO0FBakJKLElBREo7QUErSEgsQ0E5TUQ7O0FBZ05BN0MsUUFBUSxDQUFDb0QsU0FBVCxHQUFxQjtBQUNqQm5ELEVBQUFBLFlBQVksRUFBRW9ELG1CQUFVQyxJQURQO0FBRWpCcEQsRUFBQUEsaUJBQWlCLEVBQUVtRCxtQkFBVUUsS0FGWjtBQUdqQnBELEVBQUFBLGNBQWMsRUFBRWtELG1CQUFVRyxNQUhUO0FBSWpCcEQsRUFBQUEsWUFBWSxFQUFFaUQsbUJBQVVJLE1BSlA7QUFLakJwRCxFQUFBQSxrQkFBa0IsRUFBRWdELG1CQUFVRyxNQUxiO0FBTWpCbEQsRUFBQUEsc0JBQXNCLEVBQUUrQyxtQkFBVUMsSUFOakI7QUFPakIvQyxFQUFBQSxnQkFBZ0IsRUFBRThDLG1CQUFVQyxJQVBYO0FBUWpCOUMsRUFBQUEscUJBQXFCLEVBQUU2QyxtQkFBVUMsSUFSaEI7QUFTakI3QyxFQUFBQSxrQkFBa0IsRUFBRTRDLG1CQUFVQyxJQVRiO0FBVWpCNUMsRUFBQUEsWUFBWSxFQUFFMkMsbUJBQVVDLElBVlA7QUFXakIzQyxFQUFBQSwyQkFBMkIsRUFBRTBDLG1CQUFVSyxJQVh0QjtBQVlqQjlDLEVBQUFBLGVBQWUsRUFBRXlDLG1CQUFVSSxNQVpWO0FBYWpCNUMsRUFBQUEsUUFBUSxFQUFFd0MsbUJBQVVLLElBYkg7QUFjakI1QyxFQUFBQSxjQUFjLEVBQUV1QyxtQkFBVUMsSUFkVDtBQWVqQnZDLEVBQUFBLGlCQUFpQixFQUFFc0MsbUJBQVVDLElBZlo7QUFnQmpCdEMsRUFBQUEsYUFBYSxFQUFFcUMsbUJBQVVDLElBaEJSO0FBaUJqQnJDLEVBQUFBLGNBQWMsRUFBRW9DLG1CQUFVQyxJQWpCVDtBQWtCakJwQyxFQUFBQSxVQUFVLEVBQUVtQyxtQkFBVUssSUFsQkw7QUFtQmpCdkMsRUFBQUEsaUJBQWlCLEVBQUVrQyxtQkFBVUUsS0FuQlo7QUFvQmpCbkMsRUFBQUEsWUFBWSxFQUFFaUMsbUJBQVVNLE1BcEJQO0FBcUJqQnRDLEVBQUFBLGNBQWMsRUFBRWdDLG1CQUFVQyxJQXJCVDtBQXNCakJoQyxFQUFBQSxZQUFZLEVBQUUrQixtQkFBVUc7QUF0QlAsQ0FBckI7O2VBeUJlLDZCQUFVeEQsUUFBVixDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IFJlc2l6YWJsZSB9IGZyb20gJ3JlLXJlc2l6YWJsZSc7XG5pbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IERyYWdnYWJsZSBmcm9tICdyZWFjdC1kcmFnZ2FibGUnO1xuaW1wb3J0IG1vdXNlVHJhcCBmcm9tICdyZWFjdC1tb3VzZXRyYXAnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IEdSSURfV0lEVEgsIEdSSURfSEVJR0hUIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XG5pbXBvcnQgeyBOT19FRElUSU9OX01PREUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcblxuaW1wb3J0IHtcbiAgICBnZXRPcGVuU2V0dGluZ3NEaXNhYmxlZCxcbiAgICBoYW5kbGVPblN0YXJ0RHJhZ0J1aWxkLFxuICAgIGhhbmRsZU9uU3RhcnRSZXNpemVCdWlsZCxcbiAgICBoYW5kbGVSaWdodENsaWNrQnVpbGQsXG59IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xuXG5pbXBvcnQgQ29ybmVyTWFya2VyIGZyb20gJy4vQ29ybmVyTWFya2VyJztcbmltcG9ydCBIYW5kbGVyUmVzaXplIGZyb20gJy4vSGFuZGxlclJlc2l6ZSc7XG5pbXBvcnQgVG9vbGJhciBmcm9tICcuL1Rvb2xiYXInO1xuaW1wb3J0IFdpZGdldEl0ZW0gZnJvbSAnLi9XaWRnZXRJdGVtJztcblxuY29uc3QgRHJhZ2dhYmxlQm9keSA9IHN0eWxlZC5kaXZgXG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgJHsoeyBlZGl0aW9uRGlzYWJsZWQgfSkgPT4gZWRpdGlvbkRpc2FibGVkICYmICdwb2ludGVyLWV2ZW50czogbm9uZTsnfVxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4ycyBsaW5lYXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgei1pbmRleDogMjtcblxuICAgICYuZGVzaWduLW1vZGUge1xuICAgICAgICBvdXRsaW5lOiAxcHggZGFzaGVkICNiYmI7XG4gICAgfVxuXG4gICAgJi5zZWxlY3RlZCB7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCAyMHB4IHJnYmEoMCwgMCwgMCwgMC4xOSksXG4gICAgICAgICAgICAwIDZweCA2cHggcmdiYSgwLCAwLCAwLCAwLjIzKTtcbiAgICAgICAgb3V0bGluZTogMXB4IHNvbGlkIHJnYigyNTUsIDE1NCwgMCk7XG4gICAgfVxuYDtcblxuY29uc3QgR3JpZEl0ZW0gPSAoe1xuICAgIGJpbmRTaG9ydGN1dCxcbiAgICBjaGFydFdpZGdldHNOYW1lcyxcbiAgICBjdXJyZW5jeUNvbmZpZyxcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnLFxuICAgIGhhbmRsZUNsb3NlQ29udGV4dE1lbnUgPSAoKSA9PiB7fSxcbiAgICBoYW5kbGVDb3B5V2lkZ2V0ID0gKCkgPT4ge30sXG4gICAgaGFuZGxlT3BlbkNvbnRleHRNZW51ID0gKCkgPT4ge30sXG4gICAgaGFuZGxlT3BlblNldHRpbmdzID0gKCkgPT4ge30sXG4gICAgaGFuZGxlUmVtb3ZlID0gKCkgPT4ge30sXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkLFxuICAgIHBhZ2VPcmllbnRhdGlvbixcbiAgICBzZWxlY3RlZCxcbiAgICBzZWxlY3RXaWRnZXRJZCxcbiAgICBzZXRXaWRnZXRQb3NpdGlvbixcbiAgICBzZXRXaWRnZXRTaXplLFxuICAgIHNldFdpZGdldFZhbHVlID0gKCkgPT4ge30sXG4gICAgc2hvd0d1aWRlcyxcbiAgICB0YWJsZVdpZGdldHNOYW1lcyxcbiAgICB0ZW1wbGF0ZVR5cGUsXG4gICAgdW5iaW5kU2hvcnRjdXQsXG4gICAgd2lkZ2V0T2JqZWN0LFxufSkgPT4ge1xuICAgIGNvbnN0IHtcbiAgICAgICAgZmllbGQsXG4gICAgICAgIGhlaWdodCxcbiAgICAgICAgaWQsXG4gICAgICAgIG5hbWUsXG4gICAgICAgIHJlcGxhY2VJbmZvUmVxdWlyZWQsXG4gICAgICAgIHN0eWxlLFxuICAgICAgICB2YWx1ZSxcbiAgICAgICAgd2lkdGgsXG4gICAgICAgIHgsXG4gICAgICAgIHksXG4gICAgfSA9IHdpZGdldE9iamVjdDtcblxuICAgIGNvbnN0IGdlbmVyYWxHcmlkID0gW1xuICAgICAgICAhc2hvd0d1aWRlcyA/IDEgOiBHUklEX1dJRFRILFxuICAgICAgICAhc2hvd0d1aWRlcyA/IDEgOiBHUklEX0hFSUdIVCxcbiAgICBdO1xuXG4gICAgY29uc3Qgb3BlblNldHRpbmdzRGlzYWJsZWQgPSBnZXRPcGVuU2V0dGluZ3NEaXNhYmxlZChcbiAgICAgICAgd2lkZ2V0T2JqZWN0LFxuICAgICAgICB0ZW1wbGF0ZVR5cGUsXG4gICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzLFxuICAgICAgICB0YWJsZVdpZGdldHNOYW1lc1xuICAgICk7XG4gICAgY29uc3QgW3Nob3dNZW51LCBzZXRTaG93TWVudV0gPSB1c2VTdGF0ZShmYWxzZSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoIXNlbGVjdGVkICYmIHNob3dNZW51KSB7XG4gICAgICAgICAgICBzZXRTaG93TWVudShmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgaGFuZGxlU2hvcnRjdXRzID0gKGUsIGNvbWJvKSA9PiB7XG4gICAgICAgICAgICBpZiAoY29tYm8gPT09ICdjb21tYW5kK2MnIHx8IGNvbWJvID09PSAnY3RybCtjJykge1xuICAgICAgICAgICAgICAgIGhhbmRsZUNvcHlXaWRnZXQoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaGFuZGxlQ3V0KCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIGlmIChzZWxlY3RlZCkge1xuICAgICAgICAgICAgYmluZFNob3J0Y3V0KFxuICAgICAgICAgICAgICAgIFsnY29tbWFuZCtjJywgJ2N0cmwrYycsICdjb21tYW5kK3gnLCAnY3RybCt4J10sXG4gICAgICAgICAgICAgICAgaGFuZGxlU2hvcnRjdXRzXG4gICAgICAgICAgICApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdW5iaW5kU2hvcnRjdXQoWydjb21tYW5kK2MnLCAnY3RybCtjJywgJ2NvbW1hbmQreCcsICdjdHJsK3gnXSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICgpID0+IHtcbiAgICAgICAgICAgIHVuYmluZFNob3J0Y3V0KFsnY29tbWFuZCtjJywgJ2N0cmwrYycsICdjb21tYW5kK3gnLCAnY3RybCt4J10pO1xuICAgICAgICB9O1xuICAgIH0sIFtzZWxlY3RlZF0pO1xuXG4gICAgY29uc3QgaGFuZGxlQ3V0ID0gKCkgPT4ge1xuICAgICAgICBoYW5kbGVDb3B5V2lkZ2V0KCk7XG4gICAgICAgIGhhbmRsZVJlbW92ZSgpO1xuICAgIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8RHJhZ2dhYmxlXG4gICAgICAgICAgICBncmlkPXtnZW5lcmFsR3JpZH1cbiAgICAgICAgICAgIGRlZmF1bHRQb3NpdGlvbj17eyB4LCB5IH19XG4gICAgICAgICAgICBwb3NpdGlvbj17eyB4LCB5IH19XG4gICAgICAgICAgICBoYW5kbGU9XCIuaGFuZGxlci1zZWN0aW9uXCJcbiAgICAgICAgICAgIGJvdW5kcz1cInBhcmVudFwiXG4gICAgICAgICAgICBvblN0YXJ0PXtoYW5kbGVPblN0YXJ0RHJhZ0J1aWxkKFxuICAgICAgICAgICAgICAgIGlkLFxuICAgICAgICAgICAgICAgIHBhZ2VPcmllbnRhdGlvbixcbiAgICAgICAgICAgICAgICBzZXRXaWRnZXRQb3NpdGlvbixcbiAgICAgICAgICAgICAgICBzaG93R3VpZGVzXG4gICAgICAgICAgICApfVxuICAgICAgICAgICAgb25TdG9wPXsoZSwgeyBsYXN0WCwgbGFzdFkgfSkgPT5cbiAgICAgICAgICAgICAgICBzZXRXaWRnZXRQb3NpdGlvbihpZCwgbGFzdFgsIGxhc3RZKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZGlzYWJsZWQ9e2VkaXRpb25MZXZlbCA9PT0gTk9fRURJVElPTl9NT0RFfVxuICAgICAgICA+XG4gICAgICAgICAgICA8UmVzaXphYmxlXG4gICAgICAgICAgICAgICAgc2l6ZT17eyB3aWR0aCwgaGVpZ2h0IH19XG4gICAgICAgICAgICAgICAgYm91bmRzPVwicGFyZW50XCJcbiAgICAgICAgICAgICAgICBlbmFibGU9e3sgYm90dG9tUmlnaHQ6IGVkaXRpb25MZXZlbCAhPT0gTk9fRURJVElPTl9NT0RFIH19XG4gICAgICAgICAgICAgICAgZ3JpZD17Z2VuZXJhbEdyaWR9XG4gICAgICAgICAgICAgICAgbWluV2lkdGg9ezF9XG4gICAgICAgICAgICAgICAgbWluSGVpZ2h0PXsxfVxuICAgICAgICAgICAgICAgIGhhbmRsZVN0eWxlcz17eyBib3R0b21SaWdodDogeyB6SW5kZXg6IDMgfSB9fVxuICAgICAgICAgICAgICAgIG9uUmVzaXplU3RhcnQ9e2hhbmRsZU9uU3RhcnRSZXNpemVCdWlsZChcbiAgICAgICAgICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICAgICAgICAgIHBhZ2VPcmllbnRhdGlvbixcbiAgICAgICAgICAgICAgICAgICAgc2V0V2lkZ2V0UG9zaXRpb24sXG4gICAgICAgICAgICAgICAgICAgIHNob3dHdWlkZXMsXG4gICAgICAgICAgICAgICAgICAgIHgsXG4gICAgICAgICAgICAgICAgICAgIHlcbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIG9uUmVzaXplU3RvcD17KGUsIGRpciwgciwgZCkgPT5cbiAgICAgICAgICAgICAgICAgICAgc2V0V2lkZ2V0U2l6ZShpZCwgZC53aWR0aCwgZC5oZWlnaHQpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxEcmFnZ2FibGVCb2R5XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YGRyYWdnYWJsZS1ib2R5ICR7c2VsZWN0ZWQgJiYgJ3NlbGVjdGVkJ30gJHtcbiAgICAgICAgICAgICAgICAgICAgICAgIChzaG93R3VpZGVzIHx8IGVkaXRpb25MZXZlbCAhPT0gTk9fRURJVElPTl9NT0RFKSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgJ2Rlc2lnbi1tb2RlJ1xuICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkRpc2FibGVkPXtlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERX1cbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gc2VsZWN0V2lkZ2V0SWQoaWQpfVxuICAgICAgICAgICAgICAgICAgICBvbkNvbnRleHRNZW51PXtoYW5kbGVSaWdodENsaWNrQnVpbGQoXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbG9zZUNvbnRleHRNZW51LFxuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlT3BlbkNvbnRleHRNZW51LFxuICAgICAgICAgICAgICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICBvcGVuU2V0dGluZ3NEaXNhYmxlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdFdpZGdldElkLFxuICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsXG4gICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8V2lkZ2V0SXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXM9e2NoYXJ0V2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY3lDb25maWc9e2N1cnJlbmN5Q29uZmlnfVxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZT17KG5ld1ZhbHVlKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFdpZGdldFZhbHVlKGlkLCBuZXdWYWx1ZSlcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD17aGVpZ2h0fVxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxuICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkPXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlzU2VsZWN0ZWQ9e3NlbGVjdGVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlSW5mb1JlcXVpcmVkPXtyZXBsYWNlSW5mb1JlcXVpcmVkfVxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT17bmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoPXt3aWR0aH1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPEhhbmRsZXJSZXNpemUgc2hvdz17ZWRpdGlvbkxldmVsICE9PSBOT19FRElUSU9OX01PREV9IC8+XG4gICAgICAgICAgICAgICAgPC9EcmFnZ2FibGVCb2R5PlxuICAgICAgICAgICAgICAgIDxUb29sYmFyXG4gICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbD17ZWRpdGlvbkxldmVsfVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDb3B5V2lkZ2V0PXtoYW5kbGVDb3B5V2lkZ2V0fVxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDdXQ9e2hhbmRsZUN1dH1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlT3BlblNldHRpbmdzPXtoYW5kbGVPcGVuU2V0dGluZ3N9XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVJlbW92ZT17aGFuZGxlUmVtb3ZlfVxuICAgICAgICAgICAgICAgICAgICBrZXlOYW1lPXtmaWVsZH1cbiAgICAgICAgICAgICAgICAgICAgb3BlblNldHRpbmdzRGlzYWJsZWQ9e29wZW5TZXR0aW5nc0Rpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgICBzaG93PXtzZWxlY3RlZH1cbiAgICAgICAgICAgICAgICAgICAgc2hvd01lbnU9e3Nob3dNZW51fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPENvcm5lck1hcmtlclxuICAgICAgICAgICAgICAgICAgICBoaWRlPXtcbiAgICAgICAgICAgICAgICAgICAgICAgICFzZWxlY3RlZCB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgIXNob3dHdWlkZXMgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gTk9fRURJVElPTl9NT0RFXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWduPVwibGVmdFwiXG4gICAgICAgICAgICAgICAgICAgIHZlcnRpY2FsQWxpZ249XCJib3R0b21cIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPENvcm5lck1hcmtlclxuICAgICAgICAgICAgICAgICAgICBoaWRlPXtcbiAgICAgICAgICAgICAgICAgICAgICAgICFzZWxlY3RlZCB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgIXNob3dHdWlkZXMgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gTk9fRURJVElPTl9NT0RFXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbEFsaWduPVwicmlnaHRcIlxuICAgICAgICAgICAgICAgICAgICB2ZXJ0aWNhbEFsaWduPVwiYm90dG9tXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxDb3JuZXJNYXJrZXJcbiAgICAgICAgICAgICAgICAgICAgaGlkZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAhc2VsZWN0ZWQgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICFzaG93R3VpZGVzIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbj1cImxlZnRcIlxuICAgICAgICAgICAgICAgICAgICB2ZXJ0aWNhbEFsaWduPVwidG9wXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxDb3JuZXJNYXJrZXJcbiAgICAgICAgICAgICAgICAgICAgaGlkZT17XG4gICAgICAgICAgICAgICAgICAgICAgICAhc2VsZWN0ZWQgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICFzaG93R3VpZGVzIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxBbGlnbj1cInJpZ2h0XCJcbiAgICAgICAgICAgICAgICAgICAgdmVydGljYWxBbGlnbj1cInRvcFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvUmVzaXphYmxlPlxuICAgICAgICA8L0RyYWdnYWJsZT5cbiAgICApO1xufTtcblxuR3JpZEl0ZW0ucHJvcFR5cGVzID0ge1xuICAgIGJpbmRTaG9ydGN1dDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgY2hhcnRXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjdXJyZW5jeUNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGhhbmRsZUNsb3NlQ29udGV4dE1lbnU6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZUNvcHlXaWRnZXQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZU9wZW5Db250ZXh0TWVudTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlT3BlblNldHRpbmdzOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYW5kbGVSZW1vdmU6IFByb3BUeXBlcy5mdW5jLFxuICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgcGFnZU9yaWVudGF0aW9uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZWxlY3RXaWRnZXRJZDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2V0V2lkZ2V0UG9zaXRpb246IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldFdpZGdldFNpemU6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldFdpZGdldFZhbHVlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzaG93R3VpZGVzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0YWJsZVdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHRlbXBsYXRlVHlwZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICB1bmJpbmRTaG9ydGN1dDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgd2lkZ2V0T2JqZWN0OiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgbW91c2VUcmFwKEdyaWRJdGVtKTtcbiJdfQ==