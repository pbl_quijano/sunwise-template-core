"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _types = require("../../../../constants/types");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background-color: rgb(255, 154, 0);\n    display: flex;\n    height: 20px;\n    left: 0;\n    outline: 1px solid rgb(255, 154, 0);\n    padding-left: 4px;\n    position: absolute;\n    top: -21px;\n    transition: all 0.2s linear;\n    width: 100%;\n    z-index: 4;\n"])));

var HandlerSection = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    align-items: center;\n    cursor: move;\n    display: flex;\n    flex-grow: 1;\n    height: 20px;\n    overflow: hidden;\n    padding-right: 6px;\n    white-space: nowrap;\n"])));

var NameText = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #fff;\n    font-size: 11px;\n    height: 14px;\n    line-height: 14px;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    text-transform: capitalize;\n"])));

var StyledIcon = _styledComponents.default.i(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    color: #ffffff !important;\n    font-size: 14px;\n    line-height: 18px;\n"])));

var DropdownToggle = (0, _styledComponents.default)(_reactBootstrap.Dropdown.Toggle)(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    background-color: transparent;\n    border-color: transparent;\n    display: flex;\n    height: 20px;\n    justify-content: center;\n    padding: 0;\n    width: 20px;\n\n    &::after {\n        display: none;\n    }\n"])));
var DropdownItem = (0, _styledComponents.default)(_reactBootstrap.Dropdown.Item)(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    color: #848bab !important;\n    font-size: 12px;\n    line-height: 14px;\n    padding: 6px 16px 7px 16px;\n"])));

var OptionIcon = _styledComponents.default.i(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 12px;\n    line-height: 12px;\n    margin-right: 9px;\n    min-height: 10px;\n"])));

var Toolbar = function Toolbar(_ref) {
  var editionLevel = _ref.editionLevel,
      handleCopyWidget = _ref.handleCopyWidget,
      handleCut = _ref.handleCut,
      handleOpenSettings = _ref.handleOpenSettings,
      handleRemove = _ref.handleRemove,
      keyName = _ref.keyName,
      openSettingsDisabled = _ref.openSettingsDisabled,
      show = _ref.show,
      showMenu = _ref.showMenu;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  if (!show) return null;
  var deleteAndCopyDisabled = editionLevel === _types.PARTIAL_EDITION_MODE;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    className: "draggable-tab",
    showMenu: showMenu,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(HandlerSection, {
      className: "handler-section cursor",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(NameText, {
        children: keyName
      })
    }), (!deleteAndCopyDisabled || !openSettingsDisabled) && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Dropdown, {
      alignRight: true,
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(DropdownToggle, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledIcon, {
          className: "fas fa-ellipsis-v"
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Dropdown.Menu, {
        children: [!openSettingsDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
          onClick: handleOpenSettings,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
            className: "fa fa-cog"
          }), ' ', t('Set up')]
        }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
          onClick: handleCopyWidget,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
            className: "fas fa-copy"
          }), ' ', t('Copy')]
        }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
          onClick: handleCut,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
            className: "fas fa-cut"
          }), " ", t('Cut')]
        }), !deleteAndCopyDisabled && /*#__PURE__*/(0, _jsxRuntime.jsxs)(DropdownItem, {
          onClick: handleRemove,
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(OptionIcon, {
            className: "fas fa-trash"
          }), ' ', t('Delete')]
        })]
      })]
    })]
  });
};

Toolbar.propTypes = {
  editionLevel: _propTypes.default.string,
  handleCopyWidget: _propTypes.default.func,
  handleCut: _propTypes.default.func,
  handleOpenSettings: _propTypes.default.func,
  handleRemove: _propTypes.default.func,
  keyName: _propTypes.default.string,
  openSettingsDisabled: _propTypes.default.bool,
  show: _propTypes.default.bool,
  showMenu: _propTypes.default.bool
};
var _default = Toolbar;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL0dyaWRJdGVtL1Rvb2xiYXIuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiSGFuZGxlclNlY3Rpb24iLCJOYW1lVGV4dCIsInNwYW4iLCJTdHlsZWRJY29uIiwiaSIsIkRyb3Bkb3duVG9nZ2xlIiwiRHJvcGRvd24iLCJUb2dnbGUiLCJEcm9wZG93bkl0ZW0iLCJJdGVtIiwiT3B0aW9uSWNvbiIsIlRvb2xiYXIiLCJlZGl0aW9uTGV2ZWwiLCJoYW5kbGVDb3B5V2lkZ2V0IiwiaGFuZGxlQ3V0IiwiaGFuZGxlT3BlblNldHRpbmdzIiwiaGFuZGxlUmVtb3ZlIiwia2V5TmFtZSIsIm9wZW5TZXR0aW5nc0Rpc2FibGVkIiwic2hvdyIsInNob3dNZW51IiwidCIsImRlbGV0ZUFuZENvcHlEaXNhYmxlZCIsIlBBUlRJQUxfRURJVElPTl9NT0RFIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiZnVuYyIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsc1ZBQWY7O0FBY0EsSUFBTUMsY0FBYyxHQUFHRiwwQkFBT0MsR0FBViwwUEFBcEI7O0FBV0EsSUFBTUUsUUFBUSxHQUFHSCwwQkFBT0ksSUFBVixrUEFBZDs7QUFVQSxJQUFNQyxVQUFVLEdBQUdMLDBCQUFPTSxDQUFWLHVKQUFoQjs7QUFNQSxJQUFNQyxjQUFjLEdBQUcsK0JBQU9DLHlCQUFTQyxNQUFoQixDQUFILHNTQUFwQjtBQWNBLElBQU1DLFlBQVksR0FBRywrQkFBT0YseUJBQVNHLElBQWhCLENBQUgsd0xBQWxCOztBQU9BLElBQU1DLFVBQVUsR0FBR1osMEJBQU9NLENBQVYsMkxBQWhCOztBQVFBLElBQU1PLE9BQU8sR0FBRyxTQUFWQSxPQUFVLE9BVVY7QUFBQSxNQVRGQyxZQVNFLFFBVEZBLFlBU0U7QUFBQSxNQVJGQyxnQkFRRSxRQVJGQSxnQkFRRTtBQUFBLE1BUEZDLFNBT0UsUUFQRkEsU0FPRTtBQUFBLE1BTkZDLGtCQU1FLFFBTkZBLGtCQU1FO0FBQUEsTUFMRkMsWUFLRSxRQUxGQSxZQUtFO0FBQUEsTUFKRkMsT0FJRSxRQUpGQSxPQUlFO0FBQUEsTUFIRkMsb0JBR0UsUUFIRkEsb0JBR0U7QUFBQSxNQUZGQyxJQUVFLFFBRkZBLElBRUU7QUFBQSxNQURGQyxRQUNFLFFBREZBLFFBQ0U7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUVBLE1BQUksQ0FBQ0YsSUFBTCxFQUFXLE9BQU8sSUFBUDtBQUNYLE1BQU1HLHFCQUFxQixHQUFHVixZQUFZLEtBQUtXLDJCQUEvQztBQUNBLHNCQUNJLHNCQUFDLFNBQUQ7QUFBVyxJQUFBLFNBQVMsRUFBQyxlQUFyQjtBQUFxQyxJQUFBLFFBQVEsRUFBRUgsUUFBL0M7QUFBQSw0QkFDSSxxQkFBQyxjQUFEO0FBQWdCLE1BQUEsU0FBUyxFQUFDLHdCQUExQjtBQUFBLDZCQUNJLHFCQUFDLFFBQUQ7QUFBQSxrQkFBV0g7QUFBWDtBQURKLE1BREosRUFJSyxDQUFDLENBQUNLLHFCQUFELElBQTBCLENBQUNKLG9CQUE1QixrQkFDRyxzQkFBQyx3QkFBRDtBQUFVLE1BQUEsVUFBVSxNQUFwQjtBQUFBLDhCQUNJLHFCQUFDLGNBQUQ7QUFBQSwrQkFDSSxxQkFBQyxVQUFEO0FBQVksVUFBQSxTQUFTLEVBQUM7QUFBdEI7QUFESixRQURKLGVBSUksc0JBQUMsd0JBQUQsQ0FBVSxJQUFWO0FBQUEsbUJBQ0ssQ0FBQ0Esb0JBQUQsaUJBQ0csc0JBQUMsWUFBRDtBQUFjLFVBQUEsT0FBTyxFQUFFSCxrQkFBdkI7QUFBQSxrQ0FDSSxxQkFBQyxVQUFEO0FBQVksWUFBQSxTQUFTLEVBQUM7QUFBdEIsWUFESixFQUN5QyxHQUR6QyxFQUVLTSxDQUFDLENBQUMsUUFBRCxDQUZOO0FBQUEsVUFGUixFQU9LLENBQUNDLHFCQUFELGlCQUNHLHNCQUFDLFlBQUQ7QUFBYyxVQUFBLE9BQU8sRUFBRVQsZ0JBQXZCO0FBQUEsa0NBQ0kscUJBQUMsVUFBRDtBQUFZLFlBQUEsU0FBUyxFQUFDO0FBQXRCLFlBREosRUFDMkMsR0FEM0MsRUFFS1EsQ0FBQyxDQUFDLE1BQUQsQ0FGTjtBQUFBLFVBUlIsRUFhSyxDQUFDQyxxQkFBRCxpQkFDRyxzQkFBQyxZQUFEO0FBQWMsVUFBQSxPQUFPLEVBQUVSLFNBQXZCO0FBQUEsa0NBQ0kscUJBQUMsVUFBRDtBQUFZLFlBQUEsU0FBUyxFQUFDO0FBQXRCLFlBREosT0FDMkNPLENBQUMsQ0FBQyxLQUFELENBRDVDO0FBQUEsVUFkUixFQWtCSyxDQUFDQyxxQkFBRCxpQkFDRyxzQkFBQyxZQUFEO0FBQWMsVUFBQSxPQUFPLEVBQUVOLFlBQXZCO0FBQUEsa0NBQ0kscUJBQUMsVUFBRDtBQUFZLFlBQUEsU0FBUyxFQUFDO0FBQXRCLFlBREosRUFDNEMsR0FENUMsRUFFS0ssQ0FBQyxDQUFDLFFBQUQsQ0FGTjtBQUFBLFVBbkJSO0FBQUEsUUFKSjtBQUFBLE1BTFI7QUFBQSxJQURKO0FBdUNILENBdEREOztBQXdEQVYsT0FBTyxDQUFDYSxTQUFSLEdBQW9CO0FBQ2hCWixFQUFBQSxZQUFZLEVBQUVhLG1CQUFVQyxNQURSO0FBRWhCYixFQUFBQSxnQkFBZ0IsRUFBRVksbUJBQVVFLElBRlo7QUFHaEJiLEVBQUFBLFNBQVMsRUFBRVcsbUJBQVVFLElBSEw7QUFJaEJaLEVBQUFBLGtCQUFrQixFQUFFVSxtQkFBVUUsSUFKZDtBQUtoQlgsRUFBQUEsWUFBWSxFQUFFUyxtQkFBVUUsSUFMUjtBQU1oQlYsRUFBQUEsT0FBTyxFQUFFUSxtQkFBVUMsTUFOSDtBQU9oQlIsRUFBQUEsb0JBQW9CLEVBQUVPLG1CQUFVRyxJQVBoQjtBQVFoQlQsRUFBQUEsSUFBSSxFQUFFTSxtQkFBVUcsSUFSQTtBQVNoQlIsRUFBQUEsUUFBUSxFQUFFSyxtQkFBVUc7QUFUSixDQUFwQjtlQVllakIsTyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBEcm9wZG93biB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IFBBUlRJQUxfRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMTU0LCAwKTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBsZWZ0OiAwO1xuICAgIG91dGxpbmU6IDFweCBzb2xpZCByZ2IoMjU1LCAxNTQsIDApO1xuICAgIHBhZGRpbmctbGVmdDogNHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IC0yMXB4O1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGxpbmVhcjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB6LWluZGV4OiA0O1xuYDtcblxuY29uc3QgSGFuZGxlclNlY3Rpb24gPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgY3Vyc29yOiBtb3ZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1ncm93OiAxO1xuICAgIGhlaWdodDogMjBweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHBhZGRpbmctcmlnaHQ6IDZweDtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuYDtcblxuY29uc3QgTmFtZVRleHQgPSBzdHlsZWQuc3BhbmBcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgaGVpZ2h0OiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG5gO1xuXG5jb25zdCBTdHlsZWRJY29uID0gc3R5bGVkLmlgXG4gICAgY29sb3I6ICNmZmZmZmYgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG5gO1xuXG5jb25zdCBEcm9wZG93blRvZ2dsZSA9IHN0eWxlZChEcm9wZG93bi5Ub2dnbGUpYFxuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogMDtcbiAgICB3aWR0aDogMjBweDtcblxuICAgICY6OmFmdGVyIHtcbiAgICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG5gO1xuXG5jb25zdCBEcm9wZG93bkl0ZW0gPSBzdHlsZWQoRHJvcGRvd24uSXRlbSlgXG4gICAgY29sb3I6ICM4NDhiYWIgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE0cHg7XG4gICAgcGFkZGluZzogNnB4IDE2cHggN3B4IDE2cHg7XG5gO1xuXG5jb25zdCBPcHRpb25JY29uID0gc3R5bGVkLmlgXG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxMnB4O1xuICAgIG1hcmdpbi1yaWdodDogOXB4O1xuICAgIG1pbi1oZWlnaHQ6IDEwcHg7XG5gO1xuXG5jb25zdCBUb29sYmFyID0gKHtcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgaGFuZGxlQ29weVdpZGdldCxcbiAgICBoYW5kbGVDdXQsXG4gICAgaGFuZGxlT3BlblNldHRpbmdzLFxuICAgIGhhbmRsZVJlbW92ZSxcbiAgICBrZXlOYW1lLFxuICAgIG9wZW5TZXR0aW5nc0Rpc2FibGVkLFxuICAgIHNob3csXG4gICAgc2hvd01lbnUsXG59KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuXG4gICAgaWYgKCFzaG93KSByZXR1cm4gbnVsbDtcbiAgICBjb25zdCBkZWxldGVBbmRDb3B5RGlzYWJsZWQgPSBlZGl0aW9uTGV2ZWwgPT09IFBBUlRJQUxfRURJVElPTl9NT0RFO1xuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXIgY2xhc3NOYW1lPVwiZHJhZ2dhYmxlLXRhYlwiIHNob3dNZW51PXtzaG93TWVudX0+XG4gICAgICAgICAgICA8SGFuZGxlclNlY3Rpb24gY2xhc3NOYW1lPVwiaGFuZGxlci1zZWN0aW9uIGN1cnNvclwiPlxuICAgICAgICAgICAgICAgIDxOYW1lVGV4dD57a2V5TmFtZX08L05hbWVUZXh0PlxuICAgICAgICAgICAgPC9IYW5kbGVyU2VjdGlvbj5cbiAgICAgICAgICAgIHsoIWRlbGV0ZUFuZENvcHlEaXNhYmxlZCB8fCAhb3BlblNldHRpbmdzRGlzYWJsZWQpICYmIChcbiAgICAgICAgICAgICAgICA8RHJvcGRvd24gYWxpZ25SaWdodD5cbiAgICAgICAgICAgICAgICAgICAgPERyb3Bkb3duVG9nZ2xlPlxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEljb24gY2xhc3NOYW1lPVwiZmFzIGZhLWVsbGlwc2lzLXZcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L0Ryb3Bkb3duVG9nZ2xlPlxuICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd24uTWVudT5cbiAgICAgICAgICAgICAgICAgICAgICAgIHshb3BlblNldHRpbmdzRGlzYWJsZWQgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEcm9wZG93bkl0ZW0gb25DbGljaz17aGFuZGxlT3BlblNldHRpbmdzfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPE9wdGlvbkljb24gY2xhc3NOYW1lPVwiZmEgZmEtY29nXCIgLz57JyAnfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU2V0IHVwJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bkl0ZW0+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgeyFkZWxldGVBbmRDb3B5RGlzYWJsZWQgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEcm9wZG93bkl0ZW0gb25DbGljaz17aGFuZGxlQ29weVdpZGdldH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxPcHRpb25JY29uIGNsYXNzTmFtZT1cImZhcyBmYS1jb3B5XCIgLz57JyAnfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnQ29weScpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRHJvcGRvd25JdGVtPlxuICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHshZGVsZXRlQW5kQ29weURpc2FibGVkICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RHJvcGRvd25JdGVtIG9uQ2xpY2s9e2hhbmRsZUN1dH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxPcHRpb25JY29uIGNsYXNzTmFtZT1cImZhcyBmYS1jdXRcIiAvPiB7dCgnQ3V0Jyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bkl0ZW0+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgeyFkZWxldGVBbmRDb3B5RGlzYWJsZWQgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxEcm9wZG93bkl0ZW0gb25DbGljaz17aGFuZGxlUmVtb3ZlfT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPE9wdGlvbkljb24gY2xhc3NOYW1lPVwiZmFzIGZhLXRyYXNoXCIgLz57JyAnfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnRGVsZXRlJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Ecm9wZG93bkl0ZW0+XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICA8L0Ryb3Bkb3duLk1lbnU+XG4gICAgICAgICAgICAgICAgPC9Ecm9wZG93bj5cbiAgICAgICAgICAgICl9XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5Ub29sYmFyLnByb3BUeXBlcyA9IHtcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGFuZGxlQ29weVdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlQ3V0OiBQcm9wVHlwZXMuZnVuYyxcbiAgICBoYW5kbGVPcGVuU2V0dGluZ3M6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZVJlbW92ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAga2V5TmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBvcGVuU2V0dGluZ3NEaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2hvdzogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2hvd01lbnU6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgVG9vbGJhcjtcbiJdfQ==