"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNull = _interopRequireDefault(require("lodash/isNull"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _types = require("../../../../constants/types");

var _contexts = require("../../../../helpers/contexts");

var templateCoreActions = _interopRequireWildcard(require("../../../TemplateCore/actions"));

var actions = _interopRequireWildcard(require("../../actions"));

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

var _ContextMenu = _interopRequireDefault(require("../ContextMenu"));

var _index = _interopRequireDefault(require("../GridItem/index"));

var _PlaceholderItem = _interopRequireDefault(require("../PlaceholderItem"));

var _SettingsPanel = _interopRequireDefault(require("../SettingsPanel"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    position: relative;\n\n    & .react-draggable {\n        position: absolute !important;\n        top: 0;\n        left: 0;\n    }\n\n    & .react-draggable-dragging .draggable-tab,\n    & .react-draggable-dragging .draggable-body {\n        opacity: 0.4;\n        box-shadow: 0 0 0 transparent;\n    }\n\n    & .react-draggable-dragging .draggable-body {\n        outline: 1px dashed #555;\n    }\n\n    & .react-draggable-dragging .corner-marker {\n        opacity: 1;\n    }\n\n    ", "\n"])), function (_ref) {
  var showGrid = _ref.showGrid;
  return showGrid === true && "\n        box-shadow: inset 0 0 0 48.9px #fff;\n        background-image: linear-gradient(rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(90deg, rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(90deg, rgba(211, 215, 235, 0.5) 1px, transparent 1px);\n        background-size: 16.5px 16.5px;\n        ";
});

var ContextMenuBackDrop = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    z-index: 0;\n"])));

var DuplicateButton = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 24px;\n    right: -46px;\n    width: 36px;\n    height: 36px;\n    border-radius: 50%;\n    background-color: #fff;\n    font-size: 16px;\n    text-align: center;\n    line-height: 36px;\n    color: #7f7f7f;\n    cursor: pointer;\n"])));

var PageWrapper = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    background-color: #fff;\n    /* box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08); */\n    height: ", "px;\n    width: ", "px;\n    min-height: ", "px;\n    min-width: ", "px;\n    position: relative;\n    &.page-design {\n        box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);\n        margin: 24px 32px;\n    }\n    ", "\n"])), function (_ref2) {
  var orientation = _ref2.orientation;
  return orientation === 'landscape' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref3) {
  var orientation = _ref3.orientation;
  return orientation === 'landscape' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
}, function (_ref4) {
  var orientation = _ref4.orientation;
  return orientation === 'landscape' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref5) {
  var orientation = _ref5.orientation;
  return orientation === 'landscape' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
}, function (_ref6) {
  var showOutline = _ref6.showOutline;
  return showOutline && 'outline: #ff9a00 solid 2px;';
});

var PageView = function PageView(_ref7) {
  var addWidget = _ref7.addWidget,
      _ref7$availableCatalo = _ref7.availableCatalogs,
      availableCatalogs = _ref7$availableCatalo === void 0 ? [] : _ref7$availableCatalo,
      chartWidgetsNames = _ref7.chartWidgetsNames,
      className = _ref7.className,
      copiedWidget = _ref7.copiedWidget,
      editionLevel = _ref7.editionLevel,
      droppingItem = _ref7.droppingItem,
      froalaEditorConfig = _ref7.froalaEditorConfig,
      infinitePagesSupportEnabled = _ref7.infinitePagesSupportEnabled,
      moveWidgetByKey = _ref7.moveWidgetByKey,
      onDeleteWidget = _ref7.onDeleteWidget,
      onDuplicatePage = _ref7.onDuplicatePage,
      page = _ref7.page,
      pageDesignDisabled = _ref7.pageDesignDisabled,
      pasteWidget = _ref7.pasteWidget,
      selectedWidgetId = _ref7.selectedWidgetId,
      selectWidget = _ref7.selectWidget,
      setCopiedWidget = _ref7.setCopiedWidget,
      setDroppingItem = _ref7.setDroppingItem,
      setWidgetPosition = _ref7.setWidgetPosition,
      setWidgetSize = _ref7.setWidgetSize,
      setWidgetStyle = _ref7.setWidgetStyle,
      setWidgetValue = _ref7.setWidgetValue,
      showGuides = _ref7.showGuides,
      tableWidgetsNames = _ref7.tableWidgetsNames,
      templateType = _ref7.templateType;
  var wrapperRef = (0, _react.useRef)(null);
  var pagePosition = (0, _react.useMemo)(function () {
    return (0, _helpers.getPagePosition)(wrapperRef.current);
  }, [wrapperRef.current]);
  var settingsPanelRef = (0, _react.useRef)(null);
  var interval = (0, _react.useRef)(null);

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      currencyConfig = _useContext.currencyConfig,
      onChangeInPage = _useContext.onChangeInPage;

  var hasSummarySupport = (0, _react.useMemo)(function () {
    return templateType === _types.MULTIPROPOSAL_TYPE;
  }, [templateType]);

  var _useState = (0, _react.useState)({
    x: 0,
    y: 0,
    pageX: 0,
    pageY: 0,
    show: false
  }),
      _useState2 = _slicedToArray(_useState, 2),
      contextMenuData = _useState2[0],
      setContextMenuData = _useState2[1];

  var pageWidgets = (0, _react.useMemo)(function () {
    return page.widgets;
  }, [page]);
  var selectedWidget = (0, _react.useMemo)(function () {
    return (0, _helpers.getSelectedWidgetValidated)(selectedWidgetId, page.id, pageWidgets);
  }, [selectedWidgetId, page.id, pageWidgets]);
  var openSettingsDisabled = (0, _react.useMemo)(function () {
    return (0, _helpers.getOpenSettingsDisabled)(selectedWidget, templateType, chartWidgetsNames, tableWidgetsNames);
  }, [selectedWidget, templateType, chartWidgetsNames, tableWidgetsNames]);
  (0, _react.useEffect)((0, _helpers.selectedWidgetUseEffect)(selectedWidget && selectedWidget.name, function (keyCode) {
    return moveWidgetByKey(keyCode, page.orientation, handleSetPosition, showGuides, selectedWidget);
  }, interval), [selectedWidget]);
  var handleCloseContextMenu = (0, _react.useCallback)((0, _helpers.handleCloseContextMenuBuild)(setContextMenuData), []);

  var _handleCopyWidget = (0, _react.useCallback)((0, _helpers.handleCopyWidgetBuild)(setCopiedWidget), [setCopiedWidget]);

  var onAddWidget = (0, _react.useCallback)(function (type, defaultPosition) {
    return addWidget(page.id, type, defaultPosition, hasSummarySupport, onChangeInPage);
  }, [page.id, hasSummarySupport, onChangeInPage]);
  var handlePasteElement = (0, _react.useCallback)((0, _helpers.handlePasteBuild)(contextMenuData, copiedWidget, hasSummarySupport, onChangeInPage, pasteWidget, page, pagePosition, showGuides), [contextMenuData, copiedWidget, hasSummarySupport, onChangeInPage, page, pagePosition, showGuides]);
  var handleSetPosition = (0, _react.useCallback)(function (widgetId, changedX, changedY) {
    setWidgetPosition(widgetId, changedX, changedY, onChangeInPage);
  }, [onChangeInPage]);
  var handleSetWidgetSize = (0, _react.useCallback)(function (widgetId, changedWidth, changedHeight) {
    setWidgetSize(widgetId, changedWidth, changedHeight, onChangeInPage);
  }, [onChangeInPage]);
  var handleSetWidgetStyle = (0, _react.useCallback)(function (widgetId, style) {
    setWidgetStyle(widgetId, style, onChangeInPage);
  }, [onChangeInPage]);
  var handleSetWidgetValue = (0, _react.useCallback)(function (widgetId, value) {
    setWidgetValue(widgetId, value, onChangeInPage);
  }, [onChangeInPage]);
  var handleOpenPageContextMenu = (0, _react.useCallback)((0, _helpers.handleOpenPageContextMenuBuild)({
    contextMenuData: contextMenuData,
    openSettingsDisabled: openSettingsDisabled,
    editionLevel: editionLevel,
    selectedWidgetId: selectedWidget && selectedWidget.id,
    selectWidget: selectWidget,
    setContextMenuData: setContextMenuData,
    wrapperRef: wrapperRef
  }), [contextMenuData, openSettingsDisabled, editionLevel, selectedWidget, wrapperRef.current]);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(PageWrapper, {
    orientation: page.orientation,
    className: "".concat(className, " ").concat(!pageDesignDisabled && 'page-design'),
    showOutline: !pageDesignDisabled && infinitePagesSupportEnabled,
    children: [contextMenuData.show && /*#__PURE__*/(0, _jsxRuntime.jsx)(_ContextMenu.default, {
      copiedWidget: copiedWidget,
      editionLevel: editionLevel,
      handleRemove: function handleRemove() {
        return onDeleteWidget(selectedWidget.id, onChangeInPage);
      },
      handleOpenSettings: (0, _helpers.handleOpenSettingsBuild)(selectedWidget, settingsPanelRef),
      handlePasteElement: handlePasteElement,
      openSettingsDisabled: openSettingsDisabled,
      handleCopyWidget: function handleCopyWidget() {
        return _handleCopyWidget(selectedWidget);
      },
      isSelectedItem: selectedWidget && selectedWidget.id !== null,
      onClose: handleCloseContextMenu,
      x: contextMenuData.x,
      y: contextMenuData.y
    }), contextMenuData.show && /*#__PURE__*/(0, _jsxRuntime.jsx)(ContextMenuBackDrop, {
      onClick: function onClick() {
        handleCloseContextMenu();
        selectWidget(null);
      },
      onContextMenu: handleCloseContextMenu
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
      showGrid: showGuides,
      onContextMenu: handleOpenPageContextMenu,
      ref: wrapperRef,
      onClick: (0, _helpers.handleClickContainerBuild)(contextMenuData, handleCloseContextMenu, selectedWidget && selectedWidget.id, selectWidget, wrapperRef),
      children: pageWidgets && pageWidgets.sort(_helpers.sortingWidgetsByOrder).map(function (widget) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_index.default, {
          chartWidgetsNames: chartWidgetsNames,
          currencyConfig: currencyConfig,
          editionLevel: editionLevel,
          froalaEditorConfig: froalaEditorConfig,
          handleCloseContextMenu: handleCloseContextMenu,
          handleCopyWidget: function handleCopyWidget() {
            return _handleCopyWidget(selectedWidget);
          },
          handleOpenContextMenu: (0, _helpers.handleOpenItemContextMenuBuild)(setContextMenuData),
          handleOpenSettings: (0, _helpers.handleOpenSettingsBuild)(selectedWidget, settingsPanelRef),
          handleRemove: function handleRemove() {
            return onDeleteWidget(selectedWidget && selectedWidget.id, onChangeInPage);
          },
          infinitePagesSupportEnabled: infinitePagesSupportEnabled,
          pageOrientation: page.orientation,
          selected: selectedWidget && selectedWidget.id === widget.id,
          selectWidgetId: selectWidget,
          setWidgetPosition: handleSetPosition,
          setWidgetSize: handleSetWidgetSize,
          setWidgetValue: handleSetWidgetValue,
          showGuides: showGuides,
          tableWidgetsNames: tableWidgetsNames,
          templateType: templateType,
          widgetObject: widget
        }, widget.id);
      })
    }), !(0, _isNull.default)(droppingItem) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_PlaceholderItem.default, {
      droppingItem: droppingItem,
      onHandleDrop: (0, _helpers.handleDropBuild)(droppingItem, onAddWidget, setDroppingItem),
      pageOrientation: page.orientation,
      showGuides: showGuides
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_SettingsPanel.default, {
      catalogs: availableCatalogs,
      chartWidgetsNames: chartWidgetsNames,
      proposalSelectorDisabled: editionLevel === _types.PARTIAL_EDITION_MODE,
      ref: settingsPanelRef,
      tableWidgetsNames: tableWidgetsNames,
      setWidgetStyle: handleSetWidgetStyle,
      setWidgetValue: handleSetWidgetValue,
      templateType: templateType
    }), editionLevel === _types.PARTIAL_EDITION_MODE && /*#__PURE__*/(0, _jsxRuntime.jsx)(DuplicateButton, {
      className: "shadow-sm",
      onClick: function onClick() {
        return onDuplicatePage(page.id);
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
        className: "fas fa-copy"
      })
    })]
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  copiedWidget: selectors.getCopiedItem,
  droppingItem: selectors.getDroppingItem,
  selectedWidgetId: selectors.getSelectedWidgetId
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addWidget: function addWidget(pageId, type, defaultPosition, hasSummarySupport, onChangeInPage) {
      return dispatch(templateCoreActions.addWidget(pageId, type, defaultPosition, hasSummarySupport, onChangeInPage));
    },
    moveWidgetByKey: function moveWidgetByKey(keyCode, orientation, setWidgetPosition, showGuides, selectedWidget) {
      return dispatch(actions.moveWidgetByKey(keyCode, orientation, setWidgetPosition, showGuides, selectedWidget));
    },
    onDeleteWidget: function onDeleteWidget(widgetId, onChangeInPage) {
      return dispatch(templateCoreActions.deleteWidget(widgetId, onChangeInPage));
    },
    onDuplicatePage: function onDuplicatePage(id) {
      return dispatch(templateCoreActions.duplicatePage(id));
    },
    setDroppingItem: function setDroppingItem(item) {
      return dispatch(actions.droppingItem(item));
    },
    pasteWidget: function pasteWidget(pageId, options, showGuides, position, hasSummarySupport, onChangeInPage) {
      return dispatch(templateCoreActions.pasteWidget(pageId, options, showGuides, position, hasSummarySupport, onChangeInPage));
    },
    selectWidget: function selectWidget(widgetId, pageId) {
      return dispatch(actions.selectWidget(widgetId, pageId));
    },
    setCopiedWidget: function setCopiedWidget(data) {
      return dispatch(actions.setCopiedItem(data));
    },
    setWidgetPosition: function setWidgetPosition(widgetId, changedX, changedY, onChangeInPage) {
      return dispatch(templateCoreActions.setWidgetPosition(widgetId, changedX, changedY, onChangeInPage));
    },
    setWidgetSize: function setWidgetSize(widgetId, changedWidth, changedHeight, onChangeInPage) {
      return dispatch(templateCoreActions.setWidgetSize(widgetId, changedWidth, changedHeight, onChangeInPage));
    },
    setWidgetStyle: function setWidgetStyle(widgetId, style, onChangeInPage) {
      return dispatch(templateCoreActions.setWidgetStyle(widgetId, style, onChangeInPage));
    },
    setWidgetValue: function setWidgetValue(widgetId, value, onChangeInPage) {
      return dispatch(templateCoreActions.setWidgetValue(widgetId, value, onChangeInPage));
    }
  };
};

PageView.propTypes = {
  addWidget: _propTypes.default.func,
  availableCatalogs: _propTypes.default.array,
  chartWidgetsNames: _propTypes.default.array,
  className: _propTypes.default.string,
  copiedWidget: _propTypes.default.object,
  droppingItem: _propTypes.default.object,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  moveWidgetByKey: _propTypes.default.func,
  onDeleteWidget: _propTypes.default.func,
  onDuplicatePage: _propTypes.default.func,
  page: _propTypes.default.object,
  pageDesignDisabled: _propTypes.default.bool,
  pasteWidget: _propTypes.default.func,
  proposalsNumber: _propTypes.default.number,
  selectedWidgetId: _propTypes.default.string,
  selectWidget: _propTypes.default.func,
  setCopiedWidget: _propTypes.default.func,
  setDroppingItem: _propTypes.default.func,
  setWidgetPosition: _propTypes.default.func,
  setWidgetSize: _propTypes.default.func,
  setWidgetStyle: _propTypes.default.func,
  setWidgetValue: _propTypes.default.func,
  showGuides: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  templateType: _propTypes.default.number
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PageView);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3BhZ2VzL1BhZ2VWaWV3LmpzIl0sIm5hbWVzIjpbIldyYXBwZXIiLCJzdHlsZWQiLCJkaXYiLCJzaG93R3JpZCIsIkNvbnRleHRNZW51QmFja0Ryb3AiLCJEdXBsaWNhdGVCdXR0b24iLCJQYWdlV3JhcHBlciIsIm9yaWVudGF0aW9uIiwiUEFHRV9XSURUSCIsIlBBR0VfSEVJR0hUIiwic2hvd091dGxpbmUiLCJQYWdlVmlldyIsImFkZFdpZGdldCIsImF2YWlsYWJsZUNhdGFsb2dzIiwiY2hhcnRXaWRnZXRzTmFtZXMiLCJjbGFzc05hbWUiLCJjb3BpZWRXaWRnZXQiLCJlZGl0aW9uTGV2ZWwiLCJkcm9wcGluZ0l0ZW0iLCJmcm9hbGFFZGl0b3JDb25maWciLCJpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQiLCJtb3ZlV2lkZ2V0QnlLZXkiLCJvbkRlbGV0ZVdpZGdldCIsIm9uRHVwbGljYXRlUGFnZSIsInBhZ2UiLCJwYWdlRGVzaWduRGlzYWJsZWQiLCJwYXN0ZVdpZGdldCIsInNlbGVjdGVkV2lkZ2V0SWQiLCJzZWxlY3RXaWRnZXQiLCJzZXRDb3BpZWRXaWRnZXQiLCJzZXREcm9wcGluZ0l0ZW0iLCJzZXRXaWRnZXRQb3NpdGlvbiIsInNldFdpZGdldFNpemUiLCJzZXRXaWRnZXRTdHlsZSIsInNldFdpZGdldFZhbHVlIiwic2hvd0d1aWRlcyIsInRhYmxlV2lkZ2V0c05hbWVzIiwidGVtcGxhdGVUeXBlIiwid3JhcHBlclJlZiIsInBhZ2VQb3NpdGlvbiIsImN1cnJlbnQiLCJzZXR0aW5nc1BhbmVsUmVmIiwiaW50ZXJ2YWwiLCJHZW5lcmFsQ29udGV4dCIsImN1cnJlbmN5Q29uZmlnIiwib25DaGFuZ2VJblBhZ2UiLCJoYXNTdW1tYXJ5U3VwcG9ydCIsIk1VTFRJUFJPUE9TQUxfVFlQRSIsIngiLCJ5IiwicGFnZVgiLCJwYWdlWSIsInNob3ciLCJjb250ZXh0TWVudURhdGEiLCJzZXRDb250ZXh0TWVudURhdGEiLCJwYWdlV2lkZ2V0cyIsIndpZGdldHMiLCJzZWxlY3RlZFdpZGdldCIsImlkIiwib3BlblNldHRpbmdzRGlzYWJsZWQiLCJuYW1lIiwia2V5Q29kZSIsImhhbmRsZVNldFBvc2l0aW9uIiwiaGFuZGxlQ2xvc2VDb250ZXh0TWVudSIsImhhbmRsZUNvcHlXaWRnZXQiLCJvbkFkZFdpZGdldCIsInR5cGUiLCJkZWZhdWx0UG9zaXRpb24iLCJoYW5kbGVQYXN0ZUVsZW1lbnQiLCJ3aWRnZXRJZCIsImNoYW5nZWRYIiwiY2hhbmdlZFkiLCJoYW5kbGVTZXRXaWRnZXRTaXplIiwiY2hhbmdlZFdpZHRoIiwiY2hhbmdlZEhlaWdodCIsImhhbmRsZVNldFdpZGdldFN0eWxlIiwic3R5bGUiLCJoYW5kbGVTZXRXaWRnZXRWYWx1ZSIsInZhbHVlIiwiaGFuZGxlT3BlblBhZ2VDb250ZXh0TWVudSIsInNvcnQiLCJzb3J0aW5nV2lkZ2V0c0J5T3JkZXIiLCJtYXAiLCJ3aWRnZXQiLCJQQVJUSUFMX0VESVRJT05fTU9ERSIsIm1hcFN0YXRlVG9Qcm9wcyIsInNlbGVjdG9ycyIsImdldENvcGllZEl0ZW0iLCJnZXREcm9wcGluZ0l0ZW0iLCJnZXRTZWxlY3RlZFdpZGdldElkIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJwYWdlSWQiLCJ0ZW1wbGF0ZUNvcmVBY3Rpb25zIiwiYWN0aW9ucyIsImRlbGV0ZVdpZGdldCIsImR1cGxpY2F0ZVBhZ2UiLCJpdGVtIiwib3B0aW9ucyIsInBvc2l0aW9uIiwiZGF0YSIsInNldENvcGllZEl0ZW0iLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJmdW5jIiwiYXJyYXkiLCJzdHJpbmciLCJvYmplY3QiLCJib29sIiwicHJvcG9zYWxzTnVtYmVyIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFRQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFlQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxPQUFPLEdBQUdDLDBCQUFPQyxHQUFWLDJsQkF5QlA7QUFBQSxNQUFHQyxRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUNFQSxRQUFRLEtBQUssSUFBYixnWkFERjtBQUFBLENBekJPLENBQWI7O0FBa0NBLElBQU1DLG1CQUFtQixHQUFHSCwwQkFBT0MsR0FBVixtTEFBekI7O0FBU0EsSUFBTUcsZUFBZSxHQUFHSiwwQkFBT0MsR0FBVixrVkFBckI7O0FBZUEsSUFBTUksV0FBVyxHQUFHTCwwQkFBT0MsR0FBViw4WkFHSDtBQUFBLE1BQUdLLFdBQUgsU0FBR0EsV0FBSDtBQUFBLFNBQ05BLFdBQVcsS0FBSyxXQUFoQixHQUE4QkMsb0JBQTlCLEdBQTJDQyxxQkFEckM7QUFBQSxDQUhHLEVBS0o7QUFBQSxNQUFHRixXQUFILFNBQUdBLFdBQUg7QUFBQSxTQUNMQSxXQUFXLEtBQUssV0FBaEIsR0FBOEJFLHFCQUE5QixHQUE0Q0Qsb0JBRHZDO0FBQUEsQ0FMSSxFQU9DO0FBQUEsTUFBR0QsV0FBSCxTQUFHQSxXQUFIO0FBQUEsU0FDVkEsV0FBVyxLQUFLLFdBQWhCLEdBQThCQyxvQkFBOUIsR0FBMkNDLHFCQURqQztBQUFBLENBUEQsRUFTQTtBQUFBLE1BQUdGLFdBQUgsU0FBR0EsV0FBSDtBQUFBLFNBQ1RBLFdBQVcsS0FBSyxXQUFoQixHQUE4QkUscUJBQTlCLEdBQTRDRCxvQkFEbkM7QUFBQSxDQVRBLEVBZ0JYO0FBQUEsTUFBR0UsV0FBSCxTQUFHQSxXQUFIO0FBQUEsU0FBcUJBLFdBQVcsSUFBSSw2QkFBcEM7QUFBQSxDQWhCVyxDQUFqQjs7QUFtQkEsSUFBTUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsUUEyQlg7QUFBQSxNQTFCRkMsU0EwQkUsU0ExQkZBLFNBMEJFO0FBQUEsb0NBekJGQyxpQkF5QkU7QUFBQSxNQXpCRkEsaUJBeUJFLHNDQXpCa0IsRUF5QmxCO0FBQUEsTUF4QkZDLGlCQXdCRSxTQXhCRkEsaUJBd0JFO0FBQUEsTUF2QkZDLFNBdUJFLFNBdkJGQSxTQXVCRTtBQUFBLE1BdEJGQyxZQXNCRSxTQXRCRkEsWUFzQkU7QUFBQSxNQXJCRkMsWUFxQkUsU0FyQkZBLFlBcUJFO0FBQUEsTUFwQkZDLFlBb0JFLFNBcEJGQSxZQW9CRTtBQUFBLE1BbkJGQyxrQkFtQkUsU0FuQkZBLGtCQW1CRTtBQUFBLE1BbEJGQywyQkFrQkUsU0FsQkZBLDJCQWtCRTtBQUFBLE1BakJGQyxlQWlCRSxTQWpCRkEsZUFpQkU7QUFBQSxNQWhCRkMsY0FnQkUsU0FoQkZBLGNBZ0JFO0FBQUEsTUFmRkMsZUFlRSxTQWZGQSxlQWVFO0FBQUEsTUFkRkMsSUFjRSxTQWRGQSxJQWNFO0FBQUEsTUFiRkMsa0JBYUUsU0FiRkEsa0JBYUU7QUFBQSxNQVpGQyxXQVlFLFNBWkZBLFdBWUU7QUFBQSxNQVhGQyxnQkFXRSxTQVhGQSxnQkFXRTtBQUFBLE1BVkZDLFlBVUUsU0FWRkEsWUFVRTtBQUFBLE1BVEZDLGVBU0UsU0FURkEsZUFTRTtBQUFBLE1BUkZDLGVBUUUsU0FSRkEsZUFRRTtBQUFBLE1BUEZDLGlCQU9FLFNBUEZBLGlCQU9FO0FBQUEsTUFORkMsYUFNRSxTQU5GQSxhQU1FO0FBQUEsTUFMRkMsY0FLRSxTQUxGQSxjQUtFO0FBQUEsTUFKRkMsY0FJRSxTQUpGQSxjQUlFO0FBQUEsTUFIRkMsVUFHRSxTQUhGQSxVQUdFO0FBQUEsTUFGRkMsaUJBRUUsU0FGRkEsaUJBRUU7QUFBQSxNQURGQyxZQUNFLFNBREZBLFlBQ0U7QUFDRixNQUFNQyxVQUFVLEdBQUcsbUJBQU8sSUFBUCxDQUFuQjtBQUNBLE1BQU1DLFlBQVksR0FBRyxvQkFDakI7QUFBQSxXQUFNLDhCQUFnQkQsVUFBVSxDQUFDRSxPQUEzQixDQUFOO0FBQUEsR0FEaUIsRUFFakIsQ0FBQ0YsVUFBVSxDQUFDRSxPQUFaLENBRmlCLENBQXJCO0FBS0EsTUFBTUMsZ0JBQWdCLEdBQUcsbUJBQU8sSUFBUCxDQUF6QjtBQUNBLE1BQU1DLFFBQVEsR0FBRyxtQkFBTyxJQUFQLENBQWpCOztBQUNBLG9CQUEyQyx1QkFBV0Msd0JBQVgsQ0FBM0M7QUFBQSxNQUFRQyxjQUFSLGVBQVFBLGNBQVI7QUFBQSxNQUF3QkMsY0FBeEIsZUFBd0JBLGNBQXhCOztBQUNBLE1BQU1DLGlCQUFpQixHQUFHLG9CQUN0QjtBQUFBLFdBQU1ULFlBQVksS0FBS1UseUJBQXZCO0FBQUEsR0FEc0IsRUFFdEIsQ0FBQ1YsWUFBRCxDQUZzQixDQUExQjs7QUFJQSxrQkFBOEMscUJBQVM7QUFDbkRXLElBQUFBLENBQUMsRUFBRSxDQURnRDtBQUVuREMsSUFBQUEsQ0FBQyxFQUFFLENBRmdEO0FBR25EQyxJQUFBQSxLQUFLLEVBQUUsQ0FINEM7QUFJbkRDLElBQUFBLEtBQUssRUFBRSxDQUo0QztBQUtuREMsSUFBQUEsSUFBSSxFQUFFO0FBTDZDLEdBQVQsQ0FBOUM7QUFBQTtBQUFBLE1BQU9DLGVBQVA7QUFBQSxNQUF3QkMsa0JBQXhCOztBQU9BLE1BQU1DLFdBQVcsR0FBRyxvQkFBUTtBQUFBLFdBQU0vQixJQUFJLENBQUNnQyxPQUFYO0FBQUEsR0FBUixFQUE0QixDQUFDaEMsSUFBRCxDQUE1QixDQUFwQjtBQUNBLE1BQU1pQyxjQUFjLEdBQUcsb0JBQ25CO0FBQUEsV0FDSSx5Q0FBMkI5QixnQkFBM0IsRUFBNkNILElBQUksQ0FBQ2tDLEVBQWxELEVBQXNESCxXQUF0RCxDQURKO0FBQUEsR0FEbUIsRUFHbkIsQ0FBQzVCLGdCQUFELEVBQW1CSCxJQUFJLENBQUNrQyxFQUF4QixFQUE0QkgsV0FBNUIsQ0FIbUIsQ0FBdkI7QUFLQSxNQUFNSSxvQkFBb0IsR0FBRyxvQkFDekI7QUFBQSxXQUNJLHNDQUNJRixjQURKLEVBRUlwQixZQUZKLEVBR0l2QixpQkFISixFQUlJc0IsaUJBSkosQ0FESjtBQUFBLEdBRHlCLEVBUXpCLENBQUNxQixjQUFELEVBQWlCcEIsWUFBakIsRUFBK0J2QixpQkFBL0IsRUFBa0RzQixpQkFBbEQsQ0FSeUIsQ0FBN0I7QUFXQSx3QkFDSSxzQ0FDSXFCLGNBQWMsSUFBSUEsY0FBYyxDQUFDRyxJQURyQyxFQUVJLFVBQUNDLE9BQUQ7QUFBQSxXQUNJeEMsZUFBZSxDQUNYd0MsT0FEVyxFQUVYckMsSUFBSSxDQUFDakIsV0FGTSxFQUdYdUQsaUJBSFcsRUFJWDNCLFVBSlcsRUFLWHNCLGNBTFcsQ0FEbkI7QUFBQSxHQUZKLEVBVUlmLFFBVkosQ0FESixFQWFJLENBQUNlLGNBQUQsQ0FiSjtBQWVBLE1BQU1NLHNCQUFzQixHQUFHLHdCQUMzQiwwQ0FBNEJULGtCQUE1QixDQUQyQixFQUUzQixFQUYyQixDQUEvQjs7QUFJQSxNQUFNVSxpQkFBZ0IsR0FBRyx3QkFDckIsb0NBQXNCbkMsZUFBdEIsQ0FEcUIsRUFFckIsQ0FBQ0EsZUFBRCxDQUZxQixDQUF6Qjs7QUFLQSxNQUFNb0MsV0FBVyxHQUFHLHdCQUNoQixVQUFDQyxJQUFELEVBQU9DLGVBQVA7QUFBQSxXQUNJdkQsU0FBUyxDQUNMWSxJQUFJLENBQUNrQyxFQURBLEVBRUxRLElBRkssRUFHTEMsZUFISyxFQUlMckIsaUJBSkssRUFLTEQsY0FMSyxDQURiO0FBQUEsR0FEZ0IsRUFTaEIsQ0FBQ3JCLElBQUksQ0FBQ2tDLEVBQU4sRUFBVVosaUJBQVYsRUFBNkJELGNBQTdCLENBVGdCLENBQXBCO0FBWUEsTUFBTXVCLGtCQUFrQixHQUFHLHdCQUN2QiwrQkFDSWYsZUFESixFQUVJckMsWUFGSixFQUdJOEIsaUJBSEosRUFJSUQsY0FKSixFQUtJbkIsV0FMSixFQU1JRixJQU5KLEVBT0llLFlBUEosRUFRSUosVUFSSixDQUR1QixFQVd2QixDQUNJa0IsZUFESixFQUVJckMsWUFGSixFQUdJOEIsaUJBSEosRUFJSUQsY0FKSixFQUtJckIsSUFMSixFQU1JZSxZQU5KLEVBT0lKLFVBUEosQ0FYdUIsQ0FBM0I7QUFzQkEsTUFBTTJCLGlCQUFpQixHQUFHLHdCQUN0QixVQUFDTyxRQUFELEVBQVdDLFFBQVgsRUFBcUJDLFFBQXJCLEVBQWtDO0FBQzlCeEMsSUFBQUEsaUJBQWlCLENBQUNzQyxRQUFELEVBQVdDLFFBQVgsRUFBcUJDLFFBQXJCLEVBQStCMUIsY0FBL0IsQ0FBakI7QUFDSCxHQUhxQixFQUl0QixDQUFDQSxjQUFELENBSnNCLENBQTFCO0FBT0EsTUFBTTJCLG1CQUFtQixHQUFHLHdCQUN4QixVQUFDSCxRQUFELEVBQVdJLFlBQVgsRUFBeUJDLGFBQXpCLEVBQTJDO0FBQ3ZDMUMsSUFBQUEsYUFBYSxDQUNUcUMsUUFEUyxFQUVUSSxZQUZTLEVBR1RDLGFBSFMsRUFJVDdCLGNBSlMsQ0FBYjtBQU1ILEdBUnVCLEVBU3hCLENBQUNBLGNBQUQsQ0FUd0IsQ0FBNUI7QUFZQSxNQUFNOEIsb0JBQW9CLEdBQUcsd0JBQ3pCLFVBQUNOLFFBQUQsRUFBV08sS0FBWCxFQUFxQjtBQUNqQjNDLElBQUFBLGNBQWMsQ0FBQ29DLFFBQUQsRUFBV08sS0FBWCxFQUFrQi9CLGNBQWxCLENBQWQ7QUFDSCxHQUh3QixFQUl6QixDQUFDQSxjQUFELENBSnlCLENBQTdCO0FBT0EsTUFBTWdDLG9CQUFvQixHQUFHLHdCQUN6QixVQUFDUixRQUFELEVBQVdTLEtBQVgsRUFBcUI7QUFDakI1QyxJQUFBQSxjQUFjLENBQUNtQyxRQUFELEVBQVdTLEtBQVgsRUFBa0JqQyxjQUFsQixDQUFkO0FBQ0gsR0FId0IsRUFJekIsQ0FBQ0EsY0FBRCxDQUp5QixDQUE3QjtBQU9BLE1BQU1rQyx5QkFBeUIsR0FBRyx3QkFDOUIsNkNBQStCO0FBQzNCMUIsSUFBQUEsZUFBZSxFQUFmQSxlQUQyQjtBQUUzQk0sSUFBQUEsb0JBQW9CLEVBQXBCQSxvQkFGMkI7QUFHM0IxQyxJQUFBQSxZQUFZLEVBQVpBLFlBSDJCO0FBSTNCVSxJQUFBQSxnQkFBZ0IsRUFBRThCLGNBQWMsSUFBSUEsY0FBYyxDQUFDQyxFQUp4QjtBQUszQjlCLElBQUFBLFlBQVksRUFBWkEsWUFMMkI7QUFNM0IwQixJQUFBQSxrQkFBa0IsRUFBbEJBLGtCQU4yQjtBQU8zQmhCLElBQUFBLFVBQVUsRUFBVkE7QUFQMkIsR0FBL0IsQ0FEOEIsRUFVOUIsQ0FDSWUsZUFESixFQUVJTSxvQkFGSixFQUdJMUMsWUFISixFQUlJd0MsY0FKSixFQUtJbkIsVUFBVSxDQUFDRSxPQUxmLENBVjhCLENBQWxDO0FBbUJBLHNCQUNJLHNCQUFDLFdBQUQ7QUFDSSxJQUFBLFdBQVcsRUFBRWhCLElBQUksQ0FBQ2pCLFdBRHRCO0FBRUksSUFBQSxTQUFTLFlBQUtRLFNBQUwsY0FBa0IsQ0FBQ1Usa0JBQUQsSUFBdUIsYUFBekMsQ0FGYjtBQUdJLElBQUEsV0FBVyxFQUFFLENBQUNBLGtCQUFELElBQXVCTCwyQkFIeEM7QUFBQSxlQUtLaUMsZUFBZSxDQUFDRCxJQUFoQixpQkFDRyxxQkFBQyxvQkFBRDtBQUNJLE1BQUEsWUFBWSxFQUFFcEMsWUFEbEI7QUFFSSxNQUFBLFlBQVksRUFBRUMsWUFGbEI7QUFHSSxNQUFBLFlBQVksRUFBRTtBQUFBLGVBQ1ZLLGNBQWMsQ0FBQ21DLGNBQWMsQ0FBQ0MsRUFBaEIsRUFBb0JiLGNBQXBCLENBREo7QUFBQSxPQUhsQjtBQU1JLE1BQUEsa0JBQWtCLEVBQUUsc0NBQ2hCWSxjQURnQixFQUVoQmhCLGdCQUZnQixDQU54QjtBQVVJLE1BQUEsa0JBQWtCLEVBQUUyQixrQkFWeEI7QUFXSSxNQUFBLG9CQUFvQixFQUFFVCxvQkFYMUI7QUFZSSxNQUFBLGdCQUFnQixFQUFFO0FBQUEsZUFBTUssaUJBQWdCLENBQUNQLGNBQUQsQ0FBdEI7QUFBQSxPQVp0QjtBQWFJLE1BQUEsY0FBYyxFQUNWQSxjQUFjLElBQUlBLGNBQWMsQ0FBQ0MsRUFBZixLQUFzQixJQWRoRDtBQWdCSSxNQUFBLE9BQU8sRUFBRUssc0JBaEJiO0FBaUJJLE1BQUEsQ0FBQyxFQUFFVixlQUFlLENBQUNMLENBakJ2QjtBQWtCSSxNQUFBLENBQUMsRUFBRUssZUFBZSxDQUFDSjtBQWxCdkIsTUFOUixFQTJCS0ksZUFBZSxDQUFDRCxJQUFoQixpQkFDRyxxQkFBQyxtQkFBRDtBQUNJLE1BQUEsT0FBTyxFQUFFLG1CQUFNO0FBQ1hXLFFBQUFBLHNCQUFzQjtBQUN0Qm5DLFFBQUFBLFlBQVksQ0FBQyxJQUFELENBQVo7QUFDSCxPQUpMO0FBS0ksTUFBQSxhQUFhLEVBQUVtQztBQUxuQixNQTVCUixlQW9DSSxxQkFBQyxPQUFEO0FBQ0ksTUFBQSxRQUFRLEVBQUU1QixVQURkO0FBRUksTUFBQSxhQUFhLEVBQUU0Qyx5QkFGbkI7QUFHSSxNQUFBLEdBQUcsRUFBRXpDLFVBSFQ7QUFJSSxNQUFBLE9BQU8sRUFBRSx3Q0FDTGUsZUFESyxFQUVMVSxzQkFGSyxFQUdMTixjQUFjLElBQUlBLGNBQWMsQ0FBQ0MsRUFINUIsRUFJTDlCLFlBSkssRUFLTFUsVUFMSyxDQUpiO0FBQUEsZ0JBWUtpQixXQUFXLElBQ1JBLFdBQVcsQ0FBQ3lCLElBQVosQ0FBaUJDLDhCQUFqQixFQUF3Q0MsR0FBeEMsQ0FBNEMsVUFBQ0MsTUFBRCxFQUFZO0FBQ3BELDRCQUNJLHFCQUFDLGNBQUQ7QUFDSSxVQUFBLGlCQUFpQixFQUFFckUsaUJBRHZCO0FBRUksVUFBQSxjQUFjLEVBQUU4QixjQUZwQjtBQUdJLFVBQUEsWUFBWSxFQUFFM0IsWUFIbEI7QUFJSSxVQUFBLGtCQUFrQixFQUFFRSxrQkFKeEI7QUFLSSxVQUFBLHNCQUFzQixFQUFFNEMsc0JBTDVCO0FBTUksVUFBQSxnQkFBZ0IsRUFBRTtBQUFBLG1CQUNkQyxpQkFBZ0IsQ0FBQ1AsY0FBRCxDQURGO0FBQUEsV0FOdEI7QUFTSSxVQUFBLHFCQUFxQixFQUFFLDZDQUNuQkgsa0JBRG1CLENBVDNCO0FBWUksVUFBQSxrQkFBa0IsRUFBRSxzQ0FDaEJHLGNBRGdCLEVBRWhCaEIsZ0JBRmdCLENBWnhCO0FBZ0JJLFVBQUEsWUFBWSxFQUFFO0FBQUEsbUJBQ1ZuQixjQUFjLENBQ1ZtQyxjQUFjLElBQUlBLGNBQWMsQ0FBQ0MsRUFEdkIsRUFFVmIsY0FGVSxDQURKO0FBQUEsV0FoQmxCO0FBc0JJLFVBQUEsMkJBQTJCLEVBQ3ZCekIsMkJBdkJSO0FBMEJJLFVBQUEsZUFBZSxFQUFFSSxJQUFJLENBQUNqQixXQTFCMUI7QUEyQkksVUFBQSxRQUFRLEVBQ0prRCxjQUFjLElBQ2RBLGNBQWMsQ0FBQ0MsRUFBZixLQUFzQnlCLE1BQU0sQ0FBQ3pCLEVBN0JyQztBQStCSSxVQUFBLGNBQWMsRUFBRTlCLFlBL0JwQjtBQWdDSSxVQUFBLGlCQUFpQixFQUFFa0MsaUJBaEN2QjtBQWlDSSxVQUFBLGFBQWEsRUFBRVUsbUJBakNuQjtBQWtDSSxVQUFBLGNBQWMsRUFBRUssb0JBbENwQjtBQW1DSSxVQUFBLFVBQVUsRUFBRTFDLFVBbkNoQjtBQW9DSSxVQUFBLGlCQUFpQixFQUFFQyxpQkFwQ3ZCO0FBcUNJLFVBQUEsWUFBWSxFQUFFQyxZQXJDbEI7QUFzQ0ksVUFBQSxZQUFZLEVBQUU4QztBQXRDbEIsV0F5QlNBLE1BQU0sQ0FBQ3pCLEVBekJoQixDQURKO0FBMENILE9BM0NEO0FBYlIsTUFwQ0osRUE4RkssQ0FBQyxxQkFBT3hDLFlBQVAsQ0FBRCxpQkFDRyxxQkFBQyx3QkFBRDtBQUNJLE1BQUEsWUFBWSxFQUFFQSxZQURsQjtBQUVJLE1BQUEsWUFBWSxFQUFFLDhCQUNWQSxZQURVLEVBRVYrQyxXQUZVLEVBR1ZuQyxlQUhVLENBRmxCO0FBT0ksTUFBQSxlQUFlLEVBQUVOLElBQUksQ0FBQ2pCLFdBUDFCO0FBUUksTUFBQSxVQUFVLEVBQUU0QjtBQVJoQixNQS9GUixlQTJHSSxxQkFBQyxzQkFBRDtBQUNJLE1BQUEsUUFBUSxFQUFFdEIsaUJBRGQ7QUFFSSxNQUFBLGlCQUFpQixFQUFFQyxpQkFGdkI7QUFHSSxNQUFBLHdCQUF3QixFQUFFRyxZQUFZLEtBQUttRSwyQkFIL0M7QUFJSSxNQUFBLEdBQUcsRUFBRTNDLGdCQUpUO0FBS0ksTUFBQSxpQkFBaUIsRUFBRUwsaUJBTHZCO0FBTUksTUFBQSxjQUFjLEVBQUV1QyxvQkFOcEI7QUFPSSxNQUFBLGNBQWMsRUFBRUUsb0JBUHBCO0FBUUksTUFBQSxZQUFZLEVBQUV4QztBQVJsQixNQTNHSixFQXFIS3BCLFlBQVksS0FBS21FLDJCQUFqQixpQkFDRyxxQkFBQyxlQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsV0FEZDtBQUVJLE1BQUEsT0FBTyxFQUFFO0FBQUEsZUFBTTdELGVBQWUsQ0FBQ0MsSUFBSSxDQUFDa0MsRUFBTixDQUFyQjtBQUFBLE9BRmI7QUFBQSw2QkFJSTtBQUFHLFFBQUEsU0FBUyxFQUFDO0FBQWI7QUFKSixNQXRIUjtBQUFBLElBREo7QUFnSUgsQ0EvU0Q7O0FBaVRBLElBQU0yQixlQUFlLEdBQUcsd0NBQXlCO0FBQzdDckUsRUFBQUEsWUFBWSxFQUFFc0UsU0FBUyxDQUFDQyxhQURxQjtBQUU3Q3JFLEVBQUFBLFlBQVksRUFBRW9FLFNBQVMsQ0FBQ0UsZUFGcUI7QUFHN0M3RCxFQUFBQSxnQkFBZ0IsRUFBRTJELFNBQVMsQ0FBQ0c7QUFIaUIsQ0FBekIsQ0FBeEI7O0FBTUEsSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0Qy9FLElBQUFBLFNBQVMsRUFBRSxtQkFDUGdGLE1BRE8sRUFFUDFCLElBRk8sRUFHUEMsZUFITyxFQUlQckIsaUJBSk8sRUFLUEQsY0FMTztBQUFBLGFBT1A4QyxRQUFRLENBQ0pFLG1CQUFtQixDQUFDakYsU0FBcEIsQ0FDSWdGLE1BREosRUFFSTFCLElBRkosRUFHSUMsZUFISixFQUlJckIsaUJBSkosRUFLSUQsY0FMSixDQURJLENBUEQ7QUFBQSxLQUQyQjtBQWlCdEN4QixJQUFBQSxlQUFlLEVBQUUseUJBQ2J3QyxPQURhLEVBRWJ0RCxXQUZhLEVBR2J3QixpQkFIYSxFQUliSSxVQUphLEVBS2JzQixjQUxhO0FBQUEsYUFPYmtDLFFBQVEsQ0FDSkcsT0FBTyxDQUFDekUsZUFBUixDQUNJd0MsT0FESixFQUVJdEQsV0FGSixFQUdJd0IsaUJBSEosRUFJSUksVUFKSixFQUtJc0IsY0FMSixDQURJLENBUEs7QUFBQSxLQWpCcUI7QUFpQ3RDbkMsSUFBQUEsY0FBYyxFQUFFLHdCQUFDK0MsUUFBRCxFQUFXeEIsY0FBWDtBQUFBLGFBQ1o4QyxRQUFRLENBQUNFLG1CQUFtQixDQUFDRSxZQUFwQixDQUFpQzFCLFFBQWpDLEVBQTJDeEIsY0FBM0MsQ0FBRCxDQURJO0FBQUEsS0FqQ3NCO0FBbUN0Q3RCLElBQUFBLGVBQWUsRUFBRSx5QkFBQ21DLEVBQUQ7QUFBQSxhQUFRaUMsUUFBUSxDQUFDRSxtQkFBbUIsQ0FBQ0csYUFBcEIsQ0FBa0N0QyxFQUFsQyxDQUFELENBQWhCO0FBQUEsS0FuQ3FCO0FBb0N0QzVCLElBQUFBLGVBQWUsRUFBRSx5QkFBQ21FLElBQUQ7QUFBQSxhQUFVTixRQUFRLENBQUNHLE9BQU8sQ0FBQzVFLFlBQVIsQ0FBcUIrRSxJQUFyQixDQUFELENBQWxCO0FBQUEsS0FwQ3FCO0FBcUN0Q3ZFLElBQUFBLFdBQVcsRUFBRSxxQkFDVGtFLE1BRFMsRUFFVE0sT0FGUyxFQUdUL0QsVUFIUyxFQUlUZ0UsUUFKUyxFQUtUckQsaUJBTFMsRUFNVEQsY0FOUztBQUFBLGFBUVQ4QyxRQUFRLENBQ0pFLG1CQUFtQixDQUFDbkUsV0FBcEIsQ0FDSWtFLE1BREosRUFFSU0sT0FGSixFQUdJL0QsVUFISixFQUlJZ0UsUUFKSixFQUtJckQsaUJBTEosRUFNSUQsY0FOSixDQURJLENBUkM7QUFBQSxLQXJDeUI7QUF1RHRDakIsSUFBQUEsWUFBWSxFQUFFLHNCQUFDeUMsUUFBRCxFQUFXdUIsTUFBWDtBQUFBLGFBQ1ZELFFBQVEsQ0FBQ0csT0FBTyxDQUFDbEUsWUFBUixDQUFxQnlDLFFBQXJCLEVBQStCdUIsTUFBL0IsQ0FBRCxDQURFO0FBQUEsS0F2RHdCO0FBeUR0Qy9ELElBQUFBLGVBQWUsRUFBRSx5QkFBQ3VFLElBQUQ7QUFBQSxhQUFVVCxRQUFRLENBQUNHLE9BQU8sQ0FBQ08sYUFBUixDQUFzQkQsSUFBdEIsQ0FBRCxDQUFsQjtBQUFBLEtBekRxQjtBQTBEdENyRSxJQUFBQSxpQkFBaUIsRUFBRSwyQkFBQ3NDLFFBQUQsRUFBV0MsUUFBWCxFQUFxQkMsUUFBckIsRUFBK0IxQixjQUEvQjtBQUFBLGFBQ2Y4QyxRQUFRLENBQ0pFLG1CQUFtQixDQUFDOUQsaUJBQXBCLENBQ0lzQyxRQURKLEVBRUlDLFFBRkosRUFHSUMsUUFISixFQUlJMUIsY0FKSixDQURJLENBRE87QUFBQSxLQTFEbUI7QUFtRXRDYixJQUFBQSxhQUFhLEVBQUUsdUJBQUNxQyxRQUFELEVBQVdJLFlBQVgsRUFBeUJDLGFBQXpCLEVBQXdDN0IsY0FBeEM7QUFBQSxhQUNYOEMsUUFBUSxDQUNKRSxtQkFBbUIsQ0FBQzdELGFBQXBCLENBQ0lxQyxRQURKLEVBRUlJLFlBRkosRUFHSUMsYUFISixFQUlJN0IsY0FKSixDQURJLENBREc7QUFBQSxLQW5FdUI7QUE0RXRDWixJQUFBQSxjQUFjLEVBQUUsd0JBQUNvQyxRQUFELEVBQVdPLEtBQVgsRUFBa0IvQixjQUFsQjtBQUFBLGFBQ1o4QyxRQUFRLENBQ0pFLG1CQUFtQixDQUFDNUQsY0FBcEIsQ0FBbUNvQyxRQUFuQyxFQUE2Q08sS0FBN0MsRUFBb0QvQixjQUFwRCxDQURJLENBREk7QUFBQSxLQTVFc0I7QUFnRnRDWCxJQUFBQSxjQUFjLEVBQUUsd0JBQUNtQyxRQUFELEVBQVdTLEtBQVgsRUFBa0JqQyxjQUFsQjtBQUFBLGFBQ1o4QyxRQUFRLENBQ0pFLG1CQUFtQixDQUFDM0QsY0FBcEIsQ0FBbUNtQyxRQUFuQyxFQUE2Q1MsS0FBN0MsRUFBb0RqQyxjQUFwRCxDQURJLENBREk7QUFBQTtBQWhGc0IsR0FBZjtBQUFBLENBQTNCOztBQXNGQWxDLFFBQVEsQ0FBQzJGLFNBQVQsR0FBcUI7QUFDakIxRixFQUFBQSxTQUFTLEVBQUUyRixtQkFBVUMsSUFESjtBQUVqQjNGLEVBQUFBLGlCQUFpQixFQUFFMEYsbUJBQVVFLEtBRlo7QUFHakIzRixFQUFBQSxpQkFBaUIsRUFBRXlGLG1CQUFVRSxLQUhaO0FBSWpCMUYsRUFBQUEsU0FBUyxFQUFFd0YsbUJBQVVHLE1BSko7QUFLakIxRixFQUFBQSxZQUFZLEVBQUV1RixtQkFBVUksTUFMUDtBQU1qQnpGLEVBQUFBLFlBQVksRUFBRXFGLG1CQUFVSSxNQU5QO0FBT2pCMUYsRUFBQUEsWUFBWSxFQUFFc0YsbUJBQVVHLE1BUFA7QUFRakJ2RixFQUFBQSxrQkFBa0IsRUFBRW9GLG1CQUFVSSxNQVJiO0FBU2pCdkYsRUFBQUEsMkJBQTJCLEVBQUVtRixtQkFBVUssSUFUdEI7QUFVakJ2RixFQUFBQSxlQUFlLEVBQUVrRixtQkFBVUMsSUFWVjtBQVdqQmxGLEVBQUFBLGNBQWMsRUFBRWlGLG1CQUFVQyxJQVhUO0FBWWpCakYsRUFBQUEsZUFBZSxFQUFFZ0YsbUJBQVVDLElBWlY7QUFhakJoRixFQUFBQSxJQUFJLEVBQUUrRSxtQkFBVUksTUFiQztBQWNqQmxGLEVBQUFBLGtCQUFrQixFQUFFOEUsbUJBQVVLLElBZGI7QUFlakJsRixFQUFBQSxXQUFXLEVBQUU2RSxtQkFBVUMsSUFmTjtBQWdCakJLLEVBQUFBLGVBQWUsRUFBRU4sbUJBQVVPLE1BaEJWO0FBaUJqQm5GLEVBQUFBLGdCQUFnQixFQUFFNEUsbUJBQVVHLE1BakJYO0FBa0JqQjlFLEVBQUFBLFlBQVksRUFBRTJFLG1CQUFVQyxJQWxCUDtBQW1CakIzRSxFQUFBQSxlQUFlLEVBQUUwRSxtQkFBVUMsSUFuQlY7QUFvQmpCMUUsRUFBQUEsZUFBZSxFQUFFeUUsbUJBQVVDLElBcEJWO0FBcUJqQnpFLEVBQUFBLGlCQUFpQixFQUFFd0UsbUJBQVVDLElBckJaO0FBc0JqQnhFLEVBQUFBLGFBQWEsRUFBRXVFLG1CQUFVQyxJQXRCUjtBQXVCakJ2RSxFQUFBQSxjQUFjLEVBQUVzRSxtQkFBVUMsSUF2QlQ7QUF3QmpCdEUsRUFBQUEsY0FBYyxFQUFFcUUsbUJBQVVDLElBeEJUO0FBeUJqQnJFLEVBQUFBLFVBQVUsRUFBRW9FLG1CQUFVSyxJQXpCTDtBQTBCakJ4RSxFQUFBQSxpQkFBaUIsRUFBRW1FLG1CQUFVRSxLQTFCWjtBQTJCakJwRSxFQUFBQSxZQUFZLEVBQUVrRSxtQkFBVU87QUEzQlAsQ0FBckI7O2VBOEJlLHlCQUFRekIsZUFBUixFQUF5Qkssa0JBQXpCLEVBQTZDL0UsUUFBN0MsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBpc051bGwgZnJvbSAnbG9kYXNoL2lzTnVsbCc7XHJcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XHJcbmltcG9ydCB7XHJcbiAgICB1c2VDYWxsYmFjayxcclxuICAgIHVzZUNvbnRleHQsXHJcbiAgICB1c2VNZW1vLFxyXG4gICAgdXNlU3RhdGUsXHJcbiAgICB1c2VFZmZlY3QsXHJcbiAgICB1c2VSZWYsXHJcbn0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XHJcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xyXG5cclxuaW1wb3J0IHsgUEFHRV9IRUlHSFQsIFBBR0VfV0lEVEggfSBmcm9tICdAY29uc3RhbnRzL3RlbXBsYXRlJztcclxuaW1wb3J0IHsgTVVMVElQUk9QT1NBTF9UWVBFLCBQQVJUSUFMX0VESVRJT05fTU9ERSB9IGZyb20gJ0Bjb25zdGFudHMvdHlwZXMnO1xyXG5cclxuaW1wb3J0IHsgR2VuZXJhbENvbnRleHQgfSBmcm9tICdAaGVscGVycy9jb250ZXh0cyc7XHJcblxyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVBY3Rpb25zIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvYWN0aW9ucyc7XHJcblxyXG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xyXG5pbXBvcnQge1xyXG4gICAgZ2V0T3BlblNldHRpbmdzRGlzYWJsZWQsXHJcbiAgICBnZXRQYWdlUG9zaXRpb24sXHJcbiAgICBnZXRTZWxlY3RlZFdpZGdldFZhbGlkYXRlZCxcclxuICAgIGhhbmRsZUNsaWNrQ29udGFpbmVyQnVpbGQsXHJcbiAgICBoYW5kbGVDbG9zZUNvbnRleHRNZW51QnVpbGQsXHJcbiAgICBoYW5kbGVDb3B5V2lkZ2V0QnVpbGQsXHJcbiAgICBoYW5kbGVEcm9wQnVpbGQsXHJcbiAgICBoYW5kbGVPcGVuSXRlbUNvbnRleHRNZW51QnVpbGQsXHJcbiAgICBoYW5kbGVPcGVuUGFnZUNvbnRleHRNZW51QnVpbGQsXHJcbiAgICBoYW5kbGVPcGVuU2V0dGluZ3NCdWlsZCxcclxuICAgIGhhbmRsZVBhc3RlQnVpbGQsXHJcbiAgICBzZWxlY3RlZFdpZGdldFVzZUVmZmVjdCxcclxuICAgIHNvcnRpbmdXaWRnZXRzQnlPcmRlcixcclxufSBmcm9tICcuLi8uLi9oZWxwZXJzJztcclxuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJy4uLy4uL3NlbGVjdG9ycyc7XHJcbmltcG9ydCBDb250ZXh0TWVudSBmcm9tICcuLi9Db250ZXh0TWVudSc7XHJcbmltcG9ydCBHcmlkSXRlbSBmcm9tICcuLi9HcmlkSXRlbS9pbmRleCc7XHJcbmltcG9ydCBQbGFjZWhvbGRlckl0ZW0gZnJvbSAnLi4vUGxhY2Vob2xkZXJJdGVtJztcclxuaW1wb3J0IFNldHRpbmdzUGFuZWwgZnJvbSAnLi4vU2V0dGluZ3NQYW5lbCc7XHJcblxyXG5jb25zdCBXcmFwcGVyID0gc3R5bGVkLmRpdmBcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxuICAgICYgLnJlYWN0LWRyYWdnYWJsZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgJiAucmVhY3QtZHJhZ2dhYmxlLWRyYWdnaW5nIC5kcmFnZ2FibGUtdGFiLFxyXG4gICAgJiAucmVhY3QtZHJhZ2dhYmxlLWRyYWdnaW5nIC5kcmFnZ2FibGUtYm9keSB7XHJcbiAgICAgICAgb3BhY2l0eTogMC40O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG5cclxuICAgICYgLnJlYWN0LWRyYWdnYWJsZS1kcmFnZ2luZyAuZHJhZ2dhYmxlLWJvZHkge1xyXG4gICAgICAgIG91dGxpbmU6IDFweCBkYXNoZWQgIzU1NTtcclxuICAgIH1cclxuXHJcbiAgICAmIC5yZWFjdC1kcmFnZ2FibGUtZHJhZ2dpbmcgLmNvcm5lci1tYXJrZXIge1xyXG4gICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICB9XHJcblxyXG4gICAgJHsoeyBzaG93R3JpZCB9KSA9PlxyXG4gICAgICAgIHNob3dHcmlkID09PSB0cnVlICYmXHJcbiAgICAgICAgYFxyXG4gICAgICAgIGJveC1zaGFkb3c6IGluc2V0IDAgMCAwIDQ4LjlweCAjZmZmO1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudChyZ2JhKDIxMSwgMjE1LCAyMzUsIDAuNSkgMXB4LCB0cmFuc3BhcmVudCAxcHgpLCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsIHJnYmEoMjExLCAyMTUsIDIzNSwgMC41KSAxcHgsIHRyYW5zcGFyZW50IDFweCksIGxpbmVhci1ncmFkaWVudChyZ2JhKDIxMSwgMjE1LCAyMzUsIDAuNSkgMXB4LCB0cmFuc3BhcmVudCAxcHgpLCBsaW5lYXItZ3JhZGllbnQoOTBkZWcsIHJnYmEoMjExLCAyMTUsIDIzNSwgMC41KSAxcHgsIHRyYW5zcGFyZW50IDFweCk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiAxNi41cHggMTYuNXB4O1xyXG4gICAgICAgIGB9XHJcbmA7XHJcblxyXG5jb25zdCBDb250ZXh0TWVudUJhY2tEcm9wID0gc3R5bGVkLmRpdmBcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHotaW5kZXg6IDA7XHJcbmA7XHJcblxyXG5jb25zdCBEdXBsaWNhdGVCdXR0b24gPSBzdHlsZWQuZGl2YFxyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyNHB4O1xyXG4gICAgcmlnaHQ6IC00NnB4O1xyXG4gICAgd2lkdGg6IDM2cHg7XHJcbiAgICBoZWlnaHQ6IDM2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDM2cHg7XHJcbiAgICBjb2xvcjogIzdmN2Y3ZjtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuYDtcclxuXHJcbmNvbnN0IFBhZ2VXcmFwcGVyID0gc3R5bGVkLmRpdmBcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICAvKiBib3gtc2hhZG93OiAyMHB4IDJweCAxN3B4IDAgcmdiYSgxMjAsIDExMiwgMTc4LCAwLjA4KTsgKi9cclxuICAgIGhlaWdodDogJHsoeyBvcmllbnRhdGlvbiB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IFBBR0VfV0lEVEggOiBQQUdFX0hFSUdIVH1weDtcclxuICAgIHdpZHRoOiAkeyh7IG9yaWVudGF0aW9uIH0pID0+XHJcbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdsYW5kc2NhcGUnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIfXB4O1xyXG4gICAgbWluLWhlaWdodDogJHsoeyBvcmllbnRhdGlvbiB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IFBBR0VfV0lEVEggOiBQQUdFX0hFSUdIVH1weDtcclxuICAgIG1pbi13aWR0aDogJHsoeyBvcmllbnRhdGlvbiB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IFBBR0VfSEVJR0hUIDogUEFHRV9XSURUSH1weDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICYucGFnZS1kZXNpZ24ge1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDIwcHggMnB4IDE3cHggMCByZ2JhKDEyMCwgMTEyLCAxNzgsIDAuMDgpO1xyXG4gICAgICAgIG1hcmdpbjogMjRweCAzMnB4O1xyXG4gICAgfVxyXG4gICAgJHsoeyBzaG93T3V0bGluZSB9KSA9PiBzaG93T3V0bGluZSAmJiAnb3V0bGluZTogI2ZmOWEwMCBzb2xpZCAycHg7J31cclxuYDtcclxuXHJcbmNvbnN0IFBhZ2VWaWV3ID0gKHtcclxuICAgIGFkZFdpZGdldCxcclxuICAgIGF2YWlsYWJsZUNhdGFsb2dzID0gW10sXHJcbiAgICBjaGFydFdpZGdldHNOYW1lcyxcclxuICAgIGNsYXNzTmFtZSxcclxuICAgIGNvcGllZFdpZGdldCxcclxuICAgIGVkaXRpb25MZXZlbCxcclxuICAgIGRyb3BwaW5nSXRlbSxcclxuICAgIGZyb2FsYUVkaXRvckNvbmZpZyxcclxuICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZCxcclxuICAgIG1vdmVXaWRnZXRCeUtleSxcclxuICAgIG9uRGVsZXRlV2lkZ2V0LFxyXG4gICAgb25EdXBsaWNhdGVQYWdlLFxyXG4gICAgcGFnZSxcclxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZCxcclxuICAgIHBhc3RlV2lkZ2V0LFxyXG4gICAgc2VsZWN0ZWRXaWRnZXRJZCxcclxuICAgIHNlbGVjdFdpZGdldCxcclxuICAgIHNldENvcGllZFdpZGdldCxcclxuICAgIHNldERyb3BwaW5nSXRlbSxcclxuICAgIHNldFdpZGdldFBvc2l0aW9uLFxyXG4gICAgc2V0V2lkZ2V0U2l6ZSxcclxuICAgIHNldFdpZGdldFN0eWxlLFxyXG4gICAgc2V0V2lkZ2V0VmFsdWUsXHJcbiAgICBzaG93R3VpZGVzLFxyXG4gICAgdGFibGVXaWRnZXRzTmFtZXMsXHJcbiAgICB0ZW1wbGF0ZVR5cGUsXHJcbn0pID0+IHtcclxuICAgIGNvbnN0IHdyYXBwZXJSZWYgPSB1c2VSZWYobnVsbCk7XHJcbiAgICBjb25zdCBwYWdlUG9zaXRpb24gPSB1c2VNZW1vKFxyXG4gICAgICAgICgpID0+IGdldFBhZ2VQb3NpdGlvbih3cmFwcGVyUmVmLmN1cnJlbnQpLFxyXG4gICAgICAgIFt3cmFwcGVyUmVmLmN1cnJlbnRdXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IHNldHRpbmdzUGFuZWxSZWYgPSB1c2VSZWYobnVsbCk7XHJcbiAgICBjb25zdCBpbnRlcnZhbCA9IHVzZVJlZihudWxsKTtcclxuICAgIGNvbnN0IHsgY3VycmVuY3lDb25maWcsIG9uQ2hhbmdlSW5QYWdlIH0gPSB1c2VDb250ZXh0KEdlbmVyYWxDb250ZXh0KTtcclxuICAgIGNvbnN0IGhhc1N1bW1hcnlTdXBwb3J0ID0gdXNlTWVtbyhcclxuICAgICAgICAoKSA9PiB0ZW1wbGF0ZVR5cGUgPT09IE1VTFRJUFJPUE9TQUxfVFlQRSxcclxuICAgICAgICBbdGVtcGxhdGVUeXBlXVxyXG4gICAgKTtcclxuICAgIGNvbnN0IFtjb250ZXh0TWVudURhdGEsIHNldENvbnRleHRNZW51RGF0YV0gPSB1c2VTdGF0ZSh7XHJcbiAgICAgICAgeDogMCxcclxuICAgICAgICB5OiAwLFxyXG4gICAgICAgIHBhZ2VYOiAwLFxyXG4gICAgICAgIHBhZ2VZOiAwLFxyXG4gICAgICAgIHNob3c6IGZhbHNlLFxyXG4gICAgfSk7XHJcbiAgICBjb25zdCBwYWdlV2lkZ2V0cyA9IHVzZU1lbW8oKCkgPT4gcGFnZS53aWRnZXRzLCBbcGFnZV0pO1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRXaWRnZXQgPSB1c2VNZW1vKFxyXG4gICAgICAgICgpID0+XHJcbiAgICAgICAgICAgIGdldFNlbGVjdGVkV2lkZ2V0VmFsaWRhdGVkKHNlbGVjdGVkV2lkZ2V0SWQsIHBhZ2UuaWQsIHBhZ2VXaWRnZXRzKSxcclxuICAgICAgICBbc2VsZWN0ZWRXaWRnZXRJZCwgcGFnZS5pZCwgcGFnZVdpZGdldHNdXHJcbiAgICApO1xyXG4gICAgY29uc3Qgb3BlblNldHRpbmdzRGlzYWJsZWQgPSB1c2VNZW1vKFxyXG4gICAgICAgICgpID0+XHJcbiAgICAgICAgICAgIGdldE9wZW5TZXR0aW5nc0Rpc2FibGVkKFxyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVR5cGUsXHJcbiAgICAgICAgICAgICAgICBjaGFydFdpZGdldHNOYW1lcyxcclxuICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzXHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgW3NlbGVjdGVkV2lkZ2V0LCB0ZW1wbGF0ZVR5cGUsIGNoYXJ0V2lkZ2V0c05hbWVzLCB0YWJsZVdpZGdldHNOYW1lc11cclxuICAgICk7XHJcblxyXG4gICAgdXNlRWZmZWN0KFxyXG4gICAgICAgIHNlbGVjdGVkV2lkZ2V0VXNlRWZmZWN0KFxyXG4gICAgICAgICAgICBzZWxlY3RlZFdpZGdldCAmJiBzZWxlY3RlZFdpZGdldC5uYW1lLFxyXG4gICAgICAgICAgICAoa2V5Q29kZSkgPT5cclxuICAgICAgICAgICAgICAgIG1vdmVXaWRnZXRCeUtleShcclxuICAgICAgICAgICAgICAgICAgICBrZXlDb2RlLFxyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2Uub3JpZW50YXRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlU2V0UG9zaXRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd0d1aWRlcyxcclxuICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFdpZGdldFxyXG4gICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgaW50ZXJ2YWxcclxuICAgICAgICApLFxyXG4gICAgICAgIFtzZWxlY3RlZFdpZGdldF1cclxuICAgICk7XHJcbiAgICBjb25zdCBoYW5kbGVDbG9zZUNvbnRleHRNZW51ID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgaGFuZGxlQ2xvc2VDb250ZXh0TWVudUJ1aWxkKHNldENvbnRleHRNZW51RGF0YSksXHJcbiAgICAgICAgW11cclxuICAgICk7XHJcbiAgICBjb25zdCBoYW5kbGVDb3B5V2lkZ2V0ID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgaGFuZGxlQ29weVdpZGdldEJ1aWxkKHNldENvcGllZFdpZGdldCksXHJcbiAgICAgICAgW3NldENvcGllZFdpZGdldF1cclxuICAgICk7XHJcblxyXG4gICAgY29uc3Qgb25BZGRXaWRnZXQgPSB1c2VDYWxsYmFjayhcclxuICAgICAgICAodHlwZSwgZGVmYXVsdFBvc2l0aW9uKSA9PlxyXG4gICAgICAgICAgICBhZGRXaWRnZXQoXHJcbiAgICAgICAgICAgICAgICBwYWdlLmlkLFxyXG4gICAgICAgICAgICAgICAgdHlwZSxcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRQb3NpdGlvbixcclxuICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcclxuICAgICAgICAgICAgKSxcclxuICAgICAgICBbcGFnZS5pZCwgaGFzU3VtbWFyeVN1cHBvcnQsIG9uQ2hhbmdlSW5QYWdlXVxyXG4gICAgKTtcclxuXHJcbiAgICBjb25zdCBoYW5kbGVQYXN0ZUVsZW1lbnQgPSB1c2VDYWxsYmFjayhcclxuICAgICAgICBoYW5kbGVQYXN0ZUJ1aWxkKFxyXG4gICAgICAgICAgICBjb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgICAgIGNvcGllZFdpZGdldCxcclxuICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXHJcbiAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlLFxyXG4gICAgICAgICAgICBwYXN0ZVdpZGdldCxcclxuICAgICAgICAgICAgcGFnZSxcclxuICAgICAgICAgICAgcGFnZVBvc2l0aW9uLFxyXG4gICAgICAgICAgICBzaG93R3VpZGVzXHJcbiAgICAgICAgKSxcclxuICAgICAgICBbXHJcbiAgICAgICAgICAgIGNvbnRleHRNZW51RGF0YSxcclxuICAgICAgICAgICAgY29waWVkV2lkZ2V0LFxyXG4gICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2UsXHJcbiAgICAgICAgICAgIHBhZ2UsXHJcbiAgICAgICAgICAgIHBhZ2VQb3NpdGlvbixcclxuICAgICAgICAgICAgc2hvd0d1aWRlcyxcclxuICAgICAgICBdXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IGhhbmRsZVNldFBvc2l0aW9uID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgKHdpZGdldElkLCBjaGFuZ2VkWCwgY2hhbmdlZFkpID0+IHtcclxuICAgICAgICAgICAgc2V0V2lkZ2V0UG9zaXRpb24od2lkZ2V0SWQsIGNoYW5nZWRYLCBjaGFuZ2VkWSwgb25DaGFuZ2VJblBhZ2UpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgW29uQ2hhbmdlSW5QYWdlXVxyXG4gICAgKTtcclxuXHJcbiAgICBjb25zdCBoYW5kbGVTZXRXaWRnZXRTaXplID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgKHdpZGdldElkLCBjaGFuZ2VkV2lkdGgsIGNoYW5nZWRIZWlnaHQpID0+IHtcclxuICAgICAgICAgICAgc2V0V2lkZ2V0U2l6ZShcclxuICAgICAgICAgICAgICAgIHdpZGdldElkLFxyXG4gICAgICAgICAgICAgICAgY2hhbmdlZFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgY2hhbmdlZEhlaWdodCxcclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBbb25DaGFuZ2VJblBhZ2VdXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IGhhbmRsZVNldFdpZGdldFN0eWxlID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgKHdpZGdldElkLCBzdHlsZSkgPT4ge1xyXG4gICAgICAgICAgICBzZXRXaWRnZXRTdHlsZSh3aWRnZXRJZCwgc3R5bGUsIG9uQ2hhbmdlSW5QYWdlKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIFtvbkNoYW5nZUluUGFnZV1cclxuICAgICk7XHJcblxyXG4gICAgY29uc3QgaGFuZGxlU2V0V2lkZ2V0VmFsdWUgPSB1c2VDYWxsYmFjayhcclxuICAgICAgICAod2lkZ2V0SWQsIHZhbHVlKSA9PiB7XHJcbiAgICAgICAgICAgIHNldFdpZGdldFZhbHVlKHdpZGdldElkLCB2YWx1ZSwgb25DaGFuZ2VJblBhZ2UpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgW29uQ2hhbmdlSW5QYWdlXVxyXG4gICAgKTtcclxuXHJcbiAgICBjb25zdCBoYW5kbGVPcGVuUGFnZUNvbnRleHRNZW51ID0gdXNlQ2FsbGJhY2soXHJcbiAgICAgICAgaGFuZGxlT3BlblBhZ2VDb250ZXh0TWVudUJ1aWxkKHtcclxuICAgICAgICAgICAgY29udGV4dE1lbnVEYXRhLFxyXG4gICAgICAgICAgICBvcGVuU2V0dGluZ3NEaXNhYmxlZCxcclxuICAgICAgICAgICAgZWRpdGlvbkxldmVsLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFdpZGdldElkOiBzZWxlY3RlZFdpZGdldCAmJiBzZWxlY3RlZFdpZGdldC5pZCxcclxuICAgICAgICAgICAgc2VsZWN0V2lkZ2V0LFxyXG4gICAgICAgICAgICBzZXRDb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgICAgIHdyYXBwZXJSZWYsXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgW1xyXG4gICAgICAgICAgICBjb250ZXh0TWVudURhdGEsXHJcbiAgICAgICAgICAgIG9wZW5TZXR0aW5nc0Rpc2FibGVkLFxyXG4gICAgICAgICAgICBlZGl0aW9uTGV2ZWwsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0LFxyXG4gICAgICAgICAgICB3cmFwcGVyUmVmLmN1cnJlbnQsXHJcbiAgICAgICAgXVxyXG4gICAgKTtcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxQYWdlV3JhcHBlclxyXG4gICAgICAgICAgICBvcmllbnRhdGlvbj17cGFnZS5vcmllbnRhdGlvbn1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtgJHtjbGFzc05hbWV9ICR7IXBhZ2VEZXNpZ25EaXNhYmxlZCAmJiAncGFnZS1kZXNpZ24nfWB9XHJcbiAgICAgICAgICAgIHNob3dPdXRsaW5lPXshcGFnZURlc2lnbkRpc2FibGVkICYmIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZH1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIHtjb250ZXh0TWVudURhdGEuc2hvdyAmJiAoXHJcbiAgICAgICAgICAgICAgICA8Q29udGV4dE1lbnVcclxuICAgICAgICAgICAgICAgICAgICBjb3BpZWRXaWRnZXQ9e2NvcGllZFdpZGdldH1cclxuICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVSZW1vdmU9eygpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uRGVsZXRlV2lkZ2V0KHNlbGVjdGVkV2lkZ2V0LmlkLCBvbkNoYW5nZUluUGFnZSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlT3BlblNldHRpbmdzPXtoYW5kbGVPcGVuU2V0dGluZ3NCdWlsZChcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldHRpbmdzUGFuZWxSZWZcclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVBhc3RlRWxlbWVudD17aGFuZGxlUGFzdGVFbGVtZW50fVxyXG4gICAgICAgICAgICAgICAgICAgIG9wZW5TZXR0aW5nc0Rpc2FibGVkPXtvcGVuU2V0dGluZ3NEaXNhYmxlZH1cclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDb3B5V2lkZ2V0PXsoKSA9PiBoYW5kbGVDb3B5V2lkZ2V0KHNlbGVjdGVkV2lkZ2V0KX1cclxuICAgICAgICAgICAgICAgICAgICBpc1NlbGVjdGVkSXRlbT17XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0ICYmIHNlbGVjdGVkV2lkZ2V0LmlkICE9PSBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xvc2U9e2hhbmRsZUNsb3NlQ29udGV4dE1lbnV9XHJcbiAgICAgICAgICAgICAgICAgICAgeD17Y29udGV4dE1lbnVEYXRhLnh9XHJcbiAgICAgICAgICAgICAgICAgICAgeT17Y29udGV4dE1lbnVEYXRhLnl9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApfVxyXG4gICAgICAgICAgICB7Y29udGV4dE1lbnVEYXRhLnNob3cgJiYgKFxyXG4gICAgICAgICAgICAgICAgPENvbnRleHRNZW51QmFja0Ryb3BcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZUNsb3NlQ29udGV4dE1lbnUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0V2lkZ2V0KG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgb25Db250ZXh0TWVudT17aGFuZGxlQ2xvc2VDb250ZXh0TWVudX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgIDxXcmFwcGVyXHJcbiAgICAgICAgICAgICAgICBzaG93R3JpZD17c2hvd0d1aWRlc31cclxuICAgICAgICAgICAgICAgIG9uQ29udGV4dE1lbnU9e2hhbmRsZU9wZW5QYWdlQ29udGV4dE1lbnV9XHJcbiAgICAgICAgICAgICAgICByZWY9e3dyYXBwZXJSZWZ9XHJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXtoYW5kbGVDbGlja0NvbnRhaW5lckJ1aWxkKFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRleHRNZW51RGF0YSxcclxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDbG9zZUNvbnRleHRNZW51LFxyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0ICYmIHNlbGVjdGVkV2lkZ2V0LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgIHNlbGVjdFdpZGdldCxcclxuICAgICAgICAgICAgICAgICAgICB3cmFwcGVyUmVmXHJcbiAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7cGFnZVdpZGdldHMgJiZcclxuICAgICAgICAgICAgICAgICAgICBwYWdlV2lkZ2V0cy5zb3J0KHNvcnRpbmdXaWRnZXRzQnlPcmRlcikubWFwKCh3aWRnZXQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxHcmlkSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW5jeUNvbmZpZz17Y3VycmVuY3lDb25maWd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsPXtlZGl0aW9uTGV2ZWx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2xvc2VDb250ZXh0TWVudT17aGFuZGxlQ2xvc2VDb250ZXh0TWVudX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDb3B5V2lkZ2V0PXsoKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVDb3B5V2lkZ2V0KHNlbGVjdGVkV2lkZ2V0KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVPcGVuQ29udGV4dE1lbnU9e2hhbmRsZU9wZW5JdGVtQ29udGV4dE1lbnVCdWlsZChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0Q29udGV4dE1lbnVEYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVPcGVuU2V0dGluZ3M9e2hhbmRsZU9wZW5TZXR0aW5nc0J1aWxkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RlZFdpZGdldCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0dGluZ3NQYW5lbFJlZlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlUmVtb3ZlPXsoKSA9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkRlbGV0ZVdpZGdldChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkV2lkZ2V0ICYmIHNlbGVjdGVkV2lkZ2V0LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ9e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXt3aWRnZXQuaWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZU9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNlbGVjdGVkPXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXQgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXQuaWQgPT09IHdpZGdldC5pZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxlY3RXaWRnZXRJZD17c2VsZWN0V2lkZ2V0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFdpZGdldFBvc2l0aW9uPXtoYW5kbGVTZXRQb3NpdGlvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRXaWRnZXRTaXplPXtoYW5kbGVTZXRXaWRnZXRTaXplfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldFdpZGdldFZhbHVlPXtoYW5kbGVTZXRXaWRnZXRWYWx1ZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzaG93R3VpZGVzPXtzaG93R3VpZGVzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzPXt0YWJsZVdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVR5cGU9e3RlbXBsYXRlVHlwZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRPYmplY3Q9e3dpZGdldH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgIDwvV3JhcHBlcj5cclxuICAgICAgICAgICAgeyFpc051bGwoZHJvcHBpbmdJdGVtKSAmJiAoXHJcbiAgICAgICAgICAgICAgICA8UGxhY2Vob2xkZXJJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgZHJvcHBpbmdJdGVtPXtkcm9wcGluZ0l0ZW19XHJcbiAgICAgICAgICAgICAgICAgICAgb25IYW5kbGVEcm9wPXtoYW5kbGVEcm9wQnVpbGQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRyb3BwaW5nSXRlbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb25BZGRXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldERyb3BwaW5nSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZU9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9ufVxyXG4gICAgICAgICAgICAgICAgICAgIHNob3dHdWlkZXM9e3Nob3dHdWlkZXN9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICApfVxyXG5cclxuICAgICAgICAgICAgPFNldHRpbmdzUGFuZWxcclxuICAgICAgICAgICAgICAgIGNhdGFsb2dzPXthdmFpbGFibGVDYXRhbG9nc31cclxuICAgICAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgICAgIHByb3Bvc2FsU2VsZWN0b3JEaXNhYmxlZD17ZWRpdGlvbkxldmVsID09PSBQQVJUSUFMX0VESVRJT05fTU9ERX1cclxuICAgICAgICAgICAgICAgIHJlZj17c2V0dGluZ3NQYW5lbFJlZn1cclxuICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzPXt0YWJsZVdpZGdldHNOYW1lc31cclxuICAgICAgICAgICAgICAgIHNldFdpZGdldFN0eWxlPXtoYW5kbGVTZXRXaWRnZXRTdHlsZX1cclxuICAgICAgICAgICAgICAgIHNldFdpZGdldFZhbHVlPXtoYW5kbGVTZXRXaWRnZXRWYWx1ZX1cclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVHlwZT17dGVtcGxhdGVUeXBlfVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICB7ZWRpdGlvbkxldmVsID09PSBQQVJUSUFMX0VESVRJT05fTU9ERSAmJiAoXHJcbiAgICAgICAgICAgICAgICA8RHVwbGljYXRlQnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2hhZG93LXNtXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBvbkR1cGxpY2F0ZVBhZ2UocGFnZS5pZCl9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWNvcHlcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9EdXBsaWNhdGVCdXR0b24+XHJcbiAgICAgICAgICAgICl9XHJcbiAgICAgICAgPC9QYWdlV3JhcHBlcj5cclxuICAgICk7XHJcbn07XHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xyXG4gICAgY29waWVkV2lkZ2V0OiBzZWxlY3RvcnMuZ2V0Q29waWVkSXRlbSxcclxuICAgIGRyb3BwaW5nSXRlbTogc2VsZWN0b3JzLmdldERyb3BwaW5nSXRlbSxcclxuICAgIHNlbGVjdGVkV2lkZ2V0SWQ6IHNlbGVjdG9ycy5nZXRTZWxlY3RlZFdpZGdldElkLFxyXG59KTtcclxuXHJcbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT4gKHtcclxuICAgIGFkZFdpZGdldDogKFxyXG4gICAgICAgIHBhZ2VJZCxcclxuICAgICAgICB0eXBlLFxyXG4gICAgICAgIGRlZmF1bHRQb3NpdGlvbixcclxuICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICBvbkNoYW5nZUluUGFnZVxyXG4gICAgKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLmFkZFdpZGdldChcclxuICAgICAgICAgICAgICAgIHBhZ2VJZCxcclxuICAgICAgICAgICAgICAgIHR5cGUsXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0UG9zaXRpb24sXHJcbiAgICAgICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApLFxyXG4gICAgbW92ZVdpZGdldEJ5S2V5OiAoXHJcbiAgICAgICAga2V5Q29kZSxcclxuICAgICAgICBvcmllbnRhdGlvbixcclxuICAgICAgICBzZXRXaWRnZXRQb3NpdGlvbixcclxuICAgICAgICBzaG93R3VpZGVzLFxyXG4gICAgICAgIHNlbGVjdGVkV2lkZ2V0XHJcbiAgICApID0+XHJcbiAgICAgICAgZGlzcGF0Y2goXHJcbiAgICAgICAgICAgIGFjdGlvbnMubW92ZVdpZGdldEJ5S2V5KFxyXG4gICAgICAgICAgICAgICAga2V5Q29kZSxcclxuICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uLFxyXG4gICAgICAgICAgICAgICAgc2V0V2lkZ2V0UG9zaXRpb24sXHJcbiAgICAgICAgICAgICAgICBzaG93R3VpZGVzLFxyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRXaWRnZXRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICksXHJcbiAgICBvbkRlbGV0ZVdpZGdldDogKHdpZGdldElkLCBvbkNoYW5nZUluUGFnZSkgPT5cclxuICAgICAgICBkaXNwYXRjaCh0ZW1wbGF0ZUNvcmVBY3Rpb25zLmRlbGV0ZVdpZGdldCh3aWRnZXRJZCwgb25DaGFuZ2VJblBhZ2UpKSxcclxuICAgIG9uRHVwbGljYXRlUGFnZTogKGlkKSA9PiBkaXNwYXRjaCh0ZW1wbGF0ZUNvcmVBY3Rpb25zLmR1cGxpY2F0ZVBhZ2UoaWQpKSxcclxuICAgIHNldERyb3BwaW5nSXRlbTogKGl0ZW0pID0+IGRpc3BhdGNoKGFjdGlvbnMuZHJvcHBpbmdJdGVtKGl0ZW0pKSxcclxuICAgIHBhc3RlV2lkZ2V0OiAoXHJcbiAgICAgICAgcGFnZUlkLFxyXG4gICAgICAgIG9wdGlvbnMsXHJcbiAgICAgICAgc2hvd0d1aWRlcyxcclxuICAgICAgICBwb3NpdGlvbixcclxuICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICBvbkNoYW5nZUluUGFnZVxyXG4gICAgKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnBhc3RlV2lkZ2V0KFxyXG4gICAgICAgICAgICAgICAgcGFnZUlkLFxyXG4gICAgICAgICAgICAgICAgb3B0aW9ucyxcclxuICAgICAgICAgICAgICAgIHNob3dHdWlkZXMsXHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbixcclxuICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICksXHJcbiAgICBzZWxlY3RXaWRnZXQ6ICh3aWRnZXRJZCwgcGFnZUlkKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKGFjdGlvbnMuc2VsZWN0V2lkZ2V0KHdpZGdldElkLCBwYWdlSWQpKSxcclxuICAgIHNldENvcGllZFdpZGdldDogKGRhdGEpID0+IGRpc3BhdGNoKGFjdGlvbnMuc2V0Q29waWVkSXRlbShkYXRhKSksXHJcbiAgICBzZXRXaWRnZXRQb3NpdGlvbjogKHdpZGdldElkLCBjaGFuZ2VkWCwgY2hhbmdlZFksIG9uQ2hhbmdlSW5QYWdlKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldFdpZGdldFBvc2l0aW9uKFxyXG4gICAgICAgICAgICAgICAgd2lkZ2V0SWQsXHJcbiAgICAgICAgICAgICAgICBjaGFuZ2VkWCxcclxuICAgICAgICAgICAgICAgIGNoYW5nZWRZLFxyXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2VcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICksXHJcbiAgICBzZXRXaWRnZXRTaXplOiAod2lkZ2V0SWQsIGNoYW5nZWRXaWR0aCwgY2hhbmdlZEhlaWdodCwgb25DaGFuZ2VJblBhZ2UpID0+XHJcbiAgICAgICAgZGlzcGF0Y2goXHJcbiAgICAgICAgICAgIHRlbXBsYXRlQ29yZUFjdGlvbnMuc2V0V2lkZ2V0U2l6ZShcclxuICAgICAgICAgICAgICAgIHdpZGdldElkLFxyXG4gICAgICAgICAgICAgICAgY2hhbmdlZFdpZHRoLFxyXG4gICAgICAgICAgICAgICAgY2hhbmdlZEhlaWdodCxcclxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApLFxyXG4gICAgc2V0V2lkZ2V0U3R5bGU6ICh3aWRnZXRJZCwgc3R5bGUsIG9uQ2hhbmdlSW5QYWdlKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldFdpZGdldFN0eWxlKHdpZGdldElkLCBzdHlsZSwgb25DaGFuZ2VJblBhZ2UpXHJcbiAgICAgICAgKSxcclxuICAgIHNldFdpZGdldFZhbHVlOiAod2lkZ2V0SWQsIHZhbHVlLCBvbkNoYW5nZUluUGFnZSkgPT5cclxuICAgICAgICBkaXNwYXRjaChcclxuICAgICAgICAgICAgdGVtcGxhdGVDb3JlQWN0aW9ucy5zZXRXaWRnZXRWYWx1ZSh3aWRnZXRJZCwgdmFsdWUsIG9uQ2hhbmdlSW5QYWdlKVxyXG4gICAgICAgICksXHJcbn0pO1xyXG5cclxuUGFnZVZpZXcucHJvcFR5cGVzID0ge1xyXG4gICAgYWRkV2lkZ2V0OiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIGF2YWlsYWJsZUNhdGFsb2dzOiBQcm9wVHlwZXMuYXJyYXksXHJcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgY29waWVkV2lkZ2V0OiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgZHJvcHBpbmdJdGVtOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgZWRpdGlvbkxldmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIG1vdmVXaWRnZXRCeUtleTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBvbkRlbGV0ZVdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBvbkR1cGxpY2F0ZVBhZ2U6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgcGFnZTogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBwYXN0ZVdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBwcm9wb3NhbHNOdW1iZXI6IFByb3BUeXBlcy5udW1iZXIsXHJcbiAgICBzZWxlY3RlZFdpZGdldElkOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgc2VsZWN0V2lkZ2V0OiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIHNldENvcGllZFdpZGdldDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZXREcm9wcGluZ0l0ZW06IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgc2V0V2lkZ2V0UG9zaXRpb246IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgc2V0V2lkZ2V0U2l6ZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZXRXaWRnZXRTdHlsZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZXRXaWRnZXRWYWx1ZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzaG93R3VpZGVzOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIHRhYmxlV2lkZ2V0c05hbWVzOiBQcm9wVHlwZXMuYXJyYXksXHJcbiAgICB0ZW1wbGF0ZVR5cGU6IFByb3BUeXBlcy5udW1iZXIsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShQYWdlVmlldyk7XHJcbiJdfQ==