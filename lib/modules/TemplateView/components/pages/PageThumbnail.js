"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactWebfontLoader = _interopRequireDefault(require("@dr-kobros/react-webfont-loader"));

var _round = _interopRequireDefault(require("lodash/round"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _froala = require("../../../../constants/froala");

var _template = require("../../../../constants/template");

var _contexts = require("../../../../helpers/contexts");

var mainSelectors = _interopRequireWildcard(require("../../../main/selectors"));

var _helpers = require("../../helpers");

var _WidgetPreview = _interopRequireDefault(require("../GridItem/WidgetPreview"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    ", "\n    transform-origin: left top;\n    ", "\n    -moz-transform-origin: left top;\n    position: relative;\n"])), function (_ref) {
  var pageScale = _ref.pageScale;
  return "transform: scale(".concat(pageScale, ");");
}, function (_ref2) {
  var pageScale = _ref2.pageScale;
  return "-moz-transform: scale(".concat(pageScale, ");");
});

var PageWrapper = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    background-color: #fff;\n    height: ", "px;\n    width: ", "px;\n    min-height: ", "px;\n    min-width: ", "px;\n    position: relative;\n"])), function (_ref3) {
  var orientation = _ref3.orientation,
      newPageHeight = _ref3.newPageHeight,
      newPageWidth = _ref3.newPageWidth;
  return orientation === 'landscape' ? newPageWidth : newPageHeight;
}, function (_ref4) {
  var orientation = _ref4.orientation,
      newPageHeight = _ref4.newPageHeight,
      newPageWidth = _ref4.newPageWidth;
  return orientation === 'landscape' ? newPageHeight : newPageWidth;
}, function (_ref5) {
  var orientation = _ref5.orientation,
      newPageHeight = _ref5.newPageHeight,
      newPageWidth = _ref5.newPageWidth;
  return orientation === 'landscape' ? newPageWidth : newPageHeight;
}, function (_ref6) {
  var orientation = _ref6.orientation,
      newPageHeight = _ref6.newPageHeight,
      newPageWidth = _ref6.newPageWidth;
  return orientation === 'landscape' ? newPageHeight : newPageWidth;
});

var PageThumbnail = function PageThumbnail(_ref7) {
  var chartWidgets = _ref7.chartWidgets,
      className = _ref7.className,
      froalaEditorConfig = _ref7.froalaEditorConfig,
      infiniteModeEnabled = _ref7.infiniteModeEnabled,
      page = _ref7.page,
      _ref7$pageScale = _ref7.pageScale,
      pageScale = _ref7$pageScale === void 0 ? _template.PAGE_ZOOM : _ref7$pageScale,
      tableWidgets = _ref7.tableWidgets;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      currencyConfig = _useContext.currencyConfig;

  var tableWidgetsNames = (0, _react.useMemo)(function () {
    return tableWidgets.map(function (tableWidget) {
      return tableWidget.id;
    });
  }, [tableWidgets]);
  var chartWidgetsNames = (0, _react.useMemo)(function () {
    return chartWidgets.map(function (chartWidget) {
      return chartWidget.id;
    });
  }, [chartWidgets]);

  var _useMemo = (0, _react.useMemo)(function () {
    return [(0, _round.default)(pageScale * _template.PAGE_WIDTH, 3), (0, _round.default)(pageScale * _template.PAGE_HEIGHT, 3)];
  }, [pageScale]),
      _useMemo2 = _slicedToArray(_useMemo, 2),
      newPageWidth = _useMemo2[0],
      newPageHeight = _useMemo2[1];

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactWebfontLoader.default, {
    config: _froala.FONT_CONFIG,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(PageWrapper, {
      className: className,
      newPageHeight: newPageHeight,
      newPageWidth: newPageWidth,
      orientation: page.orientation,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
        pageScale: pageScale,
        children: page.widgets && page.widgets.sort(_helpers.sortingWidgetsByOrder).map(function (widget) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetPreview.default, {
            chartWidgetsNames: chartWidgetsNames,
            currencyConfig: currencyConfig,
            froalaEditorConfig: froalaEditorConfig,
            infiniteModeEnabled: infiniteModeEnabled,
            tableWidgetsNames: tableWidgetsNames,
            widgetObject: widget
          }, widget.id);
        })
      })
    })
  });
};

PageThumbnail.propTypes = {
  chartWidgets: _propTypes.default.array,
  className: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  infiniteModeEnabled: _propTypes.default.bool,
  page: _propTypes.default.object,
  pageScale: _propTypes.default.number,
  tableWidgets: _propTypes.default.array
};
var mapStateToProps = (0, _reselect.createStructuredSelector)({
  chartWidgets: mainSelectors.getDataFetchChartWidgets,
  tableWidgets: mainSelectors.getDataFetchTableWidgets
});

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(PageThumbnail);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3BhZ2VzL1BhZ2VUaHVtYm5haWwuanMiXSwibmFtZXMiOlsiV3JhcHBlciIsInN0eWxlZCIsImRpdiIsInBhZ2VTY2FsZSIsIlBhZ2VXcmFwcGVyIiwib3JpZW50YXRpb24iLCJuZXdQYWdlSGVpZ2h0IiwibmV3UGFnZVdpZHRoIiwiUGFnZVRodW1ibmFpbCIsImNoYXJ0V2lkZ2V0cyIsImNsYXNzTmFtZSIsImZyb2FsYUVkaXRvckNvbmZpZyIsImluZmluaXRlTW9kZUVuYWJsZWQiLCJwYWdlIiwiUEFHRV9aT09NIiwidGFibGVXaWRnZXRzIiwiR2VuZXJhbENvbnRleHQiLCJjdXJyZW5jeUNvbmZpZyIsInRhYmxlV2lkZ2V0c05hbWVzIiwibWFwIiwidGFibGVXaWRnZXQiLCJpZCIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiY2hhcnRXaWRnZXQiLCJQQUdFX1dJRFRIIiwiUEFHRV9IRUlHSFQiLCJGT05UX0NPTkZJRyIsIndpZGdldHMiLCJzb3J0Iiwic29ydGluZ1dpZGdldHNCeU9yZGVyIiwid2lkZ2V0IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJzdHJpbmciLCJvYmplY3QiLCJib29sIiwibnVtYmVyIiwibWFwU3RhdGVUb1Byb3BzIiwibWFpblNlbGVjdG9ycyIsImdldERhdGFGZXRjaENoYXJ0V2lkZ2V0cyIsImdldERhdGFGZXRjaFRhYmxlV2lkZ2V0cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsT0FBTyxHQUFHQywwQkFBT0MsR0FBVixpT0FHUDtBQUFBLE1BQUdDLFNBQUgsUUFBR0EsU0FBSDtBQUFBLG9DQUF1Q0EsU0FBdkM7QUFBQSxDQUhPLEVBS1A7QUFBQSxNQUFHQSxTQUFILFNBQUdBLFNBQUg7QUFBQSx5Q0FBNENBLFNBQTVDO0FBQUEsQ0FMTyxDQUFiOztBQVVBLElBQU1DLFdBQVcsR0FBR0gsMEJBQU9DLEdBQVYsME5BRUg7QUFBQSxNQUFHRyxXQUFILFNBQUdBLFdBQUg7QUFBQSxNQUFnQkMsYUFBaEIsU0FBZ0JBLGFBQWhCO0FBQUEsTUFBK0JDLFlBQS9CLFNBQStCQSxZQUEvQjtBQUFBLFNBQ05GLFdBQVcsS0FBSyxXQUFoQixHQUE4QkUsWUFBOUIsR0FBNkNELGFBRHZDO0FBQUEsQ0FGRyxFQUlKO0FBQUEsTUFBR0QsV0FBSCxTQUFHQSxXQUFIO0FBQUEsTUFBZ0JDLGFBQWhCLFNBQWdCQSxhQUFoQjtBQUFBLE1BQStCQyxZQUEvQixTQUErQkEsWUFBL0I7QUFBQSxTQUNMRixXQUFXLEtBQUssV0FBaEIsR0FBOEJDLGFBQTlCLEdBQThDQyxZQUR6QztBQUFBLENBSkksRUFNQztBQUFBLE1BQUdGLFdBQUgsU0FBR0EsV0FBSDtBQUFBLE1BQWdCQyxhQUFoQixTQUFnQkEsYUFBaEI7QUFBQSxNQUErQkMsWUFBL0IsU0FBK0JBLFlBQS9CO0FBQUEsU0FDVkYsV0FBVyxLQUFLLFdBQWhCLEdBQThCRSxZQUE5QixHQUE2Q0QsYUFEbkM7QUFBQSxDQU5ELEVBUUE7QUFBQSxNQUFHRCxXQUFILFNBQUdBLFdBQUg7QUFBQSxNQUFnQkMsYUFBaEIsU0FBZ0JBLGFBQWhCO0FBQUEsTUFBK0JDLFlBQS9CLFNBQStCQSxZQUEvQjtBQUFBLFNBQ1RGLFdBQVcsS0FBSyxXQUFoQixHQUE4QkMsYUFBOUIsR0FBOENDLFlBRHJDO0FBQUEsQ0FSQSxDQUFqQjs7QUFhQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLFFBUWhCO0FBQUEsTUFQRkMsWUFPRSxTQVBGQSxZQU9FO0FBQUEsTUFORkMsU0FNRSxTQU5GQSxTQU1FO0FBQUEsTUFMRkMsa0JBS0UsU0FMRkEsa0JBS0U7QUFBQSxNQUpGQyxtQkFJRSxTQUpGQSxtQkFJRTtBQUFBLE1BSEZDLElBR0UsU0FIRkEsSUFHRTtBQUFBLDhCQUZGVixTQUVFO0FBQUEsTUFGRkEsU0FFRSxnQ0FGVVcsbUJBRVY7QUFBQSxNQURGQyxZQUNFLFNBREZBLFlBQ0U7O0FBQ0Ysb0JBQTJCLHVCQUFXQyx3QkFBWCxDQUEzQjtBQUFBLE1BQVFDLGNBQVIsZUFBUUEsY0FBUjs7QUFDQSxNQUFNQyxpQkFBaUIsR0FBRyxvQkFDdEI7QUFBQSxXQUFNSCxZQUFZLENBQUNJLEdBQWIsQ0FBaUIsVUFBQ0MsV0FBRDtBQUFBLGFBQWlCQSxXQUFXLENBQUNDLEVBQTdCO0FBQUEsS0FBakIsQ0FBTjtBQUFBLEdBRHNCLEVBRXRCLENBQUNOLFlBQUQsQ0FGc0IsQ0FBMUI7QUFJQSxNQUFNTyxpQkFBaUIsR0FBRyxvQkFDdEI7QUFBQSxXQUFNYixZQUFZLENBQUNVLEdBQWIsQ0FBaUIsVUFBQ0ksV0FBRDtBQUFBLGFBQWlCQSxXQUFXLENBQUNGLEVBQTdCO0FBQUEsS0FBakIsQ0FBTjtBQUFBLEdBRHNCLEVBRXRCLENBQUNaLFlBQUQsQ0FGc0IsQ0FBMUI7O0FBSUEsaUJBQXNDLG9CQUNsQztBQUFBLFdBQU0sQ0FDRixvQkFBTU4sU0FBUyxHQUFHcUIsb0JBQWxCLEVBQThCLENBQTlCLENBREUsRUFFRixvQkFBTXJCLFNBQVMsR0FBR3NCLHFCQUFsQixFQUErQixDQUEvQixDQUZFLENBQU47QUFBQSxHQURrQyxFQUtsQyxDQUFDdEIsU0FBRCxDQUxrQyxDQUF0QztBQUFBO0FBQUEsTUFBT0ksWUFBUDtBQUFBLE1BQXFCRCxhQUFyQjs7QUFRQSxzQkFDSSxxQkFBQywyQkFBRDtBQUFlLElBQUEsTUFBTSxFQUFFb0IsbUJBQXZCO0FBQUEsMkJBQ0kscUJBQUMsV0FBRDtBQUNJLE1BQUEsU0FBUyxFQUFFaEIsU0FEZjtBQUVJLE1BQUEsYUFBYSxFQUFFSixhQUZuQjtBQUdJLE1BQUEsWUFBWSxFQUFFQyxZQUhsQjtBQUlJLE1BQUEsV0FBVyxFQUFFTSxJQUFJLENBQUNSLFdBSnRCO0FBQUEsNkJBTUkscUJBQUMsT0FBRDtBQUFTLFFBQUEsU0FBUyxFQUFFRixTQUFwQjtBQUFBLGtCQUNLVSxJQUFJLENBQUNjLE9BQUwsSUFDR2QsSUFBSSxDQUFDYyxPQUFMLENBQ0tDLElBREwsQ0FDVUMsOEJBRFYsRUFFS1YsR0FGTCxDQUVTLFVBQUNXLE1BQUQsRUFBWTtBQUNiLDhCQUNJLHFCQUFDLHNCQUFEO0FBQ0ksWUFBQSxpQkFBaUIsRUFBRVIsaUJBRHZCO0FBRUksWUFBQSxjQUFjLEVBQUVMLGNBRnBCO0FBR0ksWUFBQSxrQkFBa0IsRUFBRU4sa0JBSHhCO0FBSUksWUFBQSxtQkFBbUIsRUFDZkMsbUJBTFI7QUFRSSxZQUFBLGlCQUFpQixFQUFFTSxpQkFSdkI7QUFTSSxZQUFBLFlBQVksRUFBRVk7QUFUbEIsYUFPU0EsTUFBTSxDQUFDVCxFQVBoQixDQURKO0FBYUgsU0FoQkw7QUFGUjtBQU5KO0FBREosSUFESjtBQStCSCxDQXpERDs7QUEyREFiLGFBQWEsQ0FBQ3VCLFNBQWQsR0FBMEI7QUFDdEJ0QixFQUFBQSxZQUFZLEVBQUV1QixtQkFBVUMsS0FERjtBQUV0QnZCLEVBQUFBLFNBQVMsRUFBRXNCLG1CQUFVRSxNQUZDO0FBR3RCdkIsRUFBQUEsa0JBQWtCLEVBQUVxQixtQkFBVUcsTUFIUjtBQUl0QnZCLEVBQUFBLG1CQUFtQixFQUFFb0IsbUJBQVVJLElBSlQ7QUFLdEJ2QixFQUFBQSxJQUFJLEVBQUVtQixtQkFBVUcsTUFMTTtBQU10QmhDLEVBQUFBLFNBQVMsRUFBRTZCLG1CQUFVSyxNQU5DO0FBT3RCdEIsRUFBQUEsWUFBWSxFQUFFaUIsbUJBQVVDO0FBUEYsQ0FBMUI7QUFVQSxJQUFNSyxlQUFlLEdBQUcsd0NBQXlCO0FBQzdDN0IsRUFBQUEsWUFBWSxFQUFFOEIsYUFBYSxDQUFDQyx3QkFEaUI7QUFFN0N6QixFQUFBQSxZQUFZLEVBQUV3QixhQUFhLENBQUNFO0FBRmlCLENBQXpCLENBQXhCOztlQUtlLHlCQUFRSCxlQUFSLEVBQXlCLElBQXpCLEVBQStCOUIsYUFBL0IsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBXZWJmb250TG9hZGVyIGZyb20gJ0Bkci1rb2Jyb3MvcmVhY3Qtd2ViZm9udC1sb2FkZXInO1xyXG5pbXBvcnQgcm91bmQgZnJvbSAnbG9kYXNoL3JvdW5kJztcclxuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcclxuaW1wb3J0IHsgdXNlQ29udGV4dCwgdXNlTWVtbyB9IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcclxuaW1wb3J0IHsgY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuXHJcbmltcG9ydCB7IEZPTlRfQ09ORklHIH0gZnJvbSAnQGNvbnN0YW50cy9mcm9hbGEnO1xyXG5pbXBvcnQgeyBQQUdFX0hFSUdIVCwgUEFHRV9XSURUSCwgUEFHRV9aT09NIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XHJcblxyXG5pbXBvcnQgeyBHZW5lcmFsQ29udGV4dCB9IGZyb20gJ0BoZWxwZXJzL2NvbnRleHRzJztcclxuXHJcbmltcG9ydCAqIGFzIG1haW5TZWxlY3RvcnMgZnJvbSAnQG1haW4vc2VsZWN0b3JzJztcclxuXHJcbmltcG9ydCB7IHNvcnRpbmdXaWRnZXRzQnlPcmRlciB9IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xyXG5pbXBvcnQgV2lkZ2V0UHJldmlldyBmcm9tICcuLi9HcmlkSXRlbS9XaWRnZXRQcmV2aWV3JztcclxuXHJcbmNvbnN0IFdyYXBwZXIgPSBzdHlsZWQuZGl2YFxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAkeyh7IHBhZ2VTY2FsZSB9KSA9PiBgdHJhbnNmb3JtOiBzY2FsZSgke3BhZ2VTY2FsZX0pO2B9XHJcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiBsZWZ0IHRvcDtcclxuICAgICR7KHsgcGFnZVNjYWxlIH0pID0+IGAtbW96LXRyYW5zZm9ybTogc2NhbGUoJHtwYWdlU2NhbGV9KTtgfVxyXG4gICAgLW1vei10cmFuc2Zvcm0tb3JpZ2luOiBsZWZ0IHRvcDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuYDtcclxuXHJcbmNvbnN0IFBhZ2VXcmFwcGVyID0gc3R5bGVkLmRpdmBcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICBoZWlnaHQ6ICR7KHsgb3JpZW50YXRpb24sIG5ld1BhZ2VIZWlnaHQsIG5ld1BhZ2VXaWR0aCB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IG5ld1BhZ2VXaWR0aCA6IG5ld1BhZ2VIZWlnaHR9cHg7XHJcbiAgICB3aWR0aDogJHsoeyBvcmllbnRhdGlvbiwgbmV3UGFnZUhlaWdodCwgbmV3UGFnZVdpZHRoIH0pID0+XHJcbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdsYW5kc2NhcGUnID8gbmV3UGFnZUhlaWdodCA6IG5ld1BhZ2VXaWR0aH1weDtcclxuICAgIG1pbi1oZWlnaHQ6ICR7KHsgb3JpZW50YXRpb24sIG5ld1BhZ2VIZWlnaHQsIG5ld1BhZ2VXaWR0aCB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IG5ld1BhZ2VXaWR0aCA6IG5ld1BhZ2VIZWlnaHR9cHg7XHJcbiAgICBtaW4td2lkdGg6ICR7KHsgb3JpZW50YXRpb24sIG5ld1BhZ2VIZWlnaHQsIG5ld1BhZ2VXaWR0aCB9KSA9PlxyXG4gICAgICAgIG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJyA/IG5ld1BhZ2VIZWlnaHQgOiBuZXdQYWdlV2lkdGh9cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbmA7XHJcblxyXG5jb25zdCBQYWdlVGh1bWJuYWlsID0gKHtcclxuICAgIGNoYXJ0V2lkZ2V0cyxcclxuICAgIGNsYXNzTmFtZSxcclxuICAgIGZyb2FsYUVkaXRvckNvbmZpZyxcclxuICAgIGluZmluaXRlTW9kZUVuYWJsZWQsXHJcbiAgICBwYWdlLFxyXG4gICAgcGFnZVNjYWxlID0gUEFHRV9aT09NLFxyXG4gICAgdGFibGVXaWRnZXRzLFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCB7IGN1cnJlbmN5Q29uZmlnIH0gPSB1c2VDb250ZXh0KEdlbmVyYWxDb250ZXh0KTtcclxuICAgIGNvbnN0IHRhYmxlV2lkZ2V0c05hbWVzID0gdXNlTWVtbyhcclxuICAgICAgICAoKSA9PiB0YWJsZVdpZGdldHMubWFwKCh0YWJsZVdpZGdldCkgPT4gdGFibGVXaWRnZXQuaWQpLFxyXG4gICAgICAgIFt0YWJsZVdpZGdldHNdXHJcbiAgICApO1xyXG4gICAgY29uc3QgY2hhcnRXaWRnZXRzTmFtZXMgPSB1c2VNZW1vKFxyXG4gICAgICAgICgpID0+IGNoYXJ0V2lkZ2V0cy5tYXAoKGNoYXJ0V2lkZ2V0KSA9PiBjaGFydFdpZGdldC5pZCksXHJcbiAgICAgICAgW2NoYXJ0V2lkZ2V0c11cclxuICAgICk7XHJcbiAgICBjb25zdCBbbmV3UGFnZVdpZHRoLCBuZXdQYWdlSGVpZ2h0XSA9IHVzZU1lbW8oXHJcbiAgICAgICAgKCkgPT4gW1xyXG4gICAgICAgICAgICByb3VuZChwYWdlU2NhbGUgKiBQQUdFX1dJRFRILCAzKSxcclxuICAgICAgICAgICAgcm91bmQocGFnZVNjYWxlICogUEFHRV9IRUlHSFQsIDMpLFxyXG4gICAgICAgIF0sXHJcbiAgICAgICAgW3BhZ2VTY2FsZV1cclxuICAgICk7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8V2ViZm9udExvYWRlciBjb25maWc9e0ZPTlRfQ09ORklHfT5cclxuICAgICAgICAgICAgPFBhZ2VXcmFwcGVyXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzTmFtZX1cclxuICAgICAgICAgICAgICAgIG5ld1BhZ2VIZWlnaHQ9e25ld1BhZ2VIZWlnaHR9XHJcbiAgICAgICAgICAgICAgICBuZXdQYWdlV2lkdGg9e25ld1BhZ2VXaWR0aH1cclxuICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9ufVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8V3JhcHBlciBwYWdlU2NhbGU9e3BhZ2VTY2FsZX0+XHJcbiAgICAgICAgICAgICAgICAgICAge3BhZ2Uud2lkZ2V0cyAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlLndpZGdldHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5zb3J0KHNvcnRpbmdXaWRnZXRzQnlPcmRlcilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKHdpZGdldCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxXaWRnZXRQcmV2aWV3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFydFdpZGdldHNOYW1lcz17Y2hhcnRXaWRnZXRzTmFtZXN9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJyZW5jeUNvbmZpZz17Y3VycmVuY3lDb25maWd9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmcm9hbGFFZGl0b3JDb25maWc9e2Zyb2FsYUVkaXRvckNvbmZpZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlTW9kZUVuYWJsZWQ9e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlTW9kZUVuYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17d2lkZ2V0LmlkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkZ2V0T2JqZWN0PXt3aWRnZXR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pfVxyXG4gICAgICAgICAgICAgICAgPC9XcmFwcGVyPlxyXG4gICAgICAgICAgICA8L1BhZ2VXcmFwcGVyPlxyXG4gICAgICAgIDwvV2ViZm9udExvYWRlcj5cclxuICAgICk7XHJcbn07XHJcblxyXG5QYWdlVGh1bWJuYWlsLnByb3BUeXBlcyA9IHtcclxuICAgIGNoYXJ0V2lkZ2V0czogUHJvcFR5cGVzLmFycmF5LFxyXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgZnJvYWxhRWRpdG9yQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgaW5maW5pdGVNb2RlRW5hYmxlZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBwYWdlOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgcGFnZVNjYWxlOiBQcm9wVHlwZXMubnVtYmVyLFxyXG4gICAgdGFibGVXaWRnZXRzOiBQcm9wVHlwZXMuYXJyYXksXHJcbn07XHJcblxyXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xyXG4gICAgY2hhcnRXaWRnZXRzOiBtYWluU2VsZWN0b3JzLmdldERhdGFGZXRjaENoYXJ0V2lkZ2V0cyxcclxuICAgIHRhYmxlV2lkZ2V0czogbWFpblNlbGVjdG9ycy5nZXREYXRhRmV0Y2hUYWJsZVdpZGdldHMsXHJcbn0pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG51bGwpKFBhZ2VUaHVtYm5haWwpO1xyXG4iXX0=