"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _contexts = require("../../../../helpers/contexts");

var _helpers = require("../../helpers");

var _WidgetPreview = _interopRequireDefault(require("../GridItem/WidgetPreview"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    position: relative;\n"])));

var PageWrapper = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    background-color: #fff;\n    height: ", "px;\n    width: ", "px;\n    min-height: ", "px;\n    min-width: ", "px;\n    position: relative;\n    &.page-design {\n        box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);\n        margin: 24px 32px;\n    }\n    ", "\n"])), function (_ref) {
  var orientation = _ref.orientation;
  return orientation === 'landscape' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref2) {
  var orientation = _ref2.orientation;
  return orientation === 'landscape' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
}, function (_ref3) {
  var orientation = _ref3.orientation;
  return orientation === 'landscape' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref4) {
  var orientation = _ref4.orientation;
  return orientation === 'landscape' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
}, function (_ref5) {
  var showOutline = _ref5.showOutline;
  return showOutline && 'outline: #ff9a00 solid 2px;';
});

var PagePreview = function PagePreview(_ref6) {
  var chartWidgetsNames = _ref6.chartWidgetsNames,
      className = _ref6.className,
      froalaEditorConfig = _ref6.froalaEditorConfig,
      infinitePagesSupportEnabled = _ref6.infinitePagesSupportEnabled,
      page = _ref6.page,
      pageDesignDisabled = _ref6.pageDesignDisabled,
      tableWidgetsNames = _ref6.tableWidgetsNames;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      currencyConfig = _useContext.currencyConfig;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(PageWrapper, {
    orientation: page.orientation,
    showOutline: infinitePagesSupportEnabled && !pageDesignDisabled,
    className: "sunwise-template-core-page-preview ".concat(className, " ").concat(!pageDesignDisabled && 'page-design'),
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
      children: page.widgets && page.widgets.sort(_helpers.sortingWidgetsByOrder).map(function (widget) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetPreview.default, {
          chartWidgetsNames: chartWidgetsNames,
          currencyConfig: currencyConfig,
          froalaEditorConfig: froalaEditorConfig,
          infinitePagesSupportEnabled: infinitePagesSupportEnabled,
          tableWidgetsNames: tableWidgetsNames,
          widgetObject: widget
        }, widget.id);
      })
    })
  });
};

PagePreview.propTypes = {
  chartWidgetsNames: _propTypes.default.array,
  className: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  page: _propTypes.default.object,
  pageDesignDisabled: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array
};
var _default = PagePreview;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3BhZ2VzL1BhZ2VQcmV2aWV3LmpzIl0sIm5hbWVzIjpbIldyYXBwZXIiLCJzdHlsZWQiLCJkaXYiLCJQYWdlV3JhcHBlciIsIm9yaWVudGF0aW9uIiwiUEFHRV9XSURUSCIsIlBBR0VfSEVJR0hUIiwic2hvd091dGxpbmUiLCJQYWdlUHJldmlldyIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiY2xhc3NOYW1lIiwiZnJvYWxhRWRpdG9yQ29uZmlnIiwiaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkIiwicGFnZSIsInBhZ2VEZXNpZ25EaXNhYmxlZCIsInRhYmxlV2lkZ2V0c05hbWVzIiwiR2VuZXJhbENvbnRleHQiLCJjdXJyZW5jeUNvbmZpZyIsIndpZGdldHMiLCJzb3J0Iiwic29ydGluZ1dpZGdldHNCeU9yZGVyIiwibWFwIiwid2lkZ2V0IiwiaWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsInN0cmluZyIsIm9iamVjdCIsImJvb2wiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLElBQU1BLE9BQU8sR0FBR0MsMEJBQU9DLEdBQVYscUlBQWI7O0FBTUEsSUFBTUMsV0FBVyxHQUFHRiwwQkFBT0MsR0FBViw0VkFFSDtBQUFBLE1BQUdFLFdBQUgsUUFBR0EsV0FBSDtBQUFBLFNBQ05BLFdBQVcsS0FBSyxXQUFoQixHQUE4QkMsb0JBQTlCLEdBQTJDQyxxQkFEckM7QUFBQSxDQUZHLEVBSUo7QUFBQSxNQUFHRixXQUFILFNBQUdBLFdBQUg7QUFBQSxTQUNMQSxXQUFXLEtBQUssV0FBaEIsR0FBOEJFLHFCQUE5QixHQUE0Q0Qsb0JBRHZDO0FBQUEsQ0FKSSxFQU1DO0FBQUEsTUFBR0QsV0FBSCxTQUFHQSxXQUFIO0FBQUEsU0FDVkEsV0FBVyxLQUFLLFdBQWhCLEdBQThCQyxvQkFBOUIsR0FBMkNDLHFCQURqQztBQUFBLENBTkQsRUFRQTtBQUFBLE1BQUdGLFdBQUgsU0FBR0EsV0FBSDtBQUFBLFNBQ1RBLFdBQVcsS0FBSyxXQUFoQixHQUE4QkUscUJBQTlCLEdBQTRDRCxvQkFEbkM7QUFBQSxDQVJBLEVBZVg7QUFBQSxNQUFHRSxXQUFILFNBQUdBLFdBQUg7QUFBQSxTQUFxQkEsV0FBVyxJQUFJLDZCQUFwQztBQUFBLENBZlcsQ0FBakI7O0FBa0JBLElBQU1DLFdBQVcsR0FBRyxTQUFkQSxXQUFjLFFBUWQ7QUFBQSxNQVBGQyxpQkFPRSxTQVBGQSxpQkFPRTtBQUFBLE1BTkZDLFNBTUUsU0FORkEsU0FNRTtBQUFBLE1BTEZDLGtCQUtFLFNBTEZBLGtCQUtFO0FBQUEsTUFKRkMsMkJBSUUsU0FKRkEsMkJBSUU7QUFBQSxNQUhGQyxJQUdFLFNBSEZBLElBR0U7QUFBQSxNQUZGQyxrQkFFRSxTQUZGQSxrQkFFRTtBQUFBLE1BREZDLGlCQUNFLFNBREZBLGlCQUNFOztBQUNGLG9CQUEyQix1QkFBV0Msd0JBQVgsQ0FBM0I7QUFBQSxNQUFRQyxjQUFSLGVBQVFBLGNBQVI7O0FBQ0Esc0JBQ0kscUJBQUMsV0FBRDtBQUNJLElBQUEsV0FBVyxFQUFFSixJQUFJLENBQUNULFdBRHRCO0FBRUksSUFBQSxXQUFXLEVBQUVRLDJCQUEyQixJQUFJLENBQUNFLGtCQUZqRDtBQUdJLElBQUEsU0FBUywrQ0FBd0NKLFNBQXhDLGNBQ0wsQ0FBQ0ksa0JBQUQsSUFBdUIsYUFEbEIsQ0FIYjtBQUFBLDJCQU9JLHFCQUFDLE9BQUQ7QUFBQSxnQkFDS0QsSUFBSSxDQUFDSyxPQUFMLElBQ0dMLElBQUksQ0FBQ0ssT0FBTCxDQUFhQyxJQUFiLENBQWtCQyw4QkFBbEIsRUFBeUNDLEdBQXpDLENBQTZDLFVBQUNDLE1BQUQsRUFBWTtBQUNyRCw0QkFDSSxxQkFBQyxzQkFBRDtBQUNJLFVBQUEsaUJBQWlCLEVBQUViLGlCQUR2QjtBQUVJLFVBQUEsY0FBYyxFQUFFUSxjQUZwQjtBQUdJLFVBQUEsa0JBQWtCLEVBQUVOLGtCQUh4QjtBQUlJLFVBQUEsMkJBQTJCLEVBQ3ZCQywyQkFMUjtBQVFJLFVBQUEsaUJBQWlCLEVBQUVHLGlCQVJ2QjtBQVNJLFVBQUEsWUFBWSxFQUFFTztBQVRsQixXQU9TQSxNQUFNLENBQUNDLEVBUGhCLENBREo7QUFhSCxPQWREO0FBRlI7QUFQSixJQURKO0FBNEJILENBdENEOztBQXdDQWYsV0FBVyxDQUFDZ0IsU0FBWixHQUF3QjtBQUNwQmYsRUFBQUEsaUJBQWlCLEVBQUVnQixtQkFBVUMsS0FEVDtBQUVwQmhCLEVBQUFBLFNBQVMsRUFBRWUsbUJBQVVFLE1BRkQ7QUFHcEJoQixFQUFBQSxrQkFBa0IsRUFBRWMsbUJBQVVHLE1BSFY7QUFJcEJoQixFQUFBQSwyQkFBMkIsRUFBRWEsbUJBQVVJLElBSm5CO0FBS3BCaEIsRUFBQUEsSUFBSSxFQUFFWSxtQkFBVUcsTUFMSTtBQU1wQmQsRUFBQUEsa0JBQWtCLEVBQUVXLG1CQUFVSSxJQU5WO0FBT3BCZCxFQUFBQSxpQkFBaUIsRUFBRVUsbUJBQVVDO0FBUFQsQ0FBeEI7ZUFVZWxCLFciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyB1c2VDb250ZXh0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcclxuXHJcbmltcG9ydCB7IFBBR0VfSEVJR0hULCBQQUdFX1dJRFRIIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XHJcblxyXG5pbXBvcnQgeyBHZW5lcmFsQ29udGV4dCB9IGZyb20gJ0BoZWxwZXJzL2NvbnRleHRzJztcclxuXHJcbmltcG9ydCB7IHNvcnRpbmdXaWRnZXRzQnlPcmRlciB9IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xyXG5pbXBvcnQgV2lkZ2V0UHJldmlldyBmcm9tICcuLi9HcmlkSXRlbS9XaWRnZXRQcmV2aWV3JztcclxuXHJcbmNvbnN0IFdyYXBwZXIgPSBzdHlsZWQuZGl2YFxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbmA7XHJcblxyXG5jb25zdCBQYWdlV3JhcHBlciA9IHN0eWxlZC5kaXZgXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiAkeyh7IG9yaWVudGF0aW9uIH0pID0+XHJcbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdsYW5kc2NhcGUnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUfXB4O1xyXG4gICAgd2lkdGg6ICR7KHsgb3JpZW50YXRpb24gfSkgPT5cclxuICAgICAgICBvcmllbnRhdGlvbiA9PT0gJ2xhbmRzY2FwZScgPyBQQUdFX0hFSUdIVCA6IFBBR0VfV0lEVEh9cHg7XHJcbiAgICBtaW4taGVpZ2h0OiAkeyh7IG9yaWVudGF0aW9uIH0pID0+XHJcbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdsYW5kc2NhcGUnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUfXB4O1xyXG4gICAgbWluLXdpZHRoOiAkeyh7IG9yaWVudGF0aW9uIH0pID0+XHJcbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdsYW5kc2NhcGUnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIfXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgJi5wYWdlLWRlc2lnbiB7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMjBweCAycHggMTdweCAwIHJnYmEoMTIwLCAxMTIsIDE3OCwgMC4wOCk7XHJcbiAgICAgICAgbWFyZ2luOiAyNHB4IDMycHg7XHJcbiAgICB9XHJcbiAgICAkeyh7IHNob3dPdXRsaW5lIH0pID0+IHNob3dPdXRsaW5lICYmICdvdXRsaW5lOiAjZmY5YTAwIHNvbGlkIDJweDsnfVxyXG5gO1xyXG5cclxuY29uc3QgUGFnZVByZXZpZXcgPSAoe1xyXG4gICAgY2hhcnRXaWRnZXRzTmFtZXMsXHJcbiAgICBjbGFzc05hbWUsXHJcbiAgICBmcm9hbGFFZGl0b3JDb25maWcsXHJcbiAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQsXHJcbiAgICBwYWdlLFxyXG4gICAgcGFnZURlc2lnbkRpc2FibGVkLFxyXG4gICAgdGFibGVXaWRnZXRzTmFtZXMsXHJcbn0pID0+IHtcclxuICAgIGNvbnN0IHsgY3VycmVuY3lDb25maWcgfSA9IHVzZUNvbnRleHQoR2VuZXJhbENvbnRleHQpO1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8UGFnZVdyYXBwZXJcclxuICAgICAgICAgICAgb3JpZW50YXRpb249e3BhZ2Uub3JpZW50YXRpb259XHJcbiAgICAgICAgICAgIHNob3dPdXRsaW5lPXtpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQgJiYgIXBhZ2VEZXNpZ25EaXNhYmxlZH1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtgc3Vud2lzZS10ZW1wbGF0ZS1jb3JlLXBhZ2UtcHJldmlldyAke2NsYXNzTmFtZX0gJHtcclxuICAgICAgICAgICAgICAgICFwYWdlRGVzaWduRGlzYWJsZWQgJiYgJ3BhZ2UtZGVzaWduJ1xyXG4gICAgICAgICAgICB9YH1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIDxXcmFwcGVyPlxyXG4gICAgICAgICAgICAgICAge3BhZ2Uud2lkZ2V0cyAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHBhZ2Uud2lkZ2V0cy5zb3J0KHNvcnRpbmdXaWRnZXRzQnlPcmRlcikubWFwKCh3aWRnZXQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxXaWRnZXRQcmV2aWV3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXM9e2NoYXJ0V2lkZ2V0c05hbWVzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbmN5Q29uZmlnPXtjdXJyZW5jeUNvbmZpZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmcm9hbGFFZGl0b3JDb25maWc9e2Zyb2FsYUVkaXRvckNvbmZpZ31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ9e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXt3aWRnZXQuaWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZGdldE9iamVjdD17d2lkZ2V0fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9KX1cclxuICAgICAgICAgICAgPC9XcmFwcGVyPlxyXG4gICAgICAgIDwvUGFnZVdyYXBwZXI+XHJcbiAgICApO1xyXG59O1xyXG5cclxuUGFnZVByZXZpZXcucHJvcFR5cGVzID0ge1xyXG4gICAgY2hhcnRXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcclxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGZyb2FsYUVkaXRvckNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZDogUHJvcFR5cGVzLmJvb2wsXHJcbiAgICBwYWdlOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgcGFnZURlc2lnbkRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIHRhYmxlV2lkZ2V0c05hbWVzOiBQcm9wVHlwZXMuYXJyYXksXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBQYWdlUHJldmlldztcclxuIl19