"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _PageDisplay = _interopRequireDefault(require("./PageDisplay"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PageListItem = function PageListItem(_ref) {
  var availableCatalogs = _ref.availableCatalogs,
      chartWidgetsNames = _ref.chartWidgetsNames,
      editionLevel = _ref.editionLevel,
      froalaEditorConfig = _ref.froalaEditorConfig,
      infinitePagesSupportEnabled = _ref.infinitePagesSupportEnabled,
      page = _ref.page,
      pageDesignDisabled = _ref.pageDesignDisabled,
      showGuides = _ref.showGuides,
      tableWidgetsNames = _ref.tableWidgetsNames,
      templateType = _ref.templateType;

  if (page.type === 'group') {
    return page.pages.map(function (groupedPage) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(_PageDisplay.default, {
        availableCatalogs: availableCatalogs,
        chartWidgetsNames: chartWidgetsNames,
        editionLevel: editionLevel,
        froalaEditorConfig: froalaEditorConfig,
        infinitePagesSupportEnabled: infinitePagesSupportEnabled,
        page: groupedPage,
        pageDesignDisabled: pageDesignDisabled,
        showGuides: showGuides,
        tableWidgetsNames: tableWidgetsNames,
        templateType: templateType
      }, "preview-page-".concat(page.id, "/").concat(groupedPage.id));
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_PageDisplay.default, {
    availableCatalogs: availableCatalogs,
    chartWidgetsNames: chartWidgetsNames,
    editionLevel: editionLevel,
    froalaEditorConfig: froalaEditorConfig,
    infinitePagesSupportEnabled: infinitePagesSupportEnabled,
    page: page,
    pageDesignDisabled: pageDesignDisabled,
    showGuides: showGuides,
    tableWidgetsNames: tableWidgetsNames,
    templateType: templateType
  }, "preview-page-".concat(page.id));
};

PageListItem.propTypes = {
  availableCatalogs: _propTypes.default.array,
  chartWidgetsNames: _propTypes.default.array,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  page: _propTypes.default.object,
  pageDesignDisabled: _propTypes.default.bool,
  showGuides: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  templateType: _propTypes.default.number
};
var _default = PageListItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3BhZ2VzL1BhZ2VMaXN0SXRlbS5qcyJdLCJuYW1lcyI6WyJQYWdlTGlzdEl0ZW0iLCJhdmFpbGFibGVDYXRhbG9ncyIsImNoYXJ0V2lkZ2V0c05hbWVzIiwiZWRpdGlvbkxldmVsIiwiZnJvYWxhRWRpdG9yQ29uZmlnIiwiaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkIiwicGFnZSIsInBhZ2VEZXNpZ25EaXNhYmxlZCIsInNob3dHdWlkZXMiLCJ0YWJsZVdpZGdldHNOYW1lcyIsInRlbXBsYXRlVHlwZSIsInR5cGUiLCJwYWdlcyIsIm1hcCIsImdyb3VwZWRQYWdlIiwiaWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhcnJheSIsInN0cmluZyIsIm9iamVjdCIsImJvb2wiLCJudW1iZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHLFNBQWZBLFlBQWUsT0FXZjtBQUFBLE1BVkZDLGlCQVVFLFFBVkZBLGlCQVVFO0FBQUEsTUFURkMsaUJBU0UsUUFURkEsaUJBU0U7QUFBQSxNQVJGQyxZQVFFLFFBUkZBLFlBUUU7QUFBQSxNQVBGQyxrQkFPRSxRQVBGQSxrQkFPRTtBQUFBLE1BTkZDLDJCQU1FLFFBTkZBLDJCQU1FO0FBQUEsTUFMRkMsSUFLRSxRQUxGQSxJQUtFO0FBQUEsTUFKRkMsa0JBSUUsUUFKRkEsa0JBSUU7QUFBQSxNQUhGQyxVQUdFLFFBSEZBLFVBR0U7QUFBQSxNQUZGQyxpQkFFRSxRQUZGQSxpQkFFRTtBQUFBLE1BREZDLFlBQ0UsUUFERkEsWUFDRTs7QUFDRixNQUFJSixJQUFJLENBQUNLLElBQUwsS0FBYyxPQUFsQixFQUEyQjtBQUN2QixXQUFPTCxJQUFJLENBQUNNLEtBQUwsQ0FBV0MsR0FBWCxDQUFlLFVBQUNDLFdBQUQ7QUFBQSwwQkFDbEIscUJBQUMsb0JBQUQ7QUFDSSxRQUFBLGlCQUFpQixFQUFFYixpQkFEdkI7QUFFSSxRQUFBLGlCQUFpQixFQUFFQyxpQkFGdkI7QUFHSSxRQUFBLFlBQVksRUFBRUMsWUFIbEI7QUFJSSxRQUFBLGtCQUFrQixFQUFFQyxrQkFKeEI7QUFLSSxRQUFBLDJCQUEyQixFQUFFQywyQkFMakM7QUFPSSxRQUFBLElBQUksRUFBRVMsV0FQVjtBQVFJLFFBQUEsa0JBQWtCLEVBQUVQLGtCQVJ4QjtBQVNJLFFBQUEsVUFBVSxFQUFFQyxVQVRoQjtBQVVJLFFBQUEsaUJBQWlCLEVBQUVDLGlCQVZ2QjtBQVdJLFFBQUEsWUFBWSxFQUFFQztBQVhsQixnQ0FNeUJKLElBQUksQ0FBQ1MsRUFOOUIsY0FNb0NELFdBQVcsQ0FBQ0MsRUFOaEQsRUFEa0I7QUFBQSxLQUFmLENBQVA7QUFlSDs7QUFDRCxzQkFDSSxxQkFBQyxvQkFBRDtBQUNJLElBQUEsaUJBQWlCLEVBQUVkLGlCQUR2QjtBQUVJLElBQUEsaUJBQWlCLEVBQUVDLGlCQUZ2QjtBQUdJLElBQUEsWUFBWSxFQUFFQyxZQUhsQjtBQUlJLElBQUEsa0JBQWtCLEVBQUVDLGtCQUp4QjtBQUtJLElBQUEsMkJBQTJCLEVBQUVDLDJCQUxqQztBQU9JLElBQUEsSUFBSSxFQUFFQyxJQVBWO0FBUUksSUFBQSxrQkFBa0IsRUFBRUMsa0JBUnhCO0FBU0ksSUFBQSxVQUFVLEVBQUVDLFVBVGhCO0FBVUksSUFBQSxpQkFBaUIsRUFBRUMsaUJBVnZCO0FBV0ksSUFBQSxZQUFZLEVBQUVDO0FBWGxCLDRCQU15QkosSUFBSSxDQUFDUyxFQU45QixFQURKO0FBZUgsQ0E1Q0Q7O0FBOENBZixZQUFZLENBQUNnQixTQUFiLEdBQXlCO0FBQ3JCZixFQUFBQSxpQkFBaUIsRUFBRWdCLG1CQUFVQyxLQURSO0FBRXJCaEIsRUFBQUEsaUJBQWlCLEVBQUVlLG1CQUFVQyxLQUZSO0FBR3JCZixFQUFBQSxZQUFZLEVBQUVjLG1CQUFVRSxNQUhIO0FBSXJCZixFQUFBQSxrQkFBa0IsRUFBRWEsbUJBQVVHLE1BSlQ7QUFLckJmLEVBQUFBLDJCQUEyQixFQUFFWSxtQkFBVUksSUFMbEI7QUFNckJmLEVBQUFBLElBQUksRUFBRVcsbUJBQVVHLE1BTks7QUFPckJiLEVBQUFBLGtCQUFrQixFQUFFVSxtQkFBVUksSUFQVDtBQVFyQmIsRUFBQUEsVUFBVSxFQUFFUyxtQkFBVUksSUFSRDtBQVNyQlosRUFBQUEsaUJBQWlCLEVBQUVRLG1CQUFVQyxLQVRSO0FBVXJCUixFQUFBQSxZQUFZLEVBQUVPLG1CQUFVSztBQVZILENBQXpCO2VBYWV0QixZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IFBhZ2VEaXNwbGF5IGZyb20gJy4vUGFnZURpc3BsYXknO1xuXG5jb25zdCBQYWdlTGlzdEl0ZW0gPSAoe1xuICAgIGF2YWlsYWJsZUNhdGFsb2dzLFxuICAgIGNoYXJ0V2lkZ2V0c05hbWVzLFxuICAgIGVkaXRpb25MZXZlbCxcbiAgICBmcm9hbGFFZGl0b3JDb25maWcsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkLFxuICAgIHBhZ2UsXG4gICAgcGFnZURlc2lnbkRpc2FibGVkLFxuICAgIHNob3dHdWlkZXMsXG4gICAgdGFibGVXaWRnZXRzTmFtZXMsXG4gICAgdGVtcGxhdGVUeXBlLFxufSkgPT4ge1xuICAgIGlmIChwYWdlLnR5cGUgPT09ICdncm91cCcpIHtcbiAgICAgICAgcmV0dXJuIHBhZ2UucGFnZXMubWFwKChncm91cGVkUGFnZSkgPT4gKFxuICAgICAgICAgICAgPFBhZ2VEaXNwbGF5XG4gICAgICAgICAgICAgICAgYXZhaWxhYmxlQ2F0YWxvZ3M9e2F2YWlsYWJsZUNhdGFsb2dzfVxuICAgICAgICAgICAgICAgIGNoYXJ0V2lkZ2V0c05hbWVzPXtjaGFydFdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cbiAgICAgICAgICAgICAgICBmcm9hbGFFZGl0b3JDb25maWc9e2Zyb2FsYUVkaXRvckNvbmZpZ31cbiAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQ9e2luZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZH1cbiAgICAgICAgICAgICAgICBrZXk9e2BwcmV2aWV3LXBhZ2UtJHtwYWdlLmlkfS8ke2dyb3VwZWRQYWdlLmlkfWB9XG4gICAgICAgICAgICAgICAgcGFnZT17Z3JvdXBlZFBhZ2V9XG4gICAgICAgICAgICAgICAgcGFnZURlc2lnbkRpc2FibGVkPXtwYWdlRGVzaWduRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgc2hvd0d1aWRlcz17c2hvd0d1aWRlc31cbiAgICAgICAgICAgICAgICB0YWJsZVdpZGdldHNOYW1lcz17dGFibGVXaWRnZXRzTmFtZXN9XG4gICAgICAgICAgICAgICAgdGVtcGxhdGVUeXBlPXt0ZW1wbGF0ZVR5cGV9XG4gICAgICAgICAgICAvPlxuICAgICAgICApKTtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgICAgPFBhZ2VEaXNwbGF5XG4gICAgICAgICAgICBhdmFpbGFibGVDYXRhbG9ncz17YXZhaWxhYmxlQ2F0YWxvZ3N9XG4gICAgICAgICAgICBjaGFydFdpZGdldHNOYW1lcz17Y2hhcnRXaWRnZXRzTmFtZXN9XG4gICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cbiAgICAgICAgICAgIGZyb2FsYUVkaXRvckNvbmZpZz17ZnJvYWxhRWRpdG9yQ29uZmlnfVxuICAgICAgICAgICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkPXtpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWR9XG4gICAgICAgICAgICBrZXk9e2BwcmV2aWV3LXBhZ2UtJHtwYWdlLmlkfWB9XG4gICAgICAgICAgICBwYWdlPXtwYWdlfVxuICAgICAgICAgICAgcGFnZURlc2lnbkRpc2FibGVkPXtwYWdlRGVzaWduRGlzYWJsZWR9XG4gICAgICAgICAgICBzaG93R3VpZGVzPXtzaG93R3VpZGVzfVxuICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgdGVtcGxhdGVUeXBlPXt0ZW1wbGF0ZVR5cGV9XG4gICAgICAgIC8+XG4gICAgKTtcbn07XG5cblBhZ2VMaXN0SXRlbS5wcm9wVHlwZXMgPSB7XG4gICAgYXZhaWxhYmxlQ2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBmcm9hbGFFZGl0b3JDb25maWc6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBwYWdlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2hvd0d1aWRlczogUHJvcFR5cGVzLmJvb2wsXG4gICAgdGFibGVXaWRnZXRzTmFtZXM6IFByb3BUeXBlcy5hcnJheSxcbiAgICB0ZW1wbGF0ZVR5cGU6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQYWdlTGlzdEl0ZW07XG4iXX0=