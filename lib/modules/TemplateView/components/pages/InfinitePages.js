"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _types = require("../../../../constants/types");

var _helpers = require("../../helpers");

var _InfinitePage = _interopRequireDefault(require("./InfinitePage"));

var _PagePreview = _interopRequireDefault(require("./PagePreview"));

var _PageView = _interopRequireDefault(require("./PageView"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toArray(arr) { return _arrayWithHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableRest(); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PageContent = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    position: relative;\n"])));

var Wrapper = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    position: relative;\n"])));

var PageHeap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: ", ";\n    right: ", ";\n    background: #fff;\n    border: 1px solid #ccc;\n    width: ", "px;\n    height: ", "px;\n    min-width: ", "px;\n    min-height: ", "px;\n"])), function (_ref) {
  var _ref$top = _ref.top,
      top = _ref$top === void 0 ? 0 : _ref$top;
  return "".concat(top, "px");
}, function (_ref2) {
  var _ref2$right = _ref2.right,
      right = _ref2$right === void 0 ? 0 : _ref2$right;
  return "-".concat(right, "px");
}, function (_ref3) {
  var _ref3$orientation = _ref3.orientation,
      orientation = _ref3$orientation === void 0 ? 'portrait' : _ref3$orientation;
  return orientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref4) {
  var _ref4$orientation = _ref4.orientation,
      orientation = _ref4$orientation === void 0 ? 'portrait' : _ref4$orientation;
  return orientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
}, function (_ref5) {
  var _ref5$orientation = _ref5.orientation,
      orientation = _ref5$orientation === void 0 ? 'portrait' : _ref5$orientation;
  return orientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
}, function (_ref6) {
  var _ref6$orientation = _ref6.orientation,
      orientation = _ref6$orientation === void 0 ? 'portrait' : _ref6$orientation;
  return orientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;
});

var InfinitePages = function InfinitePages(_ref7) {
  var availableCatalogs = _ref7.availableCatalogs,
      chartWidgetsNames = _ref7.chartWidgetsNames,
      editionLevel = _ref7.editionLevel,
      froalaEditorConfig = _ref7.froalaEditorConfig,
      page = _ref7.page,
      pageDesignDisabled = _ref7.pageDesignDisabled,
      tableWidgetsNames = _ref7.tableWidgetsNames,
      templateType = _ref7.templateType;
  var pageRef = (0, _react.useRef)(null);

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      extraPages = _useState2[0],
      setExtraPages = _useState2[1];

  (0, _react.useEffect)(function () {
    setTimeout(function () {
      if (!(0, _isNil.default)(pageRef) && !(0, _isNil.default)(pageRef.current)) {
        var containerContentDiv = pageRef.current.getElementsByClassName('widget-table-data-container')[0];
        var bodyContentDiv = pageRef.current.getElementsByClassName('widget-table-data-body')[0];
        var footerContentDiv = pageRef.current.getElementsByClassName('widget-table-data-footer')[0];
        var rowsContentDiv = pageRef.current.getElementsByClassName('widget-table-data-row');
        var containerResizeDiv = pageRef.current.getElementsByClassName('widget-table-container-resize')[0];

        if (containerResizeDiv && containerContentDiv && bodyContentDiv && footerContentDiv) {
          containerContentDiv.classList.remove('puppeteer-scrolling');
          var tempExtraPages = (0, _helpers.calculateInfinitePages)({
            pageId: page.id,
            rowDivs: [].concat(_toConsumableArray(rowsContentDiv), [footerContentDiv]),
            tableContentHeight: containerContentDiv.offsetHeight
          });

          if (tempExtraPages.length > 0) {
            var _tempExtraPages = _toArray(tempExtraPages),
                firstElement = _tempExtraPages[0],
                restExtraPagesData = _tempExtraPages.slice(1);

            var diffHeight = containerContentDiv.offsetHeight - (firstElement.nextPosition - firstElement.tablePosition);
            containerResizeDiv.style.maxHeight = "".concat(containerResizeDiv.offsetHeight - diffHeight, "px");

            if (restExtraPagesData.length > 0) {
              setExtraPages(restExtraPagesData);
            }
          }
        }
      }
    }, 900);
  }, []);

  if (editionLevel === _types.NO_EDITION_MODE) {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
        ref: pageRef,
        className: "page puppeteer-template-page",
        "data-orientation": page.orientation || 'portrait',
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_PagePreview.default, {
          chartWidgetsNames: chartWidgetsNames,
          froalaEditorConfig: froalaEditorConfig,
          infinitePagesSupportEnabled: true,
          page: page,
          pageDesignDisabled: pageDesignDisabled,
          tableWidgetsNames: tableWidgetsNames
        }, "page-list-item-".concat(page.id))
      }), extraPages.map(function (extraPage) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_InfinitePage.default, {
          chartWidgets: chartWidgetsNames,
          froalaEditorConfig: froalaEditorConfig,
          nextPosition: extraPage.nextPosition,
          page: page,
          pageDesignDisabled: pageDesignDisabled,
          tablePosition: extraPage.tablePosition,
          tableWidgetsNames: tableWidgetsNames
        }, "preview-page-".concat(page.id, "-extra-").concat(extraPage.id));
      })]
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_jsxRuntime.Fragment, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(Wrapper, {
      className: "page",
      "data-orientation": page.orientation || 'portrait',
      children: [extraPages.length > 0 && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(PageHeap, {
          top: "35",
          right: "12",
          orientation: page.orientation
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(PageHeap, {
          top: "30",
          right: "7",
          orientation: page.orientation
        })]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(PageContent, {
        ref: pageRef,
        editionLevel: editionLevel,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_PageView.default, {
          availableCatalogs: availableCatalogs,
          chartWidgetsNames: chartWidgetsNames,
          froalaEditorConfig: froalaEditorConfig,
          editionLevel: page.blocked === 1 ? _types.NO_EDITION_MODE : editionLevel,
          infinitePagesSupportEnabled: true,
          page: page,
          pageDesignDisabled: pageDesignDisabled,
          tableWidgetsNames: tableWidgetsNames,
          templateType: templateType
        })
      })]
    })
  });
};

InfinitePages.propTypes = {
  availableCatalogs: _propTypes.default.array,
  chartWidgetsNames: _propTypes.default.array,
  editionLevel: _propTypes.default.string,
  froalaEditorConfig: _propTypes.default.object,
  page: _propTypes.default.object,
  pageDesignDisabled: _propTypes.default.bool,
  tableWidgetsNames: _propTypes.default.array,
  templateType: _propTypes.default.number
};
var _default = InfinitePages;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3BhZ2VzL0luZmluaXRlUGFnZXMuanMiXSwibmFtZXMiOlsiUGFnZUNvbnRlbnQiLCJzdHlsZWQiLCJkaXYiLCJXcmFwcGVyIiwiUGFnZUhlYXAiLCJ0b3AiLCJyaWdodCIsIm9yaWVudGF0aW9uIiwiUEFHRV9XSURUSCIsIlBBR0VfSEVJR0hUIiwiSW5maW5pdGVQYWdlcyIsImF2YWlsYWJsZUNhdGFsb2dzIiwiY2hhcnRXaWRnZXRzTmFtZXMiLCJlZGl0aW9uTGV2ZWwiLCJmcm9hbGFFZGl0b3JDb25maWciLCJwYWdlIiwicGFnZURlc2lnbkRpc2FibGVkIiwidGFibGVXaWRnZXRzTmFtZXMiLCJ0ZW1wbGF0ZVR5cGUiLCJwYWdlUmVmIiwiZXh0cmFQYWdlcyIsInNldEV4dHJhUGFnZXMiLCJzZXRUaW1lb3V0IiwiY3VycmVudCIsImNvbnRhaW5lckNvbnRlbnREaXYiLCJnZXRFbGVtZW50c0J5Q2xhc3NOYW1lIiwiYm9keUNvbnRlbnREaXYiLCJmb290ZXJDb250ZW50RGl2Iiwicm93c0NvbnRlbnREaXYiLCJjb250YWluZXJSZXNpemVEaXYiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJ0ZW1wRXh0cmFQYWdlcyIsInBhZ2VJZCIsImlkIiwicm93RGl2cyIsInRhYmxlQ29udGVudEhlaWdodCIsIm9mZnNldEhlaWdodCIsImxlbmd0aCIsImZpcnN0RWxlbWVudCIsInJlc3RFeHRyYVBhZ2VzRGF0YSIsImRpZmZIZWlnaHQiLCJuZXh0UG9zaXRpb24iLCJ0YWJsZVBvc2l0aW9uIiwic3R5bGUiLCJtYXhIZWlnaHQiLCJOT19FRElUSU9OX01PREUiLCJtYXAiLCJleHRyYVBhZ2UiLCJibG9ja2VkIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYXJyYXkiLCJzdHJpbmciLCJvYmplY3QiLCJib29sIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsV0FBVyxHQUFHQywwQkFBT0MsR0FBVixnR0FBakI7O0FBSUEsSUFBTUMsT0FBTyxHQUFHRiwwQkFBT0MsR0FBVixrR0FBYjs7QUFJQSxJQUFNRSxRQUFRLEdBQUdILDBCQUFPQyxHQUFWLG1SQUVIO0FBQUEsc0JBQUdHLEdBQUg7QUFBQSxNQUFHQSxHQUFILHlCQUFTLENBQVQ7QUFBQSxtQkFBb0JBLEdBQXBCO0FBQUEsQ0FGRyxFQUdEO0FBQUEsMEJBQUdDLEtBQUg7QUFBQSxNQUFHQSxLQUFILDRCQUFXLENBQVg7QUFBQSxvQkFBdUJBLEtBQXZCO0FBQUEsQ0FIQyxFQU1EO0FBQUEsZ0NBQUdDLFdBQUg7QUFBQSxNQUFHQSxXQUFILGtDQUFpQixVQUFqQjtBQUFBLFNBQ0xBLFdBQVcsS0FBSyxVQUFoQixHQUE2QkMsb0JBQTdCLEdBQTBDQyxxQkFEckM7QUFBQSxDQU5DLEVBUUE7QUFBQSxnQ0FBR0YsV0FBSDtBQUFBLE1BQUdBLFdBQUgsa0NBQWlCLFVBQWpCO0FBQUEsU0FDTkEsV0FBVyxLQUFLLFVBQWhCLEdBQTZCRSxxQkFBN0IsR0FBMkNELG9CQURyQztBQUFBLENBUkEsRUFVRztBQUFBLGdDQUFHRCxXQUFIO0FBQUEsTUFBR0EsV0FBSCxrQ0FBaUIsVUFBakI7QUFBQSxTQUNUQSxXQUFXLEtBQUssVUFBaEIsR0FBNkJDLG9CQUE3QixHQUEwQ0MscUJBRGpDO0FBQUEsQ0FWSCxFQVlJO0FBQUEsZ0NBQUdGLFdBQUg7QUFBQSxNQUFHQSxXQUFILGtDQUFpQixVQUFqQjtBQUFBLFNBQ1ZBLFdBQVcsS0FBSyxVQUFoQixHQUE2QkUscUJBQTdCLEdBQTJDRCxvQkFEakM7QUFBQSxDQVpKLENBQWQ7O0FBZ0JBLElBQU1FLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsUUFTaEI7QUFBQSxNQVJGQyxpQkFRRSxTQVJGQSxpQkFRRTtBQUFBLE1BUEZDLGlCQU9FLFNBUEZBLGlCQU9FO0FBQUEsTUFORkMsWUFNRSxTQU5GQSxZQU1FO0FBQUEsTUFMRkMsa0JBS0UsU0FMRkEsa0JBS0U7QUFBQSxNQUpGQyxJQUlFLFNBSkZBLElBSUU7QUFBQSxNQUhGQyxrQkFHRSxTQUhGQSxrQkFHRTtBQUFBLE1BRkZDLGlCQUVFLFNBRkZBLGlCQUVFO0FBQUEsTUFERkMsWUFDRSxTQURGQSxZQUNFO0FBQ0YsTUFBTUMsT0FBTyxHQUFHLG1CQUFPLElBQVAsQ0FBaEI7O0FBQ0Esa0JBQW9DLHFCQUFTLEVBQVQsQ0FBcEM7QUFBQTtBQUFBLE1BQU9DLFVBQVA7QUFBQSxNQUFtQkMsYUFBbkI7O0FBQ0Esd0JBQVUsWUFBTTtBQUNaQyxJQUFBQSxVQUFVLENBQUMsWUFBTTtBQUNiLFVBQUksQ0FBQyxvQkFBTUgsT0FBTixDQUFELElBQW1CLENBQUMsb0JBQU1BLE9BQU8sQ0FBQ0ksT0FBZCxDQUF4QixFQUFnRDtBQUM1QyxZQUFNQyxtQkFBbUIsR0FDckJMLE9BQU8sQ0FBQ0ksT0FBUixDQUFnQkUsc0JBQWhCLENBQ0ksNkJBREosRUFFRSxDQUZGLENBREo7QUFJQSxZQUFNQyxjQUFjLEdBQUdQLE9BQU8sQ0FBQ0ksT0FBUixDQUFnQkUsc0JBQWhCLENBQ25CLHdCQURtQixFQUVyQixDQUZxQixDQUF2QjtBQUdBLFlBQU1FLGdCQUFnQixHQUFHUixPQUFPLENBQUNJLE9BQVIsQ0FBZ0JFLHNCQUFoQixDQUNyQiwwQkFEcUIsRUFFdkIsQ0FGdUIsQ0FBekI7QUFHQSxZQUFNRyxjQUFjLEdBQUdULE9BQU8sQ0FBQ0ksT0FBUixDQUFnQkUsc0JBQWhCLENBQ25CLHVCQURtQixDQUF2QjtBQUdBLFlBQUlJLGtCQUFrQixHQUFHVixPQUFPLENBQUNJLE9BQVIsQ0FBZ0JFLHNCQUFoQixDQUNyQiwrQkFEcUIsRUFFdkIsQ0FGdUIsQ0FBekI7O0FBR0EsWUFDSUksa0JBQWtCLElBQ2xCTCxtQkFEQSxJQUVBRSxjQUZBLElBR0FDLGdCQUpKLEVBS0U7QUFDRUgsVUFBQUEsbUJBQW1CLENBQUNNLFNBQXBCLENBQThCQyxNQUE5QixDQUFxQyxxQkFBckM7QUFDQSxjQUFNQyxjQUFjLEdBQUcscUNBQXVCO0FBQzFDQyxZQUFBQSxNQUFNLEVBQUVsQixJQUFJLENBQUNtQixFQUQ2QjtBQUUxQ0MsWUFBQUEsT0FBTywrQkFBTVAsY0FBTixJQUFzQkQsZ0JBQXRCLEVBRm1DO0FBRzFDUyxZQUFBQSxrQkFBa0IsRUFBRVosbUJBQW1CLENBQUNhO0FBSEUsV0FBdkIsQ0FBdkI7O0FBS0EsY0FBSUwsY0FBYyxDQUFDTSxNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzNCLDJDQUNJTixjQURKO0FBQUEsZ0JBQU9PLFlBQVA7QUFBQSxnQkFBd0JDLGtCQUF4Qjs7QUFFQSxnQkFBTUMsVUFBVSxHQUNaakIsbUJBQW1CLENBQUNhLFlBQXBCLElBQ0NFLFlBQVksQ0FBQ0csWUFBYixHQUNHSCxZQUFZLENBQUNJLGFBRmpCLENBREo7QUFJQWQsWUFBQUEsa0JBQWtCLENBQUNlLEtBQW5CLENBQXlCQyxTQUF6QixhQUNJaEIsa0JBQWtCLENBQUNRLFlBQW5CLEdBQWtDSSxVQUR0Qzs7QUFHQSxnQkFBSUQsa0JBQWtCLENBQUNGLE1BQW5CLEdBQTRCLENBQWhDLEVBQW1DO0FBQy9CakIsY0FBQUEsYUFBYSxDQUFDbUIsa0JBQUQsQ0FBYjtBQUNIO0FBQ0o7QUFDSjtBQUNKO0FBQ0osS0E5Q1MsRUE4Q1AsR0E5Q08sQ0FBVjtBQStDSCxHQWhERCxFQWdERyxFQWhESDs7QUFrREEsTUFBSTNCLFlBQVksS0FBS2lDLHNCQUFyQixFQUFzQztBQUNsQyx3QkFDSTtBQUFBLDhCQUNJLHFCQUFDLE9BQUQ7QUFDSSxRQUFBLEdBQUcsRUFBRTNCLE9BRFQ7QUFFSSxRQUFBLFNBQVMsRUFBQyw4QkFGZDtBQUdJLDRCQUFrQkosSUFBSSxDQUFDUixXQUFMLElBQW9CLFVBSDFDO0FBQUEsK0JBS0kscUJBQUMsb0JBQUQ7QUFFSSxVQUFBLGlCQUFpQixFQUFFSyxpQkFGdkI7QUFHSSxVQUFBLGtCQUFrQixFQUFFRSxrQkFIeEI7QUFJSSxVQUFBLDJCQUEyQixNQUovQjtBQUtJLFVBQUEsSUFBSSxFQUFFQyxJQUxWO0FBTUksVUFBQSxrQkFBa0IsRUFBRUMsa0JBTnhCO0FBT0ksVUFBQSxpQkFBaUIsRUFBRUM7QUFQdkIsb0NBQzJCRixJQUFJLENBQUNtQixFQURoQztBQUxKLFFBREosRUFnQktkLFVBQVUsQ0FBQzJCLEdBQVgsQ0FBZSxVQUFDQyxTQUFEO0FBQUEsNEJBQ1oscUJBQUMscUJBQUQ7QUFDSSxVQUFBLFlBQVksRUFBRXBDLGlCQURsQjtBQUVJLFVBQUEsa0JBQWtCLEVBQUVFLGtCQUZ4QjtBQUlJLFVBQUEsWUFBWSxFQUFFa0MsU0FBUyxDQUFDTixZQUo1QjtBQUtJLFVBQUEsSUFBSSxFQUFFM0IsSUFMVjtBQU1JLFVBQUEsa0JBQWtCLEVBQUVDLGtCQU54QjtBQU9JLFVBQUEsYUFBYSxFQUFFZ0MsU0FBUyxDQUFDTCxhQVA3QjtBQVFJLFVBQUEsaUJBQWlCLEVBQUUxQjtBQVJ2QixrQ0FHeUJGLElBQUksQ0FBQ21CLEVBSDlCLG9CQUcwQ2MsU0FBUyxDQUFDZCxFQUhwRCxFQURZO0FBQUEsT0FBZixDQWhCTDtBQUFBLE1BREo7QUErQkg7O0FBRUQsc0JBQ0k7QUFBQSwyQkFDSSxzQkFBQyxPQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsTUFEZDtBQUVJLDBCQUFrQm5CLElBQUksQ0FBQ1IsV0FBTCxJQUFvQixVQUYxQztBQUFBLGlCQUlLYSxVQUFVLENBQUNrQixNQUFYLEdBQW9CLENBQXBCLGlCQUNHO0FBQUEsZ0NBQ0kscUJBQUMsUUFBRDtBQUNJLFVBQUEsR0FBRyxFQUFDLElBRFI7QUFFSSxVQUFBLEtBQUssRUFBQyxJQUZWO0FBR0ksVUFBQSxXQUFXLEVBQUV2QixJQUFJLENBQUNSO0FBSHRCLFVBREosZUFNSSxxQkFBQyxRQUFEO0FBQ0ksVUFBQSxHQUFHLEVBQUMsSUFEUjtBQUVJLFVBQUEsS0FBSyxFQUFDLEdBRlY7QUFHSSxVQUFBLFdBQVcsRUFBRVEsSUFBSSxDQUFDUjtBQUh0QixVQU5KO0FBQUEsUUFMUixlQWtCSSxxQkFBQyxXQUFEO0FBQWEsUUFBQSxHQUFHLEVBQUVZLE9BQWxCO0FBQTJCLFFBQUEsWUFBWSxFQUFFTixZQUF6QztBQUFBLCtCQUNJLHFCQUFDLGlCQUFEO0FBQ0ksVUFBQSxpQkFBaUIsRUFBRUYsaUJBRHZCO0FBRUksVUFBQSxpQkFBaUIsRUFBRUMsaUJBRnZCO0FBR0ksVUFBQSxrQkFBa0IsRUFBRUUsa0JBSHhCO0FBSUksVUFBQSxZQUFZLEVBQ1JDLElBQUksQ0FBQ2tDLE9BQUwsS0FBaUIsQ0FBakIsR0FBcUJILHNCQUFyQixHQUF1Q2pDLFlBTC9DO0FBT0ksVUFBQSwyQkFBMkIsTUFQL0I7QUFRSSxVQUFBLElBQUksRUFBRUUsSUFSVjtBQVNJLFVBQUEsa0JBQWtCLEVBQUVDLGtCQVR4QjtBQVVJLFVBQUEsaUJBQWlCLEVBQUVDLGlCQVZ2QjtBQVdJLFVBQUEsWUFBWSxFQUFFQztBQVhsQjtBQURKLFFBbEJKO0FBQUE7QUFESixJQURKO0FBc0NILENBdElEOztBQXdJQVIsYUFBYSxDQUFDd0MsU0FBZCxHQUEwQjtBQUN0QnZDLEVBQUFBLGlCQUFpQixFQUFFd0MsbUJBQVVDLEtBRFA7QUFFdEJ4QyxFQUFBQSxpQkFBaUIsRUFBRXVDLG1CQUFVQyxLQUZQO0FBR3RCdkMsRUFBQUEsWUFBWSxFQUFFc0MsbUJBQVVFLE1BSEY7QUFJdEJ2QyxFQUFBQSxrQkFBa0IsRUFBRXFDLG1CQUFVRyxNQUpSO0FBS3RCdkMsRUFBQUEsSUFBSSxFQUFFb0MsbUJBQVVHLE1BTE07QUFNdEJ0QyxFQUFBQSxrQkFBa0IsRUFBRW1DLG1CQUFVSSxJQU5SO0FBT3RCdEMsRUFBQUEsaUJBQWlCLEVBQUVrQyxtQkFBVUMsS0FQUDtBQVF0QmxDLEVBQUFBLFlBQVksRUFBRWlDLG1CQUFVSztBQVJGLENBQTFCO2VBV2U5QyxhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VSZWYsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IFBBR0VfSEVJR0hULCBQQUdFX1dJRFRIIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XG5pbXBvcnQgeyBOT19FRElUSU9OX01PREUgfSBmcm9tICdAY29uc3RhbnRzL3R5cGVzJztcblxuaW1wb3J0IHsgY2FsY3VsYXRlSW5maW5pdGVQYWdlcyB9IGZyb20gJy4uLy4uL2hlbHBlcnMnO1xuXG5pbXBvcnQgSW5maW5pdGVQYWdlIGZyb20gJy4vSW5maW5pdGVQYWdlJztcbmltcG9ydCBQYWdlUHJldmlldyBmcm9tICcuL1BhZ2VQcmV2aWV3JztcbmltcG9ydCBQYWdlVmlldyBmcm9tICcuL1BhZ2VWaWV3JztcblxuY29uc3QgUGFnZUNvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbmA7XG5cbmNvbnN0IFdyYXBwZXIgPSBzdHlsZWQuZGl2YFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbmA7XG5cbmNvbnN0IFBhZ2VIZWFwID0gc3R5bGVkLmRpdmBcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAkeyh7IHRvcCA9IDAgfSkgPT4gYCR7dG9wfXB4YH07XG4gICAgcmlnaHQ6ICR7KHsgcmlnaHQgPSAwIH0pID0+IGAtJHtyaWdodH1weGB9O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgICB3aWR0aDogJHsoeyBvcmllbnRhdGlvbiA9ICdwb3J0cmFpdCcgfSkgPT5cbiAgICAgICAgb3JpZW50YXRpb24gPT09ICdwb3J0cmFpdCcgPyBQQUdFX1dJRFRIIDogUEFHRV9IRUlHSFR9cHg7XG4gICAgaGVpZ2h0OiAkeyh7IG9yaWVudGF0aW9uID0gJ3BvcnRyYWl0JyB9KSA9PlxuICAgICAgICBvcmllbnRhdGlvbiA9PT0gJ3BvcnRyYWl0JyA/IFBBR0VfSEVJR0hUIDogUEFHRV9XSURUSH1weDtcbiAgICBtaW4td2lkdGg6ICR7KHsgb3JpZW50YXRpb24gPSAncG9ydHJhaXQnIH0pID0+XG4gICAgICAgIG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9XSURUSCA6IFBBR0VfSEVJR0hUfXB4O1xuICAgIG1pbi1oZWlnaHQ6ICR7KHsgb3JpZW50YXRpb24gPSAncG9ydHJhaXQnIH0pID0+XG4gICAgICAgIG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIfXB4O1xuYDtcblxuY29uc3QgSW5maW5pdGVQYWdlcyA9ICh7XG4gICAgYXZhaWxhYmxlQ2F0YWxvZ3MsXG4gICAgY2hhcnRXaWRnZXRzTmFtZXMsXG4gICAgZWRpdGlvbkxldmVsLFxuICAgIGZyb2FsYUVkaXRvckNvbmZpZyxcbiAgICBwYWdlLFxuICAgIHBhZ2VEZXNpZ25EaXNhYmxlZCxcbiAgICB0YWJsZVdpZGdldHNOYW1lcyxcbiAgICB0ZW1wbGF0ZVR5cGUsXG59KSA9PiB7XG4gICAgY29uc3QgcGFnZVJlZiA9IHVzZVJlZihudWxsKTtcbiAgICBjb25zdCBbZXh0cmFQYWdlcywgc2V0RXh0cmFQYWdlc10gPSB1c2VTdGF0ZShbXSk7XG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICBpZiAoIWlzTmlsKHBhZ2VSZWYpICYmICFpc05pbChwYWdlUmVmLmN1cnJlbnQpKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgY29udGFpbmVyQ29udGVudERpdiA9XG4gICAgICAgICAgICAgICAgICAgIHBhZ2VSZWYuY3VycmVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFxuICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZGdldC10YWJsZS1kYXRhLWNvbnRhaW5lcidcbiAgICAgICAgICAgICAgICAgICAgKVswXTtcbiAgICAgICAgICAgICAgICBjb25zdCBib2R5Q29udGVudERpdiA9IHBhZ2VSZWYuY3VycmVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFxuICAgICAgICAgICAgICAgICAgICAnd2lkZ2V0LXRhYmxlLWRhdGEtYm9keSdcbiAgICAgICAgICAgICAgICApWzBdO1xuICAgICAgICAgICAgICAgIGNvbnN0IGZvb3RlckNvbnRlbnREaXYgPSBwYWdlUmVmLmN1cnJlbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcbiAgICAgICAgICAgICAgICAgICAgJ3dpZGdldC10YWJsZS1kYXRhLWZvb3RlcidcbiAgICAgICAgICAgICAgICApWzBdO1xuICAgICAgICAgICAgICAgIGNvbnN0IHJvd3NDb250ZW50RGl2ID0gcGFnZVJlZi5jdXJyZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXG4gICAgICAgICAgICAgICAgICAgICd3aWRnZXQtdGFibGUtZGF0YS1yb3cnXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBsZXQgY29udGFpbmVyUmVzaXplRGl2ID0gcGFnZVJlZi5jdXJyZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXG4gICAgICAgICAgICAgICAgICAgICd3aWRnZXQtdGFibGUtY29udGFpbmVyLXJlc2l6ZSdcbiAgICAgICAgICAgICAgICApWzBdO1xuICAgICAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyUmVzaXplRGl2ICYmXG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lckNvbnRlbnREaXYgJiZcbiAgICAgICAgICAgICAgICAgICAgYm9keUNvbnRlbnREaXYgJiZcbiAgICAgICAgICAgICAgICAgICAgZm9vdGVyQ29udGVudERpdlxuICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICBjb250YWluZXJDb250ZW50RGl2LmNsYXNzTGlzdC5yZW1vdmUoJ3B1cHBldGVlci1zY3JvbGxpbmcnKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGVtcEV4dHJhUGFnZXMgPSBjYWxjdWxhdGVJbmZpbml0ZVBhZ2VzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2VJZDogcGFnZS5pZCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvd0RpdnM6IFsuLi5yb3dzQ29udGVudERpdiwgZm9vdGVyQ29udGVudERpdl0sXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWJsZUNvbnRlbnRIZWlnaHQ6IGNvbnRhaW5lckNvbnRlbnREaXYub2Zmc2V0SGVpZ2h0LFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRlbXBFeHRyYVBhZ2VzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IFtmaXJzdEVsZW1lbnQsIC4uLnJlc3RFeHRyYVBhZ2VzRGF0YV0gPVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBFeHRyYVBhZ2VzO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGlmZkhlaWdodCA9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyQ29udGVudERpdi5vZmZzZXRIZWlnaHQgLVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChmaXJzdEVsZW1lbnQubmV4dFBvc2l0aW9uIC1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlyc3RFbGVtZW50LnRhYmxlUG9zaXRpb24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyUmVzaXplRGl2LnN0eWxlLm1heEhlaWdodCA9IGAke1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lclJlc2l6ZURpdi5vZmZzZXRIZWlnaHQgLSBkaWZmSGVpZ2h0XG4gICAgICAgICAgICAgICAgICAgICAgICB9cHhgO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3RFeHRyYVBhZ2VzRGF0YS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0RXh0cmFQYWdlcyhyZXN0RXh0cmFQYWdlc0RhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCA5MDApO1xuICAgIH0sIFtdKTtcblxuICAgIGlmIChlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPD5cbiAgICAgICAgICAgICAgICA8V3JhcHBlclxuICAgICAgICAgICAgICAgICAgICByZWY9e3BhZ2VSZWZ9XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInBhZ2UgcHVwcGV0ZWVyLXRlbXBsYXRlLXBhZ2VcIlxuICAgICAgICAgICAgICAgICAgICBkYXRhLW9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9uIHx8ICdwb3J0cmFpdCd9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8UGFnZVByZXZpZXdcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHBhZ2UtbGlzdC1pdGVtLSR7cGFnZS5pZH1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXM9e2NoYXJ0V2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWRcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2U9e3BhZ2V9XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlRGVzaWduRGlzYWJsZWQ9e3BhZ2VEZXNpZ25EaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlV2lkZ2V0c05hbWVzPXt0YWJsZVdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1dyYXBwZXI+XG4gICAgICAgICAgICAgICAge2V4dHJhUGFnZXMubWFwKChleHRyYVBhZ2UpID0+IChcbiAgICAgICAgICAgICAgICAgICAgPEluZmluaXRlUGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzPXtjaGFydFdpZGdldHNOYW1lc31cbiAgICAgICAgICAgICAgICAgICAgICAgIGZyb2FsYUVkaXRvckNvbmZpZz17ZnJvYWxhRWRpdG9yQ29uZmlnfVxuICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgcHJldmlldy1wYWdlLSR7cGFnZS5pZH0tZXh0cmEtJHtleHRyYVBhZ2UuaWR9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG5leHRQb3NpdGlvbj17ZXh0cmFQYWdlLm5leHRQb3NpdGlvbn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2U9e3BhZ2V9XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlRGVzaWduRGlzYWJsZWQ9e3BhZ2VEZXNpZ25EaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYmxlUG9zaXRpb249e2V4dHJhUGFnZS50YWJsZVBvc2l0aW9ufVxuICAgICAgICAgICAgICAgICAgICAgICAgdGFibGVXaWRnZXRzTmFtZXM9e3RhYmxlV2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC8+XG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPD5cbiAgICAgICAgICAgIDxXcmFwcGVyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicGFnZVwiXG4gICAgICAgICAgICAgICAgZGF0YS1vcmllbnRhdGlvbj17cGFnZS5vcmllbnRhdGlvbiB8fCAncG9ydHJhaXQnfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtleHRyYVBhZ2VzLmxlbmd0aCA+IDAgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgICAgICAgPFBhZ2VIZWFwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wPVwiMzVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0PVwiMTJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9ufVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxQYWdlSGVhcFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcD1cIjMwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByaWdodD1cIjdcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uPXtwYWdlLm9yaWVudGF0aW9ufVxuICAgICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC8+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICA8UGFnZUNvbnRlbnQgcmVmPXtwYWdlUmVmfSBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH0+XG4gICAgICAgICAgICAgICAgICAgIDxQYWdlVmlld1xuICAgICAgICAgICAgICAgICAgICAgICAgYXZhaWxhYmxlQ2F0YWxvZ3M9e2F2YWlsYWJsZUNhdGFsb2dzfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnRXaWRnZXRzTmFtZXM9e2NoYXJ0V2lkZ2V0c05hbWVzfVxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvYWxhRWRpdG9yQ29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XG4gICAgICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2UuYmxvY2tlZCA9PT0gMSA/IE5PX0VESVRJT05fTU9ERSA6IGVkaXRpb25MZXZlbFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlPXtwYWdlfVxuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZURlc2lnbkRpc2FibGVkPXtwYWdlRGVzaWduRGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICB0YWJsZVdpZGdldHNOYW1lcz17dGFibGVXaWRnZXRzTmFtZXN9XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVR5cGU9e3RlbXBsYXRlVHlwZX1cbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L1BhZ2VDb250ZW50PlxuICAgICAgICAgICAgPC9XcmFwcGVyPlxuICAgICAgICA8Lz5cbiAgICApO1xufTtcblxuSW5maW5pdGVQYWdlcy5wcm9wVHlwZXMgPSB7XG4gICAgYXZhaWxhYmxlQ2F0YWxvZ3M6IFByb3BUeXBlcy5hcnJheSxcbiAgICBjaGFydFdpZGdldHNOYW1lczogUHJvcFR5cGVzLmFycmF5LFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBmcm9hbGFFZGl0b3JDb25maWc6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgcGFnZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBwYWdlRGVzaWduRGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHRhYmxlV2lkZ2V0c05hbWVzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgdGVtcGxhdGVUeXBlOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgSW5maW5pdGVQYWdlcztcbiJdfQ==