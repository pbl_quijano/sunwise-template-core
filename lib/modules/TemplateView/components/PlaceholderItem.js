"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactDraggable = _interopRequireDefault(require("react-draggable"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../constants/template");

var _helpers = require("../helpers");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n"])));

var Item = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    width: ", "px;\n    height: ", "px;\n    background-color: rgba(255, 154, 0, 0.4);\n    pointer-events: none;\n    z-index: 53;\n"])), function (_ref) {
  var width = _ref.width;
  return width;
}, function (_ref2) {
  var height = _ref2.height;
  return height;
});

var PlaceholderItem = function PlaceholderItem(_ref3) {
  var droppingItem = _ref3.droppingItem,
      onHandleDrop = _ref3.onHandleDrop,
      showGuides = _ref3.showGuides,
      pageOrientation = _ref3.pageOrientation;

  var _useState = (0, _react.useState)({}),
      _useState2 = _slicedToArray(_useState, 2),
      position = _useState2[0],
      setPosition = _useState2[1];

  var width = droppingItem.width,
      height = droppingItem.height;

  var handleDragOver = function handleDragOver(event) {
    event.stopPropagation();
    event.preventDefault();
    var _event$nativeEvent = event.nativeEvent,
        layerX = _event$nativeEvent.layerX,
        layerY = _event$nativeEvent.layerY;
    var newPosition = (0, _helpers.calcPositionWithLimits)(layerX, layerY, width, height, pageOrientation, showGuides);

    if (newPosition.x !== position.x || newPosition.y !== position.y) {
      setPosition({
        x: newPosition.x,
        y: newPosition.y
      });
    }
  };

  var handleDrop = function handleDrop(e) {
    onHandleDrop(e, position.x, position.y);
    setPosition({});
  };

  var renderPlaceholderItem = function renderPlaceholderItem() {
    if ((0, _isEmpty.default)(position)) return null;
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactDraggable.default, {
      position: position,
      bounds: "parent",
      grid: [_template.GRID_WIDTH, _template.GRID_HEIGHT],
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Item, {
        height: height,
        width: width
      })
    });
  };

  var _onDragEnter = function _onDragEnter(event) {
    event.preventDefault();
  };

  var _onDragStart = function _onDragStart(event) {
    event.preventDefault();
  };

  var _onDragLeave = function _onDragLeave(event) {
    event.preventDefault();
    setPosition({});
  };

  if ((0, _isEmpty.default)(droppingItem)) return null;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    className: "droptarget",
    onDragOver: handleDragOver,
    onDrop: handleDrop,
    onDragStart: _onDragStart,
    onDragEnter: _onDragEnter,
    onDragLeave: _onDragLeave,
    children: renderPlaceholderItem()
  });
};

PlaceholderItem.propTypes = {
  droppingItem: _propTypes.default.object,
  onHandleDrop: _propTypes.default.func,
  showGuides: _propTypes.default.bool,
  pageOrientation: _propTypes.default.string
};
var _default = PlaceholderItem;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1BsYWNlaG9sZGVySXRlbS5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJJdGVtIiwid2lkdGgiLCJoZWlnaHQiLCJQbGFjZWhvbGRlckl0ZW0iLCJkcm9wcGluZ0l0ZW0iLCJvbkhhbmRsZURyb3AiLCJzaG93R3VpZGVzIiwicGFnZU9yaWVudGF0aW9uIiwicG9zaXRpb24iLCJzZXRQb3NpdGlvbiIsImhhbmRsZURyYWdPdmVyIiwiZXZlbnQiLCJzdG9wUHJvcGFnYXRpb24iLCJwcmV2ZW50RGVmYXVsdCIsIm5hdGl2ZUV2ZW50IiwibGF5ZXJYIiwibGF5ZXJZIiwibmV3UG9zaXRpb24iLCJ4IiwieSIsImhhbmRsZURyb3AiLCJlIiwicmVuZGVyUGxhY2Vob2xkZXJJdGVtIiwiR1JJRF9XSURUSCIsIkdSSURfSEVJR0hUIiwiX29uRHJhZ0VudGVyIiwiX29uRHJhZ1N0YXJ0IiwiX29uRHJhZ0xlYXZlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0IiwiZnVuYyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsZ0tBQWY7O0FBUUEsSUFBTUMsSUFBSSxHQUFHRiwwQkFBT0MsR0FBViwrTUFDRztBQUFBLE1BQUdFLEtBQUgsUUFBR0EsS0FBSDtBQUFBLFNBQWVBLEtBQWY7QUFBQSxDQURILEVBRUk7QUFBQSxNQUFHQyxNQUFILFNBQUdBLE1BQUg7QUFBQSxTQUFnQkEsTUFBaEI7QUFBQSxDQUZKLENBQVY7O0FBUUEsSUFBTUMsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixRQUtsQjtBQUFBLE1BSkZDLFlBSUUsU0FKRkEsWUFJRTtBQUFBLE1BSEZDLFlBR0UsU0FIRkEsWUFHRTtBQUFBLE1BRkZDLFVBRUUsU0FGRkEsVUFFRTtBQUFBLE1BREZDLGVBQ0UsU0FERkEsZUFDRTs7QUFDRixrQkFBZ0MscUJBQVMsRUFBVCxDQUFoQztBQUFBO0FBQUEsTUFBT0MsUUFBUDtBQUFBLE1BQWlCQyxXQUFqQjs7QUFDQSxNQUFRUixLQUFSLEdBQTBCRyxZQUExQixDQUFRSCxLQUFSO0FBQUEsTUFBZUMsTUFBZixHQUEwQkUsWUFBMUIsQ0FBZUYsTUFBZjs7QUFDQSxNQUFNUSxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLEtBQUQsRUFBVztBQUM5QkEsSUFBQUEsS0FBSyxDQUFDQyxlQUFOO0FBQ0FELElBQUFBLEtBQUssQ0FBQ0UsY0FBTjtBQUNBLDZCQUEyQkYsS0FBSyxDQUFDRyxXQUFqQztBQUFBLFFBQVFDLE1BQVIsc0JBQVFBLE1BQVI7QUFBQSxRQUFnQkMsTUFBaEIsc0JBQWdCQSxNQUFoQjtBQUNBLFFBQU1DLFdBQVcsR0FBRyxxQ0FDaEJGLE1BRGdCLEVBRWhCQyxNQUZnQixFQUdoQmYsS0FIZ0IsRUFJaEJDLE1BSmdCLEVBS2hCSyxlQUxnQixFQU1oQkQsVUFOZ0IsQ0FBcEI7O0FBUUEsUUFBSVcsV0FBVyxDQUFDQyxDQUFaLEtBQWtCVixRQUFRLENBQUNVLENBQTNCLElBQWdDRCxXQUFXLENBQUNFLENBQVosS0FBa0JYLFFBQVEsQ0FBQ1csQ0FBL0QsRUFBa0U7QUFDOURWLE1BQUFBLFdBQVcsQ0FBQztBQUFFUyxRQUFBQSxDQUFDLEVBQUVELFdBQVcsQ0FBQ0MsQ0FBakI7QUFBb0JDLFFBQUFBLENBQUMsRUFBRUYsV0FBVyxDQUFDRTtBQUFuQyxPQUFELENBQVg7QUFDSDtBQUNKLEdBZkQ7O0FBaUJBLE1BQU1DLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLENBQUQsRUFBTztBQUN0QmhCLElBQUFBLFlBQVksQ0FBQ2dCLENBQUQsRUFBSWIsUUFBUSxDQUFDVSxDQUFiLEVBQWdCVixRQUFRLENBQUNXLENBQXpCLENBQVo7QUFDQVYsSUFBQUEsV0FBVyxDQUFDLEVBQUQsQ0FBWDtBQUNILEdBSEQ7O0FBS0EsTUFBTWEscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixHQUFNO0FBQ2hDLFFBQUksc0JBQVFkLFFBQVIsQ0FBSixFQUF1QixPQUFPLElBQVA7QUFDdkIsd0JBQ0kscUJBQUMsdUJBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRUEsUUFEZDtBQUVJLE1BQUEsTUFBTSxFQUFDLFFBRlg7QUFHSSxNQUFBLElBQUksRUFBRSxDQUFDZSxvQkFBRCxFQUFhQyxxQkFBYixDQUhWO0FBQUEsNkJBS0kscUJBQUMsSUFBRDtBQUFNLFFBQUEsTUFBTSxFQUFFdEIsTUFBZDtBQUFzQixRQUFBLEtBQUssRUFBRUQ7QUFBN0I7QUFMSixNQURKO0FBU0gsR0FYRDs7QUFhQSxNQUFNd0IsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ2QsS0FBRCxFQUFXO0FBQzVCQSxJQUFBQSxLQUFLLENBQUNFLGNBQU47QUFDSCxHQUZEOztBQUlBLE1BQU1hLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNmLEtBQUQsRUFBVztBQUM1QkEsSUFBQUEsS0FBSyxDQUFDRSxjQUFOO0FBQ0gsR0FGRDs7QUFJQSxNQUFNYyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDaEIsS0FBRCxFQUFXO0FBQzVCQSxJQUFBQSxLQUFLLENBQUNFLGNBQU47QUFDQUosSUFBQUEsV0FBVyxDQUFDLEVBQUQsQ0FBWDtBQUNILEdBSEQ7O0FBS0EsTUFBSSxzQkFBUUwsWUFBUixDQUFKLEVBQTJCLE9BQU8sSUFBUDtBQUMzQixzQkFDSSxxQkFBQyxTQUFEO0FBQ0ksSUFBQSxTQUFTLEVBQUMsWUFEZDtBQUVJLElBQUEsVUFBVSxFQUFFTSxjQUZoQjtBQUdJLElBQUEsTUFBTSxFQUFFVSxVQUhaO0FBSUksSUFBQSxXQUFXLEVBQUVNLFlBSmpCO0FBS0ksSUFBQSxXQUFXLEVBQUVELFlBTGpCO0FBTUksSUFBQSxXQUFXLEVBQUVFLFlBTmpCO0FBQUEsY0FRS0wscUJBQXFCO0FBUjFCLElBREo7QUFZSCxDQXJFRDs7QUF1RUFuQixlQUFlLENBQUN5QixTQUFoQixHQUE0QjtBQUN4QnhCLEVBQUFBLFlBQVksRUFBRXlCLG1CQUFVQyxNQURBO0FBRXhCekIsRUFBQUEsWUFBWSxFQUFFd0IsbUJBQVVFLElBRkE7QUFHeEJ6QixFQUFBQSxVQUFVLEVBQUV1QixtQkFBVUcsSUFIRTtBQUl4QnpCLEVBQUFBLGVBQWUsRUFBRXNCLG1CQUFVSTtBQUpILENBQTVCO2VBT2U5QixlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IERyYWdnYWJsZSBmcm9tICdyZWFjdC1kcmFnZ2FibGUnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IEdSSURfV0lEVEgsIEdSSURfSEVJR0hUIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XG5cbmltcG9ydCB7IGNhbGNQb3NpdGlvbldpdGhMaW1pdHMgfSBmcm9tICcuLi9oZWxwZXJzJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuYDtcblxuY29uc3QgSXRlbSA9IHN0eWxlZC5kaXZgXG4gICAgd2lkdGg6ICR7KHsgd2lkdGggfSkgPT4gd2lkdGh9cHg7XG4gICAgaGVpZ2h0OiAkeyh7IGhlaWdodCB9KSA9PiBoZWlnaHR9cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDE1NCwgMCwgMC40KTtcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgICB6LWluZGV4OiA1MztcbmA7XG5cbmNvbnN0IFBsYWNlaG9sZGVySXRlbSA9ICh7XG4gICAgZHJvcHBpbmdJdGVtLFxuICAgIG9uSGFuZGxlRHJvcCxcbiAgICBzaG93R3VpZGVzLFxuICAgIHBhZ2VPcmllbnRhdGlvbixcbn0pID0+IHtcbiAgICBjb25zdCBbcG9zaXRpb24sIHNldFBvc2l0aW9uXSA9IHVzZVN0YXRlKHt9KTtcbiAgICBjb25zdCB7IHdpZHRoLCBoZWlnaHQgfSA9IGRyb3BwaW5nSXRlbTtcbiAgICBjb25zdCBoYW5kbGVEcmFnT3ZlciA9IChldmVudCkgPT4ge1xuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgY29uc3QgeyBsYXllclgsIGxheWVyWSB9ID0gZXZlbnQubmF0aXZlRXZlbnQ7XG4gICAgICAgIGNvbnN0IG5ld1Bvc2l0aW9uID0gY2FsY1Bvc2l0aW9uV2l0aExpbWl0cyhcbiAgICAgICAgICAgIGxheWVyWCxcbiAgICAgICAgICAgIGxheWVyWSxcbiAgICAgICAgICAgIHdpZHRoLFxuICAgICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgICAgcGFnZU9yaWVudGF0aW9uLFxuICAgICAgICAgICAgc2hvd0d1aWRlc1xuICAgICAgICApO1xuICAgICAgICBpZiAobmV3UG9zaXRpb24ueCAhPT0gcG9zaXRpb24ueCB8fCBuZXdQb3NpdGlvbi55ICE9PSBwb3NpdGlvbi55KSB7XG4gICAgICAgICAgICBzZXRQb3NpdGlvbih7IHg6IG5ld1Bvc2l0aW9uLngsIHk6IG5ld1Bvc2l0aW9uLnkgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgY29uc3QgaGFuZGxlRHJvcCA9IChlKSA9PiB7XG4gICAgICAgIG9uSGFuZGxlRHJvcChlLCBwb3NpdGlvbi54LCBwb3NpdGlvbi55KTtcbiAgICAgICAgc2V0UG9zaXRpb24oe30pO1xuICAgIH07XG5cbiAgICBjb25zdCByZW5kZXJQbGFjZWhvbGRlckl0ZW0gPSAoKSA9PiB7XG4gICAgICAgIGlmIChpc0VtcHR5KHBvc2l0aW9uKSkgcmV0dXJuIG51bGw7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8RHJhZ2dhYmxlXG4gICAgICAgICAgICAgICAgcG9zaXRpb249e3Bvc2l0aW9ufVxuICAgICAgICAgICAgICAgIGJvdW5kcz1cInBhcmVudFwiXG4gICAgICAgICAgICAgICAgZ3JpZD17W0dSSURfV0lEVEgsIEdSSURfSEVJR0hUXX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8SXRlbSBoZWlnaHQ9e2hlaWdodH0gd2lkdGg9e3dpZHRofSAvPlxuICAgICAgICAgICAgPC9EcmFnZ2FibGU+XG4gICAgICAgICk7XG4gICAgfTtcblxuICAgIGNvbnN0IF9vbkRyYWdFbnRlciA9IChldmVudCkgPT4ge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH07XG5cbiAgICBjb25zdCBfb25EcmFnU3RhcnQgPSAoZXZlbnQpID0+IHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9O1xuXG4gICAgY29uc3QgX29uRHJhZ0xlYXZlID0gKGV2ZW50KSA9PiB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHNldFBvc2l0aW9uKHt9KTtcbiAgICB9O1xuXG4gICAgaWYgKGlzRW1wdHkoZHJvcHBpbmdJdGVtKSkgcmV0dXJuIG51bGw7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiZHJvcHRhcmdldFwiXG4gICAgICAgICAgICBvbkRyYWdPdmVyPXtoYW5kbGVEcmFnT3Zlcn1cbiAgICAgICAgICAgIG9uRHJvcD17aGFuZGxlRHJvcH1cbiAgICAgICAgICAgIG9uRHJhZ1N0YXJ0PXtfb25EcmFnU3RhcnR9XG4gICAgICAgICAgICBvbkRyYWdFbnRlcj17X29uRHJhZ0VudGVyfVxuICAgICAgICAgICAgb25EcmFnTGVhdmU9e19vbkRyYWdMZWF2ZX1cbiAgICAgICAgPlxuICAgICAgICAgICAge3JlbmRlclBsYWNlaG9sZGVySXRlbSgpfVxuICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICApO1xufTtcblxuUGxhY2Vob2xkZXJJdGVtLnByb3BUeXBlcyA9IHtcbiAgICBkcm9wcGluZ0l0ZW06IFByb3BUeXBlcy5vYmplY3QsXG4gICAgb25IYW5kbGVEcm9wOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBzaG93R3VpZGVzOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBwYWdlT3JpZW50YXRpb246IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQbGFjZWhvbGRlckl0ZW07XG4iXX0=