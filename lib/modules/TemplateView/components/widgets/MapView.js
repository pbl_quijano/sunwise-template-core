"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _googleMapsReact = require("google-maps-react");

var _i18next = _interopRequireDefault(require("i18next"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _contexts = require("../../../../helpers/contexts");

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var mapValueUseEffect = function mapValueUseEffect(_ref) {
  var google = _ref.google,
      handleCenterChanged = _ref.handleCenterChanged,
      handleTypeChanged = _ref.handleTypeChanged,
      handleZoomChanged = _ref.handleZoomChanged,
      mapValue = _ref.mapValue,
      markers = _ref.markers;
  return function () {
    if (mapValue) {
      mapValue.setTilt(0);

      if (handleCenterChanged) {
        google.maps.event.addListener(mapValue, 'dragend', function () {
          return handleCenterChanged(mapValue.getCenter());
        });
      }

      if (handleZoomChanged) {
        google.maps.event.addListener(mapValue, 'zoom_changed', function () {
          return handleZoomChanged(mapValue.getZoom());
        });
      }

      if (handleTypeChanged) {
        google.maps.event.addListener(mapValue, 'maptypeid_changed', function () {
          return handleZoomChanged(mapValue.getMapTypeId());
        });
      }

      markers.forEach(function (marker) {
        var markerObject = new google.maps.Marker({
          draggable: false,
          icon: {
            url: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png',
            scaledSize: new google.maps.Size(32, 32)
          },
          name: _i18next.default.t('Location'),
          position: new google.maps.LatLng(marker.lat, marker.lng),
          title: marker.title
        });
        markerObject.setMap(mapValue);
      });
    }

    return function () {
      if (mapValue) {
        google.maps.event.clearListeners(mapValue, 'dragend');
        google.maps.event.clearListeners(mapValue, 'zoom_changed');
        google.maps.event.clearListeners(mapValue, 'maptypeid_changed');
      }
    };
  };
};

var MapView = function MapView(_ref2) {
  var handleCenterChanged = _ref2.handleCenterChanged,
      handleTypeChanged = _ref2.handleTypeChanged,
      handleZoomChanged = _ref2.handleZoomChanged,
      lat = _ref2.lat,
      lng = _ref2.lng,
      mapType = _ref2.mapType,
      markers = _ref2.markers,
      zoom = _ref2.zoom;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      google = _useContext.google;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      mapValue = _useState2[0],
      setMapValue = _useState2[1];

  (0, _react.useEffect)(mapValueUseEffect({
    google: google,
    handleCenterChanged: handleCenterChanged,
    handleTypeChanged: handleTypeChanged,
    handleZoomChanged: handleZoomChanged,
    mapValue: mapValue,
    markers: markers
  }), [mapValue]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_googleMapsReact.Map, {
    fullscreenControl: false,
    google: google,
    initialCenter: {
      lat: lat,
      lng: lng
    },
    mapType: mapType,
    mapTypeControl: true,
    onReady: function onReady(_, map) {
      return setMapValue(map);
    },
    rotateControl: false,
    streetViewControl: false,
    zoom: zoom,
    zoomControl: true
  });
};

MapView.propTypes = {
  handleCenterChanged: _propTypes.default.func,
  handleTypeChanged: _propTypes.default.func,
  handleZoomChanged: _propTypes.default.func,
  lat: _propTypes.default.string,
  lng: _propTypes.default.string,
  mapType: _propTypes.default.string,
  markers: _propTypes.default.array,
  zoom: _propTypes.default.number
};
var _default = MapView;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvTWFwVmlldy5qcyJdLCJuYW1lcyI6WyJtYXBWYWx1ZVVzZUVmZmVjdCIsImdvb2dsZSIsImhhbmRsZUNlbnRlckNoYW5nZWQiLCJoYW5kbGVUeXBlQ2hhbmdlZCIsImhhbmRsZVpvb21DaGFuZ2VkIiwibWFwVmFsdWUiLCJtYXJrZXJzIiwic2V0VGlsdCIsIm1hcHMiLCJldmVudCIsImFkZExpc3RlbmVyIiwiZ2V0Q2VudGVyIiwiZ2V0Wm9vbSIsImdldE1hcFR5cGVJZCIsImZvckVhY2giLCJtYXJrZXIiLCJtYXJrZXJPYmplY3QiLCJNYXJrZXIiLCJkcmFnZ2FibGUiLCJpY29uIiwidXJsIiwic2NhbGVkU2l6ZSIsIlNpemUiLCJuYW1lIiwiaTE4bmV4dCIsInQiLCJwb3NpdGlvbiIsIkxhdExuZyIsImxhdCIsImxuZyIsInRpdGxlIiwic2V0TWFwIiwiY2xlYXJMaXN0ZW5lcnMiLCJNYXBWaWV3IiwibWFwVHlwZSIsInpvb20iLCJHZW5lcmFsQ29udGV4dCIsInNldE1hcFZhbHVlIiwiXyIsIm1hcCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImZ1bmMiLCJzdHJpbmciLCJhcnJheSIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxpQkFBaUIsR0FDbkIsU0FERUEsaUJBQ0Y7QUFBQSxNQUNJQyxNQURKLFFBQ0lBLE1BREo7QUFBQSxNQUVJQyxtQkFGSixRQUVJQSxtQkFGSjtBQUFBLE1BR0lDLGlCQUhKLFFBR0lBLGlCQUhKO0FBQUEsTUFJSUMsaUJBSkosUUFJSUEsaUJBSko7QUFBQSxNQUtJQyxRQUxKLFFBS0lBLFFBTEo7QUFBQSxNQU1JQyxPQU5KLFFBTUlBLE9BTko7QUFBQSxTQVFBLFlBQU07QUFDRixRQUFJRCxRQUFKLEVBQWM7QUFDVkEsTUFBQUEsUUFBUSxDQUFDRSxPQUFULENBQWlCLENBQWpCOztBQUNBLFVBQUlMLG1CQUFKLEVBQXlCO0FBQ3JCRCxRQUFBQSxNQUFNLENBQUNPLElBQVAsQ0FBWUMsS0FBWixDQUFrQkMsV0FBbEIsQ0FBOEJMLFFBQTlCLEVBQXdDLFNBQXhDLEVBQW1EO0FBQUEsaUJBQy9DSCxtQkFBbUIsQ0FBQ0csUUFBUSxDQUFDTSxTQUFULEVBQUQsQ0FENEI7QUFBQSxTQUFuRDtBQUdIOztBQUNELFVBQUlQLGlCQUFKLEVBQXVCO0FBQ25CSCxRQUFBQSxNQUFNLENBQUNPLElBQVAsQ0FBWUMsS0FBWixDQUFrQkMsV0FBbEIsQ0FBOEJMLFFBQTlCLEVBQXdDLGNBQXhDLEVBQXdEO0FBQUEsaUJBQ3BERCxpQkFBaUIsQ0FBQ0MsUUFBUSxDQUFDTyxPQUFULEVBQUQsQ0FEbUM7QUFBQSxTQUF4RDtBQUdIOztBQUNELFVBQUlULGlCQUFKLEVBQXVCO0FBQ25CRixRQUFBQSxNQUFNLENBQUNPLElBQVAsQ0FBWUMsS0FBWixDQUFrQkMsV0FBbEIsQ0FDSUwsUUFESixFQUVJLG1CQUZKLEVBR0k7QUFBQSxpQkFBTUQsaUJBQWlCLENBQUNDLFFBQVEsQ0FBQ1EsWUFBVCxFQUFELENBQXZCO0FBQUEsU0FISjtBQUtIOztBQUVEUCxNQUFBQSxPQUFPLENBQUNRLE9BQVIsQ0FBZ0IsVUFBQ0MsTUFBRCxFQUFZO0FBQ3hCLFlBQU1DLFlBQVksR0FBRyxJQUFJZixNQUFNLENBQUNPLElBQVAsQ0FBWVMsTUFBaEIsQ0FBdUI7QUFDeENDLFVBQUFBLFNBQVMsRUFBRSxLQUQ2QjtBQUV4Q0MsVUFBQUEsSUFBSSxFQUFFO0FBQ0ZDLFlBQUFBLEdBQUcsRUFBRSxrRkFESDtBQUVGQyxZQUFBQSxVQUFVLEVBQUUsSUFBSXBCLE1BQU0sQ0FBQ08sSUFBUCxDQUFZYyxJQUFoQixDQUFxQixFQUFyQixFQUF5QixFQUF6QjtBQUZWLFdBRmtDO0FBTXhDQyxVQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsVUFBVixDQU5rQztBQU94Q0MsVUFBQUEsUUFBUSxFQUFFLElBQUl6QixNQUFNLENBQUNPLElBQVAsQ0FBWW1CLE1BQWhCLENBQXVCWixNQUFNLENBQUNhLEdBQTlCLEVBQW1DYixNQUFNLENBQUNjLEdBQTFDLENBUDhCO0FBUXhDQyxVQUFBQSxLQUFLLEVBQUVmLE1BQU0sQ0FBQ2U7QUFSMEIsU0FBdkIsQ0FBckI7QUFVQWQsUUFBQUEsWUFBWSxDQUFDZSxNQUFiLENBQW9CMUIsUUFBcEI7QUFDSCxPQVpEO0FBYUg7O0FBRUQsV0FBTyxZQUFNO0FBQ1QsVUFBSUEsUUFBSixFQUFjO0FBQ1ZKLFFBQUFBLE1BQU0sQ0FBQ08sSUFBUCxDQUFZQyxLQUFaLENBQWtCdUIsY0FBbEIsQ0FBaUMzQixRQUFqQyxFQUEyQyxTQUEzQztBQUNBSixRQUFBQSxNQUFNLENBQUNPLElBQVAsQ0FBWUMsS0FBWixDQUFrQnVCLGNBQWxCLENBQWlDM0IsUUFBakMsRUFBMkMsY0FBM0M7QUFDQUosUUFBQUEsTUFBTSxDQUFDTyxJQUFQLENBQVlDLEtBQVosQ0FBa0J1QixjQUFsQixDQUFpQzNCLFFBQWpDLEVBQTJDLG1CQUEzQztBQUNIO0FBQ0osS0FORDtBQU9ILEdBbkREO0FBQUEsQ0FESjs7QUFzREEsSUFBTTRCLE9BQU8sR0FBRyxTQUFWQSxPQUFVLFFBU1Y7QUFBQSxNQVJGL0IsbUJBUUUsU0FSRkEsbUJBUUU7QUFBQSxNQVBGQyxpQkFPRSxTQVBGQSxpQkFPRTtBQUFBLE1BTkZDLGlCQU1FLFNBTkZBLGlCQU1FO0FBQUEsTUFMRndCLEdBS0UsU0FMRkEsR0FLRTtBQUFBLE1BSkZDLEdBSUUsU0FKRkEsR0FJRTtBQUFBLE1BSEZLLE9BR0UsU0FIRkEsT0FHRTtBQUFBLE1BRkY1QixPQUVFLFNBRkZBLE9BRUU7QUFBQSxNQURGNkIsSUFDRSxTQURGQSxJQUNFOztBQUNGLG9CQUFtQix1QkFBV0Msd0JBQVgsQ0FBbkI7QUFBQSxNQUFRbkMsTUFBUixlQUFRQSxNQUFSOztBQUNBLGtCQUFnQyxxQkFBUyxJQUFULENBQWhDO0FBQUE7QUFBQSxNQUFPSSxRQUFQO0FBQUEsTUFBaUJnQyxXQUFqQjs7QUFDQSx3QkFDSXJDLGlCQUFpQixDQUFDO0FBQ2RDLElBQUFBLE1BQU0sRUFBTkEsTUFEYztBQUVkQyxJQUFBQSxtQkFBbUIsRUFBbkJBLG1CQUZjO0FBR2RDLElBQUFBLGlCQUFpQixFQUFqQkEsaUJBSGM7QUFJZEMsSUFBQUEsaUJBQWlCLEVBQWpCQSxpQkFKYztBQUtkQyxJQUFBQSxRQUFRLEVBQVJBLFFBTGM7QUFNZEMsSUFBQUEsT0FBTyxFQUFQQTtBQU5jLEdBQUQsQ0FEckIsRUFTSSxDQUFDRCxRQUFELENBVEo7QUFZQSxzQkFDSSxxQkFBQyxvQkFBRDtBQUNJLElBQUEsaUJBQWlCLEVBQUUsS0FEdkI7QUFFSSxJQUFBLE1BQU0sRUFBRUosTUFGWjtBQUdJLElBQUEsYUFBYSxFQUFFO0FBQUUyQixNQUFBQSxHQUFHLEVBQUhBLEdBQUY7QUFBT0MsTUFBQUEsR0FBRyxFQUFIQTtBQUFQLEtBSG5CO0FBSUksSUFBQSxPQUFPLEVBQUVLLE9BSmI7QUFLSSxJQUFBLGNBQWMsRUFBRSxJQUxwQjtBQU1JLElBQUEsT0FBTyxFQUFFLGlCQUFDSSxDQUFELEVBQUlDLEdBQUo7QUFBQSxhQUFZRixXQUFXLENBQUNFLEdBQUQsQ0FBdkI7QUFBQSxLQU5iO0FBT0ksSUFBQSxhQUFhLEVBQUUsS0FQbkI7QUFRSSxJQUFBLGlCQUFpQixFQUFFLEtBUnZCO0FBU0ksSUFBQSxJQUFJLEVBQUVKLElBVFY7QUFVSSxJQUFBLFdBQVcsRUFBRTtBQVZqQixJQURKO0FBY0gsQ0F0Q0Q7O0FBd0NBRixPQUFPLENBQUNPLFNBQVIsR0FBb0I7QUFDaEJ0QyxFQUFBQSxtQkFBbUIsRUFBRXVDLG1CQUFVQyxJQURmO0FBRWhCdkMsRUFBQUEsaUJBQWlCLEVBQUVzQyxtQkFBVUMsSUFGYjtBQUdoQnRDLEVBQUFBLGlCQUFpQixFQUFFcUMsbUJBQVVDLElBSGI7QUFJaEJkLEVBQUFBLEdBQUcsRUFBRWEsbUJBQVVFLE1BSkM7QUFLaEJkLEVBQUFBLEdBQUcsRUFBRVksbUJBQVVFLE1BTEM7QUFNaEJULEVBQUFBLE9BQU8sRUFBRU8sbUJBQVVFLE1BTkg7QUFPaEJyQyxFQUFBQSxPQUFPLEVBQUVtQyxtQkFBVUcsS0FQSDtBQVFoQlQsRUFBQUEsSUFBSSxFQUFFTSxtQkFBVUk7QUFSQSxDQUFwQjtlQVdlWixPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWFwIH0gZnJvbSAnZ29vZ2xlLW1hcHMtcmVhY3QnO1xuaW1wb3J0IGkxOG5leHQgZnJvbSAnaTE4bmV4dCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ29udGV4dCwgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcblxuaW1wb3J0IHsgR2VuZXJhbENvbnRleHQgfSBmcm9tICdAaGVscGVycy9jb250ZXh0cyc7XG5cbmNvbnN0IG1hcFZhbHVlVXNlRWZmZWN0ID1cbiAgICAoe1xuICAgICAgICBnb29nbGUsXG4gICAgICAgIGhhbmRsZUNlbnRlckNoYW5nZWQsXG4gICAgICAgIGhhbmRsZVR5cGVDaGFuZ2VkLFxuICAgICAgICBoYW5kbGVab29tQ2hhbmdlZCxcbiAgICAgICAgbWFwVmFsdWUsXG4gICAgICAgIG1hcmtlcnMsXG4gICAgfSkgPT5cbiAgICAoKSA9PiB7XG4gICAgICAgIGlmIChtYXBWYWx1ZSkge1xuICAgICAgICAgICAgbWFwVmFsdWUuc2V0VGlsdCgwKTtcbiAgICAgICAgICAgIGlmIChoYW5kbGVDZW50ZXJDaGFuZ2VkKSB7XG4gICAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFwVmFsdWUsICdkcmFnZW5kJywgKCkgPT5cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2VudGVyQ2hhbmdlZChtYXBWYWx1ZS5nZXRDZW50ZXIoKSlcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGhhbmRsZVpvb21DaGFuZ2VkKSB7XG4gICAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFwVmFsdWUsICd6b29tX2NoYW5nZWQnLCAoKSA9PlxuICAgICAgICAgICAgICAgICAgICBoYW5kbGVab29tQ2hhbmdlZChtYXBWYWx1ZS5nZXRab29tKCkpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChoYW5kbGVUeXBlQ2hhbmdlZCkge1xuICAgICAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKFxuICAgICAgICAgICAgICAgICAgICBtYXBWYWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgJ21hcHR5cGVpZF9jaGFuZ2VkJyxcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4gaGFuZGxlWm9vbUNoYW5nZWQobWFwVmFsdWUuZ2V0TWFwVHlwZUlkKCkpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbWFya2Vycy5mb3JFYWNoKChtYXJrZXIpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBtYXJrZXJPYmplY3QgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgICAgICAgICAgZHJhZ2dhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgaWNvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiAnaHR0cHM6Ly9jZG4wLmljb25maW5kZXIuY29tL2RhdGEvaWNvbnMvc21hbGwtbi1mbGF0LzI0LzY3ODExMS1tYXAtbWFya2VyLTUxMi5wbmcnLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2NhbGVkU2l6ZTogbmV3IGdvb2dsZS5tYXBzLlNpemUoMzIsIDMyKSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogaTE4bmV4dC50KCdMb2NhdGlvbicpLFxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhtYXJrZXIubGF0LCBtYXJrZXIubG5nKSxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGU6IG1hcmtlci50aXRsZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBtYXJrZXJPYmplY3Quc2V0TWFwKG1hcFZhbHVlKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuICgpID0+IHtcbiAgICAgICAgICAgIGlmIChtYXBWYWx1ZSkge1xuICAgICAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmNsZWFyTGlzdGVuZXJzKG1hcFZhbHVlLCAnZHJhZ2VuZCcpO1xuICAgICAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmNsZWFyTGlzdGVuZXJzKG1hcFZhbHVlLCAnem9vbV9jaGFuZ2VkJyk7XG4gICAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuY2xlYXJMaXN0ZW5lcnMobWFwVmFsdWUsICdtYXB0eXBlaWRfY2hhbmdlZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH07XG5cbmNvbnN0IE1hcFZpZXcgPSAoe1xuICAgIGhhbmRsZUNlbnRlckNoYW5nZWQsXG4gICAgaGFuZGxlVHlwZUNoYW5nZWQsXG4gICAgaGFuZGxlWm9vbUNoYW5nZWQsXG4gICAgbGF0LFxuICAgIGxuZyxcbiAgICBtYXBUeXBlLFxuICAgIG1hcmtlcnMsXG4gICAgem9vbSxcbn0pID0+IHtcbiAgICBjb25zdCB7IGdvb2dsZSB9ID0gdXNlQ29udGV4dChHZW5lcmFsQ29udGV4dCk7XG4gICAgY29uc3QgW21hcFZhbHVlLCBzZXRNYXBWYWx1ZV0gPSB1c2VTdGF0ZShudWxsKTtcbiAgICB1c2VFZmZlY3QoXG4gICAgICAgIG1hcFZhbHVlVXNlRWZmZWN0KHtcbiAgICAgICAgICAgIGdvb2dsZSxcbiAgICAgICAgICAgIGhhbmRsZUNlbnRlckNoYW5nZWQsXG4gICAgICAgICAgICBoYW5kbGVUeXBlQ2hhbmdlZCxcbiAgICAgICAgICAgIGhhbmRsZVpvb21DaGFuZ2VkLFxuICAgICAgICAgICAgbWFwVmFsdWUsXG4gICAgICAgICAgICBtYXJrZXJzLFxuICAgICAgICB9KSxcbiAgICAgICAgW21hcFZhbHVlXVxuICAgICk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8TWFwXG4gICAgICAgICAgICBmdWxsc2NyZWVuQ29udHJvbD17ZmFsc2V9XG4gICAgICAgICAgICBnb29nbGU9e2dvb2dsZX1cbiAgICAgICAgICAgIGluaXRpYWxDZW50ZXI9e3sgbGF0LCBsbmcgfX1cbiAgICAgICAgICAgIG1hcFR5cGU9e21hcFR5cGV9XG4gICAgICAgICAgICBtYXBUeXBlQ29udHJvbD17dHJ1ZX1cbiAgICAgICAgICAgIG9uUmVhZHk9eyhfLCBtYXApID0+IHNldE1hcFZhbHVlKG1hcCl9XG4gICAgICAgICAgICByb3RhdGVDb250cm9sPXtmYWxzZX1cbiAgICAgICAgICAgIHN0cmVldFZpZXdDb250cm9sPXtmYWxzZX1cbiAgICAgICAgICAgIHpvb209e3pvb219XG4gICAgICAgICAgICB6b29tQ29udHJvbD17dHJ1ZX1cbiAgICAgICAgLz5cbiAgICApO1xufTtcblxuTWFwVmlldy5wcm9wVHlwZXMgPSB7XG4gICAgaGFuZGxlQ2VudGVyQ2hhbmdlZDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlVHlwZUNoYW5nZWQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhhbmRsZVpvb21DaGFuZ2VkOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBsYXQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgbG5nOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG1hcFR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgbWFya2VyczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHpvb206IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBNYXBWaWV3O1xuIl19