"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _SplitWrapper = _interopRequireDefault(require("./SplitWrapper"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: block;\n    background-color: ", ";\n    ", "\n    min-height: 29px;\n    position: relative;\n"])), function (_ref) {
  var _ref$backgroundColor = _ref.backgroundColor,
      backgroundColor = _ref$backgroundColor === void 0 ? '#000' : _ref$backgroundColor;
  return backgroundColor;
}, function (_ref2) {
  var roundedBorders = _ref2.roundedBorders;
  return roundedBorders && 'border-radius: 17px;';
});

var DataContent = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    font-weight: ", ";\n    ", "\n    font-size: ", ";\n    line-height: ", ";\n    color: ", ";\n    padding: 8px 15px;\n    align-items: center;\n    overflow: hidden;\n    span {\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: pre-line;\n    }\n"])), function (_ref3) {
  var fontBold = _ref3.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref4) {
  var fontItalic = _ref4.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref5) {
  var _ref5$fontSize = _ref5.fontSize,
      fontSize = _ref5$fontSize === void 0 ? '13px' : _ref5$fontSize;
  return fontSize;
}, function (_ref6) {
  var _ref6$fontSize = _ref6.fontSize,
      fontSize = _ref6$fontSize === void 0 ? '13px' : _ref6$fontSize;
  return fontSize;
}, function (_ref7) {
  var _ref7$color = _ref7.color,
      color = _ref7$color === void 0 ? '#fff' : _ref7$color;
  return color;
});

var WarningsContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    padding: 4px 6px 0 6px;\n    background-color: #d32f2f;\n    span {\n        min-height: 13px;\n        color: #fff;\n        font-size: 11px;\n        font-weight: bold;\n        line-height: 13px;\n        margin-bottom: 4px;\n    }\n"])));

var TableHeaderView = function TableHeaderView() {
  var _ref8 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      headerStyle = _ref8.headerStyle,
      isWidgetSelected = _ref8.isWidgetSelected,
      onSaveColumnsWidth = _ref8.onSaveColumnsWidth,
      preparedCols = _ref8.preparedCols,
      sizes = _ref8.sizes,
      textStyle = _ref8.textStyle,
      warnings = _ref8.warnings;

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    backgroundColor: headerStyle.backgroundColor,
    roundedBorders: headerStyle.roundedBorders,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_SplitWrapper.default, {
      isWidgetSelected: isWidgetSelected,
      sizes: sizes,
      direction: "horizontal",
      gutterSize: "4",
      minSize: "30",
      cursor: "col-resize",
      orderCols: preparedCols.reduce(function (acc, current) {
        return acc + current.label;
      }, ''),
      onDragEnd: onSaveColumnsWidth,
      children: preparedCols.map(function (col, index) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(DataContent, _objectSpread(_objectSpread({}, textStyle), {}, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: col.editable_label || col.label
          })
        }), "header-".concat(index));
      })
    }), warnings.length > 0 && /*#__PURE__*/(0, _jsxRuntime.jsx)(WarningsContainer, {
      children: warnings.map(function (warning, index) {
        return /*#__PURE__*/(0, _jsxRuntime.jsxs)("span", {
          children: ["*", warning]
        }, "warning-".concat(index));
      })
    })]
  });
};

TableHeaderView.propTypes = {
  headerStyle: _propTypes.default.object,
  isWidgetSelected: _propTypes.default.bool,
  onSaveColumnsWidth: _propTypes.default.func,
  preparedCols: _propTypes.default.array,
  sizes: _propTypes.default.array,
  textStyle: _propTypes.default.object,
  warnings: _propTypes.default.array
};
var _default = TableHeaderView;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvdGFibGUvVGFibGVIZWFkZXJWaWV3LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsImJhY2tncm91bmRDb2xvciIsInJvdW5kZWRCb3JkZXJzIiwiRGF0YUNvbnRlbnQiLCJmb250Qm9sZCIsImZvbnRJdGFsaWMiLCJmb250U2l6ZSIsImNvbG9yIiwiV2FybmluZ3NDb250YWluZXIiLCJUYWJsZUhlYWRlclZpZXciLCJoZWFkZXJTdHlsZSIsImlzV2lkZ2V0U2VsZWN0ZWQiLCJvblNhdmVDb2x1bW5zV2lkdGgiLCJwcmVwYXJlZENvbHMiLCJzaXplcyIsInRleHRTdHlsZSIsIndhcm5pbmdzIiwicmVkdWNlIiwiYWNjIiwiY3VycmVudCIsImxhYmVsIiwibWFwIiwiY29sIiwiaW5kZXgiLCJlZGl0YWJsZV9sYWJlbCIsImxlbmd0aCIsIndhcm5pbmciLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJib29sIiwiZnVuYyIsImFycmF5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLG9MQUVTO0FBQUEsa0NBQUdDLGVBQUg7QUFBQSxNQUFHQSxlQUFILHFDQUFxQixNQUFyQjtBQUFBLFNBQWtDQSxlQUFsQztBQUFBLENBRlQsRUFHVDtBQUFBLE1BQUdDLGNBQUgsU0FBR0EsY0FBSDtBQUFBLFNBQXdCQSxjQUFjLElBQUksc0JBQTFDO0FBQUEsQ0FIUyxDQUFmOztBQVFBLElBQU1DLFdBQVcsR0FBR0osMEJBQU9DLEdBQVYseVhBRUU7QUFBQSxNQUFHSSxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQkEsUUFBUSxHQUFHLE1BQUgsR0FBWSxLQUF2QztBQUFBLENBRkYsRUFHWDtBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLFNBQW9CQSxVQUFVLElBQUkscUJBQWxDO0FBQUEsQ0FIVyxFQUlBO0FBQUEsNkJBQUdDLFFBQUg7QUFBQSxNQUFHQSxRQUFILCtCQUFjLE1BQWQ7QUFBQSxTQUEyQkEsUUFBM0I7QUFBQSxDQUpBLEVBS0U7QUFBQSw2QkFBR0EsUUFBSDtBQUFBLE1BQUdBLFFBQUgsK0JBQWMsTUFBZDtBQUFBLFNBQTJCQSxRQUEzQjtBQUFBLENBTEYsRUFNSjtBQUFBLDBCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCw0QkFBVyxNQUFYO0FBQUEsU0FBd0JBLEtBQXhCO0FBQUEsQ0FOSSxDQUFqQjs7QUFpQkEsSUFBTUMsaUJBQWlCLEdBQUdULDBCQUFPQyxHQUFWLDBXQUF2Qjs7QUFlQSxJQUFNUyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLEdBUWI7QUFBQSxrRkFBUCxFQUFPO0FBQUEsTUFQUEMsV0FPTyxTQVBQQSxXQU9PO0FBQUEsTUFOUEMsZ0JBTU8sU0FOUEEsZ0JBTU87QUFBQSxNQUxQQyxrQkFLTyxTQUxQQSxrQkFLTztBQUFBLE1BSlBDLFlBSU8sU0FKUEEsWUFJTztBQUFBLE1BSFBDLEtBR08sU0FIUEEsS0FHTztBQUFBLE1BRlBDLFNBRU8sU0FGUEEsU0FFTztBQUFBLE1BRFBDLFFBQ08sU0FEUEEsUUFDTzs7QUFDUCxzQkFDSSxzQkFBQyxTQUFEO0FBQ0ksSUFBQSxlQUFlLEVBQUVOLFdBQVcsQ0FBQ1QsZUFEakM7QUFFSSxJQUFBLGNBQWMsRUFBRVMsV0FBVyxDQUFDUixjQUZoQztBQUFBLDRCQUlJLHFCQUFDLHFCQUFEO0FBQ0ksTUFBQSxnQkFBZ0IsRUFBRVMsZ0JBRHRCO0FBRUksTUFBQSxLQUFLLEVBQUVHLEtBRlg7QUFHSSxNQUFBLFNBQVMsRUFBQyxZQUhkO0FBSUksTUFBQSxVQUFVLEVBQUMsR0FKZjtBQUtJLE1BQUEsT0FBTyxFQUFDLElBTFo7QUFNSSxNQUFBLE1BQU0sRUFBQyxZQU5YO0FBT0ksTUFBQSxTQUFTLEVBQUVELFlBQVksQ0FBQ0ksTUFBYixDQUNQLFVBQUNDLEdBQUQsRUFBTUMsT0FBTjtBQUFBLGVBQWtCRCxHQUFHLEdBQUdDLE9BQU8sQ0FBQ0MsS0FBaEM7QUFBQSxPQURPLEVBRVAsRUFGTyxDQVBmO0FBV0ksTUFBQSxTQUFTLEVBQUVSLGtCQVhmO0FBQUEsZ0JBYUtDLFlBQVksQ0FBQ1EsR0FBYixDQUFpQixVQUFDQyxHQUFELEVBQU1DLEtBQU47QUFBQSw0QkFDZCxxQkFBQyxXQUFELGtDQUF5Q1IsU0FBekM7QUFBQSxpQ0FDSTtBQUFBLHNCQUFPTyxHQUFHLENBQUNFLGNBQUosSUFBc0JGLEdBQUcsQ0FBQ0Y7QUFBakM7QUFESiw2QkFBNEJHLEtBQTVCLEVBRGM7QUFBQSxPQUFqQjtBQWJMLE1BSkosRUF1QktQLFFBQVEsQ0FBQ1MsTUFBVCxHQUFrQixDQUFsQixpQkFDRyxxQkFBQyxpQkFBRDtBQUFBLGdCQUNLVCxRQUFRLENBQUNLLEdBQVQsQ0FBYSxVQUFDSyxPQUFELEVBQVVILEtBQVY7QUFBQSw0QkFDVjtBQUFBLDBCQUFpQ0csT0FBakM7QUFBQSw2QkFBc0JILEtBQXRCLEVBRFU7QUFBQSxPQUFiO0FBREwsTUF4QlI7QUFBQSxJQURKO0FBaUNILENBMUNEOztBQTRDQWQsZUFBZSxDQUFDa0IsU0FBaEIsR0FBNEI7QUFDeEJqQixFQUFBQSxXQUFXLEVBQUVrQixtQkFBVUMsTUFEQztBQUV4QmxCLEVBQUFBLGdCQUFnQixFQUFFaUIsbUJBQVVFLElBRko7QUFHeEJsQixFQUFBQSxrQkFBa0IsRUFBRWdCLG1CQUFVRyxJQUhOO0FBSXhCbEIsRUFBQUEsWUFBWSxFQUFFZSxtQkFBVUksS0FKQTtBQUt4QmxCLEVBQUFBLEtBQUssRUFBRWMsbUJBQVVJLEtBTE87QUFNeEJqQixFQUFBQSxTQUFTLEVBQUVhLG1CQUFVQyxNQU5HO0FBT3hCYixFQUFBQSxRQUFRLEVBQUVZLG1CQUFVSTtBQVBJLENBQTVCO2VBVWV2QixlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgU3BsaXRXcmFwcGVyIGZyb20gJy4vU3BsaXRXcmFwcGVyJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkeyh7IGJhY2tncm91bmRDb2xvciA9ICcjMDAwJyB9KSA9PiBiYWNrZ3JvdW5kQ29sb3J9O1xuICAgICR7KHsgcm91bmRlZEJvcmRlcnMgfSkgPT4gcm91bmRlZEJvcmRlcnMgJiYgJ2JvcmRlci1yYWRpdXM6IDE3cHg7J31cbiAgICBtaW4taGVpZ2h0OiAyOXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbmA7XG5cbmNvbnN0IERhdGFDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtd2VpZ2h0OiAkeyh7IGZvbnRCb2xkIH0pID0+IChmb250Qm9sZCA/ICdib2xkJyA6ICcxMDAnKX07XG4gICAgJHsoeyBmb250SXRhbGljIH0pID0+IGZvbnRJdGFsaWMgJiYgJ2ZvbnQtc3R5bGU6IGl0YWxpYzsnfVxuICAgIGZvbnQtc2l6ZTogJHsoeyBmb250U2l6ZSA9ICcxM3B4JyB9KSA9PiBmb250U2l6ZX07XG4gICAgbGluZS1oZWlnaHQ6ICR7KHsgZm9udFNpemUgPSAnMTNweCcgfSkgPT4gZm9udFNpemV9O1xuICAgIGNvbG9yOiAkeyh7IGNvbG9yID0gJyNmZmYnIH0pID0+IGNvbG9yfTtcbiAgICBwYWRkaW5nOiA4cHggMTVweDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgc3BhbiB7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG4gICAgfVxuYDtcblxuY29uc3QgV2FybmluZ3NDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBwYWRkaW5nOiA0cHggNnB4IDAgNnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkMzJmMmY7XG4gICAgc3BhbiB7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDEzcHg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXNpemU6IDExcHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBsaW5lLWhlaWdodDogMTNweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuICAgIH1cbmA7XG5cbmNvbnN0IFRhYmxlSGVhZGVyVmlldyA9ICh7XG4gICAgaGVhZGVyU3R5bGUsXG4gICAgaXNXaWRnZXRTZWxlY3RlZCxcbiAgICBvblNhdmVDb2x1bW5zV2lkdGgsXG4gICAgcHJlcGFyZWRDb2xzLFxuICAgIHNpemVzLFxuICAgIHRleHRTdHlsZSxcbiAgICB3YXJuaW5ncyxcbn0gPSB7fSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXJcbiAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcj17aGVhZGVyU3R5bGUuYmFja2dyb3VuZENvbG9yfVxuICAgICAgICAgICAgcm91bmRlZEJvcmRlcnM9e2hlYWRlclN0eWxlLnJvdW5kZWRCb3JkZXJzfVxuICAgICAgICA+XG4gICAgICAgICAgICA8U3BsaXRXcmFwcGVyXG4gICAgICAgICAgICAgICAgaXNXaWRnZXRTZWxlY3RlZD17aXNXaWRnZXRTZWxlY3RlZH1cbiAgICAgICAgICAgICAgICBzaXplcz17c2l6ZXN9XG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uPVwiaG9yaXpvbnRhbFwiXG4gICAgICAgICAgICAgICAgZ3V0dGVyU2l6ZT1cIjRcIlxuICAgICAgICAgICAgICAgIG1pblNpemU9XCIzMFwiXG4gICAgICAgICAgICAgICAgY3Vyc29yPVwiY29sLXJlc2l6ZVwiXG4gICAgICAgICAgICAgICAgb3JkZXJDb2xzPXtwcmVwYXJlZENvbHMucmVkdWNlKFxuICAgICAgICAgICAgICAgICAgICAoYWNjLCBjdXJyZW50KSA9PiBhY2MgKyBjdXJyZW50LmxhYmVsLFxuICAgICAgICAgICAgICAgICAgICAnJ1xuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgb25EcmFnRW5kPXtvblNhdmVDb2x1bW5zV2lkdGh9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge3ByZXBhcmVkQ29scy5tYXAoKGNvbCwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgICAgPERhdGFDb250ZW50IGtleT17YGhlYWRlci0ke2luZGV4fWB9IHsuLi50ZXh0U3R5bGV9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2NvbC5lZGl0YWJsZV9sYWJlbCB8fCBjb2wubGFiZWx9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L0RhdGFDb250ZW50PlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9TcGxpdFdyYXBwZXI+XG4gICAgICAgICAgICB7d2FybmluZ3MubGVuZ3RoID4gMCAmJiAoXG4gICAgICAgICAgICAgICAgPFdhcm5pbmdzQ29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICB7d2FybmluZ3MubWFwKCh3YXJuaW5nLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4ga2V5PXtgd2FybmluZy0ke2luZGV4fWB9Pip7d2FybmluZ308L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgIDwvV2FybmluZ3NDb250YWluZXI+XG4gICAgICAgICAgICApfVxuICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICApO1xufTtcblxuVGFibGVIZWFkZXJWaWV3LnByb3BUeXBlcyA9IHtcbiAgICBoZWFkZXJTdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBpc1dpZGdldFNlbGVjdGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBvblNhdmVDb2x1bW5zV2lkdGg6IFByb3BUeXBlcy5mdW5jLFxuICAgIHByZXBhcmVkQ29sczogUHJvcFR5cGVzLmFycmF5LFxuICAgIHNpemVzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgdGV4dFN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHdhcm5pbmdzOiBQcm9wVHlwZXMuYXJyYXksXG59O1xuXG5leHBvcnQgZGVmYXVsdCBUYWJsZUhlYWRlclZpZXc7XG4iXX0=