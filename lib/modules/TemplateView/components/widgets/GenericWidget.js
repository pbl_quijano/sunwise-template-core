"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledGridItem = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    height: 100%;\n    padding: 4px;\n    width: 100%;\n"])));

var GenericWidget = function GenericWidget() {
  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledGridItem, {
    children: t('Widget')
  });
};

var _default = GenericWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvR2VuZXJpY1dpZGdldC5qcyJdLCJuYW1lcyI6WyJTdHlsZWRHcmlkSXRlbSIsInN0eWxlZCIsImRpdiIsIkdlbmVyaWNXaWRnZXQiLCJ0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxjQUFjLEdBQUdDLDBCQUFPQyxHQUFWLCtIQUFwQjs7QUFNQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQU07QUFDeEIsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLHNCQUFPLHFCQUFDLGNBQUQ7QUFBQSxjQUFpQkEsQ0FBQyxDQUFDLFFBQUQ7QUFBbEIsSUFBUDtBQUNILENBSEQ7O2VBS2VELGEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmNvbnN0IFN0eWxlZEdyaWRJdGVtID0gc3R5bGVkLmRpdmBcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgcGFkZGluZzogNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuYDtcblxuY29uc3QgR2VuZXJpY1dpZGdldCA9ICgpID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgcmV0dXJuIDxTdHlsZWRHcmlkSXRlbT57dCgnV2lkZ2V0Jyl9PC9TdHlsZWRHcmlkSXRlbT47XG59O1xuXG5leHBvcnQgZGVmYXVsdCBHZW5lcmljV2lkZ2V0O1xuIl19