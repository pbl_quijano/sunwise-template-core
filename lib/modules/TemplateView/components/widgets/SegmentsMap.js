"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _googleMapsReact = require("google-maps-react");

var _get = _interopRequireDefault(require("lodash/get"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _maps = require("../../../../constants/maps");

var _contexts = require("../../../../helpers/contexts");

var _maps2 = require("../../../../helpers/maps");

var _utils = require("../../../../helpers/utils");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    position: relative;\n    width: ", ";\n    height: ", ";\n\n    &.unclickable {\n        pointer-events: none;\n        .gmnoprint a,\n        .gmnoprint span,\n        .gm-style-cc {\n            display: none;\n        }\n        .gmnoprint div {\n            background: none !important;\n        }\n    }\n    .popup-container {\n        cursor: pointer;\n        height: 0;\n        position: absolute;\n        width: 200px;\n        transform-origin: 0% 0%;\n        z-index: 1;\n        &:hover {\n            z-index: 2;\n        }\n        .popup-bubble-anchor {\n            position: absolute;\n            width: 100%;\n            bottom: 6px;\n            left: 0;\n            span {\n                position: absolute;\n                height: 13px;\n                top: 0;\n                left: 0;\n                transform: translate(-50%, -100%);\n                background-color: #002438;\n                color: #ffffff;\n                padding: 0 2px;\n                border-radius: 2px;\n                overflow-y: hidden;\n                max-height: 60px;\n                font-size: 8px;\n                letter-spacing: 0.2px;\n                line-height: 13px;\n                user-select: none;\n                font-weight: 100;\n            }\n        }\n    }\n    & > div {\n        position: absolute;\n        width: 100%;\n        height: 100%;\n    }\n"])), function (_ref) {
  var _ref$width = _ref.width,
      width = _ref$width === void 0 ? '100%' : _ref$width;
  return width;
}, function (_ref2) {
  var _ref2$height = _ref2.height,
      height = _ref2$height === void 0 ? '100%' : _ref2$height;
  return height;
});

var SegmentsMap = function SegmentsMap(_ref3) {
  var _ref3$clickable = _ref3.clickable,
      clickable = _ref3$clickable === void 0 ? true : _ref3$clickable,
      _ref3$fullscreenContr = _ref3.fullscreenControl,
      fullscreenControl = _ref3$fullscreenContr === void 0 ? false : _ref3$fullscreenContr,
      handleCenterChanged = _ref3.handleCenterChanged,
      handleZoomChanged = _ref3.handleZoomChanged,
      height = _ref3.height,
      initCenter = _ref3.initCenter,
      projectLocation = _ref3.projectLocation,
      _ref3$scaleControl = _ref3.scaleControl,
      scaleControl = _ref3$scaleControl === void 0 ? true : _ref3$scaleControl,
      _ref3$segmentFillColo = _ref3.segmentFillColor,
      segmentFillColor = _ref3$segmentFillColo === void 0 ? _maps.SEGMENT_COLOR : _ref3$segmentFillColo,
      _ref3$segments = _ref3.segments,
      segments = _ref3$segments === void 0 ? [] : _ref3$segments,
      _ref3$segmentStrokeCo = _ref3.segmentStrokeColor,
      segmentStrokeColor = _ref3$segmentStrokeCo === void 0 ? _maps.SEGMENT_STROKE_COLOR : _ref3$segmentStrokeCo,
      _ref3$showLabels = _ref3.showLabels,
      showLabels = _ref3$showLabels === void 0 ? true : _ref3$showLabels,
      _ref3$showProjectMark = _ref3.showProjectMarker,
      showProjectMarker = _ref3$showProjectMark === void 0 ? true : _ref3$showProjectMark,
      _ref3$solarModuleFill = _ref3.solarModuleFillColor,
      solarModuleFillColor = _ref3$solarModuleFill === void 0 ? _maps.SOLAR_MODULE_COLOR : _ref3$solarModuleFill,
      _ref3$solarModuleStro = _ref3.solarModuleStrokeColor,
      solarModuleStrokeColor = _ref3$solarModuleStro === void 0 ? _maps.SOLAR_MODULE_STROKE_COLOR : _ref3$solarModuleStro,
      width = _ref3.width,
      _ref3$zoom = _ref3.zoom,
      zoom = _ref3$zoom === void 0 ? 18 : _ref3$zoom,
      _ref3$zoomControl = _ref3.zoomControl,
      zoomControl = _ref3$zoomControl === void 0 ? true : _ref3$zoomControl;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      google = _useContext.google;

  var center = {
    lat: (0, _get.default)(initCenter, 'latitude', 20.9664281),
    lng: (0, _get.default)(initCenter, 'longitude', -89.6239336)
  };
  var projectMarkerCenter = {
    lat: (0, _get.default)(projectLocation, 'latitude', 20.9664281),
    lng: (0, _get.default)(projectLocation, 'longitude', -89.6239336)
  };

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      mapValue = _useState2[0],
      setMapValue = _useState2[1];

  var _useState3 = (0, _react.useState)({}),
      _useState4 = _slicedToArray(_useState3, 2),
      shapes = _useState4[0],
      setShapes = _useState4[1];

  (0, _react.useEffect)(function () {
    if (mapValue) {
      mapValue.setOptions({
        zoomControl: zoomControl
      });
    }
  }, [zoomControl]);
  (0, _react.useEffect)(function () {
    if (mapValue) {
      mapValue.setOptions({
        scaleControl: scaleControl
      });
    }
  }, [scaleControl]);
  (0, _react.useEffect)(function () {
    Object.keys(shapes).forEach(function (key) {
      shapes[key].setLabelsVisibility(showLabels);
    });
  }, [showLabels]);
  (0, _react.useEffect)(function () {
    if (mapValue) {
      if (showProjectMarker) {
        projectMarker.setMap(mapValue);
      } else {
        projectMarker.setMap(null);
      }
    }
  }, [showProjectMarker]);
  (0, _react.useEffect)(function () {
    Object.keys(shapes).forEach(function (key) {
      shapes[key].setOptions({
        fillColor: segmentFillColor
      });
    });
  }, [segmentFillColor]);
  (0, _react.useEffect)(function () {
    Object.keys(shapes).forEach(function (key) {
      shapes[key].setOptions({
        strokeColor: segmentStrokeColor
      });
    });
  }, [segmentStrokeColor]);
  (0, _react.useEffect)(function () {
    Object.keys(shapes).forEach(function (key) {
      shapes[key].setSolarModulesFillColor(solarModuleFillColor);
    });
  }, [solarModuleFillColor]);
  (0, _react.useEffect)(function () {
    Object.keys(shapes).forEach(function (key) {
      shapes[key].setSolarModulesStrokeColor(solarModuleStrokeColor);
    });
  }, [solarModuleStrokeColor]);

  var _useState5 = (0, _react.useState)(new google.maps.Marker({
    title: (0, _get.default)(projectLocation, 'address', ''),
    name: t('Location'),
    position: new google.maps.LatLng(projectMarkerCenter.lat, projectMarkerCenter.lng)
  })),
      _useState6 = _slicedToArray(_useState5, 1),
      projectMarker = _useState6[0];

  (0, _react.useEffect)((0, _maps2.mapValueUseEffect)(google, mapValue, projectMarker, (0, _utils.arraytoDictionary)(segments, 'id'), handleCenterChanged, handleZoomChanged, scaleControl, segmentFillColor, segmentStrokeColor, showLabels, showProjectMarker, solarModuleFillColor, solarModuleStrokeColor, setShapes, zoomControl), [mapValue]);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
    className: !clickable && 'unclickable',
    height: height,
    width: width,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_googleMapsReact.Map, {
      fullscreenControl: fullscreenControl,
      google: google,
      initialCenter: center,
      mapTypeControl: false,
      mapType: "satellite",
      onReady: function onReady(p, map) {
        return setMapValue(map);
      },
      rotateControl: false,
      scaleControl: scaleControl,
      streetViewControl: false,
      styles: _maps.MAP_STYLES,
      zoom: zoom
    })
  });
};

SegmentsMap.propTypes = {
  clickable: _propTypes.default.bool,
  fullscreenControl: _propTypes.default.bool,
  google: _propTypes.default.object,
  handleCenterChanged: _propTypes.default.func,
  handleZoomChanged: _propTypes.default.func,
  height: _propTypes.default.string,
  initCenter: _propTypes.default.object,
  projectLocation: _propTypes.default.object,
  scaleControl: _propTypes.default.bool,
  segmentFillColor: _propTypes.default.string,
  segments: _propTypes.default.array,
  segmentsDictionary: _propTypes.default.array,
  segmentStrokeColor: _propTypes.default.string,
  showLabels: _propTypes.default.bool,
  showProjectMarker: _propTypes.default.bool,
  solarModuleFillColor: _propTypes.default.string,
  solarModuleStrokeColor: _propTypes.default.string,
  width: _propTypes.default.string,
  zoom: _propTypes.default.number,
  zoomControl: _propTypes.default.bool
};
var _default = SegmentsMap;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU2VnbWVudHNNYXAuanMiXSwibmFtZXMiOlsiV3JhcHBlciIsInN0eWxlZCIsImRpdiIsIndpZHRoIiwiaGVpZ2h0IiwiU2VnbWVudHNNYXAiLCJjbGlja2FibGUiLCJmdWxsc2NyZWVuQ29udHJvbCIsImhhbmRsZUNlbnRlckNoYW5nZWQiLCJoYW5kbGVab29tQ2hhbmdlZCIsImluaXRDZW50ZXIiLCJwcm9qZWN0TG9jYXRpb24iLCJzY2FsZUNvbnRyb2wiLCJzZWdtZW50RmlsbENvbG9yIiwiU0VHTUVOVF9DT0xPUiIsInNlZ21lbnRzIiwic2VnbWVudFN0cm9rZUNvbG9yIiwiU0VHTUVOVF9TVFJPS0VfQ09MT1IiLCJzaG93TGFiZWxzIiwic2hvd1Byb2plY3RNYXJrZXIiLCJzb2xhck1vZHVsZUZpbGxDb2xvciIsIlNPTEFSX01PRFVMRV9DT0xPUiIsInNvbGFyTW9kdWxlU3Ryb2tlQ29sb3IiLCJTT0xBUl9NT0RVTEVfU1RST0tFX0NPTE9SIiwiem9vbSIsInpvb21Db250cm9sIiwidCIsIkdlbmVyYWxDb250ZXh0IiwiZ29vZ2xlIiwiY2VudGVyIiwibGF0IiwibG5nIiwicHJvamVjdE1hcmtlckNlbnRlciIsIm1hcFZhbHVlIiwic2V0TWFwVmFsdWUiLCJzaGFwZXMiLCJzZXRTaGFwZXMiLCJzZXRPcHRpb25zIiwiT2JqZWN0Iiwia2V5cyIsImZvckVhY2giLCJrZXkiLCJzZXRMYWJlbHNWaXNpYmlsaXR5IiwicHJvamVjdE1hcmtlciIsInNldE1hcCIsImZpbGxDb2xvciIsInN0cm9rZUNvbG9yIiwic2V0U29sYXJNb2R1bGVzRmlsbENvbG9yIiwic2V0U29sYXJNb2R1bGVzU3Ryb2tlQ29sb3IiLCJtYXBzIiwiTWFya2VyIiwidGl0bGUiLCJuYW1lIiwicG9zaXRpb24iLCJMYXRMbmciLCJwIiwibWFwIiwiTUFQX1NUWUxFUyIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJvYmplY3QiLCJmdW5jIiwic3RyaW5nIiwiYXJyYXkiLCJzZWdtZW50c0RpY3Rpb25hcnkiLCJudW1iZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFRQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLE9BQU8sR0FBR0MsMEJBQU9DLEdBQVYsbzhDQUVBO0FBQUEsd0JBQUdDLEtBQUg7QUFBQSxNQUFHQSxLQUFILDJCQUFXLE1BQVg7QUFBQSxTQUF3QkEsS0FBeEI7QUFBQSxDQUZBLEVBR0M7QUFBQSwyQkFBR0MsTUFBSDtBQUFBLE1BQUdBLE1BQUgsNkJBQVksTUFBWjtBQUFBLFNBQXlCQSxNQUF6QjtBQUFBLENBSEQsQ0FBYjs7QUEwREEsSUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsUUFtQmQ7QUFBQSw4QkFsQkZDLFNBa0JFO0FBQUEsTUFsQkZBLFNBa0JFLGdDQWxCVSxJQWtCVjtBQUFBLG9DQWpCRkMsaUJBaUJFO0FBQUEsTUFqQkZBLGlCQWlCRSxzQ0FqQmtCLEtBaUJsQjtBQUFBLE1BaEJGQyxtQkFnQkUsU0FoQkZBLG1CQWdCRTtBQUFBLE1BZkZDLGlCQWVFLFNBZkZBLGlCQWVFO0FBQUEsTUFkRkwsTUFjRSxTQWRGQSxNQWNFO0FBQUEsTUFiRk0sVUFhRSxTQWJGQSxVQWFFO0FBQUEsTUFaRkMsZUFZRSxTQVpGQSxlQVlFO0FBQUEsaUNBWEZDLFlBV0U7QUFBQSxNQVhGQSxZQVdFLG1DQVhhLElBV2I7QUFBQSxvQ0FWRkMsZ0JBVUU7QUFBQSxNQVZGQSxnQkFVRSxzQ0FWaUJDLG1CQVVqQjtBQUFBLDZCQVRGQyxRQVNFO0FBQUEsTUFURkEsUUFTRSwrQkFUUyxFQVNUO0FBQUEsb0NBUkZDLGtCQVFFO0FBQUEsTUFSRkEsa0JBUUUsc0NBUm1CQywwQkFRbkI7QUFBQSwrQkFQRkMsVUFPRTtBQUFBLE1BUEZBLFVBT0UsaUNBUFcsSUFPWDtBQUFBLG9DQU5GQyxpQkFNRTtBQUFBLE1BTkZBLGlCQU1FLHNDQU5rQixJQU1sQjtBQUFBLG9DQUxGQyxvQkFLRTtBQUFBLE1BTEZBLG9CQUtFLHNDQUxxQkMsd0JBS3JCO0FBQUEsb0NBSkZDLHNCQUlFO0FBQUEsTUFKRkEsc0JBSUUsc0NBSnVCQywrQkFJdkI7QUFBQSxNQUhGcEIsS0FHRSxTQUhGQSxLQUdFO0FBQUEseUJBRkZxQixJQUVFO0FBQUEsTUFGRkEsSUFFRSwyQkFGSyxFQUVMO0FBQUEsZ0NBREZDLFdBQ0U7QUFBQSxNQURGQSxXQUNFLGtDQURZLElBQ1o7O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLG9CQUFtQix1QkFBV0Msd0JBQVgsQ0FBbkI7QUFBQSxNQUFRQyxNQUFSLGVBQVFBLE1BQVI7O0FBQ0EsTUFBTUMsTUFBTSxHQUFHO0FBQ1hDLElBQUFBLEdBQUcsRUFBRSxrQkFBSXBCLFVBQUosRUFBZ0IsVUFBaEIsRUFBNEIsVUFBNUIsQ0FETTtBQUVYcUIsSUFBQUEsR0FBRyxFQUFFLGtCQUFJckIsVUFBSixFQUFnQixXQUFoQixFQUE2QixDQUFDLFVBQTlCO0FBRk0sR0FBZjtBQUlBLE1BQU1zQixtQkFBbUIsR0FBRztBQUN4QkYsSUFBQUEsR0FBRyxFQUFFLGtCQUFJbkIsZUFBSixFQUFxQixVQUFyQixFQUFpQyxVQUFqQyxDQURtQjtBQUV4Qm9CLElBQUFBLEdBQUcsRUFBRSxrQkFBSXBCLGVBQUosRUFBcUIsV0FBckIsRUFBa0MsQ0FBQyxVQUFuQztBQUZtQixHQUE1Qjs7QUFJQSxrQkFBZ0MscUJBQVMsSUFBVCxDQUFoQztBQUFBO0FBQUEsTUFBT3NCLFFBQVA7QUFBQSxNQUFpQkMsV0FBakI7O0FBQ0EsbUJBQTRCLHFCQUFTLEVBQVQsQ0FBNUI7QUFBQTtBQUFBLE1BQU9DLE1BQVA7QUFBQSxNQUFlQyxTQUFmOztBQUVBLHdCQUFVLFlBQU07QUFDWixRQUFJSCxRQUFKLEVBQWM7QUFDVkEsTUFBQUEsUUFBUSxDQUFDSSxVQUFULENBQW9CO0FBQUVaLFFBQUFBLFdBQVcsRUFBWEE7QUFBRixPQUFwQjtBQUNIO0FBQ0osR0FKRCxFQUlHLENBQUNBLFdBQUQsQ0FKSDtBQU1BLHdCQUFVLFlBQU07QUFDWixRQUFJUSxRQUFKLEVBQWM7QUFDVkEsTUFBQUEsUUFBUSxDQUFDSSxVQUFULENBQW9CO0FBQUV6QixRQUFBQSxZQUFZLEVBQVpBO0FBQUYsT0FBcEI7QUFDSDtBQUNKLEdBSkQsRUFJRyxDQUFDQSxZQUFELENBSkg7QUFNQSx3QkFBVSxZQUFNO0FBQ1owQixJQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWUosTUFBWixFQUFvQkssT0FBcEIsQ0FBNEIsVUFBQ0MsR0FBRCxFQUFTO0FBQ2pDTixNQUFBQSxNQUFNLENBQUNNLEdBQUQsQ0FBTixDQUFZQyxtQkFBWixDQUFnQ3hCLFVBQWhDO0FBQ0gsS0FGRDtBQUdILEdBSkQsRUFJRyxDQUFDQSxVQUFELENBSkg7QUFNQSx3QkFBVSxZQUFNO0FBQ1osUUFBSWUsUUFBSixFQUFjO0FBQ1YsVUFBSWQsaUJBQUosRUFBdUI7QUFDbkJ3QixRQUFBQSxhQUFhLENBQUNDLE1BQWQsQ0FBcUJYLFFBQXJCO0FBQ0gsT0FGRCxNQUVPO0FBQ0hVLFFBQUFBLGFBQWEsQ0FBQ0MsTUFBZCxDQUFxQixJQUFyQjtBQUNIO0FBQ0o7QUFDSixHQVJELEVBUUcsQ0FBQ3pCLGlCQUFELENBUkg7QUFVQSx3QkFBVSxZQUFNO0FBQ1ptQixJQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWUosTUFBWixFQUFvQkssT0FBcEIsQ0FBNEIsVUFBQ0MsR0FBRCxFQUFTO0FBQ2pDTixNQUFBQSxNQUFNLENBQUNNLEdBQUQsQ0FBTixDQUFZSixVQUFaLENBQXVCO0FBQ25CUSxRQUFBQSxTQUFTLEVBQUVoQztBQURRLE9BQXZCO0FBR0gsS0FKRDtBQUtILEdBTkQsRUFNRyxDQUFDQSxnQkFBRCxDQU5IO0FBUUEsd0JBQVUsWUFBTTtBQUNaeUIsSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlKLE1BQVosRUFBb0JLLE9BQXBCLENBQTRCLFVBQUNDLEdBQUQsRUFBUztBQUNqQ04sTUFBQUEsTUFBTSxDQUFDTSxHQUFELENBQU4sQ0FBWUosVUFBWixDQUF1QjtBQUNuQlMsUUFBQUEsV0FBVyxFQUFFOUI7QUFETSxPQUF2QjtBQUdILEtBSkQ7QUFLSCxHQU5ELEVBTUcsQ0FBQ0Esa0JBQUQsQ0FOSDtBQVFBLHdCQUFVLFlBQU07QUFDWnNCLElBQUFBLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZSixNQUFaLEVBQW9CSyxPQUFwQixDQUE0QixVQUFDQyxHQUFELEVBQVM7QUFDakNOLE1BQUFBLE1BQU0sQ0FBQ00sR0FBRCxDQUFOLENBQVlNLHdCQUFaLENBQXFDM0Isb0JBQXJDO0FBQ0gsS0FGRDtBQUdILEdBSkQsRUFJRyxDQUFDQSxvQkFBRCxDQUpIO0FBTUEsd0JBQVUsWUFBTTtBQUNaa0IsSUFBQUEsTUFBTSxDQUFDQyxJQUFQLENBQVlKLE1BQVosRUFBb0JLLE9BQXBCLENBQTRCLFVBQUNDLEdBQUQsRUFBUztBQUNqQ04sTUFBQUEsTUFBTSxDQUFDTSxHQUFELENBQU4sQ0FBWU8sMEJBQVosQ0FBdUMxQixzQkFBdkM7QUFDSCxLQUZEO0FBR0gsR0FKRCxFQUlHLENBQUNBLHNCQUFELENBSkg7O0FBTUEsbUJBQXdCLHFCQUNwQixJQUFJTSxNQUFNLENBQUNxQixJQUFQLENBQVlDLE1BQWhCLENBQXVCO0FBQ25CQyxJQUFBQSxLQUFLLEVBQUUsa0JBQUl4QyxlQUFKLEVBQXFCLFNBQXJCLEVBQWdDLEVBQWhDLENBRFk7QUFFbkJ5QyxJQUFBQSxJQUFJLEVBQUUxQixDQUFDLENBQUMsVUFBRCxDQUZZO0FBR25CMkIsSUFBQUEsUUFBUSxFQUFFLElBQUl6QixNQUFNLENBQUNxQixJQUFQLENBQVlLLE1BQWhCLENBQ050QixtQkFBbUIsQ0FBQ0YsR0FEZCxFQUVORSxtQkFBbUIsQ0FBQ0QsR0FGZDtBQUhTLEdBQXZCLENBRG9CLENBQXhCO0FBQUE7QUFBQSxNQUFPWSxhQUFQOztBQVdBLHdCQUNJLDhCQUNJZixNQURKLEVBRUlLLFFBRkosRUFHSVUsYUFISixFQUlJLDhCQUFrQjVCLFFBQWxCLEVBQTRCLElBQTVCLENBSkosRUFLSVAsbUJBTEosRUFNSUMsaUJBTkosRUFPSUcsWUFQSixFQVFJQyxnQkFSSixFQVNJRyxrQkFUSixFQVVJRSxVQVZKLEVBV0lDLGlCQVhKLEVBWUlDLG9CQVpKLEVBYUlFLHNCQWJKLEVBY0ljLFNBZEosRUFlSVgsV0FmSixDQURKLEVBa0JJLENBQUNRLFFBQUQsQ0FsQko7QUFxQkEsc0JBQ0kscUJBQUMsT0FBRDtBQUNJLElBQUEsU0FBUyxFQUFFLENBQUMzQixTQUFELElBQWMsYUFEN0I7QUFFSSxJQUFBLE1BQU0sRUFBRUYsTUFGWjtBQUdJLElBQUEsS0FBSyxFQUFFRCxLQUhYO0FBQUEsMkJBS0kscUJBQUMsb0JBQUQ7QUFDSSxNQUFBLGlCQUFpQixFQUFFSSxpQkFEdkI7QUFFSSxNQUFBLE1BQU0sRUFBRXFCLE1BRlo7QUFHSSxNQUFBLGFBQWEsRUFBRUMsTUFIbkI7QUFJSSxNQUFBLGNBQWMsRUFBRSxLQUpwQjtBQUtJLE1BQUEsT0FBTyxFQUFDLFdBTFo7QUFNSSxNQUFBLE9BQU8sRUFBRSxpQkFBQzBCLENBQUQsRUFBSUMsR0FBSjtBQUFBLGVBQVl0QixXQUFXLENBQUNzQixHQUFELENBQXZCO0FBQUEsT0FOYjtBQU9JLE1BQUEsYUFBYSxFQUFFLEtBUG5CO0FBUUksTUFBQSxZQUFZLEVBQUU1QyxZQVJsQjtBQVNJLE1BQUEsaUJBQWlCLEVBQUUsS0FUdkI7QUFVSSxNQUFBLE1BQU0sRUFBRTZDLGdCQVZaO0FBV0ksTUFBQSxJQUFJLEVBQUVqQztBQVhWO0FBTEosSUFESjtBQXFCSCxDQTlJRDs7QUFnSkFuQixXQUFXLENBQUNxRCxTQUFaLEdBQXdCO0FBQ3BCcEQsRUFBQUEsU0FBUyxFQUFFcUQsbUJBQVVDLElBREQ7QUFFcEJyRCxFQUFBQSxpQkFBaUIsRUFBRW9ELG1CQUFVQyxJQUZUO0FBR3BCaEMsRUFBQUEsTUFBTSxFQUFFK0IsbUJBQVVFLE1BSEU7QUFJcEJyRCxFQUFBQSxtQkFBbUIsRUFBRW1ELG1CQUFVRyxJQUpYO0FBS3BCckQsRUFBQUEsaUJBQWlCLEVBQUVrRCxtQkFBVUcsSUFMVDtBQU1wQjFELEVBQUFBLE1BQU0sRUFBRXVELG1CQUFVSSxNQU5FO0FBT3BCckQsRUFBQUEsVUFBVSxFQUFFaUQsbUJBQVVFLE1BUEY7QUFRcEJsRCxFQUFBQSxlQUFlLEVBQUVnRCxtQkFBVUUsTUFSUDtBQVNwQmpELEVBQUFBLFlBQVksRUFBRStDLG1CQUFVQyxJQVRKO0FBVXBCL0MsRUFBQUEsZ0JBQWdCLEVBQUU4QyxtQkFBVUksTUFWUjtBQVdwQmhELEVBQUFBLFFBQVEsRUFBRTRDLG1CQUFVSyxLQVhBO0FBWXBCQyxFQUFBQSxrQkFBa0IsRUFBRU4sbUJBQVVLLEtBWlY7QUFhcEJoRCxFQUFBQSxrQkFBa0IsRUFBRTJDLG1CQUFVSSxNQWJWO0FBY3BCN0MsRUFBQUEsVUFBVSxFQUFFeUMsbUJBQVVDLElBZEY7QUFlcEJ6QyxFQUFBQSxpQkFBaUIsRUFBRXdDLG1CQUFVQyxJQWZUO0FBZ0JwQnhDLEVBQUFBLG9CQUFvQixFQUFFdUMsbUJBQVVJLE1BaEJaO0FBaUJwQnpDLEVBQUFBLHNCQUFzQixFQUFFcUMsbUJBQVVJLE1BakJkO0FBa0JwQjVELEVBQUFBLEtBQUssRUFBRXdELG1CQUFVSSxNQWxCRztBQW1CcEJ2QyxFQUFBQSxJQUFJLEVBQUVtQyxtQkFBVU8sTUFuQkk7QUFvQnBCekMsRUFBQUEsV0FBVyxFQUFFa0MsbUJBQVVDO0FBcEJILENBQXhCO2VBdUJldkQsVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1hcCB9IGZyb20gJ2dvb2dsZS1tYXBzLXJlYWN0JztcbmltcG9ydCBnZXQgZnJvbSAnbG9kYXNoL2dldCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ29udGV4dCwgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHtcbiAgICBNQVBfU1RZTEVTLFxuICAgIFNFR01FTlRfQ09MT1IsXG4gICAgU0VHTUVOVF9TVFJPS0VfQ09MT1IsXG4gICAgU09MQVJfTU9EVUxFX0NPTE9SLFxuICAgIFNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IsXG59IGZyb20gJ0Bjb25zdGFudHMvbWFwcyc7XG5cbmltcG9ydCB7IEdlbmVyYWxDb250ZXh0IH0gZnJvbSAnQGhlbHBlcnMvY29udGV4dHMnO1xuaW1wb3J0IHsgbWFwVmFsdWVVc2VFZmZlY3QgfSBmcm9tICdAaGVscGVycy9tYXBzJztcbmltcG9ydCB7IGFycmF5dG9EaWN0aW9uYXJ5IH0gZnJvbSAnQGhlbHBlcnMvdXRpbHMnO1xuXG5jb25zdCBXcmFwcGVyID0gc3R5bGVkLmRpdmBcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6ICR7KHsgd2lkdGggPSAnMTAwJScgfSkgPT4gd2lkdGh9O1xuICAgIGhlaWdodDogJHsoeyBoZWlnaHQgPSAnMTAwJScgfSkgPT4gaGVpZ2h0fTtcblxuICAgICYudW5jbGlja2FibGUge1xuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbiAgICAgICAgLmdtbm9wcmludCBhLFxuICAgICAgICAuZ21ub3ByaW50IHNwYW4sXG4gICAgICAgIC5nbS1zdHlsZS1jYyB7XG4gICAgICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgICAgICB9XG4gICAgICAgIC5nbW5vcHJpbnQgZGl2IHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDtcbiAgICAgICAgfVxuICAgIH1cbiAgICAucG9wdXAtY29udGFpbmVyIHtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICBoZWlnaHQ6IDA7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgd2lkdGg6IDIwMHB4O1xuICAgICAgICB0cmFuc2Zvcm0tb3JpZ2luOiAwJSAwJTtcbiAgICAgICAgei1pbmRleDogMTtcbiAgICAgICAgJjpob3ZlciB7XG4gICAgICAgICAgICB6LWluZGV4OiAyO1xuICAgICAgICB9XG4gICAgICAgIC5wb3B1cC1idWJibGUtYW5jaG9yIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgYm90dG9tOiA2cHg7XG4gICAgICAgICAgICBsZWZ0OiAwO1xuICAgICAgICAgICAgc3BhbiB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTNweDtcbiAgICAgICAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgICAgICAgbGVmdDogMDtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtMTAwJSk7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMjQzODtcbiAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDJweDtcbiAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgICAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDYwcHg7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiA4cHg7XG4gICAgICAgICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xuICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxM3B4O1xuICAgICAgICAgICAgICAgIHVzZXItc2VsZWN0OiBub25lO1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgJiA+IGRpdiB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG5gO1xuXG5jb25zdCBTZWdtZW50c01hcCA9ICh7XG4gICAgY2xpY2thYmxlID0gdHJ1ZSxcbiAgICBmdWxsc2NyZWVuQ29udHJvbCA9IGZhbHNlLFxuICAgIGhhbmRsZUNlbnRlckNoYW5nZWQsXG4gICAgaGFuZGxlWm9vbUNoYW5nZWQsXG4gICAgaGVpZ2h0LFxuICAgIGluaXRDZW50ZXIsXG4gICAgcHJvamVjdExvY2F0aW9uLFxuICAgIHNjYWxlQ29udHJvbCA9IHRydWUsXG4gICAgc2VnbWVudEZpbGxDb2xvciA9IFNFR01FTlRfQ09MT1IsXG4gICAgc2VnbWVudHMgPSBbXSxcbiAgICBzZWdtZW50U3Ryb2tlQ29sb3IgPSBTRUdNRU5UX1NUUk9LRV9DT0xPUixcbiAgICBzaG93TGFiZWxzID0gdHJ1ZSxcbiAgICBzaG93UHJvamVjdE1hcmtlciA9IHRydWUsXG4gICAgc29sYXJNb2R1bGVGaWxsQ29sb3IgPSBTT0xBUl9NT0RVTEVfQ09MT1IsXG4gICAgc29sYXJNb2R1bGVTdHJva2VDb2xvciA9IFNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IsXG4gICAgd2lkdGgsXG4gICAgem9vbSA9IDE4LFxuICAgIHpvb21Db250cm9sID0gdHJ1ZSxcbn0pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3QgeyBnb29nbGUgfSA9IHVzZUNvbnRleHQoR2VuZXJhbENvbnRleHQpO1xuICAgIGNvbnN0IGNlbnRlciA9IHtcbiAgICAgICAgbGF0OiBnZXQoaW5pdENlbnRlciwgJ2xhdGl0dWRlJywgMjAuOTY2NDI4MSksXG4gICAgICAgIGxuZzogZ2V0KGluaXRDZW50ZXIsICdsb25naXR1ZGUnLCAtODkuNjIzOTMzNiksXG4gICAgfTtcbiAgICBjb25zdCBwcm9qZWN0TWFya2VyQ2VudGVyID0ge1xuICAgICAgICBsYXQ6IGdldChwcm9qZWN0TG9jYXRpb24sICdsYXRpdHVkZScsIDIwLjk2NjQyODEpLFxuICAgICAgICBsbmc6IGdldChwcm9qZWN0TG9jYXRpb24sICdsb25naXR1ZGUnLCAtODkuNjIzOTMzNiksXG4gICAgfTtcbiAgICBjb25zdCBbbWFwVmFsdWUsIHNldE1hcFZhbHVlXSA9IHVzZVN0YXRlKG51bGwpO1xuICAgIGNvbnN0IFtzaGFwZXMsIHNldFNoYXBlc10gPSB1c2VTdGF0ZSh7fSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAobWFwVmFsdWUpIHtcbiAgICAgICAgICAgIG1hcFZhbHVlLnNldE9wdGlvbnMoeyB6b29tQ29udHJvbCB9KTtcbiAgICAgICAgfVxuICAgIH0sIFt6b29tQ29udHJvbF0pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaWYgKG1hcFZhbHVlKSB7XG4gICAgICAgICAgICBtYXBWYWx1ZS5zZXRPcHRpb25zKHsgc2NhbGVDb250cm9sIH0pO1xuICAgICAgICB9XG4gICAgfSwgW3NjYWxlQ29udHJvbF0pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgT2JqZWN0LmtleXMoc2hhcGVzKS5mb3JFYWNoKChrZXkpID0+IHtcbiAgICAgICAgICAgIHNoYXBlc1trZXldLnNldExhYmVsc1Zpc2liaWxpdHkoc2hvd0xhYmVscyk7XG4gICAgICAgIH0pO1xuICAgIH0sIFtzaG93TGFiZWxzXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAobWFwVmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChzaG93UHJvamVjdE1hcmtlcikge1xuICAgICAgICAgICAgICAgIHByb2plY3RNYXJrZXIuc2V0TWFwKG1hcFZhbHVlKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcHJvamVjdE1hcmtlci5zZXRNYXAobnVsbCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCBbc2hvd1Byb2plY3RNYXJrZXJdKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIE9iamVjdC5rZXlzKHNoYXBlcykuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICBzaGFwZXNba2V5XS5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICBmaWxsQ29sb3I6IHNlZ21lbnRGaWxsQ29sb3IsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSwgW3NlZ21lbnRGaWxsQ29sb3JdKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIE9iamVjdC5rZXlzKHNoYXBlcykuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICBzaGFwZXNba2V5XS5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICBzdHJva2VDb2xvcjogc2VnbWVudFN0cm9rZUNvbG9yLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH0sIFtzZWdtZW50U3Ryb2tlQ29sb3JdKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIE9iamVjdC5rZXlzKHNoYXBlcykuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICBzaGFwZXNba2V5XS5zZXRTb2xhck1vZHVsZXNGaWxsQ29sb3Ioc29sYXJNb2R1bGVGaWxsQ29sb3IpO1xuICAgICAgICB9KTtcbiAgICB9LCBbc29sYXJNb2R1bGVGaWxsQ29sb3JdKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIE9iamVjdC5rZXlzKHNoYXBlcykuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICBzaGFwZXNba2V5XS5zZXRTb2xhck1vZHVsZXNTdHJva2VDb2xvcihzb2xhck1vZHVsZVN0cm9rZUNvbG9yKTtcbiAgICAgICAgfSk7XG4gICAgfSwgW3NvbGFyTW9kdWxlU3Ryb2tlQ29sb3JdKTtcblxuICAgIGNvbnN0IFtwcm9qZWN0TWFya2VyXSA9IHVzZVN0YXRlKFxuICAgICAgICBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtcbiAgICAgICAgICAgIHRpdGxlOiBnZXQocHJvamVjdExvY2F0aW9uLCAnYWRkcmVzcycsICcnKSxcbiAgICAgICAgICAgIG5hbWU6IHQoJ0xvY2F0aW9uJyksXG4gICAgICAgICAgICBwb3NpdGlvbjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhcbiAgICAgICAgICAgICAgICBwcm9qZWN0TWFya2VyQ2VudGVyLmxhdCxcbiAgICAgICAgICAgICAgICBwcm9qZWN0TWFya2VyQ2VudGVyLmxuZ1xuICAgICAgICAgICAgKSxcbiAgICAgICAgfSlcbiAgICApO1xuXG4gICAgdXNlRWZmZWN0KFxuICAgICAgICBtYXBWYWx1ZVVzZUVmZmVjdChcbiAgICAgICAgICAgIGdvb2dsZSxcbiAgICAgICAgICAgIG1hcFZhbHVlLFxuICAgICAgICAgICAgcHJvamVjdE1hcmtlcixcbiAgICAgICAgICAgIGFycmF5dG9EaWN0aW9uYXJ5KHNlZ21lbnRzLCAnaWQnKSxcbiAgICAgICAgICAgIGhhbmRsZUNlbnRlckNoYW5nZWQsXG4gICAgICAgICAgICBoYW5kbGVab29tQ2hhbmdlZCxcbiAgICAgICAgICAgIHNjYWxlQ29udHJvbCxcbiAgICAgICAgICAgIHNlZ21lbnRGaWxsQ29sb3IsXG4gICAgICAgICAgICBzZWdtZW50U3Ryb2tlQ29sb3IsXG4gICAgICAgICAgICBzaG93TGFiZWxzLFxuICAgICAgICAgICAgc2hvd1Byb2plY3RNYXJrZXIsXG4gICAgICAgICAgICBzb2xhck1vZHVsZUZpbGxDb2xvcixcbiAgICAgICAgICAgIHNvbGFyTW9kdWxlU3Ryb2tlQ29sb3IsXG4gICAgICAgICAgICBzZXRTaGFwZXMsXG4gICAgICAgICAgICB6b29tQ29udHJvbFxuICAgICAgICApLFxuICAgICAgICBbbWFwVmFsdWVdXG4gICAgKTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxXcmFwcGVyXG4gICAgICAgICAgICBjbGFzc05hbWU9eyFjbGlja2FibGUgJiYgJ3VuY2xpY2thYmxlJ31cbiAgICAgICAgICAgIGhlaWdodD17aGVpZ2h0fVxuICAgICAgICAgICAgd2lkdGg9e3dpZHRofVxuICAgICAgICA+XG4gICAgICAgICAgICA8TWFwXG4gICAgICAgICAgICAgICAgZnVsbHNjcmVlbkNvbnRyb2w9e2Z1bGxzY3JlZW5Db250cm9sfVxuICAgICAgICAgICAgICAgIGdvb2dsZT17Z29vZ2xlfVxuICAgICAgICAgICAgICAgIGluaXRpYWxDZW50ZXI9e2NlbnRlcn1cbiAgICAgICAgICAgICAgICBtYXBUeXBlQ29udHJvbD17ZmFsc2V9XG4gICAgICAgICAgICAgICAgbWFwVHlwZT1cInNhdGVsbGl0ZVwiXG4gICAgICAgICAgICAgICAgb25SZWFkeT17KHAsIG1hcCkgPT4gc2V0TWFwVmFsdWUobWFwKX1cbiAgICAgICAgICAgICAgICByb3RhdGVDb250cm9sPXtmYWxzZX1cbiAgICAgICAgICAgICAgICBzY2FsZUNvbnRyb2w9e3NjYWxlQ29udHJvbH1cbiAgICAgICAgICAgICAgICBzdHJlZXRWaWV3Q29udHJvbD17ZmFsc2V9XG4gICAgICAgICAgICAgICAgc3R5bGVzPXtNQVBfU1RZTEVTfVxuICAgICAgICAgICAgICAgIHpvb209e3pvb219XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L1dyYXBwZXI+XG4gICAgKTtcbn07XG5cblNlZ21lbnRzTWFwLnByb3BUeXBlcyA9IHtcbiAgICBjbGlja2FibGU6IFByb3BUeXBlcy5ib29sLFxuICAgIGZ1bGxzY3JlZW5Db250cm9sOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBnb29nbGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaGFuZGxlQ2VudGVyQ2hhbmdlZDogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaGFuZGxlWm9vbUNoYW5nZWQ6IFByb3BUeXBlcy5mdW5jLFxuICAgIGhlaWdodDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpbml0Q2VudGVyOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHByb2plY3RMb2NhdGlvbjogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBzY2FsZUNvbnRyb2w6IFByb3BUeXBlcy5ib29sLFxuICAgIHNlZ21lbnRGaWxsQ29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2VnbWVudHM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBzZWdtZW50c0RpY3Rpb25hcnk6IFByb3BUeXBlcy5hcnJheSxcbiAgICBzZWdtZW50U3Ryb2tlQ29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2hvd0xhYmVsczogUHJvcFR5cGVzLmJvb2wsXG4gICAgc2hvd1Byb2plY3RNYXJrZXI6IFByb3BUeXBlcy5ib29sLFxuICAgIHNvbGFyTW9kdWxlRmlsbENvbG9yOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNvbGFyTW9kdWxlU3Ryb2tlQ29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgd2lkdGg6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgem9vbTogUHJvcFR5cGVzLm51bWJlcixcbiAgICB6b29tQ29udHJvbDogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBTZWdtZW50c01hcDtcbiJdfQ==