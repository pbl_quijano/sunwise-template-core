"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactApexcharts = _interopRequireDefault(require("react-apexcharts"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _template2 = require("../../../../helpers/template");

var _utils = require("../../../../helpers/utils");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Styled = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    &.annotation-label {\n        background-color: red;\n        margin-bottom: 5px;\n    }\n\n    .appexchart-content,\n    .apexcharts-canvas,\n    .apexcharts-canvas svg {\n        height: ", ";\n        width: ", ";\n    }\n"])), function (_ref) {
  var innerHeight = _ref.innerHeight;
  return "".concat(innerHeight, "px!important");
}, function (_ref2) {
  var innerWidth = _ref2.innerWidth;
  return "".concat(innerWidth, "px!important");
});

var getColors = function getColors(type, stacked_bar, series) {
  if (type !== 'bar' || !stacked_bar || !stacked_bar.visible) {
    return series.map(function (serie) {
      return serie.color || '#ef0d0d';
    });
  }

  return (0, _template2.buildGraphsColors)({
    type: type,
    stacked_bar: stacked_bar
  });
};

var ChartView = function ChartView(_ref3) {
  var currencyIso = _ref3.currencyIso,
      currencyLocale = _ref3.currencyLocale,
      _ref3$data = _ref3.data,
      data = _ref3$data === void 0 ? {} : _ref3$data,
      innerHeight = _ref3.innerHeight,
      innerWidth = _ref3.innerWidth;
  var annotationsData = data.annotations,
      categories = data.categories,
      _data$finishCategoryI = data.finishCategoryIndex,
      finishCategoryIndex = _data$finishCategoryI === void 0 ? data.categories.length - 1 : _data$finishCategoryI,
      _data$formatY = data.formatY,
      formatY = _data$formatY === void 0 ? {} : _data$formatY,
      _data$initCategoryInd = data.initCategoryIndex,
      initCategoryIndex = _data$initCategoryInd === void 0 ? 0 : _data$initCategoryInd,
      seriesData = data.series,
      stacked_bar = data.stacked_bar,
      type = data.type;

  var _useState = (0, _react.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      chartLoaded = _useState2[0],
      setChartLoaded = _useState2[1];

  (0, _react.useEffect)(function () {
    if (!chartLoaded) setChartLoaded(true);
  }, [chartLoaded]);
  (0, _react.useEffect)(function () {
    setChartLoaded(false);
  }, [type, stacked_bar, seriesData]);
  if (!chartLoaded) return null;
  var series = seriesData.filter(function (serie) {
    return serie.visible !== false;
  });
  var colors = getColors(type, stacked_bar, series);

  var getFormattingY = function getFormattingY(value) {
    var CURRENCY = _template.FORMAT_Y_TYPES.CURRENCY,
        DECIMAL_2 = _template.FORMAT_Y_TYPES.DECIMAL_2,
        DECIMAL_4 = _template.FORMAT_Y_TYPES.DECIMAL_4;

    switch (formatY.type) {
      case CURRENCY:
        return (0, _utils.numberFormat)(value, {
          currency: currencyIso,
          locale: currencyLocale,
          style: 'currency'
        });

      case DECIMAL_2:
        return (0, _utils.numberFormat)(value, {
          locale: currencyLocale,
          style: 'decimal',
          unit: formatY.unit
        });

      case DECIMAL_4:
        return (0, _utils.numberFormat)(value, {
          decimals: 4,
          locale: currencyLocale,
          style: 'decimal',
          unit: formatY.unit
        });

      default:
        return (0, _utils.numberFormat)(value, {
          decimals: 0,
          locale: currencyLocale,
          style: 'decimal',
          unit: formatY.unit
        });
    }
  };

  var getSeries = function getSeries() {
    if (type !== 'bar' || !stacked_bar || !stacked_bar.visible) {
      var seriesType = series.reduce(function (acc, current) {
        acc[current.type] = 0;
        return acc;
      }, {});
      return series.map(function (serie) {
        return _objectSpread(_objectSpread({}, serie), {}, {
          name: serie.editable_label || serie.name,
          type: Object.keys(seriesType).length > 1 ? serie.type : undefined,
          data: serie.data.filter(function (c, index) {
            return index >= initCategoryIndex && index <= finishCategoryIndex;
          })
        });
      });
    }

    var data = stacked_bar.data,
        selected = stacked_bar.selected;
    var dataRange = data.filter(function (c, index) {
      return index >= initCategoryIndex && index <= finishCategoryIndex;
    });
    var arrayDefault = new Array(dataRange.length).fill(0);
    var dataDictionary = dataRange.reduce(function (acc, current, index) {
      var tempAcc = _objectSpread({}, acc);

      var parseItem = JSON.parse(current[selected]);

      if (parseItem) {
        Object.keys(parseItem).forEach(function (key) {
          if ((0, _isNil.default)(tempAcc[key])) {
            tempAcc[key] = _toConsumableArray(arrayDefault);
          }

          tempAcc[key][index] = parseItem[key];
        });
      }

      return _objectSpread(_objectSpread({}, acc), tempAcc);
    }, {});
    return Object.keys(dataDictionary).map(function (key) {
      return {
        name: key,
        data: dataDictionary[key]
      };
    });
  };

  var annotations = annotationsData ? annotationsData.filter(function (annotation) {
    return annotation.visible !== false;
  }).map(function (annotation) {
    return {
      y: annotation.value,
      borderColor: annotation.color,
      strokeDashArray: 0,
      label: {
        borderColor: annotation.color,
        style: {
          cssClass: 'annotation-label',
          color: '#FFF',
          background: annotation.color,
          padding: {
            top: 5,
            left: 5,
            bottom: 5,
            right: 5
          }
        },
        text: "".concat(annotation.editable_label || annotation.label, " ").concat((0, _utils.numberFormat)(annotation.value, {
          currency: currencyIso,
          locale: currencyLocale,
          style: 'currency'
        }))
      }
    };
  }) : [];
  var contentHeight = innerHeight - 20;
  var chartConfig = {
    options: {
      annotations: {
        yaxis: annotations
      },
      colors: colors,
      dataLabels: {
        enabled: false
      },
      legend: {
        show: true
      },
      xaxis: {
        categories: categories.filter(function (c, index) {
          return index >= initCategoryIndex && index <= finishCategoryIndex;
        })
      },
      yaxis: {
        labels: {
          formatter: getFormattingY
        }
      },
      chart: {
        height: contentHeight,
        id: 'apexchart',
        stacked: stacked_bar && stacked_bar.visible && type === 'bar',
        toolbar: {
          show: false
        },
        type: type,
        width: innerWidth
      },
      fill: {
        colors: colors
      },
      stroke: {
        colors: colors
      }
    },
    series: getSeries()
  };
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Styled, {
    innerHeight: contentHeight,
    innerWidth: innerWidth,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactApexcharts.default, {
      className: "appexchart-content",
      height: "".concat(contentHeight),
      width: "".concat(innerWidth),
      options: chartConfig.options,
      series: chartConfig.series || [],
      type: type || 'bar'
    })
  });
};

ChartView.propTypes = {
  currencyIso: _propTypes.default.string,
  currencyLocale: _propTypes.default.string,
  data: _propTypes.default.object,
  defaultCurrency: _propTypes.default.object,
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  isCountryCurrencyRequired: _propTypes.default.bool
};
var _default = ChartView;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvQ2hhcnRWaWV3LmpzIl0sIm5hbWVzIjpbIlN0eWxlZCIsInN0eWxlZCIsImRpdiIsImlubmVySGVpZ2h0IiwiaW5uZXJXaWR0aCIsImdldENvbG9ycyIsInR5cGUiLCJzdGFja2VkX2JhciIsInNlcmllcyIsInZpc2libGUiLCJtYXAiLCJzZXJpZSIsImNvbG9yIiwiQ2hhcnRWaWV3IiwiY3VycmVuY3lJc28iLCJjdXJyZW5jeUxvY2FsZSIsImRhdGEiLCJhbm5vdGF0aW9uc0RhdGEiLCJhbm5vdGF0aW9ucyIsImNhdGVnb3JpZXMiLCJmaW5pc2hDYXRlZ29yeUluZGV4IiwibGVuZ3RoIiwiZm9ybWF0WSIsImluaXRDYXRlZ29yeUluZGV4Iiwic2VyaWVzRGF0YSIsImNoYXJ0TG9hZGVkIiwic2V0Q2hhcnRMb2FkZWQiLCJmaWx0ZXIiLCJjb2xvcnMiLCJnZXRGb3JtYXR0aW5nWSIsInZhbHVlIiwiQ1VSUkVOQ1kiLCJGT1JNQVRfWV9UWVBFUyIsIkRFQ0lNQUxfMiIsIkRFQ0lNQUxfNCIsImN1cnJlbmN5IiwibG9jYWxlIiwic3R5bGUiLCJ1bml0IiwiZGVjaW1hbHMiLCJnZXRTZXJpZXMiLCJzZXJpZXNUeXBlIiwicmVkdWNlIiwiYWNjIiwiY3VycmVudCIsIm5hbWUiLCJlZGl0YWJsZV9sYWJlbCIsIk9iamVjdCIsImtleXMiLCJ1bmRlZmluZWQiLCJjIiwiaW5kZXgiLCJzZWxlY3RlZCIsImRhdGFSYW5nZSIsImFycmF5RGVmYXVsdCIsIkFycmF5IiwiZmlsbCIsImRhdGFEaWN0aW9uYXJ5IiwidGVtcEFjYyIsInBhcnNlSXRlbSIsIkpTT04iLCJwYXJzZSIsImZvckVhY2giLCJrZXkiLCJhbm5vdGF0aW9uIiwieSIsImJvcmRlckNvbG9yIiwic3Ryb2tlRGFzaEFycmF5IiwibGFiZWwiLCJjc3NDbGFzcyIsImJhY2tncm91bmQiLCJwYWRkaW5nIiwidG9wIiwibGVmdCIsImJvdHRvbSIsInJpZ2h0IiwidGV4dCIsImNvbnRlbnRIZWlnaHQiLCJjaGFydENvbmZpZyIsIm9wdGlvbnMiLCJ5YXhpcyIsImRhdGFMYWJlbHMiLCJlbmFibGVkIiwibGVnZW5kIiwic2hvdyIsInhheGlzIiwibGFiZWxzIiwiZm9ybWF0dGVyIiwiY2hhcnQiLCJoZWlnaHQiLCJpZCIsInN0YWNrZWQiLCJ0b29sYmFyIiwid2lkdGgiLCJzdHJva2UiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJvYmplY3QiLCJkZWZhdWx0Q3VycmVuY3kiLCJudW1iZXIiLCJpc0NvdW50cnlDdXJyZW5jeVJlcXVpcmVkIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxNQUFNLEdBQUdDLDBCQUFPQyxHQUFWLDZTQVNNO0FBQUEsTUFBR0MsV0FBSCxRQUFHQSxXQUFIO0FBQUEsbUJBQXdCQSxXQUF4QjtBQUFBLENBVE4sRUFVSztBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLG1CQUF1QkEsVUFBdkI7QUFBQSxDQVZMLENBQVo7O0FBY0EsSUFBTUMsU0FBUyxHQUFHLFNBQVpBLFNBQVksQ0FBQ0MsSUFBRCxFQUFPQyxXQUFQLEVBQW9CQyxNQUFwQixFQUErQjtBQUM3QyxNQUFJRixJQUFJLEtBQUssS0FBVCxJQUFrQixDQUFDQyxXQUFuQixJQUFrQyxDQUFDQSxXQUFXLENBQUNFLE9BQW5ELEVBQTREO0FBQ3hELFdBQU9ELE1BQU0sQ0FBQ0UsR0FBUCxDQUFXLFVBQUNDLEtBQUQ7QUFBQSxhQUFXQSxLQUFLLENBQUNDLEtBQU4sSUFBZSxTQUExQjtBQUFBLEtBQVgsQ0FBUDtBQUNIOztBQUVELFNBQU8sa0NBQWtCO0FBQUVOLElBQUFBLElBQUksRUFBSkEsSUFBRjtBQUFRQyxJQUFBQSxXQUFXLEVBQVhBO0FBQVIsR0FBbEIsQ0FBUDtBQUNILENBTkQ7O0FBUUEsSUFBTU0sU0FBUyxHQUFHLFNBQVpBLFNBQVksUUFNWjtBQUFBLE1BTEZDLFdBS0UsU0FMRkEsV0FLRTtBQUFBLE1BSkZDLGNBSUUsU0FKRkEsY0FJRTtBQUFBLHlCQUhGQyxJQUdFO0FBQUEsTUFIRkEsSUFHRSwyQkFISyxFQUdMO0FBQUEsTUFGRmIsV0FFRSxTQUZGQSxXQUVFO0FBQUEsTUFERkMsVUFDRSxTQURGQSxVQUNFO0FBQ0YsTUFDaUJhLGVBRGpCLEdBU0lELElBVEosQ0FDSUUsV0FESjtBQUFBLE1BRUlDLFVBRkosR0FTSUgsSUFUSixDQUVJRyxVQUZKO0FBQUEsOEJBU0lILElBVEosQ0FHSUksbUJBSEo7QUFBQSxNQUdJQSxtQkFISixzQ0FHMEJKLElBQUksQ0FBQ0csVUFBTCxDQUFnQkUsTUFBaEIsR0FBeUIsQ0FIbkQ7QUFBQSxzQkFTSUwsSUFUSixDQUlJTSxPQUpKO0FBQUEsTUFJSUEsT0FKSiw4QkFJYyxFQUpkO0FBQUEsOEJBU0lOLElBVEosQ0FLSU8saUJBTEo7QUFBQSxNQUtJQSxpQkFMSixzQ0FLd0IsQ0FMeEI7QUFBQSxNQU1ZQyxVQU5aLEdBU0lSLElBVEosQ0FNSVIsTUFOSjtBQUFBLE1BT0lELFdBUEosR0FTSVMsSUFUSixDQU9JVCxXQVBKO0FBQUEsTUFRSUQsSUFSSixHQVNJVSxJQVRKLENBUUlWLElBUko7O0FBVUEsa0JBQXNDLHFCQUFTLElBQVQsQ0FBdEM7QUFBQTtBQUFBLE1BQU9tQixXQUFQO0FBQUEsTUFBb0JDLGNBQXBCOztBQUVBLHdCQUFVLFlBQU07QUFDWixRQUFJLENBQUNELFdBQUwsRUFBa0JDLGNBQWMsQ0FBQyxJQUFELENBQWQ7QUFDckIsR0FGRCxFQUVHLENBQUNELFdBQUQsQ0FGSDtBQUlBLHdCQUFVLFlBQU07QUFDWkMsSUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDtBQUNILEdBRkQsRUFFRyxDQUFDcEIsSUFBRCxFQUFPQyxXQUFQLEVBQW9CaUIsVUFBcEIsQ0FGSDtBQUlBLE1BQUksQ0FBQ0MsV0FBTCxFQUFrQixPQUFPLElBQVA7QUFFbEIsTUFBTWpCLE1BQU0sR0FBR2dCLFVBQVUsQ0FBQ0csTUFBWCxDQUFrQixVQUFDaEIsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ0YsT0FBTixLQUFrQixLQUE3QjtBQUFBLEdBQWxCLENBQWY7QUFDQSxNQUFNbUIsTUFBTSxHQUFHdkIsU0FBUyxDQUFDQyxJQUFELEVBQU9DLFdBQVAsRUFBb0JDLE1BQXBCLENBQXhCOztBQUVBLE1BQU1xQixjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLEtBQUQsRUFBVztBQUM5QixRQUFRQyxRQUFSLEdBQTJDQyx3QkFBM0MsQ0FBUUQsUUFBUjtBQUFBLFFBQWtCRSxTQUFsQixHQUEyQ0Qsd0JBQTNDLENBQWtCQyxTQUFsQjtBQUFBLFFBQTZCQyxTQUE3QixHQUEyQ0Ysd0JBQTNDLENBQTZCRSxTQUE3Qjs7QUFFQSxZQUFRWixPQUFPLENBQUNoQixJQUFoQjtBQUNJLFdBQUt5QixRQUFMO0FBQ0ksZUFBTyx5QkFBYUQsS0FBYixFQUFvQjtBQUN2QkssVUFBQUEsUUFBUSxFQUFFckIsV0FEYTtBQUV2QnNCLFVBQUFBLE1BQU0sRUFBRXJCLGNBRmU7QUFHdkJzQixVQUFBQSxLQUFLLEVBQUU7QUFIZ0IsU0FBcEIsQ0FBUDs7QUFLSixXQUFLSixTQUFMO0FBQ0ksZUFBTyx5QkFBYUgsS0FBYixFQUFvQjtBQUN2Qk0sVUFBQUEsTUFBTSxFQUFFckIsY0FEZTtBQUV2QnNCLFVBQUFBLEtBQUssRUFBRSxTQUZnQjtBQUd2QkMsVUFBQUEsSUFBSSxFQUFFaEIsT0FBTyxDQUFDZ0I7QUFIUyxTQUFwQixDQUFQOztBQUtKLFdBQUtKLFNBQUw7QUFDSSxlQUFPLHlCQUFhSixLQUFiLEVBQW9CO0FBQ3ZCUyxVQUFBQSxRQUFRLEVBQUUsQ0FEYTtBQUV2QkgsVUFBQUEsTUFBTSxFQUFFckIsY0FGZTtBQUd2QnNCLFVBQUFBLEtBQUssRUFBRSxTQUhnQjtBQUl2QkMsVUFBQUEsSUFBSSxFQUFFaEIsT0FBTyxDQUFDZ0I7QUFKUyxTQUFwQixDQUFQOztBQU1KO0FBQ0ksZUFBTyx5QkFBYVIsS0FBYixFQUFvQjtBQUN2QlMsVUFBQUEsUUFBUSxFQUFFLENBRGE7QUFFdkJILFVBQUFBLE1BQU0sRUFBRXJCLGNBRmU7QUFHdkJzQixVQUFBQSxLQUFLLEVBQUUsU0FIZ0I7QUFJdkJDLFVBQUFBLElBQUksRUFBRWhCLE9BQU8sQ0FBQ2dCO0FBSlMsU0FBcEIsQ0FBUDtBQXJCUjtBQTRCSCxHQS9CRDs7QUFpQ0EsTUFBTUUsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBTTtBQUNwQixRQUFJbEMsSUFBSSxLQUFLLEtBQVQsSUFBa0IsQ0FBQ0MsV0FBbkIsSUFBa0MsQ0FBQ0EsV0FBVyxDQUFDRSxPQUFuRCxFQUE0RDtBQUN4RCxVQUFNZ0MsVUFBVSxHQUFHakMsTUFBTSxDQUFDa0MsTUFBUCxDQUFjLFVBQUNDLEdBQUQsRUFBTUMsT0FBTixFQUFrQjtBQUMvQ0QsUUFBQUEsR0FBRyxDQUFDQyxPQUFPLENBQUN0QyxJQUFULENBQUgsR0FBb0IsQ0FBcEI7QUFDQSxlQUFPcUMsR0FBUDtBQUNILE9BSGtCLEVBR2hCLEVBSGdCLENBQW5CO0FBS0EsYUFBT25DLE1BQU0sQ0FBQ0UsR0FBUCxDQUFXLFVBQUNDLEtBQUQ7QUFBQSwrQ0FDWEEsS0FEVztBQUVka0MsVUFBQUEsSUFBSSxFQUFFbEMsS0FBSyxDQUFDbUMsY0FBTixJQUF3Qm5DLEtBQUssQ0FBQ2tDLElBRnRCO0FBR2R2QyxVQUFBQSxJQUFJLEVBQ0F5QyxNQUFNLENBQUNDLElBQVAsQ0FBWVAsVUFBWixFQUF3QnBCLE1BQXhCLEdBQWlDLENBQWpDLEdBQXFDVixLQUFLLENBQUNMLElBQTNDLEdBQWtEMkMsU0FKeEM7QUFLZGpDLFVBQUFBLElBQUksRUFBRUwsS0FBSyxDQUFDSyxJQUFOLENBQVdXLE1BQVgsQ0FDRixVQUFDdUIsQ0FBRCxFQUFJQyxLQUFKO0FBQUEsbUJBQ0lBLEtBQUssSUFBSTVCLGlCQUFULElBQ0E0QixLQUFLLElBQUkvQixtQkFGYjtBQUFBLFdBREU7QUFMUTtBQUFBLE9BQVgsQ0FBUDtBQVdIOztBQUVELFFBQVFKLElBQVIsR0FBMkJULFdBQTNCLENBQVFTLElBQVI7QUFBQSxRQUFjb0MsUUFBZCxHQUEyQjdDLFdBQTNCLENBQWM2QyxRQUFkO0FBQ0EsUUFBTUMsU0FBUyxHQUFHckMsSUFBSSxDQUFDVyxNQUFMLENBQ2QsVUFBQ3VCLENBQUQsRUFBSUMsS0FBSjtBQUFBLGFBQ0lBLEtBQUssSUFBSTVCLGlCQUFULElBQThCNEIsS0FBSyxJQUFJL0IsbUJBRDNDO0FBQUEsS0FEYyxDQUFsQjtBQUlBLFFBQU1rQyxZQUFZLEdBQUcsSUFBSUMsS0FBSixDQUFVRixTQUFTLENBQUNoQyxNQUFwQixFQUE0Qm1DLElBQTVCLENBQWlDLENBQWpDLENBQXJCO0FBQ0EsUUFBTUMsY0FBYyxHQUFHSixTQUFTLENBQUNYLE1BQVYsQ0FBaUIsVUFBQ0MsR0FBRCxFQUFNQyxPQUFOLEVBQWVPLEtBQWYsRUFBeUI7QUFDN0QsVUFBSU8sT0FBTyxxQkFBUWYsR0FBUixDQUFYOztBQUNBLFVBQU1nQixTQUFTLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXakIsT0FBTyxDQUFDUSxRQUFELENBQWxCLENBQWxCOztBQUNBLFVBQUlPLFNBQUosRUFBZTtBQUNYWixRQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWVcsU0FBWixFQUF1QkcsT0FBdkIsQ0FBK0IsVUFBQ0MsR0FBRCxFQUFTO0FBQ3BDLGNBQUksb0JBQU1MLE9BQU8sQ0FBQ0ssR0FBRCxDQUFiLENBQUosRUFBeUI7QUFDckJMLFlBQUFBLE9BQU8sQ0FBQ0ssR0FBRCxDQUFQLHNCQUFtQlQsWUFBbkI7QUFDSDs7QUFDREksVUFBQUEsT0FBTyxDQUFDSyxHQUFELENBQVAsQ0FBYVosS0FBYixJQUFzQlEsU0FBUyxDQUFDSSxHQUFELENBQS9CO0FBQ0gsU0FMRDtBQU1IOztBQUNELDZDQUFZcEIsR0FBWixHQUFvQmUsT0FBcEI7QUFDSCxLQVpzQixFQVlwQixFQVpvQixDQUF2QjtBQWNBLFdBQU9YLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZUyxjQUFaLEVBQTRCL0MsR0FBNUIsQ0FBZ0MsVUFBQ3FELEdBQUQ7QUFBQSxhQUFVO0FBQzdDbEIsUUFBQUEsSUFBSSxFQUFFa0IsR0FEdUM7QUFFN0MvQyxRQUFBQSxJQUFJLEVBQUV5QyxjQUFjLENBQUNNLEdBQUQ7QUFGeUIsT0FBVjtBQUFBLEtBQWhDLENBQVA7QUFJSCxHQTVDRDs7QUE4Q0EsTUFBTTdDLFdBQVcsR0FBR0QsZUFBZSxHQUM3QkEsZUFBZSxDQUNWVSxNQURMLENBQ1ksVUFBQ3FDLFVBQUQ7QUFBQSxXQUFnQkEsVUFBVSxDQUFDdkQsT0FBWCxLQUF1QixLQUF2QztBQUFBLEdBRFosRUFFS0MsR0FGTCxDQUVTLFVBQUNzRCxVQUFEO0FBQUEsV0FBaUI7QUFDbEJDLE1BQUFBLENBQUMsRUFBRUQsVUFBVSxDQUFDbEMsS0FESTtBQUVsQm9DLE1BQUFBLFdBQVcsRUFBRUYsVUFBVSxDQUFDcEQsS0FGTjtBQUdsQnVELE1BQUFBLGVBQWUsRUFBRSxDQUhDO0FBSWxCQyxNQUFBQSxLQUFLLEVBQUU7QUFDSEYsUUFBQUEsV0FBVyxFQUFFRixVQUFVLENBQUNwRCxLQURyQjtBQUVIeUIsUUFBQUEsS0FBSyxFQUFFO0FBQ0hnQyxVQUFBQSxRQUFRLEVBQUUsa0JBRFA7QUFFSHpELFVBQUFBLEtBQUssRUFBRSxNQUZKO0FBR0gwRCxVQUFBQSxVQUFVLEVBQUVOLFVBQVUsQ0FBQ3BELEtBSHBCO0FBSUgyRCxVQUFBQSxPQUFPLEVBQUU7QUFDTEMsWUFBQUEsR0FBRyxFQUFFLENBREE7QUFFTEMsWUFBQUEsSUFBSSxFQUFFLENBRkQ7QUFHTEMsWUFBQUEsTUFBTSxFQUFFLENBSEg7QUFJTEMsWUFBQUEsS0FBSyxFQUFFO0FBSkY7QUFKTixTQUZKO0FBYUhDLFFBQUFBLElBQUksWUFDQVosVUFBVSxDQUFDbEIsY0FBWCxJQUE2QmtCLFVBQVUsQ0FBQ0ksS0FEeEMsY0FFQSx5QkFBYUosVUFBVSxDQUFDbEMsS0FBeEIsRUFBK0I7QUFDL0JLLFVBQUFBLFFBQVEsRUFBRXJCLFdBRHFCO0FBRS9Cc0IsVUFBQUEsTUFBTSxFQUFFckIsY0FGdUI7QUFHL0JzQixVQUFBQSxLQUFLLEVBQUU7QUFId0IsU0FBL0IsQ0FGQTtBQWJEO0FBSlcsS0FBakI7QUFBQSxHQUZULENBRDZCLEdBNkI3QixFQTdCTjtBQStCQSxNQUFNd0MsYUFBYSxHQUFHMUUsV0FBVyxHQUFHLEVBQXBDO0FBRUEsTUFBTTJFLFdBQVcsR0FBRztBQUNoQkMsSUFBQUEsT0FBTyxFQUFFO0FBQ0w3RCxNQUFBQSxXQUFXLEVBQUU7QUFDVDhELFFBQUFBLEtBQUssRUFBRTlEO0FBREUsT0FEUjtBQUlMVSxNQUFBQSxNQUFNLEVBQU5BLE1BSks7QUFLTHFELE1BQUFBLFVBQVUsRUFBRTtBQUNSQyxRQUFBQSxPQUFPLEVBQUU7QUFERCxPQUxQO0FBUUxDLE1BQUFBLE1BQU0sRUFBRTtBQUNKQyxRQUFBQSxJQUFJLEVBQUU7QUFERixPQVJIO0FBV0xDLE1BQUFBLEtBQUssRUFBRTtBQUNIbEUsUUFBQUEsVUFBVSxFQUFFQSxVQUFVLENBQUNRLE1BQVgsQ0FDUixVQUFDdUIsQ0FBRCxFQUFJQyxLQUFKO0FBQUEsaUJBQ0lBLEtBQUssSUFBSTVCLGlCQUFULElBQ0E0QixLQUFLLElBQUkvQixtQkFGYjtBQUFBLFNBRFE7QUFEVCxPQVhGO0FBa0JMNEQsTUFBQUEsS0FBSyxFQUFFO0FBQ0hNLFFBQUFBLE1BQU0sRUFBRTtBQUNKQyxVQUFBQSxTQUFTLEVBQUUxRDtBQURQO0FBREwsT0FsQkY7QUF1QkwyRCxNQUFBQSxLQUFLLEVBQUU7QUFDSEMsUUFBQUEsTUFBTSxFQUFFWixhQURMO0FBRUhhLFFBQUFBLEVBQUUsRUFBRSxXQUZEO0FBR0hDLFFBQUFBLE9BQU8sRUFBRXBGLFdBQVcsSUFBSUEsV0FBVyxDQUFDRSxPQUEzQixJQUFzQ0gsSUFBSSxLQUFLLEtBSHJEO0FBSUhzRixRQUFBQSxPQUFPLEVBQUU7QUFDTFIsVUFBQUEsSUFBSSxFQUFFO0FBREQsU0FKTjtBQU9IOUUsUUFBQUEsSUFBSSxFQUFKQSxJQVBHO0FBUUh1RixRQUFBQSxLQUFLLEVBQUV6RjtBQVJKLE9BdkJGO0FBaUNMb0QsTUFBQUEsSUFBSSxFQUFFO0FBQ0Y1QixRQUFBQSxNQUFNLEVBQU5BO0FBREUsT0FqQ0Q7QUFvQ0xrRSxNQUFBQSxNQUFNLEVBQUU7QUFDSmxFLFFBQUFBLE1BQU0sRUFBTkE7QUFESTtBQXBDSCxLQURPO0FBeUNoQnBCLElBQUFBLE1BQU0sRUFBRWdDLFNBQVM7QUF6Q0QsR0FBcEI7QUE0Q0Esc0JBQ0kscUJBQUMsTUFBRDtBQUFRLElBQUEsV0FBVyxFQUFFcUMsYUFBckI7QUFBb0MsSUFBQSxVQUFVLEVBQUV6RSxVQUFoRDtBQUFBLDJCQUNJLHFCQUFDLHdCQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsb0JBRGQ7QUFFSSxNQUFBLE1BQU0sWUFBS3lFLGFBQUwsQ0FGVjtBQUdJLE1BQUEsS0FBSyxZQUFLekUsVUFBTCxDQUhUO0FBSUksTUFBQSxPQUFPLEVBQUUwRSxXQUFXLENBQUNDLE9BSnpCO0FBS0ksTUFBQSxNQUFNLEVBQUVELFdBQVcsQ0FBQ3RFLE1BQVosSUFBc0IsRUFMbEM7QUFNSSxNQUFBLElBQUksRUFBRUYsSUFBSSxJQUFJO0FBTmxCO0FBREosSUFESjtBQVlILENBeE1EOztBQTBNQU8sU0FBUyxDQUFDa0YsU0FBVixHQUFzQjtBQUNsQmpGLEVBQUFBLFdBQVcsRUFBRWtGLG1CQUFVQyxNQURMO0FBRWxCbEYsRUFBQUEsY0FBYyxFQUFFaUYsbUJBQVVDLE1BRlI7QUFHbEJqRixFQUFBQSxJQUFJLEVBQUVnRixtQkFBVUUsTUFIRTtBQUlsQkMsRUFBQUEsZUFBZSxFQUFFSCxtQkFBVUUsTUFKVDtBQUtsQi9GLEVBQUFBLFdBQVcsRUFBRTZGLG1CQUFVSSxNQUxMO0FBTWxCaEcsRUFBQUEsVUFBVSxFQUFFNEYsbUJBQVVJLE1BTko7QUFPbEJDLEVBQUFBLHlCQUF5QixFQUFFTCxtQkFBVU07QUFQbkIsQ0FBdEI7ZUFVZXpGLFMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VFZmZlY3QsIHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IENoYXJ0IGZyb20gJ3JlYWN0LWFwZXhjaGFydHMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7IEZPUk1BVF9ZX1RZUEVTIH0gZnJvbSAnQGNvbnN0YW50cy90ZW1wbGF0ZSc7XG5cbmltcG9ydCB7IGJ1aWxkR3JhcGhzQ29sb3JzIH0gZnJvbSAnQGhlbHBlcnMvdGVtcGxhdGUnO1xuaW1wb3J0IHsgbnVtYmVyRm9ybWF0IH0gZnJvbSAnQGhlbHBlcnMvdXRpbHMnO1xuXG5jb25zdCBTdHlsZWQgPSBzdHlsZWQuZGl2YFxuICAgICYuYW5ub3RhdGlvbi1sYWJlbCB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIH1cblxuICAgIC5hcHBleGNoYXJ0LWNvbnRlbnQsXG4gICAgLmFwZXhjaGFydHMtY2FudmFzLFxuICAgIC5hcGV4Y2hhcnRzLWNhbnZhcyBzdmcge1xuICAgICAgICBoZWlnaHQ6ICR7KHsgaW5uZXJIZWlnaHQgfSkgPT4gYCR7aW5uZXJIZWlnaHR9cHghaW1wb3J0YW50YH07XG4gICAgICAgIHdpZHRoOiAkeyh7IGlubmVyV2lkdGggfSkgPT4gYCR7aW5uZXJXaWR0aH1weCFpbXBvcnRhbnRgfTtcbiAgICB9XG5gO1xuXG5jb25zdCBnZXRDb2xvcnMgPSAodHlwZSwgc3RhY2tlZF9iYXIsIHNlcmllcykgPT4ge1xuICAgIGlmICh0eXBlICE9PSAnYmFyJyB8fCAhc3RhY2tlZF9iYXIgfHwgIXN0YWNrZWRfYmFyLnZpc2libGUpIHtcbiAgICAgICAgcmV0dXJuIHNlcmllcy5tYXAoKHNlcmllKSA9PiBzZXJpZS5jb2xvciB8fCAnI2VmMGQwZCcpO1xuICAgIH1cblxuICAgIHJldHVybiBidWlsZEdyYXBoc0NvbG9ycyh7IHR5cGUsIHN0YWNrZWRfYmFyIH0pO1xufTtcblxuY29uc3QgQ2hhcnRWaWV3ID0gKHtcbiAgICBjdXJyZW5jeUlzbyxcbiAgICBjdXJyZW5jeUxvY2FsZSxcbiAgICBkYXRhID0ge30sXG4gICAgaW5uZXJIZWlnaHQsXG4gICAgaW5uZXJXaWR0aCxcbn0pID0+IHtcbiAgICBjb25zdCB7XG4gICAgICAgIGFubm90YXRpb25zOiBhbm5vdGF0aW9uc0RhdGEsXG4gICAgICAgIGNhdGVnb3JpZXMsXG4gICAgICAgIGZpbmlzaENhdGVnb3J5SW5kZXggPSBkYXRhLmNhdGVnb3JpZXMubGVuZ3RoIC0gMSxcbiAgICAgICAgZm9ybWF0WSA9IHt9LFxuICAgICAgICBpbml0Q2F0ZWdvcnlJbmRleCA9IDAsXG4gICAgICAgIHNlcmllczogc2VyaWVzRGF0YSxcbiAgICAgICAgc3RhY2tlZF9iYXIsXG4gICAgICAgIHR5cGUsXG4gICAgfSA9IGRhdGE7XG4gICAgY29uc3QgW2NoYXJ0TG9hZGVkLCBzZXRDaGFydExvYWRlZF0gPSB1c2VTdGF0ZSh0cnVlKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIGlmICghY2hhcnRMb2FkZWQpIHNldENoYXJ0TG9hZGVkKHRydWUpO1xuICAgIH0sIFtjaGFydExvYWRlZF0pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgc2V0Q2hhcnRMb2FkZWQoZmFsc2UpO1xuICAgIH0sIFt0eXBlLCBzdGFja2VkX2Jhciwgc2VyaWVzRGF0YV0pO1xuXG4gICAgaWYgKCFjaGFydExvYWRlZCkgcmV0dXJuIG51bGw7XG5cbiAgICBjb25zdCBzZXJpZXMgPSBzZXJpZXNEYXRhLmZpbHRlcigoc2VyaWUpID0+IHNlcmllLnZpc2libGUgIT09IGZhbHNlKTtcbiAgICBjb25zdCBjb2xvcnMgPSBnZXRDb2xvcnModHlwZSwgc3RhY2tlZF9iYXIsIHNlcmllcyk7XG5cbiAgICBjb25zdCBnZXRGb3JtYXR0aW5nWSA9ICh2YWx1ZSkgPT4ge1xuICAgICAgICBjb25zdCB7IENVUlJFTkNZLCBERUNJTUFMXzIsIERFQ0lNQUxfNCB9ID0gRk9STUFUX1lfVFlQRVM7XG5cbiAgICAgICAgc3dpdGNoIChmb3JtYXRZLnR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgQ1VSUkVOQ1k6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bWJlckZvcm1hdCh2YWx1ZSwge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW5jeTogY3VycmVuY3lJc28sXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsZTogY3VycmVuY3lMb2NhbGUsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlOiAnY3VycmVuY3knLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY2FzZSBERUNJTUFMXzI6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bWJlckZvcm1hdCh2YWx1ZSwge1xuICAgICAgICAgICAgICAgICAgICBsb2NhbGU6IGN1cnJlbmN5TG9jYWxlLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZTogJ2RlY2ltYWwnLFxuICAgICAgICAgICAgICAgICAgICB1bml0OiBmb3JtYXRZLnVuaXQsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBjYXNlIERFQ0lNQUxfNDpcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVtYmVyRm9ybWF0KHZhbHVlLCB7XG4gICAgICAgICAgICAgICAgICAgIGRlY2ltYWxzOiA0LFxuICAgICAgICAgICAgICAgICAgICBsb2NhbGU6IGN1cnJlbmN5TG9jYWxlLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZTogJ2RlY2ltYWwnLFxuICAgICAgICAgICAgICAgICAgICB1bml0OiBmb3JtYXRZLnVuaXQsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJldHVybiBudW1iZXJGb3JtYXQodmFsdWUsIHtcbiAgICAgICAgICAgICAgICAgICAgZGVjaW1hbHM6IDAsXG4gICAgICAgICAgICAgICAgICAgIGxvY2FsZTogY3VycmVuY3lMb2NhbGUsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlOiAnZGVjaW1hbCcsXG4gICAgICAgICAgICAgICAgICAgIHVuaXQ6IGZvcm1hdFkudW5pdCxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBjb25zdCBnZXRTZXJpZXMgPSAoKSA9PiB7XG4gICAgICAgIGlmICh0eXBlICE9PSAnYmFyJyB8fCAhc3RhY2tlZF9iYXIgfHwgIXN0YWNrZWRfYmFyLnZpc2libGUpIHtcbiAgICAgICAgICAgIGNvbnN0IHNlcmllc1R5cGUgPSBzZXJpZXMucmVkdWNlKChhY2MsIGN1cnJlbnQpID0+IHtcbiAgICAgICAgICAgICAgICBhY2NbY3VycmVudC50eXBlXSA9IDA7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGFjYztcbiAgICAgICAgICAgIH0sIHt9KTtcblxuICAgICAgICAgICAgcmV0dXJuIHNlcmllcy5tYXAoKHNlcmllKSA9PiAoe1xuICAgICAgICAgICAgICAgIC4uLnNlcmllLFxuICAgICAgICAgICAgICAgIG5hbWU6IHNlcmllLmVkaXRhYmxlX2xhYmVsIHx8IHNlcmllLm5hbWUsXG4gICAgICAgICAgICAgICAgdHlwZTpcbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMoc2VyaWVzVHlwZSkubGVuZ3RoID4gMSA/IHNlcmllLnR5cGUgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgZGF0YTogc2VyaWUuZGF0YS5maWx0ZXIoXG4gICAgICAgICAgICAgICAgICAgIChjLCBpbmRleCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4ID49IGluaXRDYXRlZ29yeUluZGV4ICYmXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmRleCA8PSBmaW5pc2hDYXRlZ29yeUluZGV4XG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIH0pKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHsgZGF0YSwgc2VsZWN0ZWQgfSA9IHN0YWNrZWRfYmFyO1xuICAgICAgICBjb25zdCBkYXRhUmFuZ2UgPSBkYXRhLmZpbHRlcihcbiAgICAgICAgICAgIChjLCBpbmRleCkgPT5cbiAgICAgICAgICAgICAgICBpbmRleCA+PSBpbml0Q2F0ZWdvcnlJbmRleCAmJiBpbmRleCA8PSBmaW5pc2hDYXRlZ29yeUluZGV4XG4gICAgICAgICk7XG4gICAgICAgIGNvbnN0IGFycmF5RGVmYXVsdCA9IG5ldyBBcnJheShkYXRhUmFuZ2UubGVuZ3RoKS5maWxsKDApO1xuICAgICAgICBjb25zdCBkYXRhRGljdGlvbmFyeSA9IGRhdGFSYW5nZS5yZWR1Y2UoKGFjYywgY3VycmVudCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIGxldCB0ZW1wQWNjID0geyAuLi5hY2MgfTtcbiAgICAgICAgICAgIGNvbnN0IHBhcnNlSXRlbSA9IEpTT04ucGFyc2UoY3VycmVudFtzZWxlY3RlZF0pO1xuICAgICAgICAgICAgaWYgKHBhcnNlSXRlbSkge1xuICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKHBhcnNlSXRlbSkuZm9yRWFjaCgoa2V5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChpc05pbCh0ZW1wQWNjW2tleV0pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQWNjW2tleV0gPSBbLi4uYXJyYXlEZWZhdWx0XTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0ZW1wQWNjW2tleV1baW5kZXhdID0gcGFyc2VJdGVtW2tleV07XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4geyAuLi5hY2MsIC4uLnRlbXBBY2MgfTtcbiAgICAgICAgfSwge30pO1xuXG4gICAgICAgIHJldHVybiBPYmplY3Qua2V5cyhkYXRhRGljdGlvbmFyeSkubWFwKChrZXkpID0+ICh7XG4gICAgICAgICAgICBuYW1lOiBrZXksXG4gICAgICAgICAgICBkYXRhOiBkYXRhRGljdGlvbmFyeVtrZXldLFxuICAgICAgICB9KSk7XG4gICAgfTtcblxuICAgIGNvbnN0IGFubm90YXRpb25zID0gYW5ub3RhdGlvbnNEYXRhXG4gICAgICAgID8gYW5ub3RhdGlvbnNEYXRhXG4gICAgICAgICAgICAgIC5maWx0ZXIoKGFubm90YXRpb24pID0+IGFubm90YXRpb24udmlzaWJsZSAhPT0gZmFsc2UpXG4gICAgICAgICAgICAgIC5tYXAoKGFubm90YXRpb24pID0+ICh7XG4gICAgICAgICAgICAgICAgICB5OiBhbm5vdGF0aW9uLnZhbHVlLFxuICAgICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IGFubm90YXRpb24uY29sb3IsXG4gICAgICAgICAgICAgICAgICBzdHJva2VEYXNoQXJyYXk6IDAsXG4gICAgICAgICAgICAgICAgICBsYWJlbDoge1xuICAgICAgICAgICAgICAgICAgICAgIGJvcmRlckNvbG9yOiBhbm5vdGF0aW9uLmNvbG9yLFxuICAgICAgICAgICAgICAgICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNzc0NsYXNzOiAnYW5ub3RhdGlvbi1sYWJlbCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnI0ZGRicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IGFubm90YXRpb24uY29sb3IsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogNSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IDUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3R0b206IDUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICByaWdodDogNSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgIHRleHQ6IGAke1xuICAgICAgICAgICAgICAgICAgICAgICAgICBhbm5vdGF0aW9uLmVkaXRhYmxlX2xhYmVsIHx8IGFubm90YXRpb24ubGFiZWxcbiAgICAgICAgICAgICAgICAgICAgICB9ICR7bnVtYmVyRm9ybWF0KGFubm90YXRpb24udmFsdWUsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVuY3k6IGN1cnJlbmN5SXNvLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBsb2NhbGU6IGN1cnJlbmN5TG9jYWxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZTogJ2N1cnJlbmN5JyxcbiAgICAgICAgICAgICAgICAgICAgICB9KX1gLFxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgfSkpXG4gICAgICAgIDogW107XG5cbiAgICBjb25zdCBjb250ZW50SGVpZ2h0ID0gaW5uZXJIZWlnaHQgLSAyMDtcblxuICAgIGNvbnN0IGNoYXJ0Q29uZmlnID0ge1xuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgICBhbm5vdGF0aW9uczoge1xuICAgICAgICAgICAgICAgIHlheGlzOiBhbm5vdGF0aW9ucyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBjb2xvcnMsXG4gICAgICAgICAgICBkYXRhTGFiZWxzOiB7XG4gICAgICAgICAgICAgICAgZW5hYmxlZDogZmFsc2UsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbGVnZW5kOiB7XG4gICAgICAgICAgICAgICAgc2hvdzogdHJ1ZSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB4YXhpczoge1xuICAgICAgICAgICAgICAgIGNhdGVnb3JpZXM6IGNhdGVnb3JpZXMuZmlsdGVyKFxuICAgICAgICAgICAgICAgICAgICAoYywgaW5kZXgpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICBpbmRleCA+PSBpbml0Q2F0ZWdvcnlJbmRleCAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXggPD0gZmluaXNoQ2F0ZWdvcnlJbmRleFxuICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgeWF4aXM6IHtcbiAgICAgICAgICAgICAgICBsYWJlbHM6IHtcbiAgICAgICAgICAgICAgICAgICAgZm9ybWF0dGVyOiBnZXRGb3JtYXR0aW5nWSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNoYXJ0OiB7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBjb250ZW50SGVpZ2h0LFxuICAgICAgICAgICAgICAgIGlkOiAnYXBleGNoYXJ0JyxcbiAgICAgICAgICAgICAgICBzdGFja2VkOiBzdGFja2VkX2JhciAmJiBzdGFja2VkX2Jhci52aXNpYmxlICYmIHR5cGUgPT09ICdiYXInLFxuICAgICAgICAgICAgICAgIHRvb2xiYXI6IHtcbiAgICAgICAgICAgICAgICAgICAgc2hvdzogZmFsc2UsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB0eXBlLFxuICAgICAgICAgICAgICAgIHdpZHRoOiBpbm5lcldpZHRoLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGZpbGw6IHtcbiAgICAgICAgICAgICAgICBjb2xvcnMsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc3Ryb2tlOiB7XG4gICAgICAgICAgICAgICAgY29sb3JzLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfSxcbiAgICAgICAgc2VyaWVzOiBnZXRTZXJpZXMoKSxcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFN0eWxlZCBpbm5lckhlaWdodD17Y29udGVudEhlaWdodH0gaW5uZXJXaWR0aD17aW5uZXJXaWR0aH0+XG4gICAgICAgICAgICA8Q2hhcnRcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJhcHBleGNoYXJ0LWNvbnRlbnRcIlxuICAgICAgICAgICAgICAgIGhlaWdodD17YCR7Y29udGVudEhlaWdodH1gfVxuICAgICAgICAgICAgICAgIHdpZHRoPXtgJHtpbm5lcldpZHRofWB9XG4gICAgICAgICAgICAgICAgb3B0aW9ucz17Y2hhcnRDb25maWcub3B0aW9uc31cbiAgICAgICAgICAgICAgICBzZXJpZXM9e2NoYXJ0Q29uZmlnLnNlcmllcyB8fCBbXX1cbiAgICAgICAgICAgICAgICB0eXBlPXt0eXBlIHx8ICdiYXInfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9TdHlsZWQ+XG4gICAgKTtcbn07XG5cbkNoYXJ0Vmlldy5wcm9wVHlwZXMgPSB7XG4gICAgY3VycmVuY3lJc286IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY3VycmVuY3lMb2NhbGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGF0YTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBkZWZhdWx0Q3VycmVuY3k6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgaW5uZXJIZWlnaHQ6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgaW5uZXJXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBpc0NvdW50cnlDdXJyZW5jeVJlcXVpcmVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IENoYXJ0VmlldztcbiJdfQ==