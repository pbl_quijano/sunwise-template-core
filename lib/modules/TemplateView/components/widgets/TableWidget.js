"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _template = require("../../../../constants/template");

var _types = require("../../../../constants/types");

var _utils = require("../../../../helpers/utils");

var _TableHeaderView = _interopRequireDefault(require("./table/TableHeaderView"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    background-color: #fff;\n    ", "\n    border-top-width: 0px !important;\n"])), function (_ref) {
  var roundedBorders = _ref.roundedBorders;
  return roundedBorders && 'border-radius: 17px 17px 0 0;';
});

var TableContent = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    height: 100%;\n"])));

var DataContent = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    flex: 1;\n    overflow-x: hidden;\n    overflow-y: ", ";\n"])), function (_ref2) {
  var overflowYDisabled = _ref2.overflowYDisabled,
      editionLevel = _ref2.editionLevel;
  return overflowYDisabled ? 'hidden' : editionLevel === _types.PARTIAL_EDITION_MODE ? 'overlay' : 'auto';
});

var BodyContent = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    background-color: ", ";\n"])), function (_ref3) {
  var _ref3$backgroundColor = _ref3.backgroundColor1,
      backgroundColor1 = _ref3$backgroundColor === void 0 ? '#fff' : _ref3$backgroundColor;
  return backgroundColor1;
});

var BodyDataRow = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    display: flex;\n    ", "\n"])), function (_ref4) {
  var isZebraStyle = _ref4.isZebraStyle,
      backgroundColor2 = _ref4.backgroundColor2,
      index = _ref4.index;
  return isZebraStyle && (index + 1) % 2 === 0 && "background-color: ".concat(backgroundColor2, ";");
});

var BodyData = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    display: flex;\n    font-weight: ", ";\n    ", "\n    font-size: ", ";\n    line-height: ", ";\n    color: ", ";\n    ", "\n    padding: 8px 15px;\n    overflow: hidden;\n    span {\n        white-space: pre-line;\n        overflow: hidden;\n        text-overflow: ellipsis;\n    }\n"])), function (_ref5) {
  var fontBold = _ref5.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref6) {
  var fontItalic = _ref6.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref7) {
  var _ref7$fontSize = _ref7.fontSize,
      fontSize = _ref7$fontSize === void 0 ? '13px' : _ref7$fontSize;
  return fontSize;
}, function (_ref8) {
  var _ref8$fontSize = _ref8.fontSize,
      fontSize = _ref8$fontSize === void 0 ? '13px' : _ref8$fontSize;
  return fontSize;
}, function (_ref9) {
  var _ref9$color = _ref9.color,
      color = _ref9$color === void 0 ? '#000' : _ref9$color;
  return color;
}, function (_ref10) {
  var relativeWidth = _ref10.relativeWidth;
  return !(0, _isNil.default)(relativeWidth) && "width: ".concat(relativeWidth, "%;");
});

var FooterContent = _styledComponents.default.div(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    background-color: ", ";\n    border-top-style: ", ";\n    border-top-width: ", ";\n    border-top-color: ", ";\n    ", "\n"])), function (_ref11) {
  var _ref11$backgroundColo = _ref11.backgroundColor1,
      backgroundColor1 = _ref11$backgroundColo === void 0 ? '#fff' : _ref11$backgroundColo;
  return backgroundColor1;
}, function (_ref12) {
  var borderTopStyle = _ref12.borderTopStyle;
  return borderTopStyle;
}, function (_ref13) {
  var borderTopWidth = _ref13.borderTopWidth;
  return borderTopWidth;
}, function (_ref14) {
  var borderTopColor = _ref14.borderTopColor;
  return borderTopColor;
}, function (_ref15) {
  var isZebraStyle = _ref15.isZebraStyle,
      backgroundColor2 = _ref15.backgroundColor2;
  return isZebraStyle && ".footer-row:nth-child(even) {\n            background-color: ".concat(backgroundColor2, ";\n        }");
});

var FooterDataRow = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n    display: flex;\n"])));

var FooterData = _styledComponents.default.div(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n    display: flex;\n    font-weight: ", ";\n    ", "\n    font-size: ", ";\n    line-height: ", ";\n    color: ", ";\n    ", "\n    padding: 8px 15px;\n    overflow: hidden;\n    span {\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: pre-line;\n    }\n"])), function (_ref16) {
  var fontBold = _ref16.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref17) {
  var fontItalic = _ref17.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref18) {
  var _ref18$fontSize = _ref18.fontSize,
      fontSize = _ref18$fontSize === void 0 ? '13px' : _ref18$fontSize;
  return fontSize;
}, function (_ref19) {
  var _ref19$fontSize = _ref19.fontSize,
      fontSize = _ref19$fontSize === void 0 ? '13px' : _ref19$fontSize;
  return fontSize;
}, function (_ref20) {
  var _ref20$color = _ref20.color,
      color = _ref20$color === void 0 ? '#000' : _ref20$color;
  return color;
}, function (_ref21) {
  var relativeWidth = _ref21.relativeWidth;
  return !(0, _isNil.default)(relativeWidth) && "width: ".concat(relativeWidth, "%;");
});

var getSizes = function getSizes(preparedCols) {
  var withoutWidthNum = preparedCols.reduce(function (acc, current) {
    if ((0, _isNil.default)(current.width)) {
      ++acc;
    }

    return acc;
  }, 0);

  if (withoutWidthNum > 0) {
    return preparedCols.map(function () {
      return 100 / preparedCols.length;
    });
  }

  return preparedCols.map(function (col) {
    return col.width;
  });
};

var orderSort = function orderSort(a, b) {
  if (a.order > b.order) return 1;
  if (b.order > a.order) return -1;
  return 0;
};

var processAction = function processAction() {};

var processDelay = null;

var TableWidget = function TableWidget(_ref22) {
  var currencyIso = _ref22.currencyIso,
      currencyLocale = _ref22.currencyLocale,
      editionLevel = _ref22.editionLevel,
      handleChangeValue = _ref22.handleChangeValue,
      infinitePagesSupportEnabled = _ref22.infinitePagesSupportEnabled,
      innerWidth = _ref22.innerWidth,
      isSelected = _ref22.isSelected,
      _ref22$style = _ref22.style,
      style = _ref22$style === void 0 ? {} : _ref22$style,
      _ref22$value = _ref22.value,
      value = _ref22$value === void 0 ? {} : _ref22$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var contentRef = (0, _react.useRef)(null);
  var tableStyle = style.table,
      headerStyle = style.header,
      bodyStyle = style.body,
      footerStyle = style.footer;
  var _value$data = value.data,
      data = _value$data === void 0 ? [] : _value$data,
      _value$results = value.results,
      results = _value$results === void 0 ? {} : _value$results,
      _value$cols = value.cols,
      cols = _value$cols === void 0 ? {} : _value$cols,
      _value$scrollTop = value.scrollTop,
      scrollTop = _value$scrollTop === void 0 ? 0 : _value$scrollTop;
  var preparedCols = Object.keys(cols).map(function (colKey) {
    return _objectSpread(_objectSpread({}, cols[colKey]), {}, {
      id: colKey
    });
  }).sort(orderSort).filter(function (col) {
    return col.visible;
  });

  var _useState = (0, _react.useState)({
    pendingSave: false,
    data: []
  }),
      _useState2 = _slicedToArray(_useState, 2),
      currentSizes = _useState2[0],
      setCurrentSizes = _useState2[1];

  (0, _react.useEffect)(function () {
    if (editionLevel === _types.PARTIAL_EDITION_MODE && !infinitePagesSupportEnabled) {
      if (scrollTop !== contentRef.current.scrollTop) {
        contentRef.current.scrollTop = scrollTop;
      }

      var onScrollTable = function onScrollTable(e) {
        var tempValue = (0, _utils.cloneElement)(value);
        tempValue.scrollTop = e.target.scrollTop;
        if (processDelay !== null) clearTimeout(processDelay);

        processAction = function processAction() {
          if (scrollTop !== e.target.scrollTop) {
            var _tempValue = (0, _utils.cloneElement)(value);

            _tempValue.scrollTop = e.target.scrollTop;
            handleChangeValue(_tempValue);
          }
        };

        processDelay = setTimeout(function () {
          processAction();

          processAction = function processAction() {};

          processDelay = null;
        }, 100);
      };

      contentRef.current.addEventListener('scroll', onScrollTable, {
        passive: true
      });
      return function () {
        if (editionLevel === _types.PARTIAL_EDITION_MODE && contentRef.current) {
          contentRef.current.removeEventListener('scroll', onScrollTable);
        }
      };
    }
  }, [contentRef, handleChangeValue]);
  (0, _react.useEffect)(function () {
    setCurrentSizes({
      pendingSave: false,
      data: getSizes(preparedCols)
    });
  }, [preparedCols.length]);
  (0, _react.useEffect)(function () {
    if (currentSizes.pendingSave) {
      var tempValue = (0, _utils.cloneElement)(value);
      preparedCols.forEach(function (col, index) {
        tempValue.cols[col.id].width = currentSizes.data[index];
      });
      handleChangeValue(tempValue);
    }
  }, [currentSizes]);

  var getDataTextProps = function getDataTextProps(_ref23) {
    var fontBold = _ref23.fontBold,
        fontItalic = _ref23.fontItalic,
        fontSize = _ref23.fontSize,
        color = _ref23.color;
    return {
      fontBold: fontBold,
      fontItalic: fontItalic,
      fontSize: fontSize,
      color: color
    };
  };

  var _onSaveColumnsWidth = function _onSaveColumnsWidth(newSizes) {
    setCurrentSizes({
      pendingSave: true,
      data: newSizes
    });
  };

  var getIsVisible = function getIsVisible(col, elementsByColumn) {
    return (0, _get.default)(elementsByColumn, "".concat(col.id, ".visible"), false);
  };

  var renderExtraRow = function renderExtraRow(resultKey) {
    var resultElement = results[resultKey];
    if ((0, _isNil.default)(resultElement) || !resultElement.visible || preparedCols.length <= 0) return null;
    var elementsByColumn = Object.keys(resultElement.columns).reduce(function (acc, currentValue) {
      var columnElement = resultElement.columns[currentValue];
      acc[columnElement.ref] = columnElement;
      return acc;
    }, {});
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterDataRow, {
      className: "footer-row widget-table-data-row",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({
        relativeWidth: currentSizes.data[0]
      }, getDataTextProps(footerStyle)), {}, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          children: resultElement.label
        })
      })), _toConsumableArray(preparedCols).slice(1).map(function (col, index) {
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({
          relativeWidth: currentSizes.data[index + 1]
        }, getDataTextProps(footerStyle)), {}, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
            children: getIsVisible(col, elementsByColumn) ? ValueFooterFormatted(col, elementsByColumn) : null
          })
        }), "subtotal-".concat(col.id));
      })]
    }, "table-row-".concat(resultKey));
  };

  var renderFooter = function renderFooter() {
    if ((0, _isEmpty.default)(results)) return null;
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterContent, _objectSpread(_objectSpread({}, footerStyle), {}, {
      className: "widget-table-data-footer",
      children: Object.keys(results).map(function (key) {
        return renderExtraRow(key);
      })
    }));
  };

  var getWarnings = function getWarnings() {
    if (editionLevel === _types.NO_EDITION_MODE) {
      return [];
    }

    if (editionLevel === _types.PARTIAL_EDITION_MODE) {
      var _value$warnings = value.warnings,
          warnings = _value$warnings === void 0 ? [] : _value$warnings;
      return warnings;
    }

    return [t('The data shown is for example only')];
  };

  var ValueFormatted = function ValueFormatted(col, item) {
    if (editionLevel === _types.FULL_EDITION_MODE && _template.TABLE_DATA_COLUMNS.includes(col.id)) {
      return (0, _utils.numberFormat)(item[col.id], {
        currency: currencyIso,
        style: 'currency',
        locale: currencyLocale
      });
    }

    return item[col.id];
  };

  var ValueFooterFormatted = function ValueFooterFormatted(col, elementsByColumn) {
    var value = (0, _get.default)(elementsByColumn, "".concat(col.id, ".value"), 0);

    if (editionLevel === _types.FULL_EDITION_MODE && _template.TABLE_FOOTER_COLUMNS.includes(col.id)) {
      return (0, _utils.numberFormat)(value, {
        currency: currencyIso,
        style: 'currency',
        locale: currencyLocale
      });
    }

    return value;
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    className: "widget-table-container-resize",
    roundedBorders: headerStyle.roundedBorders,
    style: tableStyle,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(TableContent, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_TableHeaderView.default, {
        headerStyle: headerStyle,
        innerWidth: innerWidth,
        isWidgetSelected: isSelected,
        onSaveColumnsWidth: _onSaveColumnsWidth,
        preparedCols: preparedCols,
        sizes: currentSizes.data,
        textStyle: _objectSpread({}, getDataTextProps(headerStyle)),
        warnings: getWarnings()
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(DataContent, {
        className: "puppeteer-scrolling widget-table-data-container",
        "data-scrolltop": scrollTop,
        overflowYDisabled: editionLevel === _types.NO_EDITION_MODE || !isSelected || infinitePagesSupportEnabled,
        ref: contentRef,
        editionLevel: editionLevel,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(BodyContent, {
          backgroundColor1: bodyStyle.backgroundColor1,
          className: "widget-table-data-body",
          children: data.map(function (item, index) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(BodyDataRow, {
              backgroundColor2: bodyStyle.backgroundColor2,
              className: "widget-table-data-row",
              index: index,
              isZebraStyle: bodyStyle.isZebraStyle,
              children: preparedCols.map(function (col, index) {
                return /*#__PURE__*/(0, _jsxRuntime.jsx)(BodyData, _objectSpread(_objectSpread({
                  relativeWidth: currentSizes.data[index]
                }, getDataTextProps(bodyStyle)), {}, {
                  children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                    children: ValueFormatted(col, item)
                  })
                }), "".concat(index, "-").concat(col.id));
              })
            }, index);
          })
        }), renderFooter()]
      })]
    })
  });
};

TableWidget.propTypes = {
  currencyIso: _propTypes.default.string,
  currencyLocale: _propTypes.default.string,
  defaultCurrency: _propTypes.default.object,
  editionLevel: _propTypes.default.string,
  handleChangeValue: _propTypes.default.func,
  infinitePagesSupportEnabled: _propTypes.default.bool,
  innerWidth: _propTypes.default.string,
  isSelected: _propTypes.default.bool,
  keyName: _propTypes.default.string,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = TableWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvVGFibGVXaWRnZXQuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2Iiwicm91bmRlZEJvcmRlcnMiLCJUYWJsZUNvbnRlbnQiLCJEYXRhQ29udGVudCIsIm92ZXJmbG93WURpc2FibGVkIiwiZWRpdGlvbkxldmVsIiwiUEFSVElBTF9FRElUSU9OX01PREUiLCJCb2R5Q29udGVudCIsImJhY2tncm91bmRDb2xvcjEiLCJCb2R5RGF0YVJvdyIsImlzWmVicmFTdHlsZSIsImJhY2tncm91bmRDb2xvcjIiLCJpbmRleCIsIkJvZHlEYXRhIiwiZm9udEJvbGQiLCJmb250SXRhbGljIiwiZm9udFNpemUiLCJjb2xvciIsInJlbGF0aXZlV2lkdGgiLCJGb290ZXJDb250ZW50IiwiYm9yZGVyVG9wU3R5bGUiLCJib3JkZXJUb3BXaWR0aCIsImJvcmRlclRvcENvbG9yIiwiRm9vdGVyRGF0YVJvdyIsIkZvb3RlckRhdGEiLCJnZXRTaXplcyIsInByZXBhcmVkQ29scyIsIndpdGhvdXRXaWR0aE51bSIsInJlZHVjZSIsImFjYyIsImN1cnJlbnQiLCJ3aWR0aCIsIm1hcCIsImxlbmd0aCIsImNvbCIsIm9yZGVyU29ydCIsImEiLCJiIiwib3JkZXIiLCJwcm9jZXNzQWN0aW9uIiwicHJvY2Vzc0RlbGF5IiwiVGFibGVXaWRnZXQiLCJjdXJyZW5jeUlzbyIsImN1cnJlbmN5TG9jYWxlIiwiaGFuZGxlQ2hhbmdlVmFsdWUiLCJpbmZpbml0ZVBhZ2VzU3VwcG9ydEVuYWJsZWQiLCJpbm5lcldpZHRoIiwiaXNTZWxlY3RlZCIsInN0eWxlIiwidmFsdWUiLCJ0IiwiY29udGVudFJlZiIsInRhYmxlU3R5bGUiLCJ0YWJsZSIsImhlYWRlclN0eWxlIiwiaGVhZGVyIiwiYm9keVN0eWxlIiwiYm9keSIsImZvb3RlclN0eWxlIiwiZm9vdGVyIiwiZGF0YSIsInJlc3VsdHMiLCJjb2xzIiwic2Nyb2xsVG9wIiwiT2JqZWN0Iiwia2V5cyIsImNvbEtleSIsImlkIiwic29ydCIsImZpbHRlciIsInZpc2libGUiLCJwZW5kaW5nU2F2ZSIsImN1cnJlbnRTaXplcyIsInNldEN1cnJlbnRTaXplcyIsIm9uU2Nyb2xsVGFibGUiLCJlIiwidGVtcFZhbHVlIiwidGFyZ2V0IiwiY2xlYXJUaW1lb3V0Iiwic2V0VGltZW91dCIsImFkZEV2ZW50TGlzdGVuZXIiLCJwYXNzaXZlIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImZvckVhY2giLCJnZXREYXRhVGV4dFByb3BzIiwiX29uU2F2ZUNvbHVtbnNXaWR0aCIsIm5ld1NpemVzIiwiZ2V0SXNWaXNpYmxlIiwiZWxlbWVudHNCeUNvbHVtbiIsInJlbmRlckV4dHJhUm93IiwicmVzdWx0S2V5IiwicmVzdWx0RWxlbWVudCIsImNvbHVtbnMiLCJjdXJyZW50VmFsdWUiLCJjb2x1bW5FbGVtZW50IiwicmVmIiwibGFiZWwiLCJzbGljZSIsIlZhbHVlRm9vdGVyRm9ybWF0dGVkIiwicmVuZGVyRm9vdGVyIiwia2V5IiwiZ2V0V2FybmluZ3MiLCJOT19FRElUSU9OX01PREUiLCJ3YXJuaW5ncyIsIlZhbHVlRm9ybWF0dGVkIiwiaXRlbSIsIkZVTExfRURJVElPTl9NT0RFIiwiVEFCTEVfREFUQV9DT0xVTU5TIiwiaW5jbHVkZXMiLCJjdXJyZW5jeSIsImxvY2FsZSIsIlRBQkxFX0ZPT1RFUl9DT0xVTU5TIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiZGVmYXVsdEN1cnJlbmN5Iiwib2JqZWN0IiwiZnVuYyIsImJvb2wiLCJrZXlOYW1lIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7O0FBTUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsMkxBSVQ7QUFBQSxNQUFHQyxjQUFILFFBQUdBLGNBQUg7QUFBQSxTQUF3QkEsY0FBYyxJQUFJLCtCQUExQztBQUFBLENBSlMsQ0FBZjs7QUFRQSxJQUFNQyxZQUFZLEdBQUdILDBCQUFPQyxHQUFWLCtKQUFsQjs7QUFPQSxJQUFNRyxXQUFXLEdBQUdKLDBCQUFPQyxHQUFWLHlMQUtDO0FBQUEsTUFBR0ksaUJBQUgsU0FBR0EsaUJBQUg7QUFBQSxNQUFzQkMsWUFBdEIsU0FBc0JBLFlBQXRCO0FBQUEsU0FDVkQsaUJBQWlCLEdBQ1gsUUFEVyxHQUVYQyxZQUFZLEtBQUtDLDJCQUFqQixHQUNBLFNBREEsR0FFQSxNQUxJO0FBQUEsQ0FMRCxDQUFqQjs7QUFhQSxJQUFNQyxXQUFXLEdBQUdSLDBCQUFPQyxHQUFWLHdKQUdPO0FBQUEsb0NBQUdRLGdCQUFIO0FBQUEsTUFBR0EsZ0JBQUgsc0NBQXNCLE1BQXRCO0FBQUEsU0FBbUNBLGdCQUFuQztBQUFBLENBSFAsQ0FBakI7O0FBTUEsSUFBTUMsV0FBVyxHQUFHViwwQkFBT0MsR0FBVix3R0FFWDtBQUFBLE1BQUdVLFlBQUgsU0FBR0EsWUFBSDtBQUFBLE1BQWlCQyxnQkFBakIsU0FBaUJBLGdCQUFqQjtBQUFBLE1BQW1DQyxLQUFuQyxTQUFtQ0EsS0FBbkM7QUFBQSxTQUNFRixZQUFZLElBQ1osQ0FBQ0UsS0FBSyxHQUFHLENBQVQsSUFBYyxDQUFkLEtBQW9CLENBRHBCLGdDQUVxQkQsZ0JBRnJCLE1BREY7QUFBQSxDQUZXLENBQWpCOztBQVFBLElBQU1FLFFBQVEsR0FBR2QsMEJBQU9DLEdBQVYseVdBRUs7QUFBQSxNQUFHYyxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQkEsUUFBUSxHQUFHLE1BQUgsR0FBWSxLQUF2QztBQUFBLENBRkwsRUFHUjtBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLFNBQW9CQSxVQUFVLElBQUkscUJBQWxDO0FBQUEsQ0FIUSxFQUlHO0FBQUEsNkJBQUdDLFFBQUg7QUFBQSxNQUFHQSxRQUFILCtCQUFjLE1BQWQ7QUFBQSxTQUEyQkEsUUFBM0I7QUFBQSxDQUpILEVBS0s7QUFBQSw2QkFBR0EsUUFBSDtBQUFBLE1BQUdBLFFBQUgsK0JBQWMsTUFBZDtBQUFBLFNBQTJCQSxRQUEzQjtBQUFBLENBTEwsRUFNRDtBQUFBLDBCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCw0QkFBVyxNQUFYO0FBQUEsU0FBd0JBLEtBQXhCO0FBQUEsQ0FOQyxFQU9SO0FBQUEsTUFBR0MsYUFBSCxVQUFHQSxhQUFIO0FBQUEsU0FDRSxDQUFDLG9CQUFNQSxhQUFOLENBQUQscUJBQW1DQSxhQUFuQyxPQURGO0FBQUEsQ0FQUSxDQUFkOztBQWtCQSxJQUFNQyxhQUFhLEdBQUdwQiwwQkFBT0MsR0FBVix5UEFHSztBQUFBLHFDQUFHUSxnQkFBSDtBQUFBLE1BQUdBLGdCQUFILHNDQUFzQixNQUF0QjtBQUFBLFNBQW1DQSxnQkFBbkM7QUFBQSxDQUhMLEVBSUs7QUFBQSxNQUFHWSxjQUFILFVBQUdBLGNBQUg7QUFBQSxTQUF3QkEsY0FBeEI7QUFBQSxDQUpMLEVBS0s7QUFBQSxNQUFHQyxjQUFILFVBQUdBLGNBQUg7QUFBQSxTQUF3QkEsY0FBeEI7QUFBQSxDQUxMLEVBTUs7QUFBQSxNQUFHQyxjQUFILFVBQUdBLGNBQUg7QUFBQSxTQUF3QkEsY0FBeEI7QUFBQSxDQU5MLEVBT2I7QUFBQSxNQUFHWixZQUFILFVBQUdBLFlBQUg7QUFBQSxNQUFpQkMsZ0JBQWpCLFVBQWlCQSxnQkFBakI7QUFBQSxTQUNFRCxZQUFZLDJFQUVZQyxnQkFGWixpQkFEZDtBQUFBLENBUGEsQ0FBbkI7O0FBY0EsSUFBTVksYUFBYSxHQUFHeEIsMEJBQU9DLEdBQVYsNkZBQW5COztBQUlBLElBQU13QixVQUFVLEdBQUd6QiwwQkFBT0MsR0FBVix5V0FFRztBQUFBLE1BQUdjLFFBQUgsVUFBR0EsUUFBSDtBQUFBLFNBQW1CQSxRQUFRLEdBQUcsTUFBSCxHQUFZLEtBQXZDO0FBQUEsQ0FGSCxFQUdWO0FBQUEsTUFBR0MsVUFBSCxVQUFHQSxVQUFIO0FBQUEsU0FBb0JBLFVBQVUsSUFBSSxxQkFBbEM7QUFBQSxDQUhVLEVBSUM7QUFBQSwrQkFBR0MsUUFBSDtBQUFBLE1BQUdBLFFBQUgsZ0NBQWMsTUFBZDtBQUFBLFNBQTJCQSxRQUEzQjtBQUFBLENBSkQsRUFLRztBQUFBLCtCQUFHQSxRQUFIO0FBQUEsTUFBR0EsUUFBSCxnQ0FBYyxNQUFkO0FBQUEsU0FBMkJBLFFBQTNCO0FBQUEsQ0FMSCxFQU1IO0FBQUEsNEJBQUdDLEtBQUg7QUFBQSxNQUFHQSxLQUFILDZCQUFXLE1BQVg7QUFBQSxTQUF3QkEsS0FBeEI7QUFBQSxDQU5HLEVBT1Y7QUFBQSxNQUFHQyxhQUFILFVBQUdBLGFBQUg7QUFBQSxTQUNFLENBQUMsb0JBQU1BLGFBQU4sQ0FBRCxxQkFBbUNBLGFBQW5DLE9BREY7QUFBQSxDQVBVLENBQWhCOztBQWtCQSxJQUFNTyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDQyxZQUFELEVBQWtCO0FBQy9CLE1BQU1DLGVBQWUsR0FBR0QsWUFBWSxDQUFDRSxNQUFiLENBQW9CLFVBQUNDLEdBQUQsRUFBTUMsT0FBTixFQUFrQjtBQUMxRCxRQUFJLG9CQUFNQSxPQUFPLENBQUNDLEtBQWQsQ0FBSixFQUEwQjtBQUN0QixRQUFFRixHQUFGO0FBQ0g7O0FBQ0QsV0FBT0EsR0FBUDtBQUNILEdBTHVCLEVBS3JCLENBTHFCLENBQXhCOztBQU1BLE1BQUlGLGVBQWUsR0FBRyxDQUF0QixFQUF5QjtBQUNyQixXQUFPRCxZQUFZLENBQUNNLEdBQWIsQ0FBaUI7QUFBQSxhQUFNLE1BQU1OLFlBQVksQ0FBQ08sTUFBekI7QUFBQSxLQUFqQixDQUFQO0FBQ0g7O0FBQ0QsU0FBT1AsWUFBWSxDQUFDTSxHQUFiLENBQWlCLFVBQUNFLEdBQUQ7QUFBQSxXQUFTQSxHQUFHLENBQUNILEtBQWI7QUFBQSxHQUFqQixDQUFQO0FBQ0gsQ0FYRDs7QUFhQSxJQUFNSSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUN4QixNQUFJRCxDQUFDLENBQUNFLEtBQUYsR0FBVUQsQ0FBQyxDQUFDQyxLQUFoQixFQUF1QixPQUFPLENBQVA7QUFDdkIsTUFBSUQsQ0FBQyxDQUFDQyxLQUFGLEdBQVVGLENBQUMsQ0FBQ0UsS0FBaEIsRUFBdUIsT0FBTyxDQUFDLENBQVI7QUFDdkIsU0FBTyxDQUFQO0FBQ0gsQ0FKRDs7QUFNQSxJQUFJQyxhQUFhLEdBQUcseUJBQU0sQ0FBRSxDQUE1Qjs7QUFDQSxJQUFJQyxZQUFZLEdBQUcsSUFBbkI7O0FBRUEsSUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsU0FVZDtBQUFBLE1BVEZDLFdBU0UsVUFURkEsV0FTRTtBQUFBLE1BUkZDLGNBUUUsVUFSRkEsY0FRRTtBQUFBLE1BUEZ0QyxZQU9FLFVBUEZBLFlBT0U7QUFBQSxNQU5GdUMsaUJBTUUsVUFORkEsaUJBTUU7QUFBQSxNQUxGQywyQkFLRSxVQUxGQSwyQkFLRTtBQUFBLE1BSkZDLFVBSUUsVUFKRkEsVUFJRTtBQUFBLE1BSEZDLFVBR0UsVUFIRkEsVUFHRTtBQUFBLDRCQUZGQyxLQUVFO0FBQUEsTUFGRkEsS0FFRSw2QkFGTSxFQUVOO0FBQUEsNEJBREZDLEtBQ0U7QUFBQSxNQURGQSxLQUNFLDZCQURNLEVBQ047O0FBQ0Ysd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLE1BQU1DLFVBQVUsR0FBRyxtQkFBTyxJQUFQLENBQW5CO0FBRUEsTUFDV0MsVUFEWCxHQUtJSixLQUxKLENBQ0lLLEtBREo7QUFBQSxNQUVZQyxXQUZaLEdBS0lOLEtBTEosQ0FFSU8sTUFGSjtBQUFBLE1BR1VDLFNBSFYsR0FLSVIsS0FMSixDQUdJUyxJQUhKO0FBQUEsTUFJWUMsV0FKWixHQUtJVixLQUxKLENBSUlXLE1BSko7QUFPQSxvQkFBOERWLEtBQTlELENBQVFXLElBQVI7QUFBQSxNQUFRQSxJQUFSLDRCQUFlLEVBQWY7QUFBQSx1QkFBOERYLEtBQTlELENBQW1CWSxPQUFuQjtBQUFBLE1BQW1CQSxPQUFuQiwrQkFBNkIsRUFBN0I7QUFBQSxvQkFBOERaLEtBQTlELENBQWlDYSxJQUFqQztBQUFBLE1BQWlDQSxJQUFqQyw0QkFBd0MsRUFBeEM7QUFBQSx5QkFBOERiLEtBQTlELENBQTRDYyxTQUE1QztBQUFBLE1BQTRDQSxTQUE1QyxpQ0FBd0QsQ0FBeEQ7QUFFQSxNQUFNckMsWUFBWSxHQUFHc0MsTUFBTSxDQUFDQyxJQUFQLENBQVlILElBQVosRUFDaEI5QixHQURnQixDQUNaLFVBQUNrQyxNQUFEO0FBQUEsMkNBQWtCSixJQUFJLENBQUNJLE1BQUQsQ0FBdEI7QUFBZ0NDLE1BQUFBLEVBQUUsRUFBRUQ7QUFBcEM7QUFBQSxHQURZLEVBRWhCRSxJQUZnQixDQUVYakMsU0FGVyxFQUdoQmtDLE1BSGdCLENBR1QsVUFBQ25DLEdBQUQ7QUFBQSxXQUFTQSxHQUFHLENBQUNvQyxPQUFiO0FBQUEsR0FIUyxDQUFyQjs7QUFLQSxrQkFBd0MscUJBQVM7QUFDN0NDLElBQUFBLFdBQVcsRUFBRSxLQURnQztBQUU3Q1gsSUFBQUEsSUFBSSxFQUFFO0FBRnVDLEdBQVQsQ0FBeEM7QUFBQTtBQUFBLE1BQU9ZLFlBQVA7QUFBQSxNQUFxQkMsZUFBckI7O0FBS0Esd0JBQVUsWUFBTTtBQUNaLFFBQ0lwRSxZQUFZLEtBQUtDLDJCQUFqQixJQUNBLENBQUN1QywyQkFGTCxFQUdFO0FBQ0UsVUFBSWtCLFNBQVMsS0FBS1osVUFBVSxDQUFDckIsT0FBWCxDQUFtQmlDLFNBQXJDLEVBQWdEO0FBQzVDWixRQUFBQSxVQUFVLENBQUNyQixPQUFYLENBQW1CaUMsU0FBbkIsR0FBK0JBLFNBQS9CO0FBQ0g7O0FBQ0QsVUFBTVcsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDQyxDQUFELEVBQU87QUFDekIsWUFBSUMsU0FBUyxHQUFHLHlCQUFhM0IsS0FBYixDQUFoQjtBQUNBMkIsUUFBQUEsU0FBUyxDQUFDYixTQUFWLEdBQXNCWSxDQUFDLENBQUNFLE1BQUYsQ0FBU2QsU0FBL0I7QUFDQSxZQUFJdkIsWUFBWSxLQUFLLElBQXJCLEVBQTJCc0MsWUFBWSxDQUFDdEMsWUFBRCxDQUFaOztBQUMzQkQsUUFBQUEsYUFBYSxHQUFHLHlCQUFNO0FBQ2xCLGNBQUl3QixTQUFTLEtBQUtZLENBQUMsQ0FBQ0UsTUFBRixDQUFTZCxTQUEzQixFQUFzQztBQUNsQyxnQkFBSWEsVUFBUyxHQUFHLHlCQUFhM0IsS0FBYixDQUFoQjs7QUFDQTJCLFlBQUFBLFVBQVMsQ0FBQ2IsU0FBVixHQUFzQlksQ0FBQyxDQUFDRSxNQUFGLENBQVNkLFNBQS9CO0FBQ0FuQixZQUFBQSxpQkFBaUIsQ0FBQ2dDLFVBQUQsQ0FBakI7QUFDSDtBQUNKLFNBTkQ7O0FBT0FwQyxRQUFBQSxZQUFZLEdBQUd1QyxVQUFVLENBQUMsWUFBTTtBQUM1QnhDLFVBQUFBLGFBQWE7O0FBQ2JBLFVBQUFBLGFBQWEsR0FBRyx5QkFBTSxDQUFFLENBQXhCOztBQUNBQyxVQUFBQSxZQUFZLEdBQUcsSUFBZjtBQUNILFNBSndCLEVBSXRCLEdBSnNCLENBQXpCO0FBS0gsT0FoQkQ7O0FBaUJBVyxNQUFBQSxVQUFVLENBQUNyQixPQUFYLENBQW1Ca0QsZ0JBQW5CLENBQW9DLFFBQXBDLEVBQThDTixhQUE5QyxFQUE2RDtBQUN6RE8sUUFBQUEsT0FBTyxFQUFFO0FBRGdELE9BQTdEO0FBR0EsYUFBTyxZQUFNO0FBQ1QsWUFDSTVFLFlBQVksS0FBS0MsMkJBQWpCLElBQ0E2QyxVQUFVLENBQUNyQixPQUZmLEVBR0U7QUFDRXFCLFVBQUFBLFVBQVUsQ0FBQ3JCLE9BQVgsQ0FBbUJvRCxtQkFBbkIsQ0FDSSxRQURKLEVBRUlSLGFBRko7QUFJSDtBQUNKLE9BVkQ7QUFXSDtBQUNKLEdBeENELEVBd0NHLENBQUN2QixVQUFELEVBQWFQLGlCQUFiLENBeENIO0FBMENBLHdCQUFVLFlBQU07QUFDWjZCLElBQUFBLGVBQWUsQ0FBQztBQUFFRixNQUFBQSxXQUFXLEVBQUUsS0FBZjtBQUFzQlgsTUFBQUEsSUFBSSxFQUFFbkMsUUFBUSxDQUFDQyxZQUFEO0FBQXBDLEtBQUQsQ0FBZjtBQUNILEdBRkQsRUFFRyxDQUFDQSxZQUFZLENBQUNPLE1BQWQsQ0FGSDtBQUlBLHdCQUFVLFlBQU07QUFDWixRQUFJdUMsWUFBWSxDQUFDRCxXQUFqQixFQUE4QjtBQUMxQixVQUFJSyxTQUFTLEdBQUcseUJBQWEzQixLQUFiLENBQWhCO0FBQ0F2QixNQUFBQSxZQUFZLENBQUN5RCxPQUFiLENBQXFCLFVBQUNqRCxHQUFELEVBQU10QixLQUFOLEVBQWdCO0FBQ2pDZ0UsUUFBQUEsU0FBUyxDQUFDZCxJQUFWLENBQWU1QixHQUFHLENBQUNpQyxFQUFuQixFQUF1QnBDLEtBQXZCLEdBQStCeUMsWUFBWSxDQUFDWixJQUFiLENBQWtCaEQsS0FBbEIsQ0FBL0I7QUFDSCxPQUZEO0FBR0FnQyxNQUFBQSxpQkFBaUIsQ0FBQ2dDLFNBQUQsQ0FBakI7QUFDSDtBQUNKLEdBUkQsRUFRRyxDQUFDSixZQUFELENBUkg7O0FBVUEsTUFBTVksZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixTQUErQztBQUFBLFFBQTVDdEUsUUFBNEMsVUFBNUNBLFFBQTRDO0FBQUEsUUFBbENDLFVBQWtDLFVBQWxDQSxVQUFrQztBQUFBLFFBQXRCQyxRQUFzQixVQUF0QkEsUUFBc0I7QUFBQSxRQUFaQyxLQUFZLFVBQVpBLEtBQVk7QUFDcEUsV0FBTztBQUFFSCxNQUFBQSxRQUFRLEVBQVJBLFFBQUY7QUFBWUMsTUFBQUEsVUFBVSxFQUFWQSxVQUFaO0FBQXdCQyxNQUFBQSxRQUFRLEVBQVJBLFFBQXhCO0FBQWtDQyxNQUFBQSxLQUFLLEVBQUxBO0FBQWxDLEtBQVA7QUFDSCxHQUZEOztBQUlBLE1BQU1vRSxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLFFBQUQsRUFBYztBQUN0Q2IsSUFBQUEsZUFBZSxDQUFDO0FBQUVGLE1BQUFBLFdBQVcsRUFBRSxJQUFmO0FBQXFCWCxNQUFBQSxJQUFJLEVBQUUwQjtBQUEzQixLQUFELENBQWY7QUFDSCxHQUZEOztBQUlBLE1BQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNyRCxHQUFELEVBQU1zRCxnQkFBTjtBQUFBLFdBQ2pCLGtCQUFJQSxnQkFBSixZQUF5QnRELEdBQUcsQ0FBQ2lDLEVBQTdCLGVBQTJDLEtBQTNDLENBRGlCO0FBQUEsR0FBckI7O0FBR0EsTUFBTXNCLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ0MsU0FBRCxFQUFlO0FBQ2xDLFFBQU1DLGFBQWEsR0FBRzlCLE9BQU8sQ0FBQzZCLFNBQUQsQ0FBN0I7QUFDQSxRQUNJLG9CQUFNQyxhQUFOLEtBQ0EsQ0FBQ0EsYUFBYSxDQUFDckIsT0FEZixJQUVBNUMsWUFBWSxDQUFDTyxNQUFiLElBQXVCLENBSDNCLEVBS0ksT0FBTyxJQUFQO0FBQ0osUUFBTXVELGdCQUFnQixHQUFHeEIsTUFBTSxDQUFDQyxJQUFQLENBQVkwQixhQUFhLENBQUNDLE9BQTFCLEVBQW1DaEUsTUFBbkMsQ0FDckIsVUFBQ0MsR0FBRCxFQUFNZ0UsWUFBTixFQUF1QjtBQUNuQixVQUFNQyxhQUFhLEdBQUdILGFBQWEsQ0FBQ0MsT0FBZCxDQUFzQkMsWUFBdEIsQ0FBdEI7QUFDQWhFLE1BQUFBLEdBQUcsQ0FBQ2lFLGFBQWEsQ0FBQ0MsR0FBZixDQUFILEdBQXlCRCxhQUF6QjtBQUNBLGFBQU9qRSxHQUFQO0FBQ0gsS0FMb0IsRUFNckIsRUFOcUIsQ0FBekI7QUFTQSx3QkFDSSxzQkFBQyxhQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUMsa0NBRGQ7QUFBQSw4QkFJSSxxQkFBQyxVQUFEO0FBQ0ksUUFBQSxhQUFhLEVBQUUyQyxZQUFZLENBQUNaLElBQWIsQ0FBa0IsQ0FBbEI7QUFEbkIsU0FFUXdCLGdCQUFnQixDQUFDMUIsV0FBRCxDQUZ4QjtBQUFBLCtCQUlJO0FBQUEsb0JBQU9pQyxhQUFhLENBQUNLO0FBQXJCO0FBSkosU0FKSixFQVVLLG1CQUFJdEUsWUFBSixFQUFrQnVFLEtBQWxCLENBQXdCLENBQXhCLEVBQTJCakUsR0FBM0IsQ0FBK0IsVUFBQ0UsR0FBRCxFQUFNdEIsS0FBTjtBQUFBLDRCQUM1QixxQkFBQyxVQUFEO0FBRUksVUFBQSxhQUFhLEVBQUU0RCxZQUFZLENBQUNaLElBQWIsQ0FBa0JoRCxLQUFLLEdBQUcsQ0FBMUI7QUFGbkIsV0FHUXdFLGdCQUFnQixDQUFDMUIsV0FBRCxDQUh4QjtBQUFBLGlDQUtJO0FBQUEsc0JBQ0s2QixZQUFZLENBQUNyRCxHQUFELEVBQU1zRCxnQkFBTixDQUFaLEdBQ0tVLG9CQUFvQixDQUFDaEUsR0FBRCxFQUFNc0QsZ0JBQU4sQ0FEekIsR0FFSztBQUhWO0FBTEosK0JBQ3FCdEQsR0FBRyxDQUFDaUMsRUFEekIsRUFENEI7QUFBQSxPQUEvQixDQVZMO0FBQUEsMkJBRXNCdUIsU0FGdEIsRUFESjtBQTBCSCxHQTNDRDs7QUE2Q0EsTUFBTVMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsR0FBTTtBQUN2QixRQUFJLHNCQUFRdEMsT0FBUixDQUFKLEVBQXNCLE9BQU8sSUFBUDtBQUN0Qix3QkFDSSxxQkFBQyxhQUFELGtDQUNRSCxXQURSO0FBRUksTUFBQSxTQUFTLEVBQUMsMEJBRmQ7QUFBQSxnQkFJS00sTUFBTSxDQUFDQyxJQUFQLENBQVlKLE9BQVosRUFBcUI3QixHQUFyQixDQUF5QixVQUFDb0UsR0FBRDtBQUFBLGVBQVNYLGNBQWMsQ0FBQ1csR0FBRCxDQUF2QjtBQUFBLE9BQXpCO0FBSkwsT0FESjtBQVFILEdBVkQ7O0FBWUEsTUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsR0FBTTtBQUN0QixRQUFJaEcsWUFBWSxLQUFLaUcsc0JBQXJCLEVBQXNDO0FBQ2xDLGFBQU8sRUFBUDtBQUNIOztBQUNELFFBQUlqRyxZQUFZLEtBQUtDLDJCQUFyQixFQUEyQztBQUN2Qyw0QkFBMEIyQyxLQUExQixDQUFRc0QsUUFBUjtBQUFBLFVBQVFBLFFBQVIsZ0NBQW1CLEVBQW5CO0FBQ0EsYUFBT0EsUUFBUDtBQUNIOztBQUNELFdBQU8sQ0FBQ3JELENBQUMsQ0FBQyxvQ0FBRCxDQUFGLENBQVA7QUFDSCxHQVREOztBQVdBLE1BQU1zRCxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUN0RSxHQUFELEVBQU11RSxJQUFOLEVBQWU7QUFDbEMsUUFDSXBHLFlBQVksS0FBS3FHLHdCQUFqQixJQUNBQyw2QkFBbUJDLFFBQW5CLENBQTRCMUUsR0FBRyxDQUFDaUMsRUFBaEMsQ0FGSixFQUdFO0FBQ0UsYUFBTyx5QkFBYXNDLElBQUksQ0FBQ3ZFLEdBQUcsQ0FBQ2lDLEVBQUwsQ0FBakIsRUFBMkI7QUFDOUIwQyxRQUFBQSxRQUFRLEVBQUVuRSxXQURvQjtBQUU5Qk0sUUFBQUEsS0FBSyxFQUFFLFVBRnVCO0FBRzlCOEQsUUFBQUEsTUFBTSxFQUFFbkU7QUFIc0IsT0FBM0IsQ0FBUDtBQUtIOztBQUNELFdBQU84RCxJQUFJLENBQUN2RSxHQUFHLENBQUNpQyxFQUFMLENBQVg7QUFDSCxHQVpEOztBQWNBLE1BQU0rQixvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNoRSxHQUFELEVBQU1zRCxnQkFBTixFQUEyQjtBQUNwRCxRQUFNdkMsS0FBSyxHQUFHLGtCQUFJdUMsZ0JBQUosWUFBeUJ0RCxHQUFHLENBQUNpQyxFQUE3QixhQUF5QyxDQUF6QyxDQUFkOztBQUNBLFFBQ0k5RCxZQUFZLEtBQUtxRyx3QkFBakIsSUFDQUssK0JBQXFCSCxRQUFyQixDQUE4QjFFLEdBQUcsQ0FBQ2lDLEVBQWxDLENBRkosRUFHRTtBQUNFLGFBQU8seUJBQWFsQixLQUFiLEVBQW9CO0FBQ3ZCNEQsUUFBQUEsUUFBUSxFQUFFbkUsV0FEYTtBQUV2Qk0sUUFBQUEsS0FBSyxFQUFFLFVBRmdCO0FBR3ZCOEQsUUFBQUEsTUFBTSxFQUFFbkU7QUFIZSxPQUFwQixDQUFQO0FBS0g7O0FBQ0QsV0FBT00sS0FBUDtBQUNILEdBYkQ7O0FBZUEsc0JBQ0kscUJBQUMsU0FBRDtBQUNJLElBQUEsU0FBUyxFQUFDLCtCQURkO0FBRUksSUFBQSxjQUFjLEVBQUVLLFdBQVcsQ0FBQ3JELGNBRmhDO0FBR0ksSUFBQSxLQUFLLEVBQUVtRCxVQUhYO0FBQUEsMkJBS0ksc0JBQUMsWUFBRDtBQUFBLDhCQUNJLHFCQUFDLHdCQUFEO0FBQ0ksUUFBQSxXQUFXLEVBQUVFLFdBRGpCO0FBRUksUUFBQSxVQUFVLEVBQUVSLFVBRmhCO0FBR0ksUUFBQSxnQkFBZ0IsRUFBRUMsVUFIdEI7QUFJSSxRQUFBLGtCQUFrQixFQUFFc0MsbUJBSnhCO0FBS0ksUUFBQSxZQUFZLEVBQUUzRCxZQUxsQjtBQU1JLFFBQUEsS0FBSyxFQUFFOEMsWUFBWSxDQUFDWixJQU54QjtBQU9JLFFBQUEsU0FBUyxvQkFBT3dCLGdCQUFnQixDQUFDOUIsV0FBRCxDQUF2QixDQVBiO0FBUUksUUFBQSxRQUFRLEVBQUUrQyxXQUFXO0FBUnpCLFFBREosZUFXSSxzQkFBQyxXQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUMsaURBRGQ7QUFFSSwwQkFBZ0J0QyxTQUZwQjtBQUdJLFFBQUEsaUJBQWlCLEVBQ2IxRCxZQUFZLEtBQUtpRyxzQkFBakIsSUFDQSxDQUFDdkQsVUFERCxJQUVBRiwyQkFOUjtBQVFJLFFBQUEsR0FBRyxFQUFFTSxVQVJUO0FBU0ksUUFBQSxZQUFZLEVBQUU5QyxZQVRsQjtBQUFBLGdDQVdJLHFCQUFDLFdBQUQ7QUFDSSxVQUFBLGdCQUFnQixFQUFFbUQsU0FBUyxDQUFDaEQsZ0JBRGhDO0FBRUksVUFBQSxTQUFTLEVBQUMsd0JBRmQ7QUFBQSxvQkFJS29ELElBQUksQ0FBQzVCLEdBQUwsQ0FBUyxVQUFDeUUsSUFBRCxFQUFPN0YsS0FBUDtBQUFBLGdDQUNOLHFCQUFDLFdBQUQ7QUFDSSxjQUFBLGdCQUFnQixFQUFFNEMsU0FBUyxDQUFDN0MsZ0JBRGhDO0FBRUksY0FBQSxTQUFTLEVBQUMsdUJBRmQ7QUFJSSxjQUFBLEtBQUssRUFBRUMsS0FKWDtBQUtJLGNBQUEsWUFBWSxFQUFFNEMsU0FBUyxDQUFDOUMsWUFMNUI7QUFBQSx3QkFPS2dCLFlBQVksQ0FBQ00sR0FBYixDQUFpQixVQUFDRSxHQUFELEVBQU10QixLQUFOO0FBQUEsb0NBQ2QscUJBQUMsUUFBRDtBQUVJLGtCQUFBLGFBQWEsRUFBRTRELFlBQVksQ0FBQ1osSUFBYixDQUFrQmhELEtBQWxCO0FBRm5CLG1CQUdRd0UsZ0JBQWdCLENBQUM1QixTQUFELENBSHhCO0FBQUEseUNBS0k7QUFBQSw4QkFBT2dELGNBQWMsQ0FBQ3RFLEdBQUQsRUFBTXVFLElBQU47QUFBckI7QUFMSiw4QkFDWTdGLEtBRFosY0FDcUJzQixHQUFHLENBQUNpQyxFQUR6QixFQURjO0FBQUEsZUFBakI7QUFQTCxlQUdTdkQsS0FIVCxDQURNO0FBQUEsV0FBVDtBQUpMLFVBWEosRUFtQ0t1RixZQUFZLEVBbkNqQjtBQUFBLFFBWEo7QUFBQTtBQUxKLElBREo7QUF5REgsQ0E5UEQ7O0FBZ1FBMUQsV0FBVyxDQUFDdUUsU0FBWixHQUF3QjtBQUNwQnRFLEVBQUFBLFdBQVcsRUFBRXVFLG1CQUFVQyxNQURIO0FBRXBCdkUsRUFBQUEsY0FBYyxFQUFFc0UsbUJBQVVDLE1BRk47QUFHcEJDLEVBQUFBLGVBQWUsRUFBRUYsbUJBQVVHLE1BSFA7QUFJcEIvRyxFQUFBQSxZQUFZLEVBQUU0RyxtQkFBVUMsTUFKSjtBQUtwQnRFLEVBQUFBLGlCQUFpQixFQUFFcUUsbUJBQVVJLElBTFQ7QUFNcEJ4RSxFQUFBQSwyQkFBMkIsRUFBRW9FLG1CQUFVSyxJQU5uQjtBQU9wQnhFLEVBQUFBLFVBQVUsRUFBRW1FLG1CQUFVQyxNQVBGO0FBUXBCbkUsRUFBQUEsVUFBVSxFQUFFa0UsbUJBQVVLLElBUkY7QUFTcEJDLEVBQUFBLE9BQU8sRUFBRU4sbUJBQVVDLE1BVEM7QUFVcEJsRSxFQUFBQSxLQUFLLEVBQUVpRSxtQkFBVUcsTUFWRztBQVdwQm5FLEVBQUFBLEtBQUssRUFBRWdFLG1CQUFVRztBQVhHLENBQXhCO2VBY2UzRSxXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldCBmcm9tICdsb2Rhc2gvZ2V0JztcbmltcG9ydCBpc0VtcHR5IGZyb20gJ2xvZGFzaC9pc0VtcHR5JztcbmltcG9ydCBpc05pbCBmcm9tICdsb2Rhc2gvaXNOaWwnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QsIHVzZVJlZiB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHsgVEFCTEVfREFUQV9DT0xVTU5TLCBUQUJMRV9GT09URVJfQ09MVU1OUyB9IGZyb20gJ0Bjb25zdGFudHMvdGVtcGxhdGUnO1xuaW1wb3J0IHtcbiAgICBGVUxMX0VESVRJT05fTU9ERSxcbiAgICBOT19FRElUSU9OX01PREUsXG4gICAgUEFSVElBTF9FRElUSU9OX01PREUsXG59IGZyb20gJ0Bjb25zdGFudHMvdHlwZXMnO1xuXG5pbXBvcnQgeyBjbG9uZUVsZW1lbnQsIG51bWJlckZvcm1hdCB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcblxuaW1wb3J0IFRhYmxlSGVhZGVyVmlldyBmcm9tICcuL3RhYmxlL1RhYmxlSGVhZGVyVmlldyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgJHsoeyByb3VuZGVkQm9yZGVycyB9KSA9PiByb3VuZGVkQm9yZGVycyAmJiAnYm9yZGVyLXJhZGl1czogMTdweCAxN3B4IDAgMDsnfVxuICAgIGJvcmRlci10b3Atd2lkdGg6IDBweCAhaW1wb3J0YW50O1xuYDtcblxuY29uc3QgVGFibGVDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuYDtcblxuY29uc3QgRGF0YUNvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmbGV4OiAxO1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBvdmVyZmxvdy15OiAkeyh7IG92ZXJmbG93WURpc2FibGVkLCBlZGl0aW9uTGV2ZWwgfSkgPT5cbiAgICAgICAgb3ZlcmZsb3dZRGlzYWJsZWRcbiAgICAgICAgICAgID8gJ2hpZGRlbidcbiAgICAgICAgICAgIDogZWRpdGlvbkxldmVsID09PSBQQVJUSUFMX0VESVRJT05fTU9ERVxuICAgICAgICAgICAgPyAnb3ZlcmxheSdcbiAgICAgICAgICAgIDogJ2F1dG8nfTtcbmA7XG5cbmNvbnN0IEJvZHlDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBiYWNrZ3JvdW5kQ29sb3IxID0gJyNmZmYnIH0pID0+IGJhY2tncm91bmRDb2xvcjF9O1xuYDtcblxuY29uc3QgQm9keURhdGFSb3cgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgJHsoeyBpc1plYnJhU3R5bGUsIGJhY2tncm91bmRDb2xvcjIsIGluZGV4IH0pID0+XG4gICAgICAgIGlzWmVicmFTdHlsZSAmJlxuICAgICAgICAoaW5kZXggKyAxKSAlIDIgPT09IDAgJiZcbiAgICAgICAgYGJhY2tncm91bmQtY29sb3I6ICR7YmFja2dyb3VuZENvbG9yMn07YH1cbmA7XG5cbmNvbnN0IEJvZHlEYXRhID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtd2VpZ2h0OiAkeyh7IGZvbnRCb2xkIH0pID0+IChmb250Qm9sZCA/ICdib2xkJyA6ICcxMDAnKX07XG4gICAgJHsoeyBmb250SXRhbGljIH0pID0+IGZvbnRJdGFsaWMgJiYgJ2ZvbnQtc3R5bGU6IGl0YWxpYzsnfVxuICAgIGZvbnQtc2l6ZTogJHsoeyBmb250U2l6ZSA9ICcxM3B4JyB9KSA9PiBmb250U2l6ZX07XG4gICAgbGluZS1oZWlnaHQ6ICR7KHsgZm9udFNpemUgPSAnMTNweCcgfSkgPT4gZm9udFNpemV9O1xuICAgIGNvbG9yOiAkeyh7IGNvbG9yID0gJyMwMDAnIH0pID0+IGNvbG9yfTtcbiAgICAkeyh7IHJlbGF0aXZlV2lkdGggfSkgPT5cbiAgICAgICAgIWlzTmlsKHJlbGF0aXZlV2lkdGgpICYmIGB3aWR0aDogJHtyZWxhdGl2ZVdpZHRofSU7YH1cbiAgICBwYWRkaW5nOiA4cHggMTVweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHNwYW4ge1xuICAgICAgICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgIH1cbmA7XG5cbmNvbnN0IEZvb3RlckNvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkeyh7IGJhY2tncm91bmRDb2xvcjEgPSAnI2ZmZicgfSkgPT4gYmFja2dyb3VuZENvbG9yMX07XG4gICAgYm9yZGVyLXRvcC1zdHlsZTogJHsoeyBib3JkZXJUb3BTdHlsZSB9KSA9PiBib3JkZXJUb3BTdHlsZX07XG4gICAgYm9yZGVyLXRvcC13aWR0aDogJHsoeyBib3JkZXJUb3BXaWR0aCB9KSA9PiBib3JkZXJUb3BXaWR0aH07XG4gICAgYm9yZGVyLXRvcC1jb2xvcjogJHsoeyBib3JkZXJUb3BDb2xvciB9KSA9PiBib3JkZXJUb3BDb2xvcn07XG4gICAgJHsoeyBpc1plYnJhU3R5bGUsIGJhY2tncm91bmRDb2xvcjIgfSkgPT5cbiAgICAgICAgaXNaZWJyYVN0eWxlICYmXG4gICAgICAgIGAuZm9vdGVyLXJvdzpudGgtY2hpbGQoZXZlbikge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHtiYWNrZ3JvdW5kQ29sb3IyfTtcbiAgICAgICAgfWB9XG5gO1xuXG5jb25zdCBGb290ZXJEYXRhUm93ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuYDtcblxuY29uc3QgRm9vdGVyRGF0YSA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmb250LXdlaWdodDogJHsoeyBmb250Qm9sZCB9KSA9PiAoZm9udEJvbGQgPyAnYm9sZCcgOiAnMTAwJyl9O1xuICAgICR7KHsgZm9udEl0YWxpYyB9KSA9PiBmb250SXRhbGljICYmICdmb250LXN0eWxlOiBpdGFsaWM7J31cbiAgICBmb250LXNpemU6ICR7KHsgZm9udFNpemUgPSAnMTNweCcgfSkgPT4gZm9udFNpemV9O1xuICAgIGxpbmUtaGVpZ2h0OiAkeyh7IGZvbnRTaXplID0gJzEzcHgnIH0pID0+IGZvbnRTaXplfTtcbiAgICBjb2xvcjogJHsoeyBjb2xvciA9ICcjMDAwJyB9KSA9PiBjb2xvcn07XG4gICAgJHsoeyByZWxhdGl2ZVdpZHRoIH0pID0+XG4gICAgICAgICFpc05pbChyZWxhdGl2ZVdpZHRoKSAmJiBgd2lkdGg6ICR7cmVsYXRpdmVXaWR0aH0lO2B9XG4gICAgcGFkZGluZzogOHB4IDE1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBzcGFuIHtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBwcmUtbGluZTtcbiAgICB9XG5gO1xuXG5jb25zdCBnZXRTaXplcyA9IChwcmVwYXJlZENvbHMpID0+IHtcbiAgICBjb25zdCB3aXRob3V0V2lkdGhOdW0gPSBwcmVwYXJlZENvbHMucmVkdWNlKChhY2MsIGN1cnJlbnQpID0+IHtcbiAgICAgICAgaWYgKGlzTmlsKGN1cnJlbnQud2lkdGgpKSB7XG4gICAgICAgICAgICArK2FjYztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYWNjO1xuICAgIH0sIDApO1xuICAgIGlmICh3aXRob3V0V2lkdGhOdW0gPiAwKSB7XG4gICAgICAgIHJldHVybiBwcmVwYXJlZENvbHMubWFwKCgpID0+IDEwMCAvIHByZXBhcmVkQ29scy5sZW5ndGgpO1xuICAgIH1cbiAgICByZXR1cm4gcHJlcGFyZWRDb2xzLm1hcCgoY29sKSA9PiBjb2wud2lkdGgpO1xufTtcblxuY29uc3Qgb3JkZXJTb3J0ID0gKGEsIGIpID0+IHtcbiAgICBpZiAoYS5vcmRlciA+IGIub3JkZXIpIHJldHVybiAxO1xuICAgIGlmIChiLm9yZGVyID4gYS5vcmRlcikgcmV0dXJuIC0xO1xuICAgIHJldHVybiAwO1xufTtcblxubGV0IHByb2Nlc3NBY3Rpb24gPSAoKSA9PiB7fTtcbmxldCBwcm9jZXNzRGVsYXkgPSBudWxsO1xuXG5jb25zdCBUYWJsZVdpZGdldCA9ICh7XG4gICAgY3VycmVuY3lJc28sXG4gICAgY3VycmVuY3lMb2NhbGUsXG4gICAgZWRpdGlvbkxldmVsLFxuICAgIGhhbmRsZUNoYW5nZVZhbHVlLFxuICAgIGluZmluaXRlUGFnZXNTdXBwb3J0RW5hYmxlZCxcbiAgICBpbm5lcldpZHRoLFxuICAgIGlzU2VsZWN0ZWQsXG4gICAgc3R5bGUgPSB7fSxcbiAgICB2YWx1ZSA9IHt9LFxufSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCBjb250ZW50UmVmID0gdXNlUmVmKG51bGwpO1xuXG4gICAgY29uc3Qge1xuICAgICAgICB0YWJsZTogdGFibGVTdHlsZSxcbiAgICAgICAgaGVhZGVyOiBoZWFkZXJTdHlsZSxcbiAgICAgICAgYm9keTogYm9keVN0eWxlLFxuICAgICAgICBmb290ZXI6IGZvb3RlclN0eWxlLFxuICAgIH0gPSBzdHlsZTtcblxuICAgIGNvbnN0IHsgZGF0YSA9IFtdLCByZXN1bHRzID0ge30sIGNvbHMgPSB7fSwgc2Nyb2xsVG9wID0gMCB9ID0gdmFsdWU7XG5cbiAgICBjb25zdCBwcmVwYXJlZENvbHMgPSBPYmplY3Qua2V5cyhjb2xzKVxuICAgICAgICAubWFwKChjb2xLZXkpID0+ICh7IC4uLmNvbHNbY29sS2V5XSwgaWQ6IGNvbEtleSB9KSlcbiAgICAgICAgLnNvcnQob3JkZXJTb3J0KVxuICAgICAgICAuZmlsdGVyKChjb2wpID0+IGNvbC52aXNpYmxlKTtcblxuICAgIGNvbnN0IFtjdXJyZW50U2l6ZXMsIHNldEN1cnJlbnRTaXplc10gPSB1c2VTdGF0ZSh7XG4gICAgICAgIHBlbmRpbmdTYXZlOiBmYWxzZSxcbiAgICAgICAgZGF0YTogW10sXG4gICAgfSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICBlZGl0aW9uTGV2ZWwgPT09IFBBUlRJQUxfRURJVElPTl9NT0RFICYmXG4gICAgICAgICAgICAhaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkXG4gICAgICAgICkge1xuICAgICAgICAgICAgaWYgKHNjcm9sbFRvcCAhPT0gY29udGVudFJlZi5jdXJyZW50LnNjcm9sbFRvcCkge1xuICAgICAgICAgICAgICAgIGNvbnRlbnRSZWYuY3VycmVudC5zY3JvbGxUb3AgPSBzY3JvbGxUb3A7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBvblNjcm9sbFRhYmxlID0gKGUpID0+IHtcbiAgICAgICAgICAgICAgICBsZXQgdGVtcFZhbHVlID0gY2xvbmVFbGVtZW50KHZhbHVlKTtcbiAgICAgICAgICAgICAgICB0ZW1wVmFsdWUuc2Nyb2xsVG9wID0gZS50YXJnZXQuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgIGlmIChwcm9jZXNzRGVsYXkgIT09IG51bGwpIGNsZWFyVGltZW91dChwcm9jZXNzRGVsYXkpO1xuICAgICAgICAgICAgICAgIHByb2Nlc3NBY3Rpb24gPSAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzY3JvbGxUb3AgIT09IGUudGFyZ2V0LnNjcm9sbFRvcCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHRlbXBWYWx1ZSA9IGNsb25lRWxlbWVudCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wVmFsdWUuc2Nyb2xsVG9wID0gZS50YXJnZXQuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2hhbmdlVmFsdWUodGVtcFZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgcHJvY2Vzc0RlbGF5ID0gc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHByb2Nlc3NBY3Rpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgcHJvY2Vzc0FjdGlvbiA9ICgpID0+IHt9O1xuICAgICAgICAgICAgICAgICAgICBwcm9jZXNzRGVsYXkgPSBudWxsO1xuICAgICAgICAgICAgICAgIH0sIDEwMCk7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgY29udGVudFJlZi5jdXJyZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIG9uU2Nyb2xsVGFibGUsIHtcbiAgICAgICAgICAgICAgICBwYXNzaXZlOiB0cnVlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICAgICAgZWRpdGlvbkxldmVsID09PSBQQVJUSUFMX0VESVRJT05fTU9ERSAmJlxuICAgICAgICAgICAgICAgICAgICBjb250ZW50UmVmLmN1cnJlbnRcbiAgICAgICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGVudFJlZi5jdXJyZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXG4gICAgICAgICAgICAgICAgICAgICAgICAnc2Nyb2xsJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uU2Nyb2xsVGFibGVcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgfSwgW2NvbnRlbnRSZWYsIGhhbmRsZUNoYW5nZVZhbHVlXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBzZXRDdXJyZW50U2l6ZXMoeyBwZW5kaW5nU2F2ZTogZmFsc2UsIGRhdGE6IGdldFNpemVzKHByZXBhcmVkQ29scykgfSk7XG4gICAgfSwgW3ByZXBhcmVkQ29scy5sZW5ndGhdKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIGlmIChjdXJyZW50U2l6ZXMucGVuZGluZ1NhdmUpIHtcbiAgICAgICAgICAgIGxldCB0ZW1wVmFsdWUgPSBjbG9uZUVsZW1lbnQodmFsdWUpO1xuICAgICAgICAgICAgcHJlcGFyZWRDb2xzLmZvckVhY2goKGNvbCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgICAgICB0ZW1wVmFsdWUuY29sc1tjb2wuaWRdLndpZHRoID0gY3VycmVudFNpemVzLmRhdGFbaW5kZXhdO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSh0ZW1wVmFsdWUpO1xuICAgICAgICB9XG4gICAgfSwgW2N1cnJlbnRTaXplc10pO1xuXG4gICAgY29uc3QgZ2V0RGF0YVRleHRQcm9wcyA9ICh7IGZvbnRCb2xkLCBmb250SXRhbGljLCBmb250U2l6ZSwgY29sb3IgfSkgPT4ge1xuICAgICAgICByZXR1cm4geyBmb250Qm9sZCwgZm9udEl0YWxpYywgZm9udFNpemUsIGNvbG9yIH07XG4gICAgfTtcblxuICAgIGNvbnN0IF9vblNhdmVDb2x1bW5zV2lkdGggPSAobmV3U2l6ZXMpID0+IHtcbiAgICAgICAgc2V0Q3VycmVudFNpemVzKHsgcGVuZGluZ1NhdmU6IHRydWUsIGRhdGE6IG5ld1NpemVzIH0pO1xuICAgIH07XG5cbiAgICBjb25zdCBnZXRJc1Zpc2libGUgPSAoY29sLCBlbGVtZW50c0J5Q29sdW1uKSA9PlxuICAgICAgICBnZXQoZWxlbWVudHNCeUNvbHVtbiwgYCR7Y29sLmlkfS52aXNpYmxlYCwgZmFsc2UpO1xuXG4gICAgY29uc3QgcmVuZGVyRXh0cmFSb3cgPSAocmVzdWx0S2V5KSA9PiB7XG4gICAgICAgIGNvbnN0IHJlc3VsdEVsZW1lbnQgPSByZXN1bHRzW3Jlc3VsdEtleV07XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIGlzTmlsKHJlc3VsdEVsZW1lbnQpIHx8XG4gICAgICAgICAgICAhcmVzdWx0RWxlbWVudC52aXNpYmxlIHx8XG4gICAgICAgICAgICBwcmVwYXJlZENvbHMubGVuZ3RoIDw9IDBcbiAgICAgICAgKVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIGNvbnN0IGVsZW1lbnRzQnlDb2x1bW4gPSBPYmplY3Qua2V5cyhyZXN1bHRFbGVtZW50LmNvbHVtbnMpLnJlZHVjZShcbiAgICAgICAgICAgIChhY2MsIGN1cnJlbnRWYWx1ZSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNvbHVtbkVsZW1lbnQgPSByZXN1bHRFbGVtZW50LmNvbHVtbnNbY3VycmVudFZhbHVlXTtcbiAgICAgICAgICAgICAgICBhY2NbY29sdW1uRWxlbWVudC5yZWZdID0gY29sdW1uRWxlbWVudDtcbiAgICAgICAgICAgICAgICByZXR1cm4gYWNjO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHt9XG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxGb290ZXJEYXRhUm93XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm9vdGVyLXJvdyB3aWRnZXQtdGFibGUtZGF0YS1yb3dcIlxuICAgICAgICAgICAgICAgIGtleT17YHRhYmxlLXJvdy0ke3Jlc3VsdEtleX1gfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb290ZXJEYXRhXG4gICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlV2lkdGg9e2N1cnJlbnRTaXplcy5kYXRhWzBdfVxuICAgICAgICAgICAgICAgICAgICB7Li4uZ2V0RGF0YVRleHRQcm9wcyhmb290ZXJTdHlsZSl9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8c3Bhbj57cmVzdWx0RWxlbWVudC5sYWJlbH08L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhPlxuICAgICAgICAgICAgICAgIHtbLi4ucHJlcGFyZWRDb2xzXS5zbGljZSgxKS5tYXAoKGNvbCwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgICAgPEZvb3RlckRhdGFcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHN1YnRvdGFsLSR7Y29sLmlkfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICByZWxhdGl2ZVdpZHRoPXtjdXJyZW50U2l6ZXMuZGF0YVtpbmRleCArIDFdfVxuICAgICAgICAgICAgICAgICAgICAgICAgey4uLmdldERhdGFUZXh0UHJvcHMoZm9vdGVyU3R5bGUpfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0SXNWaXNpYmxlKGNvbCwgZWxlbWVudHNCeUNvbHVtbilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBWYWx1ZUZvb3RlckZvcm1hdHRlZChjb2wsIGVsZW1lbnRzQnlDb2x1bW4pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogbnVsbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhPlxuICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9Gb290ZXJEYXRhUm93PlxuICAgICAgICApO1xuICAgIH07XG5cbiAgICBjb25zdCByZW5kZXJGb290ZXIgPSAoKSA9PiB7XG4gICAgICAgIGlmIChpc0VtcHR5KHJlc3VsdHMpKSByZXR1cm4gbnVsbDtcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxGb290ZXJDb250ZW50XG4gICAgICAgICAgICAgICAgey4uLmZvb3RlclN0eWxlfVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIndpZGdldC10YWJsZS1kYXRhLWZvb3RlclwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge09iamVjdC5rZXlzKHJlc3VsdHMpLm1hcCgoa2V5KSA9PiByZW5kZXJFeHRyYVJvdyhrZXkpKX1cbiAgICAgICAgICAgIDwvRm9vdGVyQ29udGVudD5cbiAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgY29uc3QgZ2V0V2FybmluZ3MgPSAoKSA9PiB7XG4gICAgICAgIGlmIChlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERSkge1xuICAgICAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlZGl0aW9uTGV2ZWwgPT09IFBBUlRJQUxfRURJVElPTl9NT0RFKSB7XG4gICAgICAgICAgICBjb25zdCB7IHdhcm5pbmdzID0gW10gfSA9IHZhbHVlO1xuICAgICAgICAgICAgcmV0dXJuIHdhcm5pbmdzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBbdCgnVGhlIGRhdGEgc2hvd24gaXMgZm9yIGV4YW1wbGUgb25seScpXTtcbiAgICB9O1xuXG4gICAgY29uc3QgVmFsdWVGb3JtYXR0ZWQgPSAoY29sLCBpdGVtKSA9PiB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUgJiZcbiAgICAgICAgICAgIFRBQkxFX0RBVEFfQ09MVU1OUy5pbmNsdWRlcyhjb2wuaWQpXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIG51bWJlckZvcm1hdChpdGVtW2NvbC5pZF0sIHtcbiAgICAgICAgICAgICAgICBjdXJyZW5jeTogY3VycmVuY3lJc28sXG4gICAgICAgICAgICAgICAgc3R5bGU6ICdjdXJyZW5jeScsXG4gICAgICAgICAgICAgICAgbG9jYWxlOiBjdXJyZW5jeUxvY2FsZSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBpdGVtW2NvbC5pZF07XG4gICAgfTtcblxuICAgIGNvbnN0IFZhbHVlRm9vdGVyRm9ybWF0dGVkID0gKGNvbCwgZWxlbWVudHNCeUNvbHVtbikgPT4ge1xuICAgICAgICBjb25zdCB2YWx1ZSA9IGdldChlbGVtZW50c0J5Q29sdW1uLCBgJHtjb2wuaWR9LnZhbHVlYCwgMCk7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUgJiZcbiAgICAgICAgICAgIFRBQkxFX0ZPT1RFUl9DT0xVTU5TLmluY2x1ZGVzKGNvbC5pZClcbiAgICAgICAgKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVtYmVyRm9ybWF0KHZhbHVlLCB7XG4gICAgICAgICAgICAgICAgY3VycmVuY3k6IGN1cnJlbmN5SXNvLFxuICAgICAgICAgICAgICAgIHN0eWxlOiAnY3VycmVuY3knLFxuICAgICAgICAgICAgICAgIGxvY2FsZTogY3VycmVuY3lMb2NhbGUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIndpZGdldC10YWJsZS1jb250YWluZXItcmVzaXplXCJcbiAgICAgICAgICAgIHJvdW5kZWRCb3JkZXJzPXtoZWFkZXJTdHlsZS5yb3VuZGVkQm9yZGVyc31cbiAgICAgICAgICAgIHN0eWxlPXt0YWJsZVN0eWxlfVxuICAgICAgICA+XG4gICAgICAgICAgICA8VGFibGVDb250ZW50PlxuICAgICAgICAgICAgICAgIDxUYWJsZUhlYWRlclZpZXdcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyU3R5bGU9e2hlYWRlclN0eWxlfVxuICAgICAgICAgICAgICAgICAgICBpbm5lcldpZHRoPXtpbm5lcldpZHRofVxuICAgICAgICAgICAgICAgICAgICBpc1dpZGdldFNlbGVjdGVkPXtpc1NlbGVjdGVkfVxuICAgICAgICAgICAgICAgICAgICBvblNhdmVDb2x1bW5zV2lkdGg9e19vblNhdmVDb2x1bW5zV2lkdGh9XG4gICAgICAgICAgICAgICAgICAgIHByZXBhcmVkQ29scz17cHJlcGFyZWRDb2xzfVxuICAgICAgICAgICAgICAgICAgICBzaXplcz17Y3VycmVudFNpemVzLmRhdGF9XG4gICAgICAgICAgICAgICAgICAgIHRleHRTdHlsZT17eyAuLi5nZXREYXRhVGV4dFByb3BzKGhlYWRlclN0eWxlKSB9fVxuICAgICAgICAgICAgICAgICAgICB3YXJuaW5ncz17Z2V0V2FybmluZ3MoKX1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxEYXRhQ29udGVudFxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJwdXBwZXRlZXItc2Nyb2xsaW5nIHdpZGdldC10YWJsZS1kYXRhLWNvbnRhaW5lclwiXG4gICAgICAgICAgICAgICAgICAgIGRhdGEtc2Nyb2xsdG9wPXtzY3JvbGxUb3B9XG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93WURpc2FibGVkPXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVkaXRpb25MZXZlbCA9PT0gTk9fRURJVElPTl9NT0RFIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAhaXNTZWxlY3RlZCB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmVmPXtjb250ZW50UmVmfVxuICAgICAgICAgICAgICAgICAgICBlZGl0aW9uTGV2ZWw9e2VkaXRpb25MZXZlbH1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxCb2R5Q29udGVudFxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yMT17Ym9keVN0eWxlLmJhY2tncm91bmRDb2xvcjF9XG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ3aWRnZXQtdGFibGUtZGF0YS1ib2R5XCJcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAge2RhdGEubWFwKChpdGVtLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCb2R5RGF0YVJvd1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3IyPXtib2R5U3R5bGUuYmFja2dyb3VuZENvbG9yMn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwid2lkZ2V0LXRhYmxlLWRhdGEtcm93XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtpbmRleH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg9e2luZGV4fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1plYnJhU3R5bGU9e2JvZHlTdHlsZS5pc1plYnJhU3R5bGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7cHJlcGFyZWRDb2xzLm1hcCgoY29sLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJvZHlEYXRhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgJHtpbmRleH0tJHtjb2wuaWR9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGl2ZVdpZHRoPXtjdXJyZW50U2l6ZXMuZGF0YVtpbmRleF19XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLmdldERhdGFUZXh0UHJvcHMoYm9keVN0eWxlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57VmFsdWVGb3JtYXR0ZWQoY29sLCBpdGVtKX08L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0JvZHlEYXRhPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0JvZHlEYXRhUm93PlxuICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgIDwvQm9keUNvbnRlbnQ+XG4gICAgICAgICAgICAgICAgICAgIHtyZW5kZXJGb290ZXIoKX1cbiAgICAgICAgICAgICAgICA8L0RhdGFDb250ZW50PlxuICAgICAgICAgICAgPC9UYWJsZUNvbnRlbnQ+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5UYWJsZVdpZGdldC5wcm9wVHlwZXMgPSB7XG4gICAgY3VycmVuY3lJc286IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY3VycmVuY3lMb2NhbGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGVmYXVsdEN1cnJlbmN5OiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGVkaXRpb25MZXZlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBoYW5kbGVDaGFuZ2VWYWx1ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaW5maW5pdGVQYWdlc1N1cHBvcnRFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpbm5lcldpZHRoOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGlzU2VsZWN0ZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGtleU5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBUYWJsZVdpZGdldDtcbiJdfQ==