"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _maps = require("../../../../constants/maps");

var _types = require("../../../../constants/types");

var _SegmentsMap = _interopRequireDefault(require("./SegmentsMap"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    overflow-y: auto;\n    position: relative;\n    width: 100%;\n"])));

var ExampleAlert = _styledComponents.default.span(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    font-size: 11px;\n    padding: 4px 6px;\n    background-color: #d32f2f;\n    color: #fff;\n    z-index: 1;\n"])));

var StyledMap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    background-color: #c5c5c5;\n    bottom: 0;\n    height: ", ";\n    left: 0;\n    position: relative;\n    right: 0;\n    top: 0;\n"])), function (_ref) {
  var heightMap = _ref.heightMap;
  return heightMap ? "".concat(heightMap, "px") : '100%';
});

var SegmentsMapWidget = function SegmentsMapWidget() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      editionLevel = _ref2.editionLevel,
      handleChangeValue = _ref2.handleChangeValue,
      innerHeight = _ref2.innerHeight,
      style = _ref2.style,
      _ref2$value = _ref2.value,
      value = _ref2$value === void 0 ? {} : _ref2$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var parent_field_segments = value.parent_field_segments,
      currentPosition = value.currentPosition;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      markers = _useState2[0],
      setMarkers = _useState2[1];

  var _useState3 = (0, _react.useState)(currentPosition.zoom),
      _useState4 = _slicedToArray(_useState3, 2),
      tempCurrentZoom = _useState4[0],
      setTempCurrentZoom = _useState4[1];

  var _useState5 = (0, _react.useState)({
    lat: currentPosition.lat,
    lng: currentPosition.lng
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      tempCurrentCenter = _useState6[0],
      setTempCurrentCenter = _useState6[1];

  var _style$segment = style.segment,
      segment = _style$segment === void 0 ? {} : _style$segment,
      _style$showLabels = style.showLabels,
      showLabels = _style$showLabels === void 0 ? true : _style$showLabels,
      _style$showProjectMar = style.showProjectMarker,
      showProjectMarker = _style$showProjectMar === void 0 ? true : _style$showProjectMar,
      _style$solarModule = style.solarModule,
      solarModule = _style$solarModule === void 0 ? {} : _style$solarModule;
  var _segment$fillColor = segment.fillColor,
      segmentFillColor = _segment$fillColor === void 0 ? _maps.SEGMENT_COLOR : _segment$fillColor,
      _segment$strokeColor = segment.strokeColor,
      segmentStrokeColor = _segment$strokeColor === void 0 ? _maps.SEGMENT_STROKE_COLOR : _segment$strokeColor;
  var _solarModule$fillColo = solarModule.fillColor,
      solarModuleFillColor = _solarModule$fillColo === void 0 ? _maps.SOLAR_MODULE_COLOR : _solarModule$fillColo,
      _solarModule$strokeCo = solarModule.strokeColor,
      solarModuleStrokeColor = _solarModule$strokeCo === void 0 ? _maps.SOLAR_MODULE_STROKE_COLOR : _solarModule$strokeCo;
  (0, _react.useEffect)(function () {
    if (!(0, _isEmpty.default)(parent_field_segments)) {
      setMarkers([].concat(_toConsumableArray(markers), [{
        title: t('Project'),
        lat: parent_field_segments.latitude,
        lng: parent_field_segments.longitude
      }]));
    }
  }, []);
  (0, _react.useEffect)(function () {
    if (currentPosition.zoom !== tempCurrentZoom) {
      handleChangeValue(_objectSpread(_objectSpread({}, value), {}, {
        currentPosition: _objectSpread(_objectSpread({}, currentPosition), {}, {
          zoom: tempCurrentZoom
        })
      }));
    }
  }, [tempCurrentZoom]);
  (0, _react.useEffect)(function () {
    if (currentPosition.lat !== tempCurrentCenter.lat || currentPosition.lng !== tempCurrentCenter.lng) {
      handleChangeValue(_objectSpread(_objectSpread({}, value), {}, {
        currentPosition: _objectSpread(_objectSpread({}, currentPosition), {}, {
          lat: tempCurrentCenter.lat,
          lng: tempCurrentCenter.lng
        })
      }));
    }
  }, [tempCurrentCenter]);

  var _handleZoomChanged = function _handleZoomChanged(newZoom) {
    return setTempCurrentZoom(newZoom);
  };

  var _handleCenterChanged = function _handleCenterChanged(newPosition) {
    return setTempCurrentCenter({
      lat: newPosition.lat(),
      lng: newPosition.lng()
    });
  };

  if (editionLevel === _types.FULL_EDITION_MODE || !(parent_field_segments && parent_field_segments.field_segments)) {
    return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ExampleAlert, {
        children: t('The segment and panels shown in this map are for reference')
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledMap, {
        heightMap: innerHeight,
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_SegmentsMap.default, {
          clickable: false,
          height: "100%",
          initCenter: {
            latitude: 20.9891608,
            longitude: -89.6114954
          },
          projectLocation: {
            latitude: 20.9891477,
            longitude: -89.6115384
          },
          scaleControl: false,
          segmentFillColor: segmentFillColor,
          segments: [_maps.EXAMPLE_SEGMENT],
          segmentStrokeColor: segmentStrokeColor,
          showLabels: showLabels,
          showProjectMarker: showProjectMarker,
          solarModuleFillColor: solarModuleFillColor,
          solarModuleStrokeColor: solarModuleStrokeColor,
          width: "100%",
          zoom: 20,
          zoomControl: false
        })
      })]
    });
  }

  var segments = parent_field_segments.field_segments.map(function (segment) {
    var solar_modules = segment.solar_modules.map(function (solarModule) {
      return solarModule.solar_module_points.map(function (point) {
        return [point.y_coordinate, point.x_coordinate];
      });
    });
    var polygon = segment.field_segment_points.map(function (point) {
      return [point.y_coordinate, point.x_coordinate];
    });
    return {
      id: segment.id,
      name: segment.name,
      polygon: polygon,
      solar_modules: solar_modules
    };
  });
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledMap, {
      heightMap: innerHeight,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_SegmentsMap.default, {
        clickable: editionLevel !== _types.NO_EDITION_MODE,
        handleCenterChanged: _handleCenterChanged,
        handleZoomChanged: _handleZoomChanged,
        height: "100%",
        initCenter: {
          latitude: tempCurrentCenter.lat,
          longitude: tempCurrentCenter.lng
        },
        projectLocation: {
          latitude: parent_field_segments.latitude,
          longitude: parent_field_segments.longitude
        },
        scaleControl: editionLevel !== _types.NO_EDITION_MODE,
        segmentFillColor: segmentFillColor,
        segments: segments,
        segmentStrokeColor: segmentStrokeColor,
        showLabels: showLabels,
        showProjectMarker: showProjectMarker,
        solarModuleFillColor: solarModuleFillColor,
        solarModuleStrokeColor: solarModuleStrokeColor,
        width: "100%",
        zoom: tempCurrentZoom,
        zoomControl: editionLevel !== _types.NO_EDITION_MODE
      })
    })
  });
};

SegmentsMapWidget.propTypes = {
  editionLevel: _propTypes.default.string,
  handleChangeValue: _propTypes.default.func,
  innerHeight: _propTypes.default.number,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = SegmentsMapWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU2VnbWVudHNNYXBXaWRnZXQuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiRXhhbXBsZUFsZXJ0Iiwic3BhbiIsIlN0eWxlZE1hcCIsImhlaWdodE1hcCIsIlNlZ21lbnRzTWFwV2lkZ2V0IiwiZWRpdGlvbkxldmVsIiwiaGFuZGxlQ2hhbmdlVmFsdWUiLCJpbm5lckhlaWdodCIsInN0eWxlIiwidmFsdWUiLCJ0IiwicGFyZW50X2ZpZWxkX3NlZ21lbnRzIiwiY3VycmVudFBvc2l0aW9uIiwibWFya2VycyIsInNldE1hcmtlcnMiLCJ6b29tIiwidGVtcEN1cnJlbnRab29tIiwic2V0VGVtcEN1cnJlbnRab29tIiwibGF0IiwibG5nIiwidGVtcEN1cnJlbnRDZW50ZXIiLCJzZXRUZW1wQ3VycmVudENlbnRlciIsInNlZ21lbnQiLCJzaG93TGFiZWxzIiwic2hvd1Byb2plY3RNYXJrZXIiLCJzb2xhck1vZHVsZSIsImZpbGxDb2xvciIsInNlZ21lbnRGaWxsQ29sb3IiLCJTRUdNRU5UX0NPTE9SIiwic3Ryb2tlQ29sb3IiLCJzZWdtZW50U3Ryb2tlQ29sb3IiLCJTRUdNRU5UX1NUUk9LRV9DT0xPUiIsInNvbGFyTW9kdWxlRmlsbENvbG9yIiwiU09MQVJfTU9EVUxFX0NPTE9SIiwic29sYXJNb2R1bGVTdHJva2VDb2xvciIsIlNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IiLCJ0aXRsZSIsImxhdGl0dWRlIiwibG9uZ2l0dWRlIiwiX2hhbmRsZVpvb21DaGFuZ2VkIiwibmV3Wm9vbSIsIl9oYW5kbGVDZW50ZXJDaGFuZ2VkIiwibmV3UG9zaXRpb24iLCJGVUxMX0VESVRJT05fTU9ERSIsImZpZWxkX3NlZ21lbnRzIiwiRVhBTVBMRV9TRUdNRU5UIiwic2VnbWVudHMiLCJtYXAiLCJzb2xhcl9tb2R1bGVzIiwic29sYXJfbW9kdWxlX3BvaW50cyIsInBvaW50IiwieV9jb29yZGluYXRlIiwieF9jb29yZGluYXRlIiwicG9seWdvbiIsImZpZWxkX3NlZ21lbnRfcG9pbnRzIiwiaWQiLCJuYW1lIiwiTk9fRURJVElPTl9NT0RFIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiZnVuYyIsIm51bWJlciIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQU9BOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDZNQUFmOztBQVNBLElBQU1DLFlBQVksR0FBR0YsMEJBQU9HLElBQVYsK1BBQWxCOztBQVlBLElBQU1DLFNBQVMsR0FBR0osMEJBQU9DLEdBQVYsZ05BR0Q7QUFBQSxNQUFHSSxTQUFILFFBQUdBLFNBQUg7QUFBQSxTQUFvQkEsU0FBUyxhQUFNQSxTQUFOLFVBQXNCLE1BQW5EO0FBQUEsQ0FIQyxDQUFmOztBQVVBLElBQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsR0FNZjtBQUFBLGtGQUFQLEVBQU87QUFBQSxNQUxQQyxZQUtPLFNBTFBBLFlBS087QUFBQSxNQUpQQyxpQkFJTyxTQUpQQSxpQkFJTztBQUFBLE1BSFBDLFdBR08sU0FIUEEsV0FHTztBQUFBLE1BRlBDLEtBRU8sU0FGUEEsS0FFTztBQUFBLDBCQURQQyxLQUNPO0FBQUEsTUFEUEEsS0FDTyw0QkFEQyxFQUNEOztBQUNQLHdCQUFjLG1DQUFkO0FBQUEsTUFBUUMsQ0FBUixtQkFBUUEsQ0FBUjs7QUFDQSxNQUFRQyxxQkFBUixHQUFtREYsS0FBbkQsQ0FBUUUscUJBQVI7QUFBQSxNQUErQkMsZUFBL0IsR0FBbURILEtBQW5ELENBQStCRyxlQUEvQjs7QUFDQSxrQkFBOEIscUJBQVMsRUFBVCxDQUE5QjtBQUFBO0FBQUEsTUFBT0MsT0FBUDtBQUFBLE1BQWdCQyxVQUFoQjs7QUFDQSxtQkFBOEMscUJBQzFDRixlQUFlLENBQUNHLElBRDBCLENBQTlDO0FBQUE7QUFBQSxNQUFPQyxlQUFQO0FBQUEsTUFBd0JDLGtCQUF4Qjs7QUFHQSxtQkFBa0QscUJBQVM7QUFDdkRDLElBQUFBLEdBQUcsRUFBRU4sZUFBZSxDQUFDTSxHQURrQztBQUV2REMsSUFBQUEsR0FBRyxFQUFFUCxlQUFlLENBQUNPO0FBRmtDLEdBQVQsQ0FBbEQ7QUFBQTtBQUFBLE1BQU9DLGlCQUFQO0FBQUEsTUFBMEJDLG9CQUExQjs7QUFJQSx1QkFLSWIsS0FMSixDQUNJYyxPQURKO0FBQUEsTUFDSUEsT0FESiwrQkFDYyxFQURkO0FBQUEsMEJBS0lkLEtBTEosQ0FFSWUsVUFGSjtBQUFBLE1BRUlBLFVBRkosa0NBRWlCLElBRmpCO0FBQUEsOEJBS0lmLEtBTEosQ0FHSWdCLGlCQUhKO0FBQUEsTUFHSUEsaUJBSEosc0NBR3dCLElBSHhCO0FBQUEsMkJBS0loQixLQUxKLENBSUlpQixXQUpKO0FBQUEsTUFJSUEsV0FKSixtQ0FJa0IsRUFKbEI7QUFNQSwyQkFHSUgsT0FISixDQUNJSSxTQURKO0FBQUEsTUFDZUMsZ0JBRGYsbUNBQ2tDQyxtQkFEbEM7QUFBQSw2QkFHSU4sT0FISixDQUVJTyxXQUZKO0FBQUEsTUFFaUJDLGtCQUZqQixxQ0FFc0NDLDBCQUZ0QztBQUlBLDhCQUdJTixXQUhKLENBQ0lDLFNBREo7QUFBQSxNQUNlTSxvQkFEZixzQ0FDc0NDLHdCQUR0QztBQUFBLDhCQUdJUixXQUhKLENBRUlJLFdBRko7QUFBQSxNQUVpQkssc0JBRmpCLHNDQUUwQ0MsK0JBRjFDO0FBS0Esd0JBQVUsWUFBTTtBQUNaLFFBQUksQ0FBQyxzQkFBUXhCLHFCQUFSLENBQUwsRUFBcUM7QUFDakNHLE1BQUFBLFVBQVUsOEJBQ0hELE9BREcsSUFFTjtBQUNJdUIsUUFBQUEsS0FBSyxFQUFFMUIsQ0FBQyxDQUFDLFNBQUQsQ0FEWjtBQUVJUSxRQUFBQSxHQUFHLEVBQUVQLHFCQUFxQixDQUFDMEIsUUFGL0I7QUFHSWxCLFFBQUFBLEdBQUcsRUFBRVIscUJBQXFCLENBQUMyQjtBQUgvQixPQUZNLEdBQVY7QUFRSDtBQUNKLEdBWEQsRUFXRyxFQVhIO0FBYUEsd0JBQVUsWUFBTTtBQUNaLFFBQUkxQixlQUFlLENBQUNHLElBQWhCLEtBQXlCQyxlQUE3QixFQUE4QztBQUMxQ1YsTUFBQUEsaUJBQWlCLGlDQUNWRyxLQURVO0FBRWJHLFFBQUFBLGVBQWUsa0NBQ1JBLGVBRFE7QUFFWEcsVUFBQUEsSUFBSSxFQUFFQztBQUZLO0FBRkYsU0FBakI7QUFPSDtBQUNKLEdBVkQsRUFVRyxDQUFDQSxlQUFELENBVkg7QUFZQSx3QkFBVSxZQUFNO0FBQ1osUUFDSUosZUFBZSxDQUFDTSxHQUFoQixLQUF3QkUsaUJBQWlCLENBQUNGLEdBQTFDLElBQ0FOLGVBQWUsQ0FBQ08sR0FBaEIsS0FBd0JDLGlCQUFpQixDQUFDRCxHQUY5QyxFQUdFO0FBQ0ViLE1BQUFBLGlCQUFpQixpQ0FDVkcsS0FEVTtBQUViRyxRQUFBQSxlQUFlLGtDQUNSQSxlQURRO0FBRVhNLFVBQUFBLEdBQUcsRUFBRUUsaUJBQWlCLENBQUNGLEdBRlo7QUFHWEMsVUFBQUEsR0FBRyxFQUFFQyxpQkFBaUIsQ0FBQ0Q7QUFIWjtBQUZGLFNBQWpCO0FBUUg7QUFDSixHQWRELEVBY0csQ0FBQ0MsaUJBQUQsQ0FkSDs7QUFnQkEsTUFBTW1CLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsT0FBRDtBQUFBLFdBQWF2QixrQkFBa0IsQ0FBQ3VCLE9BQUQsQ0FBL0I7QUFBQSxHQUEzQjs7QUFFQSxNQUFNQyxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQUNDLFdBQUQ7QUFBQSxXQUN6QnJCLG9CQUFvQixDQUFDO0FBQ2pCSCxNQUFBQSxHQUFHLEVBQUV3QixXQUFXLENBQUN4QixHQUFaLEVBRFk7QUFFakJDLE1BQUFBLEdBQUcsRUFBRXVCLFdBQVcsQ0FBQ3ZCLEdBQVo7QUFGWSxLQUFELENBREs7QUFBQSxHQUE3Qjs7QUFNQSxNQUNJZCxZQUFZLEtBQUtzQyx3QkFBakIsSUFDQSxFQUFFaEMscUJBQXFCLElBQUlBLHFCQUFxQixDQUFDaUMsY0FBakQsQ0FGSixFQUdFO0FBQ0Usd0JBQ0ksc0JBQUMsU0FBRDtBQUFBLDhCQUNJLHFCQUFDLFlBQUQ7QUFBQSxrQkFDS2xDLENBQUMsQ0FDRSw0REFERjtBQUROLFFBREosZUFNSSxxQkFBQyxTQUFEO0FBQVcsUUFBQSxTQUFTLEVBQUVILFdBQXRCO0FBQUEsK0JBQ0kscUJBQUMsb0JBQUQ7QUFDSSxVQUFBLFNBQVMsRUFBRSxLQURmO0FBRUksVUFBQSxNQUFNLEVBQUMsTUFGWDtBQUdJLFVBQUEsVUFBVSxFQUFFO0FBQ1I4QixZQUFBQSxRQUFRLEVBQUUsVUFERjtBQUVSQyxZQUFBQSxTQUFTLEVBQUUsQ0FBQztBQUZKLFdBSGhCO0FBT0ksVUFBQSxlQUFlLEVBQUU7QUFDYkQsWUFBQUEsUUFBUSxFQUFFLFVBREc7QUFFYkMsWUFBQUEsU0FBUyxFQUFFLENBQUM7QUFGQyxXQVByQjtBQVdJLFVBQUEsWUFBWSxFQUFFLEtBWGxCO0FBWUksVUFBQSxnQkFBZ0IsRUFBRVgsZ0JBWnRCO0FBYUksVUFBQSxRQUFRLEVBQUUsQ0FBQ2tCLHFCQUFELENBYmQ7QUFjSSxVQUFBLGtCQUFrQixFQUFFZixrQkFkeEI7QUFlSSxVQUFBLFVBQVUsRUFBRVAsVUFmaEI7QUFnQkksVUFBQSxpQkFBaUIsRUFBRUMsaUJBaEJ2QjtBQWlCSSxVQUFBLG9CQUFvQixFQUFFUSxvQkFqQjFCO0FBa0JJLFVBQUEsc0JBQXNCLEVBQUVFLHNCQWxCNUI7QUFtQkksVUFBQSxLQUFLLEVBQUMsTUFuQlY7QUFvQkksVUFBQSxJQUFJLEVBQUUsRUFwQlY7QUFxQkksVUFBQSxXQUFXLEVBQUU7QUFyQmpCO0FBREosUUFOSjtBQUFBLE1BREo7QUFrQ0g7O0FBRUQsTUFBTVksUUFBUSxHQUFHbkMscUJBQXFCLENBQUNpQyxjQUF0QixDQUFxQ0csR0FBckMsQ0FBeUMsVUFBQ3pCLE9BQUQsRUFBYTtBQUNuRSxRQUFNMEIsYUFBYSxHQUFHMUIsT0FBTyxDQUFDMEIsYUFBUixDQUFzQkQsR0FBdEIsQ0FBMEIsVUFBQ3RCLFdBQUQsRUFBaUI7QUFDN0QsYUFBT0EsV0FBVyxDQUFDd0IsbUJBQVosQ0FBZ0NGLEdBQWhDLENBQW9DLFVBQUNHLEtBQUQ7QUFBQSxlQUFXLENBQ2xEQSxLQUFLLENBQUNDLFlBRDRDLEVBRWxERCxLQUFLLENBQUNFLFlBRjRDLENBQVg7QUFBQSxPQUFwQyxDQUFQO0FBSUgsS0FMcUIsQ0FBdEI7QUFNQSxRQUFNQyxPQUFPLEdBQUcvQixPQUFPLENBQUNnQyxvQkFBUixDQUE2QlAsR0FBN0IsQ0FBaUMsVUFBQ0csS0FBRDtBQUFBLGFBQVcsQ0FDeERBLEtBQUssQ0FBQ0MsWUFEa0QsRUFFeERELEtBQUssQ0FBQ0UsWUFGa0QsQ0FBWDtBQUFBLEtBQWpDLENBQWhCO0FBSUEsV0FBTztBQUNIRyxNQUFBQSxFQUFFLEVBQUVqQyxPQUFPLENBQUNpQyxFQURUO0FBRUhDLE1BQUFBLElBQUksRUFBRWxDLE9BQU8sQ0FBQ2tDLElBRlg7QUFHSEgsTUFBQUEsT0FBTyxFQUFQQSxPQUhHO0FBSUhMLE1BQUFBLGFBQWEsRUFBYkE7QUFKRyxLQUFQO0FBTUgsR0FqQmdCLENBQWpCO0FBbUJBLHNCQUNJLHFCQUFDLFNBQUQ7QUFBQSwyQkFDSSxxQkFBQyxTQUFEO0FBQVcsTUFBQSxTQUFTLEVBQUV6QyxXQUF0QjtBQUFBLDZCQUNJLHFCQUFDLG9CQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUVGLFlBQVksS0FBS29ELHNCQURoQztBQUVJLFFBQUEsbUJBQW1CLEVBQUVoQixvQkFGekI7QUFHSSxRQUFBLGlCQUFpQixFQUFFRixrQkFIdkI7QUFJSSxRQUFBLE1BQU0sRUFBQyxNQUpYO0FBS0ksUUFBQSxVQUFVLEVBQUU7QUFDUkYsVUFBQUEsUUFBUSxFQUFFakIsaUJBQWlCLENBQUNGLEdBRHBCO0FBRVJvQixVQUFBQSxTQUFTLEVBQUVsQixpQkFBaUIsQ0FBQ0Q7QUFGckIsU0FMaEI7QUFTSSxRQUFBLGVBQWUsRUFBRTtBQUNia0IsVUFBQUEsUUFBUSxFQUFFMUIscUJBQXFCLENBQUMwQixRQURuQjtBQUViQyxVQUFBQSxTQUFTLEVBQUUzQixxQkFBcUIsQ0FBQzJCO0FBRnBCLFNBVHJCO0FBYUksUUFBQSxZQUFZLEVBQUVqQyxZQUFZLEtBQUtvRCxzQkFibkM7QUFjSSxRQUFBLGdCQUFnQixFQUFFOUIsZ0JBZHRCO0FBZUksUUFBQSxRQUFRLEVBQUVtQixRQWZkO0FBZ0JJLFFBQUEsa0JBQWtCLEVBQUVoQixrQkFoQnhCO0FBaUJJLFFBQUEsVUFBVSxFQUFFUCxVQWpCaEI7QUFrQkksUUFBQSxpQkFBaUIsRUFBRUMsaUJBbEJ2QjtBQW1CSSxRQUFBLG9CQUFvQixFQUFFUSxvQkFuQjFCO0FBb0JJLFFBQUEsc0JBQXNCLEVBQUVFLHNCQXBCNUI7QUFxQkksUUFBQSxLQUFLLEVBQUMsTUFyQlY7QUFzQkksUUFBQSxJQUFJLEVBQUVsQixlQXRCVjtBQXVCSSxRQUFBLFdBQVcsRUFBRVgsWUFBWSxLQUFLb0Q7QUF2QmxDO0FBREo7QUFESixJQURKO0FBK0JILENBM0tEOztBQTZLQXJELGlCQUFpQixDQUFDc0QsU0FBbEIsR0FBOEI7QUFDMUJyRCxFQUFBQSxZQUFZLEVBQUVzRCxtQkFBVUMsTUFERTtBQUUxQnRELEVBQUFBLGlCQUFpQixFQUFFcUQsbUJBQVVFLElBRkg7QUFHMUJ0RCxFQUFBQSxXQUFXLEVBQUVvRCxtQkFBVUcsTUFIRztBQUkxQnRELEVBQUFBLEtBQUssRUFBRW1ELG1CQUFVSSxNQUpTO0FBSzFCdEQsRUFBQUEsS0FBSyxFQUFFa0QsbUJBQVVJO0FBTFMsQ0FBOUI7ZUFRZTNELGlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHVzZUVmZmVjdCwgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCB7XG4gICAgRVhBTVBMRV9TRUdNRU5ULFxuICAgIFNFR01FTlRfQ09MT1IsXG4gICAgU0VHTUVOVF9TVFJPS0VfQ09MT1IsXG4gICAgU09MQVJfTU9EVUxFX0NPTE9SLFxuICAgIFNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IsXG59IGZyb20gJ0Bjb25zdGFudHMvbWFwcyc7XG5pbXBvcnQgeyBOT19FRElUSU9OX01PREUsIEZVTExfRURJVElPTl9NT0RFIH0gZnJvbSAnQGNvbnN0YW50cy90eXBlcyc7XG5cbmltcG9ydCBTZWdtZW50c01hcCBmcm9tICcuL1NlZ21lbnRzTWFwJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuYDtcblxuY29uc3QgRXhhbXBsZUFsZXJ0ID0gc3R5bGVkLnNwYW5gXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBwYWRkaW5nOiA0cHggNnB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkMzJmMmY7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgei1pbmRleDogMTtcbmA7XG5cbmNvbnN0IFN0eWxlZE1hcCA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2M1YzVjNTtcbiAgICBib3R0b206IDA7XG4gICAgaGVpZ2h0OiAkeyh7IGhlaWdodE1hcCB9KSA9PiAoaGVpZ2h0TWFwID8gYCR7aGVpZ2h0TWFwfXB4YCA6ICcxMDAlJyl9O1xuICAgIGxlZnQ6IDA7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogMDtcbmA7XG5cbmNvbnN0IFNlZ21lbnRzTWFwV2lkZ2V0ID0gKHtcbiAgICBlZGl0aW9uTGV2ZWwsXG4gICAgaGFuZGxlQ2hhbmdlVmFsdWUsXG4gICAgaW5uZXJIZWlnaHQsXG4gICAgc3R5bGUsXG4gICAgdmFsdWUgPSB7fSxcbn0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IHsgdCB9ID0gdXNlVHJhbnNsYXRpb24oKTtcbiAgICBjb25zdCB7IHBhcmVudF9maWVsZF9zZWdtZW50cywgY3VycmVudFBvc2l0aW9uIH0gPSB2YWx1ZTtcbiAgICBjb25zdCBbbWFya2Vycywgc2V0TWFya2Vyc10gPSB1c2VTdGF0ZShbXSk7XG4gICAgY29uc3QgW3RlbXBDdXJyZW50Wm9vbSwgc2V0VGVtcEN1cnJlbnRab29tXSA9IHVzZVN0YXRlKFxuICAgICAgICBjdXJyZW50UG9zaXRpb24uem9vbVxuICAgICk7XG4gICAgY29uc3QgW3RlbXBDdXJyZW50Q2VudGVyLCBzZXRUZW1wQ3VycmVudENlbnRlcl0gPSB1c2VTdGF0ZSh7XG4gICAgICAgIGxhdDogY3VycmVudFBvc2l0aW9uLmxhdCxcbiAgICAgICAgbG5nOiBjdXJyZW50UG9zaXRpb24ubG5nLFxuICAgIH0pO1xuICAgIGNvbnN0IHtcbiAgICAgICAgc2VnbWVudCA9IHt9LFxuICAgICAgICBzaG93TGFiZWxzID0gdHJ1ZSxcbiAgICAgICAgc2hvd1Byb2plY3RNYXJrZXIgPSB0cnVlLFxuICAgICAgICBzb2xhck1vZHVsZSA9IHt9LFxuICAgIH0gPSBzdHlsZTtcbiAgICBjb25zdCB7XG4gICAgICAgIGZpbGxDb2xvcjogc2VnbWVudEZpbGxDb2xvciA9IFNFR01FTlRfQ09MT1IsXG4gICAgICAgIHN0cm9rZUNvbG9yOiBzZWdtZW50U3Ryb2tlQ29sb3IgPSBTRUdNRU5UX1NUUk9LRV9DT0xPUixcbiAgICB9ID0gc2VnbWVudDtcbiAgICBjb25zdCB7XG4gICAgICAgIGZpbGxDb2xvcjogc29sYXJNb2R1bGVGaWxsQ29sb3IgPSBTT0xBUl9NT0RVTEVfQ09MT1IsXG4gICAgICAgIHN0cm9rZUNvbG9yOiBzb2xhck1vZHVsZVN0cm9rZUNvbG9yID0gU09MQVJfTU9EVUxFX1NUUk9LRV9DT0xPUixcbiAgICB9ID0gc29sYXJNb2R1bGU7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoIWlzRW1wdHkocGFyZW50X2ZpZWxkX3NlZ21lbnRzKSkge1xuICAgICAgICAgICAgc2V0TWFya2VycyhbXG4gICAgICAgICAgICAgICAgLi4ubWFya2VycyxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0KCdQcm9qZWN0JyksXG4gICAgICAgICAgICAgICAgICAgIGxhdDogcGFyZW50X2ZpZWxkX3NlZ21lbnRzLmxhdGl0dWRlLFxuICAgICAgICAgICAgICAgICAgICBsbmc6IHBhcmVudF9maWVsZF9zZWdtZW50cy5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF0pO1xuICAgICAgICB9XG4gICAgfSwgW10pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaWYgKGN1cnJlbnRQb3NpdGlvbi56b29tICE9PSB0ZW1wQ3VycmVudFpvb20pIHtcbiAgICAgICAgICAgIGhhbmRsZUNoYW5nZVZhbHVlKHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgICAgICBjdXJyZW50UG9zaXRpb246IHtcbiAgICAgICAgICAgICAgICAgICAgLi4uY3VycmVudFBvc2l0aW9uLFxuICAgICAgICAgICAgICAgICAgICB6b29tOiB0ZW1wQ3VycmVudFpvb20sXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfSwgW3RlbXBDdXJyZW50Wm9vbV0pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgY3VycmVudFBvc2l0aW9uLmxhdCAhPT0gdGVtcEN1cnJlbnRDZW50ZXIubGF0IHx8XG4gICAgICAgICAgICBjdXJyZW50UG9zaXRpb24ubG5nICE9PSB0ZW1wQ3VycmVudENlbnRlci5sbmdcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSh7XG4gICAgICAgICAgICAgICAgLi4udmFsdWUsXG4gICAgICAgICAgICAgICAgY3VycmVudFBvc2l0aW9uOiB7XG4gICAgICAgICAgICAgICAgICAgIC4uLmN1cnJlbnRQb3NpdGlvbixcbiAgICAgICAgICAgICAgICAgICAgbGF0OiB0ZW1wQ3VycmVudENlbnRlci5sYXQsXG4gICAgICAgICAgICAgICAgICAgIGxuZzogdGVtcEN1cnJlbnRDZW50ZXIubG5nLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH0sIFt0ZW1wQ3VycmVudENlbnRlcl0pO1xuXG4gICAgY29uc3QgX2hhbmRsZVpvb21DaGFuZ2VkID0gKG5ld1pvb20pID0+IHNldFRlbXBDdXJyZW50Wm9vbShuZXdab29tKTtcblxuICAgIGNvbnN0IF9oYW5kbGVDZW50ZXJDaGFuZ2VkID0gKG5ld1Bvc2l0aW9uKSA9PlxuICAgICAgICBzZXRUZW1wQ3VycmVudENlbnRlcih7XG4gICAgICAgICAgICBsYXQ6IG5ld1Bvc2l0aW9uLmxhdCgpLFxuICAgICAgICAgICAgbG5nOiBuZXdQb3NpdGlvbi5sbmcoKSxcbiAgICAgICAgfSk7XG5cbiAgICBpZiAoXG4gICAgICAgIGVkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUgfHxcbiAgICAgICAgIShwYXJlbnRfZmllbGRfc2VnbWVudHMgJiYgcGFyZW50X2ZpZWxkX3NlZ21lbnRzLmZpZWxkX3NlZ21lbnRzKVxuICAgICkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8RXhhbXBsZUFsZXJ0PlxuICAgICAgICAgICAgICAgICAgICB7dChcbiAgICAgICAgICAgICAgICAgICAgICAgICdUaGUgc2VnbWVudCBhbmQgcGFuZWxzIHNob3duIGluIHRoaXMgbWFwIGFyZSBmb3IgcmVmZXJlbmNlJ1xuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvRXhhbXBsZUFsZXJ0PlxuICAgICAgICAgICAgICAgIDxTdHlsZWRNYXAgaGVpZ2h0TWFwPXtpbm5lckhlaWdodH0+XG4gICAgICAgICAgICAgICAgICAgIDxTZWdtZW50c01hcFxuICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2thYmxlPXtmYWxzZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjEwMCVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaW5pdENlbnRlcj17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxhdGl0dWRlOiAyMC45ODkxNjA4LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvbmdpdHVkZTogLTg5LjYxMTQ5NTQsXG4gICAgICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvamVjdExvY2F0aW9uPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IDIwLjk4OTE0NzcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbG9uZ2l0dWRlOiAtODkuNjExNTM4NCxcbiAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZUNvbnRyb2w9e2ZhbHNlfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2VnbWVudEZpbGxDb2xvcj17c2VnbWVudEZpbGxDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNlZ21lbnRzPXtbRVhBTVBMRV9TRUdNRU5UXX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNlZ21lbnRTdHJva2VDb2xvcj17c2VnbWVudFN0cm9rZUNvbG9yfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2hvd0xhYmVscz17c2hvd0xhYmVsc31cbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3dQcm9qZWN0TWFya2VyPXtzaG93UHJvamVjdE1hcmtlcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNvbGFyTW9kdWxlRmlsbENvbG9yPXtzb2xhck1vZHVsZUZpbGxDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNvbGFyTW9kdWxlU3Ryb2tlQ29sb3I9e3NvbGFyTW9kdWxlU3Ryb2tlQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjEwMCVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgem9vbT17MjB9XG4gICAgICAgICAgICAgICAgICAgICAgICB6b29tQ29udHJvbD17ZmFsc2V9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRNYXA+XG4gICAgICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBjb25zdCBzZWdtZW50cyA9IHBhcmVudF9maWVsZF9zZWdtZW50cy5maWVsZF9zZWdtZW50cy5tYXAoKHNlZ21lbnQpID0+IHtcbiAgICAgICAgY29uc3Qgc29sYXJfbW9kdWxlcyA9IHNlZ21lbnQuc29sYXJfbW9kdWxlcy5tYXAoKHNvbGFyTW9kdWxlKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gc29sYXJNb2R1bGUuc29sYXJfbW9kdWxlX3BvaW50cy5tYXAoKHBvaW50KSA9PiBbXG4gICAgICAgICAgICAgICAgcG9pbnQueV9jb29yZGluYXRlLFxuICAgICAgICAgICAgICAgIHBvaW50LnhfY29vcmRpbmF0ZSxcbiAgICAgICAgICAgIF0pO1xuICAgICAgICB9KTtcbiAgICAgICAgY29uc3QgcG9seWdvbiA9IHNlZ21lbnQuZmllbGRfc2VnbWVudF9wb2ludHMubWFwKChwb2ludCkgPT4gW1xuICAgICAgICAgICAgcG9pbnQueV9jb29yZGluYXRlLFxuICAgICAgICAgICAgcG9pbnQueF9jb29yZGluYXRlLFxuICAgICAgICBdKTtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGlkOiBzZWdtZW50LmlkLFxuICAgICAgICAgICAgbmFtZTogc2VnbWVudC5uYW1lLFxuICAgICAgICAgICAgcG9seWdvbixcbiAgICAgICAgICAgIHNvbGFyX21vZHVsZXMsXG4gICAgICAgIH07XG4gICAgfSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgPFN0eWxlZE1hcCBoZWlnaHRNYXA9e2lubmVySGVpZ2h0fT5cbiAgICAgICAgICAgICAgICA8U2VnbWVudHNNYXBcbiAgICAgICAgICAgICAgICAgICAgY2xpY2thYmxlPXtlZGl0aW9uTGV2ZWwgIT09IE5PX0VESVRJT05fTU9ERX1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlQ2VudGVyQ2hhbmdlZD17X2hhbmRsZUNlbnRlckNoYW5nZWR9XG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZVpvb21DaGFuZ2VkPXtfaGFuZGxlWm9vbUNoYW5nZWR9XG4gICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjEwMCVcIlxuICAgICAgICAgICAgICAgICAgICBpbml0Q2VudGVyPXt7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYXRpdHVkZTogdGVtcEN1cnJlbnRDZW50ZXIubGF0LFxuICAgICAgICAgICAgICAgICAgICAgICAgbG9uZ2l0dWRlOiB0ZW1wQ3VycmVudENlbnRlci5sbmcsXG4gICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgIHByb2plY3RMb2NhdGlvbj17e1xuICAgICAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IHBhcmVudF9maWVsZF9zZWdtZW50cy5sYXRpdHVkZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxvbmdpdHVkZTogcGFyZW50X2ZpZWxkX3NlZ21lbnRzLmxvbmdpdHVkZSxcbiAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgc2NhbGVDb250cm9sPXtlZGl0aW9uTGV2ZWwgIT09IE5PX0VESVRJT05fTU9ERX1cbiAgICAgICAgICAgICAgICAgICAgc2VnbWVudEZpbGxDb2xvcj17c2VnbWVudEZpbGxDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgc2VnbWVudHM9e3NlZ21lbnRzfVxuICAgICAgICAgICAgICAgICAgICBzZWdtZW50U3Ryb2tlQ29sb3I9e3NlZ21lbnRTdHJva2VDb2xvcn1cbiAgICAgICAgICAgICAgICAgICAgc2hvd0xhYmVscz17c2hvd0xhYmVsc31cbiAgICAgICAgICAgICAgICAgICAgc2hvd1Byb2plY3RNYXJrZXI9e3Nob3dQcm9qZWN0TWFya2VyfVxuICAgICAgICAgICAgICAgICAgICBzb2xhck1vZHVsZUZpbGxDb2xvcj17c29sYXJNb2R1bGVGaWxsQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgIHNvbGFyTW9kdWxlU3Ryb2tlQ29sb3I9e3NvbGFyTW9kdWxlU3Ryb2tlQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTAwJVwiXG4gICAgICAgICAgICAgICAgICAgIHpvb209e3RlbXBDdXJyZW50Wm9vbX1cbiAgICAgICAgICAgICAgICAgICAgem9vbUNvbnRyb2w9e2VkaXRpb25MZXZlbCAhPT0gTk9fRURJVElPTl9NT0RFfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L1N0eWxlZE1hcD5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cblNlZ21lbnRzTWFwV2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGFuZGxlQ2hhbmdlVmFsdWU6IFByb3BUeXBlcy5mdW5jLFxuICAgIGlubmVySGVpZ2h0OiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU2VnbWVudHNNYXBXaWRnZXQ7XG4iXX0=