"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _defaultImage = _interopRequireDefault(require("../../../../res/defaultImage.png"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    height: 100%;\n    overflow-y: auto;\n"])));

var Image = _styledComponents.default.img(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n"])));

var ImageWidget = function ImageWidget() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      value = _ref.value,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Image, {
      style: style,
      src: (0, _isEmpty.default)(value) ? _defaultImage.default : value
    })
  });
};

ImageWidget.propTypes = {
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = ImageWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvSW1hZ2VXaWRnZXQuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiSW1hZ2UiLCJpbWciLCJJbWFnZVdpZGdldCIsInZhbHVlIiwic3R5bGUiLCJEZWZhdWx0SW1hZ2UiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsb0xBQWY7O0FBUUEsSUFBTUMsS0FBSyxHQUFHRiwwQkFBT0csR0FBVixrS0FBWDs7QUFRQSxJQUFNQyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUFnQztBQUFBLGlGQUFQLEVBQU87QUFBQSxNQUE3QkMsS0FBNkIsUUFBN0JBLEtBQTZCO0FBQUEsd0JBQXRCQyxLQUFzQjtBQUFBLE1BQXRCQSxLQUFzQiwyQkFBZCxFQUFjOztBQUNoRCxzQkFDSSxxQkFBQyxTQUFEO0FBQUEsMkJBQ0kscUJBQUMsS0FBRDtBQUFPLE1BQUEsS0FBSyxFQUFFQSxLQUFkO0FBQXFCLE1BQUEsR0FBRyxFQUFFLHNCQUFRRCxLQUFSLElBQWlCRSxxQkFBakIsR0FBZ0NGO0FBQTFEO0FBREosSUFESjtBQUtILENBTkQ7O0FBUUFELFdBQVcsQ0FBQ0ksU0FBWixHQUF3QjtBQUNwQkYsRUFBQUEsS0FBSyxFQUFFRyxtQkFBVUMsTUFERztBQUVwQkwsRUFBQUEsS0FBSyxFQUFFSSxtQkFBVUM7QUFGRyxDQUF4QjtlQUtlTixXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgRGVmYXVsdEltYWdlIGZyb20gJ0ByZXMvZGVmYXVsdEltYWdlLnBuZyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuYDtcblxuY29uc3QgSW1hZ2UgPSBzdHlsZWQuaW1nYFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG5gO1xuXG5jb25zdCBJbWFnZVdpZGdldCA9ICh7IHZhbHVlLCBzdHlsZSA9IHt9IH0gPSB7fSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXI+XG4gICAgICAgICAgICA8SW1hZ2Ugc3R5bGU9e3N0eWxlfSBzcmM9e2lzRW1wdHkodmFsdWUpID8gRGVmYXVsdEltYWdlIDogdmFsdWV9IC8+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5JbWFnZVdpZGdldC5wcm9wVHlwZXMgPSB7XG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBJbWFnZVdpZGdldDtcbiJdfQ==