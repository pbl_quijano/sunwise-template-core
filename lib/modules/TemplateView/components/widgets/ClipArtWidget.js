"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _clipArts = require("./clipArts");

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: block;\n    height: 100%;\n    width: 100%;\n\n    svg {\n        display: block;\n    }\n"])));

var ClipArtWidget = function ClipArtWidget() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      _ref$innerHeight = _ref.innerHeight,
      innerHeight = _ref$innerHeight === void 0 ? 1 : _ref$innerHeight,
      _ref$innerWidth = _ref.innerWidth,
      innerWidth = _ref$innerWidth === void 0 ? 1 : _ref$innerWidth;

  var getClipArt = function getClipArt() {
    var type = style.type,
        background = style.background,
        corner = style.corner,
        strokeWidth = style.strokeWidth,
        stroke = style.stroke;
    var basicProps = {
      width: innerWidth,
      heigth: innerHeight,
      corner: corner,
      background: background,
      strokeWidth: strokeWidth,
      stroke: stroke
    };

    switch (type) {
      case 'circle':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Circle, _objectSpread({}, basicProps));

      case 'ellipse':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Ellipse, _objectSpread({}, basicProps));

      case 'square':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Square, _objectSpread({}, basicProps));

      case 'rectangle':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Rectangle, _objectSpread({}, basicProps));

      case 'triangle':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Triangle, _objectSpread({}, basicProps));

      case 'hexagon':
        return /*#__PURE__*/(0, _jsxRuntime.jsx)(_clipArts.Hexagon, _objectSpread({}, basicProps));

      default:
        return null;
    }
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: getClipArt()
  });
};

ClipArtWidget.propTypes = {
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  style: _propTypes.default.object
};
var _default = ClipArtWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvQ2xpcEFydFdpZGdldC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJDbGlwQXJ0V2lkZ2V0Iiwic3R5bGUiLCJpbm5lckhlaWdodCIsImlubmVyV2lkdGgiLCJnZXRDbGlwQXJ0IiwidHlwZSIsImJhY2tncm91bmQiLCJjb3JuZXIiLCJzdHJva2VXaWR0aCIsInN0cm9rZSIsImJhc2ljUHJvcHMiLCJ3aWR0aCIsImhlaWd0aCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm51bWJlciIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBU0EsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViw4S0FBZjs7QUFVQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBSVg7QUFBQSxpRkFBUCxFQUFPO0FBQUEsd0JBSFBDLEtBR087QUFBQSxNQUhQQSxLQUdPLDJCQUhDLEVBR0Q7QUFBQSw4QkFGUEMsV0FFTztBQUFBLE1BRlBBLFdBRU8saUNBRk8sQ0FFUDtBQUFBLDZCQURQQyxVQUNPO0FBQUEsTUFEUEEsVUFDTyxnQ0FETSxDQUNOOztBQUNQLE1BQU1DLFVBQVUsR0FBRyxTQUFiQSxVQUFhLEdBQU07QUFDckIsUUFBUUMsSUFBUixHQUEwREosS0FBMUQsQ0FBUUksSUFBUjtBQUFBLFFBQWNDLFVBQWQsR0FBMERMLEtBQTFELENBQWNLLFVBQWQ7QUFBQSxRQUEwQkMsTUFBMUIsR0FBMEROLEtBQTFELENBQTBCTSxNQUExQjtBQUFBLFFBQWtDQyxXQUFsQyxHQUEwRFAsS0FBMUQsQ0FBa0NPLFdBQWxDO0FBQUEsUUFBK0NDLE1BQS9DLEdBQTBEUixLQUExRCxDQUErQ1EsTUFBL0M7QUFDQSxRQUFNQyxVQUFVLEdBQUc7QUFDZkMsTUFBQUEsS0FBSyxFQUFFUixVQURRO0FBRWZTLE1BQUFBLE1BQU0sRUFBRVYsV0FGTztBQUdmSyxNQUFBQSxNQUFNLEVBQU5BLE1BSGU7QUFJZkQsTUFBQUEsVUFBVSxFQUFWQSxVQUplO0FBS2ZFLE1BQUFBLFdBQVcsRUFBWEEsV0FMZTtBQU1mQyxNQUFBQSxNQUFNLEVBQU5BO0FBTmUsS0FBbkI7O0FBUUEsWUFBUUosSUFBUjtBQUNJLFdBQUssUUFBTDtBQUNJLDRCQUFPLHFCQUFDLGdCQUFELG9CQUFZSyxVQUFaLEVBQVA7O0FBQ0osV0FBSyxTQUFMO0FBQ0ksNEJBQU8scUJBQUMsaUJBQUQsb0JBQWFBLFVBQWIsRUFBUDs7QUFDSixXQUFLLFFBQUw7QUFDSSw0QkFBTyxxQkFBQyxnQkFBRCxvQkFBWUEsVUFBWixFQUFQOztBQUNKLFdBQUssV0FBTDtBQUNJLDRCQUFPLHFCQUFDLG1CQUFELG9CQUFlQSxVQUFmLEVBQVA7O0FBQ0osV0FBSyxVQUFMO0FBQ0ksNEJBQU8scUJBQUMsa0JBQUQsb0JBQWNBLFVBQWQsRUFBUDs7QUFDSixXQUFLLFNBQUw7QUFDSSw0QkFBTyxxQkFBQyxpQkFBRCxvQkFBYUEsVUFBYixFQUFQOztBQUNKO0FBQ0ksZUFBTyxJQUFQO0FBZFI7QUFnQkgsR0ExQkQ7O0FBMkJBLHNCQUFPLHFCQUFDLFNBQUQ7QUFBQSxjQUFZTixVQUFVO0FBQXRCLElBQVA7QUFDSCxDQWpDRDs7QUFtQ0FKLGFBQWEsQ0FBQ2EsU0FBZCxHQUEwQjtBQUN0QlgsRUFBQUEsV0FBVyxFQUFFWSxtQkFBVUMsTUFERDtBQUV0QlosRUFBQUEsVUFBVSxFQUFFVyxtQkFBVUMsTUFGQTtBQUd0QmQsRUFBQUEsS0FBSyxFQUFFYSxtQkFBVUU7QUFISyxDQUExQjtlQU1laEIsYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHtcbiAgICBDaXJjbGUsXG4gICAgRWxsaXBzZSxcbiAgICBIZXhhZ29uLFxuICAgIFJlY3RhbmdsZSxcbiAgICBTcXVhcmUsXG4gICAgVHJpYW5nbGUsXG59IGZyb20gJy4vY2xpcEFydHMnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcblxuICAgIHN2ZyB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIH1cbmA7XG5cbmNvbnN0IENsaXBBcnRXaWRnZXQgPSAoe1xuICAgIHN0eWxlID0ge30sXG4gICAgaW5uZXJIZWlnaHQgPSAxLFxuICAgIGlubmVyV2lkdGggPSAxLFxufSA9IHt9KSA9PiB7XG4gICAgY29uc3QgZ2V0Q2xpcEFydCA9ICgpID0+IHtcbiAgICAgICAgY29uc3QgeyB0eXBlLCBiYWNrZ3JvdW5kLCBjb3JuZXIsIHN0cm9rZVdpZHRoLCBzdHJva2UgfSA9IHN0eWxlO1xuICAgICAgICBjb25zdCBiYXNpY1Byb3BzID0ge1xuICAgICAgICAgICAgd2lkdGg6IGlubmVyV2lkdGgsXG4gICAgICAgICAgICBoZWlndGg6IGlubmVySGVpZ2h0LFxuICAgICAgICAgICAgY29ybmVyLFxuICAgICAgICAgICAgYmFja2dyb3VuZCxcbiAgICAgICAgICAgIHN0cm9rZVdpZHRoLFxuICAgICAgICAgICAgc3Ryb2tlLFxuICAgICAgICB9O1xuICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ2NpcmNsZSc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIDxDaXJjbGUgey4uLmJhc2ljUHJvcHN9IC8+O1xuICAgICAgICAgICAgY2FzZSAnZWxsaXBzZSc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIDxFbGxpcHNlIHsuLi5iYXNpY1Byb3BzfSAvPjtcbiAgICAgICAgICAgIGNhc2UgJ3NxdWFyZSc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIDxTcXVhcmUgey4uLmJhc2ljUHJvcHN9IC8+O1xuICAgICAgICAgICAgY2FzZSAncmVjdGFuZ2xlJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gPFJlY3RhbmdsZSB7Li4uYmFzaWNQcm9wc30gLz47XG4gICAgICAgICAgICBjYXNlICd0cmlhbmdsZSc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIDxUcmlhbmdsZSB7Li4uYmFzaWNQcm9wc30gLz47XG4gICAgICAgICAgICBjYXNlICdoZXhhZ29uJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gPEhleGFnb24gey4uLmJhc2ljUHJvcHN9IC8+O1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIDxDb250YWluZXI+e2dldENsaXBBcnQoKX08L0NvbnRhaW5lcj47XG59O1xuXG5DbGlwQXJ0V2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBpbm5lckhlaWdodDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBpbm5lcldpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQ2xpcEFydFdpZGdldDtcbiJdfQ==