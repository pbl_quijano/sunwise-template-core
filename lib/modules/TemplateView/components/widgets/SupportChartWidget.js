"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ChartView = _interopRequireDefault(require("./ChartView"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    height: 100%;\n    overflow-x: hidden;\n    overflow-y: ", ";\n    width: 100%;\n"])), function (_ref) {
  var overflowYDisabled = _ref.overflowYDisabled;
  return overflowYDisabled ? 'hidden' : 'auto';
});

var SupportChartWidget = function SupportChartWidget() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      currencyIso = _ref2.currencyIso,
      currencyLocale = _ref2.currencyLocale,
      editionDisabled = _ref2.editionDisabled,
      innerHeight = _ref2.innerHeight,
      innerWidth = _ref2.innerWidth,
      _ref2$value = _ref2.value,
      value = _ref2$value === void 0 ? {} : _ref2$value;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    overflowYDisabled: editionDisabled,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ChartView.default, {
      currencyIso: currencyIso,
      currencyLocale: currencyLocale,
      data: value,
      innerHeight: innerHeight,
      innerWidth: innerWidth
    })
  });
};

SupportChartWidget.propTypes = {
  currencyIso: _propTypes.default.string,
  currencyLocale: _propTypes.default.string,
  editionDisabled: _propTypes.default.bool,
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  value: _propTypes.default.object
};
var _default = SupportChartWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU3VwcG9ydENoYXJ0V2lkZ2V0LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsIm92ZXJmbG93WURpc2FibGVkIiwiU3VwcG9ydENoYXJ0V2lkZ2V0IiwiY3VycmVuY3lJc28iLCJjdXJyZW5jeUxvY2FsZSIsImVkaXRpb25EaXNhYmxlZCIsImlubmVySGVpZ2h0IiwiaW5uZXJXaWR0aCIsInZhbHVlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiYm9vbCIsIm51bWJlciIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViw2SkFHRztBQUFBLE1BQUdDLGlCQUFILFFBQUdBLGlCQUFIO0FBQUEsU0FDVkEsaUJBQWlCLEdBQUcsUUFBSCxHQUFjLE1BRHJCO0FBQUEsQ0FISCxDQUFmOztBQVFBLElBQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUI7QUFBQSxrRkFPdkIsRUFQdUI7QUFBQSxNQUN2QkMsV0FEdUIsU0FDdkJBLFdBRHVCO0FBQUEsTUFFdkJDLGNBRnVCLFNBRXZCQSxjQUZ1QjtBQUFBLE1BR3ZCQyxlQUh1QixTQUd2QkEsZUFIdUI7QUFBQSxNQUl2QkMsV0FKdUIsU0FJdkJBLFdBSnVCO0FBQUEsTUFLdkJDLFVBTHVCLFNBS3ZCQSxVQUx1QjtBQUFBLDBCQU12QkMsS0FOdUI7QUFBQSxNQU12QkEsS0FOdUIsNEJBTWYsRUFOZTs7QUFBQSxzQkFRdkIscUJBQUMsU0FBRDtBQUFXLElBQUEsaUJBQWlCLEVBQUVILGVBQTlCO0FBQUEsMkJBQ0kscUJBQUMsa0JBQUQ7QUFDSSxNQUFBLFdBQVcsRUFBRUYsV0FEakI7QUFFSSxNQUFBLGNBQWMsRUFBRUMsY0FGcEI7QUFHSSxNQUFBLElBQUksRUFBRUksS0FIVjtBQUlJLE1BQUEsV0FBVyxFQUFFRixXQUpqQjtBQUtJLE1BQUEsVUFBVSxFQUFFQztBQUxoQjtBQURKLElBUnVCO0FBQUEsQ0FBM0I7O0FBbUJBTCxrQkFBa0IsQ0FBQ08sU0FBbkIsR0FBK0I7QUFDM0JOLEVBQUFBLFdBQVcsRUFBRU8sbUJBQVVDLE1BREk7QUFFM0JQLEVBQUFBLGNBQWMsRUFBRU0sbUJBQVVDLE1BRkM7QUFHM0JOLEVBQUFBLGVBQWUsRUFBRUssbUJBQVVFLElBSEE7QUFJM0JOLEVBQUFBLFdBQVcsRUFBRUksbUJBQVVHLE1BSkk7QUFLM0JOLEVBQUFBLFVBQVUsRUFBRUcsbUJBQVVHLE1BTEs7QUFNM0JMLEVBQUFBLEtBQUssRUFBRUUsbUJBQVVJO0FBTlUsQ0FBL0I7ZUFTZVosa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBDaGFydFZpZXcgZnJvbSAnLi9DaGFydFZpZXcnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy14OiBoaWRkZW47XG4gICAgb3ZlcmZsb3cteTogJHsoeyBvdmVyZmxvd1lEaXNhYmxlZCB9KSA9PlxuICAgICAgICBvdmVyZmxvd1lEaXNhYmxlZCA/ICdoaWRkZW4nIDogJ2F1dG8nfTtcbiAgICB3aWR0aDogMTAwJTtcbmA7XG5cbmNvbnN0IFN1cHBvcnRDaGFydFdpZGdldCA9ICh7XG4gICAgY3VycmVuY3lJc28sXG4gICAgY3VycmVuY3lMb2NhbGUsXG4gICAgZWRpdGlvbkRpc2FibGVkLFxuICAgIGlubmVySGVpZ2h0LFxuICAgIGlubmVyV2lkdGgsXG4gICAgdmFsdWUgPSB7fSxcbn0gPSB7fSkgPT4gKFxuICAgIDxDb250YWluZXIgb3ZlcmZsb3dZRGlzYWJsZWQ9e2VkaXRpb25EaXNhYmxlZH0+XG4gICAgICAgIDxDaGFydFZpZXdcbiAgICAgICAgICAgIGN1cnJlbmN5SXNvPXtjdXJyZW5jeUlzb31cbiAgICAgICAgICAgIGN1cnJlbmN5TG9jYWxlPXtjdXJyZW5jeUxvY2FsZX1cbiAgICAgICAgICAgIGRhdGE9e3ZhbHVlfVxuICAgICAgICAgICAgaW5uZXJIZWlnaHQ9e2lubmVySGVpZ2h0fVxuICAgICAgICAgICAgaW5uZXJXaWR0aD17aW5uZXJXaWR0aH1cbiAgICAgICAgLz5cbiAgICA8L0NvbnRhaW5lcj5cbik7XG5cblN1cHBvcnRDaGFydFdpZGdldC5wcm9wVHlwZXMgPSB7XG4gICAgY3VycmVuY3lJc286IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY3VycmVuY3lMb2NhbGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZWRpdGlvbkRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpbm5lckhlaWdodDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBpbm5lcldpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU3VwcG9ydENoYXJ0V2lkZ2V0O1xuIl19