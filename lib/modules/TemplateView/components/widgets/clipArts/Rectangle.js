"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Rectangle = function Rectangle() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      _ref$corner = _ref.corner,
      corner = _ref$corner === void 0 ? 0 : _ref$corner,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  var realCorner = heigth < width ? heigth * (corner / 100) : width * (corner / 100);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("rect", {
      x: strokeWidth / 2,
      y: strokeWidth / 2,
      rx: realCorner,
      ry: realCorner,
      width: width - strokeWidth,
      height: heigth - strokeWidth,
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Rectangle.propTypes = {
  background: _propTypes.default.string,
  corner: _propTypes.default.number,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Rectangle;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvUmVjdGFuZ2xlLmpzIl0sIm5hbWVzIjpbIlJlY3RhbmdsZSIsImJhY2tncm91bmQiLCJjb3JuZXIiLCJoZWlndGgiLCJzdHJva2UiLCJzdHJva2VXaWR0aCIsIndpZHRoIiwicmVhbENvcm5lciIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxHQU9QO0FBQUEsaUZBQVAsRUFBTztBQUFBLDZCQU5QQyxVQU1PO0FBQUEsTUFOUEEsVUFNTyxnQ0FOTSxNQU1OO0FBQUEseUJBTFBDLE1BS087QUFBQSxNQUxQQSxNQUtPLDRCQUxFLENBS0Y7QUFBQSxNQUpQQyxNQUlPLFFBSlBBLE1BSU87QUFBQSx5QkFIUEMsTUFHTztBQUFBLE1BSFBBLE1BR08sNEJBSEUsTUFHRjtBQUFBLDhCQUZQQyxXQUVPO0FBQUEsTUFGUEEsV0FFTyxpQ0FGTyxDQUVQO0FBQUEsTUFEUEMsS0FDTyxRQURQQSxLQUNPOztBQUNQLE1BQU1DLFVBQVUsR0FDWkosTUFBTSxHQUFHRyxLQUFULEdBQWlCSCxNQUFNLElBQUlELE1BQU0sR0FBRyxHQUFiLENBQXZCLEdBQTJDSSxLQUFLLElBQUlKLE1BQU0sR0FBRyxHQUFiLENBRHBEO0FBR0Esc0JBQ0k7QUFDSSxJQUFBLEtBQUssRUFBRUksS0FEWDtBQUVJLElBQUEsTUFBTSxFQUFFSCxNQUZaO0FBR0ksSUFBQSxPQUFPLEVBQUMsS0FIWjtBQUlJLElBQUEsS0FBSyxFQUFDLDRCQUpWO0FBQUEsMkJBTUk7QUFDSSxNQUFBLENBQUMsRUFBRUUsV0FBVyxHQUFHLENBRHJCO0FBRUksTUFBQSxDQUFDLEVBQUVBLFdBQVcsR0FBRyxDQUZyQjtBQUdJLE1BQUEsRUFBRSxFQUFFRSxVQUhSO0FBSUksTUFBQSxFQUFFLEVBQUVBLFVBSlI7QUFLSSxNQUFBLEtBQUssRUFBRUQsS0FBSyxHQUFHRCxXQUxuQjtBQU1JLE1BQUEsTUFBTSxFQUFFRixNQUFNLEdBQUdFLFdBTnJCO0FBT0ksTUFBQSxNQUFNLEVBQUVELE1BUFo7QUFRSSxNQUFBLElBQUksRUFBRUgsVUFSVjtBQVNJLE1BQUEsV0FBVyxFQUFFSTtBQVRqQjtBQU5KLElBREo7QUFvQkgsQ0EvQkQ7O0FBaUNBTCxTQUFTLENBQUNRLFNBQVYsR0FBc0I7QUFDbEJQLEVBQUFBLFVBQVUsRUFBRVEsbUJBQVVDLE1BREo7QUFFbEJSLEVBQUFBLE1BQU0sRUFBRU8sbUJBQVVFLE1BRkE7QUFHbEJSLEVBQUFBLE1BQU0sRUFBRU0sbUJBQVVFLE1BSEE7QUFJbEJQLEVBQUFBLE1BQU0sRUFBRUssbUJBQVVDLE1BSkE7QUFLbEJMLEVBQUFBLFdBQVcsRUFBRUksbUJBQVVFLE1BTEw7QUFNbEJMLEVBQUFBLEtBQUssRUFBRUcsbUJBQVVFO0FBTkMsQ0FBdEI7ZUFTZVgsUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmNvbnN0IFJlY3RhbmdsZSA9ICh7XG4gICAgYmFja2dyb3VuZCA9ICcjMDAwJyxcbiAgICBjb3JuZXIgPSAwLFxuICAgIGhlaWd0aCxcbiAgICBzdHJva2UgPSAnIzAwMCcsXG4gICAgc3Ryb2tlV2lkdGggPSAwLFxuICAgIHdpZHRoLFxufSA9IHt9KSA9PiB7XG4gICAgY29uc3QgcmVhbENvcm5lciA9XG4gICAgICAgIGhlaWd0aCA8IHdpZHRoID8gaGVpZ3RoICogKGNvcm5lciAvIDEwMCkgOiB3aWR0aCAqIChjb3JuZXIgLyAxMDApO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPHN2Z1xuICAgICAgICAgICAgd2lkdGg9e3dpZHRofVxuICAgICAgICAgICAgaGVpZ2h0PXtoZWlndGh9XG4gICAgICAgICAgICB2ZXJzaW9uPVwiMS4xXCJcbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICA+XG4gICAgICAgICAgICA8cmVjdFxuICAgICAgICAgICAgICAgIHg9e3N0cm9rZVdpZHRoIC8gMn1cbiAgICAgICAgICAgICAgICB5PXtzdHJva2VXaWR0aCAvIDJ9XG4gICAgICAgICAgICAgICAgcng9e3JlYWxDb3JuZXJ9XG4gICAgICAgICAgICAgICAgcnk9e3JlYWxDb3JuZXJ9XG4gICAgICAgICAgICAgICAgd2lkdGg9e3dpZHRoIC0gc3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAgICAgaGVpZ2h0PXtoZWlndGggLSBzdHJva2VXaWR0aH1cbiAgICAgICAgICAgICAgICBzdHJva2U9e3N0cm9rZX1cbiAgICAgICAgICAgICAgICBmaWxsPXtiYWNrZ3JvdW5kfVxuICAgICAgICAgICAgICAgIHN0cm9rZVdpZHRoPXtzdHJva2VXaWR0aH1cbiAgICAgICAgICAgIC8+XG4gICAgICAgIDwvc3ZnPlxuICAgICk7XG59O1xuXG5SZWN0YW5nbGUucHJvcFR5cGVzID0ge1xuICAgIGJhY2tncm91bmQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29ybmVyOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIGhlaWd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBzdHJva2U6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3Ryb2tlV2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgd2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBSZWN0YW5nbGU7XG4iXX0=