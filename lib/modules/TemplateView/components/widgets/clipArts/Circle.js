"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Circle = function Circle() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  var cx = width / 2;
  var cy = heigth / 2;
  var radius = (heigth < width ? cy : cx) - strokeWidth / 2;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("circle", {
      cx: cx,
      cy: cy,
      r: radius,
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Circle.propTypes = {
  background: _propTypes.default.string,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Circle;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvQ2lyY2xlLmpzIl0sIm5hbWVzIjpbIkNpcmNsZSIsImJhY2tncm91bmQiLCJoZWlndGgiLCJzdHJva2UiLCJzdHJva2VXaWR0aCIsIndpZHRoIiwiY3giLCJjeSIsInJhZGl1cyIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7QUFFQSxJQUFNQSxNQUFNLEdBQUcsU0FBVEEsTUFBUyxHQU1KO0FBQUEsaUZBQVAsRUFBTztBQUFBLDZCQUxQQyxVQUtPO0FBQUEsTUFMUEEsVUFLTyxnQ0FMTSxNQUtOO0FBQUEsTUFKUEMsTUFJTyxRQUpQQSxNQUlPO0FBQUEseUJBSFBDLE1BR087QUFBQSxNQUhQQSxNQUdPLDRCQUhFLE1BR0Y7QUFBQSw4QkFGUEMsV0FFTztBQUFBLE1BRlBBLFdBRU8saUNBRk8sQ0FFUDtBQUFBLE1BRFBDLEtBQ08sUUFEUEEsS0FDTzs7QUFDUCxNQUFNQyxFQUFFLEdBQUdELEtBQUssR0FBRyxDQUFuQjtBQUNBLE1BQU1FLEVBQUUsR0FBR0wsTUFBTSxHQUFHLENBQXBCO0FBQ0EsTUFBTU0sTUFBTSxHQUFHLENBQUNOLE1BQU0sR0FBR0csS0FBVCxHQUFpQkUsRUFBakIsR0FBc0JELEVBQXZCLElBQTZCRixXQUFXLEdBQUcsQ0FBMUQ7QUFFQSxzQkFDSTtBQUNJLElBQUEsS0FBSyxFQUFFQyxLQURYO0FBRUksSUFBQSxNQUFNLEVBQUVILE1BRlo7QUFHSSxJQUFBLE9BQU8sRUFBQyxLQUhaO0FBSUksSUFBQSxLQUFLLEVBQUMsNEJBSlY7QUFBQSwyQkFNSTtBQUNJLE1BQUEsRUFBRSxFQUFFSSxFQURSO0FBRUksTUFBQSxFQUFFLEVBQUVDLEVBRlI7QUFHSSxNQUFBLENBQUMsRUFBRUMsTUFIUDtBQUlJLE1BQUEsTUFBTSxFQUFFTCxNQUpaO0FBS0ksTUFBQSxJQUFJLEVBQUVGLFVBTFY7QUFNSSxNQUFBLFdBQVcsRUFBRUc7QUFOakI7QUFOSixJQURKO0FBaUJILENBNUJEOztBQThCQUosTUFBTSxDQUFDUyxTQUFQLEdBQW1CO0FBQ2ZSLEVBQUFBLFVBQVUsRUFBRVMsbUJBQVVDLE1BRFA7QUFFZlQsRUFBQUEsTUFBTSxFQUFFUSxtQkFBVUUsTUFGSDtBQUdmVCxFQUFBQSxNQUFNLEVBQUVPLG1CQUFVQyxNQUhIO0FBSWZQLEVBQUFBLFdBQVcsRUFBRU0sbUJBQVVFLE1BSlI7QUFLZlAsRUFBQUEsS0FBSyxFQUFFSyxtQkFBVUU7QUFMRixDQUFuQjtlQVFlWixNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgQ2lyY2xlID0gKHtcbiAgICBiYWNrZ3JvdW5kID0gJyMwMDAnLFxuICAgIGhlaWd0aCxcbiAgICBzdHJva2UgPSAnIzAwMCcsXG4gICAgc3Ryb2tlV2lkdGggPSAwLFxuICAgIHdpZHRoLFxufSA9IHt9KSA9PiB7XG4gICAgY29uc3QgY3ggPSB3aWR0aCAvIDI7XG4gICAgY29uc3QgY3kgPSBoZWlndGggLyAyO1xuICAgIGNvbnN0IHJhZGl1cyA9IChoZWlndGggPCB3aWR0aCA/IGN5IDogY3gpIC0gc3Ryb2tlV2lkdGggLyAyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPHN2Z1xuICAgICAgICAgICAgd2lkdGg9e3dpZHRofVxuICAgICAgICAgICAgaGVpZ2h0PXtoZWlndGh9XG4gICAgICAgICAgICB2ZXJzaW9uPVwiMS4xXCJcbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICA+XG4gICAgICAgICAgICA8Y2lyY2xlXG4gICAgICAgICAgICAgICAgY3g9e2N4fVxuICAgICAgICAgICAgICAgIGN5PXtjeX1cbiAgICAgICAgICAgICAgICByPXtyYWRpdXN9XG4gICAgICAgICAgICAgICAgc3Ryb2tlPXtzdHJva2V9XG4gICAgICAgICAgICAgICAgZmlsbD17YmFja2dyb3VuZH1cbiAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17c3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L3N2Zz5cbiAgICApO1xufTtcblxuQ2lyY2xlLnByb3BUeXBlcyA9IHtcbiAgICBiYWNrZ3JvdW5kOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGhlaWd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBzdHJva2U6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3Ryb2tlV2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgd2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDaXJjbGU7XG4iXX0=