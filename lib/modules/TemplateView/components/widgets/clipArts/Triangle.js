"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Triangle = function Triangle() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("polygon", {
      points: "".concat(width / 2, " ").concat(strokeWidth, " ").concat(strokeWidth, " ").concat(heigth - strokeWidth / 2, " ").concat(width - strokeWidth, " ").concat(heigth - strokeWidth / 2),
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Triangle.propTypes = {
  background: _propTypes.default.string,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Triangle;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvVHJpYW5nbGUuanMiXSwibmFtZXMiOlsiVHJpYW5nbGUiLCJiYWNrZ3JvdW5kIiwiaGVpZ3RoIiwic3Ryb2tlIiwic3Ryb2tlV2lkdGgiLCJ3aWR0aCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7QUFFQSxJQUFNQSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQU1OO0FBQUEsaUZBQVAsRUFBTztBQUFBLDZCQUxQQyxVQUtPO0FBQUEsTUFMUEEsVUFLTyxnQ0FMTSxNQUtOO0FBQUEsTUFKUEMsTUFJTyxRQUpQQSxNQUlPO0FBQUEseUJBSFBDLE1BR087QUFBQSxNQUhQQSxNQUdPLDRCQUhFLE1BR0Y7QUFBQSw4QkFGUEMsV0FFTztBQUFBLE1BRlBBLFdBRU8saUNBRk8sQ0FFUDtBQUFBLE1BRFBDLEtBQ08sUUFEUEEsS0FDTzs7QUFDUCxzQkFDSTtBQUNJLElBQUEsS0FBSyxFQUFFQSxLQURYO0FBRUksSUFBQSxNQUFNLEVBQUVILE1BRlo7QUFHSSxJQUFBLE9BQU8sRUFBQyxLQUhaO0FBSUksSUFBQSxLQUFLLEVBQUMsNEJBSlY7QUFBQSwyQkFNSTtBQUNJLE1BQUEsTUFBTSxZQUFLRyxLQUFLLEdBQUcsQ0FBYixjQUFrQkQsV0FBbEIsY0FBaUNBLFdBQWpDLGNBQ0ZGLE1BQU0sR0FBR0UsV0FBVyxHQUFHLENBRHJCLGNBRUZDLEtBQUssR0FBR0QsV0FGTixjQUVxQkYsTUFBTSxHQUFHRSxXQUFXLEdBQUcsQ0FGNUMsQ0FEVjtBQUlJLE1BQUEsTUFBTSxFQUFFRCxNQUpaO0FBS0ksTUFBQSxJQUFJLEVBQUVGLFVBTFY7QUFNSSxNQUFBLFdBQVcsRUFBRUc7QUFOakI7QUFOSixJQURKO0FBaUJILENBeEJEOztBQTBCQUosUUFBUSxDQUFDTSxTQUFULEdBQXFCO0FBQ2pCTCxFQUFBQSxVQUFVLEVBQUVNLG1CQUFVQyxNQURMO0FBRWpCTixFQUFBQSxNQUFNLEVBQUVLLG1CQUFVRSxNQUZEO0FBR2pCTixFQUFBQSxNQUFNLEVBQUVJLG1CQUFVQyxNQUhEO0FBSWpCSixFQUFBQSxXQUFXLEVBQUVHLG1CQUFVRSxNQUpOO0FBS2pCSixFQUFBQSxLQUFLLEVBQUVFLG1CQUFVRTtBQUxBLENBQXJCO2VBUWVULFEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5jb25zdCBUcmlhbmdsZSA9ICh7XG4gICAgYmFja2dyb3VuZCA9ICcjMDAwJyxcbiAgICBoZWlndGgsXG4gICAgc3Ryb2tlID0gJyMwMDAnLFxuICAgIHN0cm9rZVdpZHRoID0gMCxcbiAgICB3aWR0aCxcbn0gPSB7fSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxzdmdcbiAgICAgICAgICAgIHdpZHRoPXt3aWR0aH1cbiAgICAgICAgICAgIGhlaWdodD17aGVpZ3RofVxuICAgICAgICAgICAgdmVyc2lvbj1cIjEuMVwiXG4gICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgPlxuICAgICAgICAgICAgPHBvbHlnb25cbiAgICAgICAgICAgICAgICBwb2ludHM9e2Ake3dpZHRoIC8gMn0gJHtzdHJva2VXaWR0aH0gJHtzdHJva2VXaWR0aH0gJHtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ3RoIC0gc3Ryb2tlV2lkdGggLyAyXG4gICAgICAgICAgICAgICAgfSAke3dpZHRoIC0gc3Ryb2tlV2lkdGh9ICR7aGVpZ3RoIC0gc3Ryb2tlV2lkdGggLyAyfWB9XG4gICAgICAgICAgICAgICAgc3Ryb2tlPXtzdHJva2V9XG4gICAgICAgICAgICAgICAgZmlsbD17YmFja2dyb3VuZH1cbiAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17c3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L3N2Zz5cbiAgICApO1xufTtcblxuVHJpYW5nbGUucHJvcFR5cGVzID0ge1xuICAgIGJhY2tncm91bmQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGVpZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHN0cm9rZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzdHJva2VXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB3aWR0aDogUHJvcFR5cGVzLm51bWJlcixcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRyaWFuZ2xlO1xuIl19