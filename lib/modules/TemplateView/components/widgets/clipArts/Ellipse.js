"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Ellipse = function Ellipse() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  var cx = width / 2;
  var cy = heigth / 2;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("ellipse", {
      cx: cx,
      cy: cy,
      rx: cx - strokeWidth / 2,
      ry: cy - strokeWidth / 2,
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Ellipse.propTypes = {
  background: _propTypes.default.string,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Ellipse;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvRWxsaXBzZS5qcyJdLCJuYW1lcyI6WyJFbGxpcHNlIiwiYmFja2dyb3VuZCIsImhlaWd0aCIsInN0cm9rZSIsInN0cm9rZVdpZHRoIiwid2lkdGgiLCJjeCIsImN5IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwibnVtYmVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7OztBQUVBLElBQU1BLE9BQU8sR0FBRyxTQUFWQSxPQUFVLEdBTUw7QUFBQSxpRkFBUCxFQUFPO0FBQUEsNkJBTFBDLFVBS087QUFBQSxNQUxQQSxVQUtPLGdDQUxNLE1BS047QUFBQSxNQUpQQyxNQUlPLFFBSlBBLE1BSU87QUFBQSx5QkFIUEMsTUFHTztBQUFBLE1BSFBBLE1BR08sNEJBSEUsTUFHRjtBQUFBLDhCQUZQQyxXQUVPO0FBQUEsTUFGUEEsV0FFTyxpQ0FGTyxDQUVQO0FBQUEsTUFEUEMsS0FDTyxRQURQQSxLQUNPOztBQUNQLE1BQU1DLEVBQUUsR0FBR0QsS0FBSyxHQUFHLENBQW5CO0FBQ0EsTUFBTUUsRUFBRSxHQUFHTCxNQUFNLEdBQUcsQ0FBcEI7QUFFQSxzQkFDSTtBQUNJLElBQUEsS0FBSyxFQUFFRyxLQURYO0FBRUksSUFBQSxNQUFNLEVBQUVILE1BRlo7QUFHSSxJQUFBLE9BQU8sRUFBQyxLQUhaO0FBSUksSUFBQSxLQUFLLEVBQUMsNEJBSlY7QUFBQSwyQkFNSTtBQUNJLE1BQUEsRUFBRSxFQUFFSSxFQURSO0FBRUksTUFBQSxFQUFFLEVBQUVDLEVBRlI7QUFHSSxNQUFBLEVBQUUsRUFBRUQsRUFBRSxHQUFHRixXQUFXLEdBQUcsQ0FIM0I7QUFJSSxNQUFBLEVBQUUsRUFBRUcsRUFBRSxHQUFHSCxXQUFXLEdBQUcsQ0FKM0I7QUFLSSxNQUFBLE1BQU0sRUFBRUQsTUFMWjtBQU1JLE1BQUEsSUFBSSxFQUFFRixVQU5WO0FBT0ksTUFBQSxXQUFXLEVBQUVHO0FBUGpCO0FBTkosSUFESjtBQWtCSCxDQTVCRDs7QUE4QkFKLE9BQU8sQ0FBQ1EsU0FBUixHQUFvQjtBQUNoQlAsRUFBQUEsVUFBVSxFQUFFUSxtQkFBVUMsTUFETjtBQUVoQlIsRUFBQUEsTUFBTSxFQUFFTyxtQkFBVUUsTUFGRjtBQUdoQlIsRUFBQUEsTUFBTSxFQUFFTSxtQkFBVUMsTUFIRjtBQUloQk4sRUFBQUEsV0FBVyxFQUFFSyxtQkFBVUUsTUFKUDtBQUtoQk4sRUFBQUEsS0FBSyxFQUFFSSxtQkFBVUU7QUFMRCxDQUFwQjtlQVFlWCxPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgRWxsaXBzZSA9ICh7XG4gICAgYmFja2dyb3VuZCA9ICcjMDAwJyxcbiAgICBoZWlndGgsXG4gICAgc3Ryb2tlID0gJyMwMDAnLFxuICAgIHN0cm9rZVdpZHRoID0gMCxcbiAgICB3aWR0aCxcbn0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IGN4ID0gd2lkdGggLyAyO1xuICAgIGNvbnN0IGN5ID0gaGVpZ3RoIC8gMjtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxzdmdcbiAgICAgICAgICAgIHdpZHRoPXt3aWR0aH1cbiAgICAgICAgICAgIGhlaWdodD17aGVpZ3RofVxuICAgICAgICAgICAgdmVyc2lvbj1cIjEuMVwiXG4gICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgPlxuICAgICAgICAgICAgPGVsbGlwc2VcbiAgICAgICAgICAgICAgICBjeD17Y3h9XG4gICAgICAgICAgICAgICAgY3k9e2N5fVxuICAgICAgICAgICAgICAgIHJ4PXtjeCAtIHN0cm9rZVdpZHRoIC8gMn1cbiAgICAgICAgICAgICAgICByeT17Y3kgLSBzdHJva2VXaWR0aCAvIDJ9XG4gICAgICAgICAgICAgICAgc3Ryb2tlPXtzdHJva2V9XG4gICAgICAgICAgICAgICAgZmlsbD17YmFja2dyb3VuZH1cbiAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17c3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L3N2Zz5cbiAgICApO1xufTtcblxuRWxsaXBzZS5wcm9wVHlwZXMgPSB7XG4gICAgYmFja2dyb3VuZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBoZWlndGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgc3Ryb2tlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHN0cm9rZVdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgRWxsaXBzZTtcbiJdfQ==