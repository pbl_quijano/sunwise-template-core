"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Hexagon = function Hexagon() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  var fourWidth = width / 4;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("polygon", {
      points: "".concat(fourWidth, " ").concat(strokeWidth / 2, " ").concat(fourWidth * 3, " ").concat(strokeWidth / 2, " ").concat(width - strokeWidth / 2, " ").concat(heigth / 2, " ").concat(fourWidth * 3, " ").concat(heigth - strokeWidth / 2, " ").concat(fourWidth, " ").concat(heigth - strokeWidth / 2, " ").concat(strokeWidth / 2, " ").concat(heigth / 2),
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Hexagon.propTypes = {
  background: _propTypes.default.string,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Hexagon;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvSGV4YWdvbi5qcyJdLCJuYW1lcyI6WyJIZXhhZ29uIiwiYmFja2dyb3VuZCIsImhlaWd0aCIsInN0cm9rZSIsInN0cm9rZVdpZHRoIiwid2lkdGgiLCJmb3VyV2lkdGgiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJudW1iZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7O0FBRUEsSUFBTUEsT0FBTyxHQUFHLFNBQVZBLE9BQVUsR0FNTDtBQUFBLGlGQUFQLEVBQU87QUFBQSw2QkFMUEMsVUFLTztBQUFBLE1BTFBBLFVBS08sZ0NBTE0sTUFLTjtBQUFBLE1BSlBDLE1BSU8sUUFKUEEsTUFJTztBQUFBLHlCQUhQQyxNQUdPO0FBQUEsTUFIUEEsTUFHTyw0QkFIRSxNQUdGO0FBQUEsOEJBRlBDLFdBRU87QUFBQSxNQUZQQSxXQUVPLGlDQUZPLENBRVA7QUFBQSxNQURQQyxLQUNPLFFBRFBBLEtBQ087O0FBQ1AsTUFBTUMsU0FBUyxHQUFHRCxLQUFLLEdBQUcsQ0FBMUI7QUFDQSxzQkFDSTtBQUNJLElBQUEsS0FBSyxFQUFFQSxLQURYO0FBRUksSUFBQSxNQUFNLEVBQUVILE1BRlo7QUFHSSxJQUFBLE9BQU8sRUFBQyxLQUhaO0FBSUksSUFBQSxLQUFLLEVBQUMsNEJBSlY7QUFBQSwyQkFNSTtBQUNJLE1BQUEsTUFBTSxZQUFLSSxTQUFMLGNBQWtCRixXQUFXLEdBQUcsQ0FBaEMsY0FBcUNFLFNBQVMsR0FBRyxDQUFqRCxjQUNGRixXQUFXLEdBQUcsQ0FEWixjQUVGQyxLQUFLLEdBQUdELFdBQVcsR0FBRyxDQUZwQixjQUV5QkYsTUFBTSxHQUFHLENBRmxDLGNBRXVDSSxTQUFTLEdBQUcsQ0FGbkQsY0FHRkosTUFBTSxHQUFHRSxXQUFXLEdBQUcsQ0FIckIsY0FJRkUsU0FKRSxjQUlXSixNQUFNLEdBQUdFLFdBQVcsR0FBRyxDQUpsQyxjQUl1Q0EsV0FBVyxHQUFHLENBSnJELGNBS0ZGLE1BQU0sR0FBRyxDQUxQLENBRFY7QUFRSSxNQUFBLE1BQU0sRUFBRUMsTUFSWjtBQVNJLE1BQUEsSUFBSSxFQUFFRixVQVRWO0FBVUksTUFBQSxXQUFXLEVBQUVHO0FBVmpCO0FBTkosSUFESjtBQXFCSCxDQTdCRDs7QUErQkFKLE9BQU8sQ0FBQ08sU0FBUixHQUFvQjtBQUNoQk4sRUFBQUEsVUFBVSxFQUFFTyxtQkFBVUMsTUFETjtBQUVoQlAsRUFBQUEsTUFBTSxFQUFFTSxtQkFBVUUsTUFGRjtBQUdoQlAsRUFBQUEsTUFBTSxFQUFFSyxtQkFBVUMsTUFIRjtBQUloQkwsRUFBQUEsV0FBVyxFQUFFSSxtQkFBVUUsTUFKUDtBQUtoQkwsRUFBQUEsS0FBSyxFQUFFRyxtQkFBVUU7QUFMRCxDQUFwQjtlQVFlVixPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgSGV4YWdvbiA9ICh7XG4gICAgYmFja2dyb3VuZCA9ICcjMDAwJyxcbiAgICBoZWlndGgsXG4gICAgc3Ryb2tlID0gJyMwMDAnLFxuICAgIHN0cm9rZVdpZHRoID0gMCxcbiAgICB3aWR0aCxcbn0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IGZvdXJXaWR0aCA9IHdpZHRoIC8gNDtcbiAgICByZXR1cm4gKFxuICAgICAgICA8c3ZnXG4gICAgICAgICAgICB3aWR0aD17d2lkdGh9XG4gICAgICAgICAgICBoZWlnaHQ9e2hlaWd0aH1cbiAgICAgICAgICAgIHZlcnNpb249XCIxLjFcIlxuICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgID5cbiAgICAgICAgICAgIDxwb2x5Z29uXG4gICAgICAgICAgICAgICAgcG9pbnRzPXtgJHtmb3VyV2lkdGh9ICR7c3Ryb2tlV2lkdGggLyAyfSAke2ZvdXJXaWR0aCAqIDN9ICR7XG4gICAgICAgICAgICAgICAgICAgIHN0cm9rZVdpZHRoIC8gMlxuICAgICAgICAgICAgICAgIH0gJHt3aWR0aCAtIHN0cm9rZVdpZHRoIC8gMn0gJHtoZWlndGggLyAyfSAke2ZvdXJXaWR0aCAqIDN9ICR7XG4gICAgICAgICAgICAgICAgICAgIGhlaWd0aCAtIHN0cm9rZVdpZHRoIC8gMlxuICAgICAgICAgICAgICAgIH0gJHtmb3VyV2lkdGh9ICR7aGVpZ3RoIC0gc3Ryb2tlV2lkdGggLyAyfSAke3N0cm9rZVdpZHRoIC8gMn0gJHtcbiAgICAgICAgICAgICAgICAgICAgaGVpZ3RoIC8gMlxuICAgICAgICAgICAgICAgIH1gfVxuICAgICAgICAgICAgICAgIHN0cm9rZT17c3Ryb2tlfVxuICAgICAgICAgICAgICAgIGZpbGw9e2JhY2tncm91bmR9XG4gICAgICAgICAgICAgICAgc3Ryb2tlV2lkdGg9e3N0cm9rZVdpZHRofVxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9zdmc+XG4gICAgKTtcbn07XG5cbkhleGFnb24ucHJvcFR5cGVzID0ge1xuICAgIGJhY2tncm91bmQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGVpZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHN0cm9rZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzdHJva2VXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB3aWR0aDogUHJvcFR5cGVzLm51bWJlcixcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEhleGFnb247XG4iXX0=