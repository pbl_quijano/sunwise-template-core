"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Circle", {
  enumerable: true,
  get: function get() {
    return _Circle.default;
  }
});
Object.defineProperty(exports, "Ellipse", {
  enumerable: true,
  get: function get() {
    return _Ellipse.default;
  }
});
Object.defineProperty(exports, "Hexagon", {
  enumerable: true,
  get: function get() {
    return _Hexagon.default;
  }
});
Object.defineProperty(exports, "Rectangle", {
  enumerable: true,
  get: function get() {
    return _Rectangle.default;
  }
});
Object.defineProperty(exports, "Square", {
  enumerable: true,
  get: function get() {
    return _Square.default;
  }
});
Object.defineProperty(exports, "Triangle", {
  enumerable: true,
  get: function get() {
    return _Triangle.default;
  }
});

var _Circle = _interopRequireDefault(require("./Circle"));

var _Ellipse = _interopRequireDefault(require("./Ellipse"));

var _Hexagon = _interopRequireDefault(require("./Hexagon"));

var _Rectangle = _interopRequireDefault(require("./Rectangle"));

var _Square = _interopRequireDefault(require("./Square"));

var _Triangle = _interopRequireDefault(require("./Triangle"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgeyBkZWZhdWx0IGFzIENpcmNsZSB9IGZyb20gJy4vQ2lyY2xlJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgRWxsaXBzZSB9IGZyb20gJy4vRWxsaXBzZSc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIEhleGFnb24gfSBmcm9tICcuL0hleGFnb24nO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyBSZWN0YW5nbGUgfSBmcm9tICcuL1JlY3RhbmdsZSc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFNxdWFyZSB9IGZyb20gJy4vU3F1YXJlJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgVHJpYW5nbGUgfSBmcm9tICcuL1RyaWFuZ2xlJztcbiJdfQ==