"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Square = function Square() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$background = _ref.background,
      background = _ref$background === void 0 ? '#000' : _ref$background,
      _ref$corner = _ref.corner,
      corner = _ref$corner === void 0 ? 0 : _ref$corner,
      heigth = _ref.heigth,
      _ref$stroke = _ref.stroke,
      stroke = _ref$stroke === void 0 ? '#000' : _ref$stroke,
      _ref$strokeWidth = _ref.strokeWidth,
      strokeWidth = _ref$strokeWidth === void 0 ? 0 : _ref$strokeWidth,
      width = _ref.width;

  var l = (heigth < width ? heigth : width) - strokeWidth;
  var x = heigth < width ? (width - l) / 2 : strokeWidth / 2;
  var y = heigth < width ? strokeWidth / 2 : (heigth - l) / 2;
  var realCorner = heigth < width ? heigth * (corner / 100) : width * (corner / 100);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)("svg", {
    width: width,
    height: heigth,
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)("rect", {
      x: x,
      y: y,
      rx: realCorner,
      ry: realCorner,
      width: l,
      height: l,
      stroke: stroke,
      fill: background,
      strokeWidth: strokeWidth
    })
  });
};

Square.propTypes = {
  background: _propTypes.default.string,
  corner: _propTypes.default.number,
  heigth: _propTypes.default.number,
  stroke: _propTypes.default.string,
  strokeWidth: _propTypes.default.number,
  width: _propTypes.default.number
};
var _default = Square;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvY2xpcEFydHMvU3F1YXJlLmpzIl0sIm5hbWVzIjpbIlNxdWFyZSIsImJhY2tncm91bmQiLCJjb3JuZXIiLCJoZWlndGgiLCJzdHJva2UiLCJzdHJva2VXaWR0aCIsIndpZHRoIiwibCIsIngiLCJ5IiwicmVhbENvcm5lciIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsIm51bWJlciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7QUFFQSxJQUFNQSxNQUFNLEdBQUcsU0FBVEEsTUFBUyxHQU9KO0FBQUEsaUZBQVAsRUFBTztBQUFBLDZCQU5QQyxVQU1PO0FBQUEsTUFOUEEsVUFNTyxnQ0FOTSxNQU1OO0FBQUEseUJBTFBDLE1BS087QUFBQSxNQUxQQSxNQUtPLDRCQUxFLENBS0Y7QUFBQSxNQUpQQyxNQUlPLFFBSlBBLE1BSU87QUFBQSx5QkFIUEMsTUFHTztBQUFBLE1BSFBBLE1BR08sNEJBSEUsTUFHRjtBQUFBLDhCQUZQQyxXQUVPO0FBQUEsTUFGUEEsV0FFTyxpQ0FGTyxDQUVQO0FBQUEsTUFEUEMsS0FDTyxRQURQQSxLQUNPOztBQUNQLE1BQU1DLENBQUMsR0FBRyxDQUFDSixNQUFNLEdBQUdHLEtBQVQsR0FBaUJILE1BQWpCLEdBQTBCRyxLQUEzQixJQUFvQ0QsV0FBOUM7QUFDQSxNQUFNRyxDQUFDLEdBQUdMLE1BQU0sR0FBR0csS0FBVCxHQUFpQixDQUFDQSxLQUFLLEdBQUdDLENBQVQsSUFBYyxDQUEvQixHQUFtQ0YsV0FBVyxHQUFHLENBQTNEO0FBQ0EsTUFBTUksQ0FBQyxHQUFHTixNQUFNLEdBQUdHLEtBQVQsR0FBaUJELFdBQVcsR0FBRyxDQUEvQixHQUFtQyxDQUFDRixNQUFNLEdBQUdJLENBQVYsSUFBZSxDQUE1RDtBQUNBLE1BQU1HLFVBQVUsR0FDWlAsTUFBTSxHQUFHRyxLQUFULEdBQWlCSCxNQUFNLElBQUlELE1BQU0sR0FBRyxHQUFiLENBQXZCLEdBQTJDSSxLQUFLLElBQUlKLE1BQU0sR0FBRyxHQUFiLENBRHBEO0FBR0Esc0JBQ0k7QUFDSSxJQUFBLEtBQUssRUFBRUksS0FEWDtBQUVJLElBQUEsTUFBTSxFQUFFSCxNQUZaO0FBR0ksSUFBQSxPQUFPLEVBQUMsS0FIWjtBQUlJLElBQUEsS0FBSyxFQUFDLDRCQUpWO0FBQUEsMkJBTUk7QUFDSSxNQUFBLENBQUMsRUFBRUssQ0FEUDtBQUVJLE1BQUEsQ0FBQyxFQUFFQyxDQUZQO0FBR0ksTUFBQSxFQUFFLEVBQUVDLFVBSFI7QUFJSSxNQUFBLEVBQUUsRUFBRUEsVUFKUjtBQUtJLE1BQUEsS0FBSyxFQUFFSCxDQUxYO0FBTUksTUFBQSxNQUFNLEVBQUVBLENBTlo7QUFPSSxNQUFBLE1BQU0sRUFBRUgsTUFQWjtBQVFJLE1BQUEsSUFBSSxFQUFFSCxVQVJWO0FBU0ksTUFBQSxXQUFXLEVBQUVJO0FBVGpCO0FBTkosSUFESjtBQW9CSCxDQWxDRDs7QUFvQ0FMLE1BQU0sQ0FBQ1csU0FBUCxHQUFtQjtBQUNmVixFQUFBQSxVQUFVLEVBQUVXLG1CQUFVQyxNQURQO0FBRWZYLEVBQUFBLE1BQU0sRUFBRVUsbUJBQVVFLE1BRkg7QUFHZlgsRUFBQUEsTUFBTSxFQUFFUyxtQkFBVUUsTUFISDtBQUlmVixFQUFBQSxNQUFNLEVBQUVRLG1CQUFVQyxNQUpIO0FBS2ZSLEVBQUFBLFdBQVcsRUFBRU8sbUJBQVVFLE1BTFI7QUFNZlIsRUFBQUEsS0FBSyxFQUFFTSxtQkFBVUU7QUFORixDQUFuQjtlQVNlZCxNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgU3F1YXJlID0gKHtcbiAgICBiYWNrZ3JvdW5kID0gJyMwMDAnLFxuICAgIGNvcm5lciA9IDAsXG4gICAgaGVpZ3RoLFxuICAgIHN0cm9rZSA9ICcjMDAwJyxcbiAgICBzdHJva2VXaWR0aCA9IDAsXG4gICAgd2lkdGgsXG59ID0ge30pID0+IHtcbiAgICBjb25zdCBsID0gKGhlaWd0aCA8IHdpZHRoID8gaGVpZ3RoIDogd2lkdGgpIC0gc3Ryb2tlV2lkdGg7XG4gICAgY29uc3QgeCA9IGhlaWd0aCA8IHdpZHRoID8gKHdpZHRoIC0gbCkgLyAyIDogc3Ryb2tlV2lkdGggLyAyO1xuICAgIGNvbnN0IHkgPSBoZWlndGggPCB3aWR0aCA/IHN0cm9rZVdpZHRoIC8gMiA6IChoZWlndGggLSBsKSAvIDI7XG4gICAgY29uc3QgcmVhbENvcm5lciA9XG4gICAgICAgIGhlaWd0aCA8IHdpZHRoID8gaGVpZ3RoICogKGNvcm5lciAvIDEwMCkgOiB3aWR0aCAqIChjb3JuZXIgLyAxMDApO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPHN2Z1xuICAgICAgICAgICAgd2lkdGg9e3dpZHRofVxuICAgICAgICAgICAgaGVpZ2h0PXtoZWlndGh9XG4gICAgICAgICAgICB2ZXJzaW9uPVwiMS4xXCJcbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICA+XG4gICAgICAgICAgICA8cmVjdFxuICAgICAgICAgICAgICAgIHg9e3h9XG4gICAgICAgICAgICAgICAgeT17eX1cbiAgICAgICAgICAgICAgICByeD17cmVhbENvcm5lcn1cbiAgICAgICAgICAgICAgICByeT17cmVhbENvcm5lcn1cbiAgICAgICAgICAgICAgICB3aWR0aD17bH1cbiAgICAgICAgICAgICAgICBoZWlnaHQ9e2x9XG4gICAgICAgICAgICAgICAgc3Ryb2tlPXtzdHJva2V9XG4gICAgICAgICAgICAgICAgZmlsbD17YmFja2dyb3VuZH1cbiAgICAgICAgICAgICAgICBzdHJva2VXaWR0aD17c3Ryb2tlV2lkdGh9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L3N2Zz5cbiAgICApO1xufTtcblxuU3F1YXJlLnByb3BUeXBlcyA9IHtcbiAgICBiYWNrZ3JvdW5kOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNvcm5lcjogUHJvcFR5cGVzLm51bWJlcixcbiAgICBoZWlndGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgc3Ryb2tlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHN0cm9rZVdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHdpZHRoOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgU3F1YXJlO1xuIl19