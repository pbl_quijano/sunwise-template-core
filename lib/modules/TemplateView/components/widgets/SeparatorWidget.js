"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: block;\n    height: 0;\n    width: 100%;\n"])));

var SeparatorWidget = function SeparatorWidget() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$innerHeight = _ref.innerHeight,
      innerHeight = _ref$innerHeight === void 0 ? 1 : _ref$innerHeight,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    style: _objectSpread(_objectSpread({}, style), {}, {
      borderBottomWidth: "".concat(innerHeight, "px")
    })
  });
};

SeparatorWidget.propTypes = {
  innerHeight: _propTypes.default.number,
  style: _propTypes.default.object
};
var _default = SeparatorWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU2VwYXJhdG9yV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsIlNlcGFyYXRvcldpZGdldCIsImlubmVySGVpZ2h0Iiwic3R5bGUiLCJib3JkZXJCb3R0b21XaWR0aCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm51bWJlciIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViw4SEFBZjs7QUFNQSxJQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCO0FBQUEsaUZBQW1DLEVBQW5DO0FBQUEsOEJBQUdDLFdBQUg7QUFBQSxNQUFHQSxXQUFILGlDQUFpQixDQUFqQjtBQUFBLHdCQUFvQkMsS0FBcEI7QUFBQSxNQUFvQkEsS0FBcEIsMkJBQTRCLEVBQTVCOztBQUFBLHNCQUNwQixxQkFBQyxTQUFEO0FBQVcsSUFBQSxLQUFLLGtDQUFPQSxLQUFQO0FBQWNDLE1BQUFBLGlCQUFpQixZQUFLRixXQUFMO0FBQS9CO0FBQWhCLElBRG9CO0FBQUEsQ0FBeEI7O0FBSUFELGVBQWUsQ0FBQ0ksU0FBaEIsR0FBNEI7QUFDeEJILEVBQUFBLFdBQVcsRUFBRUksbUJBQVVDLE1BREM7QUFFeEJKLEVBQUFBLEtBQUssRUFBRUcsbUJBQVVFO0FBRk8sQ0FBNUI7ZUFLZVAsZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBoZWlnaHQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBTZXBhcmF0b3JXaWRnZXQgPSAoeyBpbm5lckhlaWdodCA9IDEsIHN0eWxlID0ge30gfSA9IHt9KSA9PiAoXG4gICAgPENvbnRhaW5lciBzdHlsZT17eyAuLi5zdHlsZSwgYm9yZGVyQm90dG9tV2lkdGg6IGAke2lubmVySGVpZ2h0fXB4YCB9fSAvPlxuKTtcblxuU2VwYXJhdG9yV2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBpbm5lckhlaWdodDogUHJvcFR5cGVzLm51bWJlcixcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFNlcGFyYXRvcldpZGdldDtcbiJdfQ==