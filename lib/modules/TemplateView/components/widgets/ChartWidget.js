"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ChartView = _interopRequireDefault(require("./ChartView"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    overflow: hidden;\n"])));

var ChartWidget = function ChartWidget() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      currencyIso = _ref.currencyIso,
      currencyLocale = _ref.currencyLocale,
      editionDisabled = _ref.editionDisabled,
      innerHeight = _ref.innerHeight,
      innerWidth = _ref.innerWidth,
      _ref$value = _ref.value,
      value = _ref$value === void 0 ? {} : _ref$value;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    overflowYDisabled: editionDisabled,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ChartView.default, {
      currencyIso: currencyIso,
      currencyLocale: currencyLocale,
      data: value,
      innerHeight: innerHeight,
      innerWidth: innerWidth
    })
  });
};

ChartWidget.propTypes = {
  currencyIso: _propTypes.default.string,
  currencyLocale: _propTypes.default.string,
  editionDisabled: _propTypes.default.bool,
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = ChartWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvQ2hhcnRXaWRnZXQuanMiXSwibmFtZXMiOlsiQ29udGFpbmVyIiwic3R5bGVkIiwiZGl2IiwiQ2hhcnRXaWRnZXQiLCJjdXJyZW5jeUlzbyIsImN1cnJlbmN5TG9jYWxlIiwiZWRpdGlvbkRpc2FibGVkIiwiaW5uZXJIZWlnaHQiLCJpbm5lcldpZHRoIiwidmFsdWUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJib29sIiwibnVtYmVyIiwic3R5bGUiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsbUlBQWY7O0FBTUEsSUFBTUMsV0FBVyxHQUFHLFNBQWRBLFdBQWM7QUFBQSxpRkFPaEIsRUFQZ0I7QUFBQSxNQUNoQkMsV0FEZ0IsUUFDaEJBLFdBRGdCO0FBQUEsTUFFaEJDLGNBRmdCLFFBRWhCQSxjQUZnQjtBQUFBLE1BR2hCQyxlQUhnQixRQUdoQkEsZUFIZ0I7QUFBQSxNQUloQkMsV0FKZ0IsUUFJaEJBLFdBSmdCO0FBQUEsTUFLaEJDLFVBTGdCLFFBS2hCQSxVQUxnQjtBQUFBLHdCQU1oQkMsS0FOZ0I7QUFBQSxNQU1oQkEsS0FOZ0IsMkJBTVIsRUFOUTs7QUFBQSxzQkFRaEIscUJBQUMsU0FBRDtBQUFXLElBQUEsaUJBQWlCLEVBQUVILGVBQTlCO0FBQUEsMkJBQ0kscUJBQUMsa0JBQUQ7QUFDSSxNQUFBLFdBQVcsRUFBRUYsV0FEakI7QUFFSSxNQUFBLGNBQWMsRUFBRUMsY0FGcEI7QUFHSSxNQUFBLElBQUksRUFBRUksS0FIVjtBQUlJLE1BQUEsV0FBVyxFQUFFRixXQUpqQjtBQUtJLE1BQUEsVUFBVSxFQUFFQztBQUxoQjtBQURKLElBUmdCO0FBQUEsQ0FBcEI7O0FBbUJBTCxXQUFXLENBQUNPLFNBQVosR0FBd0I7QUFDcEJOLEVBQUFBLFdBQVcsRUFBRU8sbUJBQVVDLE1BREg7QUFFcEJQLEVBQUFBLGNBQWMsRUFBRU0sbUJBQVVDLE1BRk47QUFHcEJOLEVBQUFBLGVBQWUsRUFBRUssbUJBQVVFLElBSFA7QUFJcEJOLEVBQUFBLFdBQVcsRUFBRUksbUJBQVVHLE1BSkg7QUFLcEJOLEVBQUFBLFVBQVUsRUFBRUcsbUJBQVVHLE1BTEY7QUFNcEJDLEVBQUFBLEtBQUssRUFBRUosbUJBQVVLLE1BTkc7QUFPcEJQLEVBQUFBLEtBQUssRUFBRUUsbUJBQVVLO0FBUEcsQ0FBeEI7ZUFVZWIsVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IENoYXJ0VmlldyBmcm9tICcuL0NoYXJ0Vmlldyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5gO1xuXG5jb25zdCBDaGFydFdpZGdldCA9ICh7XG4gICAgY3VycmVuY3lJc28sXG4gICAgY3VycmVuY3lMb2NhbGUsXG4gICAgZWRpdGlvbkRpc2FibGVkLFxuICAgIGlubmVySGVpZ2h0LFxuICAgIGlubmVyV2lkdGgsXG4gICAgdmFsdWUgPSB7fSxcbn0gPSB7fSkgPT4gKFxuICAgIDxDb250YWluZXIgb3ZlcmZsb3dZRGlzYWJsZWQ9e2VkaXRpb25EaXNhYmxlZH0+XG4gICAgICAgIDxDaGFydFZpZXdcbiAgICAgICAgICAgIGN1cnJlbmN5SXNvPXtjdXJyZW5jeUlzb31cbiAgICAgICAgICAgIGN1cnJlbmN5TG9jYWxlPXtjdXJyZW5jeUxvY2FsZX1cbiAgICAgICAgICAgIGRhdGE9e3ZhbHVlfVxuICAgICAgICAgICAgaW5uZXJIZWlnaHQ9e2lubmVySGVpZ2h0fVxuICAgICAgICAgICAgaW5uZXJXaWR0aD17aW5uZXJXaWR0aH1cbiAgICAgICAgLz5cbiAgICA8L0NvbnRhaW5lcj5cbik7XG5cbkNoYXJ0V2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBjdXJyZW5jeUlzbzogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjdXJyZW5jeUxvY2FsZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBlZGl0aW9uRGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGlubmVySGVpZ2h0OiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIGlubmVyV2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDaGFydFdpZGdldDtcbiJdfQ==