"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _HelioScopeDefault = _interopRequireDefault(require("../../../../res/HelioScopeDefault.png"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    overflow-y: auto;\n    width: 100%;\n"])));

var Image = _styledComponents.default.img(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    ", "\n"])), function (_ref) {
  var isTemplate = _ref.isTemplate;
  return isTemplate && 'filter: grayscale(0.8);';
});

var HelioscopeWidget = function HelioscopeWidget() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      value = _ref2.value,
      _ref2$style = _ref2.style,
      style = _ref2$style === void 0 ? {} : _ref2$style;

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Image, {
      isTemplate: (0, _isEmpty.default)(value),
      src: (0, _isEmpty.default)(value) ? _HelioScopeDefault.default : value,
      style: style
    })
  });
};

HelioscopeWidget.propTypes = {
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = HelioscopeWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvSGVsaW9zY29wZVdpZGdldC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJJbWFnZSIsImltZyIsImlzVGVtcGxhdGUiLCJIZWxpb3Njb3BlV2lkZ2V0IiwidmFsdWUiLCJzdHlsZSIsIkhlbGlvU2NvcGVEZWZhdWx0IiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLG9MQUFmOztBQVFBLElBQU1DLEtBQUssR0FBR0YsMEJBQU9HLEdBQVYsNktBTUw7QUFBQSxNQUFHQyxVQUFILFFBQUdBLFVBQUg7QUFBQSxTQUFvQkEsVUFBVSxJQUFJLHlCQUFsQztBQUFBLENBTkssQ0FBWDs7QUFTQSxJQUFNQyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CO0FBQUEsa0ZBQXlCLEVBQXpCO0FBQUEsTUFBR0MsS0FBSCxTQUFHQSxLQUFIO0FBQUEsMEJBQVVDLEtBQVY7QUFBQSxNQUFVQSxLQUFWLDRCQUFrQixFQUFsQjs7QUFBQSxzQkFDckIscUJBQUMsU0FBRDtBQUFBLDJCQUNJLHFCQUFDLEtBQUQ7QUFDSSxNQUFBLFVBQVUsRUFBRSxzQkFBUUQsS0FBUixDQURoQjtBQUVJLE1BQUEsR0FBRyxFQUFFLHNCQUFRQSxLQUFSLElBQWlCRSwwQkFBakIsR0FBcUNGLEtBRjlDO0FBR0ksTUFBQSxLQUFLLEVBQUVDO0FBSFg7QUFESixJQURxQjtBQUFBLENBQXpCOztBQVVBRixnQkFBZ0IsQ0FBQ0ksU0FBakIsR0FBNkI7QUFDekJGLEVBQUFBLEtBQUssRUFBRUcsbUJBQVVDLE1BRFE7QUFFekJMLEVBQUFBLEtBQUssRUFBRUksbUJBQVVDO0FBRlEsQ0FBN0I7ZUFLZU4sZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaXNFbXB0eSBmcm9tICdsb2Rhc2gvaXNFbXB0eSc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBIZWxpb1Njb3BlRGVmYXVsdCBmcm9tICdAcmVzL0hlbGlvU2NvcGVEZWZhdWx0LnBuZyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIHdpZHRoOiAxMDAlO1xuYDtcblxuY29uc3QgSW1hZ2UgPSBzdHlsZWQuaW1nYFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgJHsoeyBpc1RlbXBsYXRlIH0pID0+IGlzVGVtcGxhdGUgJiYgJ2ZpbHRlcjogZ3JheXNjYWxlKDAuOCk7J31cbmA7XG5cbmNvbnN0IEhlbGlvc2NvcGVXaWRnZXQgPSAoeyB2YWx1ZSwgc3R5bGUgPSB7fSB9ID0ge30pID0+IChcbiAgICA8Q29udGFpbmVyPlxuICAgICAgICA8SW1hZ2VcbiAgICAgICAgICAgIGlzVGVtcGxhdGU9e2lzRW1wdHkodmFsdWUpfVxuICAgICAgICAgICAgc3JjPXtpc0VtcHR5KHZhbHVlKSA/IEhlbGlvU2NvcGVEZWZhdWx0IDogdmFsdWV9XG4gICAgICAgICAgICBzdHlsZT17c3R5bGV9XG4gICAgICAgIC8+XG4gICAgPC9Db250YWluZXI+XG4pO1xuXG5IZWxpb3Njb3BlV2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEhlbGlvc2NvcGVXaWRnZXQ7XG4iXX0=