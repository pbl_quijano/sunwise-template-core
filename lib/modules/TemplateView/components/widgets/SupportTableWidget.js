"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7, _templateObject8, _templateObject9, _templateObject10, _templateObject11;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    width: 100%;\n    height: 100%;\n    background-color: #fff;\n    ", "\n    border-top-width: 0px !important;\n"])), function (_ref) {
  var roundedBorders = _ref.roundedBorders;
  return roundedBorders && 'border-radius: 17px 17px 0 0;';
});

var TableContent = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    height: 100%;\n"])));

var HeaderContent = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    display: flex;\n    background-color: ", ";\n    ", "\n"])), function (_ref2) {
  var _ref2$backgroundColor = _ref2.backgroundColor,
      backgroundColor = _ref2$backgroundColor === void 0 ? '#000' : _ref2$backgroundColor;
  return backgroundColor;
}, function (_ref3) {
  var roundedBorders = _ref3.roundedBorders;
  return roundedBorders && 'border-radius: 17px;';
});

var HeaderData = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    display: flex;\n    flex: 1;\n    font-weight: ", ";\n    ", "\n    font-size: 13px;\n    color: ", ";\n    padding: 5px 15px;\n"])), function (_ref4) {
  var fontBold = _ref4.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref5) {
  var fontItalic = _ref5.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref6) {
  var _ref6$color = _ref6.color,
      color = _ref6$color === void 0 ? '#fff' : _ref6$color;
  return color;
});

var DataContent = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    flex: 1;\n    overflow-x: hidden;\n    overflow-y: ", ";\n"])), function (_ref7) {
  var overflowYDisabled = _ref7.overflowYDisabled;
  return overflowYDisabled ? 'hidden' : 'auto';
});

var BodyContent = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    background-color: ", ";\n"])), function (_ref8) {
  var _ref8$backgroundColor = _ref8.backgroundColor1,
      backgroundColor1 = _ref8$backgroundColor === void 0 ? '#fff' : _ref8$backgroundColor;
  return backgroundColor1;
});

var BodyDataRow = _styledComponents.default.div(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    display: flex;\n    ", "\n"])), function (_ref9) {
  var isZebraStyle = _ref9.isZebraStyle,
      backgroundColor2 = _ref9.backgroundColor2,
      index = _ref9.index;
  return isZebraStyle && (index + 1) % 2 === 0 && "background-color: ".concat(backgroundColor2, ";");
});

var BodyData = _styledComponents.default.div(_templateObject8 || (_templateObject8 = _taggedTemplateLiteral(["\n    font-weight: ", ";\n    ", "\n    font-size: 13px;\n    color: ", ";\n    flex: 1;\n    padding: 5px 15px;\n"])), function (_ref10) {
  var fontBold = _ref10.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref11) {
  var fontItalic = _ref11.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref12) {
  var _ref12$color = _ref12.color,
      color = _ref12$color === void 0 ? '#000' : _ref12$color;
  return color;
});

var FooterContent = _styledComponents.default.div(_templateObject9 || (_templateObject9 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    background-color: ", ";\n    border-top-style: ", ";\n    border-top-width: ", ";\n    border-top-color: ", ";\n    ", "\n"])), function (_ref13) {
  var _ref13$backgroundColo = _ref13.backgroundColor1,
      backgroundColor1 = _ref13$backgroundColo === void 0 ? '#fff' : _ref13$backgroundColo;
  return backgroundColor1;
}, function (_ref14) {
  var borderTopStyle = _ref14.borderTopStyle;
  return borderTopStyle;
}, function (_ref15) {
  var borderTopWidth = _ref15.borderTopWidth;
  return borderTopWidth;
}, function (_ref16) {
  var borderTopColor = _ref16.borderTopColor;
  return borderTopColor;
}, function (_ref17) {
  var isZebraStyle = _ref17.isZebraStyle,
      backgroundColor2 = _ref17.backgroundColor2;
  return isZebraStyle && ".footer-row:nth-child(even) {\n            background-color: ".concat(backgroundColor2, ";\n        }");
});

var FooterDataRow = _styledComponents.default.div(_templateObject10 || (_templateObject10 = _taggedTemplateLiteral(["\n    display: flex;\n"])));

var FooterData = _styledComponents.default.div(_templateObject11 || (_templateObject11 = _taggedTemplateLiteral(["\n    font-weight: ", ";\n    ", "\n    font-size: 13px;\n    color: ", ";\n    flex: 1;\n    padding: 5px 15px;\n"])), function (_ref18) {
  var fontBold = _ref18.fontBold;
  return fontBold ? 'bold' : '100';
}, function (_ref19) {
  var fontItalic = _ref19.fontItalic;
  return fontItalic && 'font-style: italic;';
}, function (_ref20) {
  var _ref20$color = _ref20.color,
      color = _ref20$color === void 0 ? '#000' : _ref20$color;
  return color;
});

var SupportTableWidget = function SupportTableWidget(_ref21) {
  var editionDisabled = _ref21.editionDisabled,
      _ref21$style = _ref21.style,
      style = _ref21$style === void 0 ? {} : _ref21$style,
      _ref21$value = _ref21.value,
      value = _ref21$value === void 0 ? {} : _ref21$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var _style$cols = style.cols,
      cols = _style$cols === void 0 ? [] : _style$cols,
      tableStyle = style.table,
      headerStyle = style.header,
      bodyStyle = style.body,
      footerStyle = style.footer,
      showTotal = style.showTotal,
      showIVA = style.showIVA,
      showDiscount = style.showDiscount;
  var _value$data = value.data,
      data = _value$data === void 0 ? [] : _value$data,
      _value$results = value.results,
      results = _value$results === void 0 ? {} : _value$results;

  var showDiscountsRow = function showDiscountsRow() {
    if (!showTotal || !showDiscount || cols.slice(1).filter(function (col) {
      return !(0, _isNil.default)((0, _get.default)(results, "discount.".concat(col.name), null));
    }).length <= 0) {
      return false;
    }

    return false;
  };

  var showIVARow = function showIVARow() {
    if (!showTotal || !showIVA || cols.slice(1).filter(function (col) {
      return !(0, _isNil.default)((0, _get.default)(results, "iva.".concat(col.name), null));
    }).length <= 0) {
      return false;
    }

    return true;
  };

  var getDataTextProps = function getDataTextProps(_ref22) {
    var fontBold = _ref22.fontBold,
        fontItalic = _ref22.fontItalic,
        color = _ref22.color;
    return {
      fontBold: fontBold,
      fontItalic: fontItalic,
      color: color
    };
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    roundedBorders: headerStyle.roundedBorders,
    style: tableStyle,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(TableContent, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(HeaderContent, {
        backgroundColor: headerStyle.backgroundColor,
        roundedBorders: headerStyle.roundedBorders,
        children: cols.map(function (col) {
          return /*#__PURE__*/(0, _jsxRuntime.jsx)(HeaderData, _objectSpread(_objectSpread({}, getDataTextProps(headerStyle)), {}, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
              children: col.label
            })
          }), col.name);
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(DataContent, {
        overflowYDisabled: editionDisabled,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(BodyContent, {
          backgroundColor1: bodyStyle.backgroundColor1,
          children: data.map(function (item, index) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)(BodyDataRow, {
              backgroundColor2: bodyStyle.backgroundColor2,
              index: index,
              isZebraStyle: bodyStyle.isZebraStyle,
              children: cols.map(function (col) {
                return /*#__PURE__*/(0, _jsxRuntime.jsx)(BodyData, _objectSpread(_objectSpread({}, getDataTextProps(bodyStyle)), {}, {
                  children: /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
                    children: item[col.name]
                  })
                }), "".concat(index, "-").concat(col.name));
              })
            }, index);
          })
        }), showTotal && /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterContent, _objectSpread(_objectSpread({}, footerStyle), {}, {
          children: [(showIVARow() || showDiscountsRow()) && showTotal && /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterDataRow, {
            className: "footer-row",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
              children: t('Subtotal')
            })), cols.slice(1).map(function (col) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
                children: (0, _get.default)(results, "subtotal.".concat(col.name), '')
              }), "subtotal-".concat(col.name));
            })]
          }), showIVARow() && /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterDataRow, {
            className: "footer-row",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
              children: t('Taxes')
            })), cols.slice(1).map(function (col) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
                children: (0, _get.default)(results, "iva.".concat(col.name), '')
              }), "iva-".concat(col.name));
            })]
          }), showDiscountsRow() && /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterDataRow, {
            className: "footer-row",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
              children: t('Discount')
            })), cols.slice(1).map(function (col) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
                children: (0, _get.default)(results, "discount.".concat(col.name), '')
              }), "discount-".concat(col.name));
            })]
          }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(FooterDataRow, {
            className: "footer-row",
            children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
              children: t('Total')
            })), cols.slice(1).map(function (col) {
              return /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterData, _objectSpread(_objectSpread({}, getDataTextProps(footerStyle)), {}, {
                children: (0, _get.default)(results, "total.".concat(col.name), '')
              }), "total-".concat(col.name));
            })]
          })]
        }))]
      })]
    })
  });
};

SupportTableWidget.propTypes = {
  editionDisabled: _propTypes.default.bool,
  style: _propTypes.default.object,
  value: _propTypes.default.object
};
var _default = SupportTableWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU3VwcG9ydFRhYmxlV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsInJvdW5kZWRCb3JkZXJzIiwiVGFibGVDb250ZW50IiwiSGVhZGVyQ29udGVudCIsImJhY2tncm91bmRDb2xvciIsIkhlYWRlckRhdGEiLCJmb250Qm9sZCIsImZvbnRJdGFsaWMiLCJjb2xvciIsIkRhdGFDb250ZW50Iiwib3ZlcmZsb3dZRGlzYWJsZWQiLCJCb2R5Q29udGVudCIsImJhY2tncm91bmRDb2xvcjEiLCJCb2R5RGF0YVJvdyIsImlzWmVicmFTdHlsZSIsImJhY2tncm91bmRDb2xvcjIiLCJpbmRleCIsIkJvZHlEYXRhIiwiRm9vdGVyQ29udGVudCIsImJvcmRlclRvcFN0eWxlIiwiYm9yZGVyVG9wV2lkdGgiLCJib3JkZXJUb3BDb2xvciIsIkZvb3RlckRhdGFSb3ciLCJGb290ZXJEYXRhIiwiU3VwcG9ydFRhYmxlV2lkZ2V0IiwiZWRpdGlvbkRpc2FibGVkIiwic3R5bGUiLCJ2YWx1ZSIsInQiLCJjb2xzIiwidGFibGVTdHlsZSIsInRhYmxlIiwiaGVhZGVyU3R5bGUiLCJoZWFkZXIiLCJib2R5U3R5bGUiLCJib2R5IiwiZm9vdGVyU3R5bGUiLCJmb290ZXIiLCJzaG93VG90YWwiLCJzaG93SVZBIiwic2hvd0Rpc2NvdW50IiwiZGF0YSIsInJlc3VsdHMiLCJzaG93RGlzY291bnRzUm93Iiwic2xpY2UiLCJmaWx0ZXIiLCJjb2wiLCJuYW1lIiwibGVuZ3RoIiwic2hvd0lWQVJvdyIsImdldERhdGFUZXh0UHJvcHMiLCJtYXAiLCJsYWJlbCIsIml0ZW0iLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJib29sIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLDJMQUlUO0FBQUEsTUFBR0MsY0FBSCxRQUFHQSxjQUFIO0FBQUEsU0FBd0JBLGNBQWMsSUFBSSwrQkFBMUM7QUFBQSxDQUpTLENBQWY7O0FBUUEsSUFBTUMsWUFBWSxHQUFHSCwwQkFBT0MsR0FBViwrSkFBbEI7O0FBT0EsSUFBTUcsYUFBYSxHQUFHSiwwQkFBT0MsR0FBVixxSUFFSztBQUFBLG9DQUFHSSxlQUFIO0FBQUEsTUFBR0EsZUFBSCxzQ0FBcUIsTUFBckI7QUFBQSxTQUFrQ0EsZUFBbEM7QUFBQSxDQUZMLEVBR2I7QUFBQSxNQUFHSCxjQUFILFNBQUdBLGNBQUg7QUFBQSxTQUF3QkEsY0FBYyxJQUFJLHNCQUExQztBQUFBLENBSGEsQ0FBbkI7O0FBTUEsSUFBTUksVUFBVSxHQUFHTiwwQkFBT0MsR0FBViw4TUFHRztBQUFBLE1BQUdNLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CQSxRQUFRLEdBQUcsTUFBSCxHQUFZLEtBQXZDO0FBQUEsQ0FISCxFQUlWO0FBQUEsTUFBR0MsVUFBSCxTQUFHQSxVQUFIO0FBQUEsU0FBb0JBLFVBQVUsSUFBSSxxQkFBbEM7QUFBQSxDQUpVLEVBTUg7QUFBQSwwQkFBR0MsS0FBSDtBQUFBLE1BQUdBLEtBQUgsNEJBQVcsTUFBWDtBQUFBLFNBQXdCQSxLQUF4QjtBQUFBLENBTkcsQ0FBaEI7O0FBVUEsSUFBTUMsV0FBVyxHQUFHViwwQkFBT0MsR0FBVix5TEFLQztBQUFBLE1BQUdVLGlCQUFILFNBQUdBLGlCQUFIO0FBQUEsU0FDVkEsaUJBQWlCLEdBQUcsUUFBSCxHQUFjLE1BRHJCO0FBQUEsQ0FMRCxDQUFqQjs7QUFRQSxJQUFNQyxXQUFXLEdBQUdaLDBCQUFPQyxHQUFWLHdKQUdPO0FBQUEsb0NBQUdZLGdCQUFIO0FBQUEsTUFBR0EsZ0JBQUgsc0NBQXNCLE1BQXRCO0FBQUEsU0FBbUNBLGdCQUFuQztBQUFBLENBSFAsQ0FBakI7O0FBTUEsSUFBTUMsV0FBVyxHQUFHZCwwQkFBT0MsR0FBVix3R0FFWDtBQUFBLE1BQUdjLFlBQUgsU0FBR0EsWUFBSDtBQUFBLE1BQWlCQyxnQkFBakIsU0FBaUJBLGdCQUFqQjtBQUFBLE1BQW1DQyxLQUFuQyxTQUFtQ0EsS0FBbkM7QUFBQSxTQUNFRixZQUFZLElBQ1osQ0FBQ0UsS0FBSyxHQUFHLENBQVQsSUFBYyxDQUFkLEtBQW9CLENBRHBCLGdDQUVxQkQsZ0JBRnJCLE1BREY7QUFBQSxDQUZXLENBQWpCOztBQVFBLElBQU1FLFFBQVEsR0FBR2xCLDBCQUFPQyxHQUFWLDBMQUNLO0FBQUEsTUFBR00sUUFBSCxVQUFHQSxRQUFIO0FBQUEsU0FBbUJBLFFBQVEsR0FBRyxNQUFILEdBQVksS0FBdkM7QUFBQSxDQURMLEVBRVI7QUFBQSxNQUFHQyxVQUFILFVBQUdBLFVBQUg7QUFBQSxTQUFvQkEsVUFBVSxJQUFJLHFCQUFsQztBQUFBLENBRlEsRUFJRDtBQUFBLDRCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCw2QkFBVyxNQUFYO0FBQUEsU0FBd0JBLEtBQXhCO0FBQUEsQ0FKQyxDQUFkOztBQVNBLElBQU1VLGFBQWEsR0FBR25CLDBCQUFPQyxHQUFWLHlQQUdLO0FBQUEscUNBQUdZLGdCQUFIO0FBQUEsTUFBR0EsZ0JBQUgsc0NBQXNCLE1BQXRCO0FBQUEsU0FBbUNBLGdCQUFuQztBQUFBLENBSEwsRUFJSztBQUFBLE1BQUdPLGNBQUgsVUFBR0EsY0FBSDtBQUFBLFNBQXdCQSxjQUF4QjtBQUFBLENBSkwsRUFLSztBQUFBLE1BQUdDLGNBQUgsVUFBR0EsY0FBSDtBQUFBLFNBQXdCQSxjQUF4QjtBQUFBLENBTEwsRUFNSztBQUFBLE1BQUdDLGNBQUgsVUFBR0EsY0FBSDtBQUFBLFNBQXdCQSxjQUF4QjtBQUFBLENBTkwsRUFPYjtBQUFBLE1BQUdQLFlBQUgsVUFBR0EsWUFBSDtBQUFBLE1BQWlCQyxnQkFBakIsVUFBaUJBLGdCQUFqQjtBQUFBLFNBQ0VELFlBQVksMkVBRVlDLGdCQUZaLGlCQURkO0FBQUEsQ0FQYSxDQUFuQjs7QUFjQSxJQUFNTyxhQUFhLEdBQUd2QiwwQkFBT0MsR0FBViwrRkFBbkI7O0FBSUEsSUFBTXVCLFVBQVUsR0FBR3hCLDBCQUFPQyxHQUFWLDRMQUNHO0FBQUEsTUFBR00sUUFBSCxVQUFHQSxRQUFIO0FBQUEsU0FBbUJBLFFBQVEsR0FBRyxNQUFILEdBQVksS0FBdkM7QUFBQSxDQURILEVBRVY7QUFBQSxNQUFHQyxVQUFILFVBQUdBLFVBQUg7QUFBQSxTQUFvQkEsVUFBVSxJQUFJLHFCQUFsQztBQUFBLENBRlUsRUFJSDtBQUFBLDRCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCw2QkFBVyxNQUFYO0FBQUEsU0FBd0JBLEtBQXhCO0FBQUEsQ0FKRyxDQUFoQjs7QUFTQSxJQUFNZ0Isa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixTQUFpRDtBQUFBLE1BQTlDQyxlQUE4QyxVQUE5Q0EsZUFBOEM7QUFBQSw0QkFBN0JDLEtBQTZCO0FBQUEsTUFBN0JBLEtBQTZCLDZCQUFyQixFQUFxQjtBQUFBLDRCQUFqQkMsS0FBaUI7QUFBQSxNQUFqQkEsS0FBaUIsNkJBQVQsRUFBUzs7QUFDeEUsd0JBQWMsbUNBQWQ7QUFBQSxNQUFRQyxDQUFSLG1CQUFRQSxDQUFSOztBQUNBLG9CQVNJRixLQVRKLENBQ0lHLElBREo7QUFBQSxNQUNJQSxJQURKLDRCQUNXLEVBRFg7QUFBQSxNQUVXQyxVQUZYLEdBU0lKLEtBVEosQ0FFSUssS0FGSjtBQUFBLE1BR1lDLFdBSFosR0FTSU4sS0FUSixDQUdJTyxNQUhKO0FBQUEsTUFJVUMsU0FKVixHQVNJUixLQVRKLENBSUlTLElBSko7QUFBQSxNQUtZQyxXQUxaLEdBU0lWLEtBVEosQ0FLSVcsTUFMSjtBQUFBLE1BTUlDLFNBTkosR0FTSVosS0FUSixDQU1JWSxTQU5KO0FBQUEsTUFPSUMsT0FQSixHQVNJYixLQVRKLENBT0lhLE9BUEo7QUFBQSxNQVFJQyxZQVJKLEdBU0lkLEtBVEosQ0FRSWMsWUFSSjtBQVVBLG9CQUFvQ2IsS0FBcEMsQ0FBUWMsSUFBUjtBQUFBLE1BQVFBLElBQVIsNEJBQWUsRUFBZjtBQUFBLHVCQUFvQ2QsS0FBcEMsQ0FBbUJlLE9BQW5CO0FBQUEsTUFBbUJBLE9BQW5CLCtCQUE2QixFQUE3Qjs7QUFFQSxNQUFNQyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLEdBQU07QUFDM0IsUUFDSSxDQUFDTCxTQUFELElBQ0EsQ0FBQ0UsWUFERCxJQUVBWCxJQUFJLENBQ0NlLEtBREwsQ0FDVyxDQURYLEVBRUtDLE1BRkwsQ0FHUSxVQUFDQyxHQUFEO0FBQUEsYUFBUyxDQUFDLG9CQUFNLGtCQUFJSixPQUFKLHFCQUF5QkksR0FBRyxDQUFDQyxJQUE3QixHQUFxQyxJQUFyQyxDQUFOLENBQVY7QUFBQSxLQUhSLEVBSU1DLE1BSk4sSUFJZ0IsQ0FQcEIsRUFRRTtBQUNFLGFBQU8sS0FBUDtBQUNIOztBQUNELFdBQU8sS0FBUDtBQUNILEdBYkQ7O0FBZUEsTUFBTUMsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBTTtBQUNyQixRQUNJLENBQUNYLFNBQUQsSUFDQSxDQUFDQyxPQURELElBRUFWLElBQUksQ0FDQ2UsS0FETCxDQUNXLENBRFgsRUFFS0MsTUFGTCxDQUVZLFVBQUNDLEdBQUQ7QUFBQSxhQUFTLENBQUMsb0JBQU0sa0JBQUlKLE9BQUosZ0JBQW9CSSxHQUFHLENBQUNDLElBQXhCLEdBQWdDLElBQWhDLENBQU4sQ0FBVjtBQUFBLEtBRlosRUFHS0MsTUFITCxJQUdlLENBTm5CLEVBT0U7QUFDRSxhQUFPLEtBQVA7QUFDSDs7QUFDRCxXQUFPLElBQVA7QUFDSCxHQVpEOztBQWNBLE1BQU1FLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUI7QUFBQSxRQUFHNUMsUUFBSCxVQUFHQSxRQUFIO0FBQUEsUUFBYUMsVUFBYixVQUFhQSxVQUFiO0FBQUEsUUFBeUJDLEtBQXpCLFVBQXlCQSxLQUF6QjtBQUFBLFdBQXNDO0FBQzNERixNQUFBQSxRQUFRLEVBQVJBLFFBRDJEO0FBRTNEQyxNQUFBQSxVQUFVLEVBQVZBLFVBRjJEO0FBRzNEQyxNQUFBQSxLQUFLLEVBQUxBO0FBSDJELEtBQXRDO0FBQUEsR0FBekI7O0FBTUEsc0JBQ0kscUJBQUMsU0FBRDtBQUNJLElBQUEsY0FBYyxFQUFFd0IsV0FBVyxDQUFDL0IsY0FEaEM7QUFFSSxJQUFBLEtBQUssRUFBRTZCLFVBRlg7QUFBQSwyQkFJSSxzQkFBQyxZQUFEO0FBQUEsOEJBQ0kscUJBQUMsYUFBRDtBQUNJLFFBQUEsZUFBZSxFQUFFRSxXQUFXLENBQUM1QixlQURqQztBQUVJLFFBQUEsY0FBYyxFQUFFNEIsV0FBVyxDQUFDL0IsY0FGaEM7QUFBQSxrQkFJSzRCLElBQUksQ0FBQ3NCLEdBQUwsQ0FBUyxVQUFDTCxHQUFEO0FBQUEsOEJBQ04scUJBQUMsVUFBRCxrQ0FFUUksZ0JBQWdCLENBQUNsQixXQUFELENBRnhCO0FBQUEsbUNBSUk7QUFBQSx3QkFBT2MsR0FBRyxDQUFDTTtBQUFYO0FBSkosY0FDU04sR0FBRyxDQUFDQyxJQURiLENBRE07QUFBQSxTQUFUO0FBSkwsUUFESixlQWNJLHNCQUFDLFdBQUQ7QUFBYSxRQUFBLGlCQUFpQixFQUFFdEIsZUFBaEM7QUFBQSxnQ0FDSSxxQkFBQyxXQUFEO0FBQWEsVUFBQSxnQkFBZ0IsRUFBRVMsU0FBUyxDQUFDdEIsZ0JBQXpDO0FBQUEsb0JBQ0s2QixJQUFJLENBQUNVLEdBQUwsQ0FBUyxVQUFDRSxJQUFELEVBQU9yQyxLQUFQO0FBQUEsZ0NBQ04scUJBQUMsV0FBRDtBQUNJLGNBQUEsZ0JBQWdCLEVBQUVrQixTQUFTLENBQUNuQixnQkFEaEM7QUFHSSxjQUFBLEtBQUssRUFBRUMsS0FIWDtBQUlJLGNBQUEsWUFBWSxFQUFFa0IsU0FBUyxDQUFDcEIsWUFKNUI7QUFBQSx3QkFNS2UsSUFBSSxDQUFDc0IsR0FBTCxDQUFTLFVBQUNMLEdBQUQ7QUFBQSxvQ0FDTixxQkFBQyxRQUFELGtDQUVRSSxnQkFBZ0IsQ0FBQ2hCLFNBQUQsQ0FGeEI7QUFBQSx5Q0FJSTtBQUFBLDhCQUFPbUIsSUFBSSxDQUFDUCxHQUFHLENBQUNDLElBQUw7QUFBWDtBQUpKLDhCQUNZL0IsS0FEWixjQUNxQjhCLEdBQUcsQ0FBQ0MsSUFEekIsRUFETTtBQUFBLGVBQVQ7QUFOTCxlQUVTL0IsS0FGVCxDQURNO0FBQUEsV0FBVDtBQURMLFVBREosRUFxQktzQixTQUFTLGlCQUNOLHNCQUFDLGFBQUQsa0NBQW1CRixXQUFuQjtBQUFBLHFCQUNLLENBQUNhLFVBQVUsTUFBTU4sZ0JBQWdCLEVBQWpDLEtBQXdDTCxTQUF4QyxpQkFDRyxzQkFBQyxhQUFEO0FBQWUsWUFBQSxTQUFTLEVBQUMsWUFBekI7QUFBQSxvQ0FDSSxxQkFBQyxVQUFELGtDQUNRWSxnQkFBZ0IsQ0FBQ2QsV0FBRCxDQUR4QjtBQUFBLHdCQUdLUixDQUFDLENBQUMsVUFBRDtBQUhOLGVBREosRUFNS0MsSUFBSSxDQUFDZSxLQUFMLENBQVcsQ0FBWCxFQUFjTyxHQUFkLENBQWtCLFVBQUNMLEdBQUQ7QUFBQSxrQ0FDZixxQkFBQyxVQUFELGtDQUVRSSxnQkFBZ0IsQ0FBQ2QsV0FBRCxDQUZ4QjtBQUFBLDBCQUlLLGtCQUNHTSxPQURILHFCQUVlSSxHQUFHLENBQUNDLElBRm5CLEdBR0csRUFISDtBQUpMLHFDQUNxQkQsR0FBRyxDQUFDQyxJQUR6QixFQURlO0FBQUEsYUFBbEIsQ0FOTDtBQUFBLFlBRlIsRUF1QktFLFVBQVUsbUJBQ1Asc0JBQUMsYUFBRDtBQUFlLFlBQUEsU0FBUyxFQUFDLFlBQXpCO0FBQUEsb0NBQ0kscUJBQUMsVUFBRCxrQ0FDUUMsZ0JBQWdCLENBQUNkLFdBQUQsQ0FEeEI7QUFBQSx3QkFHS1IsQ0FBQyxDQUFDLE9BQUQ7QUFITixlQURKLEVBTUtDLElBQUksQ0FBQ2UsS0FBTCxDQUFXLENBQVgsRUFBY08sR0FBZCxDQUFrQixVQUFDTCxHQUFEO0FBQUEsa0NBQ2YscUJBQUMsVUFBRCxrQ0FFUUksZ0JBQWdCLENBQUNkLFdBQUQsQ0FGeEI7QUFBQSwwQkFJSyxrQkFDR00sT0FESCxnQkFFVUksR0FBRyxDQUFDQyxJQUZkLEdBR0csRUFISDtBQUpMLGdDQUNnQkQsR0FBRyxDQUFDQyxJQURwQixFQURlO0FBQUEsYUFBbEIsQ0FOTDtBQUFBLFlBeEJSLEVBNkNLSixnQkFBZ0IsbUJBQ2Isc0JBQUMsYUFBRDtBQUFlLFlBQUEsU0FBUyxFQUFDLFlBQXpCO0FBQUEsb0NBQ0kscUJBQUMsVUFBRCxrQ0FDUU8sZ0JBQWdCLENBQUNkLFdBQUQsQ0FEeEI7QUFBQSx3QkFHS1IsQ0FBQyxDQUFDLFVBQUQ7QUFITixlQURKLEVBTUtDLElBQUksQ0FBQ2UsS0FBTCxDQUFXLENBQVgsRUFBY08sR0FBZCxDQUFrQixVQUFDTCxHQUFEO0FBQUEsa0NBQ2YscUJBQUMsVUFBRCxrQ0FFUUksZ0JBQWdCLENBQUNkLFdBQUQsQ0FGeEI7QUFBQSwwQkFJSyxrQkFDR00sT0FESCxxQkFFZUksR0FBRyxDQUFDQyxJQUZuQixHQUdHLEVBSEg7QUFKTCxxQ0FDcUJELEdBQUcsQ0FBQ0MsSUFEekIsRUFEZTtBQUFBLGFBQWxCLENBTkw7QUFBQSxZQTlDUixlQW1FSSxzQkFBQyxhQUFEO0FBQWUsWUFBQSxTQUFTLEVBQUMsWUFBekI7QUFBQSxvQ0FDSSxxQkFBQyxVQUFELGtDQUFnQkcsZ0JBQWdCLENBQUNkLFdBQUQsQ0FBaEM7QUFBQSx3QkFDS1IsQ0FBQyxDQUFDLE9BQUQ7QUFETixlQURKLEVBSUtDLElBQUksQ0FBQ2UsS0FBTCxDQUFXLENBQVgsRUFBY08sR0FBZCxDQUFrQixVQUFDTCxHQUFEO0FBQUEsa0NBQ2YscUJBQUMsVUFBRCxrQ0FFUUksZ0JBQWdCLENBQUNkLFdBQUQsQ0FGeEI7QUFBQSwwQkFJSyxrQkFBSU0sT0FBSixrQkFBc0JJLEdBQUcsQ0FBQ0MsSUFBMUIsR0FBa0MsRUFBbEM7QUFKTCxrQ0FDa0JELEdBQUcsQ0FBQ0MsSUFEdEIsRUFEZTtBQUFBLGFBQWxCLENBSkw7QUFBQSxZQW5FSjtBQUFBLFdBdEJSO0FBQUEsUUFkSjtBQUFBO0FBSkosSUFESjtBQStISCxDQWhMRDs7QUFrTEF2QixrQkFBa0IsQ0FBQzhCLFNBQW5CLEdBQStCO0FBQzNCN0IsRUFBQUEsZUFBZSxFQUFFOEIsbUJBQVVDLElBREE7QUFFM0I5QixFQUFBQSxLQUFLLEVBQUU2QixtQkFBVUUsTUFGVTtBQUczQjlCLEVBQUFBLEtBQUssRUFBRTRCLG1CQUFVRTtBQUhVLENBQS9CO2VBTWVqQyxrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnZXQgZnJvbSAnbG9kYXNoL2dldCc7XG5pbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgJHsoeyByb3VuZGVkQm9yZGVycyB9KSA9PiByb3VuZGVkQm9yZGVycyAmJiAnYm9yZGVyLXJhZGl1czogMTdweCAxN3B4IDAgMDsnfVxuICAgIGJvcmRlci10b3Atd2lkdGg6IDBweCAhaW1wb3J0YW50O1xuYDtcblxuY29uc3QgVGFibGVDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuYDtcblxuY29uc3QgSGVhZGVyQ29udGVudCA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkeyh7IGJhY2tncm91bmRDb2xvciA9ICcjMDAwJyB9KSA9PiBiYWNrZ3JvdW5kQ29sb3J9O1xuICAgICR7KHsgcm91bmRlZEJvcmRlcnMgfSkgPT4gcm91bmRlZEJvcmRlcnMgJiYgJ2JvcmRlci1yYWRpdXM6IDE3cHg7J31cbmA7XG5cbmNvbnN0IEhlYWRlckRhdGEgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleDogMTtcbiAgICBmb250LXdlaWdodDogJHsoeyBmb250Qm9sZCB9KSA9PiAoZm9udEJvbGQgPyAnYm9sZCcgOiAnMTAwJyl9O1xuICAgICR7KHsgZm9udEl0YWxpYyB9KSA9PiBmb250SXRhbGljICYmICdmb250LXN0eWxlOiBpdGFsaWM7J31cbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgY29sb3I6ICR7KHsgY29sb3IgPSAnI2ZmZicgfSkgPT4gY29sb3J9O1xuICAgIHBhZGRpbmc6IDVweCAxNXB4O1xuYDtcblxuY29uc3QgRGF0YUNvbnRlbnQgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBmbGV4OiAxO1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBvdmVyZmxvdy15OiAkeyh7IG92ZXJmbG93WURpc2FibGVkIH0pID0+XG4gICAgICAgIG92ZXJmbG93WURpc2FibGVkID8gJ2hpZGRlbicgOiAnYXV0byd9O1xuYDtcbmNvbnN0IEJvZHlDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBiYWNrZ3JvdW5kQ29sb3IxID0gJyNmZmYnIH0pID0+IGJhY2tncm91bmRDb2xvcjF9O1xuYDtcblxuY29uc3QgQm9keURhdGFSb3cgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgJHsoeyBpc1plYnJhU3R5bGUsIGJhY2tncm91bmRDb2xvcjIsIGluZGV4IH0pID0+XG4gICAgICAgIGlzWmVicmFTdHlsZSAmJlxuICAgICAgICAoaW5kZXggKyAxKSAlIDIgPT09IDAgJiZcbiAgICAgICAgYGJhY2tncm91bmQtY29sb3I6ICR7YmFja2dyb3VuZENvbG9yMn07YH1cbmA7XG5cbmNvbnN0IEJvZHlEYXRhID0gc3R5bGVkLmRpdmBcbiAgICBmb250LXdlaWdodDogJHsoeyBmb250Qm9sZCB9KSA9PiAoZm9udEJvbGQgPyAnYm9sZCcgOiAnMTAwJyl9O1xuICAgICR7KHsgZm9udEl0YWxpYyB9KSA9PiBmb250SXRhbGljICYmICdmb250LXN0eWxlOiBpdGFsaWM7J31cbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgY29sb3I6ICR7KHsgY29sb3IgPSAnIzAwMCcgfSkgPT4gY29sb3J9O1xuICAgIGZsZXg6IDE7XG4gICAgcGFkZGluZzogNXB4IDE1cHg7XG5gO1xuXG5jb25zdCBGb290ZXJDb250ZW50ID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBiYWNrZ3JvdW5kQ29sb3IxID0gJyNmZmYnIH0pID0+IGJhY2tncm91bmRDb2xvcjF9O1xuICAgIGJvcmRlci10b3Atc3R5bGU6ICR7KHsgYm9yZGVyVG9wU3R5bGUgfSkgPT4gYm9yZGVyVG9wU3R5bGV9O1xuICAgIGJvcmRlci10b3Atd2lkdGg6ICR7KHsgYm9yZGVyVG9wV2lkdGggfSkgPT4gYm9yZGVyVG9wV2lkdGh9O1xuICAgIGJvcmRlci10b3AtY29sb3I6ICR7KHsgYm9yZGVyVG9wQ29sb3IgfSkgPT4gYm9yZGVyVG9wQ29sb3J9O1xuICAgICR7KHsgaXNaZWJyYVN0eWxlLCBiYWNrZ3JvdW5kQ29sb3IyIH0pID0+XG4gICAgICAgIGlzWmVicmFTdHlsZSAmJlxuICAgICAgICBgLmZvb3Rlci1yb3c6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICR7YmFja2dyb3VuZENvbG9yMn07XG4gICAgICAgIH1gfVxuYDtcblxuY29uc3QgRm9vdGVyRGF0YVJvdyA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbmA7XG5cbmNvbnN0IEZvb3RlckRhdGEgPSBzdHlsZWQuZGl2YFxuICAgIGZvbnQtd2VpZ2h0OiAkeyh7IGZvbnRCb2xkIH0pID0+IChmb250Qm9sZCA/ICdib2xkJyA6ICcxMDAnKX07XG4gICAgJHsoeyBmb250SXRhbGljIH0pID0+IGZvbnRJdGFsaWMgJiYgJ2ZvbnQtc3R5bGU6IGl0YWxpYzsnfVxuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogJHsoeyBjb2xvciA9ICcjMDAwJyB9KSA9PiBjb2xvcn07XG4gICAgZmxleDogMTtcbiAgICBwYWRkaW5nOiA1cHggMTVweDtcbmA7XG5cbmNvbnN0IFN1cHBvcnRUYWJsZVdpZGdldCA9ICh7IGVkaXRpb25EaXNhYmxlZCwgc3R5bGUgPSB7fSwgdmFsdWUgPSB7fSB9KSA9PiB7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuICAgIGNvbnN0IHtcbiAgICAgICAgY29scyA9IFtdLFxuICAgICAgICB0YWJsZTogdGFibGVTdHlsZSxcbiAgICAgICAgaGVhZGVyOiBoZWFkZXJTdHlsZSxcbiAgICAgICAgYm9keTogYm9keVN0eWxlLFxuICAgICAgICBmb290ZXI6IGZvb3RlclN0eWxlLFxuICAgICAgICBzaG93VG90YWwsXG4gICAgICAgIHNob3dJVkEsXG4gICAgICAgIHNob3dEaXNjb3VudCxcbiAgICB9ID0gc3R5bGU7XG4gICAgY29uc3QgeyBkYXRhID0gW10sIHJlc3VsdHMgPSB7fSB9ID0gdmFsdWU7XG5cbiAgICBjb25zdCBzaG93RGlzY291bnRzUm93ID0gKCkgPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICAhc2hvd1RvdGFsIHx8XG4gICAgICAgICAgICAhc2hvd0Rpc2NvdW50IHx8XG4gICAgICAgICAgICBjb2xzXG4gICAgICAgICAgICAgICAgLnNsaWNlKDEpXG4gICAgICAgICAgICAgICAgLmZpbHRlcihcbiAgICAgICAgICAgICAgICAgICAgKGNvbCkgPT4gIWlzTmlsKGdldChyZXN1bHRzLCBgZGlzY291bnQuJHtjb2wubmFtZX1gLCBudWxsKSlcbiAgICAgICAgICAgICAgICApLmxlbmd0aCA8PSAwXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9O1xuXG4gICAgY29uc3Qgc2hvd0lWQVJvdyA9ICgpID0+IHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIXNob3dUb3RhbCB8fFxuICAgICAgICAgICAgIXNob3dJVkEgfHxcbiAgICAgICAgICAgIGNvbHNcbiAgICAgICAgICAgICAgICAuc2xpY2UoMSlcbiAgICAgICAgICAgICAgICAuZmlsdGVyKChjb2wpID0+ICFpc05pbChnZXQocmVzdWx0cywgYGl2YS4ke2NvbC5uYW1lfWAsIG51bGwpKSlcbiAgICAgICAgICAgICAgICAubGVuZ3RoIDw9IDBcbiAgICAgICAgKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcblxuICAgIGNvbnN0IGdldERhdGFUZXh0UHJvcHMgPSAoeyBmb250Qm9sZCwgZm9udEl0YWxpYywgY29sb3IgfSkgPT4gKHtcbiAgICAgICAgZm9udEJvbGQsXG4gICAgICAgIGZvbnRJdGFsaWMsXG4gICAgICAgIGNvbG9yLFxuICAgIH0pO1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lclxuICAgICAgICAgICAgcm91bmRlZEJvcmRlcnM9e2hlYWRlclN0eWxlLnJvdW5kZWRCb3JkZXJzfVxuICAgICAgICAgICAgc3R5bGU9e3RhYmxlU3R5bGV9XG4gICAgICAgID5cbiAgICAgICAgICAgIDxUYWJsZUNvbnRlbnQ+XG4gICAgICAgICAgICAgICAgPEhlYWRlckNvbnRlbnRcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yPXtoZWFkZXJTdHlsZS5iYWNrZ3JvdW5kQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgIHJvdW5kZWRCb3JkZXJzPXtoZWFkZXJTdHlsZS5yb3VuZGVkQm9yZGVyc31cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIHtjb2xzLm1hcCgoY29sKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8SGVhZGVyRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17Y29sLm5hbWV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLmdldERhdGFUZXh0UHJvcHMoaGVhZGVyU3R5bGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPntjb2wubGFiZWx9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9IZWFkZXJEYXRhPlxuICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICA8L0hlYWRlckNvbnRlbnQ+XG4gICAgICAgICAgICAgICAgPERhdGFDb250ZW50IG92ZXJmbG93WURpc2FibGVkPXtlZGl0aW9uRGlzYWJsZWR9PlxuICAgICAgICAgICAgICAgICAgICA8Qm9keUNvbnRlbnQgYmFja2dyb3VuZENvbG9yMT17Ym9keVN0eWxlLmJhY2tncm91bmRDb2xvcjF9PlxuICAgICAgICAgICAgICAgICAgICAgICAge2RhdGEubWFwKChpdGVtLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCb2R5RGF0YVJvd1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3IyPXtib2R5U3R5bGUuYmFja2dyb3VuZENvbG9yMn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtpbmRleH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg9e2luZGV4fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1plYnJhU3R5bGU9e2JvZHlTdHlsZS5pc1plYnJhU3R5bGV9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Y29scy5tYXAoKGNvbCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJvZHlEYXRhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgJHtpbmRleH0tJHtjb2wubmFtZX1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXREYXRhVGV4dFByb3BzKGJvZHlTdHlsZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4+e2l0ZW1bY29sLm5hbWVdfTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQm9keURhdGE+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvQm9keURhdGFSb3c+XG4gICAgICAgICAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgICAgICAgPC9Cb2R5Q29udGVudD5cblxuICAgICAgICAgICAgICAgICAgICB7c2hvd1RvdGFsICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxGb290ZXJDb250ZW50IHsuLi5mb290ZXJTdHlsZX0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeyhzaG93SVZBUm93KCkgfHwgc2hvd0Rpc2NvdW50c1JvdygpKSAmJiBzaG93VG90YWwgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YVJvdyBjbGFzc05hbWU9XCJmb290ZXItcm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXREYXRhVGV4dFByb3BzKGZvb3RlclN0eWxlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnU3VidG90YWwnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtjb2xzLnNsaWNlKDEpLm1hcCgoY29sKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvb3RlckRhdGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgc3VidG90YWwtJHtjb2wubmFtZX1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Li4uZ2V0RGF0YVRleHRQcm9wcyhmb290ZXJTdHlsZSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0cyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGBzdWJ0b3RhbC4ke2NvbC5uYW1lfWAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0Zvb3RlckRhdGFSb3c+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93SVZBUm93KCkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YVJvdyBjbGFzc05hbWU9XCJmb290ZXItcm93XCI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXREYXRhVGV4dFByb3BzKGZvb3RlclN0eWxlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dCgnVGF4ZXMnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtjb2xzLnNsaWNlKDEpLm1hcCgoY29sKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvb3RlckRhdGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgaXZhLSR7Y29sLm5hbWV9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLmdldERhdGFUZXh0UHJvcHMoZm9vdGVyU3R5bGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2dldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBgaXZhLiR7Y29sLm5hbWV9YCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YVJvdz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge3Nob3dEaXNjb3VudHNSb3coKSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb290ZXJEYXRhUm93IGNsYXNzTmFtZT1cImZvb3Rlci1yb3dcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb290ZXJEYXRhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLmdldERhdGFUZXh0UHJvcHMoZm9vdGVyU3R5bGUpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt0KCdEaXNjb3VudCcpfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2NvbHMuc2xpY2UoMSkubWFwKChjb2wpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk9e2BkaXNjb3VudC0ke2NvbC5uYW1lfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXREYXRhVGV4dFByb3BzKGZvb3RlclN0eWxlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtnZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHRzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYGRpc2NvdW50LiR7Y29sLm5hbWV9YCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcnXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YVJvdz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEZvb3RlckRhdGFSb3cgY2xhc3NOYW1lPVwiZm9vdGVyLXJvd1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Rm9vdGVyRGF0YSB7Li4uZ2V0RGF0YVRleHRQcm9wcyhmb290ZXJTdHlsZSl9PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3QoJ1RvdGFsJyl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2NvbHMuc2xpY2UoMSkubWFwKChjb2wpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxGb290ZXJEYXRhXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgdG90YWwtJHtjb2wubmFtZX1gfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5nZXREYXRhVGV4dFByb3BzKGZvb3RlclN0eWxlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Z2V0KHJlc3VsdHMsIGB0b3RhbC4ke2NvbC5uYW1lfWAsICcnKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRm9vdGVyRGF0YT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJEYXRhUm93PlxuICAgICAgICAgICAgICAgICAgICAgICAgPC9Gb290ZXJDb250ZW50PlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIDwvRGF0YUNvbnRlbnQ+XG4gICAgICAgICAgICA8L1RhYmxlQ29udGVudD5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cblN1cHBvcnRUYWJsZVdpZGdldC5wcm9wVHlwZXMgPSB7XG4gICAgZWRpdGlvbkRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdCxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFN1cHBvcnRUYWJsZVdpZGdldDtcbiJdfQ==