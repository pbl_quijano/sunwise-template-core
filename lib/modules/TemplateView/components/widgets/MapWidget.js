"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactI18next = require("react-i18next");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _types = require("../../../../constants/types");

var _defaultmap = _interopRequireDefault(require("../../../../res/defaultmap.png"));

var _MapView = _interopRequireDefault(require("./MapView"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    overflow-y: auto;\n    position: relative;\n    width: 100%;\n"])));

var Image = _styledComponents.default.img(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 100%;\n    left: 0;\n    object-fit: cover;\n    object-position: center;\n    position: absolute;\n    top: 0;\n    width: 100%;\n"])));

var StyledMap = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    background-color: #c5c5c5;\n    bottom: 0;\n    height: ", ";\n    left: 0;\n    position: relative;\n    right: 0;\n    top: 0;\n"])), function (_ref) {
  var heightMap = _ref.heightMap;
  return heightMap ? "".concat(heightMap, "px") : '100%';
});

var MapWidget = function MapWidget() {
  var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      editionLevel = _ref2.editionLevel,
      handleChangeValue = _ref2.handleChangeValue,
      innerHeight = _ref2.innerHeight,
      innerWidth = _ref2.innerWidth,
      _ref2$value = _ref2.value,
      value = _ref2$value === void 0 ? {} : _ref2$value;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  var projectPosition = value.projectPosition,
      currentPosition = value.currentPosition,
      _value$mapType = value.mapType,
      mapType = _value$mapType === void 0 ? 'roadmap' : _value$mapType;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      markers = _useState2[0],
      setMarkers = _useState2[1];

  var _useState3 = (0, _react.useState)(currentPosition.zoom),
      _useState4 = _slicedToArray(_useState3, 2),
      tempCurrentZoom = _useState4[0],
      setTempCurrentZoom = _useState4[1];

  var _useState5 = (0, _react.useState)(mapType),
      _useState6 = _slicedToArray(_useState5, 2),
      tempCurrentMapType = _useState6[0],
      setTempCurrentMapType = _useState6[1];

  var _useState7 = (0, _react.useState)({
    lat: currentPosition.lat,
    lng: currentPosition.lng
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      tempCurrentCenter = _useState8[0],
      setTempCurrentCenter = _useState8[1];

  (0, _react.useEffect)(function () {
    if (currentPosition.zoom !== tempCurrentZoom) {
      handleChangeValue(_objectSpread(_objectSpread({}, value), {}, {
        currentPosition: _objectSpread(_objectSpread({}, currentPosition), {}, {
          zoom: tempCurrentZoom
        })
      }));
    }
  }, [tempCurrentZoom]);
  (0, _react.useEffect)(function () {
    if (currentPosition.lat !== tempCurrentCenter.lat || currentPosition.lng !== tempCurrentCenter.lng) {
      handleChangeValue(_objectSpread(_objectSpread({}, value), {}, {
        currentPosition: _objectSpread(_objectSpread({}, currentPosition), {}, {
          lat: tempCurrentCenter.lat,
          lng: tempCurrentCenter.lng
        })
      }));
    }
  }, [tempCurrentCenter]);
  (0, _react.useEffect)(function () {
    if (tempCurrentMapType !== mapType) {
      handleChangeValue(_objectSpread(_objectSpread({}, value), {}, {
        mapType: tempCurrentMapType
      }));
    }
  }, [tempCurrentMapType]);
  (0, _react.useEffect)(function () {
    if (!(0, _isEmpty.default)(projectPosition)) {
      setMarkers([].concat(_toConsumableArray(markers), [{
        title: t('Project'),
        lat: projectPosition.lat,
        lng: projectPosition.lng
      }]));
    }
  }, []);

  var getMarkerPosition = function getMarkerPosition() {
    return markers.reduce(function (acc, current) {
      return acc + "&markers=color:red%7Clabel:Proyecto%7C".concat(current.lat, ",").concat(current.lng);
    }, '');
  };

  var _handleZoomChanged = function _handleZoomChanged(newZoom) {
    return setTempCurrentZoom(newZoom);
  };

  var _handleCenterChanged = function _handleCenterChanged(newPosition) {
    return setTempCurrentCenter({
      lat: newPosition.lat(),
      lng: newPosition.lng()
    });
  };

  var _handleTypeChanged = function _handleTypeChanged(newType) {
    return setTempCurrentMapType(newType);
  };

  if (editionLevel === _types.FULL_EDITION_MODE) return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Image, {
      src: _defaultmap.default
    })
  });

  if (editionLevel === _types.NO_EDITION_MODE) {
    return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Image, {
        src: "https://maps.googleapis.com/maps/api/staticmap?center=".concat(tempCurrentCenter.lat, ",").concat(tempCurrentCenter.lng, "&zoom=").concat(tempCurrentZoom, "&size=").concat(Math.ceil(innerWidth), "x").concat(Math.ceil(innerHeight), "&maptype=").concat(tempCurrentMapType).concat(getMarkerPosition(), "&key=").concat(process.env.REACT_APP_GOOGLE_MAPS_KEY)
      })
    });
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledMap, {
      heightMap: innerHeight,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_MapView.default, {
        handleCenterChanged: _handleCenterChanged,
        handleTypeChanged: _handleTypeChanged,
        handleZoomChanged: _handleZoomChanged,
        lat: "".concat(tempCurrentCenter.lat),
        lng: "".concat(tempCurrentCenter.lng),
        mapType: tempCurrentMapType,
        markers: markers,
        zoom: tempCurrentZoom
      })
    })
  });
};

MapWidget.propTypes = {
  editionLevel: _propTypes.default.string,
  handleChangeValue: _propTypes.default.func,
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  value: _propTypes.default.object
};
var _default = MapWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvTWFwV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsIkltYWdlIiwiaW1nIiwiU3R5bGVkTWFwIiwiaGVpZ2h0TWFwIiwiTWFwV2lkZ2V0IiwiZWRpdGlvbkxldmVsIiwiaGFuZGxlQ2hhbmdlVmFsdWUiLCJpbm5lckhlaWdodCIsImlubmVyV2lkdGgiLCJ2YWx1ZSIsInQiLCJwcm9qZWN0UG9zaXRpb24iLCJjdXJyZW50UG9zaXRpb24iLCJtYXBUeXBlIiwibWFya2VycyIsInNldE1hcmtlcnMiLCJ6b29tIiwidGVtcEN1cnJlbnRab29tIiwic2V0VGVtcEN1cnJlbnRab29tIiwidGVtcEN1cnJlbnRNYXBUeXBlIiwic2V0VGVtcEN1cnJlbnRNYXBUeXBlIiwibGF0IiwibG5nIiwidGVtcEN1cnJlbnRDZW50ZXIiLCJzZXRUZW1wQ3VycmVudENlbnRlciIsInRpdGxlIiwiZ2V0TWFya2VyUG9zaXRpb24iLCJyZWR1Y2UiLCJhY2MiLCJjdXJyZW50IiwiX2hhbmRsZVpvb21DaGFuZ2VkIiwibmV3Wm9vbSIsIl9oYW5kbGVDZW50ZXJDaGFuZ2VkIiwibmV3UG9zaXRpb24iLCJfaGFuZGxlVHlwZUNoYW5nZWQiLCJuZXdUeXBlIiwiRlVMTF9FRElUSU9OX01PREUiLCJkZWZhdWx0TWFwIiwiTk9fRURJVElPTl9NT0RFIiwiTWF0aCIsImNlaWwiLCJwcm9jZXNzIiwiZW52IiwiUkVBQ1RfQVBQX0dPT0dMRV9NQVBTX0tFWSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsImZ1bmMiLCJudW1iZXIiLCJvYmplY3QiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBViw2TUFBZjs7QUFTQSxJQUFNQyxLQUFLLEdBQUdGLDBCQUFPRyxHQUFWLHdOQUFYOztBQVVBLElBQU1DLFNBQVMsR0FBR0osMEJBQU9DLEdBQVYsZ05BR0Q7QUFBQSxNQUFHSSxTQUFILFFBQUdBLFNBQUg7QUFBQSxTQUFvQkEsU0FBUyxhQUFNQSxTQUFOLFVBQXNCLE1BQW5EO0FBQUEsQ0FIQyxDQUFmOztBQVVBLElBQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFZLEdBTVA7QUFBQSxrRkFBUCxFQUFPO0FBQUEsTUFMUEMsWUFLTyxTQUxQQSxZQUtPO0FBQUEsTUFKUEMsaUJBSU8sU0FKUEEsaUJBSU87QUFBQSxNQUhQQyxXQUdPLFNBSFBBLFdBR087QUFBQSxNQUZQQyxVQUVPLFNBRlBBLFVBRU87QUFBQSwwQkFEUEMsS0FDTztBQUFBLE1BRFBBLEtBQ08sNEJBREMsRUFDRDs7QUFDUCx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBQ0EsTUFBUUMsZUFBUixHQUFrRUYsS0FBbEUsQ0FBUUUsZUFBUjtBQUFBLE1BQXlCQyxlQUF6QixHQUFrRUgsS0FBbEUsQ0FBeUJHLGVBQXpCO0FBQUEsdUJBQWtFSCxLQUFsRSxDQUEwQ0ksT0FBMUM7QUFBQSxNQUEwQ0EsT0FBMUMsK0JBQW9ELFNBQXBEOztBQUNBLGtCQUE4QixxQkFBUyxFQUFULENBQTlCO0FBQUE7QUFBQSxNQUFPQyxPQUFQO0FBQUEsTUFBZ0JDLFVBQWhCOztBQUNBLG1CQUE4QyxxQkFDMUNILGVBQWUsQ0FBQ0ksSUFEMEIsQ0FBOUM7QUFBQTtBQUFBLE1BQU9DLGVBQVA7QUFBQSxNQUF3QkMsa0JBQXhCOztBQUdBLG1CQUFvRCxxQkFBU0wsT0FBVCxDQUFwRDtBQUFBO0FBQUEsTUFBT00sa0JBQVA7QUFBQSxNQUEyQkMscUJBQTNCOztBQUNBLG1CQUFrRCxxQkFBUztBQUN2REMsSUFBQUEsR0FBRyxFQUFFVCxlQUFlLENBQUNTLEdBRGtDO0FBRXZEQyxJQUFBQSxHQUFHLEVBQUVWLGVBQWUsQ0FBQ1U7QUFGa0MsR0FBVCxDQUFsRDtBQUFBO0FBQUEsTUFBT0MsaUJBQVA7QUFBQSxNQUEwQkMsb0JBQTFCOztBQUtBLHdCQUFVLFlBQU07QUFDWixRQUFJWixlQUFlLENBQUNJLElBQWhCLEtBQXlCQyxlQUE3QixFQUE4QztBQUMxQ1gsTUFBQUEsaUJBQWlCLGlDQUNWRyxLQURVO0FBRWJHLFFBQUFBLGVBQWUsa0NBQ1JBLGVBRFE7QUFFWEksVUFBQUEsSUFBSSxFQUFFQztBQUZLO0FBRkYsU0FBakI7QUFPSDtBQUNKLEdBVkQsRUFVRyxDQUFDQSxlQUFELENBVkg7QUFZQSx3QkFBVSxZQUFNO0FBQ1osUUFDSUwsZUFBZSxDQUFDUyxHQUFoQixLQUF3QkUsaUJBQWlCLENBQUNGLEdBQTFDLElBQ0FULGVBQWUsQ0FBQ1UsR0FBaEIsS0FBd0JDLGlCQUFpQixDQUFDRCxHQUY5QyxFQUdFO0FBQ0VoQixNQUFBQSxpQkFBaUIsaUNBQ1ZHLEtBRFU7QUFFYkcsUUFBQUEsZUFBZSxrQ0FDUkEsZUFEUTtBQUVYUyxVQUFBQSxHQUFHLEVBQUVFLGlCQUFpQixDQUFDRixHQUZaO0FBR1hDLFVBQUFBLEdBQUcsRUFBRUMsaUJBQWlCLENBQUNEO0FBSFo7QUFGRixTQUFqQjtBQVFIO0FBQ0osR0FkRCxFQWNHLENBQUNDLGlCQUFELENBZEg7QUFnQkEsd0JBQVUsWUFBTTtBQUNaLFFBQUlKLGtCQUFrQixLQUFLTixPQUEzQixFQUFvQztBQUNoQ1AsTUFBQUEsaUJBQWlCLGlDQUNWRyxLQURVO0FBRWJJLFFBQUFBLE9BQU8sRUFBRU07QUFGSSxTQUFqQjtBQUlIO0FBQ0osR0FQRCxFQU9HLENBQUNBLGtCQUFELENBUEg7QUFTQSx3QkFBVSxZQUFNO0FBQ1osUUFBSSxDQUFDLHNCQUFRUixlQUFSLENBQUwsRUFBK0I7QUFDM0JJLE1BQUFBLFVBQVUsOEJBQ0hELE9BREcsSUFFTjtBQUNJVyxRQUFBQSxLQUFLLEVBQUVmLENBQUMsQ0FBQyxTQUFELENBRFo7QUFFSVcsUUFBQUEsR0FBRyxFQUFFVixlQUFlLENBQUNVLEdBRnpCO0FBR0lDLFFBQUFBLEdBQUcsRUFBRVgsZUFBZSxDQUFDVztBQUh6QixPQUZNLEdBQVY7QUFRSDtBQUNKLEdBWEQsRUFXRyxFQVhIOztBQWFBLE1BQU1JLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0I7QUFBQSxXQUN0QlosT0FBTyxDQUFDYSxNQUFSLENBQWUsVUFBQ0MsR0FBRCxFQUFNQyxPQUFOLEVBQWtCO0FBQzdCLGFBQ0lELEdBQUcsbURBQ3NDQyxPQUFPLENBQUNSLEdBRDlDLGNBQ3FEUSxPQUFPLENBQUNQLEdBRDdELENBRFA7QUFJSCxLQUxELEVBS0csRUFMSCxDQURzQjtBQUFBLEdBQTFCOztBQVFBLE1BQU1RLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsT0FBRDtBQUFBLFdBQWFiLGtCQUFrQixDQUFDYSxPQUFELENBQS9CO0FBQUEsR0FBM0I7O0FBRUEsTUFBTUMsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFDQyxXQUFEO0FBQUEsV0FDekJULG9CQUFvQixDQUFDO0FBQ2pCSCxNQUFBQSxHQUFHLEVBQUVZLFdBQVcsQ0FBQ1osR0FBWixFQURZO0FBRWpCQyxNQUFBQSxHQUFHLEVBQUVXLFdBQVcsQ0FBQ1gsR0FBWjtBQUZZLEtBQUQsQ0FESztBQUFBLEdBQTdCOztBQU1BLE1BQU1ZLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsT0FBRDtBQUFBLFdBQWFmLHFCQUFxQixDQUFDZSxPQUFELENBQWxDO0FBQUEsR0FBM0I7O0FBRUEsTUFBSTlCLFlBQVksS0FBSytCLHdCQUFyQixFQUNJLG9CQUNJLHFCQUFDLFNBQUQ7QUFBQSwyQkFDSSxxQkFBQyxLQUFEO0FBQU8sTUFBQSxHQUFHLEVBQUVDO0FBQVo7QUFESixJQURKOztBQU1KLE1BQUloQyxZQUFZLEtBQUtpQyxzQkFBckIsRUFBc0M7QUFDbEMsd0JBQ0kscUJBQUMsU0FBRDtBQUFBLDZCQUNJLHFCQUFDLEtBQUQ7QUFDSSxRQUFBLEdBQUcsa0VBQ0NmLGlCQUFpQixDQUFDRixHQURuQixjQUdDRSxpQkFBaUIsQ0FBQ0QsR0FIbkIsbUJBSU1MLGVBSk4sbUJBSThCc0IsSUFBSSxDQUFDQyxJQUFMLENBQzdCaEMsVUFENkIsQ0FKOUIsY0FNRStCLElBQUksQ0FBQ0MsSUFBTCxDQUNEakMsV0FEQyxDQU5GLHNCQVFVWSxrQkFSVixTQVErQk8saUJBQWlCLEVBUmhELGtCQVNDZSxPQUFPLENBQUNDLEdBQVIsQ0FBWUMseUJBVGI7QUFEUDtBQURKLE1BREo7QUFpQkg7O0FBQ0Qsc0JBQ0kscUJBQUMsU0FBRDtBQUFBLDJCQUNJLHFCQUFDLFNBQUQ7QUFBVyxNQUFBLFNBQVMsRUFBRXBDLFdBQXRCO0FBQUEsNkJBQ0kscUJBQUMsZ0JBQUQ7QUFDSSxRQUFBLG1CQUFtQixFQUFFeUIsb0JBRHpCO0FBRUksUUFBQSxpQkFBaUIsRUFBRUUsa0JBRnZCO0FBR0ksUUFBQSxpQkFBaUIsRUFBRUosa0JBSHZCO0FBSUksUUFBQSxHQUFHLFlBQUtQLGlCQUFpQixDQUFDRixHQUF2QixDQUpQO0FBS0ksUUFBQSxHQUFHLFlBQUtFLGlCQUFpQixDQUFDRCxHQUF2QixDQUxQO0FBTUksUUFBQSxPQUFPLEVBQUVILGtCQU5iO0FBT0ksUUFBQSxPQUFPLEVBQUVMLE9BUGI7QUFRSSxRQUFBLElBQUksRUFBRUc7QUFSVjtBQURKO0FBREosSUFESjtBQWdCSCxDQWpJRDs7QUFtSUFiLFNBQVMsQ0FBQ3dDLFNBQVYsR0FBc0I7QUFDbEJ2QyxFQUFBQSxZQUFZLEVBQUV3QyxtQkFBVUMsTUFETjtBQUVsQnhDLEVBQUFBLGlCQUFpQixFQUFFdUMsbUJBQVVFLElBRlg7QUFHbEJ4QyxFQUFBQSxXQUFXLEVBQUVzQyxtQkFBVUcsTUFITDtBQUlsQnhDLEVBQUFBLFVBQVUsRUFBRXFDLG1CQUFVRyxNQUpKO0FBS2xCdkMsRUFBQUEsS0FBSyxFQUFFb0MsbUJBQVVJO0FBTEMsQ0FBdEI7ZUFRZTdDLFMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaXNFbXB0eSBmcm9tICdsb2Rhc2gvaXNFbXB0eSc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IHVzZVRyYW5zbGF0aW9uIH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IHsgTk9fRURJVElPTl9NT0RFLCBGVUxMX0VESVRJT05fTU9ERSB9IGZyb20gJ0Bjb25zdGFudHMvdHlwZXMnO1xuXG5pbXBvcnQgZGVmYXVsdE1hcCBmcm9tICdAcmVzL2RlZmF1bHRtYXAucG5nJztcblxuaW1wb3J0IE1hcFZpZXcgZnJvbSAnLi9NYXBWaWV3JztcblxuY29uc3QgQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMDAlO1xuYDtcblxuY29uc3QgSW1hZ2UgPSBzdHlsZWQuaW1nYFxuICAgIGhlaWdodDogMTAwJTtcbiAgICBsZWZ0OiAwO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIG9iamVjdC1wb3NpdGlvbjogY2VudGVyO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBTdHlsZWRNYXAgPSBzdHlsZWQuZGl2YFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjNWM1YzU7XG4gICAgYm90dG9tOiAwO1xuICAgIGhlaWdodDogJHsoeyBoZWlnaHRNYXAgfSkgPT4gKGhlaWdodE1hcCA/IGAke2hlaWdodE1hcH1weGAgOiAnMTAwJScpfTtcbiAgICBsZWZ0OiAwO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDA7XG5gO1xuXG5jb25zdCBNYXBXaWRnZXQgPSAoe1xuICAgIGVkaXRpb25MZXZlbCxcbiAgICBoYW5kbGVDaGFuZ2VWYWx1ZSxcbiAgICBpbm5lckhlaWdodCxcbiAgICBpbm5lcldpZHRoLFxuICAgIHZhbHVlID0ge30sXG59ID0ge30pID0+IHtcbiAgICBjb25zdCB7IHQgfSA9IHVzZVRyYW5zbGF0aW9uKCk7XG4gICAgY29uc3QgeyBwcm9qZWN0UG9zaXRpb24sIGN1cnJlbnRQb3NpdGlvbiwgbWFwVHlwZSA9ICdyb2FkbWFwJyB9ID0gdmFsdWU7XG4gICAgY29uc3QgW21hcmtlcnMsIHNldE1hcmtlcnNdID0gdXNlU3RhdGUoW10pO1xuICAgIGNvbnN0IFt0ZW1wQ3VycmVudFpvb20sIHNldFRlbXBDdXJyZW50Wm9vbV0gPSB1c2VTdGF0ZShcbiAgICAgICAgY3VycmVudFBvc2l0aW9uLnpvb21cbiAgICApO1xuICAgIGNvbnN0IFt0ZW1wQ3VycmVudE1hcFR5cGUsIHNldFRlbXBDdXJyZW50TWFwVHlwZV0gPSB1c2VTdGF0ZShtYXBUeXBlKTtcbiAgICBjb25zdCBbdGVtcEN1cnJlbnRDZW50ZXIsIHNldFRlbXBDdXJyZW50Q2VudGVyXSA9IHVzZVN0YXRlKHtcbiAgICAgICAgbGF0OiBjdXJyZW50UG9zaXRpb24ubGF0LFxuICAgICAgICBsbmc6IGN1cnJlbnRQb3NpdGlvbi5sbmcsXG4gICAgfSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoY3VycmVudFBvc2l0aW9uLnpvb20gIT09IHRlbXBDdXJyZW50Wm9vbSkge1xuICAgICAgICAgICAgaGFuZGxlQ2hhbmdlVmFsdWUoe1xuICAgICAgICAgICAgICAgIC4uLnZhbHVlLFxuICAgICAgICAgICAgICAgIGN1cnJlbnRQb3NpdGlvbjoge1xuICAgICAgICAgICAgICAgICAgICAuLi5jdXJyZW50UG9zaXRpb24sXG4gICAgICAgICAgICAgICAgICAgIHpvb206IHRlbXBDdXJyZW50Wm9vbSxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LCBbdGVtcEN1cnJlbnRab29tXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICBjdXJyZW50UG9zaXRpb24ubGF0ICE9PSB0ZW1wQ3VycmVudENlbnRlci5sYXQgfHxcbiAgICAgICAgICAgIGN1cnJlbnRQb3NpdGlvbi5sbmcgIT09IHRlbXBDdXJyZW50Q2VudGVyLmxuZ1xuICAgICAgICApIHtcbiAgICAgICAgICAgIGhhbmRsZUNoYW5nZVZhbHVlKHtcbiAgICAgICAgICAgICAgICAuLi52YWx1ZSxcbiAgICAgICAgICAgICAgICBjdXJyZW50UG9zaXRpb246IHtcbiAgICAgICAgICAgICAgICAgICAgLi4uY3VycmVudFBvc2l0aW9uLFxuICAgICAgICAgICAgICAgICAgICBsYXQ6IHRlbXBDdXJyZW50Q2VudGVyLmxhdCxcbiAgICAgICAgICAgICAgICAgICAgbG5nOiB0ZW1wQ3VycmVudENlbnRlci5sbmcsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfSwgW3RlbXBDdXJyZW50Q2VudGVyXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAodGVtcEN1cnJlbnRNYXBUeXBlICE9PSBtYXBUeXBlKSB7XG4gICAgICAgICAgICBoYW5kbGVDaGFuZ2VWYWx1ZSh7XG4gICAgICAgICAgICAgICAgLi4udmFsdWUsXG4gICAgICAgICAgICAgICAgbWFwVHlwZTogdGVtcEN1cnJlbnRNYXBUeXBlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9LCBbdGVtcEN1cnJlbnRNYXBUeXBlXSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoIWlzRW1wdHkocHJvamVjdFBvc2l0aW9uKSkge1xuICAgICAgICAgICAgc2V0TWFya2VycyhbXG4gICAgICAgICAgICAgICAgLi4ubWFya2VycyxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIHRpdGxlOiB0KCdQcm9qZWN0JyksXG4gICAgICAgICAgICAgICAgICAgIGxhdDogcHJvamVjdFBvc2l0aW9uLmxhdCxcbiAgICAgICAgICAgICAgICAgICAgbG5nOiBwcm9qZWN0UG9zaXRpb24ubG5nLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdKTtcbiAgICAgICAgfVxuICAgIH0sIFtdKTtcblxuICAgIGNvbnN0IGdldE1hcmtlclBvc2l0aW9uID0gKCkgPT5cbiAgICAgICAgbWFya2Vycy5yZWR1Y2UoKGFjYywgY3VycmVudCkgPT4ge1xuICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICBhY2MgK1xuICAgICAgICAgICAgICAgIGAmbWFya2Vycz1jb2xvcjpyZWQlN0NsYWJlbDpQcm95ZWN0byU3QyR7Y3VycmVudC5sYXR9LCR7Y3VycmVudC5sbmd9YFxuICAgICAgICAgICAgKTtcbiAgICAgICAgfSwgJycpO1xuXG4gICAgY29uc3QgX2hhbmRsZVpvb21DaGFuZ2VkID0gKG5ld1pvb20pID0+IHNldFRlbXBDdXJyZW50Wm9vbShuZXdab29tKTtcblxuICAgIGNvbnN0IF9oYW5kbGVDZW50ZXJDaGFuZ2VkID0gKG5ld1Bvc2l0aW9uKSA9PlxuICAgICAgICBzZXRUZW1wQ3VycmVudENlbnRlcih7XG4gICAgICAgICAgICBsYXQ6IG5ld1Bvc2l0aW9uLmxhdCgpLFxuICAgICAgICAgICAgbG5nOiBuZXdQb3NpdGlvbi5sbmcoKSxcbiAgICAgICAgfSk7XG5cbiAgICBjb25zdCBfaGFuZGxlVHlwZUNoYW5nZWQgPSAobmV3VHlwZSkgPT4gc2V0VGVtcEN1cnJlbnRNYXBUeXBlKG5ld1R5cGUpO1xuXG4gICAgaWYgKGVkaXRpb25MZXZlbCA9PT0gRlVMTF9FRElUSU9OX01PREUpXG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9e2RlZmF1bHRNYXB9IC8+XG4gICAgICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICAgICAgKTtcblxuICAgIGlmIChlZGl0aW9uTGV2ZWwgPT09IE5PX0VESVRJT05fTU9ERSkge1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgICAgICA8SW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgc3JjPXtgaHR0cHM6Ly9tYXBzLmdvb2dsZWFwaXMuY29tL21hcHMvYXBpL3N0YXRpY21hcD9jZW50ZXI9JHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBDdXJyZW50Q2VudGVyLmxhdFxuICAgICAgICAgICAgICAgICAgICB9LCR7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ3VycmVudENlbnRlci5sbmdcbiAgICAgICAgICAgICAgICAgICAgfSZ6b29tPSR7dGVtcEN1cnJlbnRab29tfSZzaXplPSR7TWF0aC5jZWlsKFxuICAgICAgICAgICAgICAgICAgICAgICAgaW5uZXJXaWR0aFxuICAgICAgICAgICAgICAgICAgICApfXgke01hdGguY2VpbChcbiAgICAgICAgICAgICAgICAgICAgICAgIGlubmVySGVpZ2h0XG4gICAgICAgICAgICAgICAgICAgICl9Jm1hcHR5cGU9JHt0ZW1wQ3VycmVudE1hcFR5cGV9JHtnZXRNYXJrZXJQb3NpdGlvbigpfSZrZXk9JHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb2Nlc3MuZW52LlJFQUNUX0FQUF9HT09HTEVfTUFQU19LRVlcbiAgICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvQ29udGFpbmVyPlxuICAgICAgICApO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyPlxuICAgICAgICAgICAgPFN0eWxlZE1hcCBoZWlnaHRNYXA9e2lubmVySGVpZ2h0fT5cbiAgICAgICAgICAgICAgICA8TWFwVmlld1xuICAgICAgICAgICAgICAgICAgICBoYW5kbGVDZW50ZXJDaGFuZ2VkPXtfaGFuZGxlQ2VudGVyQ2hhbmdlZH1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlVHlwZUNoYW5nZWQ9e19oYW5kbGVUeXBlQ2hhbmdlZH1cbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlWm9vbUNoYW5nZWQ9e19oYW5kbGVab29tQ2hhbmdlZH1cbiAgICAgICAgICAgICAgICAgICAgbGF0PXtgJHt0ZW1wQ3VycmVudENlbnRlci5sYXR9YH1cbiAgICAgICAgICAgICAgICAgICAgbG5nPXtgJHt0ZW1wQ3VycmVudENlbnRlci5sbmd9YH1cbiAgICAgICAgICAgICAgICAgICAgbWFwVHlwZT17dGVtcEN1cnJlbnRNYXBUeXBlfVxuICAgICAgICAgICAgICAgICAgICBtYXJrZXJzPXttYXJrZXJzfVxuICAgICAgICAgICAgICAgICAgICB6b29tPXt0ZW1wQ3VycmVudFpvb219XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvU3R5bGVkTWFwPlxuICAgICAgICA8L0NvbnRhaW5lcj5cbiAgICApO1xufTtcblxuTWFwV2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBlZGl0aW9uTGV2ZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGFuZGxlQ2hhbmdlVmFsdWU6IFByb3BUeXBlcy5mdW5jLFxuICAgIGlubmVySGVpZ2h0OiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIGlubmVyV2lkdGg6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBNYXBXaWRnZXQ7XG4iXX0=