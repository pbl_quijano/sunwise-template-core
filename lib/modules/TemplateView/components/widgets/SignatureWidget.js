"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _signature_pad = _interopRequireDefault(require("signature_pad"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    position: relative;\n    width: ", "px;\n    height: ", "px;\n    overflow-y: auto;\n"])), function (_ref) {
  var _ref$innerWidth = _ref.innerWidth,
      innerWidth = _ref$innerWidth === void 0 ? 1 : _ref$innerWidth;
  return innerWidth;
}, function (_ref2) {
  var _ref2$innerHeight = _ref2.innerHeight,
      innerHeight = _ref2$innerHeight === void 0 ? 1 : _ref2$innerHeight;
  return innerHeight;
});

var Canvas = _styledComponents.default.canvas(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    width: 100%;\n"])));

var Line = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    position: absolute;\n    height: 2px;\n    width: 100%;\n    bottom: 0;\n    background: #000;\n    left: 0;\n    pointer-events: none;\n"])));

var SignatureWidget = function SignatureWidget() {
  var _ref3 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      value = _ref3.value,
      innerHeight = _ref3.innerHeight,
      innerWidth = _ref3.innerWidth;

  var canvasRef = (0, _react.useRef)(null);
  var signaturePad = (0, _react.useRef)(null);

  var resizeCanvas = function resizeCanvas() {
    var canvas = canvasRef.current;
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext('2d').scale(ratio, ratio);
  };

  (0, _react.useEffect)(function () {
    signaturePad.current = new _signature_pad.default(canvasRef.current);
    signaturePad.current.off();
  }, []);
  (0, _react.useEffect)(function () {
    if (signaturePad.current && value) {
      signaturePad.current.fromData(JSON.parse(value));
    }
  }, [value, signaturePad.current]);
  (0, _react.useEffect)(function () {
    if (signaturePad.current && innerHeight && innerWidth) {
      resizeCanvas();
    }
  }, [innerHeight, innerWidth, signaturePad.current]);
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    innerHeight: innerHeight,
    innerWidth: innerWidth,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Canvas, {
      ref: canvasRef
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Line, {})]
  });
};

SignatureWidget.propTypes = {
  innerHeight: _propTypes.default.number,
  innerWidth: _propTypes.default.number,
  value: _propTypes.default.object
};
var _default = SignatureWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvU2lnbmF0dXJlV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsIkNhbnZhcyIsImNhbnZhcyIsIkxpbmUiLCJTaWduYXR1cmVXaWRnZXQiLCJ2YWx1ZSIsImNhbnZhc1JlZiIsInNpZ25hdHVyZVBhZCIsInJlc2l6ZUNhbnZhcyIsImN1cnJlbnQiLCJyYXRpbyIsIk1hdGgiLCJtYXgiLCJ3aW5kb3ciLCJkZXZpY2VQaXhlbFJhdGlvIiwid2lkdGgiLCJvZmZzZXRXaWR0aCIsImhlaWdodCIsIm9mZnNldEhlaWdodCIsImdldENvbnRleHQiLCJzY2FsZSIsIlNpZ25hdHVyZVBhZCIsIm9mZiIsImZyb21EYXRhIiwiSlNPTiIsInBhcnNlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwibnVtYmVyIiwib2JqZWN0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxTQUFTLEdBQUdDLDBCQUFPQyxHQUFWLGtOQUlGO0FBQUEsNkJBQUdDLFVBQUg7QUFBQSxNQUFHQSxVQUFILGdDQUFnQixDQUFoQjtBQUFBLFNBQXdCQSxVQUF4QjtBQUFBLENBSkUsRUFLRDtBQUFBLGdDQUFHQyxXQUFIO0FBQUEsTUFBR0EsV0FBSCxrQ0FBaUIsQ0FBakI7QUFBQSxTQUF5QkEsV0FBekI7QUFBQSxDQUxDLENBQWY7O0FBU0EsSUFBTUMsTUFBTSxHQUFHSiwwQkFBT0ssTUFBViwrSkFBWjs7QUFPQSxJQUFNQyxJQUFJLEdBQUdOLDBCQUFPQyxHQUFWLHNOQUFWOztBQVVBLElBQU1NLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsR0FBNkM7QUFBQSxrRkFBUCxFQUFPO0FBQUEsTUFBMUNDLEtBQTBDLFNBQTFDQSxLQUEwQztBQUFBLE1BQW5DTCxXQUFtQyxTQUFuQ0EsV0FBbUM7QUFBQSxNQUF0QkQsVUFBc0IsU0FBdEJBLFVBQXNCOztBQUNqRSxNQUFNTyxTQUFTLEdBQUcsbUJBQU8sSUFBUCxDQUFsQjtBQUNBLE1BQU1DLFlBQVksR0FBRyxtQkFBTyxJQUFQLENBQXJCOztBQUVBLE1BQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLEdBQU07QUFDdkIsUUFBTU4sTUFBTSxHQUFHSSxTQUFTLENBQUNHLE9BQXpCO0FBQ0EsUUFBTUMsS0FBSyxHQUFHQyxJQUFJLENBQUNDLEdBQUwsQ0FBU0MsTUFBTSxDQUFDQyxnQkFBUCxJQUEyQixDQUFwQyxFQUF1QyxDQUF2QyxDQUFkO0FBQ0FaLElBQUFBLE1BQU0sQ0FBQ2EsS0FBUCxHQUFlYixNQUFNLENBQUNjLFdBQVAsR0FBcUJOLEtBQXBDO0FBQ0FSLElBQUFBLE1BQU0sQ0FBQ2UsTUFBUCxHQUFnQmYsTUFBTSxDQUFDZ0IsWUFBUCxHQUFzQlIsS0FBdEM7QUFDQVIsSUFBQUEsTUFBTSxDQUFDaUIsVUFBUCxDQUFrQixJQUFsQixFQUF3QkMsS0FBeEIsQ0FBOEJWLEtBQTlCLEVBQXFDQSxLQUFyQztBQUNILEdBTkQ7O0FBUUEsd0JBQVUsWUFBTTtBQUNaSCxJQUFBQSxZQUFZLENBQUNFLE9BQWIsR0FBdUIsSUFBSVksc0JBQUosQ0FBaUJmLFNBQVMsQ0FBQ0csT0FBM0IsQ0FBdkI7QUFDQUYsSUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCYSxHQUFyQjtBQUNILEdBSEQsRUFHRyxFQUhIO0FBS0Esd0JBQVUsWUFBTTtBQUNaLFFBQUlmLFlBQVksQ0FBQ0UsT0FBYixJQUF3QkosS0FBNUIsRUFBbUM7QUFDL0JFLE1BQUFBLFlBQVksQ0FBQ0UsT0FBYixDQUFxQmMsUUFBckIsQ0FBOEJDLElBQUksQ0FBQ0MsS0FBTCxDQUFXcEIsS0FBWCxDQUE5QjtBQUNIO0FBQ0osR0FKRCxFQUlHLENBQUNBLEtBQUQsRUFBUUUsWUFBWSxDQUFDRSxPQUFyQixDQUpIO0FBTUEsd0JBQVUsWUFBTTtBQUNaLFFBQUlGLFlBQVksQ0FBQ0UsT0FBYixJQUF3QlQsV0FBeEIsSUFBdUNELFVBQTNDLEVBQXVEO0FBQ25EUyxNQUFBQSxZQUFZO0FBQ2Y7QUFDSixHQUpELEVBSUcsQ0FBQ1IsV0FBRCxFQUFjRCxVQUFkLEVBQTBCUSxZQUFZLENBQUNFLE9BQXZDLENBSkg7QUFNQSxzQkFDSSxzQkFBQyxTQUFEO0FBQVcsSUFBQSxXQUFXLEVBQUVULFdBQXhCO0FBQXFDLElBQUEsVUFBVSxFQUFFRCxVQUFqRDtBQUFBLDRCQUNJLHFCQUFDLE1BQUQ7QUFBUSxNQUFBLEdBQUcsRUFBRU87QUFBYixNQURKLGVBRUkscUJBQUMsSUFBRCxLQUZKO0FBQUEsSUFESjtBQU1ILENBbkNEOztBQXFDQUYsZUFBZSxDQUFDc0IsU0FBaEIsR0FBNEI7QUFDeEIxQixFQUFBQSxXQUFXLEVBQUUyQixtQkFBVUMsTUFEQztBQUV4QjdCLEVBQUFBLFVBQVUsRUFBRTRCLG1CQUFVQyxNQUZFO0FBR3hCdkIsRUFBQUEsS0FBSyxFQUFFc0IsbUJBQVVFO0FBSE8sQ0FBNUI7ZUFNZXpCLGUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlUmVmLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgU2lnbmF0dXJlUGFkIGZyb20gJ3NpZ25hdHVyZV9wYWQnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogJHsoeyBpbm5lcldpZHRoID0gMSB9KSA9PiBpbm5lcldpZHRofXB4O1xuICAgIGhlaWdodDogJHsoeyBpbm5lckhlaWdodCA9IDEgfSkgPT4gaW5uZXJIZWlnaHR9cHg7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbmA7XG5cbmNvbnN0IENhbnZhcyA9IHN0eWxlZC5jYW52YXNgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbmA7XG5cbmNvbnN0IExpbmUgPSBzdHlsZWQuZGl2YFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDJweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZDogIzAwMDtcbiAgICBsZWZ0OiAwO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuYDtcblxuY29uc3QgU2lnbmF0dXJlV2lkZ2V0ID0gKHsgdmFsdWUsIGlubmVySGVpZ2h0LCBpbm5lcldpZHRoIH0gPSB7fSkgPT4ge1xuICAgIGNvbnN0IGNhbnZhc1JlZiA9IHVzZVJlZihudWxsKTtcbiAgICBjb25zdCBzaWduYXR1cmVQYWQgPSB1c2VSZWYobnVsbCk7XG5cbiAgICBjb25zdCByZXNpemVDYW52YXMgPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IGNhbnZhcyA9IGNhbnZhc1JlZi5jdXJyZW50O1xuICAgICAgICBjb25zdCByYXRpbyA9IE1hdGgubWF4KHdpbmRvdy5kZXZpY2VQaXhlbFJhdGlvIHx8IDEsIDEpO1xuICAgICAgICBjYW52YXMud2lkdGggPSBjYW52YXMub2Zmc2V0V2lkdGggKiByYXRpbztcbiAgICAgICAgY2FudmFzLmhlaWdodCA9IGNhbnZhcy5vZmZzZXRIZWlnaHQgKiByYXRpbztcbiAgICAgICAgY2FudmFzLmdldENvbnRleHQoJzJkJykuc2NhbGUocmF0aW8sIHJhdGlvKTtcbiAgICB9O1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgc2lnbmF0dXJlUGFkLmN1cnJlbnQgPSBuZXcgU2lnbmF0dXJlUGFkKGNhbnZhc1JlZi5jdXJyZW50KTtcbiAgICAgICAgc2lnbmF0dXJlUGFkLmN1cnJlbnQub2ZmKCk7XG4gICAgfSwgW10pO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgaWYgKHNpZ25hdHVyZVBhZC5jdXJyZW50ICYmIHZhbHVlKSB7XG4gICAgICAgICAgICBzaWduYXR1cmVQYWQuY3VycmVudC5mcm9tRGF0YShKU09OLnBhcnNlKHZhbHVlKSk7XG4gICAgICAgIH1cbiAgICB9LCBbdmFsdWUsIHNpZ25hdHVyZVBhZC5jdXJyZW50XSk7XG5cbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgICBpZiAoc2lnbmF0dXJlUGFkLmN1cnJlbnQgJiYgaW5uZXJIZWlnaHQgJiYgaW5uZXJXaWR0aCkge1xuICAgICAgICAgICAgcmVzaXplQ2FudmFzKCk7XG4gICAgICAgIH1cbiAgICB9LCBbaW5uZXJIZWlnaHQsIGlubmVyV2lkdGgsIHNpZ25hdHVyZVBhZC5jdXJyZW50XSk7XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Q29udGFpbmVyIGlubmVySGVpZ2h0PXtpbm5lckhlaWdodH0gaW5uZXJXaWR0aD17aW5uZXJXaWR0aH0+XG4gICAgICAgICAgICA8Q2FudmFzIHJlZj17Y2FudmFzUmVmfSAvPlxuICAgICAgICAgICAgPExpbmUgLz5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cblNpZ25hdHVyZVdpZGdldC5wcm9wVHlwZXMgPSB7XG4gICAgaW5uZXJIZWlnaHQ6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgaW5uZXJXaWR0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFNpZ25hdHVyZVdpZGdldDtcbiJdfQ==