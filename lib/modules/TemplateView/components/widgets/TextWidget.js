"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactFroalaWysiwyg = _interopRequireDefault(require("react-froala-wysiwyg"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    overflow-y: hidden;\n    width: 100%;\n    .fr-box {\n        height: 100%;\n    }\n\n    .fr-wrapper {\n        height: 100%;\n        background: transparent !important;\n        border-width: 0 !important;\n\n        .fr-element {\n            height: 100%;\n            min-height: 0 !important;\n            padding: 0 !important;\n\n            &.fr-view {\n                color: #000;\n                font-family: inherit;\n                font-size: 14px;\n                line-height: normal;\n                overflow-y: ", ";\n            }\n\n            .fr-command {\n                cursor: pointer;\n                color: inherit;\n                padding: 0;\n                background: ", ";\n            }\n        }\n    }\n\n    .second-toolbar {\n        display: none;\n    }\n"])), function (_ref) {
  var overflowYDisabled = _ref.overflowYDisabled;
  return overflowYDisabled ? 'hidden' : 'auto';
}, function (_ref2) {
  var overflowYDisabled = _ref2.overflowYDisabled,
      editionDisabled = _ref2.editionDisabled;
  return overflowYDisabled || editionDisabled ? 'transparent !important' : '#EEEEEE';
});

var FroalaContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    height: 100%;\n    position: relative;\n    p {\n        margin-bottom: 0;\n    }\n"])));

var TextWidget = function TextWidget(_ref3) {
  var editionDisabled = _ref3.editionDisabled,
      froalaEditorConfig = _ref3.froalaEditorConfig,
      handleChangeValue = _ref3.handleChangeValue,
      isSelected = _ref3.isSelected,
      _ref3$value = _ref3.value,
      value = _ref3$value === void 0 ? '' : _ref3$value;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Container, {
    overflowYDisabled: !isSelected,
    editionDisabled: editionDisabled,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(FroalaContainer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactFroalaWysiwyg.default, {
        config: froalaEditorConfig,
        model: value,
        onModelChange: handleChangeValue,
        tag: "textarea"
      })
    })
  });
};

TextWidget.propTypes = {
  editionDisabled: _propTypes.default.bool,
  froalaEditorConfig: _propTypes.default.object,
  handleChangeValue: _propTypes.default.func,
  isSelected: _propTypes.default.bool,
  keyName: _propTypes.default.string,
  value: _propTypes.default.string
};
var _default = TextWidget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL3dpZGdldHMvVGV4dFdpZGdldC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJvdmVyZmxvd1lEaXNhYmxlZCIsImVkaXRpb25EaXNhYmxlZCIsIkZyb2FsYUNvbnRhaW5lciIsIlRleHRXaWRnZXQiLCJmcm9hbGFFZGl0b3JDb25maWciLCJoYW5kbGVDaGFuZ2VWYWx1ZSIsImlzU2VsZWN0ZWQiLCJ2YWx1ZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJvYmplY3QiLCJmdW5jIiwia2V5TmFtZSIsInN0cmluZyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBVixvN0JBeUJlO0FBQUEsTUFBR0MsaUJBQUgsUUFBR0EsaUJBQUg7QUFBQSxTQUNWQSxpQkFBaUIsR0FBRyxRQUFILEdBQWMsTUFEckI7QUFBQSxDQXpCZixFQWlDZTtBQUFBLE1BQUdBLGlCQUFILFNBQUdBLGlCQUFIO0FBQUEsTUFBc0JDLGVBQXRCLFNBQXNCQSxlQUF0QjtBQUFBLFNBQ1ZELGlCQUFpQixJQUFJQyxlQUFyQixHQUNNLHdCQUROLEdBRU0sU0FISTtBQUFBLENBakNmLENBQWY7O0FBOENBLElBQU1DLGVBQWUsR0FBR0osMEJBQU9DLEdBQVYsZ0tBQXJCOztBQVFBLElBQU1JLFVBQVUsR0FBRyxTQUFiQSxVQUFhLFFBTWI7QUFBQSxNQUxGRixlQUtFLFNBTEZBLGVBS0U7QUFBQSxNQUpGRyxrQkFJRSxTQUpGQSxrQkFJRTtBQUFBLE1BSEZDLGlCQUdFLFNBSEZBLGlCQUdFO0FBQUEsTUFGRkMsVUFFRSxTQUZGQSxVQUVFO0FBQUEsMEJBREZDLEtBQ0U7QUFBQSxNQURGQSxLQUNFLDRCQURNLEVBQ047QUFDRixzQkFDSSxxQkFBQyxTQUFEO0FBQ0ksSUFBQSxpQkFBaUIsRUFBRSxDQUFDRCxVQUR4QjtBQUVJLElBQUEsZUFBZSxFQUFFTCxlQUZyQjtBQUFBLDJCQUlJLHFCQUFDLGVBQUQ7QUFBQSw2QkFDSSxxQkFBQywyQkFBRDtBQUNJLFFBQUEsTUFBTSxFQUFFRyxrQkFEWjtBQUVJLFFBQUEsS0FBSyxFQUFFRyxLQUZYO0FBR0ksUUFBQSxhQUFhLEVBQUVGLGlCQUhuQjtBQUlJLFFBQUEsR0FBRyxFQUFDO0FBSlI7QUFESjtBQUpKLElBREo7QUFlSCxDQXRCRDs7QUF3QkFGLFVBQVUsQ0FBQ0ssU0FBWCxHQUF1QjtBQUNuQlAsRUFBQUEsZUFBZSxFQUFFUSxtQkFBVUMsSUFEUjtBQUVuQk4sRUFBQUEsa0JBQWtCLEVBQUVLLG1CQUFVRSxNQUZYO0FBR25CTixFQUFBQSxpQkFBaUIsRUFBRUksbUJBQVVHLElBSFY7QUFJbkJOLEVBQUFBLFVBQVUsRUFBRUcsbUJBQVVDLElBSkg7QUFLbkJHLEVBQUFBLE9BQU8sRUFBRUosbUJBQVVLLE1BTEE7QUFNbkJQLEVBQUFBLEtBQUssRUFBRUUsbUJBQVVLO0FBTkUsQ0FBdkI7ZUFTZVgsVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgRnJvYWxhRWRpdG9yQ29tcG9uZW50IGZyb20gJ3JlYWN0LWZyb2FsYS13eXNpd3lnJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC5mci1ib3gge1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgfVxuXG4gICAgLmZyLXdyYXBwZXIge1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgICAgIGJvcmRlci13aWR0aDogMCAhaW1wb3J0YW50O1xuXG4gICAgICAgIC5mci1lbGVtZW50IHtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDAgIWltcG9ydGFudDtcbiAgICAgICAgICAgIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcblxuICAgICAgICAgICAgJi5mci12aWV3IHtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogaW5oZXJpdDtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgICAgICAgICBvdmVyZmxvdy15OiAkeyh7IG92ZXJmbG93WURpc2FibGVkIH0pID0+XG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93WURpc2FibGVkID8gJ2hpZGRlbicgOiAnYXV0byd9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAuZnItY29tbWFuZCB7XG4gICAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgICAgIGNvbG9yOiBpbmhlcml0O1xuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogJHsoeyBvdmVyZmxvd1lEaXNhYmxlZCwgZWRpdGlvbkRpc2FibGVkIH0pID0+XG4gICAgICAgICAgICAgICAgICAgIG92ZXJmbG93WURpc2FibGVkIHx8IGVkaXRpb25EaXNhYmxlZFxuICAgICAgICAgICAgICAgICAgICAgICAgPyAndHJhbnNwYXJlbnQgIWltcG9ydGFudCdcbiAgICAgICAgICAgICAgICAgICAgICAgIDogJyNFRUVFRUUnfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5zZWNvbmQtdG9vbGJhciB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuYDtcblxuY29uc3QgRnJvYWxhQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHAge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIH1cbmA7XG5cbmNvbnN0IFRleHRXaWRnZXQgPSAoe1xuICAgIGVkaXRpb25EaXNhYmxlZCxcbiAgICBmcm9hbGFFZGl0b3JDb25maWcsXG4gICAgaGFuZGxlQ2hhbmdlVmFsdWUsXG4gICAgaXNTZWxlY3RlZCxcbiAgICB2YWx1ZSA9ICcnLFxufSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXJcbiAgICAgICAgICAgIG92ZXJmbG93WURpc2FibGVkPXshaXNTZWxlY3RlZH1cbiAgICAgICAgICAgIGVkaXRpb25EaXNhYmxlZD17ZWRpdGlvbkRpc2FibGVkfVxuICAgICAgICA+XG4gICAgICAgICAgICA8RnJvYWxhQ29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxGcm9hbGFFZGl0b3JDb21wb25lbnRcbiAgICAgICAgICAgICAgICAgICAgY29uZmlnPXtmcm9hbGFFZGl0b3JDb25maWd9XG4gICAgICAgICAgICAgICAgICAgIG1vZGVsPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgb25Nb2RlbENoYW5nZT17aGFuZGxlQ2hhbmdlVmFsdWV9XG4gICAgICAgICAgICAgICAgICAgIHRhZz1cInRleHRhcmVhXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9Gcm9hbGFDb250YWluZXI+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5UZXh0V2lkZ2V0LnByb3BUeXBlcyA9IHtcbiAgICBlZGl0aW9uRGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGZyb2FsYUVkaXRvckNvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBoYW5kbGVDaGFuZ2VWYWx1ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaXNTZWxlY3RlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAga2V5TmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRleHRXaWRnZXQ7XG4iXX0=