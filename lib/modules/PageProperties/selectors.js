"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getModel = exports.getLanguagesDictionary = exports.getLanguagesData = exports.getLanguageModel = void 0;

var _reselect = require("reselect");

var _utils = require("../../helpers/utils");

var selectors = _interopRequireWildcard(require("../main/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var getModel = (0, _reselect.createSelector)(selectors.getLibState, function (sunwiseTemplateCore) {
  return sunwiseTemplateCore.pageProperties;
});
exports.getModel = getModel;
var getLanguageModel = (0, _reselect.createSelector)(getModel, function (model) {
  return model.languages;
});
exports.getLanguageModel = getLanguageModel;
var getLanguagesData = (0, _reselect.createSelector)(getLanguageModel, function (model) {
  return model.data || [];
});
exports.getLanguagesData = getLanguagesData;
var getLanguagesDictionary = (0, _reselect.createSelector)(getLanguagesData, function (languagesData) {
  return (0, _utils.arraytoDictionary)(languagesData, 'key');
});
exports.getLanguagesDictionary = getLanguagesDictionary;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1BhZ2VQcm9wZXJ0aWVzL3NlbGVjdG9ycy5qcyJdLCJuYW1lcyI6WyJnZXRNb2RlbCIsInNlbGVjdG9ycyIsImdldExpYlN0YXRlIiwic3Vud2lzZVRlbXBsYXRlQ29yZSIsInBhZ2VQcm9wZXJ0aWVzIiwiZ2V0TGFuZ3VhZ2VNb2RlbCIsIm1vZGVsIiwibGFuZ3VhZ2VzIiwiZ2V0TGFuZ3VhZ2VzRGF0YSIsImRhdGEiLCJnZXRMYW5ndWFnZXNEaWN0aW9uYXJ5IiwibGFuZ3VhZ2VzRGF0YSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBRUE7O0FBRUE7Ozs7OztBQUVPLElBQU1BLFFBQVEsR0FBRyw4QkFDcEJDLFNBQVMsQ0FBQ0MsV0FEVSxFQUVwQixVQUFDQyxtQkFBRDtBQUFBLFNBQXlCQSxtQkFBbUIsQ0FBQ0MsY0FBN0M7QUFBQSxDQUZvQixDQUFqQjs7QUFLQSxJQUFNQyxnQkFBZ0IsR0FBRyw4QkFDNUJMLFFBRDRCLEVBRTVCLFVBQUNNLEtBQUQ7QUFBQSxTQUFXQSxLQUFLLENBQUNDLFNBQWpCO0FBQUEsQ0FGNEIsQ0FBekI7O0FBS0EsSUFBTUMsZ0JBQWdCLEdBQUcsOEJBQzVCSCxnQkFENEIsRUFFNUIsVUFBQ0MsS0FBRDtBQUFBLFNBQVdBLEtBQUssQ0FBQ0csSUFBTixJQUFjLEVBQXpCO0FBQUEsQ0FGNEIsQ0FBekI7O0FBS0EsSUFBTUMsc0JBQXNCLEdBQUcsOEJBQ2xDRixnQkFEa0MsRUFFbEMsVUFBQ0csYUFBRDtBQUFBLFNBQW1CLDhCQUFrQkEsYUFBbEIsRUFBaUMsS0FBakMsQ0FBbkI7QUFBQSxDQUZrQyxDQUEvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGNyZWF0ZVNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xuXG5pbXBvcnQgeyBhcnJheXRvRGljdGlvbmFyeSB9IGZyb20gJ0BoZWxwZXJzL3V0aWxzJztcblxuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XG5cbmV4cG9ydCBjb25zdCBnZXRNb2RlbCA9IGNyZWF0ZVNlbGVjdG9yKFxuICAgIHNlbGVjdG9ycy5nZXRMaWJTdGF0ZSxcbiAgICAoc3Vud2lzZVRlbXBsYXRlQ29yZSkgPT4gc3Vud2lzZVRlbXBsYXRlQ29yZS5wYWdlUHJvcGVydGllc1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldExhbmd1YWdlTW9kZWwgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRNb2RlbCxcbiAgICAobW9kZWwpID0+IG1vZGVsLmxhbmd1YWdlc1xuKTtcblxuZXhwb3J0IGNvbnN0IGdldExhbmd1YWdlc0RhdGEgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRMYW5ndWFnZU1vZGVsLFxuICAgIChtb2RlbCkgPT4gbW9kZWwuZGF0YSB8fCBbXVxuKTtcblxuZXhwb3J0IGNvbnN0IGdldExhbmd1YWdlc0RpY3Rpb25hcnkgPSBjcmVhdGVTZWxlY3RvcihcbiAgICBnZXRMYW5ndWFnZXNEYXRhLFxuICAgIChsYW5ndWFnZXNEYXRhKSA9PiBhcnJheXRvRGljdGlvbmFyeShsYW5ndWFnZXNEYXRhLCAna2V5Jylcbik7XG4iXX0=