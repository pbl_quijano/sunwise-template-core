"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactBootstrap = require("react-bootstrap");

var _Form = _interopRequireDefault(require("react-bootstrap/Form"));

var _reactI18next = require("react-i18next");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ButtonComponent = _interopRequireDefault(require("../../components/ButtonComponent"));

var _styled = require("../../components/styled");

var _TitleWithDetail = _interopRequireDefault(require("../../components/TitleWithDetail"));

var _contexts = require("../../helpers/contexts");

var templateViewSelectors = _interopRequireWildcard(require("../TemplateView/selectors"));

var templateCoreActions = _interopRequireWildcard(require("../TemplateCore/actions"));

var _constants = require("../TemplateCore/constants");

var templateCoreSelectors = _interopRequireWildcard(require("../TemplateCore/selectors"));

var actions = _interopRequireWildcard(require("./actions"));

var _helpers = require("./helpers");

var selectors = _interopRequireWildcard(require("./selectors"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: block;\n    padding: 25px 27px 20px 27px;\n    ", "\n"])), function (_ref) {
  var disabled = _ref.disabled;
  return disabled && "opacity: 0.6;\n        cursor: not-allowed;\n        pointer-events: none;";
});

var InstructionText = _styledComponents.default.span(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    display: block;\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-top: 14px;\n    padding-left: 12px;\n"])));

var ThemeSelectContainer = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    justify-content: space-between;\n    margin-top: 17px;\n    padding-left: 12px;\n"])));

var TemplatePropiertyContainer = (0, _styledComponents.default)(_styled.FlexRow)(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    padding-left: 12px;\n    margin-top: 32px;\n"])));

var LabelText = _styledComponents.default.span(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 12px;\n    line-height: 15px;\n    min-height: 15px;\n"])));

var TemplatePropiertyText = _styledComponents.default.p(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    font-weight: 500;\n    font-size: 12px;\n    line-height: 12px;\n    letter-spacing: 0.25px;\n    color: #848bab;\n    margin-bottom: 0;\n"])));

var StyledInputGroup = (0, _styledComponents.default)(_reactBootstrap.InputGroup)(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    height: 32px;\n    width: ", ";\n\n    & select {\n        background-color: #ffffff;\n        border-radius: 3px;\n        border: 1px solid #ecedf0;\n        font-size: 12px;\n        height: 32px;\n        width: ", ";\n    }\n"])), function (_ref2) {
  var _ref2$width = _ref2.width,
      width = _ref2$width === void 0 ? '190px' : _ref2$width;
  return width;
}, function (_ref3) {
  var _ref3$width = _ref3.width,
      width = _ref3$width === void 0 ? '190px' : _ref3$width;
  return width;
});

var PageProperties = function PageProperties(_ref4) {
  var fetchLanguages = _ref4.fetchLanguages,
      isInfiniteModeValid = _ref4.isInfiniteModeValid,
      languagesDictionary = _ref4.languagesDictionary,
      selectedPageId = _ref4.selectedPageId,
      selectedPageInfiniteMode = _ref4.selectedPageInfiniteMode,
      selectedPageIsBlocked = _ref4.selectedPageIsBlocked,
      selectedPageOrientation = _ref4.selectedPageOrientation,
      selectedPageTheme = _ref4.selectedPageTheme,
      setInfiniteMode = _ref4.setInfiniteMode,
      setOrientation = _ref4.setOrientation,
      setTheme = _ref4.setTheme,
      templateLanguage = _ref4.templateLanguage,
      templateType = _ref4.templateType,
      _ref4$themeEnabled = _ref4.themeEnabled,
      themeEnabled = _ref4$themeEnabled === void 0 ? true : _ref4$themeEnabled;

  var _useContext = (0, _react.useContext)(_contexts.GeneralContext),
      onChangeInPage = _useContext.onChangeInPage;

  var _useTranslation = (0, _reactI18next.useTranslation)(),
      t = _useTranslation.t;

  (0, _react.useEffect)(function () {
    fetchLanguages();
  }, []);

  var _useState = (0, _react.useState)(selectedPageInfiniteMode),
      _useState2 = _slicedToArray(_useState, 2),
      infiniteModeControl = _useState2[0],
      setInfiniteModeControl = _useState2[1];

  (0, _react.useEffect)(function () {
    if (selectedPageInfiniteMode !== infiniteModeControl) {
      setInfiniteModeControl(selectedPageInfiniteMode);
    }
  }, [selectedPageInfiniteMode]);

  var onChangeThemeSelect = function onChangeThemeSelect(e) {
    setTheme(selectedPageId, e.target.value, onChangeInPage);
  };

  var onChangeOrientationSelect = function onChangeOrientationSelect(e) {
    setOrientation(selectedPageId, e.target.value, onChangeInPage);
  };

  var handleChangeInfiniteMode = function handleChangeInfiniteMode(e) {
    setInfiniteMode(selectedPageId, e.target.checked, onChangeInPage);
  };

  if (!themeEnabled) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    disabled: selectedPageIsBlocked,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_TitleWithDetail.default, {
      className: "mb-0",
      children: t('Properties')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(TemplatePropiertyContainer, {
      alignItems: "center",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        xs: "6 pl-0",
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(TemplatePropiertyText, {
          children: [t('Language'), ":"]
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        xs: "12 pl-0",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(TemplatePropiertyText, {
          children: (0, _helpers.getTemplateLanguage)(templateLanguage, languagesDictionary)
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(TemplatePropiertyContainer, {
      alignItems: "center",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        xs: "6 pl-0",
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(TemplatePropiertyText, {
          children: [t('Template type'), ":"]
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
        xs: "12 pl-0",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(TemplatePropiertyText, {
          children: (0, _helpers.getTemplateTypeText)(templateType)
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_styled.FlexRow, {
      justifyContent: "center",
      className: "mt-2",
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_ButtonComponent.default, {
        as: "a",
        className: "mt-2 text-left",
        fontSize: "12px",
        fontWeight: "bold",
        href: "http://academy.sunwise.mx/es/articles/5547159-resumen-de-propuestas",
        target: "_blank",
        textColor: "#1F3C53",
        children: t('See manual')
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(InstructionText, {
      children: t('This theme will be used in the new components of your template.')
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(ThemeSelectContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(LabelText, {
        children: [t('Theme'), ":"]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Control, {
          value: selectedPageTheme,
          onChange: onChangeThemeSelect,
          as: "select",
          id: "select-theme",
          children: Object.keys(_constants.TABLE_THEMES).map(function (key) {
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: key,
              children: _constants.TABLE_THEMES[key].name
            }, "select-".concat(key, "-").concat(_constants.TABLE_THEMES[key].name));
          })
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(ThemeSelectContainer, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(LabelText, {
        children: [t('Orientation'), ":"]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        width: "160px",
        children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_Form.default.Control, {
          value: selectedPageOrientation,
          onChange: onChangeOrientationSelect,
          as: "select",
          id: "select-orientation",
          children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
            value: "portrait",
            children: t('Vertical')
          }), /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
            value: "landscape",
            children: t('Horizontal')
          })]
        })
      })]
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(ThemeSelectContainer, {
      className: "text-top",
      children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(LabelText, {
        children: [t('Dynamic content'), ":"]
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputGroup, {
        width: "160px",
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_Form.default.Check, {
          type: "checkbox",
          id: "infite-mode",
          onChange: handleChangeInfiniteMode,
          disabled: !isInfiniteModeValid,
          label: "",
          checked: infiniteModeControl,
          value: infiniteModeControl
        })
      })]
    })]
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  isInfiniteModeValid: templateViewSelectors.isInfiniteModeValid,
  languagesDictionary: selectors.getLanguagesDictionary,
  selectedPageId: templateViewSelectors.getSelectedPageId,
  selectedPageInfiniteMode: templateViewSelectors.getSelectedPageInfiniteMode,
  selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked,
  selectedPageOrientation: templateViewSelectors.getSelectedPageOrientation,
  selectedPageTheme: templateViewSelectors.getSelectedPageTheme,
  templateLanguage: templateCoreSelectors.getCurrentTemplateLanguage,
  templateType: templateCoreSelectors.getCurrentTemplateType
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    fetchLanguages: function fetchLanguages() {
      return dispatch(actions.fetchLanguages());
    },
    setInfiniteMode: function setInfiniteMode(pageId, infiniteMode, onChangeInPage) {
      return dispatch(templateCoreActions.setInfiniteMode(pageId, infiniteMode, onChangeInPage));
    },
    setOrientation: function setOrientation(pageId, orientation, onChangeInPage) {
      return dispatch(templateCoreActions.setOrientation(pageId, orientation, onChangeInPage));
    },
    setTheme: function setTheme(pageId, theme, onChangeInPage) {
      return dispatch(templateCoreActions.setTheme(pageId, theme, onChangeInPage));
    }
  };
};

PageProperties.propTypes = {
  chartWidgets: _propTypes.default.array,
  fetchLanguages: _propTypes.default.func,
  isInfiniteModeValid: _propTypes.default.bool,
  languagesDictionary: _propTypes.default.object,
  selectedPageId: _propTypes.default.string,
  selectedPageInfiniteMode: _propTypes.default.bool,
  selectedPageIsBlocked: _propTypes.default.bool,
  selectedPageOrientation: _propTypes.default.string,
  selectedPageTheme: _propTypes.default.string,
  setInfiniteMode: _propTypes.default.func,
  setOrientation: _propTypes.default.func,
  setTheme: _propTypes.default.func,
  templateLanguage: _propTypes.default.string,
  templateType: _propTypes.default.number,
  themeEnabled: _propTypes.default.bool
};

var _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(PageProperties);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1BhZ2VQcm9wZXJ0aWVzL2luZGV4LmpzIl0sIm5hbWVzIjpbIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsImRpc2FibGVkIiwiSW5zdHJ1Y3Rpb25UZXh0Iiwic3BhbiIsIlRoZW1lU2VsZWN0Q29udGFpbmVyIiwiVGVtcGxhdGVQcm9waWVydHlDb250YWluZXIiLCJGbGV4Um93IiwiTGFiZWxUZXh0IiwiVGVtcGxhdGVQcm9waWVydHlUZXh0IiwicCIsIlN0eWxlZElucHV0R3JvdXAiLCJJbnB1dEdyb3VwIiwid2lkdGgiLCJQYWdlUHJvcGVydGllcyIsImZldGNoTGFuZ3VhZ2VzIiwiaXNJbmZpbml0ZU1vZGVWYWxpZCIsImxhbmd1YWdlc0RpY3Rpb25hcnkiLCJzZWxlY3RlZFBhZ2VJZCIsInNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSIsInNlbGVjdGVkUGFnZUlzQmxvY2tlZCIsInNlbGVjdGVkUGFnZU9yaWVudGF0aW9uIiwic2VsZWN0ZWRQYWdlVGhlbWUiLCJzZXRJbmZpbml0ZU1vZGUiLCJzZXRPcmllbnRhdGlvbiIsInNldFRoZW1lIiwidGVtcGxhdGVMYW5ndWFnZSIsInRlbXBsYXRlVHlwZSIsInRoZW1lRW5hYmxlZCIsIkdlbmVyYWxDb250ZXh0Iiwib25DaGFuZ2VJblBhZ2UiLCJ0IiwiaW5maW5pdGVNb2RlQ29udHJvbCIsInNldEluZmluaXRlTW9kZUNvbnRyb2wiLCJvbkNoYW5nZVRoZW1lU2VsZWN0IiwiZSIsInRhcmdldCIsInZhbHVlIiwib25DaGFuZ2VPcmllbnRhdGlvblNlbGVjdCIsImhhbmRsZUNoYW5nZUluZmluaXRlTW9kZSIsImNoZWNrZWQiLCJPYmplY3QiLCJrZXlzIiwiVEFCTEVfVEhFTUVTIiwibWFwIiwia2V5IiwibmFtZSIsIm1hcFN0YXRlVG9Qcm9wcyIsInRlbXBsYXRlVmlld1NlbGVjdG9ycyIsInNlbGVjdG9ycyIsImdldExhbmd1YWdlc0RpY3Rpb25hcnkiLCJnZXRTZWxlY3RlZFBhZ2VJZCIsImdldFNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSIsImdldFNlbGVjdGVkUGFnZUlzQmxvY2tlZCIsImdldFNlbGVjdGVkUGFnZU9yaWVudGF0aW9uIiwiZ2V0U2VsZWN0ZWRQYWdlVGhlbWUiLCJ0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMiLCJnZXRDdXJyZW50VGVtcGxhdGVMYW5ndWFnZSIsImdldEN1cnJlbnRUZW1wbGF0ZVR5cGUiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCIsImFjdGlvbnMiLCJwYWdlSWQiLCJpbmZpbml0ZU1vZGUiLCJ0ZW1wbGF0ZUNvcmVBY3Rpb25zIiwib3JpZW50YXRpb24iLCJ0aGVtZSIsInByb3BUeXBlcyIsImNoYXJ0V2lkZ2V0cyIsIlByb3BUeXBlcyIsImFycmF5IiwiZnVuYyIsImJvb2wiLCJvYmplY3QiLCJzdHJpbmciLCJudW1iZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsMElBR1Q7QUFBQSxNQUFHQyxRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUNFQSxRQUFRLGdGQURWO0FBQUEsQ0FIUyxDQUFmOztBQVVBLElBQU1DLGVBQWUsR0FBR0gsMEJBQU9JLElBQVYsaU5BQXJCOztBQVNBLElBQU1DLG9CQUFvQixHQUFHTCwwQkFBT0MsR0FBViw0TUFBMUI7O0FBUUEsSUFBTUssMEJBQTBCLEdBQUcsK0JBQU9DLGVBQVAsQ0FBSCx5SEFBaEM7O0FBS0EsSUFBTUMsU0FBUyxHQUFHUiwwQkFBT0ksSUFBVixtS0FBZjs7QUFPQSxJQUFNSyxxQkFBcUIsR0FBR1QsMEJBQU9VLENBQVYsdU5BQTNCOztBQVNBLElBQU1DLGdCQUFnQixHQUFHLCtCQUFPQywwQkFBUCxDQUFILG9UQUVUO0FBQUEsMEJBQUdDLEtBQUg7QUFBQSxNQUFHQSxLQUFILDRCQUFXLE9BQVg7QUFBQSxTQUF5QkEsS0FBekI7QUFBQSxDQUZTLEVBVUw7QUFBQSwwQkFBR0EsS0FBSDtBQUFBLE1BQUdBLEtBQUgsNEJBQVcsT0FBWDtBQUFBLFNBQXlCQSxLQUF6QjtBQUFBLENBVkssQ0FBdEI7O0FBY0EsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixRQWVqQjtBQUFBLE1BZEZDLGNBY0UsU0FkRkEsY0FjRTtBQUFBLE1BYkZDLG1CQWFFLFNBYkZBLG1CQWFFO0FBQUEsTUFaRkMsbUJBWUUsU0FaRkEsbUJBWUU7QUFBQSxNQVhGQyxjQVdFLFNBWEZBLGNBV0U7QUFBQSxNQVZGQyx3QkFVRSxTQVZGQSx3QkFVRTtBQUFBLE1BVEZDLHFCQVNFLFNBVEZBLHFCQVNFO0FBQUEsTUFSRkMsdUJBUUUsU0FSRkEsdUJBUUU7QUFBQSxNQVBGQyxpQkFPRSxTQVBGQSxpQkFPRTtBQUFBLE1BTkZDLGVBTUUsU0FORkEsZUFNRTtBQUFBLE1BTEZDLGNBS0UsU0FMRkEsY0FLRTtBQUFBLE1BSkZDLFFBSUUsU0FKRkEsUUFJRTtBQUFBLE1BSEZDLGdCQUdFLFNBSEZBLGdCQUdFO0FBQUEsTUFGRkMsWUFFRSxTQUZGQSxZQUVFO0FBQUEsaUNBREZDLFlBQ0U7QUFBQSxNQURGQSxZQUNFLG1DQURhLElBQ2I7O0FBQ0Ysb0JBQTJCLHVCQUFXQyx3QkFBWCxDQUEzQjtBQUFBLE1BQVFDLGNBQVIsZUFBUUEsY0FBUjs7QUFDQSx3QkFBYyxtQ0FBZDtBQUFBLE1BQVFDLENBQVIsbUJBQVFBLENBQVI7O0FBRUEsd0JBQVUsWUFBTTtBQUNaaEIsSUFBQUEsY0FBYztBQUNqQixHQUZELEVBRUcsRUFGSDs7QUFJQSxrQkFBc0QscUJBQ2xESSx3QkFEa0QsQ0FBdEQ7QUFBQTtBQUFBLE1BQU9hLG1CQUFQO0FBQUEsTUFBNEJDLHNCQUE1Qjs7QUFJQSx3QkFBVSxZQUFNO0FBQ1osUUFBSWQsd0JBQXdCLEtBQUthLG1CQUFqQyxFQUFzRDtBQUNsREMsTUFBQUEsc0JBQXNCLENBQUNkLHdCQUFELENBQXRCO0FBQ0g7QUFDSixHQUpELEVBSUcsQ0FBQ0Esd0JBQUQsQ0FKSDs7QUFNQSxNQUFNZSxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLENBQUQsRUFBTztBQUMvQlYsSUFBQUEsUUFBUSxDQUFDUCxjQUFELEVBQWlCaUIsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEtBQTFCLEVBQWlDUCxjQUFqQyxDQUFSO0FBQ0gsR0FGRDs7QUFJQSxNQUFNUSx5QkFBeUIsR0FBRyxTQUE1QkEseUJBQTRCLENBQUNILENBQUQsRUFBTztBQUNyQ1gsSUFBQUEsY0FBYyxDQUFDTixjQUFELEVBQWlCaUIsQ0FBQyxDQUFDQyxNQUFGLENBQVNDLEtBQTFCLEVBQWlDUCxjQUFqQyxDQUFkO0FBQ0gsR0FGRDs7QUFJQSxNQUFNUyx3QkFBd0IsR0FBRyxTQUEzQkEsd0JBQTJCLENBQUNKLENBQUQsRUFBTztBQUNwQ1osSUFBQUEsZUFBZSxDQUFDTCxjQUFELEVBQWlCaUIsQ0FBQyxDQUFDQyxNQUFGLENBQVNJLE9BQTFCLEVBQW1DVixjQUFuQyxDQUFmO0FBQ0gsR0FGRDs7QUFJQSxNQUFJLENBQUNGLFlBQUwsRUFBbUI7QUFDZixXQUFPLElBQVA7QUFDSDs7QUFDRCxzQkFDSSxzQkFBQyxTQUFEO0FBQVcsSUFBQSxRQUFRLEVBQUVSLHFCQUFyQjtBQUFBLDRCQUNJLHFCQUFDLHdCQUFEO0FBQWlCLE1BQUEsU0FBUyxFQUFDLE1BQTNCO0FBQUEsZ0JBQ0tXLENBQUMsQ0FBQyxZQUFEO0FBRE4sTUFESixlQUtJLHNCQUFDLDBCQUFEO0FBQTRCLE1BQUEsVUFBVSxFQUFDLFFBQXZDO0FBQUEsOEJBQ0kscUJBQUMsbUJBQUQ7QUFBSyxRQUFBLEVBQUUsRUFBQyxRQUFSO0FBQUEsK0JBQ0ksc0JBQUMscUJBQUQ7QUFBQSxxQkFDS0EsQ0FBQyxDQUFDLFVBQUQsQ0FETjtBQUFBO0FBREosUUFESixlQU1JLHFCQUFDLG1CQUFEO0FBQUssUUFBQSxFQUFFLEVBQUMsU0FBUjtBQUFBLCtCQUNJLHFCQUFDLHFCQUFEO0FBQUEsb0JBQ0ssa0NBQ0dMLGdCQURILEVBRUdULG1CQUZIO0FBREw7QUFESixRQU5KO0FBQUEsTUFMSixlQXFCSSxzQkFBQywwQkFBRDtBQUE0QixNQUFBLFVBQVUsRUFBQyxRQUF2QztBQUFBLDhCQUNJLHFCQUFDLG1CQUFEO0FBQUssUUFBQSxFQUFFLEVBQUMsUUFBUjtBQUFBLCtCQUNJLHNCQUFDLHFCQUFEO0FBQUEscUJBQ0tjLENBQUMsQ0FBQyxlQUFELENBRE47QUFBQTtBQURKLFFBREosZUFNSSxxQkFBQyxtQkFBRDtBQUFLLFFBQUEsRUFBRSxFQUFDLFNBQVI7QUFBQSwrQkFDSSxxQkFBQyxxQkFBRDtBQUFBLG9CQUNLLGtDQUFvQkosWUFBcEI7QUFETDtBQURKLFFBTko7QUFBQSxNQXJCSixlQWtDSSxxQkFBQyxlQUFEO0FBQVMsTUFBQSxjQUFjLEVBQUMsUUFBeEI7QUFBaUMsTUFBQSxTQUFTLEVBQUMsTUFBM0M7QUFBQSw2QkFDSSxxQkFBQyx3QkFBRDtBQUNJLFFBQUEsRUFBRSxFQUFDLEdBRFA7QUFFSSxRQUFBLFNBQVMsRUFBQyxnQkFGZDtBQUdJLFFBQUEsUUFBUSxFQUFDLE1BSGI7QUFJSSxRQUFBLFVBQVUsRUFBQyxNQUpmO0FBS0ksUUFBQSxJQUFJLEVBQUMscUVBTFQ7QUFNSSxRQUFBLE1BQU0sRUFBQyxRQU5YO0FBT0ksUUFBQSxTQUFTLEVBQUMsU0FQZDtBQUFBLGtCQVNLSSxDQUFDLENBQUMsWUFBRDtBQVROO0FBREosTUFsQ0osZUFnREkscUJBQUMsZUFBRDtBQUFBLGdCQUNLQSxDQUFDLENBQ0UsaUVBREY7QUFETixNQWhESixlQXNESSxzQkFBQyxvQkFBRDtBQUFBLDhCQUNJLHNCQUFDLFNBQUQ7QUFBQSxtQkFBWUEsQ0FBQyxDQUFDLE9BQUQsQ0FBYjtBQUFBLFFBREosZUFHSSxxQkFBQyxnQkFBRDtBQUFBLCtCQUNJLHFCQUFDLGFBQUQsQ0FBTSxPQUFOO0FBQ0ksVUFBQSxLQUFLLEVBQUVULGlCQURYO0FBRUksVUFBQSxRQUFRLEVBQUVZLG1CQUZkO0FBR0ksVUFBQSxFQUFFLEVBQUMsUUFIUDtBQUlJLFVBQUEsRUFBRSxFQUFDLGNBSlA7QUFBQSxvQkFNS08sTUFBTSxDQUFDQyxJQUFQLENBQVlDLHVCQUFaLEVBQTBCQyxHQUExQixDQUE4QixVQUFDQyxHQUFELEVBQVM7QUFDcEMsZ0NBQ0k7QUFFSSxjQUFBLEtBQUssRUFBRUEsR0FGWDtBQUFBLHdCQUlLRix3QkFBYUUsR0FBYixFQUFrQkM7QUFKdkIsZ0NBQ21CRCxHQURuQixjQUMwQkYsd0JBQWFFLEdBQWIsRUFBa0JDLElBRDVDLEVBREo7QUFRSCxXQVRBO0FBTkw7QUFESixRQUhKO0FBQUEsTUF0REosZUE2RUksc0JBQUMsb0JBQUQ7QUFBQSw4QkFDSSxzQkFBQyxTQUFEO0FBQUEsbUJBQVlmLENBQUMsQ0FBQyxhQUFELENBQWI7QUFBQSxRQURKLGVBR0kscUJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxLQUFLLEVBQUMsT0FBeEI7QUFBQSwrQkFDSSxzQkFBQyxhQUFELENBQU0sT0FBTjtBQUNJLFVBQUEsS0FBSyxFQUFFVix1QkFEWDtBQUVJLFVBQUEsUUFBUSxFQUFFaUIseUJBRmQ7QUFHSSxVQUFBLEVBQUUsRUFBQyxRQUhQO0FBSUksVUFBQSxFQUFFLEVBQUMsb0JBSlA7QUFBQSxrQ0FNSTtBQUFRLFlBQUEsS0FBSyxFQUFDLFVBQWQ7QUFBQSxzQkFBMEJQLENBQUMsQ0FBQyxVQUFEO0FBQTNCLFlBTkosZUFPSTtBQUFRLFlBQUEsS0FBSyxFQUFDLFdBQWQ7QUFBQSxzQkFBMkJBLENBQUMsQ0FBQyxZQUFEO0FBQTVCLFlBUEo7QUFBQTtBQURKLFFBSEo7QUFBQSxNQTdFSixlQTRGSSxzQkFBQyxvQkFBRDtBQUFzQixNQUFBLFNBQVMsRUFBQyxVQUFoQztBQUFBLDhCQUNJLHNCQUFDLFNBQUQ7QUFBQSxtQkFBWUEsQ0FBQyxDQUFDLGlCQUFELENBQWI7QUFBQSxRQURKLGVBRUkscUJBQUMsZ0JBQUQ7QUFBa0IsUUFBQSxLQUFLLEVBQUMsT0FBeEI7QUFBQSwrQkFDSSxxQkFBQyxhQUFELENBQU0sS0FBTjtBQUNJLFVBQUEsSUFBSSxFQUFDLFVBRFQ7QUFFSSxVQUFBLEVBQUUsRUFBQyxhQUZQO0FBR0ksVUFBQSxRQUFRLEVBQUVRLHdCQUhkO0FBSUksVUFBQSxRQUFRLEVBQUUsQ0FBQ3ZCLG1CQUpmO0FBS0ksVUFBQSxLQUFLLEVBQUMsRUFMVjtBQU1JLFVBQUEsT0FBTyxFQUFFZ0IsbUJBTmI7QUFPSSxVQUFBLEtBQUssRUFBRUE7QUFQWDtBQURKLFFBRko7QUFBQSxNQTVGSjtBQUFBLElBREo7QUE2R0gsQ0E3SkQ7O0FBK0pBLElBQU1lLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0MvQixFQUFBQSxtQkFBbUIsRUFBRWdDLHFCQUFxQixDQUFDaEMsbUJBREU7QUFFN0NDLEVBQUFBLG1CQUFtQixFQUFFZ0MsU0FBUyxDQUFDQyxzQkFGYztBQUc3Q2hDLEVBQUFBLGNBQWMsRUFBRThCLHFCQUFxQixDQUFDRyxpQkFITztBQUk3Q2hDLEVBQUFBLHdCQUF3QixFQUFFNkIscUJBQXFCLENBQUNJLDJCQUpIO0FBSzdDaEMsRUFBQUEscUJBQXFCLEVBQUU0QixxQkFBcUIsQ0FBQ0ssd0JBTEE7QUFNN0NoQyxFQUFBQSx1QkFBdUIsRUFBRTJCLHFCQUFxQixDQUFDTSwwQkFORjtBQU83Q2hDLEVBQUFBLGlCQUFpQixFQUFFMEIscUJBQXFCLENBQUNPLG9CQVBJO0FBUTdDN0IsRUFBQUEsZ0JBQWdCLEVBQUU4QixxQkFBcUIsQ0FBQ0MsMEJBUks7QUFTN0M5QixFQUFBQSxZQUFZLEVBQUU2QixxQkFBcUIsQ0FBQ0U7QUFUUyxDQUF6QixDQUF4Qjs7QUFZQSxJQUFNQyxrQkFBa0IsR0FBRyxTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUFlO0FBQ3RDN0MsSUFBQUEsY0FBYyxFQUFFO0FBQUEsYUFBTTZDLFFBQVEsQ0FBQ0MsT0FBTyxDQUFDOUMsY0FBUixFQUFELENBQWQ7QUFBQSxLQURzQjtBQUV0Q1EsSUFBQUEsZUFBZSxFQUFFLHlCQUFDdUMsTUFBRCxFQUFTQyxZQUFULEVBQXVCakMsY0FBdkI7QUFBQSxhQUNiOEIsUUFBUSxDQUNKSSxtQkFBbUIsQ0FBQ3pDLGVBQXBCLENBQ0l1QyxNQURKLEVBRUlDLFlBRkosRUFHSWpDLGNBSEosQ0FESSxDQURLO0FBQUEsS0FGcUI7QUFVdENOLElBQUFBLGNBQWMsRUFBRSx3QkFBQ3NDLE1BQUQsRUFBU0csV0FBVCxFQUFzQm5DLGNBQXRCO0FBQUEsYUFDWjhCLFFBQVEsQ0FDSkksbUJBQW1CLENBQUN4QyxjQUFwQixDQUNJc0MsTUFESixFQUVJRyxXQUZKLEVBR0luQyxjQUhKLENBREksQ0FESTtBQUFBLEtBVnNCO0FBa0J0Q0wsSUFBQUEsUUFBUSxFQUFFLGtCQUFDcUMsTUFBRCxFQUFTSSxLQUFULEVBQWdCcEMsY0FBaEI7QUFBQSxhQUNOOEIsUUFBUSxDQUFDSSxtQkFBbUIsQ0FBQ3ZDLFFBQXBCLENBQTZCcUMsTUFBN0IsRUFBcUNJLEtBQXJDLEVBQTRDcEMsY0FBNUMsQ0FBRCxDQURGO0FBQUE7QUFsQjRCLEdBQWY7QUFBQSxDQUEzQjs7QUFzQkFoQixjQUFjLENBQUNxRCxTQUFmLEdBQTJCO0FBQ3ZCQyxFQUFBQSxZQUFZLEVBQUVDLG1CQUFVQyxLQUREO0FBRXZCdkQsRUFBQUEsY0FBYyxFQUFFc0QsbUJBQVVFLElBRkg7QUFHdkJ2RCxFQUFBQSxtQkFBbUIsRUFBRXFELG1CQUFVRyxJQUhSO0FBSXZCdkQsRUFBQUEsbUJBQW1CLEVBQUVvRCxtQkFBVUksTUFKUjtBQUt2QnZELEVBQUFBLGNBQWMsRUFBRW1ELG1CQUFVSyxNQUxIO0FBTXZCdkQsRUFBQUEsd0JBQXdCLEVBQUVrRCxtQkFBVUcsSUFOYjtBQU92QnBELEVBQUFBLHFCQUFxQixFQUFFaUQsbUJBQVVHLElBUFY7QUFRdkJuRCxFQUFBQSx1QkFBdUIsRUFBRWdELG1CQUFVSyxNQVJaO0FBU3ZCcEQsRUFBQUEsaUJBQWlCLEVBQUUrQyxtQkFBVUssTUFUTjtBQVV2Qm5ELEVBQUFBLGVBQWUsRUFBRThDLG1CQUFVRSxJQVZKO0FBV3ZCL0MsRUFBQUEsY0FBYyxFQUFFNkMsbUJBQVVFLElBWEg7QUFZdkI5QyxFQUFBQSxRQUFRLEVBQUU0QyxtQkFBVUUsSUFaRztBQWF2QjdDLEVBQUFBLGdCQUFnQixFQUFFMkMsbUJBQVVLLE1BYkw7QUFjdkIvQyxFQUFBQSxZQUFZLEVBQUUwQyxtQkFBVU0sTUFkRDtBQWV2Qi9DLEVBQUFBLFlBQVksRUFBRXlDLG1CQUFVRztBQWZELENBQTNCOztlQWtCZSx5QkFBUXpCLGVBQVIsRUFBeUJZLGtCQUF6QixFQUE2QzdDLGNBQTdDLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlQ29udGV4dCwgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IENvbCwgSW5wdXRHcm91cCB9IGZyb20gJ3JlYWN0LWJvb3RzdHJhcCc7XG5pbXBvcnQgRm9ybSBmcm9tICdyZWFjdC1ib290c3RyYXAvRm9ybSc7XG5pbXBvcnQgeyB1c2VUcmFuc2xhdGlvbiB9IGZyb20gJ3JlYWN0LWkxOG5leHQnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3RvciB9IGZyb20gJ3Jlc2VsZWN0JztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgQnV0dG9uIGZyb20gJ0Bjb21wb25lbnRzL0J1dHRvbkNvbXBvbmVudCc7XG5pbXBvcnQgeyBGbGV4Um93IH0gZnJvbSAnQGNvbXBvbmVudHMvc3R5bGVkJztcbmltcG9ydCBUaXRsZVdpdGhEZXRhaWwgZnJvbSAnQGNvbXBvbmVudHMvVGl0bGVXaXRoRGV0YWlsJztcblxuaW1wb3J0IHsgR2VuZXJhbENvbnRleHQgfSBmcm9tICdAaGVscGVycy9jb250ZXh0cyc7XG5cbmltcG9ydCAqIGFzIHRlbXBsYXRlVmlld1NlbGVjdG9ycyBmcm9tICdAbW9kdWxlcy9UZW1wbGF0ZVZpZXcvc2VsZWN0b3JzJztcblxuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlQWN0aW9ucyBmcm9tICdAdGVtcGxhdGVDb3JlL2FjdGlvbnMnO1xuaW1wb3J0IHsgVEFCTEVfVEhFTUVTIH0gZnJvbSAnQHRlbXBsYXRlQ29yZS9jb25zdGFudHMnO1xuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlU2VsZWN0b3JzIGZyb20gJ0B0ZW1wbGF0ZUNvcmUvc2VsZWN0b3JzJztcblxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICcuL2FjdGlvbnMnO1xuaW1wb3J0IHsgZ2V0VGVtcGxhdGVMYW5ndWFnZSwgZ2V0VGVtcGxhdGVUeXBlVGV4dCB9IGZyb20gJy4vaGVscGVycyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi9zZWxlY3RvcnMnO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBhZGRpbmc6IDI1cHggMjdweCAyMHB4IDI3cHg7XG4gICAgJHsoeyBkaXNhYmxlZCB9KSA9PlxuICAgICAgICBkaXNhYmxlZCAmJlxuICAgICAgICBgb3BhY2l0eTogMC42O1xuICAgICAgICBjdXJzb3I6IG5vdC1hbGxvd2VkO1xuICAgICAgICBwb2ludGVyLWV2ZW50czogbm9uZTtgfVxuYDtcblxuY29uc3QgSW5zdHJ1Y3Rpb25UZXh0ID0gc3R5bGVkLnNwYW5gXG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi10b3A6IDE0cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxMnB4O1xuYDtcblxuY29uc3QgVGhlbWVTZWxlY3RDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgbWFyZ2luLXRvcDogMTdweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XG5gO1xuXG5jb25zdCBUZW1wbGF0ZVByb3BpZXJ0eUNvbnRhaW5lciA9IHN0eWxlZChGbGV4Um93KWBcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XG4gICAgbWFyZ2luLXRvcDogMzJweDtcbmA7XG5cbmNvbnN0IExhYmVsVGV4dCA9IHN0eWxlZC5zcGFuYFxuICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMTVweDtcbiAgICBtaW4taGVpZ2h0OiAxNXB4O1xuYDtcblxuY29uc3QgVGVtcGxhdGVQcm9waWVydHlUZXh0ID0gc3R5bGVkLnBgXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDEycHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDAuMjVweDtcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuYDtcblxuY29uc3QgU3R5bGVkSW5wdXRHcm91cCA9IHN0eWxlZChJbnB1dEdyb3VwKWBcbiAgICBoZWlnaHQ6IDMycHg7XG4gICAgd2lkdGg6ICR7KHsgd2lkdGggPSAnMTkwcHgnIH0pID0+IHdpZHRofTtcblxuICAgICYgc2VsZWN0IHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZWNlZGYwO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGhlaWdodDogMzJweDtcbiAgICAgICAgd2lkdGg6ICR7KHsgd2lkdGggPSAnMTkwcHgnIH0pID0+IHdpZHRofTtcbiAgICB9XG5gO1xuXG5jb25zdCBQYWdlUHJvcGVydGllcyA9ICh7XG4gICAgZmV0Y2hMYW5ndWFnZXMsXG4gICAgaXNJbmZpbml0ZU1vZGVWYWxpZCxcbiAgICBsYW5ndWFnZXNEaWN0aW9uYXJ5LFxuICAgIHNlbGVjdGVkUGFnZUlkLFxuICAgIHNlbGVjdGVkUGFnZUluZmluaXRlTW9kZSxcbiAgICBzZWxlY3RlZFBhZ2VJc0Jsb2NrZWQsXG4gICAgc2VsZWN0ZWRQYWdlT3JpZW50YXRpb24sXG4gICAgc2VsZWN0ZWRQYWdlVGhlbWUsXG4gICAgc2V0SW5maW5pdGVNb2RlLFxuICAgIHNldE9yaWVudGF0aW9uLFxuICAgIHNldFRoZW1lLFxuICAgIHRlbXBsYXRlTGFuZ3VhZ2UsXG4gICAgdGVtcGxhdGVUeXBlLFxuICAgIHRoZW1lRW5hYmxlZCA9IHRydWUsXG59KSA9PiB7XG4gICAgY29uc3QgeyBvbkNoYW5nZUluUGFnZSB9ID0gdXNlQ29udGV4dChHZW5lcmFsQ29udGV4dCk7XG4gICAgY29uc3QgeyB0IH0gPSB1c2VUcmFuc2xhdGlvbigpO1xuXG4gICAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICAgICAgZmV0Y2hMYW5ndWFnZXMoKTtcbiAgICB9LCBbXSk7XG5cbiAgICBjb25zdCBbaW5maW5pdGVNb2RlQ29udHJvbCwgc2V0SW5maW5pdGVNb2RlQ29udHJvbF0gPSB1c2VTdGF0ZShcbiAgICAgICAgc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlXG4gICAgKTtcblxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGUgIT09IGluZmluaXRlTW9kZUNvbnRyb2wpIHtcbiAgICAgICAgICAgIHNldEluZmluaXRlTW9kZUNvbnRyb2woc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlKTtcbiAgICAgICAgfVxuICAgIH0sIFtzZWxlY3RlZFBhZ2VJbmZpbml0ZU1vZGVdKTtcblxuICAgIGNvbnN0IG9uQ2hhbmdlVGhlbWVTZWxlY3QgPSAoZSkgPT4ge1xuICAgICAgICBzZXRUaGVtZShzZWxlY3RlZFBhZ2VJZCwgZS50YXJnZXQudmFsdWUsIG9uQ2hhbmdlSW5QYWdlKTtcbiAgICB9O1xuXG4gICAgY29uc3Qgb25DaGFuZ2VPcmllbnRhdGlvblNlbGVjdCA9IChlKSA9PiB7XG4gICAgICAgIHNldE9yaWVudGF0aW9uKHNlbGVjdGVkUGFnZUlkLCBlLnRhcmdldC52YWx1ZSwgb25DaGFuZ2VJblBhZ2UpO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVDaGFuZ2VJbmZpbml0ZU1vZGUgPSAoZSkgPT4ge1xuICAgICAgICBzZXRJbmZpbml0ZU1vZGUoc2VsZWN0ZWRQYWdlSWQsIGUudGFyZ2V0LmNoZWNrZWQsIG9uQ2hhbmdlSW5QYWdlKTtcbiAgICB9O1xuXG4gICAgaWYgKCF0aGVtZUVuYWJsZWQpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXIgZGlzYWJsZWQ9e3NlbGVjdGVkUGFnZUlzQmxvY2tlZH0+XG4gICAgICAgICAgICA8VGl0bGVXaXRoRGV0YWlsIGNsYXNzTmFtZT1cIm1iLTBcIj5cbiAgICAgICAgICAgICAgICB7dCgnUHJvcGVydGllcycpfVxuICAgICAgICAgICAgPC9UaXRsZVdpdGhEZXRhaWw+XG5cbiAgICAgICAgICAgIDxUZW1wbGF0ZVByb3BpZXJ0eUNvbnRhaW5lciBhbGlnbkl0ZW1zPVwiY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgPENvbCB4cz1cIjYgcGwtMFwiPlxuICAgICAgICAgICAgICAgICAgICA8VGVtcGxhdGVQcm9waWVydHlUZXh0PlxuICAgICAgICAgICAgICAgICAgICAgICAge3QoJ0xhbmd1YWdlJyl9OlxuICAgICAgICAgICAgICAgICAgICA8L1RlbXBsYXRlUHJvcGllcnR5VGV4dD5cbiAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICA8Q29sIHhzPVwiMTIgcGwtMFwiPlxuICAgICAgICAgICAgICAgICAgICA8VGVtcGxhdGVQcm9waWVydHlUZXh0PlxuICAgICAgICAgICAgICAgICAgICAgICAge2dldFRlbXBsYXRlTGFuZ3VhZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVMYW5ndWFnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYW5ndWFnZXNEaWN0aW9uYXJ5XG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICA8L1RlbXBsYXRlUHJvcGllcnR5VGV4dD5cbiAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDwvVGVtcGxhdGVQcm9waWVydHlDb250YWluZXI+XG5cbiAgICAgICAgICAgIDxUZW1wbGF0ZVByb3BpZXJ0eUNvbnRhaW5lciBhbGlnbkl0ZW1zPVwiY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgPENvbCB4cz1cIjYgcGwtMFwiPlxuICAgICAgICAgICAgICAgICAgICA8VGVtcGxhdGVQcm9waWVydHlUZXh0PlxuICAgICAgICAgICAgICAgICAgICAgICAge3QoJ1RlbXBsYXRlIHR5cGUnKX06XG4gICAgICAgICAgICAgICAgICAgIDwvVGVtcGxhdGVQcm9waWVydHlUZXh0PlxuICAgICAgICAgICAgICAgIDwvQ29sPlxuICAgICAgICAgICAgICAgIDxDb2wgeHM9XCIxMiBwbC0wXCI+XG4gICAgICAgICAgICAgICAgICAgIDxUZW1wbGF0ZVByb3BpZXJ0eVRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Z2V0VGVtcGxhdGVUeXBlVGV4dCh0ZW1wbGF0ZVR5cGUpfVxuICAgICAgICAgICAgICAgICAgICA8L1RlbXBsYXRlUHJvcGllcnR5VGV4dD5cbiAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgIDwvVGVtcGxhdGVQcm9waWVydHlDb250YWluZXI+XG5cbiAgICAgICAgICAgIDxGbGV4Um93IGp1c3RpZnlDb250ZW50PVwiY2VudGVyXCIgY2xhc3NOYW1lPVwibXQtMlwiPlxuICAgICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICAgICAgYXM9XCJhXCJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXQtMiB0ZXh0LWxlZnRcIlxuICAgICAgICAgICAgICAgICAgICBmb250U2l6ZT1cIjEycHhcIlxuICAgICAgICAgICAgICAgICAgICBmb250V2VpZ2h0PVwiYm9sZFwiXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwOi8vYWNhZGVteS5zdW53aXNlLm14L2VzL2FydGljbGVzLzU1NDcxNTktcmVzdW1lbi1kZS1wcm9wdWVzdGFzXCJcbiAgICAgICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgICAgICAgICAgdGV4dENvbG9yPVwiIzFGM0M1M1wiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICB7dCgnU2VlIG1hbnVhbCcpfVxuICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9GbGV4Um93PlxuXG4gICAgICAgICAgICA8SW5zdHJ1Y3Rpb25UZXh0PlxuICAgICAgICAgICAgICAgIHt0KFxuICAgICAgICAgICAgICAgICAgICAnVGhpcyB0aGVtZSB3aWxsIGJlIHVzZWQgaW4gdGhlIG5ldyBjb21wb25lbnRzIG9mIHlvdXIgdGVtcGxhdGUuJ1xuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L0luc3RydWN0aW9uVGV4dD5cblxuICAgICAgICAgICAgPFRoZW1lU2VsZWN0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbFRleHQ+e3QoJ1RoZW1lJyl9OjwvTGFiZWxUZXh0PlxuXG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNvbnRyb2xcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtzZWxlY3RlZFBhZ2VUaGVtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZVRoZW1lU2VsZWN0fVxuICAgICAgICAgICAgICAgICAgICAgICAgYXM9XCJzZWxlY3RcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJzZWxlY3QtdGhlbWVcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7T2JqZWN0LmtleXMoVEFCTEVfVEhFTUVTKS5tYXAoKGtleSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleT17YHNlbGVjdC0ke2tleX0tJHtUQUJMRV9USEVNRVNba2V5XS5uYW1lfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17a2V5fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7VEFCTEVfVEhFTUVTW2tleV0ubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cbiAgICAgICAgICAgICAgICA8L1N0eWxlZElucHV0R3JvdXA+XG4gICAgICAgICAgICA8L1RoZW1lU2VsZWN0Q29udGFpbmVyPlxuICAgICAgICAgICAgPFRoZW1lU2VsZWN0Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgIDxMYWJlbFRleHQ+e3QoJ09yaWVudGF0aW9uJyl9OjwvTGFiZWxUZXh0PlxuXG4gICAgICAgICAgICAgICAgPFN0eWxlZElucHV0R3JvdXAgd2lkdGg9XCIxNjBweFwiPlxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17c2VsZWN0ZWRQYWdlT3JpZW50YXRpb259XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2VPcmllbnRhdGlvblNlbGVjdH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGFzPVwic2VsZWN0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic2VsZWN0LW9yaWVudGF0aW9uXCJcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbiB2YWx1ZT1cInBvcnRyYWl0XCI+e3QoJ1ZlcnRpY2FsJyl9PC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwibGFuZHNjYXBlXCI+e3QoJ0hvcml6b250YWwnKX08L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgPC9Gb3JtLkNvbnRyb2w+XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJbnB1dEdyb3VwPlxuICAgICAgICAgICAgPC9UaGVtZVNlbGVjdENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxUaGVtZVNlbGVjdENvbnRhaW5lciBjbGFzc05hbWU9XCJ0ZXh0LXRvcFwiPlxuICAgICAgICAgICAgICAgIDxMYWJlbFRleHQ+e3QoJ0R5bmFtaWMgY29udGVudCcpfTo8L0xhYmVsVGV4dD5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSW5wdXRHcm91cCB3aWR0aD1cIjE2MHB4XCI+XG4gICAgICAgICAgICAgICAgICAgIDxGb3JtLkNoZWNrXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9XCJpbmZpdGUtbW9kZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17aGFuZGxlQ2hhbmdlSW5maW5pdGVNb2RlfVxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyFpc0luZmluaXRlTW9kZVZhbGlkfVxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZD17aW5maW5pdGVNb2RlQ29udHJvbH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXtpbmZpbml0ZU1vZGVDb250cm9sfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvU3R5bGVkSW5wdXRHcm91cD5cbiAgICAgICAgICAgIDwvVGhlbWVTZWxlY3RDb250YWluZXI+XG4gICAgICAgIDwvQ29udGFpbmVyPlxuICAgICk7XG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3Ioe1xuICAgIGlzSW5maW5pdGVNb2RlVmFsaWQ6IHRlbXBsYXRlVmlld1NlbGVjdG9ycy5pc0luZmluaXRlTW9kZVZhbGlkLFxuICAgIGxhbmd1YWdlc0RpY3Rpb25hcnk6IHNlbGVjdG9ycy5nZXRMYW5ndWFnZXNEaWN0aW9uYXJ5LFxuICAgIHNlbGVjdGVkUGFnZUlkOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlSWQsXG4gICAgc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlLFxuICAgIHNlbGVjdGVkUGFnZUlzQmxvY2tlZDogdGVtcGxhdGVWaWV3U2VsZWN0b3JzLmdldFNlbGVjdGVkUGFnZUlzQmxvY2tlZCxcbiAgICBzZWxlY3RlZFBhZ2VPcmllbnRhdGlvbjogdGVtcGxhdGVWaWV3U2VsZWN0b3JzLmdldFNlbGVjdGVkUGFnZU9yaWVudGF0aW9uLFxuICAgIHNlbGVjdGVkUGFnZVRoZW1lOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlVGhlbWUsXG4gICAgdGVtcGxhdGVMYW5ndWFnZTogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZUxhbmd1YWdlLFxuICAgIHRlbXBsYXRlVHlwZTogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVR5cGUsXG59KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiAoe1xuICAgIGZldGNoTGFuZ3VhZ2VzOiAoKSA9PiBkaXNwYXRjaChhY3Rpb25zLmZldGNoTGFuZ3VhZ2VzKCkpLFxuICAgIHNldEluZmluaXRlTW9kZTogKHBhZ2VJZCwgaW5maW5pdGVNb2RlLCBvbkNoYW5nZUluUGFnZSkgPT5cbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldEluZmluaXRlTW9kZShcbiAgICAgICAgICAgICAgICBwYWdlSWQsXG4gICAgICAgICAgICAgICAgaW5maW5pdGVNb2RlLFxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlSW5QYWdlXG4gICAgICAgICAgICApXG4gICAgICAgICksXG4gICAgc2V0T3JpZW50YXRpb246IChwYWdlSWQsIG9yaWVudGF0aW9uLCBvbkNoYW5nZUluUGFnZSkgPT5cbiAgICAgICAgZGlzcGF0Y2goXG4gICAgICAgICAgICB0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldE9yaWVudGF0aW9uKFxuICAgICAgICAgICAgICAgIHBhZ2VJZCxcbiAgICAgICAgICAgICAgICBvcmllbnRhdGlvbixcbiAgICAgICAgICAgICAgICBvbkNoYW5nZUluUGFnZVxuICAgICAgICAgICAgKVxuICAgICAgICApLFxuICAgIHNldFRoZW1lOiAocGFnZUlkLCB0aGVtZSwgb25DaGFuZ2VJblBhZ2UpID0+XG4gICAgICAgIGRpc3BhdGNoKHRlbXBsYXRlQ29yZUFjdGlvbnMuc2V0VGhlbWUocGFnZUlkLCB0aGVtZSwgb25DaGFuZ2VJblBhZ2UpKSxcbn0pO1xuXG5QYWdlUHJvcGVydGllcy5wcm9wVHlwZXMgPSB7XG4gICAgY2hhcnRXaWRnZXRzOiBQcm9wVHlwZXMuYXJyYXksXG4gICAgZmV0Y2hMYW5ndWFnZXM6IFByb3BUeXBlcy5mdW5jLFxuICAgIGlzSW5maW5pdGVNb2RlVmFsaWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGxhbmd1YWdlc0RpY3Rpb25hcnk6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgc2VsZWN0ZWRQYWdlSWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2VsZWN0ZWRQYWdlSW5maW5pdGVNb2RlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBzZWxlY3RlZFBhZ2VJc0Jsb2NrZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHNlbGVjdGVkUGFnZU9yaWVudGF0aW9uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlbGVjdGVkUGFnZVRoZW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNldEluZmluaXRlTW9kZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2V0T3JpZW50YXRpb246IFByb3BUeXBlcy5mdW5jLFxuICAgIHNldFRoZW1lOiBQcm9wVHlwZXMuZnVuYyxcbiAgICB0ZW1wbGF0ZUxhbmd1YWdlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHRlbXBsYXRlVHlwZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICB0aGVtZUVuYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoUGFnZVByb3BlcnRpZXMpO1xuIl19