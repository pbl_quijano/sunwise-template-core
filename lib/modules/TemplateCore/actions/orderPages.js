"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _arrayMove = _interopRequireDefault(require("array-move"));

var templateCoreSelectors = _interopRequireWildcard(require("../selectors"));

var _actionTypes = require("../actionTypes");

var _helpers = require("../helpers");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(oldIndex, newIndex) {
  return function (dispatch, getState) {
    var currentPages = templateCoreSelectors.getCurrentTemplatePages(getState());
    var newPages = (0, _helpers.orderPages)((0, _arrayMove.default)(currentPages, oldIndex, newIndex));
    dispatch({
      type: _actionTypes.RE_ORDER_PAGES,
      payload: newPages
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL29yZGVyUGFnZXMuanMiXSwibmFtZXMiOlsib2xkSW5kZXgiLCJuZXdJbmRleCIsImRpc3BhdGNoIiwiZ2V0U3RhdGUiLCJjdXJyZW50UGFnZXMiLCJ0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMiLCJnZXRDdXJyZW50VGVtcGxhdGVQYWdlcyIsIm5ld1BhZ2VzIiwidHlwZSIsIlJFX09SREVSX1BBR0VTIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBRUE7O0FBRUE7O0FBQ0E7Ozs7Ozs7O2VBRWUsa0JBQUNBLFFBQUQsRUFBV0MsUUFBWDtBQUFBLFNBQXdCLFVBQUNDLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUMzRCxRQUFNQyxZQUFZLEdBQUdDLHFCQUFxQixDQUFDQyx1QkFBdEIsQ0FDakJILFFBQVEsRUFEUyxDQUFyQjtBQUdBLFFBQU1JLFFBQVEsR0FBRyx5QkFBVyx3QkFBVUgsWUFBVixFQUF3QkosUUFBeEIsRUFBa0NDLFFBQWxDLENBQVgsQ0FBakI7QUFDQUMsSUFBQUEsUUFBUSxDQUFDO0FBQ0xNLE1BQUFBLElBQUksRUFBRUMsMkJBREQ7QUFFTEMsTUFBQUEsT0FBTyxFQUFFSDtBQUZKLEtBQUQsQ0FBUjtBQUlILEdBVGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGFycmF5TW92ZSBmcm9tICdhcnJheS1tb3ZlJztcclxuXHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBSRV9PUkRFUl9QQUdFUyB9IGZyb20gJy4uL2FjdGlvblR5cGVzJztcclxuaW1wb3J0IHsgb3JkZXJQYWdlcyB9IGZyb20gJy4uL2hlbHBlcnMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgKG9sZEluZGV4LCBuZXdJbmRleCkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gICAgY29uc3QgY3VycmVudFBhZ2VzID0gdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzKFxyXG4gICAgICAgIGdldFN0YXRlKClcclxuICAgICk7XHJcbiAgICBjb25zdCBuZXdQYWdlcyA9IG9yZGVyUGFnZXMoYXJyYXlNb3ZlKGN1cnJlbnRQYWdlcywgb2xkSW5kZXgsIG5ld0luZGV4KSk7XHJcbiAgICBkaXNwYXRjaCh7XHJcbiAgICAgICAgdHlwZTogUkVfT1JERVJfUEFHRVMsXHJcbiAgICAgICAgcGF5bG9hZDogbmV3UGFnZXMsXHJcbiAgICB9KTtcclxufTtcclxuIl19