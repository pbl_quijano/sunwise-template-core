"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _selectPage = _interopRequireDefault(require("../../TemplateView/actions/selectPage"));

var templateViewSelectors = _interopRequireWildcard(require("../../TemplateView/selectors"));

var templateCoreSelectors = _interopRequireWildcard(require("../selectors"));

var _actionTypes = require("../actionTypes");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var _default = function _default(deletingId, pagesTemplate) {
  return function (dispatch, getState) {
    var state = getState();
    var selectedPage = templateViewSelectors.getSelectedPage(state);

    if (deletingId === selectedPage.groupId || deletingId === selectedPage.id) {
      var currentPages = templateCoreSelectors.getCurrentTemplatePages(state);
      var pagesIds = currentPages.reduce(function (acc, current) {
        if (current.type === 'group') {
          return [].concat(_toConsumableArray(acc), _toConsumableArray(current.pages.map(function (p) {
            return p.id;
          })));
        }

        return [].concat(_toConsumableArray(acc), [current.id]);
      }, []);
      var selectedPageIndex = 0;

      if (selectedPage.groupId) {
        var selectedGroupIndex = currentPages.map(function (p) {
          return p.id;
        }).indexOf(selectedPage.groupId);

        if (selectedGroupIndex === 0) {
          selectedPageIndex = 0;
        } else {
          selectedPageIndex = pagesIds.indexOf(currentPages[selectedGroupIndex].pages[0].id);
        }
      } else {
        selectedPageIndex = pagesIds.indexOf(selectedPage.id);
      }

      if (selectedPageIndex === 0) {
        if (!selectedPage.groupId && pagesIds.length > 1) {
          dispatch((0, _selectPage.default)(pagesIds[1]));
        } else {
          dispatch((0, _selectPage.default)(null));
        }
      } else {
        dispatch((0, _selectPage.default)(pagesIds[selectedPageIndex - 1]));
      }
    }

    dispatch({
      type: _actionTypes.DELETE_PAGES,
      payload: pagesTemplate
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL2RlbGV0ZVBhZ2VzLmpzIl0sIm5hbWVzIjpbImRlbGV0aW5nSWQiLCJwYWdlc1RlbXBsYXRlIiwiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInN0YXRlIiwic2VsZWN0ZWRQYWdlIiwidGVtcGxhdGVWaWV3U2VsZWN0b3JzIiwiZ2V0U2VsZWN0ZWRQYWdlIiwiZ3JvdXBJZCIsImlkIiwiY3VycmVudFBhZ2VzIiwidGVtcGxhdGVDb3JlU2VsZWN0b3JzIiwiZ2V0Q3VycmVudFRlbXBsYXRlUGFnZXMiLCJwYWdlc0lkcyIsInJlZHVjZSIsImFjYyIsImN1cnJlbnQiLCJ0eXBlIiwicGFnZXMiLCJtYXAiLCJwIiwic2VsZWN0ZWRQYWdlSW5kZXgiLCJzZWxlY3RlZEdyb3VwSW5kZXgiLCJpbmRleE9mIiwibGVuZ3RoIiwiREVMRVRFX1BBR0VTIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2VBRWUsa0JBQUNBLFVBQUQsRUFBYUMsYUFBYjtBQUFBLFNBQStCLFVBQUNDLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUNsRSxRQUFNQyxLQUFLLEdBQUdELFFBQVEsRUFBdEI7QUFDQSxRQUFNRSxZQUFZLEdBQUdDLHFCQUFxQixDQUFDQyxlQUF0QixDQUFzQ0gsS0FBdEMsQ0FBckI7O0FBQ0EsUUFBSUosVUFBVSxLQUFLSyxZQUFZLENBQUNHLE9BQTVCLElBQXVDUixVQUFVLEtBQUtLLFlBQVksQ0FBQ0ksRUFBdkUsRUFBMkU7QUFDdkUsVUFBTUMsWUFBWSxHQUNkQyxxQkFBcUIsQ0FBQ0MsdUJBQXRCLENBQThDUixLQUE5QyxDQURKO0FBRUEsVUFBTVMsUUFBUSxHQUFHSCxZQUFZLENBQUNJLE1BQWIsQ0FBb0IsVUFBQ0MsR0FBRCxFQUFNQyxPQUFOLEVBQWtCO0FBQ25ELFlBQUlBLE9BQU8sQ0FBQ0MsSUFBUixLQUFpQixPQUFyQixFQUE4QjtBQUMxQiw4Q0FBV0YsR0FBWCxzQkFBbUJDLE9BQU8sQ0FBQ0UsS0FBUixDQUFjQyxHQUFkLENBQWtCLFVBQUNDLENBQUQ7QUFBQSxtQkFBT0EsQ0FBQyxDQUFDWCxFQUFUO0FBQUEsV0FBbEIsQ0FBbkI7QUFDSDs7QUFDRCw0Q0FBV00sR0FBWCxJQUFnQkMsT0FBTyxDQUFDUCxFQUF4QjtBQUNILE9BTGdCLEVBS2QsRUFMYyxDQUFqQjtBQU1BLFVBQUlZLGlCQUFpQixHQUFHLENBQXhCOztBQUNBLFVBQUloQixZQUFZLENBQUNHLE9BQWpCLEVBQTBCO0FBQ3RCLFlBQUljLGtCQUFrQixHQUFHWixZQUFZLENBQ2hDUyxHQURvQixDQUNoQixVQUFDQyxDQUFEO0FBQUEsaUJBQU9BLENBQUMsQ0FBQ1gsRUFBVDtBQUFBLFNBRGdCLEVBRXBCYyxPQUZvQixDQUVabEIsWUFBWSxDQUFDRyxPQUZELENBQXpCOztBQUdBLFlBQUljLGtCQUFrQixLQUFLLENBQTNCLEVBQThCO0FBQzFCRCxVQUFBQSxpQkFBaUIsR0FBRyxDQUFwQjtBQUNILFNBRkQsTUFFTztBQUNIQSxVQUFBQSxpQkFBaUIsR0FBR1IsUUFBUSxDQUFDVSxPQUFULENBQ2hCYixZQUFZLENBQUNZLGtCQUFELENBQVosQ0FBaUNKLEtBQWpDLENBQXVDLENBQXZDLEVBQTBDVCxFQUQxQixDQUFwQjtBQUdIO0FBQ0osT0FYRCxNQVdPO0FBQ0hZLFFBQUFBLGlCQUFpQixHQUFHUixRQUFRLENBQUNVLE9BQVQsQ0FBaUJsQixZQUFZLENBQUNJLEVBQTlCLENBQXBCO0FBQ0g7O0FBQ0QsVUFBSVksaUJBQWlCLEtBQUssQ0FBMUIsRUFBNkI7QUFDekIsWUFBSSxDQUFDaEIsWUFBWSxDQUFDRyxPQUFkLElBQXlCSyxRQUFRLENBQUNXLE1BQVQsR0FBa0IsQ0FBL0MsRUFBa0Q7QUFDOUN0QixVQUFBQSxRQUFRLENBQUMseUJBQVdXLFFBQVEsQ0FBQyxDQUFELENBQW5CLENBQUQsQ0FBUjtBQUNILFNBRkQsTUFFTztBQUNIWCxVQUFBQSxRQUFRLENBQUMseUJBQVcsSUFBWCxDQUFELENBQVI7QUFDSDtBQUNKLE9BTkQsTUFNTztBQUNIQSxRQUFBQSxRQUFRLENBQUMseUJBQVdXLFFBQVEsQ0FBQ1EsaUJBQWlCLEdBQUcsQ0FBckIsQ0FBbkIsQ0FBRCxDQUFSO0FBQ0g7QUFDSjs7QUFDRG5CLElBQUFBLFFBQVEsQ0FBQztBQUNMZSxNQUFBQSxJQUFJLEVBQUVRLHlCQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRXpCO0FBRkosS0FBRCxDQUFSO0FBSUgsR0F6Q2M7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHNlbGVjdFBhZ2UgZnJvbSAnQG1vZHVsZXMvVGVtcGxhdGVWaWV3L2FjdGlvbnMvc2VsZWN0UGFnZSc7XHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlVmlld1NlbGVjdG9ycyBmcm9tICdAbW9kdWxlcy9UZW1wbGF0ZVZpZXcvc2VsZWN0b3JzJztcclxuXHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlQ29yZVNlbGVjdG9ycyBmcm9tICdAdGVtcGxhdGVDb3JlL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBERUxFVEVfUEFHRVMgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCAoZGVsZXRpbmdJZCwgcGFnZXNUZW1wbGF0ZSkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gICAgY29uc3Qgc3RhdGUgPSBnZXRTdGF0ZSgpO1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRQYWdlID0gdGVtcGxhdGVWaWV3U2VsZWN0b3JzLmdldFNlbGVjdGVkUGFnZShzdGF0ZSk7XHJcbiAgICBpZiAoZGVsZXRpbmdJZCA9PT0gc2VsZWN0ZWRQYWdlLmdyb3VwSWQgfHwgZGVsZXRpbmdJZCA9PT0gc2VsZWN0ZWRQYWdlLmlkKSB7XHJcbiAgICAgICAgY29uc3QgY3VycmVudFBhZ2VzID1cclxuICAgICAgICAgICAgdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzKHN0YXRlKTtcclxuICAgICAgICBjb25zdCBwYWdlc0lkcyA9IGN1cnJlbnRQYWdlcy5yZWR1Y2UoKGFjYywgY3VycmVudCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoY3VycmVudC50eXBlID09PSAnZ3JvdXAnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gWy4uLmFjYywgLi4uY3VycmVudC5wYWdlcy5tYXAoKHApID0+IHAuaWQpXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gWy4uLmFjYywgY3VycmVudC5pZF07XHJcbiAgICAgICAgfSwgW10pO1xyXG4gICAgICAgIGxldCBzZWxlY3RlZFBhZ2VJbmRleCA9IDA7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkUGFnZS5ncm91cElkKSB7XHJcbiAgICAgICAgICAgIGxldCBzZWxlY3RlZEdyb3VwSW5kZXggPSBjdXJyZW50UGFnZXNcclxuICAgICAgICAgICAgICAgIC5tYXAoKHApID0+IHAuaWQpXHJcbiAgICAgICAgICAgICAgICAuaW5kZXhPZihzZWxlY3RlZFBhZ2UuZ3JvdXBJZCk7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZEdyb3VwSW5kZXggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkUGFnZUluZGV4ID0gMDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkUGFnZUluZGV4ID0gcGFnZXNJZHMuaW5kZXhPZihcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50UGFnZXNbc2VsZWN0ZWRHcm91cEluZGV4XS5wYWdlc1swXS5pZFxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkUGFnZUluZGV4ID0gcGFnZXNJZHMuaW5kZXhPZihzZWxlY3RlZFBhZ2UuaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoc2VsZWN0ZWRQYWdlSW5kZXggPT09IDApIHtcclxuICAgICAgICAgICAgaWYgKCFzZWxlY3RlZFBhZ2UuZ3JvdXBJZCAmJiBwYWdlc0lkcy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwYXRjaChzZWxlY3RQYWdlKHBhZ2VzSWRzWzFdKSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkaXNwYXRjaChzZWxlY3RQYWdlKG51bGwpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGRpc3BhdGNoKHNlbGVjdFBhZ2UocGFnZXNJZHNbc2VsZWN0ZWRQYWdlSW5kZXggLSAxXSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGRpc3BhdGNoKHtcclxuICAgICAgICB0eXBlOiBERUxFVEVfUEFHRVMsXHJcbiAgICAgICAgcGF5bG9hZDogcGFnZXNUZW1wbGF0ZSxcclxuICAgIH0pO1xyXG59O1xyXG4iXX0=