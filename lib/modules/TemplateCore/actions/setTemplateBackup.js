"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var selectors = _interopRequireWildcard(require("../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default(clean) {
  return function (dispatch, getState) {
    if (clean) {
      dispatch({
        type: _actionTypes.SET_TEMPLATE_BACKUP,
        payload: null
      });
    } else {
      var currentPages = selectors.getCurrentTemplatePages(getState());
      dispatch({
        type: _actionTypes.SET_TEMPLATE_BACKUP,
        payload: JSON.stringify(currentPages.filter(function (p) {
          return p.type === 'page';
        }))
      });
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3NldFRlbXBsYXRlQmFja3VwLmpzIl0sIm5hbWVzIjpbImNsZWFuIiwiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInR5cGUiLCJTRVRfVEVNUExBVEVfQkFDS1VQIiwicGF5bG9hZCIsImN1cnJlbnRQYWdlcyIsInNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzIiwiSlNPTiIsInN0cmluZ2lmeSIsImZpbHRlciIsInAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7ZUFFZSxrQkFBQ0EsS0FBRDtBQUFBLFNBQVcsVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQzlDLFFBQUlGLEtBQUosRUFBVztBQUNQQyxNQUFBQSxRQUFRLENBQUM7QUFDTEUsUUFBQUEsSUFBSSxFQUFFQyxnQ0FERDtBQUVMQyxRQUFBQSxPQUFPLEVBQUU7QUFGSixPQUFELENBQVI7QUFJSCxLQUxELE1BS087QUFDSCxVQUFNQyxZQUFZLEdBQUdDLFNBQVMsQ0FBQ0MsdUJBQVYsQ0FBa0NOLFFBQVEsRUFBMUMsQ0FBckI7QUFDQUQsTUFBQUEsUUFBUSxDQUFDO0FBQ0xFLFFBQUFBLElBQUksRUFBRUMsZ0NBREQ7QUFFTEMsUUFBQUEsT0FBTyxFQUFFSSxJQUFJLENBQUNDLFNBQUwsQ0FDTEosWUFBWSxDQUFDSyxNQUFiLENBQW9CLFVBQUNDLENBQUQ7QUFBQSxpQkFBT0EsQ0FBQyxDQUFDVCxJQUFGLEtBQVcsTUFBbEI7QUFBQSxTQUFwQixDQURLO0FBRkosT0FBRCxDQUFSO0FBTUg7QUFDSixHQWZjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9URU1QTEFURV9CQUNLVVAgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XHJcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi9zZWxlY3RvcnMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgKGNsZWFuKSA9PiAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XHJcbiAgICBpZiAoY2xlYW4pIHtcclxuICAgICAgICBkaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIHR5cGU6IFNFVF9URU1QTEFURV9CQUNLVVAsXHJcbiAgICAgICAgICAgIHBheWxvYWQ6IG51bGwsXHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRQYWdlcyA9IHNlbGVjdG9ycy5nZXRDdXJyZW50VGVtcGxhdGVQYWdlcyhnZXRTdGF0ZSgpKTtcclxuICAgICAgICBkaXNwYXRjaCh7XHJcbiAgICAgICAgICAgIHR5cGU6IFNFVF9URU1QTEFURV9CQUNLVVAsXHJcbiAgICAgICAgICAgIHBheWxvYWQ6IEpTT04uc3RyaW5naWZ5KFxyXG4gICAgICAgICAgICAgICAgY3VycmVudFBhZ2VzLmZpbHRlcigocCkgPT4gcC50eXBlID09PSAncGFnZScpXHJcbiAgICAgICAgICAgICksXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn07XHJcbiJdfQ==