"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "addNewPages", {
  enumerable: true,
  get: function get() {
    return _addNewPages.default;
  }
});
Object.defineProperty(exports, "addWidget", {
  enumerable: true,
  get: function get() {
    return _addWidget.default;
  }
});
Object.defineProperty(exports, "deletePages", {
  enumerable: true,
  get: function get() {
    return _deletePages.default;
  }
});
Object.defineProperty(exports, "deleteWidget", {
  enumerable: true,
  get: function get() {
    return _deleteWidget.default;
  }
});
Object.defineProperty(exports, "duplicatePage", {
  enumerable: true,
  get: function get() {
    return _duplicatePage.default;
  }
});
Object.defineProperty(exports, "orderPages", {
  enumerable: true,
  get: function get() {
    return _orderPages.default;
  }
});
Object.defineProperty(exports, "pasteWidget", {
  enumerable: true,
  get: function get() {
    return _pasteWidget.default;
  }
});
Object.defineProperty(exports, "resetTemplate", {
  enumerable: true,
  get: function get() {
    return _resetTemplate.default;
  }
});
Object.defineProperty(exports, "restorePages", {
  enumerable: true,
  get: function get() {
    return _restorePages.default;
  }
});
Object.defineProperty(exports, "setInfiniteMode", {
  enumerable: true,
  get: function get() {
    return _setInfiniteMode.default;
  }
});
Object.defineProperty(exports, "setOrientation", {
  enumerable: true,
  get: function get() {
    return _setOrientation.default;
  }
});
Object.defineProperty(exports, "setTemplateBackup", {
  enumerable: true,
  get: function get() {
    return _setTemplateBackup.default;
  }
});
Object.defineProperty(exports, "setTheme", {
  enumerable: true,
  get: function get() {
    return _setTheme.default;
  }
});
Object.defineProperty(exports, "setWidgetOrder", {
  enumerable: true,
  get: function get() {
    return _setWidgetOrder.default;
  }
});
Object.defineProperty(exports, "setWidgetPosition", {
  enumerable: true,
  get: function get() {
    return _setPosition.default;
  }
});
Object.defineProperty(exports, "setWidgetSize", {
  enumerable: true,
  get: function get() {
    return _setSize.default;
  }
});
Object.defineProperty(exports, "setWidgetStyle", {
  enumerable: true,
  get: function get() {
    return _setStyle.default;
  }
});
Object.defineProperty(exports, "setWidgetValue", {
  enumerable: true,
  get: function get() {
    return _setValue.default;
  }
});

var _addNewPages = _interopRequireDefault(require("./addNewPages"));

var _addWidget = _interopRequireDefault(require("./pageChanges/addWidget"));

var _deletePages = _interopRequireDefault(require("./deletePages"));

var _deleteWidget = _interopRequireDefault(require("./widgetChanges/deleteWidget"));

var _duplicatePage = _interopRequireDefault(require("./duplicatePage"));

var _orderPages = _interopRequireDefault(require("./orderPages"));

var _pasteWidget = _interopRequireDefault(require("./pageChanges/pasteWidget"));

var _resetTemplate = _interopRequireDefault(require("./resetTemplate"));

var _restorePages = _interopRequireDefault(require("./restorePages"));

var _setInfiniteMode = _interopRequireDefault(require("./pageChanges/setInfiniteMode"));

var _setOrientation = _interopRequireDefault(require("./pageChanges/setOrientation"));

var _setTheme = _interopRequireDefault(require("./pageChanges/setTheme"));

var _setTemplateBackup = _interopRequireDefault(require("./setTemplateBackup"));

var _setWidgetOrder = _interopRequireDefault(require("./pageChanges/setWidgetOrder"));

var _setPosition = _interopRequireDefault(require("./widgetChanges/setPosition"));

var _setSize = _interopRequireDefault(require("./widgetChanges/setSize"));

var _setStyle = _interopRequireDefault(require("./widgetChanges/setStyle"));

var _setValue = _interopRequireDefault(require("./widgetChanges/setValue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IHsgZGVmYXVsdCBhcyBhZGROZXdQYWdlcyB9IGZyb20gJy4vYWRkTmV3UGFnZXMnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIGFkZFdpZGdldCB9IGZyb20gJy4vcGFnZUNoYW5nZXMvYWRkV2lkZ2V0JztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBkZWxldGVQYWdlcyB9IGZyb20gJy4vZGVsZXRlUGFnZXMnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIGRlbGV0ZVdpZGdldCB9IGZyb20gJy4vd2lkZ2V0Q2hhbmdlcy9kZWxldGVXaWRnZXQnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIGR1cGxpY2F0ZVBhZ2UgfSBmcm9tICcuL2R1cGxpY2F0ZVBhZ2UnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIG9yZGVyUGFnZXMgfSBmcm9tICcuL29yZGVyUGFnZXMnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHBhc3RlV2lkZ2V0IH0gZnJvbSAnLi9wYWdlQ2hhbmdlcy9wYXN0ZVdpZGdldCc7XHJcbmV4cG9ydCB7IGRlZmF1bHQgYXMgcmVzZXRUZW1wbGF0ZSB9IGZyb20gJy4vcmVzZXRUZW1wbGF0ZSc7XHJcbmV4cG9ydCB7IGRlZmF1bHQgYXMgcmVzdG9yZVBhZ2VzIH0gZnJvbSAnLi9yZXN0b3JlUGFnZXMnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHNldEluZmluaXRlTW9kZSB9IGZyb20gJy4vcGFnZUNoYW5nZXMvc2V0SW5maW5pdGVNb2RlJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBzZXRPcmllbnRhdGlvbiB9IGZyb20gJy4vcGFnZUNoYW5nZXMvc2V0T3JpZW50YXRpb24nO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHNldFRoZW1lIH0gZnJvbSAnLi9wYWdlQ2hhbmdlcy9zZXRUaGVtZSc7XHJcbmV4cG9ydCB7IGRlZmF1bHQgYXMgc2V0VGVtcGxhdGVCYWNrdXAgfSBmcm9tICcuL3NldFRlbXBsYXRlQmFja3VwJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBzZXRXaWRnZXRPcmRlciB9IGZyb20gJy4vcGFnZUNoYW5nZXMvc2V0V2lkZ2V0T3JkZXInO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHNldFdpZGdldFBvc2l0aW9uIH0gZnJvbSAnLi93aWRnZXRDaGFuZ2VzL3NldFBvc2l0aW9uJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBzZXRXaWRnZXRTaXplIH0gZnJvbSAnLi93aWRnZXRDaGFuZ2VzL3NldFNpemUnO1xyXG5leHBvcnQgeyBkZWZhdWx0IGFzIHNldFdpZGdldFN0eWxlIH0gZnJvbSAnLi93aWRnZXRDaGFuZ2VzL3NldFN0eWxlJztcclxuZXhwb3J0IHsgZGVmYXVsdCBhcyBzZXRXaWRnZXRWYWx1ZSB9IGZyb20gJy4vd2lkZ2V0Q2hhbmdlcy9zZXRWYWx1ZSc7XHJcbiJdfQ==