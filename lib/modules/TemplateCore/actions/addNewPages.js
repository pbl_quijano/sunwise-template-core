"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _selectPage = _interopRequireDefault(require("../../TemplateView/actions/selectPage"));

var templateViewSelectors = _interopRequireWildcard(require("../../TemplateView/selectors"));

var _actionTypes = require("../actionTypes");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(pagesTemplate) {
  return function (dispatch, getState) {
    dispatch({
      type: _actionTypes.ADD_NEW_PAGES,
      payload: pagesTemplate
    });
    var selectedPageFromAdd = templateViewSelectors.getSelectedPage(getState());

    if (selectedPageFromAdd === null && pagesTemplate.length > 0) {
      dispatch((0, _selectPage.default)(pagesTemplate[0].id));
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL2FkZE5ld1BhZ2VzLmpzIl0sIm5hbWVzIjpbInBhZ2VzVGVtcGxhdGUiLCJkaXNwYXRjaCIsImdldFN0YXRlIiwidHlwZSIsIkFERF9ORVdfUEFHRVMiLCJwYXlsb2FkIiwic2VsZWN0ZWRQYWdlRnJvbUFkZCIsInRlbXBsYXRlVmlld1NlbGVjdG9ycyIsImdldFNlbGVjdGVkUGFnZSIsImxlbmd0aCIsImlkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7ZUFFZSxrQkFBQ0EsYUFBRDtBQUFBLFNBQW1CLFVBQUNDLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUN0REQsSUFBQUEsUUFBUSxDQUFDO0FBQ0xFLE1BQUFBLElBQUksRUFBRUMsMEJBREQ7QUFFTEMsTUFBQUEsT0FBTyxFQUFFTDtBQUZKLEtBQUQsQ0FBUjtBQUlBLFFBQU1NLG1CQUFtQixHQUFHQyxxQkFBcUIsQ0FBQ0MsZUFBdEIsQ0FDeEJOLFFBQVEsRUFEZ0IsQ0FBNUI7O0FBR0EsUUFBSUksbUJBQW1CLEtBQUssSUFBeEIsSUFBZ0NOLGFBQWEsQ0FBQ1MsTUFBZCxHQUF1QixDQUEzRCxFQUE4RDtBQUMxRFIsTUFBQUEsUUFBUSxDQUFDLHlCQUFXRCxhQUFhLENBQUMsQ0FBRCxDQUFiLENBQWlCVSxFQUE1QixDQUFELENBQVI7QUFDSDtBQUNKLEdBWGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHNlbGVjdFBhZ2UgZnJvbSAnQG1vZHVsZXMvVGVtcGxhdGVWaWV3L2FjdGlvbnMvc2VsZWN0UGFnZSc7XHJcbmltcG9ydCAqIGFzIHRlbXBsYXRlVmlld1NlbGVjdG9ycyBmcm9tICdAbW9kdWxlcy9UZW1wbGF0ZVZpZXcvc2VsZWN0b3JzJztcclxuXHJcbmltcG9ydCB7IEFERF9ORVdfUEFHRVMgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCAocGFnZXNUZW1wbGF0ZSkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gICAgZGlzcGF0Y2goe1xyXG4gICAgICAgIHR5cGU6IEFERF9ORVdfUEFHRVMsXHJcbiAgICAgICAgcGF5bG9hZDogcGFnZXNUZW1wbGF0ZSxcclxuICAgIH0pO1xyXG4gICAgY29uc3Qgc2VsZWN0ZWRQYWdlRnJvbUFkZCA9IHRlbXBsYXRlVmlld1NlbGVjdG9ycy5nZXRTZWxlY3RlZFBhZ2UoXHJcbiAgICAgICAgZ2V0U3RhdGUoKVxyXG4gICAgKTtcclxuICAgIGlmIChzZWxlY3RlZFBhZ2VGcm9tQWRkID09PSBudWxsICYmIHBhZ2VzVGVtcGxhdGUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIGRpc3BhdGNoKHNlbGVjdFBhZ2UocGFnZXNUZW1wbGF0ZVswXS5pZCkpO1xyXG4gICAgfVxyXG59O1xyXG4iXX0=