"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _selectWidget = _interopRequireDefault(require("../../../TemplateView/actions/selectWidget"));

var _actionTypes = require("../../actionTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(widgetId, onChangeInPage) {
  return function (dispatch) {
    if (!widgetId) {
      return;
    }

    dispatch({
      type: _actionTypes.DELETE_WIDGET,
      payload: widgetId
    });
    dispatch((0, _selectWidget.default)(null));
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3dpZGdldENoYW5nZXMvZGVsZXRlV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIndpZGdldElkIiwib25DaGFuZ2VJblBhZ2UiLCJkaXNwYXRjaCIsInR5cGUiLCJERUxFVEVfV0lER0VUIiwicGF5bG9hZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOzs7O2VBRWUsa0JBQUNBLFFBQUQsRUFBV0MsY0FBWDtBQUFBLFNBQThCLFVBQUNDLFFBQUQsRUFBYztBQUN2RCxRQUFJLENBQUNGLFFBQUwsRUFBZTtBQUNYO0FBQ0g7O0FBQ0RFLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxJQUFJLEVBQUVDLDBCQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRUw7QUFGSixLQUFELENBQVI7QUFJQUUsSUFBQUEsUUFBUSxDQUFDLDJCQUFhLElBQWIsQ0FBRCxDQUFSO0FBQ0FELElBQUFBLGNBQWM7QUFDakIsR0FWYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc2VsZWN0V2lkZ2V0IGZyb20gJ0Btb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NlbGVjdFdpZGdldCc7XG5cbmltcG9ydCB7IERFTEVURV9XSURHRVQgfSBmcm9tICcuLi8uLi9hY3Rpb25UeXBlcyc7XG5cbmV4cG9ydCBkZWZhdWx0ICh3aWRnZXRJZCwgb25DaGFuZ2VJblBhZ2UpID0+IChkaXNwYXRjaCkgPT4ge1xuICAgIGlmICghd2lkZ2V0SWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkaXNwYXRjaCh7XG4gICAgICAgIHR5cGU6IERFTEVURV9XSURHRVQsXG4gICAgICAgIHBheWxvYWQ6IHdpZGdldElkLFxuICAgIH0pO1xuICAgIGRpc3BhdGNoKHNlbGVjdFdpZGdldChudWxsKSk7XG4gICAgb25DaGFuZ2VJblBhZ2UoKTtcbn07XG4iXX0=