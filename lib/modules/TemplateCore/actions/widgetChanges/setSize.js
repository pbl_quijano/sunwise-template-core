"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(widgetId, changedWidth, changedHeight, onChangeInPage) {
  return function (dispatch) {
    if (!widgetId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_WIDGET_SIZE,
      payload: {
        widgetId: widgetId,
        changedWidth: changedWidth,
        changedHeight: changedHeight
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3dpZGdldENoYW5nZXMvc2V0U2l6ZS5qcyJdLCJuYW1lcyI6WyJ3aWRnZXRJZCIsImNoYW5nZWRXaWR0aCIsImNoYW5nZWRIZWlnaHQiLCJvbkNoYW5nZUluUGFnZSIsImRpc3BhdGNoIiwidHlwZSIsIkNIQU5HRV9XSURHRVRfU0laRSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsUUFBRCxFQUFXQyxZQUFYLEVBQXlCQyxhQUF6QixFQUF3Q0MsY0FBeEM7QUFBQSxTQUNYLFVBQUNDLFFBQUQsRUFBYztBQUNWLFFBQUksQ0FBQ0osUUFBTCxFQUFlO0FBQ1g7QUFDSDs7QUFDREksSUFBQUEsUUFBUSxDQUFDO0FBQ0xDLE1BQUFBLElBQUksRUFBRUMsK0JBREQ7QUFFTEMsTUFBQUEsT0FBTyxFQUFFO0FBQUVQLFFBQUFBLFFBQVEsRUFBUkEsUUFBRjtBQUFZQyxRQUFBQSxZQUFZLEVBQVpBLFlBQVo7QUFBMEJDLFFBQUFBLGFBQWEsRUFBYkE7QUFBMUI7QUFGSixLQUFELENBQVI7QUFJQUMsSUFBQUEsY0FBYztBQUNqQixHQVZVO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENIQU5HRV9XSURHRVRfU0laRSB9IGZyb20gJy4uLy4uL2FjdGlvblR5cGVzJztcblxuZXhwb3J0IGRlZmF1bHQgKHdpZGdldElkLCBjaGFuZ2VkV2lkdGgsIGNoYW5nZWRIZWlnaHQsIG9uQ2hhbmdlSW5QYWdlKSA9PlxuICAgIChkaXNwYXRjaCkgPT4ge1xuICAgICAgICBpZiAoIXdpZGdldElkKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgZGlzcGF0Y2goe1xuICAgICAgICAgICAgdHlwZTogQ0hBTkdFX1dJREdFVF9TSVpFLFxuICAgICAgICAgICAgcGF5bG9hZDogeyB3aWRnZXRJZCwgY2hhbmdlZFdpZHRoLCBjaGFuZ2VkSGVpZ2h0IH0sXG4gICAgICAgIH0pO1xuICAgICAgICBvbkNoYW5nZUluUGFnZSgpO1xuICAgIH07XG4iXX0=