"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(widgetId, value, onChangeInPage) {
  return function (dispatch) {
    if (!widgetId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_WIDGET_VALUE,
      payload: {
        widgetId: widgetId,
        value: value
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3dpZGdldENoYW5nZXMvc2V0VmFsdWUuanMiXSwibmFtZXMiOlsid2lkZ2V0SWQiLCJ2YWx1ZSIsIm9uQ2hhbmdlSW5QYWdlIiwiZGlzcGF0Y2giLCJ0eXBlIiwiQ0hBTkdFX1dJREdFVF9WQUxVRSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsUUFBRCxFQUFXQyxLQUFYLEVBQWtCQyxjQUFsQjtBQUFBLFNBQXFDLFVBQUNDLFFBQUQsRUFBYztBQUM5RCxRQUFJLENBQUNILFFBQUwsRUFBZTtBQUNYO0FBQ0g7O0FBQ0RHLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxJQUFJLEVBQUVDLGdDQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRTtBQUFFTixRQUFBQSxRQUFRLEVBQVJBLFFBQUY7QUFBWUMsUUFBQUEsS0FBSyxFQUFMQTtBQUFaO0FBRkosS0FBRCxDQUFSO0FBSUFDLElBQUFBLGNBQWM7QUFDakIsR0FUYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDSEFOR0VfV0lER0VUX1ZBTFVFIH0gZnJvbSAnLi4vLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAod2lkZ2V0SWQsIHZhbHVlLCBvbkNoYW5nZUluUGFnZSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gICAgaWYgKCF3aWRnZXRJZCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRpc3BhdGNoKHtcbiAgICAgICAgdHlwZTogQ0hBTkdFX1dJREdFVF9WQUxVRSxcbiAgICAgICAgcGF5bG9hZDogeyB3aWRnZXRJZCwgdmFsdWUgfSxcbiAgICB9KTtcbiAgICBvbkNoYW5nZUluUGFnZSgpO1xufTtcbiJdfQ==