"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(widgetId, style, onChangeInPage) {
  return function (dispatch) {
    if (!widgetId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_WIDGET_STYLE,
      payload: {
        widgetId: widgetId,
        style: style
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3dpZGdldENoYW5nZXMvc2V0U3R5bGUuanMiXSwibmFtZXMiOlsid2lkZ2V0SWQiLCJzdHlsZSIsIm9uQ2hhbmdlSW5QYWdlIiwiZGlzcGF0Y2giLCJ0eXBlIiwiQ0hBTkdFX1dJREdFVF9TVFlMRSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsUUFBRCxFQUFXQyxLQUFYLEVBQWtCQyxjQUFsQjtBQUFBLFNBQXFDLFVBQUNDLFFBQUQsRUFBYztBQUM5RCxRQUFJLENBQUNILFFBQUwsRUFBZTtBQUNYO0FBQ0g7O0FBQ0RHLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxJQUFJLEVBQUVDLGdDQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRTtBQUFFTixRQUFBQSxRQUFRLEVBQVJBLFFBQUY7QUFBWUMsUUFBQUEsS0FBSyxFQUFMQTtBQUFaO0FBRkosS0FBRCxDQUFSO0FBSUFDLElBQUFBLGNBQWM7QUFDakIsR0FUYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDSEFOR0VfV0lER0VUX1NUWUxFIH0gZnJvbSAnLi4vLi4vYWN0aW9uVHlwZXMnO1xuXG5leHBvcnQgZGVmYXVsdCAod2lkZ2V0SWQsIHN0eWxlLCBvbkNoYW5nZUluUGFnZSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gICAgaWYgKCF3aWRnZXRJZCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRpc3BhdGNoKHtcbiAgICAgICAgdHlwZTogQ0hBTkdFX1dJREdFVF9TVFlMRSxcbiAgICAgICAgcGF5bG9hZDogeyB3aWRnZXRJZCwgc3R5bGUgfSxcbiAgICB9KTtcbiAgICBvbkNoYW5nZUluUGFnZSgpO1xufTtcbiJdfQ==