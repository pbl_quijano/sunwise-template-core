"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../actionTypes");

var _default = function _default(pageData) {
  return function (dispatch) {
    dispatch({
      type: _actionTypes.SET_PAGE_DATA,
      payload: pageData
    });
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3NldFBhZ2VEYXRhLmpzIl0sIm5hbWVzIjpbInBhZ2VEYXRhIiwiZGlzcGF0Y2giLCJ0eXBlIiwiU0VUX1BBR0VfREFUQSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsUUFBRDtBQUFBLFNBQWMsVUFBQ0MsUUFBRCxFQUFjO0FBQ3ZDQSxJQUFBQSxRQUFRLENBQUM7QUFBRUMsTUFBQUEsSUFBSSxFQUFFQywwQkFBUjtBQUF1QkMsTUFBQUEsT0FBTyxFQUFFSjtBQUFoQyxLQUFELENBQVI7QUFDSCxHQUZjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNFVF9QQUdFX0RBVEEgfSBmcm9tICcuLi9hY3Rpb25UeXBlcyc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCAocGFnZURhdGEpID0+IChkaXNwYXRjaCkgPT4ge1xyXG4gICAgZGlzcGF0Y2goeyB0eXBlOiBTRVRfUEFHRV9EQVRBLCBwYXlsb2FkOiBwYWdlRGF0YSB9KTtcclxufTtcclxuIl19