"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(pageId, theme, onChangeInPage) {
  return function (dispatch) {
    if (!pageId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_PAGE_THEME,
      payload: {
        pageId: pageId,
        theme: theme
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL3NldFRoZW1lLmpzIl0sIm5hbWVzIjpbInBhZ2VJZCIsInRoZW1lIiwib25DaGFuZ2VJblBhZ2UiLCJkaXNwYXRjaCIsInR5cGUiLCJDSEFOR0VfUEFHRV9USEVNRSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFFZSxrQkFBQ0EsTUFBRCxFQUFTQyxLQUFULEVBQWdCQyxjQUFoQjtBQUFBLFNBQW1DLFVBQUNDLFFBQUQsRUFBYztBQUM1RCxRQUFJLENBQUNILE1BQUwsRUFBYTtBQUNUO0FBQ0g7O0FBQ0RHLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxJQUFJLEVBQUVDLDhCQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRTtBQUFFTixRQUFBQSxNQUFNLEVBQU5BLE1BQUY7QUFBVUMsUUFBQUEsS0FBSyxFQUFMQTtBQUFWO0FBRkosS0FBRCxDQUFSO0FBSUFDLElBQUFBLGNBQWM7QUFDakIsR0FUYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDSEFOR0VfUEFHRV9USEVNRSB9IGZyb20gJy4uLy4uL2FjdGlvblR5cGVzJztcblxuZXhwb3J0IGRlZmF1bHQgKHBhZ2VJZCwgdGhlbWUsIG9uQ2hhbmdlSW5QYWdlKSA9PiAoZGlzcGF0Y2gpID0+IHtcbiAgICBpZiAoIXBhZ2VJZCkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGRpc3BhdGNoKHtcbiAgICAgICAgdHlwZTogQ0hBTkdFX1BBR0VfVEhFTUUsXG4gICAgICAgIHBheWxvYWQ6IHsgcGFnZUlkLCB0aGVtZSB9LFxuICAgIH0pO1xuICAgIG9uQ2hhbmdlSW5QYWdlKCk7XG59O1xuIl19