"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(pageId, orientation, onChangeInPage) {
  return function (dispatch) {
    if (!pageId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_PAGE_ORIENTATION,
      payload: {
        pageId: pageId,
        orientation: orientation
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL3NldE9yaWVudGF0aW9uLmpzIl0sIm5hbWVzIjpbInBhZ2VJZCIsIm9yaWVudGF0aW9uIiwib25DaGFuZ2VJblBhZ2UiLCJkaXNwYXRjaCIsInR5cGUiLCJDSEFOR0VfUEFHRV9PUklFTlRBVElPTiIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7ZUFDZSxrQkFBQ0EsTUFBRCxFQUFTQyxXQUFULEVBQXNCQyxjQUF0QjtBQUFBLFNBQXlDLFVBQUNDLFFBQUQsRUFBYztBQUNsRSxRQUFJLENBQUNILE1BQUwsRUFBYTtBQUNUO0FBQ0g7O0FBQ0RHLElBQUFBLFFBQVEsQ0FBQztBQUNMQyxNQUFBQSxJQUFJLEVBQUVDLG9DQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRTtBQUFFTixRQUFBQSxNQUFNLEVBQU5BLE1BQUY7QUFBVUMsUUFBQUEsV0FBVyxFQUFYQTtBQUFWO0FBRkosS0FBRCxDQUFSO0FBSUFDLElBQUFBLGNBQWM7QUFDakIsR0FUYztBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDSEFOR0VfUEFHRV9PUklFTlRBVElPTiB9IGZyb20gJy4uLy4uL2FjdGlvblR5cGVzJztcbmV4cG9ydCBkZWZhdWx0IChwYWdlSWQsIG9yaWVudGF0aW9uLCBvbkNoYW5nZUluUGFnZSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gICAgaWYgKCFwYWdlSWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkaXNwYXRjaCh7XG4gICAgICAgIHR5cGU6IENIQU5HRV9QQUdFX09SSUVOVEFUSU9OLFxuICAgICAgICBwYXlsb2FkOiB7IHBhZ2VJZCwgb3JpZW50YXRpb24gfSxcbiAgICB9KTtcbiAgICBvbkNoYW5nZUluUGFnZSgpO1xufTtcbiJdfQ==