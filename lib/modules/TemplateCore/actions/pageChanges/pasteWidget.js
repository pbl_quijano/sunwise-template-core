"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _cloneDeep2 = _interopRequireDefault(require("lodash/cloneDeep"));

var _selectWidget = _interopRequireDefault(require("../../../TemplateView/actions/selectWidget"));

var _actionTypes = require("../../actionTypes");

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(pageId, optionsRef, showGuides) {
  var position = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var hasSummarySupport = arguments.length > 4 ? arguments[4] : undefined;
  var onChangeInPage = arguments.length > 5 ? arguments[5] : undefined;
  return function (dispatch, getState) {
    var state = getState();

    var _selectors$getEntitie = selectors.getEntitiesSession(state),
        Page = _selectors$getEntitie.Page;

    if (!pageId || !Page.idExists(pageId)) {
      return;
    }

    var pageModel = Page.withId(pageId);
    var pageData = pageModel.ref;
    var pageWidgets = pageModel.widgets.toRefArray();

    var _cloneDeep = (0, _cloneDeep2.default)(optionsRef),
        name = _cloneDeep.name,
        height = _cloneDeep.height,
        replaceInfoRequired = _cloneDeep.replaceInfoRequired,
        style = _cloneDeep.style,
        _cloneDeep$supportVer = _cloneDeep.supportVersion,
        supportVersion = _cloneDeep$supportVer === void 0 ? 2 : _cloneDeep$supportVer,
        value = _cloneDeep.value,
        width = _cloneDeep.width;

    var newNum = (0, _helpers.getNum)(name, pageWidgets);
    var newName = "".concat(name, "-").concat(newNum);

    var _getDefaultPosition = (0, _helpers.getDefaultPosition)(height, width, position, pageData.orientation, showGuides),
        x = _getDefaultPosition.x,
        y = _getDefaultPosition.y;

    var newWidget = {
      id: "".concat(pageId, "/").concat(newName),
      field: newName,
      hasSummarySupport: hasSummarySupport,
      height: height,
      name: name,
      num: newNum,
      order: pageWidgets.length,
      pageId: pageId,
      replaceInfoRequired: replaceInfoRequired,
      style: style,
      supportVersion: supportVersion,
      value: value,
      width: width,
      x: x,
      y: y
    };
    dispatch({
      type: _actionTypes.PASTE_WIDGET,
      payload: newWidget
    });
    dispatch((0, _selectWidget.default)(newWidget.id));
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL3Bhc3RlV2lkZ2V0LmpzIl0sIm5hbWVzIjpbInBhZ2VJZCIsIm9wdGlvbnNSZWYiLCJzaG93R3VpZGVzIiwicG9zaXRpb24iLCJoYXNTdW1tYXJ5U3VwcG9ydCIsIm9uQ2hhbmdlSW5QYWdlIiwiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInN0YXRlIiwic2VsZWN0b3JzIiwiZ2V0RW50aXRpZXNTZXNzaW9uIiwiUGFnZSIsImlkRXhpc3RzIiwicGFnZU1vZGVsIiwid2l0aElkIiwicGFnZURhdGEiLCJyZWYiLCJwYWdlV2lkZ2V0cyIsIndpZGdldHMiLCJ0b1JlZkFycmF5IiwibmFtZSIsImhlaWdodCIsInJlcGxhY2VJbmZvUmVxdWlyZWQiLCJzdHlsZSIsInN1cHBvcnRWZXJzaW9uIiwidmFsdWUiLCJ3aWR0aCIsIm5ld051bSIsIm5ld05hbWUiLCJvcmllbnRhdGlvbiIsIngiLCJ5IiwibmV3V2lkZ2V0IiwiaWQiLCJmaWVsZCIsIm51bSIsIm9yZGVyIiwibGVuZ3RoIiwidHlwZSIsIlBBU1RFX1dJREdFVCIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7OztlQUVlLGtCQUNQQSxNQURPLEVBRVBDLFVBRk8sRUFHUEMsVUFITztBQUFBLE1BSVBDLFFBSk8sdUVBSUksSUFKSjtBQUFBLE1BS1BDLGlCQUxPO0FBQUEsTUFNUEMsY0FOTztBQUFBLFNBUVgsVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQ3BCLFFBQU1DLEtBQUssR0FBR0QsUUFBUSxFQUF0Qjs7QUFDQSxnQ0FBaUJFLFNBQVMsQ0FBQ0Msa0JBQVYsQ0FBNkJGLEtBQTdCLENBQWpCO0FBQUEsUUFBUUcsSUFBUix5QkFBUUEsSUFBUjs7QUFDQSxRQUFJLENBQUNYLE1BQUQsSUFBVyxDQUFDVyxJQUFJLENBQUNDLFFBQUwsQ0FBY1osTUFBZCxDQUFoQixFQUF1QztBQUNuQztBQUNIOztBQUNELFFBQU1hLFNBQVMsR0FBR0YsSUFBSSxDQUFDRyxNQUFMLENBQVlkLE1BQVosQ0FBbEI7QUFDQSxRQUFNZSxRQUFRLEdBQUdGLFNBQVMsQ0FBQ0csR0FBM0I7QUFDQSxRQUFNQyxXQUFXLEdBQUdKLFNBQVMsQ0FBQ0ssT0FBVixDQUFrQkMsVUFBbEIsRUFBcEI7O0FBQ0EscUJBUUkseUJBQVVsQixVQUFWLENBUko7QUFBQSxRQUNJbUIsSUFESixjQUNJQSxJQURKO0FBQUEsUUFFSUMsTUFGSixjQUVJQSxNQUZKO0FBQUEsUUFHSUMsbUJBSEosY0FHSUEsbUJBSEo7QUFBQSxRQUlJQyxLQUpKLGNBSUlBLEtBSko7QUFBQSwyQ0FLSUMsY0FMSjtBQUFBLFFBS0lBLGNBTEosc0NBS3FCLENBTHJCO0FBQUEsUUFNSUMsS0FOSixjQU1JQSxLQU5KO0FBQUEsUUFPSUMsS0FQSixjQU9JQSxLQVBKOztBQVNBLFFBQU1DLE1BQU0sR0FBRyxxQkFBT1AsSUFBUCxFQUFhSCxXQUFiLENBQWY7QUFDQSxRQUFNVyxPQUFPLGFBQU1SLElBQU4sY0FBY08sTUFBZCxDQUFiOztBQUNBLDhCQUFpQixpQ0FDYk4sTUFEYSxFQUViSyxLQUZhLEVBR2J2QixRQUhhLEVBSWJZLFFBQVEsQ0FBQ2MsV0FKSSxFQUtiM0IsVUFMYSxDQUFqQjtBQUFBLFFBQVE0QixDQUFSLHVCQUFRQSxDQUFSO0FBQUEsUUFBV0MsQ0FBWCx1QkFBV0EsQ0FBWDs7QUFPQSxRQUFNQyxTQUFTLEdBQUc7QUFDZEMsTUFBQUEsRUFBRSxZQUFLakMsTUFBTCxjQUFlNEIsT0FBZixDQURZO0FBRWRNLE1BQUFBLEtBQUssRUFBRU4sT0FGTztBQUdkeEIsTUFBQUEsaUJBQWlCLEVBQWpCQSxpQkFIYztBQUlkaUIsTUFBQUEsTUFBTSxFQUFOQSxNQUpjO0FBS2RELE1BQUFBLElBQUksRUFBSkEsSUFMYztBQU1kZSxNQUFBQSxHQUFHLEVBQUVSLE1BTlM7QUFPZFMsTUFBQUEsS0FBSyxFQUFFbkIsV0FBVyxDQUFDb0IsTUFQTDtBQVFkckMsTUFBQUEsTUFBTSxFQUFOQSxNQVJjO0FBU2RzQixNQUFBQSxtQkFBbUIsRUFBbkJBLG1CQVRjO0FBVWRDLE1BQUFBLEtBQUssRUFBTEEsS0FWYztBQVdkQyxNQUFBQSxjQUFjLEVBQWRBLGNBWGM7QUFZZEMsTUFBQUEsS0FBSyxFQUFMQSxLQVpjO0FBYWRDLE1BQUFBLEtBQUssRUFBTEEsS0FiYztBQWNkSSxNQUFBQSxDQUFDLEVBQURBLENBZGM7QUFlZEMsTUFBQUEsQ0FBQyxFQUFEQTtBQWZjLEtBQWxCO0FBaUJBekIsSUFBQUEsUUFBUSxDQUFDO0FBQ0xnQyxNQUFBQSxJQUFJLEVBQUVDLHlCQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRVI7QUFGSixLQUFELENBQVI7QUFJQTFCLElBQUFBLFFBQVEsQ0FBQywyQkFBYTBCLFNBQVMsQ0FBQ0MsRUFBdkIsQ0FBRCxDQUFSO0FBRUE1QixJQUFBQSxjQUFjO0FBQ2pCLEdBM0RVO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjbG9uZURlZXAgZnJvbSAnbG9kYXNoL2Nsb25lRGVlcCc7XG5cbmltcG9ydCBzZWxlY3RXaWRnZXQgZnJvbSAnQG1vZHVsZXMvVGVtcGxhdGVWaWV3L2FjdGlvbnMvc2VsZWN0V2lkZ2V0JztcblxuaW1wb3J0IHsgUEFTVEVfV0lER0VUIH0gZnJvbSAnLi4vLi4vYWN0aW9uVHlwZXMnO1xuaW1wb3J0IHsgZ2V0RGVmYXVsdFBvc2l0aW9uLCBnZXROdW0gfSBmcm9tICcuLi8uLi9oZWxwZXJzJztcbmltcG9ydCAqIGFzIHNlbGVjdG9ycyBmcm9tICcuLi8uLi9zZWxlY3RvcnMnO1xuXG5leHBvcnQgZGVmYXVsdCAoXG4gICAgICAgIHBhZ2VJZCxcbiAgICAgICAgb3B0aW9uc1JlZixcbiAgICAgICAgc2hvd0d1aWRlcyxcbiAgICAgICAgcG9zaXRpb24gPSBudWxsLFxuICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcbiAgICAgICAgb25DaGFuZ2VJblBhZ2VcbiAgICApID0+XG4gICAgKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xuICAgICAgICBjb25zdCBzdGF0ZSA9IGdldFN0YXRlKCk7XG4gICAgICAgIGNvbnN0IHsgUGFnZSB9ID0gc2VsZWN0b3JzLmdldEVudGl0aWVzU2Vzc2lvbihzdGF0ZSk7XG4gICAgICAgIGlmICghcGFnZUlkIHx8ICFQYWdlLmlkRXhpc3RzKHBhZ2VJZCkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBwYWdlTW9kZWwgPSBQYWdlLndpdGhJZChwYWdlSWQpO1xuICAgICAgICBjb25zdCBwYWdlRGF0YSA9IHBhZ2VNb2RlbC5yZWY7XG4gICAgICAgIGNvbnN0IHBhZ2VXaWRnZXRzID0gcGFnZU1vZGVsLndpZGdldHMudG9SZWZBcnJheSgpO1xuICAgICAgICBjb25zdCB7XG4gICAgICAgICAgICBuYW1lLFxuICAgICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcbiAgICAgICAgICAgIHN0eWxlLFxuICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24gPSAyLFxuICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICB3aWR0aCxcbiAgICAgICAgfSA9IGNsb25lRGVlcChvcHRpb25zUmVmKTtcbiAgICAgICAgY29uc3QgbmV3TnVtID0gZ2V0TnVtKG5hbWUsIHBhZ2VXaWRnZXRzKTtcbiAgICAgICAgY29uc3QgbmV3TmFtZSA9IGAke25hbWV9LSR7bmV3TnVtfWA7XG4gICAgICAgIGNvbnN0IHsgeCwgeSB9ID0gZ2V0RGVmYXVsdFBvc2l0aW9uKFxuICAgICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgICAgd2lkdGgsXG4gICAgICAgICAgICBwb3NpdGlvbixcbiAgICAgICAgICAgIHBhZ2VEYXRhLm9yaWVudGF0aW9uLFxuICAgICAgICAgICAgc2hvd0d1aWRlc1xuICAgICAgICApO1xuICAgICAgICBjb25zdCBuZXdXaWRnZXQgPSB7XG4gICAgICAgICAgICBpZDogYCR7cGFnZUlkfS8ke25ld05hbWV9YCxcbiAgICAgICAgICAgIGZpZWxkOiBuZXdOYW1lLFxuICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXG4gICAgICAgICAgICBoZWlnaHQsXG4gICAgICAgICAgICBuYW1lLFxuICAgICAgICAgICAgbnVtOiBuZXdOdW0sXG4gICAgICAgICAgICBvcmRlcjogcGFnZVdpZGdldHMubGVuZ3RoLFxuICAgICAgICAgICAgcGFnZUlkLFxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcbiAgICAgICAgICAgIHN0eWxlLFxuICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24sXG4gICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgIHdpZHRoLFxuICAgICAgICAgICAgeCxcbiAgICAgICAgICAgIHksXG4gICAgICAgIH07XG4gICAgICAgIGRpc3BhdGNoKHtcbiAgICAgICAgICAgIHR5cGU6IFBBU1RFX1dJREdFVCxcbiAgICAgICAgICAgIHBheWxvYWQ6IG5ld1dpZGdldCxcbiAgICAgICAgfSk7XG4gICAgICAgIGRpc3BhdGNoKHNlbGVjdFdpZGdldChuZXdXaWRnZXQuaWQpKTtcblxuICAgICAgICBvbkNoYW5nZUluUGFnZSgpO1xuICAgIH07XG4iXX0=