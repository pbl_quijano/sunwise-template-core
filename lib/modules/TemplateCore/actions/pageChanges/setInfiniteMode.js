"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _default = function _default(pageId, infiniteMode, onChangeInPage) {
  return function (dispatch) {
    if (!pageId) {
      return;
    }

    dispatch({
      type: _actionTypes.CHANGE_PAGE_INFINITE_MODE,
      payload: {
        pageId: pageId,
        infiniteMode: infiniteMode
      }
    });
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL3NldEluZmluaXRlTW9kZS5qcyJdLCJuYW1lcyI6WyJwYWdlSWQiLCJpbmZpbml0ZU1vZGUiLCJvbkNoYW5nZUluUGFnZSIsImRpc3BhdGNoIiwidHlwZSIsIkNIQU5HRV9QQUdFX0lORklOSVRFX01PREUiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O2VBQ2Usa0JBQUNBLE1BQUQsRUFBU0MsWUFBVCxFQUF1QkMsY0FBdkI7QUFBQSxTQUEwQyxVQUFDQyxRQUFELEVBQWM7QUFDbkUsUUFBSSxDQUFDSCxNQUFMLEVBQWE7QUFDVDtBQUNIOztBQUNERyxJQUFBQSxRQUFRLENBQUM7QUFDTEMsTUFBQUEsSUFBSSxFQUFFQyxzQ0FERDtBQUVMQyxNQUFBQSxPQUFPLEVBQUU7QUFBRU4sUUFBQUEsTUFBTSxFQUFOQSxNQUFGO0FBQVVDLFFBQUFBLFlBQVksRUFBWkE7QUFBVjtBQUZKLEtBQUQsQ0FBUjtBQUlBQyxJQUFBQSxjQUFjO0FBQ2pCLEdBVGM7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ0hBTkdFX1BBR0VfSU5GSU5JVEVfTU9ERSB9IGZyb20gJy4uLy4uL2FjdGlvblR5cGVzJztcbmV4cG9ydCBkZWZhdWx0IChwYWdlSWQsIGluZmluaXRlTW9kZSwgb25DaGFuZ2VJblBhZ2UpID0+IChkaXNwYXRjaCkgPT4ge1xuICAgIGlmICghcGFnZUlkKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgZGlzcGF0Y2goe1xuICAgICAgICB0eXBlOiBDSEFOR0VfUEFHRV9JTkZJTklURV9NT0RFLFxuICAgICAgICBwYXlsb2FkOiB7IHBhZ2VJZCwgaW5maW5pdGVNb2RlIH0sXG4gICAgfSk7XG4gICAgb25DaGFuZ2VJblBhZ2UoKTtcbn07XG4iXX0=