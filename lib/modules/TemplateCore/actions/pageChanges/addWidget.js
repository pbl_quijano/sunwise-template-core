"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _template = require("../../../../constants/template");

var mainSelectors = _interopRequireWildcard(require("../../../main/selectors"));

var _selectWidget = _interopRequireDefault(require("../../../TemplateView/actions/selectWidget"));

var _actionTypes = require("../../actionTypes");

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default(pageId, type, defaultPosition, hasSummarySupport, onChangeInPage) {
  return function (dispatch, getState) {
    var state = getState();

    var _selectors$getEntitie = selectors.getEntitiesSession(state),
        Page = _selectors$getEntitie.Page;

    if (!pageId || !Page.idExists(pageId)) {
      return;
    }

    var pageModel = Page.withId(pageId);
    var pageData = pageModel.ref;
    var pageWidgets = pageModel.widgets.toRefArray();
    var chartWidgets = mainSelectors.getDataFetchChartWidgets(state);
    var tableWidgets = mainSelectors.getDataFetchTableWidgets(state);
    var catalogs = mainSelectors.getFetchCatalogData(state);
    var newNum = (0, _helpers.getNum)(type, pageWidgets);
    var newName = "".concat(type, "-").concat(newNum);

    var _getDefaultWidgetPosi = (0, _helpers.getDefaultWidgetPosition)((0, _helpers.getDefaultWidgetValues)(hasSummarySupport, {
      chartWidgets: chartWidgets,
      tableWidgets: tableWidgets
    }, pageData.orientation, hasSummarySupport ? (0, _helpers.getFirstCatalogId)(catalogs) : '', pageData.theme, type), defaultPosition, pageData.orientation),
        hasSummarySupportWidget = _getDefaultWidgetPosi.hasSummarySupport,
        height = _getDefaultWidgetPosi.height,
        replaceInfoRequired = _getDefaultWidgetPosi.replaceInfoRequired,
        style = _getDefaultWidgetPosi.style,
        value = _getDefaultWidgetPosi.value,
        width = _getDefaultWidgetPosi.width,
        x = _getDefaultWidgetPosi.x,
        y = _getDefaultWidgetPosi.y;

    var newWidget = {
      id: "".concat(pageId, "/").concat(newName),
      field: newName,
      hasSummarySupport: hasSummarySupportWidget,
      height: height,
      name: type,
      num: newNum,
      order: pageWidgets.length,
      pageId: pageId,
      replaceInfoRequired: replaceInfoRequired,
      style: style,
      supportVersion: _template.WIDGET_SUPPORT_VERSION,
      value: value,
      width: width,
      x: x,
      y: y
    };
    dispatch({
      type: _actionTypes.ADD_WIDGET,
      payload: newWidget
    });
    dispatch((0, _selectWidget.default)(newWidget.id));
    onChangeInPage();
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL2FkZFdpZGdldC5qcyJdLCJuYW1lcyI6WyJwYWdlSWQiLCJ0eXBlIiwiZGVmYXVsdFBvc2l0aW9uIiwiaGFzU3VtbWFyeVN1cHBvcnQiLCJvbkNoYW5nZUluUGFnZSIsImRpc3BhdGNoIiwiZ2V0U3RhdGUiLCJzdGF0ZSIsInNlbGVjdG9ycyIsImdldEVudGl0aWVzU2Vzc2lvbiIsIlBhZ2UiLCJpZEV4aXN0cyIsInBhZ2VNb2RlbCIsIndpdGhJZCIsInBhZ2VEYXRhIiwicmVmIiwicGFnZVdpZGdldHMiLCJ3aWRnZXRzIiwidG9SZWZBcnJheSIsImNoYXJ0V2lkZ2V0cyIsIm1haW5TZWxlY3RvcnMiLCJnZXREYXRhRmV0Y2hDaGFydFdpZGdldHMiLCJ0YWJsZVdpZGdldHMiLCJnZXREYXRhRmV0Y2hUYWJsZVdpZGdldHMiLCJjYXRhbG9ncyIsImdldEZldGNoQ2F0YWxvZ0RhdGEiLCJuZXdOdW0iLCJuZXdOYW1lIiwib3JpZW50YXRpb24iLCJ0aGVtZSIsImhhc1N1bW1hcnlTdXBwb3J0V2lkZ2V0IiwiaGVpZ2h0IiwicmVwbGFjZUluZm9SZXF1aXJlZCIsInN0eWxlIiwidmFsdWUiLCJ3aWR0aCIsIngiLCJ5IiwibmV3V2lkZ2V0IiwiaWQiLCJmaWVsZCIsIm5hbWUiLCJudW0iLCJvcmRlciIsImxlbmd0aCIsInN1cHBvcnRWZXJzaW9uIiwiV0lER0VUX1NVUFBPUlRfVkVSU0lPTiIsIkFERF9XSURHRVQiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFDQTs7QUFNQTs7Ozs7Ozs7ZUFFZSxrQkFDUEEsTUFETyxFQUVQQyxJQUZPLEVBR1BDLGVBSE8sRUFJUEMsaUJBSk8sRUFLUEMsY0FMTztBQUFBLFNBT1gsVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQ3BCLFFBQU1DLEtBQUssR0FBR0QsUUFBUSxFQUF0Qjs7QUFDQSxnQ0FBaUJFLFNBQVMsQ0FBQ0Msa0JBQVYsQ0FBNkJGLEtBQTdCLENBQWpCO0FBQUEsUUFBUUcsSUFBUix5QkFBUUEsSUFBUjs7QUFDQSxRQUFJLENBQUNWLE1BQUQsSUFBVyxDQUFDVSxJQUFJLENBQUNDLFFBQUwsQ0FBY1gsTUFBZCxDQUFoQixFQUF1QztBQUNuQztBQUNIOztBQUNELFFBQU1ZLFNBQVMsR0FBR0YsSUFBSSxDQUFDRyxNQUFMLENBQVliLE1BQVosQ0FBbEI7QUFDQSxRQUFNYyxRQUFRLEdBQUdGLFNBQVMsQ0FBQ0csR0FBM0I7QUFDQSxRQUFNQyxXQUFXLEdBQUdKLFNBQVMsQ0FBQ0ssT0FBVixDQUFrQkMsVUFBbEIsRUFBcEI7QUFDQSxRQUFNQyxZQUFZLEdBQUdDLGFBQWEsQ0FBQ0Msd0JBQWQsQ0FBdUNkLEtBQXZDLENBQXJCO0FBQ0EsUUFBTWUsWUFBWSxHQUFHRixhQUFhLENBQUNHLHdCQUFkLENBQXVDaEIsS0FBdkMsQ0FBckI7QUFDQSxRQUFNaUIsUUFBUSxHQUFHSixhQUFhLENBQUNLLG1CQUFkLENBQWtDbEIsS0FBbEMsQ0FBakI7QUFFQSxRQUFNbUIsTUFBTSxHQUFHLHFCQUFPekIsSUFBUCxFQUFhZSxXQUFiLENBQWY7QUFDQSxRQUFNVyxPQUFPLGFBQU0xQixJQUFOLGNBQWN5QixNQUFkLENBQWI7O0FBQ0EsZ0NBU0ksdUNBQ0EscUNBQ0l2QixpQkFESixFQUVJO0FBQUVnQixNQUFBQSxZQUFZLEVBQVpBLFlBQUY7QUFBZ0JHLE1BQUFBLFlBQVksRUFBWkE7QUFBaEIsS0FGSixFQUdJUixRQUFRLENBQUNjLFdBSGIsRUFJSXpCLGlCQUFpQixHQUFHLGdDQUFrQnFCLFFBQWxCLENBQUgsR0FBaUMsRUFKdEQsRUFLSVYsUUFBUSxDQUFDZSxLQUxiLEVBTUk1QixJQU5KLENBREEsRUFTQUMsZUFUQSxFQVVBWSxRQUFRLENBQUNjLFdBVlQsQ0FUSjtBQUFBLFFBQ3VCRSx1QkFEdkIseUJBQ0kzQixpQkFESjtBQUFBLFFBRUk0QixNQUZKLHlCQUVJQSxNQUZKO0FBQUEsUUFHSUMsbUJBSEoseUJBR0lBLG1CQUhKO0FBQUEsUUFJSUMsS0FKSix5QkFJSUEsS0FKSjtBQUFBLFFBS0lDLEtBTEoseUJBS0lBLEtBTEo7QUFBQSxRQU1JQyxLQU5KLHlCQU1JQSxLQU5KO0FBQUEsUUFPSUMsQ0FQSix5QkFPSUEsQ0FQSjtBQUFBLFFBUUlDLENBUkoseUJBUUlBLENBUko7O0FBcUJBLFFBQU1DLFNBQVMsR0FBRztBQUNkQyxNQUFBQSxFQUFFLFlBQUt2QyxNQUFMLGNBQWUyQixPQUFmLENBRFk7QUFFZGEsTUFBQUEsS0FBSyxFQUFFYixPQUZPO0FBR2R4QixNQUFBQSxpQkFBaUIsRUFBRTJCLHVCQUhMO0FBSWRDLE1BQUFBLE1BQU0sRUFBTkEsTUFKYztBQUtkVSxNQUFBQSxJQUFJLEVBQUV4QyxJQUxRO0FBTWR5QyxNQUFBQSxHQUFHLEVBQUVoQixNQU5TO0FBT2RpQixNQUFBQSxLQUFLLEVBQUUzQixXQUFXLENBQUM0QixNQVBMO0FBUWQ1QyxNQUFBQSxNQUFNLEVBQU5BLE1BUmM7QUFTZGdDLE1BQUFBLG1CQUFtQixFQUFuQkEsbUJBVGM7QUFVZEMsTUFBQUEsS0FBSyxFQUFMQSxLQVZjO0FBV2RZLE1BQUFBLGNBQWMsRUFBRUMsZ0NBWEY7QUFZZFosTUFBQUEsS0FBSyxFQUFMQSxLQVpjO0FBYWRDLE1BQUFBLEtBQUssRUFBTEEsS0FiYztBQWNkQyxNQUFBQSxDQUFDLEVBQURBLENBZGM7QUFlZEMsTUFBQUEsQ0FBQyxFQUFEQTtBQWZjLEtBQWxCO0FBa0JBaEMsSUFBQUEsUUFBUSxDQUFDO0FBQ0xKLE1BQUFBLElBQUksRUFBRThDLHVCQUREO0FBRUxDLE1BQUFBLE9BQU8sRUFBRVY7QUFGSixLQUFELENBQVI7QUFJQWpDLElBQUFBLFFBQVEsQ0FBQywyQkFBYWlDLFNBQVMsQ0FBQ0MsRUFBdkIsQ0FBRCxDQUFSO0FBQ0FuQyxJQUFBQSxjQUFjO0FBQ2pCLEdBbkVVO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFdJREdFVF9TVVBQT1JUX1ZFUlNJT04gfSBmcm9tICdAY29uc3RhbnRzL3RlbXBsYXRlJztcblxuaW1wb3J0ICogYXMgbWFpblNlbGVjdG9ycyBmcm9tICdAbWFpbi9zZWxlY3RvcnMnO1xuXG5pbXBvcnQgc2VsZWN0V2lkZ2V0IGZyb20gJ0Btb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zL3NlbGVjdFdpZGdldCc7XG5cbmltcG9ydCB7IEFERF9XSURHRVQgfSBmcm9tICcuLi8uLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQge1xuICAgIGdldERlZmF1bHRXaWRnZXRQb3NpdGlvbixcbiAgICBnZXREZWZhdWx0V2lkZ2V0VmFsdWVzLFxuICAgIGdldEZpcnN0Q2F0YWxvZ0lkLFxuICAgIGdldE51bSxcbn0gZnJvbSAnLi4vLi4vaGVscGVycyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vLi4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGRlZmF1bHQgKFxuICAgICAgICBwYWdlSWQsXG4gICAgICAgIHR5cGUsXG4gICAgICAgIGRlZmF1bHRQb3NpdGlvbixcbiAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXG4gICAgICAgIG9uQ2hhbmdlSW5QYWdlXG4gICAgKSA9PlxuICAgIChkaXNwYXRjaCwgZ2V0U3RhdGUpID0+IHtcbiAgICAgICAgY29uc3Qgc3RhdGUgPSBnZXRTdGF0ZSgpO1xuICAgICAgICBjb25zdCB7IFBhZ2UgfSA9IHNlbGVjdG9ycy5nZXRFbnRpdGllc1Nlc3Npb24oc3RhdGUpO1xuICAgICAgICBpZiAoIXBhZ2VJZCB8fCAhUGFnZS5pZEV4aXN0cyhwYWdlSWQpKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgcGFnZU1vZGVsID0gUGFnZS53aXRoSWQocGFnZUlkKTtcbiAgICAgICAgY29uc3QgcGFnZURhdGEgPSBwYWdlTW9kZWwucmVmO1xuICAgICAgICBjb25zdCBwYWdlV2lkZ2V0cyA9IHBhZ2VNb2RlbC53aWRnZXRzLnRvUmVmQXJyYXkoKTtcbiAgICAgICAgY29uc3QgY2hhcnRXaWRnZXRzID0gbWFpblNlbGVjdG9ycy5nZXREYXRhRmV0Y2hDaGFydFdpZGdldHMoc3RhdGUpO1xuICAgICAgICBjb25zdCB0YWJsZVdpZGdldHMgPSBtYWluU2VsZWN0b3JzLmdldERhdGFGZXRjaFRhYmxlV2lkZ2V0cyhzdGF0ZSk7XG4gICAgICAgIGNvbnN0IGNhdGFsb2dzID0gbWFpblNlbGVjdG9ycy5nZXRGZXRjaENhdGFsb2dEYXRhKHN0YXRlKTtcblxuICAgICAgICBjb25zdCBuZXdOdW0gPSBnZXROdW0odHlwZSwgcGFnZVdpZGdldHMpO1xuICAgICAgICBjb25zdCBuZXdOYW1lID0gYCR7dHlwZX0tJHtuZXdOdW19YDtcbiAgICAgICAgY29uc3Qge1xuICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQ6IGhhc1N1bW1hcnlTdXBwb3J0V2lkZ2V0LFxuICAgICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcbiAgICAgICAgICAgIHN0eWxlLFxuICAgICAgICAgICAgdmFsdWUsXG4gICAgICAgICAgICB3aWR0aCxcbiAgICAgICAgICAgIHgsXG4gICAgICAgICAgICB5LFxuICAgICAgICB9ID0gZ2V0RGVmYXVsdFdpZGdldFBvc2l0aW9uKFxuICAgICAgICAgICAgZ2V0RGVmYXVsdFdpZGdldFZhbHVlcyhcbiAgICAgICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcbiAgICAgICAgICAgICAgICB7IGNoYXJ0V2lkZ2V0cywgdGFibGVXaWRnZXRzIH0sXG4gICAgICAgICAgICAgICAgcGFnZURhdGEub3JpZW50YXRpb24sXG4gICAgICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQgPyBnZXRGaXJzdENhdGFsb2dJZChjYXRhbG9ncykgOiAnJyxcbiAgICAgICAgICAgICAgICBwYWdlRGF0YS50aGVtZSxcbiAgICAgICAgICAgICAgICB0eXBlXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgZGVmYXVsdFBvc2l0aW9uLFxuICAgICAgICAgICAgcGFnZURhdGEub3JpZW50YXRpb25cbiAgICAgICAgKTtcbiAgICAgICAgY29uc3QgbmV3V2lkZ2V0ID0ge1xuICAgICAgICAgICAgaWQ6IGAke3BhZ2VJZH0vJHtuZXdOYW1lfWAsXG4gICAgICAgICAgICBmaWVsZDogbmV3TmFtZSxcbiAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0OiBoYXNTdW1tYXJ5U3VwcG9ydFdpZGdldCxcbiAgICAgICAgICAgIGhlaWdodCxcbiAgICAgICAgICAgIG5hbWU6IHR5cGUsXG4gICAgICAgICAgICBudW06IG5ld051bSxcbiAgICAgICAgICAgIG9yZGVyOiBwYWdlV2lkZ2V0cy5sZW5ndGgsXG4gICAgICAgICAgICBwYWdlSWQsXG4gICAgICAgICAgICByZXBsYWNlSW5mb1JlcXVpcmVkLFxuICAgICAgICAgICAgc3R5bGUsXG4gICAgICAgICAgICBzdXBwb3J0VmVyc2lvbjogV0lER0VUX1NVUFBPUlRfVkVSU0lPTixcbiAgICAgICAgICAgIHZhbHVlLFxuICAgICAgICAgICAgd2lkdGgsXG4gICAgICAgICAgICB4LFxuICAgICAgICAgICAgeSxcbiAgICAgICAgfTtcblxuICAgICAgICBkaXNwYXRjaCh7XG4gICAgICAgICAgICB0eXBlOiBBRERfV0lER0VULFxuICAgICAgICAgICAgcGF5bG9hZDogbmV3V2lkZ2V0LFxuICAgICAgICB9KTtcbiAgICAgICAgZGlzcGF0Y2goc2VsZWN0V2lkZ2V0KG5ld1dpZGdldC5pZCkpO1xuICAgICAgICBvbkNoYW5nZUluUGFnZSgpO1xuICAgIH07XG4iXX0=