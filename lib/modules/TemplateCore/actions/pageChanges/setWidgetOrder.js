"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actionTypes = require("../../actionTypes");

var _helpers = require("../../helpers");

var selectors = _interopRequireWildcard(require("../../selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default(pageId, widgetId, moveType, onChangeInPage) {
  return function (dispatch, getState) {
    var state = getState();

    var _selectors$getEntitie = selectors.getEntitiesSession(state),
        Page = _selectors$getEntitie.Page;

    if (!pageId || !Page.idExists(pageId)) {
      return;
    }

    var pageModel = Page.withId(pageId);
    var pageWidgets = pageModel.widgets.toRefArray();
    var widgetIndex = pageWidgets.map(function (w) {
      return w.id;
    }).indexOf(widgetId);
    var newOrderIndex = (0, _helpers.getNewOrderIndex)(widgetIndex, moveType, pageWidgets.length);

    if (newOrderIndex !== null) {
      dispatch({
        type: _actionTypes.CHANGE_WIDGET_ORDER,
        payload: {
          pageId: pageId,
          widgetIndex: widgetIndex,
          newOrderIndex: newOrderIndex
        }
      });
      onChangeInPage();
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL3BhZ2VDaGFuZ2VzL3NldFdpZGdldE9yZGVyLmpzIl0sIm5hbWVzIjpbInBhZ2VJZCIsIndpZGdldElkIiwibW92ZVR5cGUiLCJvbkNoYW5nZUluUGFnZSIsImRpc3BhdGNoIiwiZ2V0U3RhdGUiLCJzdGF0ZSIsInNlbGVjdG9ycyIsImdldEVudGl0aWVzU2Vzc2lvbiIsIlBhZ2UiLCJpZEV4aXN0cyIsInBhZ2VNb2RlbCIsIndpdGhJZCIsInBhZ2VXaWRnZXRzIiwid2lkZ2V0cyIsInRvUmVmQXJyYXkiLCJ3aWRnZXRJbmRleCIsIm1hcCIsInciLCJpZCIsImluZGV4T2YiLCJuZXdPcmRlckluZGV4IiwibGVuZ3RoIiwidHlwZSIsIkNIQU5HRV9XSURHRVRfT1JERVIiLCJwYXlsb2FkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7Ozs7O2VBRWUsa0JBQUNBLE1BQUQsRUFBU0MsUUFBVCxFQUFtQkMsUUFBbkIsRUFBNkJDLGNBQTdCO0FBQUEsU0FDWCxVQUFDQyxRQUFELEVBQVdDLFFBQVgsRUFBd0I7QUFDcEIsUUFBTUMsS0FBSyxHQUFHRCxRQUFRLEVBQXRCOztBQUNBLGdDQUFpQkUsU0FBUyxDQUFDQyxrQkFBVixDQUE2QkYsS0FBN0IsQ0FBakI7QUFBQSxRQUFRRyxJQUFSLHlCQUFRQSxJQUFSOztBQUNBLFFBQUksQ0FBQ1QsTUFBRCxJQUFXLENBQUNTLElBQUksQ0FBQ0MsUUFBTCxDQUFjVixNQUFkLENBQWhCLEVBQXVDO0FBQ25DO0FBQ0g7O0FBQ0QsUUFBTVcsU0FBUyxHQUFHRixJQUFJLENBQUNHLE1BQUwsQ0FBWVosTUFBWixDQUFsQjtBQUNBLFFBQU1hLFdBQVcsR0FBR0YsU0FBUyxDQUFDRyxPQUFWLENBQWtCQyxVQUFsQixFQUFwQjtBQUNBLFFBQU1DLFdBQVcsR0FBR0gsV0FBVyxDQUFDSSxHQUFaLENBQWdCLFVBQUNDLENBQUQ7QUFBQSxhQUFPQSxDQUFDLENBQUNDLEVBQVQ7QUFBQSxLQUFoQixFQUE2QkMsT0FBN0IsQ0FBcUNuQixRQUFyQyxDQUFwQjtBQUNBLFFBQU1vQixhQUFhLEdBQUcsK0JBQ2xCTCxXQURrQixFQUVsQmQsUUFGa0IsRUFHbEJXLFdBQVcsQ0FBQ1MsTUFITSxDQUF0Qjs7QUFNQSxRQUFJRCxhQUFhLEtBQUssSUFBdEIsRUFBNEI7QUFDeEJqQixNQUFBQSxRQUFRLENBQUM7QUFDTG1CLFFBQUFBLElBQUksRUFBRUMsZ0NBREQ7QUFFTEMsUUFBQUEsT0FBTyxFQUFFO0FBQUV6QixVQUFBQSxNQUFNLEVBQU5BLE1BQUY7QUFBVWdCLFVBQUFBLFdBQVcsRUFBWEEsV0FBVjtBQUF1QkssVUFBQUEsYUFBYSxFQUFiQTtBQUF2QjtBQUZKLE9BQUQsQ0FBUjtBQUlBbEIsTUFBQUEsY0FBYztBQUNqQjtBQUNKLEdBdkJVO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENIQU5HRV9XSURHRVRfT1JERVIgfSBmcm9tICcuLi8uLi9hY3Rpb25UeXBlcyc7XG5pbXBvcnQgeyBnZXROZXdPcmRlckluZGV4IH0gZnJvbSAnLi4vLi4vaGVscGVycyc7XG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnLi4vLi4vc2VsZWN0b3JzJztcblxuZXhwb3J0IGRlZmF1bHQgKHBhZ2VJZCwgd2lkZ2V0SWQsIG1vdmVUeXBlLCBvbkNoYW5nZUluUGFnZSkgPT5cbiAgICAoZGlzcGF0Y2gsIGdldFN0YXRlKSA9PiB7XG4gICAgICAgIGNvbnN0IHN0YXRlID0gZ2V0U3RhdGUoKTtcbiAgICAgICAgY29uc3QgeyBQYWdlIH0gPSBzZWxlY3RvcnMuZ2V0RW50aXRpZXNTZXNzaW9uKHN0YXRlKTtcbiAgICAgICAgaWYgKCFwYWdlSWQgfHwgIVBhZ2UuaWRFeGlzdHMocGFnZUlkKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IHBhZ2VNb2RlbCA9IFBhZ2Uud2l0aElkKHBhZ2VJZCk7XG4gICAgICAgIGNvbnN0IHBhZ2VXaWRnZXRzID0gcGFnZU1vZGVsLndpZGdldHMudG9SZWZBcnJheSgpO1xuICAgICAgICBjb25zdCB3aWRnZXRJbmRleCA9IHBhZ2VXaWRnZXRzLm1hcCgodykgPT4gdy5pZCkuaW5kZXhPZih3aWRnZXRJZCk7XG4gICAgICAgIGNvbnN0IG5ld09yZGVySW5kZXggPSBnZXROZXdPcmRlckluZGV4KFxuICAgICAgICAgICAgd2lkZ2V0SW5kZXgsXG4gICAgICAgICAgICBtb3ZlVHlwZSxcbiAgICAgICAgICAgIHBhZ2VXaWRnZXRzLmxlbmd0aFxuICAgICAgICApO1xuXG4gICAgICAgIGlmIChuZXdPcmRlckluZGV4ICE9PSBudWxsKSB7XG4gICAgICAgICAgICBkaXNwYXRjaCh7XG4gICAgICAgICAgICAgICAgdHlwZTogQ0hBTkdFX1dJREdFVF9PUkRFUixcbiAgICAgICAgICAgICAgICBwYXlsb2FkOiB7IHBhZ2VJZCwgd2lkZ2V0SW5kZXgsIG5ld09yZGVySW5kZXggfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgb25DaGFuZ2VJblBhZ2UoKTtcbiAgICAgICAgfVxuICAgIH07XG4iXX0=