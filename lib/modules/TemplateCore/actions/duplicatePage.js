"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _arrayMove = _interopRequireDefault(require("array-move"));

var _sortBy = _interopRequireDefault(require("lodash/sortBy"));

var templateCoreSelectors = _interopRequireWildcard(require("../selectors"));

var _helpers = require("../helpers");

var _addNewPages = _interopRequireDefault(require("./addNewPages"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(pageId) {
  return function (dispatch, getState) {
    var updateData = templateCoreSelectors.getTemplateUpdatingContentData(getState());
    var indexOfPageId = updateData.pages_template.map(function (p) {
      return p.id;
    }).indexOf(pageId);

    if (indexOfPageId >= 0) {
      var duplicatedPage = _objectSpread(_objectSpread({}, updateData.pages_template[indexOfPageId]), {}, {
        id: "PageCopy-".concat(new Date().getTime())
      });

      var pagesTemplate = (0, _helpers.orderPages)((0, _arrayMove.default)([].concat(_toConsumableArray((0, _sortBy.default)(updateData.pages_template, [function (w) {
        return parseInt(w.page);
      }])), [duplicatedPage]), updateData.pages_template.length, parseInt(duplicatedPage.page)));
      dispatch((0, _addNewPages.default)(pagesTemplate));
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9hY3Rpb25zL2R1cGxpY2F0ZVBhZ2UuanMiXSwibmFtZXMiOlsicGFnZUlkIiwiZGlzcGF0Y2giLCJnZXRTdGF0ZSIsInVwZGF0ZURhdGEiLCJ0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMiLCJnZXRUZW1wbGF0ZVVwZGF0aW5nQ29udGVudERhdGEiLCJpbmRleE9mUGFnZUlkIiwicGFnZXNfdGVtcGxhdGUiLCJtYXAiLCJwIiwiaWQiLCJpbmRleE9mIiwiZHVwbGljYXRlZFBhZ2UiLCJEYXRlIiwiZ2V0VGltZSIsInBhZ2VzVGVtcGxhdGUiLCJ3IiwicGFyc2VJbnQiLCJwYWdlIiwibGVuZ3RoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFFZSxrQkFBQ0EsTUFBRDtBQUFBLFNBQVksVUFBQ0MsUUFBRCxFQUFXQyxRQUFYLEVBQXdCO0FBQy9DLFFBQU1DLFVBQVUsR0FBR0MscUJBQXFCLENBQUNDLDhCQUF0QixDQUNmSCxRQUFRLEVBRE8sQ0FBbkI7QUFHQSxRQUFNSSxhQUFhLEdBQUdILFVBQVUsQ0FBQ0ksY0FBWCxDQUNqQkMsR0FEaUIsQ0FDYixVQUFDQyxDQUFEO0FBQUEsYUFBT0EsQ0FBQyxDQUFDQyxFQUFUO0FBQUEsS0FEYSxFQUVqQkMsT0FGaUIsQ0FFVFgsTUFGUyxDQUF0Qjs7QUFHQSxRQUFJTSxhQUFhLElBQUksQ0FBckIsRUFBd0I7QUFDcEIsVUFBTU0sY0FBYyxtQ0FDYlQsVUFBVSxDQUFDSSxjQUFYLENBQTBCRCxhQUExQixDQURhO0FBRWhCSSxRQUFBQSxFQUFFLHFCQUFjLElBQUlHLElBQUosR0FBV0MsT0FBWCxFQUFkO0FBRmMsUUFBcEI7O0FBSUEsVUFBSUMsYUFBYSxHQUFHLHlCQUNoQixxREFFVyxxQkFBT1osVUFBVSxDQUFDSSxjQUFsQixFQUFrQyxDQUNqQyxVQUFDUyxDQUFELEVBQU87QUFDSCxlQUFPQyxRQUFRLENBQUNELENBQUMsQ0FBQ0UsSUFBSCxDQUFmO0FBQ0gsT0FIZ0MsQ0FBbEMsQ0FGWCxJQU9RTixjQVBSLElBU0lULFVBQVUsQ0FBQ0ksY0FBWCxDQUEwQlksTUFUOUIsRUFVSUYsUUFBUSxDQUFDTCxjQUFjLENBQUNNLElBQWhCLENBVlosQ0FEZ0IsQ0FBcEI7QUFjQWpCLE1BQUFBLFFBQVEsQ0FBQywwQkFBWWMsYUFBWixDQUFELENBQVI7QUFDSDtBQUNKLEdBNUJjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBhcnJheU1vdmUgZnJvbSAnYXJyYXktbW92ZSc7XHJcbmltcG9ydCBzb3J0QnkgZnJvbSAnbG9kYXNoL3NvcnRCeSc7XHJcblxyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xyXG5cclxuaW1wb3J0IHsgb3JkZXJQYWdlcyB9IGZyb20gJy4uL2hlbHBlcnMnO1xyXG5cclxuaW1wb3J0IGFkZE5ld1BhZ2VzIGZyb20gJy4vYWRkTmV3UGFnZXMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgKHBhZ2VJZCkgPT4gKGRpc3BhdGNoLCBnZXRTdGF0ZSkgPT4ge1xyXG4gICAgY29uc3QgdXBkYXRlRGF0YSA9IHRlbXBsYXRlQ29yZVNlbGVjdG9ycy5nZXRUZW1wbGF0ZVVwZGF0aW5nQ29udGVudERhdGEoXHJcbiAgICAgICAgZ2V0U3RhdGUoKVxyXG4gICAgKTtcclxuICAgIGNvbnN0IGluZGV4T2ZQYWdlSWQgPSB1cGRhdGVEYXRhLnBhZ2VzX3RlbXBsYXRlXHJcbiAgICAgICAgLm1hcCgocCkgPT4gcC5pZClcclxuICAgICAgICAuaW5kZXhPZihwYWdlSWQpO1xyXG4gICAgaWYgKGluZGV4T2ZQYWdlSWQgPj0gMCkge1xyXG4gICAgICAgIGNvbnN0IGR1cGxpY2F0ZWRQYWdlID0ge1xyXG4gICAgICAgICAgICAuLi51cGRhdGVEYXRhLnBhZ2VzX3RlbXBsYXRlW2luZGV4T2ZQYWdlSWRdLFxyXG4gICAgICAgICAgICBpZDogYFBhZ2VDb3B5LSR7bmV3IERhdGUoKS5nZXRUaW1lKCl9YCxcclxuICAgICAgICB9O1xyXG4gICAgICAgIGxldCBwYWdlc1RlbXBsYXRlID0gb3JkZXJQYWdlcyhcclxuICAgICAgICAgICAgYXJyYXlNb3ZlKFxyXG4gICAgICAgICAgICAgICAgW1xyXG4gICAgICAgICAgICAgICAgICAgIC4uLnNvcnRCeSh1cGRhdGVEYXRhLnBhZ2VzX3RlbXBsYXRlLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICh3KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQody5wYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgICAgICAgICBkdXBsaWNhdGVkUGFnZSxcclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICB1cGRhdGVEYXRhLnBhZ2VzX3RlbXBsYXRlLmxlbmd0aCxcclxuICAgICAgICAgICAgICAgIHBhcnNlSW50KGR1cGxpY2F0ZWRQYWdlLnBhZ2UpXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICAgIGRpc3BhdGNoKGFkZE5ld1BhZ2VzKHBhZ2VzVGVtcGxhdGUpKTtcclxuICAgIH1cclxufTtcclxuIl19