"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTemplateUpdatingContentData = exports.getModel = exports.getIsEmptyPages = exports.getEntitiesSession = exports.getCurrentTemplateVersion = exports.getCurrentTemplateType = exports.getCurrentTemplateTitle = exports.getCurrentTemplateProposalNumber = exports.getCurrentTemplatePages = exports.getCurrentTemplateModel = exports.getCurrentTemplateLanguage = exports.getCurrentTemplateId = exports.getCurrentTemplateFullData = exports.getCurrentTemplateData = exports.getCurrentTemplateCreatedAt = void 0;

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _sortBy = _interopRequireDefault(require("lodash/sortBy"));

var _reselect = require("reselect");

var selectors = _interopRequireWildcard(require("../main/selectors"));

var _orm = _interopRequireDefault(require("../../orm"));

var _helpers = require("./helpers");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getModel = (0, _reselect.createSelector)(selectors.getLibState, function (sunwiseTemplateCore) {
  return sunwiseTemplateCore.templateCore;
});
exports.getModel = getModel;
var getEntitiesSession = (0, _reselect.createSelector)(getModel, function (state) {
  return _orm.default.session(state.entities);
});
exports.getEntitiesSession = getEntitiesSession;
var getCurrentTemplateId = (0, _reselect.createSelector)(getModel, function (model) {
  return model.currentTemplateId;
});
exports.getCurrentTemplateId = getCurrentTemplateId;
var getCurrentTemplateModel = (0, _reselect.createSelector)(getEntitiesSession, getCurrentTemplateId, function (_ref, currentTemplateId) {
  var Template = _ref.Template;

  if (currentTemplateId === null || !Template.idExists(currentTemplateId)) {
    return null;
  }

  return Template.withId(currentTemplateId);
});
exports.getCurrentTemplateModel = getCurrentTemplateModel;
var getCurrentTemplatePages = (0, _reselect.createSelector)(getCurrentTemplateModel, function (currentTemplateModel) {
  if (currentTemplateModel === null) {
    return [];
  }

  var pages = currentTemplateModel.templatePages.toModelArray().map(function (pageModel) {
    return _objectSpread(_objectSpread({}, pageModel.ref), {}, {
      type: 'page',
      widgets: pageModel.widgets.toRefArray()
    });
  });
  var groups = currentTemplateModel.groups.toModelArray().map(function (groupModel) {
    var pagesInGroup = groupModel.groupPages.toModelArray().map(function (pageModel) {
      return _objectSpread(_objectSpread({}, pageModel.ref), {}, {
        type: 'page',
        widgets: pageModel.widgets.toRefArray()
      });
    });
    return _objectSpread(_objectSpread({}, groupModel.ref), {}, {
      type: 'group',
      pages: pagesInGroup
    });
  });
  return (0, _sortBy.default)([].concat(_toConsumableArray(pages), _toConsumableArray(groups)), [function (p) {
    return parseInt(p.page);
  }]);
});
exports.getCurrentTemplatePages = getCurrentTemplatePages;
var getIsEmptyPages = (0, _reselect.createSelector)(getCurrentTemplatePages, function (currentTemplatePages) {
  return (0, _isEmpty.default)(currentTemplatePages);
});
exports.getIsEmptyPages = getIsEmptyPages;
var getCurrentTemplateData = (0, _reselect.createSelector)(getCurrentTemplateModel, function (currentTemplateModel) {
  return currentTemplateModel && currentTemplateModel.ref;
});
exports.getCurrentTemplateData = getCurrentTemplateData;
var getCurrentTemplateLanguage = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.language;
});
exports.getCurrentTemplateLanguage = getCurrentTemplateLanguage;
var getCurrentTemplateType = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.type;
});
exports.getCurrentTemplateType = getCurrentTemplateType;
var getCurrentTemplateTitle = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.title;
});
exports.getCurrentTemplateTitle = getCurrentTemplateTitle;
var getCurrentTemplateCreatedAt = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.created_at;
});
exports.getCurrentTemplateCreatedAt = getCurrentTemplateCreatedAt;
var getCurrentTemplateVersion = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.version;
});
exports.getCurrentTemplateVersion = getCurrentTemplateVersion;
var getCurrentTemplateProposalNumber = (0, _reselect.createSelector)(getCurrentTemplateData, function (currentTemplateData) {
  return currentTemplateData && currentTemplateData.proposals_number;
});
exports.getCurrentTemplateProposalNumber = getCurrentTemplateProposalNumber;
var getCurrentTemplateFullData = (0, _reselect.createSelector)(getCurrentTemplateData, getCurrentTemplatePages, function (currentTemplateData, currentTemplatePages) {
  if (currentTemplateData === null) {
    return null;
  }

  return _objectSpread(_objectSpread({}, currentTemplateData), {}, {
    pages: currentTemplatePages
  });
});
exports.getCurrentTemplateFullData = getCurrentTemplateFullData;
var getTemplateUpdatingContentData = (0, _reselect.createSelector)(getCurrentTemplateFullData, function (currentTemplateFullData) {
  if (currentTemplateFullData === null) {
    return null;
  }

  return (0, _helpers.getUpdatingContentData)(currentTemplateFullData);
});
exports.getTemplateUpdatingContentData = getTemplateUpdatingContentData;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9zZWxlY3RvcnMuanMiXSwibmFtZXMiOlsiZ2V0TW9kZWwiLCJzZWxlY3RvcnMiLCJnZXRMaWJTdGF0ZSIsInN1bndpc2VUZW1wbGF0ZUNvcmUiLCJ0ZW1wbGF0ZUNvcmUiLCJnZXRFbnRpdGllc1Nlc3Npb24iLCJzdGF0ZSIsIm9ybSIsInNlc3Npb24iLCJlbnRpdGllcyIsImdldEN1cnJlbnRUZW1wbGF0ZUlkIiwibW9kZWwiLCJjdXJyZW50VGVtcGxhdGVJZCIsImdldEN1cnJlbnRUZW1wbGF0ZU1vZGVsIiwiVGVtcGxhdGUiLCJpZEV4aXN0cyIsIndpdGhJZCIsImdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzIiwiY3VycmVudFRlbXBsYXRlTW9kZWwiLCJwYWdlcyIsInRlbXBsYXRlUGFnZXMiLCJ0b01vZGVsQXJyYXkiLCJtYXAiLCJwYWdlTW9kZWwiLCJyZWYiLCJ0eXBlIiwid2lkZ2V0cyIsInRvUmVmQXJyYXkiLCJncm91cHMiLCJncm91cE1vZGVsIiwicGFnZXNJbkdyb3VwIiwiZ3JvdXBQYWdlcyIsInAiLCJwYXJzZUludCIsInBhZ2UiLCJnZXRJc0VtcHR5UGFnZXMiLCJjdXJyZW50VGVtcGxhdGVQYWdlcyIsImdldEN1cnJlbnRUZW1wbGF0ZURhdGEiLCJnZXRDdXJyZW50VGVtcGxhdGVMYW5ndWFnZSIsImN1cnJlbnRUZW1wbGF0ZURhdGEiLCJsYW5ndWFnZSIsImdldEN1cnJlbnRUZW1wbGF0ZVR5cGUiLCJnZXRDdXJyZW50VGVtcGxhdGVUaXRsZSIsInRpdGxlIiwiZ2V0Q3VycmVudFRlbXBsYXRlQ3JlYXRlZEF0IiwiY3JlYXRlZF9hdCIsImdldEN1cnJlbnRUZW1wbGF0ZVZlcnNpb24iLCJ2ZXJzaW9uIiwiZ2V0Q3VycmVudFRlbXBsYXRlUHJvcG9zYWxOdW1iZXIiLCJwcm9wb3NhbHNfbnVtYmVyIiwiZ2V0Q3VycmVudFRlbXBsYXRlRnVsbERhdGEiLCJnZXRUZW1wbGF0ZVVwZGF0aW5nQ29udGVudERhdGEiLCJjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRU8sSUFBTUEsUUFBUSxHQUFHLDhCQUNwQkMsU0FBUyxDQUFDQyxXQURVLEVBRXBCLFVBQUNDLG1CQUFEO0FBQUEsU0FBeUJBLG1CQUFtQixDQUFDQyxZQUE3QztBQUFBLENBRm9CLENBQWpCOztBQUtBLElBQU1DLGtCQUFrQixHQUFHLDhCQUFlTCxRQUFmLEVBQXlCLFVBQUNNLEtBQUQ7QUFBQSxTQUN2REMsYUFBSUMsT0FBSixDQUFZRixLQUFLLENBQUNHLFFBQWxCLENBRHVEO0FBQUEsQ0FBekIsQ0FBM0I7O0FBSUEsSUFBTUMsb0JBQW9CLEdBQUcsOEJBQ2hDVixRQURnQyxFQUVoQyxVQUFDVyxLQUFEO0FBQUEsU0FBV0EsS0FBSyxDQUFDQyxpQkFBakI7QUFBQSxDQUZnQyxDQUE3Qjs7QUFLQSxJQUFNQyx1QkFBdUIsR0FBRyw4QkFDbkNSLGtCQURtQyxFQUVuQ0ssb0JBRm1DLEVBR25DLGdCQUFlRSxpQkFBZixFQUFxQztBQUFBLE1BQWxDRSxRQUFrQyxRQUFsQ0EsUUFBa0M7O0FBQ2pDLE1BQ0lGLGlCQUFpQixLQUFLLElBQXRCLElBQ0EsQ0FBQ0UsUUFBUSxDQUFDQyxRQUFULENBQWtCSCxpQkFBbEIsQ0FGTCxFQUdFO0FBQ0UsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsU0FBT0UsUUFBUSxDQUFDRSxNQUFULENBQWdCSixpQkFBaEIsQ0FBUDtBQUNILENBWGtDLENBQWhDOztBQWNBLElBQU1LLHVCQUF1QixHQUFHLDhCQUNuQ0osdUJBRG1DLEVBRW5DLFVBQUNLLG9CQUFELEVBQTBCO0FBQ3RCLE1BQUlBLG9CQUFvQixLQUFLLElBQTdCLEVBQW1DO0FBQy9CLFdBQU8sRUFBUDtBQUNIOztBQUNELE1BQU1DLEtBQUssR0FBR0Qsb0JBQW9CLENBQUNFLGFBQXJCLENBQ1RDLFlBRFMsR0FFVEMsR0FGUyxDQUVMLFVBQUNDLFNBQUQsRUFBZTtBQUNoQiwyQ0FDT0EsU0FBUyxDQUFDQyxHQURqQjtBQUVJQyxNQUFBQSxJQUFJLEVBQUUsTUFGVjtBQUdJQyxNQUFBQSxPQUFPLEVBQUVILFNBQVMsQ0FBQ0csT0FBVixDQUFrQkMsVUFBbEI7QUFIYjtBQUtILEdBUlMsQ0FBZDtBQVNBLE1BQU1DLE1BQU0sR0FBR1Ysb0JBQW9CLENBQUNVLE1BQXJCLENBQ1ZQLFlBRFUsR0FFVkMsR0FGVSxDQUVOLFVBQUNPLFVBQUQsRUFBZ0I7QUFDakIsUUFBTUMsWUFBWSxHQUFHRCxVQUFVLENBQUNFLFVBQVgsQ0FDaEJWLFlBRGdCLEdBRWhCQyxHQUZnQixDQUVaLFVBQUNDLFNBQUQsRUFBZTtBQUNoQiw2Q0FDT0EsU0FBUyxDQUFDQyxHQURqQjtBQUVJQyxRQUFBQSxJQUFJLEVBQUUsTUFGVjtBQUdJQyxRQUFBQSxPQUFPLEVBQUVILFNBQVMsQ0FBQ0csT0FBVixDQUFrQkMsVUFBbEI7QUFIYjtBQUtILEtBUmdCLENBQXJCO0FBU0EsMkNBQ09FLFVBQVUsQ0FBQ0wsR0FEbEI7QUFFSUMsTUFBQUEsSUFBSSxFQUFFLE9BRlY7QUFHSU4sTUFBQUEsS0FBSyxFQUFFVztBQUhYO0FBS0gsR0FqQlUsQ0FBZjtBQWtCQSxTQUFPLGtEQUNDWCxLQURELHNCQUNXUyxNQURYLElBRUgsQ0FDSSxVQUFDSSxDQUFELEVBQU87QUFDSCxXQUFPQyxRQUFRLENBQUNELENBQUMsQ0FBQ0UsSUFBSCxDQUFmO0FBQ0gsR0FITCxDQUZHLENBQVA7QUFRSCxDQXpDa0MsQ0FBaEM7O0FBNENBLElBQU1DLGVBQWUsR0FBRyw4QkFDM0JsQix1QkFEMkIsRUFFM0IsVUFBQ21CLG9CQUFEO0FBQUEsU0FBMEIsc0JBQVFBLG9CQUFSLENBQTFCO0FBQUEsQ0FGMkIsQ0FBeEI7O0FBS0EsSUFBTUMsc0JBQXNCLEdBQUcsOEJBQ2xDeEIsdUJBRGtDLEVBRWxDLFVBQUNLLG9CQUFEO0FBQUEsU0FBMEJBLG9CQUFvQixJQUFJQSxvQkFBb0IsQ0FBQ00sR0FBdkU7QUFBQSxDQUZrQyxDQUEvQjs7QUFLQSxJQUFNYywwQkFBMEIsR0FBRyw4QkFDdENELHNCQURzQyxFQUV0QyxVQUFDRSxtQkFBRDtBQUFBLFNBQXlCQSxtQkFBbUIsSUFBSUEsbUJBQW1CLENBQUNDLFFBQXBFO0FBQUEsQ0FGc0MsQ0FBbkM7O0FBS0EsSUFBTUMsc0JBQXNCLEdBQUcsOEJBQ2xDSixzQkFEa0MsRUFFbEMsVUFBQ0UsbUJBQUQ7QUFBQSxTQUF5QkEsbUJBQW1CLElBQUlBLG1CQUFtQixDQUFDZCxJQUFwRTtBQUFBLENBRmtDLENBQS9COztBQUtBLElBQU1pQix1QkFBdUIsR0FBRyw4QkFDbkNMLHNCQURtQyxFQUVuQyxVQUFDRSxtQkFBRDtBQUFBLFNBQXlCQSxtQkFBbUIsSUFBSUEsbUJBQW1CLENBQUNJLEtBQXBFO0FBQUEsQ0FGbUMsQ0FBaEM7O0FBS0EsSUFBTUMsMkJBQTJCLEdBQUcsOEJBQ3ZDUCxzQkFEdUMsRUFFdkMsVUFBQ0UsbUJBQUQ7QUFBQSxTQUNJQSxtQkFBbUIsSUFBSUEsbUJBQW1CLENBQUNNLFVBRC9DO0FBQUEsQ0FGdUMsQ0FBcEM7O0FBTUEsSUFBTUMseUJBQXlCLEdBQUcsOEJBQ3JDVCxzQkFEcUMsRUFFckMsVUFBQ0UsbUJBQUQ7QUFBQSxTQUF5QkEsbUJBQW1CLElBQUlBLG1CQUFtQixDQUFDUSxPQUFwRTtBQUFBLENBRnFDLENBQWxDOztBQUtBLElBQU1DLGdDQUFnQyxHQUFHLDhCQUM1Q1gsc0JBRDRDLEVBRTVDLFVBQUNFLG1CQUFEO0FBQUEsU0FDSUEsbUJBQW1CLElBQUlBLG1CQUFtQixDQUFDVSxnQkFEL0M7QUFBQSxDQUY0QyxDQUF6Qzs7QUFNQSxJQUFNQywwQkFBMEIsR0FBRyw4QkFDdENiLHNCQURzQyxFQUV0Q3BCLHVCQUZzQyxFQUd0QyxVQUFDc0IsbUJBQUQsRUFBc0JILG9CQUF0QixFQUErQztBQUMzQyxNQUFJRyxtQkFBbUIsS0FBSyxJQUE1QixFQUFrQztBQUM5QixXQUFPLElBQVA7QUFDSDs7QUFDRCx5Q0FDT0EsbUJBRFA7QUFFSXBCLElBQUFBLEtBQUssRUFBRWlCO0FBRlg7QUFJSCxDQVhxQyxDQUFuQzs7QUFjQSxJQUFNZSw4QkFBOEIsR0FBRyw4QkFDMUNELDBCQUQwQyxFQUUxQyxVQUFDRSx1QkFBRCxFQUE2QjtBQUN6QixNQUFJQSx1QkFBdUIsS0FBSyxJQUFoQyxFQUFzQztBQUNsQyxXQUFPLElBQVA7QUFDSDs7QUFDRCxTQUFPLHFDQUF1QkEsdUJBQXZCLENBQVA7QUFDSCxDQVB5QyxDQUF2QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBpc0VtcHR5IGZyb20gJ2xvZGFzaC9pc0VtcHR5JztcclxuaW1wb3J0IHNvcnRCeSBmcm9tICdsb2Rhc2gvc29ydEJ5JztcclxuaW1wb3J0IHsgY3JlYXRlU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XHJcblxyXG5pbXBvcnQgKiBhcyBzZWxlY3RvcnMgZnJvbSAnQG1haW4vc2VsZWN0b3JzJztcclxuXHJcbmltcG9ydCBvcm0gZnJvbSAnQG9ybSc7XHJcblxyXG5pbXBvcnQgeyBnZXRVcGRhdGluZ0NvbnRlbnREYXRhIH0gZnJvbSAnLi9oZWxwZXJzJztcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRNb2RlbCA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgc2VsZWN0b3JzLmdldExpYlN0YXRlLFxyXG4gICAgKHN1bndpc2VUZW1wbGF0ZUNvcmUpID0+IHN1bndpc2VUZW1wbGF0ZUNvcmUudGVtcGxhdGVDb3JlXHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0RW50aXRpZXNTZXNzaW9uID0gY3JlYXRlU2VsZWN0b3IoZ2V0TW9kZWwsIChzdGF0ZSkgPT5cclxuICAgIG9ybS5zZXNzaW9uKHN0YXRlLmVudGl0aWVzKVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRUZW1wbGF0ZUlkID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRNb2RlbCxcclxuICAgIChtb2RlbCkgPT4gbW9kZWwuY3VycmVudFRlbXBsYXRlSWRcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDdXJyZW50VGVtcGxhdGVNb2RlbCA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0RW50aXRpZXNTZXNzaW9uLFxyXG4gICAgZ2V0Q3VycmVudFRlbXBsYXRlSWQsXHJcbiAgICAoeyBUZW1wbGF0ZSB9LCBjdXJyZW50VGVtcGxhdGVJZCkgPT4ge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlSWQgPT09IG51bGwgfHxcclxuICAgICAgICAgICAgIVRlbXBsYXRlLmlkRXhpc3RzKGN1cnJlbnRUZW1wbGF0ZUlkKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIFRlbXBsYXRlLndpdGhJZChjdXJyZW50VGVtcGxhdGVJZCk7XHJcbiAgICB9XHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Q3VycmVudFRlbXBsYXRlUGFnZXMgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZU1vZGVsLFxyXG4gICAgKGN1cnJlbnRUZW1wbGF0ZU1vZGVsKSA9PiB7XHJcbiAgICAgICAgaWYgKGN1cnJlbnRUZW1wbGF0ZU1vZGVsID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgcGFnZXMgPSBjdXJyZW50VGVtcGxhdGVNb2RlbC50ZW1wbGF0ZVBhZ2VzXHJcbiAgICAgICAgICAgIC50b01vZGVsQXJyYXkoKVxyXG4gICAgICAgICAgICAubWFwKChwYWdlTW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgLi4ucGFnZU1vZGVsLnJlZixcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAncGFnZScsXHJcbiAgICAgICAgICAgICAgICAgICAgd2lkZ2V0czogcGFnZU1vZGVsLndpZGdldHMudG9SZWZBcnJheSgpLFxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc3QgZ3JvdXBzID0gY3VycmVudFRlbXBsYXRlTW9kZWwuZ3JvdXBzXHJcbiAgICAgICAgICAgIC50b01vZGVsQXJyYXkoKVxyXG4gICAgICAgICAgICAubWFwKChncm91cE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwYWdlc0luR3JvdXAgPSBncm91cE1vZGVsLmdyb3VwUGFnZXNcclxuICAgICAgICAgICAgICAgICAgICAudG9Nb2RlbEFycmF5KClcclxuICAgICAgICAgICAgICAgICAgICAubWFwKChwYWdlTW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLnBhZ2VNb2RlbC5yZWYsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAncGFnZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRzOiBwYWdlTW9kZWwud2lkZ2V0cy50b1JlZkFycmF5KCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC4uLmdyb3VwTW9kZWwucmVmLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdncm91cCcsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZXM6IHBhZ2VzSW5Hcm91cCxcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBzb3J0QnkoXHJcbiAgICAgICAgICAgIFsuLi5wYWdlcywgLi4uZ3JvdXBzXSxcclxuICAgICAgICAgICAgW1xyXG4gICAgICAgICAgICAgICAgKHApID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQocC5wYWdlKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldElzRW1wdHlQYWdlcyA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0Q3VycmVudFRlbXBsYXRlUGFnZXMsXHJcbiAgICAoY3VycmVudFRlbXBsYXRlUGFnZXMpID0+IGlzRW1wdHkoY3VycmVudFRlbXBsYXRlUGFnZXMpXHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Q3VycmVudFRlbXBsYXRlRGF0YSA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0Q3VycmVudFRlbXBsYXRlTW9kZWwsXHJcbiAgICAoY3VycmVudFRlbXBsYXRlTW9kZWwpID0+IGN1cnJlbnRUZW1wbGF0ZU1vZGVsICYmIGN1cnJlbnRUZW1wbGF0ZU1vZGVsLnJlZlxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRUZW1wbGF0ZUxhbmd1YWdlID0gY3JlYXRlU2VsZWN0b3IoXHJcbiAgICBnZXRDdXJyZW50VGVtcGxhdGVEYXRhLFxyXG4gICAgKGN1cnJlbnRUZW1wbGF0ZURhdGEpID0+IGN1cnJlbnRUZW1wbGF0ZURhdGEgJiYgY3VycmVudFRlbXBsYXRlRGF0YS5sYW5ndWFnZVxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRUZW1wbGF0ZVR5cGUgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZURhdGEsXHJcbiAgICAoY3VycmVudFRlbXBsYXRlRGF0YSkgPT4gY3VycmVudFRlbXBsYXRlRGF0YSAmJiBjdXJyZW50VGVtcGxhdGVEYXRhLnR5cGVcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDdXJyZW50VGVtcGxhdGVUaXRsZSA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0Q3VycmVudFRlbXBsYXRlRGF0YSxcclxuICAgIChjdXJyZW50VGVtcGxhdGVEYXRhKSA9PiBjdXJyZW50VGVtcGxhdGVEYXRhICYmIGN1cnJlbnRUZW1wbGF0ZURhdGEudGl0bGVcclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDdXJyZW50VGVtcGxhdGVDcmVhdGVkQXQgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZURhdGEsXHJcbiAgICAoY3VycmVudFRlbXBsYXRlRGF0YSkgPT5cclxuICAgICAgICBjdXJyZW50VGVtcGxhdGVEYXRhICYmIGN1cnJlbnRUZW1wbGF0ZURhdGEuY3JlYXRlZF9hdFxyXG4pO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbnRUZW1wbGF0ZVZlcnNpb24gPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZURhdGEsXHJcbiAgICAoY3VycmVudFRlbXBsYXRlRGF0YSkgPT4gY3VycmVudFRlbXBsYXRlRGF0YSAmJiBjdXJyZW50VGVtcGxhdGVEYXRhLnZlcnNpb25cclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRDdXJyZW50VGVtcGxhdGVQcm9wb3NhbE51bWJlciA9IGNyZWF0ZVNlbGVjdG9yKFxyXG4gICAgZ2V0Q3VycmVudFRlbXBsYXRlRGF0YSxcclxuICAgIChjdXJyZW50VGVtcGxhdGVEYXRhKSA9PlxyXG4gICAgICAgIGN1cnJlbnRUZW1wbGF0ZURhdGEgJiYgY3VycmVudFRlbXBsYXRlRGF0YS5wcm9wb3NhbHNfbnVtYmVyXHJcbik7XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Q3VycmVudFRlbXBsYXRlRnVsbERhdGEgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZURhdGEsXHJcbiAgICBnZXRDdXJyZW50VGVtcGxhdGVQYWdlcyxcclxuICAgIChjdXJyZW50VGVtcGxhdGVEYXRhLCBjdXJyZW50VGVtcGxhdGVQYWdlcykgPT4ge1xyXG4gICAgICAgIGlmIChjdXJyZW50VGVtcGxhdGVEYXRhID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAuLi5jdXJyZW50VGVtcGxhdGVEYXRhLFxyXG4gICAgICAgICAgICBwYWdlczogY3VycmVudFRlbXBsYXRlUGFnZXMsXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuKTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRUZW1wbGF0ZVVwZGF0aW5nQ29udGVudERhdGEgPSBjcmVhdGVTZWxlY3RvcihcclxuICAgIGdldEN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLFxyXG4gICAgKGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZ2V0VXBkYXRpbmdDb250ZW50RGF0YShjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSk7XHJcbiAgICB9XHJcbik7XHJcbiJdfQ==