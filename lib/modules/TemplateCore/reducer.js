"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = _default;

var _arrayMove = _interopRequireDefault(require("array-move"));

var _immutabilityHelper = _interopRequireDefault(require("immutability-helper"));

var _difference = _interopRequireDefault(require("lodash/difference"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _sortBy = _interopRequireDefault(require("lodash/sortBy"));

var _orm = _interopRequireDefault(require("../../orm"));

var _actionTypes = require("./actionTypes");

var _helpers = require("./helpers");

var _excluded = ["pages_template"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var INITIAL_STATE = {
  entities: _orm.default.getEmptyState(),
  currentTemplateId: null,
  templateBackUp: null
};

function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : INITIAL_STATE;
  var action = arguments.length > 1 ? arguments[1] : undefined;

  var session = _orm.default.session(state.entities);

  var Group = session.Group,
      Page = session.Page,
      Template = session.Template,
      Widget = session.Widget;

  switch (action.type) {
    case _actionTypes.PASTE_WIDGET:
    case _actionTypes.ADD_WIDGET:
      {
        Widget.create(action.payload);
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.ADD_NEW_PAGES:
      {
        var pagesTemplate = action.payload;
        var templateModel = Template.withId(state.currentTemplateId);
        var pageIds = templateModel ? templateModel.templatePages.toRefArray().map(function (p) {
          return p.id;
        }) : [];
        pagesTemplate.filter(function (p) {
          return (0, _isNil.default)(p.grouped_by);
        }).forEach(function (p) {
          if (pageIds.includes(p.id)) {
            Page.withId(p.id).set('page', p.page);
          } else {
            (0, _helpers.createPage)({
              pageModel: Page,
              tempPage: p,
              templateId: state.currentTemplateId,
              widgetModel: Widget
            });
          }
        });
        var groupIds = templateModel.groups.toRefArray().map(function (g) {
          return g.id;
        });
        var groups = (0, _helpers.createGroups)(pagesTemplate);

        for (var key in groups) {
          if (groupIds.includes(key)) {
            Group.withId(key).set('page', groups[key].firstPage);

            var _iterator = _createForOfIteratorHelper(groups[key].pages),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var p = _step.value;
                Page.withId(p.id).set('page', p.page);
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          } else {
            (0, _helpers.createGroup)({
              group: groups[key],
              groupModel: Group,
              pageModel: Page,
              templateId: state.currentTemplateId,
              widgetModel: Widget
            });
          }
        }

        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.CHANGE_PAGE_INFINITE_MODE:
      Page.withId(action.payload.pageId).set('infiniteMode', action.payload.infiniteMode);
      return (0, _immutabilityHelper.default)(state, {
        entities: {
          $set: session.state
        }
      });

    case _actionTypes.CHANGE_PAGE_ORIENTATION:
      {
        var _action$payload = action.payload,
            orientation = _action$payload.orientation,
            pageId = _action$payload.pageId;
        Page.withId(pageId).widgets.toModelArray().forEach(function (widgetModel) {
          var _widgetModel$ref = widgetModel.ref,
              width = _widgetModel$ref.width,
              height = _widgetModel$ref.height,
              x = _widgetModel$ref.x,
              y = _widgetModel$ref.y;
          var widgetWidth = 0 + width;
          var widgetHeight = 0 + height;

          var _getLimits = (0, _helpers.getLimits)(orientation),
              limitX = _getLimits.x,
              limitY = _getLimits.y;

          if (widgetWidth > limitX) {
            widgetWidth = limitX;
            widgetModel.set('width', widgetWidth);
          }

          if (widgetHeight > limitY) {
            widgetHeight = limitY;
            widgetModel.set('height', widgetHeight);
          }

          if (x + widgetWidth > limitX) {
            widgetModel.set('x', limitX - widgetWidth);
          }

          if (y + widgetHeight > limitY) {
            widgetModel.set('y', limitY - widgetHeight);
          }
        });
        Page.withId(pageId).set('orientation', orientation);
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.CHANGE_PAGE_THEME:
      Page.withId(action.payload.pageId).set('theme', action.payload.theme);
      return (0, _immutabilityHelper.default)(state, {
        entities: {
          $set: session.state
        }
      });

    case _actionTypes.CHANGE_WIDGET_ORDER:
      {
        var _action$payload2 = action.payload,
            _pageId = _action$payload2.pageId,
            newOrderIndex = _action$payload2.newOrderIndex,
            widgetIndex = _action$payload2.widgetIndex;
        (0, _arrayMove.default)((0, _sortBy.default)(Page.withId(_pageId).widgets.toModelArray(), [function (w) {
          return parseInt(w.ref.order);
        }]), widgetIndex, newOrderIndex).forEach(function (widgetModel, index) {
          widgetModel.set('order', index);
        });
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.CHANGE_WIDGET_POSITION:
      {
        var widgetModel = Widget.withId(action.payload.widgetId);
        widgetModel.set('x', action.payload.changedX);
        widgetModel.set('y', action.payload.changedY);
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.CHANGE_WIDGET_SIZE:
      {
        var _widgetModel = Widget.withId(action.payload.widgetId);

        _widgetModel.set('width', _widgetModel.ref.width + action.payload.changedWidth);

        _widgetModel.set('height', _widgetModel.ref.height + action.payload.changedHeight);

        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.CHANGE_WIDGET_STYLE:
      Widget.withId(action.payload.widgetId).set('style', action.payload.style);
      return (0, _immutabilityHelper.default)(state, {
        entities: {
          $set: session.state
        }
      });

    case _actionTypes.CHANGE_WIDGET_VALUE:
      Widget.withId(action.payload.widgetId).set('value', action.payload.value);
      return (0, _immutabilityHelper.default)(state, {
        entities: {
          $set: session.state
        }
      });

    case _actionTypes.DELETE_PAGES:
      {
        var _pagesTemplate = action.payload;
        (0, _difference.default)(Template.withId(state.currentTemplateId).templatePages.toRefArray().map(function (p) {
          return p.id;
        }), _pagesTemplate.filter(function (p) {
          return (0, _isNil.default)(p.grouped_by);
        }).map(function (p) {
          return p.id;
        })).forEach(function (p) {
          Page.withId(p).widgets.delete();
          Page.withId(p).delete();
        });
        (0, _difference.default)(Template.withId(state.currentTemplateId).templatePages.toRefArray().map(function (p) {
          return p.id;
        }), _pagesTemplate.filter(function (p) {
          return (0, _isNil.default)(p.grouped_by);
        }).map(function (p) {
          return p.id;
        })).forEach(function (p) {
          Page.withId(p).widgets.delete();
          Page.withId(p).delete();
        });
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.DELETE_WIDGET:
      Widget.withId(action.payload).delete();
      return (0, _immutabilityHelper.default)(state, {
        entities: {
          $set: session.state
        }
      });

    case _actionTypes.INITIALIZE:
      {
        var _action$payload3 = action.payload,
            _pagesTemplate2 = _action$payload3.pages_template,
            templateData = _objectWithoutProperties(_action$payload3, _excluded);

        Template.create(_objectSpread({}, templateData));

        _pagesTemplate2.filter(function (p) {
          return (0, _isNil.default)(p.grouped_by);
        }).forEach(function (p) {
          return (0, _helpers.createPage)({
            pageModel: Page,
            tempPage: p,
            templateId: templateData.id,
            widgetModel: Widget
          });
        });

        var _groups = (0, _helpers.createGroups)(_pagesTemplate2);

        for (var groupKey in _groups) {
          (0, _helpers.createGroup)({
            group: _groups[groupKey],
            groupModel: Group,
            pageModel: Page,
            templateId: templateData.id,
            widgetModel: Widget
          });
        }

        return (0, _immutabilityHelper.default)(state, {
          currentTemplateId: {
            $set: templateData.id
          },
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.RE_ORDER_PAGES:
      {
        var _pagesTemplate3 = action.payload;

        _pagesTemplate3.forEach(function (p) {
          if (Page.idExists(p.id)) {
            Page.withId(p.id).set('page', p.page);
          }
        });

        var _groups2 = (0, _helpers.createGroups)(_pagesTemplate3);

        for (var _key in _groups2) {
          var currentGroup = _groups2[_key];
          Group.withId(currentGroup.id).set('page', currentGroup.firstPage);
        }

        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.RESET:
    case _actionTypes.RESET_TEMPLATE:
      return (0, _immutabilityHelper.default)(state, {
        $set: INITIAL_STATE
      });

    case _actionTypes.SET_PAGE_DATA:
      {
        var _action$payload4 = action.payload,
            id = _action$payload4.id,
            infiniteMode = _action$payload4.infiniteMode,
            _orientation = _action$payload4.orientation,
            theme = _action$payload4.theme,
            widgets = _action$payload4.widgets;
        Page.withId(id).update({
          infiniteMode: infiniteMode,
          orientation: _orientation,
          theme: theme
        });
        (0, _difference.default)(Page.withId(id).widgets.toRefArray().map(function (w) {
          return w.id;
        }), widgets.map(function (w) {
          return w.id;
        })).forEach(function (widgetId) {
          Widget.withId(widgetId).delete();
        });
        widgets.forEach(function (widget) {
          if (Widget.idExists(widget.id)) {
            Widget.withId(widget.id).update(widget);
          } else {
            Widget.create(widget);
          }
        });
        return (0, _immutabilityHelper.default)(state, {
          entities: {
            $set: session.state
          }
        });
      }

    case _actionTypes.SET_TEMPLATE_BACKUP:
      return (0, _immutabilityHelper.default)(state, {
        templateBackUp: {
          $set: action.payload
        }
      });

    case _actionTypes.RESTORE_REVIEW_PAGES:
      {
        var backupPages = JSON.parse(state.templateBackUp);
        backupPages.forEach(function (p) {
          if (Page.idExists(p.id)) {
            Page.withId(p.id).update({
              infiniteMode: p.infiniteMode,
              orientation: p.orientation,
              theme: p.theme
            });
            (0, _difference.default)(Page.withId(p.id).widgets.toRefArray().map(function (w) {
              return w.id;
            }), p.widgets.map(function (w) {
              return w.id;
            })).forEach(function (widgetId) {
              Widget.withId(widgetId).delete();
            });
            p.widgets.forEach(function (widget) {
              if (Widget.idExists(widget.id)) {
                Widget.withId(widget.id).update(widget);
              } else {
                Widget.create(widget);
              }
            });
          }
        });
        (0, _difference.default)(Template.withId(state.currentTemplateId).templatePages.toRefArray().map(function (p) {
          return p.id;
        }), backupPages.map(function (p) {
          return p.id;
        })).forEach(function (p) {
          Page.withId(p).widgets.delete();
          Page.withId(p).delete();
        });
        return (0, _immutabilityHelper.default)(state, {
          templateBackUp: {
            $set: null
          },
          entities: {
            $set: session.state
          }
        });
      }

    default:
      return state;
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9yZWR1Y2VyLmpzIl0sIm5hbWVzIjpbIklOSVRJQUxfU1RBVEUiLCJlbnRpdGllcyIsIm9ybSIsImdldEVtcHR5U3RhdGUiLCJjdXJyZW50VGVtcGxhdGVJZCIsInRlbXBsYXRlQmFja1VwIiwic3RhdGUiLCJhY3Rpb24iLCJzZXNzaW9uIiwiR3JvdXAiLCJQYWdlIiwiVGVtcGxhdGUiLCJXaWRnZXQiLCJ0eXBlIiwiUEFTVEVfV0lER0VUIiwiQUREX1dJREdFVCIsImNyZWF0ZSIsInBheWxvYWQiLCIkc2V0IiwiQUREX05FV19QQUdFUyIsInBhZ2VzVGVtcGxhdGUiLCJ0ZW1wbGF0ZU1vZGVsIiwid2l0aElkIiwicGFnZUlkcyIsInRlbXBsYXRlUGFnZXMiLCJ0b1JlZkFycmF5IiwibWFwIiwicCIsImlkIiwiZmlsdGVyIiwiZ3JvdXBlZF9ieSIsImZvckVhY2giLCJpbmNsdWRlcyIsInNldCIsInBhZ2UiLCJwYWdlTW9kZWwiLCJ0ZW1wUGFnZSIsInRlbXBsYXRlSWQiLCJ3aWRnZXRNb2RlbCIsImdyb3VwSWRzIiwiZ3JvdXBzIiwiZyIsImtleSIsImZpcnN0UGFnZSIsInBhZ2VzIiwiZ3JvdXAiLCJncm91cE1vZGVsIiwiQ0hBTkdFX1BBR0VfSU5GSU5JVEVfTU9ERSIsInBhZ2VJZCIsImluZmluaXRlTW9kZSIsIkNIQU5HRV9QQUdFX09SSUVOVEFUSU9OIiwib3JpZW50YXRpb24iLCJ3aWRnZXRzIiwidG9Nb2RlbEFycmF5IiwicmVmIiwid2lkdGgiLCJoZWlnaHQiLCJ4IiwieSIsIndpZGdldFdpZHRoIiwid2lkZ2V0SGVpZ2h0IiwibGltaXRYIiwibGltaXRZIiwiQ0hBTkdFX1BBR0VfVEhFTUUiLCJ0aGVtZSIsIkNIQU5HRV9XSURHRVRfT1JERVIiLCJuZXdPcmRlckluZGV4Iiwid2lkZ2V0SW5kZXgiLCJ3IiwicGFyc2VJbnQiLCJvcmRlciIsImluZGV4IiwiQ0hBTkdFX1dJREdFVF9QT1NJVElPTiIsIndpZGdldElkIiwiY2hhbmdlZFgiLCJjaGFuZ2VkWSIsIkNIQU5HRV9XSURHRVRfU0laRSIsImNoYW5nZWRXaWR0aCIsImNoYW5nZWRIZWlnaHQiLCJDSEFOR0VfV0lER0VUX1NUWUxFIiwic3R5bGUiLCJDSEFOR0VfV0lER0VUX1ZBTFVFIiwidmFsdWUiLCJERUxFVEVfUEFHRVMiLCJkZWxldGUiLCJERUxFVEVfV0lER0VUIiwiSU5JVElBTElaRSIsInBhZ2VzX3RlbXBsYXRlIiwidGVtcGxhdGVEYXRhIiwiZ3JvdXBLZXkiLCJSRV9PUkRFUl9QQUdFUyIsImlkRXhpc3RzIiwiY3VycmVudEdyb3VwIiwiUkVTRVQiLCJSRVNFVF9URU1QTEFURSIsIlNFVF9QQUdFX0RBVEEiLCJ1cGRhdGUiLCJ3aWRnZXQiLCJTRVRfVEVNUExBVEVfQkFDS1VQIiwiUkVTVE9SRV9SRVZJRVdfUEFHRVMiLCJiYWNrdXBQYWdlcyIsIkpTT04iLCJwYXJzZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQXNCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLGFBQWEsR0FBRztBQUNsQkMsRUFBQUEsUUFBUSxFQUFFQyxhQUFJQyxhQUFKLEVBRFE7QUFFbEJDLEVBQUFBLGlCQUFpQixFQUFFLElBRkQ7QUFHbEJDLEVBQUFBLGNBQWMsRUFBRTtBQUhFLENBQXRCOztBQU1lLG9CQUF5QztBQUFBLE1BQS9CQyxLQUErQix1RUFBdkJOLGFBQXVCO0FBQUEsTUFBUk8sTUFBUTs7QUFDcEQsTUFBTUMsT0FBTyxHQUFHTixhQUFJTSxPQUFKLENBQVlGLEtBQUssQ0FBQ0wsUUFBbEIsQ0FBaEI7O0FBQ0EsTUFBUVEsS0FBUixHQUEwQ0QsT0FBMUMsQ0FBUUMsS0FBUjtBQUFBLE1BQWVDLElBQWYsR0FBMENGLE9BQTFDLENBQWVFLElBQWY7QUFBQSxNQUFxQkMsUUFBckIsR0FBMENILE9BQTFDLENBQXFCRyxRQUFyQjtBQUFBLE1BQStCQyxNQUEvQixHQUEwQ0osT0FBMUMsQ0FBK0JJLE1BQS9COztBQUNBLFVBQVFMLE1BQU0sQ0FBQ00sSUFBZjtBQUNJLFNBQUtDLHlCQUFMO0FBQ0EsU0FBS0MsdUJBQUw7QUFBaUI7QUFDYkgsUUFBQUEsTUFBTSxDQUFDSSxNQUFQLENBQWNULE1BQU0sQ0FBQ1UsT0FBckI7QUFDQSxlQUFPLGlDQUFPWCxLQUFQLEVBQWM7QUFDakJMLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxTQUFkLENBQVA7QUFLSDs7QUFFRCxTQUFLYSwwQkFBTDtBQUFvQjtBQUNoQixZQUFNQyxhQUFhLEdBQUdiLE1BQU0sQ0FBQ1UsT0FBN0I7QUFDQSxZQUFNSSxhQUFhLEdBQUdWLFFBQVEsQ0FBQ1csTUFBVCxDQUFnQmhCLEtBQUssQ0FBQ0YsaUJBQXRCLENBQXRCO0FBQ0EsWUFBTW1CLE9BQU8sR0FBR0YsYUFBYSxHQUN2QkEsYUFBYSxDQUFDRyxhQUFkLENBQTRCQyxVQUE1QixHQUF5Q0MsR0FBekMsQ0FBNkMsVUFBQ0MsQ0FBRDtBQUFBLGlCQUFPQSxDQUFDLENBQUNDLEVBQVQ7QUFBQSxTQUE3QyxDQUR1QixHQUV2QixFQUZOO0FBR0FSLFFBQUFBLGFBQWEsQ0FDUlMsTUFETCxDQUNZLFVBQUNGLENBQUQ7QUFBQSxpQkFBTyxvQkFBTUEsQ0FBQyxDQUFDRyxVQUFSLENBQVA7QUFBQSxTQURaLEVBRUtDLE9BRkwsQ0FFYSxVQUFDSixDQUFELEVBQU87QUFDWixjQUFJSixPQUFPLENBQUNTLFFBQVIsQ0FBaUJMLENBQUMsQ0FBQ0MsRUFBbkIsQ0FBSixFQUE0QjtBQUN4QmxCLFlBQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZSyxDQUFDLENBQUNDLEVBQWQsRUFBa0JLLEdBQWxCLENBQXNCLE1BQXRCLEVBQThCTixDQUFDLENBQUNPLElBQWhDO0FBQ0gsV0FGRCxNQUVPO0FBQ0gscUNBQVc7QUFDUEMsY0FBQUEsU0FBUyxFQUFFekIsSUFESjtBQUVQMEIsY0FBQUEsUUFBUSxFQUFFVCxDQUZIO0FBR1BVLGNBQUFBLFVBQVUsRUFBRS9CLEtBQUssQ0FBQ0YsaUJBSFg7QUFJUGtDLGNBQUFBLFdBQVcsRUFBRTFCO0FBSk4sYUFBWDtBQU1IO0FBQ0osU0FiTDtBQWNBLFlBQU0yQixRQUFRLEdBQUdsQixhQUFhLENBQUNtQixNQUFkLENBQXFCZixVQUFyQixHQUFrQ0MsR0FBbEMsQ0FBc0MsVUFBQ2UsQ0FBRDtBQUFBLGlCQUFPQSxDQUFDLENBQUNiLEVBQVQ7QUFBQSxTQUF0QyxDQUFqQjtBQUNBLFlBQU1ZLE1BQU0sR0FBRywyQkFBYXBCLGFBQWIsQ0FBZjs7QUFDQSxhQUFLLElBQU1zQixHQUFYLElBQWtCRixNQUFsQixFQUEwQjtBQUN0QixjQUFJRCxRQUFRLENBQUNQLFFBQVQsQ0FBa0JVLEdBQWxCLENBQUosRUFBNEI7QUFDeEJqQyxZQUFBQSxLQUFLLENBQUNhLE1BQU4sQ0FBYW9CLEdBQWIsRUFBa0JULEdBQWxCLENBQXNCLE1BQXRCLEVBQThCTyxNQUFNLENBQUNFLEdBQUQsQ0FBTixDQUFZQyxTQUExQzs7QUFEd0IsdURBRVJILE1BQU0sQ0FBQ0UsR0FBRCxDQUFOLENBQVlFLEtBRko7QUFBQTs7QUFBQTtBQUV4QixrRUFBbUM7QUFBQSxvQkFBeEJqQixDQUF3QjtBQUMvQmpCLGdCQUFBQSxJQUFJLENBQUNZLE1BQUwsQ0FBWUssQ0FBQyxDQUFDQyxFQUFkLEVBQWtCSyxHQUFsQixDQUFzQixNQUF0QixFQUE4Qk4sQ0FBQyxDQUFDTyxJQUFoQztBQUNIO0FBSnVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLM0IsV0FMRCxNQUtPO0FBQ0gsc0NBQVk7QUFDUlcsY0FBQUEsS0FBSyxFQUFFTCxNQUFNLENBQUNFLEdBQUQsQ0FETDtBQUVSSSxjQUFBQSxVQUFVLEVBQUVyQyxLQUZKO0FBR1IwQixjQUFBQSxTQUFTLEVBQUV6QixJQUhIO0FBSVIyQixjQUFBQSxVQUFVLEVBQUUvQixLQUFLLENBQUNGLGlCQUpWO0FBS1JrQyxjQUFBQSxXQUFXLEVBQUUxQjtBQUxMLGFBQVo7QUFPSDtBQUNKOztBQUNELGVBQU8saUNBQU9OLEtBQVAsRUFBYztBQUNqQkwsVUFBQUEsUUFBUSxFQUFFO0FBQ05pQixZQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLFNBQWQsQ0FBUDtBQUtIOztBQUVELFNBQUt5QyxzQ0FBTDtBQUNJckMsTUFBQUEsSUFBSSxDQUFDWSxNQUFMLENBQVlmLE1BQU0sQ0FBQ1UsT0FBUCxDQUFlK0IsTUFBM0IsRUFBbUNmLEdBQW5DLENBQ0ksY0FESixFQUVJMUIsTUFBTSxDQUFDVSxPQUFQLENBQWVnQyxZQUZuQjtBQUlBLGFBQU8saUNBQU8zQyxLQUFQLEVBQWM7QUFDakJMLFFBQUFBLFFBQVEsRUFBRTtBQUNOaUIsVUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxPQUFkLENBQVA7O0FBTUosU0FBSzRDLG9DQUFMO0FBQThCO0FBQzFCLDhCQUFnQzNDLE1BQU0sQ0FBQ1UsT0FBdkM7QUFBQSxZQUFRa0MsV0FBUixtQkFBUUEsV0FBUjtBQUFBLFlBQXFCSCxNQUFyQixtQkFBcUJBLE1BQXJCO0FBQ0F0QyxRQUFBQSxJQUFJLENBQUNZLE1BQUwsQ0FBWTBCLE1BQVosRUFDS0ksT0FETCxDQUNhQyxZQURiLEdBRUt0QixPQUZMLENBRWEsVUFBQ08sV0FBRCxFQUFpQjtBQUN0QixpQ0FBZ0NBLFdBQVcsQ0FBQ2dCLEdBQTVDO0FBQUEsY0FBUUMsS0FBUixvQkFBUUEsS0FBUjtBQUFBLGNBQWVDLE1BQWYsb0JBQWVBLE1BQWY7QUFBQSxjQUF1QkMsQ0FBdkIsb0JBQXVCQSxDQUF2QjtBQUFBLGNBQTBCQyxDQUExQixvQkFBMEJBLENBQTFCO0FBQ0EsY0FBSUMsV0FBVyxHQUFHLElBQUlKLEtBQXRCO0FBQ0EsY0FBSUssWUFBWSxHQUFHLElBQUlKLE1BQXZCOztBQUNBLDJCQUFpQyx3QkFBVUwsV0FBVixDQUFqQztBQUFBLGNBQVdVLE1BQVgsY0FBUUosQ0FBUjtBQUFBLGNBQXNCSyxNQUF0QixjQUFtQkosQ0FBbkI7O0FBQ0EsY0FBSUMsV0FBVyxHQUFHRSxNQUFsQixFQUEwQjtBQUN0QkYsWUFBQUEsV0FBVyxHQUFHRSxNQUFkO0FBQ0F2QixZQUFBQSxXQUFXLENBQUNMLEdBQVosQ0FBZ0IsT0FBaEIsRUFBeUIwQixXQUF6QjtBQUNIOztBQUVELGNBQUlDLFlBQVksR0FBR0UsTUFBbkIsRUFBMkI7QUFDdkJGLFlBQUFBLFlBQVksR0FBR0UsTUFBZjtBQUNBeEIsWUFBQUEsV0FBVyxDQUFDTCxHQUFaLENBQWdCLFFBQWhCLEVBQTBCMkIsWUFBMUI7QUFDSDs7QUFFRCxjQUFJSCxDQUFDLEdBQUdFLFdBQUosR0FBa0JFLE1BQXRCLEVBQThCO0FBQzFCdkIsWUFBQUEsV0FBVyxDQUFDTCxHQUFaLENBQWdCLEdBQWhCLEVBQXFCNEIsTUFBTSxHQUFHRixXQUE5QjtBQUNIOztBQUVELGNBQUlELENBQUMsR0FBR0UsWUFBSixHQUFtQkUsTUFBdkIsRUFBK0I7QUFDM0J4QixZQUFBQSxXQUFXLENBQUNMLEdBQVosQ0FBZ0IsR0FBaEIsRUFBcUI2QixNQUFNLEdBQUdGLFlBQTlCO0FBQ0g7QUFDSixTQXhCTDtBQXlCQWxELFFBQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZMEIsTUFBWixFQUFvQmYsR0FBcEIsQ0FBd0IsYUFBeEIsRUFBdUNrQixXQUF2QztBQUNBLGVBQU8saUNBQU83QyxLQUFQLEVBQWM7QUFDakJMLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxTQUFkLENBQVA7QUFLSDs7QUFFRCxTQUFLeUQsOEJBQUw7QUFDSXJELE1BQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZZixNQUFNLENBQUNVLE9BQVAsQ0FBZStCLE1BQTNCLEVBQW1DZixHQUFuQyxDQUNJLE9BREosRUFFSTFCLE1BQU0sQ0FBQ1UsT0FBUCxDQUFlK0MsS0FGbkI7QUFJQSxhQUFPLGlDQUFPMUQsS0FBUCxFQUFjO0FBQ2pCTCxRQUFBQSxRQUFRLEVBQUU7QUFDTmlCLFVBQUFBLElBQUksRUFBRVYsT0FBTyxDQUFDRjtBQURSO0FBRE8sT0FBZCxDQUFQOztBQU1KLFNBQUsyRCxnQ0FBTDtBQUEwQjtBQUN0QiwrQkFBK0MxRCxNQUFNLENBQUNVLE9BQXREO0FBQUEsWUFBUStCLE9BQVIsb0JBQVFBLE1BQVI7QUFBQSxZQUFnQmtCLGFBQWhCLG9CQUFnQkEsYUFBaEI7QUFBQSxZQUErQkMsV0FBL0Isb0JBQStCQSxXQUEvQjtBQUNBLGdDQUNJLHFCQUFPekQsSUFBSSxDQUFDWSxNQUFMLENBQVkwQixPQUFaLEVBQW9CSSxPQUFwQixDQUE0QkMsWUFBNUIsRUFBUCxFQUFtRCxDQUMvQyxVQUFDZSxDQUFELEVBQU87QUFDSCxpQkFBT0MsUUFBUSxDQUFDRCxDQUFDLENBQUNkLEdBQUYsQ0FBTWdCLEtBQVAsQ0FBZjtBQUNILFNBSDhDLENBQW5ELENBREosRUFNSUgsV0FOSixFQU9JRCxhQVBKLEVBUUVuQyxPQVJGLENBUVUsVUFBQ08sV0FBRCxFQUFjaUMsS0FBZCxFQUF3QjtBQUM5QmpDLFVBQUFBLFdBQVcsQ0FBQ0wsR0FBWixDQUFnQixPQUFoQixFQUF5QnNDLEtBQXpCO0FBQ0gsU0FWRDtBQVdBLGVBQU8saUNBQU9qRSxLQUFQLEVBQWM7QUFDakJMLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxTQUFkLENBQVA7QUFLSDs7QUFFRCxTQUFLa0UsbUNBQUw7QUFBNkI7QUFDekIsWUFBTWxDLFdBQVcsR0FBRzFCLE1BQU0sQ0FBQ1UsTUFBUCxDQUFjZixNQUFNLENBQUNVLE9BQVAsQ0FBZXdELFFBQTdCLENBQXBCO0FBQ0FuQyxRQUFBQSxXQUFXLENBQUNMLEdBQVosQ0FBZ0IsR0FBaEIsRUFBcUIxQixNQUFNLENBQUNVLE9BQVAsQ0FBZXlELFFBQXBDO0FBQ0FwQyxRQUFBQSxXQUFXLENBQUNMLEdBQVosQ0FBZ0IsR0FBaEIsRUFBcUIxQixNQUFNLENBQUNVLE9BQVAsQ0FBZTBELFFBQXBDO0FBQ0EsZUFBTyxpQ0FBT3JFLEtBQVAsRUFBYztBQUNqQkwsVUFBQUEsUUFBUSxFQUFFO0FBQ05pQixZQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLFNBQWQsQ0FBUDtBQUtIOztBQUVELFNBQUtzRSwrQkFBTDtBQUF5QjtBQUNyQixZQUFNdEMsWUFBVyxHQUFHMUIsTUFBTSxDQUFDVSxNQUFQLENBQWNmLE1BQU0sQ0FBQ1UsT0FBUCxDQUFld0QsUUFBN0IsQ0FBcEI7O0FBQ0FuQyxRQUFBQSxZQUFXLENBQUNMLEdBQVosQ0FDSSxPQURKLEVBRUlLLFlBQVcsQ0FBQ2dCLEdBQVosQ0FBZ0JDLEtBQWhCLEdBQXdCaEQsTUFBTSxDQUFDVSxPQUFQLENBQWU0RCxZQUYzQzs7QUFJQXZDLFFBQUFBLFlBQVcsQ0FBQ0wsR0FBWixDQUNJLFFBREosRUFFSUssWUFBVyxDQUFDZ0IsR0FBWixDQUFnQkUsTUFBaEIsR0FBeUJqRCxNQUFNLENBQUNVLE9BQVAsQ0FBZTZELGFBRjVDOztBQUlBLGVBQU8saUNBQU94RSxLQUFQLEVBQWM7QUFDakJMLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxTQUFkLENBQVA7QUFLSDs7QUFFRCxTQUFLeUUsZ0NBQUw7QUFDSW5FLE1BQUFBLE1BQU0sQ0FBQ1UsTUFBUCxDQUFjZixNQUFNLENBQUNVLE9BQVAsQ0FBZXdELFFBQTdCLEVBQXVDeEMsR0FBdkMsQ0FDSSxPQURKLEVBRUkxQixNQUFNLENBQUNVLE9BQVAsQ0FBZStELEtBRm5CO0FBSUEsYUFBTyxpQ0FBTzFFLEtBQVAsRUFBYztBQUNqQkwsUUFBQUEsUUFBUSxFQUFFO0FBQ05pQixVQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLE9BQWQsQ0FBUDs7QUFLSixTQUFLMkUsZ0NBQUw7QUFDSXJFLE1BQUFBLE1BQU0sQ0FBQ1UsTUFBUCxDQUFjZixNQUFNLENBQUNVLE9BQVAsQ0FBZXdELFFBQTdCLEVBQXVDeEMsR0FBdkMsQ0FDSSxPQURKLEVBRUkxQixNQUFNLENBQUNVLE9BQVAsQ0FBZWlFLEtBRm5CO0FBSUEsYUFBTyxpQ0FBTzVFLEtBQVAsRUFBYztBQUNqQkwsUUFBQUEsUUFBUSxFQUFFO0FBQ05pQixVQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLE9BQWQsQ0FBUDs7QUFNSixTQUFLNkUseUJBQUw7QUFBbUI7QUFDZixZQUFNL0QsY0FBYSxHQUFHYixNQUFNLENBQUNVLE9BQTdCO0FBQ0EsaUNBQ0lOLFFBQVEsQ0FBQ1csTUFBVCxDQUFnQmhCLEtBQUssQ0FBQ0YsaUJBQXRCLEVBQ0tvQixhQURMLENBQ21CQyxVQURuQixHQUVLQyxHQUZMLENBRVMsVUFBQ0MsQ0FBRDtBQUFBLGlCQUFPQSxDQUFDLENBQUNDLEVBQVQ7QUFBQSxTQUZULENBREosRUFJSVIsY0FBYSxDQUNSUyxNQURMLENBQ1ksVUFBQ0YsQ0FBRDtBQUFBLGlCQUFPLG9CQUFNQSxDQUFDLENBQUNHLFVBQVIsQ0FBUDtBQUFBLFNBRFosRUFFS0osR0FGTCxDQUVTLFVBQUNDLENBQUQ7QUFBQSxpQkFBT0EsQ0FBQyxDQUFDQyxFQUFUO0FBQUEsU0FGVCxDQUpKLEVBT0VHLE9BUEYsQ0FPVSxVQUFDSixDQUFELEVBQU87QUFDYmpCLFVBQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZSyxDQUFaLEVBQWV5QixPQUFmLENBQXVCZ0MsTUFBdkI7QUFDQTFFLFVBQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZSyxDQUFaLEVBQWV5RCxNQUFmO0FBQ0gsU0FWRDtBQVlBLGlDQUNJekUsUUFBUSxDQUFDVyxNQUFULENBQWdCaEIsS0FBSyxDQUFDRixpQkFBdEIsRUFDS29CLGFBREwsQ0FDbUJDLFVBRG5CLEdBRUtDLEdBRkwsQ0FFUyxVQUFDQyxDQUFEO0FBQUEsaUJBQU9BLENBQUMsQ0FBQ0MsRUFBVDtBQUFBLFNBRlQsQ0FESixFQUlJUixjQUFhLENBQ1JTLE1BREwsQ0FDWSxVQUFDRixDQUFEO0FBQUEsaUJBQU8sb0JBQU1BLENBQUMsQ0FBQ0csVUFBUixDQUFQO0FBQUEsU0FEWixFQUVLSixHQUZMLENBRVMsVUFBQ0MsQ0FBRDtBQUFBLGlCQUFPQSxDQUFDLENBQUNDLEVBQVQ7QUFBQSxTQUZULENBSkosRUFPRUcsT0FQRixDQU9VLFVBQUNKLENBQUQsRUFBTztBQUNiakIsVUFBQUEsSUFBSSxDQUFDWSxNQUFMLENBQVlLLENBQVosRUFBZXlCLE9BQWYsQ0FBdUJnQyxNQUF2QjtBQUNBMUUsVUFBQUEsSUFBSSxDQUFDWSxNQUFMLENBQVlLLENBQVosRUFBZXlELE1BQWY7QUFDSCxTQVZEO0FBV0EsZUFBTyxpQ0FBTzlFLEtBQVAsRUFBYztBQUNqQkwsVUFBQUEsUUFBUSxFQUFFO0FBQ05pQixZQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLFNBQWQsQ0FBUDtBQUtIOztBQUVELFNBQUsrRSwwQkFBTDtBQUNJekUsTUFBQUEsTUFBTSxDQUFDVSxNQUFQLENBQWNmLE1BQU0sQ0FBQ1UsT0FBckIsRUFBOEJtRSxNQUE5QjtBQUNBLGFBQU8saUNBQU85RSxLQUFQLEVBQWM7QUFDakJMLFFBQUFBLFFBQVEsRUFBRTtBQUNOaUIsVUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFETyxPQUFkLENBQVA7O0FBTUosU0FBS2dGLHVCQUFMO0FBQWlCO0FBQ2IsK0JBQ0kvRSxNQUFNLENBQUNVLE9BRFg7QUFBQSxZQUF3QkcsZUFBeEIsb0JBQVFtRSxjQUFSO0FBQUEsWUFBMENDLFlBQTFDOztBQUVBN0UsUUFBQUEsUUFBUSxDQUFDSyxNQUFULG1CQUFxQndFLFlBQXJCOztBQUNBcEUsUUFBQUEsZUFBYSxDQUNSUyxNQURMLENBQ1ksVUFBQ0YsQ0FBRDtBQUFBLGlCQUFPLG9CQUFNQSxDQUFDLENBQUNHLFVBQVIsQ0FBUDtBQUFBLFNBRFosRUFFS0MsT0FGTCxDQUVhLFVBQUNKLENBQUQ7QUFBQSxpQkFDTCx5QkFBVztBQUNQUSxZQUFBQSxTQUFTLEVBQUV6QixJQURKO0FBRVAwQixZQUFBQSxRQUFRLEVBQUVULENBRkg7QUFHUFUsWUFBQUEsVUFBVSxFQUFFbUQsWUFBWSxDQUFDNUQsRUFIbEI7QUFJUFUsWUFBQUEsV0FBVyxFQUFFMUI7QUFKTixXQUFYLENBREs7QUFBQSxTQUZiOztBQVVBLFlBQU00QixPQUFNLEdBQUcsMkJBQWFwQixlQUFiLENBQWY7O0FBQ0EsYUFBSyxJQUFNcUUsUUFBWCxJQUF1QmpELE9BQXZCLEVBQStCO0FBQzNCLG9DQUFZO0FBQ1JLLFlBQUFBLEtBQUssRUFBRUwsT0FBTSxDQUFDaUQsUUFBRCxDQURMO0FBRVIzQyxZQUFBQSxVQUFVLEVBQUVyQyxLQUZKO0FBR1IwQixZQUFBQSxTQUFTLEVBQUV6QixJQUhIO0FBSVIyQixZQUFBQSxVQUFVLEVBQUVtRCxZQUFZLENBQUM1RCxFQUpqQjtBQUtSVSxZQUFBQSxXQUFXLEVBQUUxQjtBQUxMLFdBQVo7QUFPSDs7QUFDRCxlQUFPLGlDQUFPTixLQUFQLEVBQWM7QUFDakJGLFVBQUFBLGlCQUFpQixFQUFFO0FBQ2ZjLFlBQUFBLElBQUksRUFBRXNFLFlBQVksQ0FBQzVEO0FBREosV0FERjtBQUlqQjNCLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFKTyxTQUFkLENBQVA7QUFRSDs7QUFFRCxTQUFLb0YsMkJBQUw7QUFBcUI7QUFDakIsWUFBTXRFLGVBQWEsR0FBR2IsTUFBTSxDQUFDVSxPQUE3Qjs7QUFDQUcsUUFBQUEsZUFBYSxDQUFDVyxPQUFkLENBQXNCLFVBQUNKLENBQUQsRUFBTztBQUN6QixjQUFJakIsSUFBSSxDQUFDaUYsUUFBTCxDQUFjaEUsQ0FBQyxDQUFDQyxFQUFoQixDQUFKLEVBQXlCO0FBQ3JCbEIsWUFBQUEsSUFBSSxDQUFDWSxNQUFMLENBQVlLLENBQUMsQ0FBQ0MsRUFBZCxFQUFrQkssR0FBbEIsQ0FBc0IsTUFBdEIsRUFBOEJOLENBQUMsQ0FBQ08sSUFBaEM7QUFDSDtBQUNKLFNBSkQ7O0FBS0EsWUFBTU0sUUFBTSxHQUFHLDJCQUFhcEIsZUFBYixDQUFmOztBQUNBLGFBQUssSUFBTXNCLElBQVgsSUFBa0JGLFFBQWxCLEVBQTBCO0FBQ3RCLGNBQU1vRCxZQUFZLEdBQUdwRCxRQUFNLENBQUNFLElBQUQsQ0FBM0I7QUFDQWpDLFVBQUFBLEtBQUssQ0FBQ2EsTUFBTixDQUFhc0UsWUFBWSxDQUFDaEUsRUFBMUIsRUFBOEJLLEdBQTlCLENBQ0ksTUFESixFQUVJMkQsWUFBWSxDQUFDakQsU0FGakI7QUFJSDs7QUFDRCxlQUFPLGlDQUFPckMsS0FBUCxFQUFjO0FBQ2pCTCxVQUFBQSxRQUFRLEVBQUU7QUFDTmlCLFlBQUFBLElBQUksRUFBRVYsT0FBTyxDQUFDRjtBQURSO0FBRE8sU0FBZCxDQUFQO0FBS0g7O0FBRUQsU0FBS3VGLGtCQUFMO0FBQ0EsU0FBS0MsMkJBQUw7QUFDSSxhQUFPLGlDQUFPeEYsS0FBUCxFQUFjO0FBQ2pCWSxRQUFBQSxJQUFJLEVBQUVsQjtBQURXLE9BQWQsQ0FBUDs7QUFJSixTQUFLK0YsMEJBQUw7QUFBb0I7QUFDaEIsK0JBQ0l4RixNQUFNLENBQUNVLE9BRFg7QUFBQSxZQUFRVyxFQUFSLG9CQUFRQSxFQUFSO0FBQUEsWUFBWXFCLFlBQVosb0JBQVlBLFlBQVo7QUFBQSxZQUEwQkUsWUFBMUIsb0JBQTBCQSxXQUExQjtBQUFBLFlBQXVDYSxLQUF2QyxvQkFBdUNBLEtBQXZDO0FBQUEsWUFBOENaLE9BQTlDLG9CQUE4Q0EsT0FBOUM7QUFFQTFDLFFBQUFBLElBQUksQ0FBQ1ksTUFBTCxDQUFZTSxFQUFaLEVBQWdCb0UsTUFBaEIsQ0FBdUI7QUFDbkIvQyxVQUFBQSxZQUFZLEVBQUVBLFlBREs7QUFFbkJFLFVBQUFBLFdBQVcsRUFBRUEsWUFGTTtBQUduQmEsVUFBQUEsS0FBSyxFQUFFQTtBQUhZLFNBQXZCO0FBS0EsaUNBQ0l0RCxJQUFJLENBQUNZLE1BQUwsQ0FBWU0sRUFBWixFQUNLd0IsT0FETCxDQUNhM0IsVUFEYixHQUVLQyxHQUZMLENBRVMsVUFBQzBDLENBQUQ7QUFBQSxpQkFBT0EsQ0FBQyxDQUFDeEMsRUFBVDtBQUFBLFNBRlQsQ0FESixFQUlJd0IsT0FBTyxDQUFDMUIsR0FBUixDQUFZLFVBQUMwQyxDQUFEO0FBQUEsaUJBQU9BLENBQUMsQ0FBQ3hDLEVBQVQ7QUFBQSxTQUFaLENBSkosRUFLRUcsT0FMRixDQUtVLFVBQUMwQyxRQUFELEVBQWM7QUFDcEI3RCxVQUFBQSxNQUFNLENBQUNVLE1BQVAsQ0FBY21ELFFBQWQsRUFBd0JXLE1BQXhCO0FBQ0gsU0FQRDtBQVFBaEMsUUFBQUEsT0FBTyxDQUFDckIsT0FBUixDQUFnQixVQUFDa0UsTUFBRCxFQUFZO0FBQ3hCLGNBQUlyRixNQUFNLENBQUMrRSxRQUFQLENBQWdCTSxNQUFNLENBQUNyRSxFQUF2QixDQUFKLEVBQWdDO0FBQzVCaEIsWUFBQUEsTUFBTSxDQUFDVSxNQUFQLENBQWMyRSxNQUFNLENBQUNyRSxFQUFyQixFQUF5Qm9FLE1BQXpCLENBQWdDQyxNQUFoQztBQUNILFdBRkQsTUFFTztBQUNIckYsWUFBQUEsTUFBTSxDQUFDSSxNQUFQLENBQWNpRixNQUFkO0FBQ0g7QUFDSixTQU5EO0FBT0EsZUFBTyxpQ0FBTzNGLEtBQVAsRUFBYztBQUNqQkwsVUFBQUEsUUFBUSxFQUFFO0FBQ05pQixZQUFBQSxJQUFJLEVBQUVWLE9BQU8sQ0FBQ0Y7QUFEUjtBQURPLFNBQWQsQ0FBUDtBQUtIOztBQUVELFNBQUs0RixnQ0FBTDtBQUNJLGFBQU8saUNBQU81RixLQUFQLEVBQWM7QUFDakJELFFBQUFBLGNBQWMsRUFBRTtBQUNaYSxVQUFBQSxJQUFJLEVBQUVYLE1BQU0sQ0FBQ1U7QUFERDtBQURDLE9BQWQsQ0FBUDs7QUFNSixTQUFLa0YsaUNBQUw7QUFBMkI7QUFDdkIsWUFBTUMsV0FBVyxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBV2hHLEtBQUssQ0FBQ0QsY0FBakIsQ0FBcEI7QUFDQStGLFFBQUFBLFdBQVcsQ0FBQ3JFLE9BQVosQ0FBb0IsVUFBQ0osQ0FBRCxFQUFPO0FBQ3ZCLGNBQUlqQixJQUFJLENBQUNpRixRQUFMLENBQWNoRSxDQUFDLENBQUNDLEVBQWhCLENBQUosRUFBeUI7QUFDckJsQixZQUFBQSxJQUFJLENBQUNZLE1BQUwsQ0FBWUssQ0FBQyxDQUFDQyxFQUFkLEVBQWtCb0UsTUFBbEIsQ0FBeUI7QUFDckIvQyxjQUFBQSxZQUFZLEVBQUV0QixDQUFDLENBQUNzQixZQURLO0FBRXJCRSxjQUFBQSxXQUFXLEVBQUV4QixDQUFDLENBQUN3QixXQUZNO0FBR3JCYSxjQUFBQSxLQUFLLEVBQUVyQyxDQUFDLENBQUNxQztBQUhZLGFBQXpCO0FBS0EscUNBQ0l0RCxJQUFJLENBQUNZLE1BQUwsQ0FBWUssQ0FBQyxDQUFDQyxFQUFkLEVBQ0t3QixPQURMLENBQ2EzQixVQURiLEdBRUtDLEdBRkwsQ0FFUyxVQUFDMEMsQ0FBRDtBQUFBLHFCQUFPQSxDQUFDLENBQUN4QyxFQUFUO0FBQUEsYUFGVCxDQURKLEVBSUlELENBQUMsQ0FBQ3lCLE9BQUYsQ0FBVTFCLEdBQVYsQ0FBYyxVQUFDMEMsQ0FBRDtBQUFBLHFCQUFPQSxDQUFDLENBQUN4QyxFQUFUO0FBQUEsYUFBZCxDQUpKLEVBS0VHLE9BTEYsQ0FLVSxVQUFDMEMsUUFBRCxFQUFjO0FBQ3BCN0QsY0FBQUEsTUFBTSxDQUFDVSxNQUFQLENBQWNtRCxRQUFkLEVBQXdCVyxNQUF4QjtBQUNILGFBUEQ7QUFTQXpELFlBQUFBLENBQUMsQ0FBQ3lCLE9BQUYsQ0FBVXJCLE9BQVYsQ0FBa0IsVUFBQ2tFLE1BQUQsRUFBWTtBQUMxQixrQkFBSXJGLE1BQU0sQ0FBQytFLFFBQVAsQ0FBZ0JNLE1BQU0sQ0FBQ3JFLEVBQXZCLENBQUosRUFBZ0M7QUFDNUJoQixnQkFBQUEsTUFBTSxDQUFDVSxNQUFQLENBQWMyRSxNQUFNLENBQUNyRSxFQUFyQixFQUF5Qm9FLE1BQXpCLENBQWdDQyxNQUFoQztBQUNILGVBRkQsTUFFTztBQUNIckYsZ0JBQUFBLE1BQU0sQ0FBQ0ksTUFBUCxDQUFjaUYsTUFBZDtBQUNIO0FBQ0osYUFORDtBQU9IO0FBQ0osU0F4QkQ7QUF5QkEsaUNBQ0l0RixRQUFRLENBQUNXLE1BQVQsQ0FBZ0JoQixLQUFLLENBQUNGLGlCQUF0QixFQUNLb0IsYUFETCxDQUNtQkMsVUFEbkIsR0FFS0MsR0FGTCxDQUVTLFVBQUNDLENBQUQ7QUFBQSxpQkFBT0EsQ0FBQyxDQUFDQyxFQUFUO0FBQUEsU0FGVCxDQURKLEVBSUl3RSxXQUFXLENBQUMxRSxHQUFaLENBQWdCLFVBQUNDLENBQUQ7QUFBQSxpQkFBT0EsQ0FBQyxDQUFDQyxFQUFUO0FBQUEsU0FBaEIsQ0FKSixFQUtFRyxPQUxGLENBS1UsVUFBQ0osQ0FBRCxFQUFPO0FBQ2JqQixVQUFBQSxJQUFJLENBQUNZLE1BQUwsQ0FBWUssQ0FBWixFQUFleUIsT0FBZixDQUF1QmdDLE1BQXZCO0FBQ0ExRSxVQUFBQSxJQUFJLENBQUNZLE1BQUwsQ0FBWUssQ0FBWixFQUFleUQsTUFBZjtBQUNILFNBUkQ7QUFTQSxlQUFPLGlDQUFPOUUsS0FBUCxFQUFjO0FBQ2pCRCxVQUFBQSxjQUFjLEVBQUU7QUFDWmEsWUFBQUEsSUFBSSxFQUFFO0FBRE0sV0FEQztBQUlqQmpCLFVBQUFBLFFBQVEsRUFBRTtBQUNOaUIsWUFBQUEsSUFBSSxFQUFFVixPQUFPLENBQUNGO0FBRFI7QUFKTyxTQUFkLENBQVA7QUFRSDs7QUFFRDtBQUNJLGFBQU9BLEtBQVA7QUFoWFI7QUFrWEgiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXJyYXlNb3ZlIGZyb20gJ2FycmF5LW1vdmUnO1xyXG5pbXBvcnQgdXBkYXRlIGZyb20gJ2ltbXV0YWJpbGl0eS1oZWxwZXInO1xyXG5pbXBvcnQgZGlmZmVyZW5jZSBmcm9tICdsb2Rhc2gvZGlmZmVyZW5jZSc7XHJcbmltcG9ydCBpc05pbCBmcm9tICdsb2Rhc2gvaXNOaWwnO1xyXG5pbXBvcnQgc29ydEJ5IGZyb20gJ2xvZGFzaC9zb3J0QnknO1xyXG5cclxuaW1wb3J0IG9ybSBmcm9tICdAb3JtJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBBRERfV0lER0VULFxyXG4gICAgQUREX05FV19QQUdFUyxcclxuICAgIERFTEVURV9QQUdFUyxcclxuICAgIERFTEVURV9XSURHRVQsXHJcbiAgICBDSEFOR0VfUEFHRV9JTkZJTklURV9NT0RFLFxyXG4gICAgQ0hBTkdFX1BBR0VfT1JJRU5UQVRJT04sXHJcbiAgICBDSEFOR0VfUEFHRV9USEVNRSxcclxuICAgIENIQU5HRV9XSURHRVRfT1JERVIsXHJcbiAgICBDSEFOR0VfV0lER0VUX1BPU0lUSU9OLFxyXG4gICAgQ0hBTkdFX1dJREdFVF9TSVpFLFxyXG4gICAgQ0hBTkdFX1dJREdFVF9TVFlMRSxcclxuICAgIENIQU5HRV9XSURHRVRfVkFMVUUsXHJcbiAgICBJTklUSUFMSVpFLFxyXG4gICAgUEFTVEVfV0lER0VULFxyXG4gICAgUkVfT1JERVJfUEFHRVMsXHJcbiAgICBSRVNUT1JFX1JFVklFV19QQUdFUyxcclxuICAgIFJFU0VULFxyXG4gICAgUkVTRVRfVEVNUExBVEUsXHJcbiAgICBTRVRfUEFHRV9EQVRBLFxyXG4gICAgU0VUX1RFTVBMQVRFX0JBQ0tVUCxcclxufSBmcm9tICcuL2FjdGlvblR5cGVzJztcclxuaW1wb3J0IHsgY3JlYXRlR3JvdXBzLCBjcmVhdGVHcm91cCwgY3JlYXRlUGFnZSwgZ2V0TGltaXRzIH0gZnJvbSAnLi9oZWxwZXJzJztcclxuXHJcbmNvbnN0IElOSVRJQUxfU1RBVEUgPSB7XHJcbiAgICBlbnRpdGllczogb3JtLmdldEVtcHR5U3RhdGUoKSxcclxuICAgIGN1cnJlbnRUZW1wbGF0ZUlkOiBudWxsLFxyXG4gICAgdGVtcGxhdGVCYWNrVXA6IG51bGwsXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiAoc3RhdGUgPSBJTklUSUFMX1NUQVRFLCBhY3Rpb24pIHtcclxuICAgIGNvbnN0IHNlc3Npb24gPSBvcm0uc2Vzc2lvbihzdGF0ZS5lbnRpdGllcyk7XHJcbiAgICBjb25zdCB7IEdyb3VwLCBQYWdlLCBUZW1wbGF0ZSwgV2lkZ2V0IH0gPSBzZXNzaW9uO1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgUEFTVEVfV0lER0VUOlxyXG4gICAgICAgIGNhc2UgQUREX1dJREdFVDoge1xyXG4gICAgICAgICAgICBXaWRnZXQuY3JlYXRlKGFjdGlvbi5wYXlsb2FkKTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIEFERF9ORVdfUEFHRVM6IHtcclxuICAgICAgICAgICAgY29uc3QgcGFnZXNUZW1wbGF0ZSA9IGFjdGlvbi5wYXlsb2FkO1xyXG4gICAgICAgICAgICBjb25zdCB0ZW1wbGF0ZU1vZGVsID0gVGVtcGxhdGUud2l0aElkKHN0YXRlLmN1cnJlbnRUZW1wbGF0ZUlkKTtcclxuICAgICAgICAgICAgY29uc3QgcGFnZUlkcyA9IHRlbXBsYXRlTW9kZWxcclxuICAgICAgICAgICAgICAgID8gdGVtcGxhdGVNb2RlbC50ZW1wbGF0ZVBhZ2VzLnRvUmVmQXJyYXkoKS5tYXAoKHApID0+IHAuaWQpXHJcbiAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICBwYWdlc1RlbXBsYXRlXHJcbiAgICAgICAgICAgICAgICAuZmlsdGVyKChwKSA9PiBpc05pbChwLmdyb3VwZWRfYnkpKVxyXG4gICAgICAgICAgICAgICAgLmZvckVhY2goKHApID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocGFnZUlkcy5pbmNsdWRlcyhwLmlkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChwLmlkKS5zZXQoJ3BhZ2UnLCBwLnBhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZVBhZ2Uoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnZU1vZGVsOiBQYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGVtcFBhZ2U6IHAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZUlkOiBzdGF0ZS5jdXJyZW50VGVtcGxhdGVJZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZGdldE1vZGVsOiBXaWRnZXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBncm91cElkcyA9IHRlbXBsYXRlTW9kZWwuZ3JvdXBzLnRvUmVmQXJyYXkoKS5tYXAoKGcpID0+IGcuaWQpO1xyXG4gICAgICAgICAgICBjb25zdCBncm91cHMgPSBjcmVhdGVHcm91cHMocGFnZXNUZW1wbGF0ZSk7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIGdyb3Vwcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGdyb3VwSWRzLmluY2x1ZGVzKGtleSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBHcm91cC53aXRoSWQoa2V5KS5zZXQoJ3BhZ2UnLCBncm91cHNba2V5XS5maXJzdFBhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3QgcCBvZiBncm91cHNba2V5XS5wYWdlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChwLmlkKS5zZXQoJ3BhZ2UnLCBwLnBhZ2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlR3JvdXAoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBncm91cDogZ3JvdXBzW2tleV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdyb3VwTW9kZWw6IEdyb3VwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlTW9kZWw6IFBhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlSWQ6IHN0YXRlLmN1cnJlbnRUZW1wbGF0ZUlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRNb2RlbDogV2lkZ2V0LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGVudGl0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogc2Vzc2lvbi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FzZSBDSEFOR0VfUEFHRV9JTkZJTklURV9NT0RFOlxyXG4gICAgICAgICAgICBQYWdlLndpdGhJZChhY3Rpb24ucGF5bG9hZC5wYWdlSWQpLnNldChcclxuICAgICAgICAgICAgICAgICdpbmZpbml0ZU1vZGUnLFxyXG4gICAgICAgICAgICAgICAgYWN0aW9uLnBheWxvYWQuaW5maW5pdGVNb2RlXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGVudGl0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogc2Vzc2lvbi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIENIQU5HRV9QQUdFX09SSUVOVEFUSU9OOiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHsgb3JpZW50YXRpb24sIHBhZ2VJZCB9ID0gYWN0aW9uLnBheWxvYWQ7XHJcbiAgICAgICAgICAgIFBhZ2Uud2l0aElkKHBhZ2VJZClcclxuICAgICAgICAgICAgICAgIC53aWRnZXRzLnRvTW9kZWxBcnJheSgpXHJcbiAgICAgICAgICAgICAgICAuZm9yRWFjaCgod2lkZ2V0TW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB7IHdpZHRoLCBoZWlnaHQsIHgsIHkgfSA9IHdpZGdldE1vZGVsLnJlZjtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgd2lkZ2V0V2lkdGggPSAwICsgd2lkdGg7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHdpZGdldEhlaWdodCA9IDAgKyBoZWlnaHQ7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgeyB4OiBsaW1pdFgsIHk6IGxpbWl0WSB9ID0gZ2V0TGltaXRzKG9yaWVudGF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAod2lkZ2V0V2lkdGggPiBsaW1pdFgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkZ2V0V2lkdGggPSBsaW1pdFg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZGdldE1vZGVsLnNldCgnd2lkdGgnLCB3aWRnZXRXaWR0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAod2lkZ2V0SGVpZ2h0ID4gbGltaXRZKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZGdldEhlaWdodCA9IGxpbWl0WTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkZ2V0TW9kZWwuc2V0KCdoZWlnaHQnLCB3aWRnZXRIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHggKyB3aWRnZXRXaWR0aCA+IGxpbWl0WCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRNb2RlbC5zZXQoJ3gnLCBsaW1pdFggLSB3aWRnZXRXaWR0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoeSArIHdpZGdldEhlaWdodCA+IGxpbWl0WSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRNb2RlbC5zZXQoJ3knLCBsaW1pdFkgLSB3aWRnZXRIZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBQYWdlLndpdGhJZChwYWdlSWQpLnNldCgnb3JpZW50YXRpb24nLCBvcmllbnRhdGlvbik7XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGVudGl0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogc2Vzc2lvbi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FzZSBDSEFOR0VfUEFHRV9USEVNRTpcclxuICAgICAgICAgICAgUGFnZS53aXRoSWQoYWN0aW9uLnBheWxvYWQucGFnZUlkKS5zZXQoXHJcbiAgICAgICAgICAgICAgICAndGhlbWUnLFxyXG4gICAgICAgICAgICAgICAgYWN0aW9uLnBheWxvYWQudGhlbWVcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgQ0hBTkdFX1dJREdFVF9PUkRFUjoge1xyXG4gICAgICAgICAgICBjb25zdCB7IHBhZ2VJZCwgbmV3T3JkZXJJbmRleCwgd2lkZ2V0SW5kZXggfSA9IGFjdGlvbi5wYXlsb2FkO1xyXG4gICAgICAgICAgICBhcnJheU1vdmUoXHJcbiAgICAgICAgICAgICAgICBzb3J0QnkoUGFnZS53aXRoSWQocGFnZUlkKS53aWRnZXRzLnRvTW9kZWxBcnJheSgpLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgKHcpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlSW50KHcucmVmLm9yZGVyKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgICAgICB3aWRnZXRJbmRleCxcclxuICAgICAgICAgICAgICAgIG5ld09yZGVySW5kZXhcclxuICAgICAgICAgICAgKS5mb3JFYWNoKCh3aWRnZXRNb2RlbCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIHdpZGdldE1vZGVsLnNldCgnb3JkZXInLCBpbmRleCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBlbnRpdGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHNlc3Npb24uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNhc2UgQ0hBTkdFX1dJREdFVF9QT1NJVElPTjoge1xyXG4gICAgICAgICAgICBjb25zdCB3aWRnZXRNb2RlbCA9IFdpZGdldC53aXRoSWQoYWN0aW9uLnBheWxvYWQud2lkZ2V0SWQpO1xyXG4gICAgICAgICAgICB3aWRnZXRNb2RlbC5zZXQoJ3gnLCBhY3Rpb24ucGF5bG9hZC5jaGFuZ2VkWCk7XHJcbiAgICAgICAgICAgIHdpZGdldE1vZGVsLnNldCgneScsIGFjdGlvbi5wYXlsb2FkLmNoYW5nZWRZKTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIENIQU5HRV9XSURHRVRfU0laRToge1xyXG4gICAgICAgICAgICBjb25zdCB3aWRnZXRNb2RlbCA9IFdpZGdldC53aXRoSWQoYWN0aW9uLnBheWxvYWQud2lkZ2V0SWQpO1xyXG4gICAgICAgICAgICB3aWRnZXRNb2RlbC5zZXQoXHJcbiAgICAgICAgICAgICAgICAnd2lkdGgnLFxyXG4gICAgICAgICAgICAgICAgd2lkZ2V0TW9kZWwucmVmLndpZHRoICsgYWN0aW9uLnBheWxvYWQuY2hhbmdlZFdpZHRoXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHdpZGdldE1vZGVsLnNldChcclxuICAgICAgICAgICAgICAgICdoZWlnaHQnLFxyXG4gICAgICAgICAgICAgICAgd2lkZ2V0TW9kZWwucmVmLmhlaWdodCArIGFjdGlvbi5wYXlsb2FkLmNoYW5nZWRIZWlnaHRcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIENIQU5HRV9XSURHRVRfU1RZTEU6XHJcbiAgICAgICAgICAgIFdpZGdldC53aXRoSWQoYWN0aW9uLnBheWxvYWQud2lkZ2V0SWQpLnNldChcclxuICAgICAgICAgICAgICAgICdzdHlsZScsXHJcbiAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZC5zdHlsZVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBlbnRpdGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHNlc3Npb24uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICBjYXNlIENIQU5HRV9XSURHRVRfVkFMVUU6XHJcbiAgICAgICAgICAgIFdpZGdldC53aXRoSWQoYWN0aW9uLnBheWxvYWQud2lkZ2V0SWQpLnNldChcclxuICAgICAgICAgICAgICAgICd2YWx1ZScsXHJcbiAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZC52YWx1ZVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBlbnRpdGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHNlc3Npb24uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBERUxFVEVfUEFHRVM6IHtcclxuICAgICAgICAgICAgY29uc3QgcGFnZXNUZW1wbGF0ZSA9IGFjdGlvbi5wYXlsb2FkO1xyXG4gICAgICAgICAgICBkaWZmZXJlbmNlKFxyXG4gICAgICAgICAgICAgICAgVGVtcGxhdGUud2l0aElkKHN0YXRlLmN1cnJlbnRUZW1wbGF0ZUlkKVxyXG4gICAgICAgICAgICAgICAgICAgIC50ZW1wbGF0ZVBhZ2VzLnRvUmVmQXJyYXkoKVxyXG4gICAgICAgICAgICAgICAgICAgIC5tYXAoKHApID0+IHAuaWQpLFxyXG4gICAgICAgICAgICAgICAgcGFnZXNUZW1wbGF0ZVxyXG4gICAgICAgICAgICAgICAgICAgIC5maWx0ZXIoKHApID0+IGlzTmlsKHAuZ3JvdXBlZF9ieSkpXHJcbiAgICAgICAgICAgICAgICAgICAgLm1hcCgocCkgPT4gcC5pZClcclxuICAgICAgICAgICAgKS5mb3JFYWNoKChwKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChwKS53aWRnZXRzLmRlbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgUGFnZS53aXRoSWQocCkuZGVsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgZGlmZmVyZW5jZShcclxuICAgICAgICAgICAgICAgIFRlbXBsYXRlLndpdGhJZChzdGF0ZS5jdXJyZW50VGVtcGxhdGVJZClcclxuICAgICAgICAgICAgICAgICAgICAudGVtcGxhdGVQYWdlcy50b1JlZkFycmF5KClcclxuICAgICAgICAgICAgICAgICAgICAubWFwKChwKSA9PiBwLmlkKSxcclxuICAgICAgICAgICAgICAgIHBhZ2VzVGVtcGxhdGVcclxuICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKChwKSA9PiBpc05pbChwLmdyb3VwZWRfYnkpKVxyXG4gICAgICAgICAgICAgICAgICAgIC5tYXAoKHApID0+IHAuaWQpXHJcbiAgICAgICAgICAgICkuZm9yRWFjaCgocCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgUGFnZS53aXRoSWQocCkud2lkZ2V0cy5kZWxldGUoKTtcclxuICAgICAgICAgICAgICAgIFBhZ2Uud2l0aElkKHApLmRlbGV0ZSgpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIERFTEVURV9XSURHRVQ6XHJcbiAgICAgICAgICAgIFdpZGdldC53aXRoSWQoYWN0aW9uLnBheWxvYWQpLmRlbGV0ZSgpO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBlbnRpdGllczoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IHNlc3Npb24uc3RhdGUsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgY2FzZSBJTklUSUFMSVpFOiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHsgcGFnZXNfdGVtcGxhdGU6IHBhZ2VzVGVtcGxhdGUsIC4uLnRlbXBsYXRlRGF0YSB9ID1cclxuICAgICAgICAgICAgICAgIGFjdGlvbi5wYXlsb2FkO1xyXG4gICAgICAgICAgICBUZW1wbGF0ZS5jcmVhdGUoeyAuLi50ZW1wbGF0ZURhdGEgfSk7XHJcbiAgICAgICAgICAgIHBhZ2VzVGVtcGxhdGVcclxuICAgICAgICAgICAgICAgIC5maWx0ZXIoKHApID0+IGlzTmlsKHAuZ3JvdXBlZF9ieSkpXHJcbiAgICAgICAgICAgICAgICAuZm9yRWFjaCgocCkgPT5cclxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVQYWdlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFnZU1vZGVsOiBQYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wUGFnZTogcCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVJZDogdGVtcGxhdGVEYXRhLmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWRnZXRNb2RlbDogV2lkZ2V0LFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBjb25zdCBncm91cHMgPSBjcmVhdGVHcm91cHMocGFnZXNUZW1wbGF0ZSk7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgZ3JvdXBLZXkgaW4gZ3JvdXBzKSB7XHJcbiAgICAgICAgICAgICAgICBjcmVhdGVHcm91cCh7XHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXA6IGdyb3Vwc1tncm91cEtleV0sXHJcbiAgICAgICAgICAgICAgICAgICAgZ3JvdXBNb2RlbDogR3JvdXAsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZU1vZGVsOiBQYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlSWQ6IHRlbXBsYXRlRGF0YS5pZCxcclxuICAgICAgICAgICAgICAgICAgICB3aWRnZXRNb2RlbDogV2lkZ2V0LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlSWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiB0ZW1wbGF0ZURhdGEuaWQsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIFJFX09SREVSX1BBR0VTOiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VzVGVtcGxhdGUgPSBhY3Rpb24ucGF5bG9hZDtcclxuICAgICAgICAgICAgcGFnZXNUZW1wbGF0ZS5mb3JFYWNoKChwKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoUGFnZS5pZEV4aXN0cyhwLmlkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIFBhZ2Uud2l0aElkKHAuaWQpLnNldCgncGFnZScsIHAucGFnZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zdCBncm91cHMgPSBjcmVhdGVHcm91cHMocGFnZXNUZW1wbGF0ZSk7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIGdyb3Vwcykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY3VycmVudEdyb3VwID0gZ3JvdXBzW2tleV07XHJcbiAgICAgICAgICAgICAgICBHcm91cC53aXRoSWQoY3VycmVudEdyb3VwLmlkKS5zZXQoXHJcbiAgICAgICAgICAgICAgICAgICAgJ3BhZ2UnLFxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRHcm91cC5maXJzdFBhZ2VcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZShzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjYXNlIFJFU0VUOlxyXG4gICAgICAgIGNhc2UgUkVTRVRfVEVNUExBVEU6XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgICRzZXQ6IElOSVRJQUxfU1RBVEUsXHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjYXNlIFNFVF9QQUdFX0RBVEE6IHtcclxuICAgICAgICAgICAgY29uc3QgeyBpZCwgaW5maW5pdGVNb2RlLCBvcmllbnRhdGlvbiwgdGhlbWUsIHdpZGdldHMgfSA9XHJcbiAgICAgICAgICAgICAgICBhY3Rpb24ucGF5bG9hZDtcclxuICAgICAgICAgICAgUGFnZS53aXRoSWQoaWQpLnVwZGF0ZSh7XHJcbiAgICAgICAgICAgICAgICBpbmZpbml0ZU1vZGU6IGluZmluaXRlTW9kZSxcclxuICAgICAgICAgICAgICAgIG9yaWVudGF0aW9uOiBvcmllbnRhdGlvbixcclxuICAgICAgICAgICAgICAgIHRoZW1lOiB0aGVtZSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGRpZmZlcmVuY2UoXHJcbiAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChpZClcclxuICAgICAgICAgICAgICAgICAgICAud2lkZ2V0cy50b1JlZkFycmF5KClcclxuICAgICAgICAgICAgICAgICAgICAubWFwKCh3KSA9PiB3LmlkKSxcclxuICAgICAgICAgICAgICAgIHdpZGdldHMubWFwKCh3KSA9PiB3LmlkKVxyXG4gICAgICAgICAgICApLmZvckVhY2goKHdpZGdldElkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBXaWRnZXQud2l0aElkKHdpZGdldElkKS5kZWxldGUoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHdpZGdldHMuZm9yRWFjaCgod2lkZ2V0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoV2lkZ2V0LmlkRXhpc3RzKHdpZGdldC5pZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBXaWRnZXQud2l0aElkKHdpZGdldC5pZCkudXBkYXRlKHdpZGdldCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIFdpZGdldC5jcmVhdGUod2lkZ2V0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiB1cGRhdGUoc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGVudGl0aWVzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNldDogc2Vzc2lvbi5zdGF0ZSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2FzZSBTRVRfVEVNUExBVEVfQkFDS1VQOlxyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZUJhY2tVcDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IGFjdGlvbi5wYXlsb2FkLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNhc2UgUkVTVE9SRV9SRVZJRVdfUEFHRVM6IHtcclxuICAgICAgICAgICAgY29uc3QgYmFja3VwUGFnZXMgPSBKU09OLnBhcnNlKHN0YXRlLnRlbXBsYXRlQmFja1VwKTtcclxuICAgICAgICAgICAgYmFja3VwUGFnZXMuZm9yRWFjaCgocCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKFBhZ2UuaWRFeGlzdHMocC5pZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChwLmlkKS51cGRhdGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZU1vZGU6IHAuaW5maW5pdGVNb2RlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcmllbnRhdGlvbjogcC5vcmllbnRhdGlvbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhlbWU6IHAudGhlbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlmZmVyZW5jZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgUGFnZS53aXRoSWQocC5pZClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC53aWRnZXRzLnRvUmVmQXJyYXkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLm1hcCgodykgPT4gdy5pZCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHAud2lkZ2V0cy5tYXAoKHcpID0+IHcuaWQpXHJcbiAgICAgICAgICAgICAgICAgICAgKS5mb3JFYWNoKCh3aWRnZXRJZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBXaWRnZXQud2l0aElkKHdpZGdldElkKS5kZWxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcC53aWRnZXRzLmZvckVhY2goKHdpZGdldCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoV2lkZ2V0LmlkRXhpc3RzKHdpZGdldC5pZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFdpZGdldC53aXRoSWQod2lkZ2V0LmlkKS51cGRhdGUod2lkZ2V0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFdpZGdldC5jcmVhdGUod2lkZ2V0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgZGlmZmVyZW5jZShcclxuICAgICAgICAgICAgICAgIFRlbXBsYXRlLndpdGhJZChzdGF0ZS5jdXJyZW50VGVtcGxhdGVJZClcclxuICAgICAgICAgICAgICAgICAgICAudGVtcGxhdGVQYWdlcy50b1JlZkFycmF5KClcclxuICAgICAgICAgICAgICAgICAgICAubWFwKChwKSA9PiBwLmlkKSxcclxuICAgICAgICAgICAgICAgIGJhY2t1cFBhZ2VzLm1hcCgocCkgPT4gcC5pZClcclxuICAgICAgICAgICAgKS5mb3JFYWNoKChwKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBQYWdlLndpdGhJZChwKS53aWRnZXRzLmRlbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgUGFnZS53aXRoSWQocCkuZGVsZXRlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICByZXR1cm4gdXBkYXRlKHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZUJhY2tVcDoge1xyXG4gICAgICAgICAgICAgICAgICAgICRzZXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZW50aXRpZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAkc2V0OiBzZXNzaW9uLnN0YXRlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gc3RhdGU7XHJcbiAgICB9XHJcbn1cclxuIl19