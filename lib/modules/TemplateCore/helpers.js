"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.orderPages = exports.getUpdatingContentData = exports.getPageUpdatingContentData = exports.getNum = exports.getNewOrderIndex = exports.getLimits = exports.getFirstCatalogId = exports.getDefaultWidgetValues = exports.getDefaultWidgetPosition = exports.getDefaultPosition = exports.createPage = exports.createGroups = exports.createGroup = exports.buildTemplateWidgets = void 0;

var _cloneDeep = _interopRequireDefault(require("lodash/cloneDeep"));

var _get = _interopRequireDefault(require("lodash/get"));

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _sortBy = _interopRequireDefault(require("lodash/sortBy"));

var _template = require("../../constants/template");

var _template2 = require("../../helpers/template");

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var getPagesTemplate = function getPagesTemplate(pages) {
  return pages.reduce(function (acc, item) {
    if (item.type === 'group') {
      var tempGroupPages = item.pages.map(function (page) {
        return getPageUpdatingContentData(page);
      });
      return [].concat(_toConsumableArray(acc), _toConsumableArray(tempGroupPages));
    }

    return [].concat(_toConsumableArray(acc), [getPageUpdatingContentData(item)]);
  }, []);
};

var getWidgetUpdatingContentData = function getWidgetUpdatingContentData(widget) {
  var widgetData = {
    field: widget.field,
    meta: {
      hasSummarySupport: widget.hasSummarySupport,
      height: widget.height,
      num: widget.num,
      order: widget.order,
      replaceInfoRequired: widget.replaceInfoRequired,
      style: widget.style,
      value: widget.value,
      supportVersion: widget.supportVersion,
      width: widget.width,
      x: widget.x,
      y: widget.y
    },
    name: widget.name
  };
  return {
    widget: widgetData,
    value: _defineProperty({}, widget.field, widget.value),
    replaceInfoData: widget.replaceInfoRequired ? {
      field: widget.field,
      replaceBy: widget.replaceInfoRequired
    } : null
  };
};

var buildTemplateWidgets = function buildTemplateWidgets(_ref) {
  var pageId = _ref.pageId,
      values = _ref.values,
      widgetModel = _ref.widgetModel,
      widgets = _ref.widgets;
  widgets.forEach(function (widget) {
    var name = widget.name,
        field = widget.field,
        meta = widget.meta;
    var freeGrid = meta.freeGrid,
        hasSummarySupport = meta.hasSummarySupport,
        height = meta.height,
        id = meta.id,
        num = meta.num,
        order = meta.order,
        replaceInfoRequired = meta.replaceInfoRequired,
        _meta$style = meta.style,
        style = _meta$style === void 0 ? {} : _meta$style,
        _meta$supportVersion = meta.supportVersion,
        supportVersion = _meta$supportVersion === void 0 ? 1 : _meta$supportVersion,
        width = meta.width,
        x = meta.x,
        y = meta.y;
    widgetModel.create({
      id: id || "".concat(pageId, "/").concat(field),
      field: field,
      hasSummarySupport: hasSummarySupport,
      height: supportVersion === 1 && !freeGrid ? height * _template.GRID_HEIGHT : height,
      name: name,
      num: num,
      order: order,
      pageId: pageId,
      replaceInfoRequired: replaceInfoRequired,
      style: style,
      supportVersion: _template.WIDGET_SUPPORT_VERSION,
      value: values[field],
      width: supportVersion === 1 && !freeGrid ? width * _template.GRID_WIDTH : width,
      x: supportVersion === 1 && !freeGrid ? x * _template.GRID_WIDTH : x,
      y: supportVersion === 1 && !freeGrid ? y * _template.GRID_HEIGHT : y
    });
  });
};

exports.buildTemplateWidgets = buildTemplateWidgets;

var createGroup = function createGroup(_ref2) {
  var group = _ref2.group,
      groupModel = _ref2.groupModel,
      pageModel = _ref2.pageModel,
      templateId = _ref2.templateId,
      widgetModel = _ref2.widgetModel;
  var groupBlocked = group.blocked,
      firstPage = group.firstPage,
      groupId = group.id,
      _group$pages = group.pages,
      pages = _group$pages === void 0 ? [] : _group$pages;
  groupModel.create({
    blocked: groupBlocked,
    id: groupId,
    page: firstPage,
    templateId: templateId
  });
  pages.forEach(function (p) {
    return createPage({
      pageModel: pageModel,
      tempPage: p,
      groupId: groupId,
      widgetModel: widgetModel
    });
  });
};

exports.createGroup = createGroup;

var createGroups = function createGroups(pagesTemplate) {
  return (0, _sortBy.default)(pagesTemplate.filter(function (p) {
    return !(0, _isNil.default)(p.grouped_by);
  }), [function (p) {
    return parseInt(p.page);
  }]).reduce(function (acc, current) {
    if (acc[current.grouped_by]) {
      return _objectSpread(_objectSpread({}, acc), {}, _defineProperty({}, current.grouped_by, _objectSpread(_objectSpread({}, acc[current.grouped_by]), {}, {
        pages: [].concat(_toConsumableArray(acc[current.grouped_by].pages), [current])
      })));
    } else {
      return _objectSpread(_objectSpread({}, acc), {}, _defineProperty({}, current.grouped_by, {
        blocked: current.blocked,
        firstPage: current.page,
        id: current.grouped_by,
        pages: [current]
      }));
    }
  }, {});
};

exports.createGroups = createGroups;

var createPage = function createPage(_ref3) {
  var groupId = _ref3.groupId,
      pageModel = _ref3.pageModel,
      tempPage = _ref3.tempPage,
      templateId = _ref3.templateId,
      widgetModel = _ref3.widgetModel;
  var _tempPage$blocked = tempPage.blocked,
      blocked = _tempPage$blocked === void 0 ? 0 : _tempPage$blocked,
      content = tempPage.content,
      created_at = tempPage.created_at,
      custom_template = tempPage.custom_template,
      grouped_by = tempPage.grouped_by,
      id = tempPage.id,
      page = tempPage.page,
      page_parent = tempPage.page_parent,
      company = tempPage.company,
      updated_at = tempPage.updated_at;
  var parsedData = JSON.parse(content);

  var _Object$keys = Object.keys(parsedData),
      _Object$keys2 = _slicedToArray(_Object$keys, 1),
      _Object$keys2$ = _Object$keys2[0],
      parsedDataKey = _Object$keys2$ === void 0 ? {} : _Object$keys2$;

  var _parsedData$parsedDat = parsedData[parsedDataKey],
      _parsedData$parsedDat2 = _parsedData$parsedDat.infiniteMode,
      infiniteMode = _parsedData$parsedDat2 === void 0 ? false : _parsedData$parsedDat2,
      _parsedData$parsedDat3 = _parsedData$parsedDat.orientation,
      orientation = _parsedData$parsedDat3 === void 0 ? 'portrait' : _parsedData$parsedDat3,
      replaceInfo = _parsedData$parsedDat.replaceInfo,
      _parsedData$parsedDat4 = _parsedData$parsedDat.theme,
      theme = _parsedData$parsedDat4 === void 0 ? 'defaultTheme' : _parsedData$parsedDat4,
      values = _parsedData$parsedDat.values,
      widgets = _parsedData$parsedDat.widgets;
  var tempFields = {
    blocked: blocked,
    content: content,
    created_at: created_at,
    custom_template: custom_template,
    grouped_by: grouped_by,
    id: id,
    infiniteMode: infiniteMode,
    orientation: orientation,
    page: page,
    page_parent: page_parent,
    parsedDataKey: parsedDataKey,
    replaceInfo: replaceInfo,
    templateId: templateId,
    theme: theme,
    company: company,
    updated_at: updated_at
  };

  if (groupId) {
    tempFields.groupId = groupId;
  }

  if (templateId) {
    tempFields.templateId = templateId;
  }

  pageModel.create(tempFields);
  buildTemplateWidgets({
    pageId: id,
    values: values,
    widgetModel: widgetModel,
    widgets: widgets
  });
};

exports.createPage = createPage;

var getDefaultPosition = function getDefaultPosition(height, width, defaultPosition, orientation, showGuides) {
  if (defaultPosition === null) {
    return {
      x: getMiddleX(showGuides, orientation, width),
      y: getMiddleY(showGuides, orientation, height)
    };
  }

  return {
    x: defaultPosition.x,
    y: defaultPosition.y
  };
};

exports.getDefaultPosition = getDefaultPosition;

var getDefaultWidgetPosition = function getDefaultWidgetPosition(defaultValues, defaultPosition, orientation) {
  var height = defaultValues.height,
      width = defaultValues.width;
  return _objectSpread(_objectSpread({}, defaultValues), getDefaultPosition(height, width, defaultPosition, orientation));
};

exports.getDefaultWidgetPosition = getDefaultWidgetPosition;

var getDefaultWidgetValues = function getDefaultWidgetValues(hasSummarySupport, onlineWidgets, orientation, proposalNumber, theme, type) {
  var chartWidgets = onlineWidgets.chartWidgets,
      tableWidgets = onlineWidgets.tableWidgets;
  var limitX = orientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT;
  var limitY = orientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH;

  switch (type) {
    case 'clip-art':
      return _constants.DEFAULT_WIDGET_VALUES.clipArt;

    case 'image':
      return _constants.DEFAULT_WIDGET_VALUES.image;

    case 'map':
      return _objectSpread(_objectSpread({}, _constants.DEFAULT_WIDGET_VALUES.map), {}, {
        hasSummarySupport: hasSummarySupport,
        value: _objectSpread(_objectSpread({}, _constants.DEFAULT_WIDGET_VALUES.map.value), {}, {
          proposal_number: proposalNumber
        })
      });

    case 'segments-map':
      return _objectSpread(_objectSpread({}, _constants.DEFAULT_WIDGET_VALUES.segmentsMap), {}, {
        hasSummarySupport: hasSummarySupport,
        value: _objectSpread(_objectSpread({}, _constants.DEFAULT_WIDGET_VALUES.segmentsMap.value), {}, {
          proposal_number: proposalNumber
        })
      });

    case 'text':
      return _constants.DEFAULT_WIDGET_VALUES.text;

    case 'separator':
      return _constants.DEFAULT_WIDGET_VALUES.separator;

    case 'signature':
      return _constants.DEFAULT_WIDGET_VALUES.signature;

    default:
      var _iterator = _createForOfIteratorHelper(chartWidgets),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var _step$value = _step.value,
              id = _step$value.id,
              defaultValue = _step$value.defaultValue,
              meta = _step$value.meta;

          if (type === id) {
            var freeGrid = meta.freeGrid,
                height = meta.height,
                replaceInfoRequired = meta.replaceInfoRequired,
                _meta$supportVersion2 = meta.supportVersion,
                supportVersion = _meta$supportVersion2 === void 0 ? 1 : _meta$supportVersion2,
                width = meta.width;
            var clonedDefaultValues = (0, _cloneDeep.default)(defaultValue);
            var realWidth = supportVersion === 1 && !freeGrid ? width * _template.GRID_WIDTH : width;
            var realHeight = supportVersion === 1 && !freeGrid ? height * _template.GRID_HEIGHT : height;
            return {
              hasSummarySupport: hasSummarySupport,
              height: realHeight > limitY ? limitY : realHeight,
              replaceInfoRequired: replaceInfoRequired,
              style: {},
              value: _objectSpread(_objectSpread({}, clonedDefaultValues), {}, {
                colors: (0, _template2.buildGraphsColors)(clonedDefaultValues),
                proposal_number: proposalNumber
              }),
              width: realWidth > limitX ? limitX : realWidth
            };
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      var _iterator2 = _createForOfIteratorHelper(tableWidgets),
          _step2;

      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _step2$value = _step2.value,
              _id = _step2$value.id,
              _defaultValue = _step2$value.defaultValue,
              _meta = _step2$value.meta;

          if (type === _id) {
            var _freeGrid = _meta.freeGrid,
                _height = _meta.height,
                _replaceInfoRequired = _meta.replaceInfoRequired,
                _meta$supportVersion3 = _meta.supportVersion,
                _supportVersion = _meta$supportVersion3 === void 0 ? 1 : _meta$supportVersion3,
                _width = _meta.width;

            var _realWidth = _supportVersion === 1 && !_freeGrid ? _width * _template.GRID_WIDTH : _width;

            var _realHeight = _supportVersion === 1 && !_freeGrid ? _height * _template.GRID_HEIGHT : _height;

            return {
              hasSummarySupport: hasSummarySupport,
              height: _realHeight > limitY ? limitY : _realHeight,
              replaceInfoRequired: _replaceInfoRequired,
              style: (0, _get.default)(_constants.TABLE_THEMES, "".concat(theme, ".tableStyle"), {}),
              value: (0, _cloneDeep.default)(_objectSpread(_objectSpread({}, _defaultValue), {}, {
                proposal_number: proposalNumber
              })),
              width: _realWidth > limitX ? limitX : _realWidth
            };
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }

      return {};
  }
};

exports.getDefaultWidgetValues = getDefaultWidgetValues;

var getFirstCatalogId = function getFirstCatalogId(catalogs) {
  if (catalogs.length) {
    var catalog = catalogs.find(function (item) {
      return item.order === 1;
    });

    if (catalog) {
      return catalog.id;
    }
  }

  return '';
};

exports.getFirstCatalogId = getFirstCatalogId;

var getLimits = function getLimits(orientation) {
  if (orientation === 'landscape') {
    return {
      x: _template.PAGE_HEIGHT,
      y: _template.PAGE_WIDTH
    };
  }

  return {
    x: _template.PAGE_WIDTH,
    y: _template.PAGE_HEIGHT
  };
};

exports.getLimits = getLimits;

var getMiddleX = function getMiddleX(showGuides, orientation, width) {
  var middle = ((orientation === 'portrait' ? _template.PAGE_WIDTH : _template.PAGE_HEIGHT) - width) / 2;

  if (showGuides) {
    return middle - middle % _template.GRID_WIDTH;
  }

  return middle;
};

var getMiddleY = function getMiddleY(showGuides, orientation, height) {
  var middle = ((orientation === 'portrait' ? _template.PAGE_HEIGHT : _template.PAGE_WIDTH) - height) / 2;

  if (showGuides) {
    return middle - middle % _template.GRID_HEIGHT;
  }

  return middle;
};

var getNewOrderIndex = function getNewOrderIndex(currentIndex, moveType, listSize) {
  switch (moveType) {
    case _template.MOVE_BACK:
      if (currentIndex === 0) {
        return null;
      }

      return 0;

    case _template.MOVE_FORWARD:
      if (currentIndex === listSize - 1) {
        return null;
      }

      return listSize - 1;

    case _template.MOVE_STEP_BACK:
      if (currentIndex === 0) {
        return null;
      }

      return currentIndex - 1;

    case _template.MOVE_STEP_FORWARD:
      if (currentIndex === listSize - 1) {
        return null;
      }

      return currentIndex + 1;

    default:
      return null;
  }
};

exports.getNewOrderIndex = getNewOrderIndex;

var getNum = function getNum(type, array) {
  var typeList = (0, _sortBy.default)(array.filter(function (item) {
    return item.name === type;
  }), [function (p) {
    return parseInt(p.num);
  }]);

  if (typeList.length === 0) {
    return 1;
  }

  return parseInt(typeList[typeList.length - 1].num) + 1;
};

exports.getNum = getNum;

var getPageUpdatingContentData = function getPageUpdatingContentData(page) {
  if (page.id === '') {
    return page;
  }

  var tempValues = {};
  var tempWidgets = [];
  var replaceInfo = [];
  page.widgets.forEach(function (widget) {
    var _getWidgetUpdatingCon = getWidgetUpdatingContentData(widget),
        replaceInfoData = _getWidgetUpdatingCon.replaceInfoData,
        value = _getWidgetUpdatingCon.value,
        widgetData = _getWidgetUpdatingCon.widget;

    tempValues = _objectSpread(_objectSpread({}, tempValues), value);
    tempWidgets = [].concat(_toConsumableArray(tempWidgets), [widgetData]);

    if (replaceInfoData) {
      replaceInfo = [].concat(_toConsumableArray(replaceInfo), [replaceInfoData]);
    }
  });
  var content = JSON.stringify(_defineProperty({}, page.parsedDataKey, {
    theme: page.theme,
    orientation: page.orientation,
    infiniteMode: page.infiniteMode,
    replaceInfo: replaceInfo,
    values: tempValues,
    widgets: tempWidgets
  }));
  return {
    blocked: page.blocked,
    company: page.company,
    content: content,
    custom_template: page.custom_template,
    grouped_by: page.grouped_by,
    id: page.id,
    page: page.page,
    page_parent: page.page_parent
  };
};

exports.getPageUpdatingContentData = getPageUpdatingContentData;

var getUpdatingContentData = function getUpdatingContentData(templateData) {
  var modifiedTemplateData = {
    company: templateData.company,
    created_at: templateData.created_at,
    description: templateData.description,
    footer: templateData.footer,
    id: templateData.id,
    logo: templateData.logo,
    parent: templateData.parent,
    primary_color: templateData.primary_color,
    secondary_color: templateData.secondary_color,
    title: templateData.title,
    typography: templateData.typography,
    updated_at: templateData.updated_at,
    version: templateData.version,
    pages_template: getPagesTemplate(templateData.pages)
  };
  return modifiedTemplateData;
};

exports.getUpdatingContentData = getUpdatingContentData;

var orderPages = function orderPages(pagesList) {
  var indexPage = 0;
  return pagesList.map(function (item) {
    if (item.type === 'group') {
      var firstPage = "".concat(indexPage + 1);
      var newPages = item.pages.map(function (element) {
        ++indexPage;
        return _objectSpread(_objectSpread({}, element), {}, {
          page: "".concat(indexPage)
        });
      });
      return _objectSpread(_objectSpread({}, item), {}, {
        page: firstPage,
        pages: newPages
      });
    }

    ++indexPage;
    return _objectSpread(_objectSpread({}, item), {}, {
      page: "".concat(indexPage)
    });
  });
};

exports.orderPages = orderPages;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9tb2R1bGVzL1RlbXBsYXRlQ29yZS9oZWxwZXJzLmpzIl0sIm5hbWVzIjpbImdldFBhZ2VzVGVtcGxhdGUiLCJwYWdlcyIsInJlZHVjZSIsImFjYyIsIml0ZW0iLCJ0eXBlIiwidGVtcEdyb3VwUGFnZXMiLCJtYXAiLCJwYWdlIiwiZ2V0UGFnZVVwZGF0aW5nQ29udGVudERhdGEiLCJnZXRXaWRnZXRVcGRhdGluZ0NvbnRlbnREYXRhIiwid2lkZ2V0Iiwid2lkZ2V0RGF0YSIsImZpZWxkIiwibWV0YSIsImhhc1N1bW1hcnlTdXBwb3J0IiwiaGVpZ2h0IiwibnVtIiwib3JkZXIiLCJyZXBsYWNlSW5mb1JlcXVpcmVkIiwic3R5bGUiLCJ2YWx1ZSIsInN1cHBvcnRWZXJzaW9uIiwid2lkdGgiLCJ4IiwieSIsIm5hbWUiLCJyZXBsYWNlSW5mb0RhdGEiLCJyZXBsYWNlQnkiLCJidWlsZFRlbXBsYXRlV2lkZ2V0cyIsInBhZ2VJZCIsInZhbHVlcyIsIndpZGdldE1vZGVsIiwid2lkZ2V0cyIsImZvckVhY2giLCJmcmVlR3JpZCIsImlkIiwiY3JlYXRlIiwiR1JJRF9IRUlHSFQiLCJXSURHRVRfU1VQUE9SVF9WRVJTSU9OIiwiR1JJRF9XSURUSCIsImNyZWF0ZUdyb3VwIiwiZ3JvdXAiLCJncm91cE1vZGVsIiwicGFnZU1vZGVsIiwidGVtcGxhdGVJZCIsImdyb3VwQmxvY2tlZCIsImJsb2NrZWQiLCJmaXJzdFBhZ2UiLCJncm91cElkIiwicCIsImNyZWF0ZVBhZ2UiLCJ0ZW1wUGFnZSIsImNyZWF0ZUdyb3VwcyIsInBhZ2VzVGVtcGxhdGUiLCJmaWx0ZXIiLCJncm91cGVkX2J5IiwicGFyc2VJbnQiLCJjdXJyZW50IiwiY29udGVudCIsImNyZWF0ZWRfYXQiLCJjdXN0b21fdGVtcGxhdGUiLCJwYWdlX3BhcmVudCIsImNvbXBhbnkiLCJ1cGRhdGVkX2F0IiwicGFyc2VkRGF0YSIsIkpTT04iLCJwYXJzZSIsIk9iamVjdCIsImtleXMiLCJwYXJzZWREYXRhS2V5IiwiaW5maW5pdGVNb2RlIiwib3JpZW50YXRpb24iLCJyZXBsYWNlSW5mbyIsInRoZW1lIiwidGVtcEZpZWxkcyIsImdldERlZmF1bHRQb3NpdGlvbiIsImRlZmF1bHRQb3NpdGlvbiIsInNob3dHdWlkZXMiLCJnZXRNaWRkbGVYIiwiZ2V0TWlkZGxlWSIsImdldERlZmF1bHRXaWRnZXRQb3NpdGlvbiIsImRlZmF1bHRWYWx1ZXMiLCJnZXREZWZhdWx0V2lkZ2V0VmFsdWVzIiwib25saW5lV2lkZ2V0cyIsInByb3Bvc2FsTnVtYmVyIiwiY2hhcnRXaWRnZXRzIiwidGFibGVXaWRnZXRzIiwibGltaXRYIiwiUEFHRV9XSURUSCIsIlBBR0VfSEVJR0hUIiwibGltaXRZIiwiREVGQVVMVF9XSURHRVRfVkFMVUVTIiwiY2xpcEFydCIsImltYWdlIiwicHJvcG9zYWxfbnVtYmVyIiwic2VnbWVudHNNYXAiLCJ0ZXh0Iiwic2VwYXJhdG9yIiwic2lnbmF0dXJlIiwiZGVmYXVsdFZhbHVlIiwiY2xvbmVkRGVmYXVsdFZhbHVlcyIsInJlYWxXaWR0aCIsInJlYWxIZWlnaHQiLCJjb2xvcnMiLCJUQUJMRV9USEVNRVMiLCJnZXRGaXJzdENhdGFsb2dJZCIsImNhdGFsb2dzIiwibGVuZ3RoIiwiY2F0YWxvZyIsImZpbmQiLCJnZXRMaW1pdHMiLCJtaWRkbGUiLCJnZXROZXdPcmRlckluZGV4IiwiY3VycmVudEluZGV4IiwibW92ZVR5cGUiLCJsaXN0U2l6ZSIsIk1PVkVfQkFDSyIsIk1PVkVfRk9SV0FSRCIsIk1PVkVfU1RFUF9CQUNLIiwiTU9WRV9TVEVQX0ZPUldBUkQiLCJnZXROdW0iLCJhcnJheSIsInR5cGVMaXN0IiwidGVtcFZhbHVlcyIsInRlbXBXaWRnZXRzIiwic3RyaW5naWZ5IiwiZ2V0VXBkYXRpbmdDb250ZW50RGF0YSIsInRlbXBsYXRlRGF0YSIsIm1vZGlmaWVkVGVtcGxhdGVEYXRhIiwiZGVzY3JpcHRpb24iLCJmb290ZXIiLCJsb2dvIiwicGFyZW50IiwicHJpbWFyeV9jb2xvciIsInNlY29uZGFyeV9jb2xvciIsInRpdGxlIiwidHlwb2dyYXBoeSIsInZlcnNpb24iLCJwYWdlc190ZW1wbGF0ZSIsIm9yZGVyUGFnZXMiLCJwYWdlc0xpc3QiLCJpbmRleFBhZ2UiLCJuZXdQYWdlcyIsImVsZW1lbnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFFQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFZQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFNQSxJQUFNQSxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLEtBQUQsRUFBVztBQUNoQyxTQUFPQSxLQUFLLENBQUNDLE1BQU4sQ0FBYSxVQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBZTtBQUMvQixRQUFJQSxJQUFJLENBQUNDLElBQUwsS0FBYyxPQUFsQixFQUEyQjtBQUN2QixVQUFNQyxjQUFjLEdBQUdGLElBQUksQ0FBQ0gsS0FBTCxDQUFXTSxHQUFYLENBQWUsVUFBQ0MsSUFBRDtBQUFBLGVBQ2xDQywwQkFBMEIsQ0FBQ0QsSUFBRCxDQURRO0FBQUEsT0FBZixDQUF2QjtBQUdBLDBDQUFXTCxHQUFYLHNCQUFtQkcsY0FBbkI7QUFDSDs7QUFDRCx3Q0FBV0gsR0FBWCxJQUFnQk0sMEJBQTBCLENBQUNMLElBQUQsQ0FBMUM7QUFDSCxHQVJNLEVBUUosRUFSSSxDQUFQO0FBU0gsQ0FWRDs7QUFZQSxJQUFNTSw0QkFBNEIsR0FBRyxTQUEvQkEsNEJBQStCLENBQUNDLE1BQUQsRUFBWTtBQUM3QyxNQUFNQyxVQUFVLEdBQUc7QUFDZkMsSUFBQUEsS0FBSyxFQUFFRixNQUFNLENBQUNFLEtBREM7QUFFZkMsSUFBQUEsSUFBSSxFQUFFO0FBQ0ZDLE1BQUFBLGlCQUFpQixFQUFFSixNQUFNLENBQUNJLGlCQUR4QjtBQUVGQyxNQUFBQSxNQUFNLEVBQUVMLE1BQU0sQ0FBQ0ssTUFGYjtBQUdGQyxNQUFBQSxHQUFHLEVBQUVOLE1BQU0sQ0FBQ00sR0FIVjtBQUlGQyxNQUFBQSxLQUFLLEVBQUVQLE1BQU0sQ0FBQ08sS0FKWjtBQUtGQyxNQUFBQSxtQkFBbUIsRUFBRVIsTUFBTSxDQUFDUSxtQkFMMUI7QUFNRkMsTUFBQUEsS0FBSyxFQUFFVCxNQUFNLENBQUNTLEtBTlo7QUFPRkMsTUFBQUEsS0FBSyxFQUFFVixNQUFNLENBQUNVLEtBUFo7QUFRRkMsTUFBQUEsY0FBYyxFQUFFWCxNQUFNLENBQUNXLGNBUnJCO0FBU0ZDLE1BQUFBLEtBQUssRUFBRVosTUFBTSxDQUFDWSxLQVRaO0FBVUZDLE1BQUFBLENBQUMsRUFBRWIsTUFBTSxDQUFDYSxDQVZSO0FBV0ZDLE1BQUFBLENBQUMsRUFBRWQsTUFBTSxDQUFDYztBQVhSLEtBRlM7QUFlZkMsSUFBQUEsSUFBSSxFQUFFZixNQUFNLENBQUNlO0FBZkUsR0FBbkI7QUFrQkEsU0FBTztBQUNIZixJQUFBQSxNQUFNLEVBQUVDLFVBREw7QUFFSFMsSUFBQUEsS0FBSyxzQkFBS1YsTUFBTSxDQUFDRSxLQUFaLEVBQW9CRixNQUFNLENBQUNVLEtBQTNCLENBRkY7QUFHSE0sSUFBQUEsZUFBZSxFQUFFaEIsTUFBTSxDQUFDUSxtQkFBUCxHQUNYO0FBQ0lOLE1BQUFBLEtBQUssRUFBRUYsTUFBTSxDQUFDRSxLQURsQjtBQUVJZSxNQUFBQSxTQUFTLEVBQUVqQixNQUFNLENBQUNRO0FBRnRCLEtBRFcsR0FLWDtBQVJILEdBQVA7QUFVSCxDQTdCRDs7QUErQk8sSUFBTVUsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixPQUs5QjtBQUFBLE1BSkZDLE1BSUUsUUFKRkEsTUFJRTtBQUFBLE1BSEZDLE1BR0UsUUFIRkEsTUFHRTtBQUFBLE1BRkZDLFdBRUUsUUFGRkEsV0FFRTtBQUFBLE1BREZDLE9BQ0UsUUFERkEsT0FDRTtBQUNGQSxFQUFBQSxPQUFPLENBQUNDLE9BQVIsQ0FBZ0IsVUFBQ3ZCLE1BQUQsRUFBWTtBQUN4QixRQUFRZSxJQUFSLEdBQThCZixNQUE5QixDQUFRZSxJQUFSO0FBQUEsUUFBY2IsS0FBZCxHQUE4QkYsTUFBOUIsQ0FBY0UsS0FBZDtBQUFBLFFBQXFCQyxJQUFyQixHQUE4QkgsTUFBOUIsQ0FBcUJHLElBQXJCO0FBQ0EsUUFDSXFCLFFBREosR0FhSXJCLElBYkosQ0FDSXFCLFFBREo7QUFBQSxRQUVJcEIsaUJBRkosR0FhSUQsSUFiSixDQUVJQyxpQkFGSjtBQUFBLFFBR0lDLE1BSEosR0FhSUYsSUFiSixDQUdJRSxNQUhKO0FBQUEsUUFJSW9CLEVBSkosR0FhSXRCLElBYkosQ0FJSXNCLEVBSko7QUFBQSxRQUtJbkIsR0FMSixHQWFJSCxJQWJKLENBS0lHLEdBTEo7QUFBQSxRQU1JQyxLQU5KLEdBYUlKLElBYkosQ0FNSUksS0FOSjtBQUFBLFFBT0lDLG1CQVBKLEdBYUlMLElBYkosQ0FPSUssbUJBUEo7QUFBQSxzQkFhSUwsSUFiSixDQVFJTSxLQVJKO0FBQUEsUUFRSUEsS0FSSiw0QkFRWSxFQVJaO0FBQUEsK0JBYUlOLElBYkosQ0FTSVEsY0FUSjtBQUFBLFFBU0lBLGNBVEoscUNBU3FCLENBVHJCO0FBQUEsUUFVSUMsS0FWSixHQWFJVCxJQWJKLENBVUlTLEtBVko7QUFBQSxRQVdJQyxDQVhKLEdBYUlWLElBYkosQ0FXSVUsQ0FYSjtBQUFBLFFBWUlDLENBWkosR0FhSVgsSUFiSixDQVlJVyxDQVpKO0FBY0FPLElBQUFBLFdBQVcsQ0FBQ0ssTUFBWixDQUFtQjtBQUNmRCxNQUFBQSxFQUFFLEVBQUVBLEVBQUUsY0FBT04sTUFBUCxjQUFpQmpCLEtBQWpCLENBRFM7QUFFZkEsTUFBQUEsS0FBSyxFQUFMQSxLQUZlO0FBR2ZFLE1BQUFBLGlCQUFpQixFQUFqQkEsaUJBSGU7QUFJZkMsTUFBQUEsTUFBTSxFQUNGTSxjQUFjLEtBQUssQ0FBbkIsSUFBd0IsQ0FBQ2EsUUFBekIsR0FDTW5CLE1BQU0sR0FBR3NCLHFCQURmLEdBRU10QixNQVBLO0FBUWZVLE1BQUFBLElBQUksRUFBSkEsSUFSZTtBQVNmVCxNQUFBQSxHQUFHLEVBQUhBLEdBVGU7QUFVZkMsTUFBQUEsS0FBSyxFQUFMQSxLQVZlO0FBV2ZZLE1BQUFBLE1BQU0sRUFBTkEsTUFYZTtBQVlmWCxNQUFBQSxtQkFBbUIsRUFBbkJBLG1CQVplO0FBYWZDLE1BQUFBLEtBQUssRUFBTEEsS0FiZTtBQWNmRSxNQUFBQSxjQUFjLEVBQUVpQixnQ0FkRDtBQWVmbEIsTUFBQUEsS0FBSyxFQUFFVSxNQUFNLENBQUNsQixLQUFELENBZkU7QUFnQmZVLE1BQUFBLEtBQUssRUFDREQsY0FBYyxLQUFLLENBQW5CLElBQXdCLENBQUNhLFFBQXpCLEdBQW9DWixLQUFLLEdBQUdpQixvQkFBNUMsR0FBeURqQixLQWpCOUM7QUFrQmZDLE1BQUFBLENBQUMsRUFBRUYsY0FBYyxLQUFLLENBQW5CLElBQXdCLENBQUNhLFFBQXpCLEdBQW9DWCxDQUFDLEdBQUdnQixvQkFBeEMsR0FBcURoQixDQWxCekM7QUFtQmZDLE1BQUFBLENBQUMsRUFBRUgsY0FBYyxLQUFLLENBQW5CLElBQXdCLENBQUNhLFFBQXpCLEdBQW9DVixDQUFDLEdBQUdhLHFCQUF4QyxHQUFzRGI7QUFuQjFDLEtBQW5CO0FBcUJILEdBckNEO0FBc0NILENBNUNNOzs7O0FBOENBLElBQU1nQixXQUFXLEdBQUcsU0FBZEEsV0FBYyxRQU1yQjtBQUFBLE1BTEZDLEtBS0UsU0FMRkEsS0FLRTtBQUFBLE1BSkZDLFVBSUUsU0FKRkEsVUFJRTtBQUFBLE1BSEZDLFNBR0UsU0FIRkEsU0FHRTtBQUFBLE1BRkZDLFVBRUUsU0FGRkEsVUFFRTtBQUFBLE1BREZiLFdBQ0UsU0FERkEsV0FDRTtBQUNGLE1BQWlCYyxZQUFqQixHQUFzRUosS0FBdEUsQ0FBUUssT0FBUjtBQUFBLE1BQStCQyxTQUEvQixHQUFzRU4sS0FBdEUsQ0FBK0JNLFNBQS9CO0FBQUEsTUFBOENDLE9BQTlDLEdBQXNFUCxLQUF0RSxDQUEwQ04sRUFBMUM7QUFBQSxxQkFBc0VNLEtBQXRFLENBQXVEekMsS0FBdkQ7QUFBQSxNQUF1REEsS0FBdkQsNkJBQStELEVBQS9EO0FBQ0EwQyxFQUFBQSxVQUFVLENBQUNOLE1BQVgsQ0FBa0I7QUFDZFUsSUFBQUEsT0FBTyxFQUFFRCxZQURLO0FBRWRWLElBQUFBLEVBQUUsRUFBRWEsT0FGVTtBQUdkekMsSUFBQUEsSUFBSSxFQUFFd0MsU0FIUTtBQUlkSCxJQUFBQSxVQUFVLEVBQVZBO0FBSmMsR0FBbEI7QUFPQTVDLEVBQUFBLEtBQUssQ0FBQ2lDLE9BQU4sQ0FBYyxVQUFDZ0IsQ0FBRDtBQUFBLFdBQ1ZDLFVBQVUsQ0FBQztBQUNQUCxNQUFBQSxTQUFTLEVBQUVBLFNBREo7QUFFUFEsTUFBQUEsUUFBUSxFQUFFRixDQUZIO0FBR1BELE1BQUFBLE9BQU8sRUFBRUEsT0FIRjtBQUlQakIsTUFBQUEsV0FBVyxFQUFFQTtBQUpOLEtBQUQsQ0FEQTtBQUFBLEdBQWQ7QUFRSCxDQXZCTTs7OztBQXlCQSxJQUFNcUIsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsYUFBRCxFQUFtQjtBQUMzQyxTQUFPLHFCQUNIQSxhQUFhLENBQUNDLE1BQWQsQ0FBcUIsVUFBQ0wsQ0FBRDtBQUFBLFdBQU8sQ0FBQyxvQkFBTUEsQ0FBQyxDQUFDTSxVQUFSLENBQVI7QUFBQSxHQUFyQixDQURHLEVBRUgsQ0FDSSxVQUFDTixDQUFELEVBQU87QUFDSCxXQUFPTyxRQUFRLENBQUNQLENBQUMsQ0FBQzFDLElBQUgsQ0FBZjtBQUNILEdBSEwsQ0FGRyxFQU9MTixNQVBLLENBT0UsVUFBQ0MsR0FBRCxFQUFNdUQsT0FBTixFQUFrQjtBQUN2QixRQUFJdkQsR0FBRyxDQUFDdUQsT0FBTyxDQUFDRixVQUFULENBQVAsRUFBNkI7QUFDekIsNkNBQ09yRCxHQURQLDJCQUVLdUQsT0FBTyxDQUFDRixVQUZiLGtDQUdXckQsR0FBRyxDQUFDdUQsT0FBTyxDQUFDRixVQUFULENBSGQ7QUFJUXZELFFBQUFBLEtBQUssK0JBQU1FLEdBQUcsQ0FBQ3VELE9BQU8sQ0FBQ0YsVUFBVCxDQUFILENBQXdCdkQsS0FBOUIsSUFBcUN5RCxPQUFyQztBQUpiO0FBT0gsS0FSRCxNQVFPO0FBQ0gsNkNBQ092RCxHQURQLDJCQUVLdUQsT0FBTyxDQUFDRixVQUZiLEVBRTBCO0FBQ2xCVCxRQUFBQSxPQUFPLEVBQUVXLE9BQU8sQ0FBQ1gsT0FEQztBQUVsQkMsUUFBQUEsU0FBUyxFQUFFVSxPQUFPLENBQUNsRCxJQUZEO0FBR2xCNEIsUUFBQUEsRUFBRSxFQUFFc0IsT0FBTyxDQUFDRixVQUhNO0FBSWxCdkQsUUFBQUEsS0FBSyxFQUFFLENBQUN5RCxPQUFEO0FBSlcsT0FGMUI7QUFTSDtBQUNKLEdBM0JNLEVBMkJKLEVBM0JJLENBQVA7QUE0QkgsQ0E3Qk07Ozs7QUErQkEsSUFBTVAsVUFBVSxHQUFHLFNBQWJBLFVBQWEsUUFNcEI7QUFBQSxNQUxGRixPQUtFLFNBTEZBLE9BS0U7QUFBQSxNQUpGTCxTQUlFLFNBSkZBLFNBSUU7QUFBQSxNQUhGUSxRQUdFLFNBSEZBLFFBR0U7QUFBQSxNQUZGUCxVQUVFLFNBRkZBLFVBRUU7QUFBQSxNQURGYixXQUNFLFNBREZBLFdBQ0U7QUFDRiwwQkFXSW9CLFFBWEosQ0FDSUwsT0FESjtBQUFBLE1BQ0lBLE9BREosa0NBQ2MsQ0FEZDtBQUFBLE1BRUlZLE9BRkosR0FXSVAsUUFYSixDQUVJTyxPQUZKO0FBQUEsTUFHSUMsVUFISixHQVdJUixRQVhKLENBR0lRLFVBSEo7QUFBQSxNQUlJQyxlQUpKLEdBV0lULFFBWEosQ0FJSVMsZUFKSjtBQUFBLE1BS0lMLFVBTEosR0FXSUosUUFYSixDQUtJSSxVQUxKO0FBQUEsTUFNSXBCLEVBTkosR0FXSWdCLFFBWEosQ0FNSWhCLEVBTko7QUFBQSxNQU9JNUIsSUFQSixHQVdJNEMsUUFYSixDQU9JNUMsSUFQSjtBQUFBLE1BUUlzRCxXQVJKLEdBV0lWLFFBWEosQ0FRSVUsV0FSSjtBQUFBLE1BU0lDLE9BVEosR0FXSVgsUUFYSixDQVNJVyxPQVRKO0FBQUEsTUFVSUMsVUFWSixHQVdJWixRQVhKLENBVUlZLFVBVko7QUFZQSxNQUFNQyxVQUFVLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXUixPQUFYLENBQW5COztBQUNBLHFCQUE2QlMsTUFBTSxDQUFDQyxJQUFQLENBQVlKLFVBQVosQ0FBN0I7QUFBQTtBQUFBO0FBQUEsTUFBT0ssYUFBUCwrQkFBdUIsRUFBdkI7O0FBQ0EsOEJBT0lMLFVBQVUsQ0FBQ0ssYUFBRCxDQVBkO0FBQUEscURBQ0lDLFlBREo7QUFBQSxNQUNJQSxZQURKLHVDQUNtQixLQURuQjtBQUFBLHFEQUVJQyxXQUZKO0FBQUEsTUFFSUEsV0FGSix1Q0FFa0IsVUFGbEI7QUFBQSxNQUdJQyxXQUhKLHlCQUdJQSxXQUhKO0FBQUEscURBSUlDLEtBSko7QUFBQSxNQUlJQSxLQUpKLHVDQUlZLGNBSlo7QUFBQSxNQUtJM0MsTUFMSix5QkFLSUEsTUFMSjtBQUFBLE1BTUlFLE9BTkoseUJBTUlBLE9BTko7QUFTQSxNQUFJMEMsVUFBVSxHQUFHO0FBQ2I1QixJQUFBQSxPQUFPLEVBQVBBLE9BRGE7QUFFYlksSUFBQUEsT0FBTyxFQUFQQSxPQUZhO0FBR2JDLElBQUFBLFVBQVUsRUFBVkEsVUFIYTtBQUliQyxJQUFBQSxlQUFlLEVBQWZBLGVBSmE7QUFLYkwsSUFBQUEsVUFBVSxFQUFWQSxVQUxhO0FBTWJwQixJQUFBQSxFQUFFLEVBQUZBLEVBTmE7QUFPYm1DLElBQUFBLFlBQVksRUFBWkEsWUFQYTtBQVFiQyxJQUFBQSxXQUFXLEVBQVhBLFdBUmE7QUFTYmhFLElBQUFBLElBQUksRUFBSkEsSUFUYTtBQVVic0QsSUFBQUEsV0FBVyxFQUFYQSxXQVZhO0FBV2JRLElBQUFBLGFBQWEsRUFBYkEsYUFYYTtBQVliRyxJQUFBQSxXQUFXLEVBQVhBLFdBWmE7QUFhYjVCLElBQUFBLFVBQVUsRUFBVkEsVUFiYTtBQWNiNkIsSUFBQUEsS0FBSyxFQUFMQSxLQWRhO0FBZWJYLElBQUFBLE9BQU8sRUFBUEEsT0FmYTtBQWdCYkMsSUFBQUEsVUFBVSxFQUFWQTtBQWhCYSxHQUFqQjs7QUFtQkEsTUFBSWYsT0FBSixFQUFhO0FBQ1QwQixJQUFBQSxVQUFVLENBQUMxQixPQUFYLEdBQXFCQSxPQUFyQjtBQUNIOztBQUVELE1BQUlKLFVBQUosRUFBZ0I7QUFDWjhCLElBQUFBLFVBQVUsQ0FBQzlCLFVBQVgsR0FBd0JBLFVBQXhCO0FBQ0g7O0FBRURELEVBQUFBLFNBQVMsQ0FBQ1AsTUFBVixDQUFpQnNDLFVBQWpCO0FBRUE5QyxFQUFBQSxvQkFBb0IsQ0FBQztBQUNqQkMsSUFBQUEsTUFBTSxFQUFFTSxFQURTO0FBRWpCTCxJQUFBQSxNQUFNLEVBQU5BLE1BRmlCO0FBR2pCQyxJQUFBQSxXQUFXLEVBQVhBLFdBSGlCO0FBSWpCQyxJQUFBQSxPQUFPLEVBQVBBO0FBSmlCLEdBQUQsQ0FBcEI7QUFNSCxDQWpFTTs7OztBQW1FQSxJQUFNMkMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUM5QjVELE1BRDhCLEVBRTlCTyxLQUY4QixFQUc5QnNELGVBSDhCLEVBSTlCTCxXQUo4QixFQUs5Qk0sVUFMOEIsRUFNN0I7QUFDRCxNQUFJRCxlQUFlLEtBQUssSUFBeEIsRUFBOEI7QUFDMUIsV0FBTztBQUNIckQsTUFBQUEsQ0FBQyxFQUFFdUQsVUFBVSxDQUFDRCxVQUFELEVBQWFOLFdBQWIsRUFBMEJqRCxLQUExQixDQURWO0FBRUhFLE1BQUFBLENBQUMsRUFBRXVELFVBQVUsQ0FBQ0YsVUFBRCxFQUFhTixXQUFiLEVBQTBCeEQsTUFBMUI7QUFGVixLQUFQO0FBSUg7O0FBQ0QsU0FBTztBQUNIUSxJQUFBQSxDQUFDLEVBQUVxRCxlQUFlLENBQUNyRCxDQURoQjtBQUVIQyxJQUFBQSxDQUFDLEVBQUVvRCxlQUFlLENBQUNwRDtBQUZoQixHQUFQO0FBSUgsQ0FqQk07Ozs7QUFtQkEsSUFBTXdELHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsQ0FDcENDLGFBRG9DLEVBRXBDTCxlQUZvQyxFQUdwQ0wsV0FIb0MsRUFJbkM7QUFDRCxNQUFReEQsTUFBUixHQUEwQmtFLGFBQTFCLENBQVFsRSxNQUFSO0FBQUEsTUFBZ0JPLEtBQWhCLEdBQTBCMkQsYUFBMUIsQ0FBZ0IzRCxLQUFoQjtBQUNBLHlDQUNPMkQsYUFEUCxHQUVPTixrQkFBa0IsQ0FBQzVELE1BQUQsRUFBU08sS0FBVCxFQUFnQnNELGVBQWhCLEVBQWlDTCxXQUFqQyxDQUZ6QjtBQUlILENBVk07Ozs7QUFZQSxJQUFNVyxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQ2xDcEUsaUJBRGtDLEVBRWxDcUUsYUFGa0MsRUFHbENaLFdBSGtDLEVBSWxDYSxjQUprQyxFQUtsQ1gsS0FMa0MsRUFNbENyRSxJQU5rQyxFQU9qQztBQUNELE1BQVFpRixZQUFSLEdBQXVDRixhQUF2QyxDQUFRRSxZQUFSO0FBQUEsTUFBc0JDLFlBQXRCLEdBQXVDSCxhQUF2QyxDQUFzQkcsWUFBdEI7QUFDQSxNQUFNQyxNQUFNLEdBQUdoQixXQUFXLEtBQUssVUFBaEIsR0FBNkJpQixvQkFBN0IsR0FBMENDLHFCQUF6RDtBQUNBLE1BQU1DLE1BQU0sR0FBR25CLFdBQVcsS0FBSyxVQUFoQixHQUE2QmtCLHFCQUE3QixHQUEyQ0Qsb0JBQTFEOztBQUNBLFVBQVFwRixJQUFSO0FBQ0ksU0FBSyxVQUFMO0FBQ0ksYUFBT3VGLGlDQUFzQkMsT0FBN0I7O0FBQ0osU0FBSyxPQUFMO0FBQ0ksYUFBT0QsaUNBQXNCRSxLQUE3Qjs7QUFDSixTQUFLLEtBQUw7QUFDSSw2Q0FDT0YsaUNBQXNCckYsR0FEN0I7QUFFSVEsUUFBQUEsaUJBQWlCLEVBQWpCQSxpQkFGSjtBQUdJTSxRQUFBQSxLQUFLLGtDQUNFdUUsaUNBQXNCckYsR0FBdEIsQ0FBMEJjLEtBRDVCO0FBRUQwRSxVQUFBQSxlQUFlLEVBQUVWO0FBRmhCO0FBSFQ7O0FBUUosU0FBSyxjQUFMO0FBQ0ksNkNBQ09PLGlDQUFzQkksV0FEN0I7QUFFSWpGLFFBQUFBLGlCQUFpQixFQUFqQkEsaUJBRko7QUFHSU0sUUFBQUEsS0FBSyxrQ0FDRXVFLGlDQUFzQkksV0FBdEIsQ0FBa0MzRSxLQURwQztBQUVEMEUsVUFBQUEsZUFBZSxFQUFFVjtBQUZoQjtBQUhUOztBQVFKLFNBQUssTUFBTDtBQUNJLGFBQU9PLGlDQUFzQkssSUFBN0I7O0FBQ0osU0FBSyxXQUFMO0FBQ0ksYUFBT0wsaUNBQXNCTSxTQUE3Qjs7QUFDSixTQUFLLFdBQUw7QUFDSSxhQUFPTixpQ0FBc0JPLFNBQTdCOztBQUNKO0FBQUEsaURBQzZDYixZQUQ3QztBQUFBOztBQUFBO0FBQ0ksNERBQXVEO0FBQUE7QUFBQSxjQUExQ2xELEVBQTBDLGVBQTFDQSxFQUEwQztBQUFBLGNBQXRDZ0UsWUFBc0MsZUFBdENBLFlBQXNDO0FBQUEsY0FBeEJ0RixJQUF3QixlQUF4QkEsSUFBd0I7O0FBQ25ELGNBQUlULElBQUksS0FBSytCLEVBQWIsRUFBaUI7QUFDYixnQkFDSUQsUUFESixHQU1JckIsSUFOSixDQUNJcUIsUUFESjtBQUFBLGdCQUVJbkIsTUFGSixHQU1JRixJQU5KLENBRUlFLE1BRko7QUFBQSxnQkFHSUcsbUJBSEosR0FNSUwsSUFOSixDQUdJSyxtQkFISjtBQUFBLHdDQU1JTCxJQU5KLENBSUlRLGNBSko7QUFBQSxnQkFJSUEsY0FKSixzQ0FJcUIsQ0FKckI7QUFBQSxnQkFLSUMsS0FMSixHQU1JVCxJQU5KLENBS0lTLEtBTEo7QUFPQSxnQkFBTThFLG1CQUFtQixHQUFHLHdCQUFVRCxZQUFWLENBQTVCO0FBQ0EsZ0JBQU1FLFNBQVMsR0FDWGhGLGNBQWMsS0FBSyxDQUFuQixJQUF3QixDQUFDYSxRQUF6QixHQUNNWixLQUFLLEdBQUdpQixvQkFEZCxHQUVNakIsS0FIVjtBQUlBLGdCQUFNZ0YsVUFBVSxHQUNaakYsY0FBYyxLQUFLLENBQW5CLElBQXdCLENBQUNhLFFBQXpCLEdBQ01uQixNQUFNLEdBQUdzQixxQkFEZixHQUVNdEIsTUFIVjtBQUlBLG1CQUFPO0FBQ0hELGNBQUFBLGlCQUFpQixFQUFqQkEsaUJBREc7QUFFSEMsY0FBQUEsTUFBTSxFQUFFdUYsVUFBVSxHQUFHWixNQUFiLEdBQXNCQSxNQUF0QixHQUErQlksVUFGcEM7QUFHSHBGLGNBQUFBLG1CQUFtQixFQUFFQSxtQkFIbEI7QUFJSEMsY0FBQUEsS0FBSyxFQUFFLEVBSko7QUFLSEMsY0FBQUEsS0FBSyxrQ0FDRWdGLG1CQURGO0FBRURHLGdCQUFBQSxNQUFNLEVBQUUsa0NBQWtCSCxtQkFBbEIsQ0FGUDtBQUdETixnQkFBQUEsZUFBZSxFQUFFVjtBQUhoQixnQkFMRjtBQVVIOUQsY0FBQUEsS0FBSyxFQUFFK0UsU0FBUyxHQUFHZCxNQUFaLEdBQXFCQSxNQUFyQixHQUE4QmM7QUFWbEMsYUFBUDtBQVlIO0FBQ0o7QUFoQ0w7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQSxrREFrQzZDZixZQWxDN0M7QUFBQTs7QUFBQTtBQWtDSSwrREFBdUQ7QUFBQTtBQUFBLGNBQTFDbkQsR0FBMEMsZ0JBQTFDQSxFQUEwQztBQUFBLGNBQXRDZ0UsYUFBc0MsZ0JBQXRDQSxZQUFzQztBQUFBLGNBQXhCdEYsS0FBd0IsZ0JBQXhCQSxJQUF3Qjs7QUFDbkQsY0FBSVQsSUFBSSxLQUFLK0IsR0FBYixFQUFpQjtBQUNiLGdCQUNJRCxTQURKLEdBTUlyQixLQU5KLENBQ0lxQixRQURKO0FBQUEsZ0JBRUluQixPQUZKLEdBTUlGLEtBTkosQ0FFSUUsTUFGSjtBQUFBLGdCQUdJRyxvQkFISixHQU1JTCxLQU5KLENBR0lLLG1CQUhKO0FBQUEsd0NBTUlMLEtBTkosQ0FJSVEsY0FKSjtBQUFBLGdCQUlJQSxlQUpKLHNDQUlxQixDQUpyQjtBQUFBLGdCQUtJQyxNQUxKLEdBTUlULEtBTkosQ0FLSVMsS0FMSjs7QUFPQSxnQkFBTStFLFVBQVMsR0FDWGhGLGVBQWMsS0FBSyxDQUFuQixJQUF3QixDQUFDYSxTQUF6QixHQUNNWixNQUFLLEdBQUdpQixvQkFEZCxHQUVNakIsTUFIVjs7QUFJQSxnQkFBTWdGLFdBQVUsR0FDWmpGLGVBQWMsS0FBSyxDQUFuQixJQUF3QixDQUFDYSxTQUF6QixHQUNNbkIsT0FBTSxHQUFHc0IscUJBRGYsR0FFTXRCLE9BSFY7O0FBSUEsbUJBQU87QUFDSEQsY0FBQUEsaUJBQWlCLEVBQWpCQSxpQkFERztBQUVIQyxjQUFBQSxNQUFNLEVBQUV1RixXQUFVLEdBQUdaLE1BQWIsR0FBc0JBLE1BQXRCLEdBQStCWSxXQUZwQztBQUdIcEYsY0FBQUEsbUJBQW1CLEVBQUVBLG9CQUhsQjtBQUlIQyxjQUFBQSxLQUFLLEVBQUUsa0JBQUlxRix1QkFBSixZQUFxQi9CLEtBQXJCLGtCQUF5QyxFQUF6QyxDQUpKO0FBS0hyRCxjQUFBQSxLQUFLLEVBQUUsd0RBQ0ErRSxhQURBO0FBRUhMLGdCQUFBQSxlQUFlLEVBQUVWO0FBRmQsaUJBTEo7QUFTSDlELGNBQUFBLEtBQUssRUFBRStFLFVBQVMsR0FBR2QsTUFBWixHQUFxQkEsTUFBckIsR0FBOEJjO0FBVGxDLGFBQVA7QUFXSDtBQUNKO0FBL0RMO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBZ0VJLGFBQU8sRUFBUDtBQTdGUjtBQStGSCxDQTFHTTs7OztBQTRHQSxJQUFNSSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLFFBQUQsRUFBYztBQUMzQyxNQUFJQSxRQUFRLENBQUNDLE1BQWIsRUFBcUI7QUFDakIsUUFBTUMsT0FBTyxHQUFHRixRQUFRLENBQUNHLElBQVQsQ0FBYyxVQUFDMUcsSUFBRDtBQUFBLGFBQVVBLElBQUksQ0FBQ2MsS0FBTCxLQUFlLENBQXpCO0FBQUEsS0FBZCxDQUFoQjs7QUFDQSxRQUFJMkYsT0FBSixFQUFhO0FBQ1QsYUFBT0EsT0FBTyxDQUFDekUsRUFBZjtBQUNIO0FBQ0o7O0FBQ0QsU0FBTyxFQUFQO0FBQ0gsQ0FSTTs7OztBQVVBLElBQU0yRSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDdkMsV0FBRCxFQUFpQjtBQUN0QyxNQUFJQSxXQUFXLEtBQUssV0FBcEIsRUFBaUM7QUFDN0IsV0FBTztBQUNIaEQsTUFBQUEsQ0FBQyxFQUFFa0UscUJBREE7QUFFSGpFLE1BQUFBLENBQUMsRUFBRWdFO0FBRkEsS0FBUDtBQUlIOztBQUNELFNBQU87QUFDSGpFLElBQUFBLENBQUMsRUFBRWlFLG9CQURBO0FBRUhoRSxJQUFBQSxDQUFDLEVBQUVpRTtBQUZBLEdBQVA7QUFJSCxDQVhNOzs7O0FBYVAsSUFBTVgsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0QsVUFBRCxFQUFhTixXQUFiLEVBQTBCakQsS0FBMUIsRUFBb0M7QUFDbkQsTUFBTXlGLE1BQU0sR0FDUixDQUFDLENBQUN4QyxXQUFXLEtBQUssVUFBaEIsR0FBNkJpQixvQkFBN0IsR0FBMENDLHFCQUEzQyxJQUEwRG5FLEtBQTNELElBQW9FLENBRHhFOztBQUVBLE1BQUl1RCxVQUFKLEVBQWdCO0FBQ1osV0FBT2tDLE1BQU0sR0FBSUEsTUFBTSxHQUFHeEUsb0JBQTFCO0FBQ0g7O0FBQ0QsU0FBT3dFLE1BQVA7QUFDSCxDQVBEOztBQVNBLElBQU1oQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDRixVQUFELEVBQWFOLFdBQWIsRUFBMEJ4RCxNQUExQixFQUFxQztBQUNwRCxNQUFNZ0csTUFBTSxHQUNSLENBQUMsQ0FBQ3hDLFdBQVcsS0FBSyxVQUFoQixHQUE2QmtCLHFCQUE3QixHQUEyQ0Qsb0JBQTVDLElBQTBEekUsTUFBM0QsSUFBcUUsQ0FEekU7O0FBRUEsTUFBSThELFVBQUosRUFBZ0I7QUFDWixXQUFPa0MsTUFBTSxHQUFJQSxNQUFNLEdBQUcxRSxxQkFBMUI7QUFDSDs7QUFDRCxTQUFPMEUsTUFBUDtBQUNILENBUEQ7O0FBU08sSUFBTUMsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFDQyxZQUFELEVBQWVDLFFBQWYsRUFBeUJDLFFBQXpCLEVBQXNDO0FBQ2xFLFVBQVFELFFBQVI7QUFDSSxTQUFLRSxtQkFBTDtBQUNJLFVBQUlILFlBQVksS0FBSyxDQUFyQixFQUF3QjtBQUNwQixlQUFPLElBQVA7QUFDSDs7QUFDRCxhQUFPLENBQVA7O0FBQ0osU0FBS0ksc0JBQUw7QUFDSSxVQUFJSixZQUFZLEtBQUtFLFFBQVEsR0FBRyxDQUFoQyxFQUFtQztBQUMvQixlQUFPLElBQVA7QUFDSDs7QUFDRCxhQUFPQSxRQUFRLEdBQUcsQ0FBbEI7O0FBQ0osU0FBS0csd0JBQUw7QUFDSSxVQUFJTCxZQUFZLEtBQUssQ0FBckIsRUFBd0I7QUFDcEIsZUFBTyxJQUFQO0FBQ0g7O0FBQ0QsYUFBT0EsWUFBWSxHQUFHLENBQXRCOztBQUNKLFNBQUtNLDJCQUFMO0FBQ0ksVUFBSU4sWUFBWSxLQUFLRSxRQUFRLEdBQUcsQ0FBaEMsRUFBbUM7QUFDL0IsZUFBTyxJQUFQO0FBQ0g7O0FBQ0QsYUFBT0YsWUFBWSxHQUFHLENBQXRCOztBQUNKO0FBQ0ksYUFBTyxJQUFQO0FBdEJSO0FBd0JILENBekJNOzs7O0FBMkJBLElBQU1PLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUNwSCxJQUFELEVBQU9xSCxLQUFQLEVBQWlCO0FBQ25DLE1BQU1DLFFBQVEsR0FBRyxxQkFDYkQsS0FBSyxDQUFDbkUsTUFBTixDQUFhLFVBQUNuRCxJQUFEO0FBQUEsV0FBVUEsSUFBSSxDQUFDc0IsSUFBTCxLQUFjckIsSUFBeEI7QUFBQSxHQUFiLENBRGEsRUFFYixDQUNJLFVBQUM2QyxDQUFELEVBQU87QUFDSCxXQUFPTyxRQUFRLENBQUNQLENBQUMsQ0FBQ2pDLEdBQUgsQ0FBZjtBQUNILEdBSEwsQ0FGYSxDQUFqQjs7QUFRQSxNQUFJMEcsUUFBUSxDQUFDZixNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLFdBQU8sQ0FBUDtBQUNIOztBQUNELFNBQU9uRCxRQUFRLENBQUNrRSxRQUFRLENBQUNBLFFBQVEsQ0FBQ2YsTUFBVCxHQUFrQixDQUFuQixDQUFSLENBQThCM0YsR0FBL0IsQ0FBUixHQUE4QyxDQUFyRDtBQUNILENBYk07Ozs7QUFlQSxJQUFNUiwwQkFBMEIsR0FBRyxTQUE3QkEsMEJBQTZCLENBQUNELElBQUQsRUFBVTtBQUNoRCxNQUFJQSxJQUFJLENBQUM0QixFQUFMLEtBQVksRUFBaEIsRUFBb0I7QUFDaEIsV0FBTzVCLElBQVA7QUFDSDs7QUFDRCxNQUFJb0gsVUFBVSxHQUFHLEVBQWpCO0FBQ0EsTUFBSUMsV0FBVyxHQUFHLEVBQWxCO0FBQ0EsTUFBSXBELFdBQVcsR0FBRyxFQUFsQjtBQUNBakUsRUFBQUEsSUFBSSxDQUFDeUIsT0FBTCxDQUFhQyxPQUFiLENBQXFCLFVBQUN2QixNQUFELEVBQVk7QUFDN0IsZ0NBSUlELDRCQUE0QixDQUFDQyxNQUFELENBSmhDO0FBQUEsUUFDSWdCLGVBREoseUJBQ0lBLGVBREo7QUFBQSxRQUVJTixLQUZKLHlCQUVJQSxLQUZKO0FBQUEsUUFHWVQsVUFIWix5QkFHSUQsTUFISjs7QUFLQWlILElBQUFBLFVBQVUsbUNBQVFBLFVBQVIsR0FBdUJ2RyxLQUF2QixDQUFWO0FBQ0F3RyxJQUFBQSxXQUFXLGdDQUFPQSxXQUFQLElBQW9CakgsVUFBcEIsRUFBWDs7QUFDQSxRQUFJZSxlQUFKLEVBQXFCO0FBQ2pCOEMsTUFBQUEsV0FBVyxnQ0FBT0EsV0FBUCxJQUFvQjlDLGVBQXBCLEVBQVg7QUFDSDtBQUNKLEdBWEQ7QUFhQSxNQUFNZ0MsT0FBTyxHQUFHTyxJQUFJLENBQUM0RCxTQUFMLHFCQUNYdEgsSUFBSSxDQUFDOEQsYUFETSxFQUNVO0FBQ2xCSSxJQUFBQSxLQUFLLEVBQUVsRSxJQUFJLENBQUNrRSxLQURNO0FBRWxCRixJQUFBQSxXQUFXLEVBQUVoRSxJQUFJLENBQUNnRSxXQUZBO0FBR2xCRCxJQUFBQSxZQUFZLEVBQUUvRCxJQUFJLENBQUMrRCxZQUhEO0FBSWxCRSxJQUFBQSxXQUFXLEVBQUVBLFdBSks7QUFLbEIxQyxJQUFBQSxNQUFNLEVBQUU2RixVQUxVO0FBTWxCM0YsSUFBQUEsT0FBTyxFQUFFNEY7QUFOUyxHQURWLEVBQWhCO0FBVUEsU0FBTztBQUNIOUUsSUFBQUEsT0FBTyxFQUFFdkMsSUFBSSxDQUFDdUMsT0FEWDtBQUVIZ0IsSUFBQUEsT0FBTyxFQUFFdkQsSUFBSSxDQUFDdUQsT0FGWDtBQUdISixJQUFBQSxPQUFPLEVBQVBBLE9BSEc7QUFJSEUsSUFBQUEsZUFBZSxFQUFFckQsSUFBSSxDQUFDcUQsZUFKbkI7QUFLSEwsSUFBQUEsVUFBVSxFQUFFaEQsSUFBSSxDQUFDZ0QsVUFMZDtBQU1IcEIsSUFBQUEsRUFBRSxFQUFFNUIsSUFBSSxDQUFDNEIsRUFOTjtBQU9INUIsSUFBQUEsSUFBSSxFQUFFQSxJQUFJLENBQUNBLElBUFI7QUFRSHNELElBQUFBLFdBQVcsRUFBRXRELElBQUksQ0FBQ3NEO0FBUmYsR0FBUDtBQVVILENBeENNOzs7O0FBMENBLElBQU1pRSxzQkFBc0IsR0FBRyxTQUF6QkEsc0JBQXlCLENBQUNDLFlBQUQsRUFBa0I7QUFDcEQsTUFBTUMsb0JBQW9CLEdBQUc7QUFDekJsRSxJQUFBQSxPQUFPLEVBQUVpRSxZQUFZLENBQUNqRSxPQURHO0FBRXpCSCxJQUFBQSxVQUFVLEVBQUVvRSxZQUFZLENBQUNwRSxVQUZBO0FBR3pCc0UsSUFBQUEsV0FBVyxFQUFFRixZQUFZLENBQUNFLFdBSEQ7QUFJekJDLElBQUFBLE1BQU0sRUFBRUgsWUFBWSxDQUFDRyxNQUpJO0FBS3pCL0YsSUFBQUEsRUFBRSxFQUFFNEYsWUFBWSxDQUFDNUYsRUFMUTtBQU16QmdHLElBQUFBLElBQUksRUFBRUosWUFBWSxDQUFDSSxJQU5NO0FBT3pCQyxJQUFBQSxNQUFNLEVBQUVMLFlBQVksQ0FBQ0ssTUFQSTtBQVF6QkMsSUFBQUEsYUFBYSxFQUFFTixZQUFZLENBQUNNLGFBUkg7QUFTekJDLElBQUFBLGVBQWUsRUFBRVAsWUFBWSxDQUFDTyxlQVRMO0FBVXpCQyxJQUFBQSxLQUFLLEVBQUVSLFlBQVksQ0FBQ1EsS0FWSztBQVd6QkMsSUFBQUEsVUFBVSxFQUFFVCxZQUFZLENBQUNTLFVBWEE7QUFZekJ6RSxJQUFBQSxVQUFVLEVBQUVnRSxZQUFZLENBQUNoRSxVQVpBO0FBYXpCMEUsSUFBQUEsT0FBTyxFQUFFVixZQUFZLENBQUNVLE9BYkc7QUFjekJDLElBQUFBLGNBQWMsRUFBRTNJLGdCQUFnQixDQUFDZ0ksWUFBWSxDQUFDL0gsS0FBZDtBQWRQLEdBQTdCO0FBZ0JBLFNBQU9nSSxvQkFBUDtBQUNILENBbEJNOzs7O0FBb0JBLElBQU1XLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLFNBQUQsRUFBZTtBQUNyQyxNQUFJQyxTQUFTLEdBQUcsQ0FBaEI7QUFDQSxTQUFPRCxTQUFTLENBQUN0SSxHQUFWLENBQWMsVUFBQ0gsSUFBRCxFQUFVO0FBQzNCLFFBQUlBLElBQUksQ0FBQ0MsSUFBTCxLQUFjLE9BQWxCLEVBQTJCO0FBQ3ZCLFVBQU0yQyxTQUFTLGFBQU04RixTQUFTLEdBQUcsQ0FBbEIsQ0FBZjtBQUNBLFVBQU1DLFFBQVEsR0FBRzNJLElBQUksQ0FBQ0gsS0FBTCxDQUFXTSxHQUFYLENBQWUsVUFBQ3lJLE9BQUQsRUFBYTtBQUN6QyxVQUFFRixTQUFGO0FBQ0EsK0NBQ09FLE9BRFA7QUFFSXhJLFVBQUFBLElBQUksWUFBS3NJLFNBQUw7QUFGUjtBQUlILE9BTmdCLENBQWpCO0FBT0EsNkNBQVkxSSxJQUFaO0FBQWtCSSxRQUFBQSxJQUFJLEVBQUV3QyxTQUF4QjtBQUFtQy9DLFFBQUFBLEtBQUssRUFBRThJO0FBQTFDO0FBQ0g7O0FBQ0QsTUFBRUQsU0FBRjtBQUNBLDJDQUFZMUksSUFBWjtBQUFrQkksTUFBQUEsSUFBSSxZQUFLc0ksU0FBTDtBQUF0QjtBQUNILEdBZE0sQ0FBUDtBQWVILENBakJNIiwic291cmNlc0NvbnRlbnQiOlsiLyogZXNsaW50LWRpc2FibGUgbm8tbWlzbGVhZGluZy1jaGFyYWN0ZXItY2xhc3MgKi9cclxuLyogZXNsaW50LWRpc2FibGUgbm8tY29udHJvbC1yZWdleCAqL1xyXG5pbXBvcnQgY2xvbmVEZWVwIGZyb20gJ2xvZGFzaC9jbG9uZURlZXAnO1xyXG5pbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xyXG5pbXBvcnQgaXNOaWwgZnJvbSAnbG9kYXNoL2lzTmlsJztcclxuaW1wb3J0IHNvcnRCeSBmcm9tICdsb2Rhc2gvc29ydEJ5JztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBHUklEX1dJRFRILFxyXG4gICAgR1JJRF9IRUlHSFQsXHJcbiAgICBNT1ZFX0JBQ0ssXHJcbiAgICBNT1ZFX0ZPUldBUkQsXHJcbiAgICBNT1ZFX1NURVBfQkFDSyxcclxuICAgIE1PVkVfU1RFUF9GT1JXQVJELFxyXG4gICAgUEFHRV9IRUlHSFQsXHJcbiAgICBQQUdFX1dJRFRILFxyXG4gICAgV0lER0VUX1NVUFBPUlRfVkVSU0lPTixcclxufSBmcm9tICdAY29uc3RhbnRzL3RlbXBsYXRlJztcclxuXHJcbmltcG9ydCB7IGJ1aWxkR3JhcGhzQ29sb3JzIH0gZnJvbSAnQGhlbHBlcnMvdGVtcGxhdGUnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIERFRkFVTFRfV0lER0VUX1ZBTFVFUyxcclxuICAgIC8vIE5FV19QQUdFX0NPTlRFTlQsXHJcbiAgICBUQUJMRV9USEVNRVMsXHJcbn0gZnJvbSAnLi9jb25zdGFudHMnO1xyXG5cclxuY29uc3QgZ2V0UGFnZXNUZW1wbGF0ZSA9IChwYWdlcykgPT4ge1xyXG4gICAgcmV0dXJuIHBhZ2VzLnJlZHVjZSgoYWNjLCBpdGVtKSA9PiB7XHJcbiAgICAgICAgaWYgKGl0ZW0udHlwZSA9PT0gJ2dyb3VwJykge1xyXG4gICAgICAgICAgICBjb25zdCB0ZW1wR3JvdXBQYWdlcyA9IGl0ZW0ucGFnZXMubWFwKChwYWdlKSA9PlxyXG4gICAgICAgICAgICAgICAgZ2V0UGFnZVVwZGF0aW5nQ29udGVudERhdGEocGFnZSlcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgcmV0dXJuIFsuLi5hY2MsIC4uLnRlbXBHcm91cFBhZ2VzXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIFsuLi5hY2MsIGdldFBhZ2VVcGRhdGluZ0NvbnRlbnREYXRhKGl0ZW0pXTtcclxuICAgIH0sIFtdKTtcclxufTtcclxuXHJcbmNvbnN0IGdldFdpZGdldFVwZGF0aW5nQ29udGVudERhdGEgPSAod2lkZ2V0KSA9PiB7XHJcbiAgICBjb25zdCB3aWRnZXREYXRhID0ge1xyXG4gICAgICAgIGZpZWxkOiB3aWRnZXQuZmllbGQsXHJcbiAgICAgICAgbWV0YToge1xyXG4gICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydDogd2lkZ2V0Lmhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICBoZWlnaHQ6IHdpZGdldC5oZWlnaHQsXHJcbiAgICAgICAgICAgIG51bTogd2lkZ2V0Lm51bSxcclxuICAgICAgICAgICAgb3JkZXI6IHdpZGdldC5vcmRlcixcclxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZDogd2lkZ2V0LnJlcGxhY2VJbmZvUmVxdWlyZWQsXHJcbiAgICAgICAgICAgIHN0eWxlOiB3aWRnZXQuc3R5bGUsXHJcbiAgICAgICAgICAgIHZhbHVlOiB3aWRnZXQudmFsdWUsXHJcbiAgICAgICAgICAgIHN1cHBvcnRWZXJzaW9uOiB3aWRnZXQuc3VwcG9ydFZlcnNpb24sXHJcbiAgICAgICAgICAgIHdpZHRoOiB3aWRnZXQud2lkdGgsXHJcbiAgICAgICAgICAgIHg6IHdpZGdldC54LFxyXG4gICAgICAgICAgICB5OiB3aWRnZXQueSxcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5hbWU6IHdpZGdldC5uYW1lLFxyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHdpZGdldDogd2lkZ2V0RGF0YSxcclxuICAgICAgICB2YWx1ZTogeyBbd2lkZ2V0LmZpZWxkXTogd2lkZ2V0LnZhbHVlIH0sXHJcbiAgICAgICAgcmVwbGFjZUluZm9EYXRhOiB3aWRnZXQucmVwbGFjZUluZm9SZXF1aXJlZFxyXG4gICAgICAgICAgICA/IHtcclxuICAgICAgICAgICAgICAgICAgZmllbGQ6IHdpZGdldC5maWVsZCxcclxuICAgICAgICAgICAgICAgICAgcmVwbGFjZUJ5OiB3aWRnZXQucmVwbGFjZUluZm9SZXF1aXJlZCxcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIDogbnVsbCxcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgYnVpbGRUZW1wbGF0ZVdpZGdldHMgPSAoe1xyXG4gICAgcGFnZUlkLFxyXG4gICAgdmFsdWVzLFxyXG4gICAgd2lkZ2V0TW9kZWwsXHJcbiAgICB3aWRnZXRzLFxyXG59KSA9PiB7XHJcbiAgICB3aWRnZXRzLmZvckVhY2goKHdpZGdldCkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHsgbmFtZSwgZmllbGQsIG1ldGEgfSA9IHdpZGdldDtcclxuICAgICAgICBjb25zdCB7XHJcbiAgICAgICAgICAgIGZyZWVHcmlkLFxyXG4gICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICAgICAgaGVpZ2h0LFxyXG4gICAgICAgICAgICBpZCxcclxuICAgICAgICAgICAgbnVtLFxyXG4gICAgICAgICAgICBvcmRlcixcclxuICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcclxuICAgICAgICAgICAgc3R5bGUgPSB7fSxcclxuICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24gPSAxLFxyXG4gICAgICAgICAgICB3aWR0aCxcclxuICAgICAgICAgICAgeCxcclxuICAgICAgICAgICAgeSxcclxuICAgICAgICB9ID0gbWV0YTtcclxuICAgICAgICB3aWRnZXRNb2RlbC5jcmVhdGUoe1xyXG4gICAgICAgICAgICBpZDogaWQgfHwgYCR7cGFnZUlkfS8ke2ZpZWxkfWAsXHJcbiAgICAgICAgICAgIGZpZWxkLFxyXG4gICAgICAgICAgICBoYXNTdW1tYXJ5U3VwcG9ydCxcclxuICAgICAgICAgICAgaGVpZ2h0OlxyXG4gICAgICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24gPT09IDEgJiYgIWZyZWVHcmlkXHJcbiAgICAgICAgICAgICAgICAgICAgPyBoZWlnaHQgKiBHUklEX0hFSUdIVFxyXG4gICAgICAgICAgICAgICAgICAgIDogaGVpZ2h0LFxyXG4gICAgICAgICAgICBuYW1lLFxyXG4gICAgICAgICAgICBudW0sXHJcbiAgICAgICAgICAgIG9yZGVyLFxyXG4gICAgICAgICAgICBwYWdlSWQsXHJcbiAgICAgICAgICAgIHJlcGxhY2VJbmZvUmVxdWlyZWQsXHJcbiAgICAgICAgICAgIHN0eWxlLFxyXG4gICAgICAgICAgICBzdXBwb3J0VmVyc2lvbjogV0lER0VUX1NVUFBPUlRfVkVSU0lPTixcclxuICAgICAgICAgICAgdmFsdWU6IHZhbHVlc1tmaWVsZF0sXHJcbiAgICAgICAgICAgIHdpZHRoOlxyXG4gICAgICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24gPT09IDEgJiYgIWZyZWVHcmlkID8gd2lkdGggKiBHUklEX1dJRFRIIDogd2lkdGgsXHJcbiAgICAgICAgICAgIHg6IHN1cHBvcnRWZXJzaW9uID09PSAxICYmICFmcmVlR3JpZCA/IHggKiBHUklEX1dJRFRIIDogeCxcclxuICAgICAgICAgICAgeTogc3VwcG9ydFZlcnNpb24gPT09IDEgJiYgIWZyZWVHcmlkID8geSAqIEdSSURfSEVJR0hUIDogeSxcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNyZWF0ZUdyb3VwID0gKHtcclxuICAgIGdyb3VwLFxyXG4gICAgZ3JvdXBNb2RlbCxcclxuICAgIHBhZ2VNb2RlbCxcclxuICAgIHRlbXBsYXRlSWQsXHJcbiAgICB3aWRnZXRNb2RlbCxcclxufSkgPT4ge1xyXG4gICAgY29uc3QgeyBibG9ja2VkOiBncm91cEJsb2NrZWQsIGZpcnN0UGFnZSwgaWQ6IGdyb3VwSWQsIHBhZ2VzID0gW10gfSA9IGdyb3VwO1xyXG4gICAgZ3JvdXBNb2RlbC5jcmVhdGUoe1xyXG4gICAgICAgIGJsb2NrZWQ6IGdyb3VwQmxvY2tlZCxcclxuICAgICAgICBpZDogZ3JvdXBJZCxcclxuICAgICAgICBwYWdlOiBmaXJzdFBhZ2UsXHJcbiAgICAgICAgdGVtcGxhdGVJZCxcclxuICAgIH0pO1xyXG5cclxuICAgIHBhZ2VzLmZvckVhY2goKHApID0+XHJcbiAgICAgICAgY3JlYXRlUGFnZSh7XHJcbiAgICAgICAgICAgIHBhZ2VNb2RlbDogcGFnZU1vZGVsLFxyXG4gICAgICAgICAgICB0ZW1wUGFnZTogcCxcclxuICAgICAgICAgICAgZ3JvdXBJZDogZ3JvdXBJZCxcclxuICAgICAgICAgICAgd2lkZ2V0TW9kZWw6IHdpZGdldE1vZGVsLFxyXG4gICAgICAgIH0pXHJcbiAgICApO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNyZWF0ZUdyb3VwcyA9IChwYWdlc1RlbXBsYXRlKSA9PiB7XHJcbiAgICByZXR1cm4gc29ydEJ5KFxyXG4gICAgICAgIHBhZ2VzVGVtcGxhdGUuZmlsdGVyKChwKSA9PiAhaXNOaWwocC5ncm91cGVkX2J5KSksXHJcbiAgICAgICAgW1xyXG4gICAgICAgICAgICAocCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlSW50KHAucGFnZSk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgXVxyXG4gICAgKS5yZWR1Y2UoKGFjYywgY3VycmVudCkgPT4ge1xyXG4gICAgICAgIGlmIChhY2NbY3VycmVudC5ncm91cGVkX2J5XSkge1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgLi4uYWNjLFxyXG4gICAgICAgICAgICAgICAgW2N1cnJlbnQuZ3JvdXBlZF9ieV06IHtcclxuICAgICAgICAgICAgICAgICAgICAuLi5hY2NbY3VycmVudC5ncm91cGVkX2J5XSxcclxuICAgICAgICAgICAgICAgICAgICBwYWdlczogWy4uLmFjY1tjdXJyZW50Lmdyb3VwZWRfYnldLnBhZ2VzLCBjdXJyZW50XSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLmFjYyxcclxuICAgICAgICAgICAgICAgIFtjdXJyZW50Lmdyb3VwZWRfYnldOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYmxvY2tlZDogY3VycmVudC5ibG9ja2VkLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpcnN0UGFnZTogY3VycmVudC5wYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBjdXJyZW50Lmdyb3VwZWRfYnksXHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZXM6IFtjdXJyZW50XSxcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgfSwge30pO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNyZWF0ZVBhZ2UgPSAoe1xyXG4gICAgZ3JvdXBJZCxcclxuICAgIHBhZ2VNb2RlbCxcclxuICAgIHRlbXBQYWdlLFxyXG4gICAgdGVtcGxhdGVJZCxcclxuICAgIHdpZGdldE1vZGVsLFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCB7XHJcbiAgICAgICAgYmxvY2tlZCA9IDAsXHJcbiAgICAgICAgY29udGVudCxcclxuICAgICAgICBjcmVhdGVkX2F0LFxyXG4gICAgICAgIGN1c3RvbV90ZW1wbGF0ZSxcclxuICAgICAgICBncm91cGVkX2J5LFxyXG4gICAgICAgIGlkLFxyXG4gICAgICAgIHBhZ2UsXHJcbiAgICAgICAgcGFnZV9wYXJlbnQsXHJcbiAgICAgICAgY29tcGFueSxcclxuICAgICAgICB1cGRhdGVkX2F0LFxyXG4gICAgfSA9IHRlbXBQYWdlO1xyXG4gICAgY29uc3QgcGFyc2VkRGF0YSA9IEpTT04ucGFyc2UoY29udGVudCk7XHJcbiAgICBjb25zdCBbcGFyc2VkRGF0YUtleSA9IHt9XSA9IE9iamVjdC5rZXlzKHBhcnNlZERhdGEpO1xyXG4gICAgY29uc3Qge1xyXG4gICAgICAgIGluZmluaXRlTW9kZSA9IGZhbHNlLFxyXG4gICAgICAgIG9yaWVudGF0aW9uID0gJ3BvcnRyYWl0JyxcclxuICAgICAgICByZXBsYWNlSW5mbyxcclxuICAgICAgICB0aGVtZSA9ICdkZWZhdWx0VGhlbWUnLFxyXG4gICAgICAgIHZhbHVlcyxcclxuICAgICAgICB3aWRnZXRzLFxyXG4gICAgfSA9IHBhcnNlZERhdGFbcGFyc2VkRGF0YUtleV07XHJcblxyXG4gICAgbGV0IHRlbXBGaWVsZHMgPSB7XHJcbiAgICAgICAgYmxvY2tlZCxcclxuICAgICAgICBjb250ZW50LFxyXG4gICAgICAgIGNyZWF0ZWRfYXQsXHJcbiAgICAgICAgY3VzdG9tX3RlbXBsYXRlLFxyXG4gICAgICAgIGdyb3VwZWRfYnksXHJcbiAgICAgICAgaWQsXHJcbiAgICAgICAgaW5maW5pdGVNb2RlLFxyXG4gICAgICAgIG9yaWVudGF0aW9uLFxyXG4gICAgICAgIHBhZ2UsXHJcbiAgICAgICAgcGFnZV9wYXJlbnQsXHJcbiAgICAgICAgcGFyc2VkRGF0YUtleSxcclxuICAgICAgICByZXBsYWNlSW5mbyxcclxuICAgICAgICB0ZW1wbGF0ZUlkLFxyXG4gICAgICAgIHRoZW1lLFxyXG4gICAgICAgIGNvbXBhbnksXHJcbiAgICAgICAgdXBkYXRlZF9hdCxcclxuICAgIH07XHJcblxyXG4gICAgaWYgKGdyb3VwSWQpIHtcclxuICAgICAgICB0ZW1wRmllbGRzLmdyb3VwSWQgPSBncm91cElkO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0ZW1wbGF0ZUlkKSB7XHJcbiAgICAgICAgdGVtcEZpZWxkcy50ZW1wbGF0ZUlkID0gdGVtcGxhdGVJZDtcclxuICAgIH1cclxuXHJcbiAgICBwYWdlTW9kZWwuY3JlYXRlKHRlbXBGaWVsZHMpO1xyXG5cclxuICAgIGJ1aWxkVGVtcGxhdGVXaWRnZXRzKHtcclxuICAgICAgICBwYWdlSWQ6IGlkLFxyXG4gICAgICAgIHZhbHVlcyxcclxuICAgICAgICB3aWRnZXRNb2RlbCxcclxuICAgICAgICB3aWRnZXRzLFxyXG4gICAgfSk7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0RGVmYXVsdFBvc2l0aW9uID0gKFxyXG4gICAgaGVpZ2h0LFxyXG4gICAgd2lkdGgsXHJcbiAgICBkZWZhdWx0UG9zaXRpb24sXHJcbiAgICBvcmllbnRhdGlvbixcclxuICAgIHNob3dHdWlkZXNcclxuKSA9PiB7XHJcbiAgICBpZiAoZGVmYXVsdFBvc2l0aW9uID09PSBudWxsKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgeDogZ2V0TWlkZGxlWChzaG93R3VpZGVzLCBvcmllbnRhdGlvbiwgd2lkdGgpLFxyXG4gICAgICAgICAgICB5OiBnZXRNaWRkbGVZKHNob3dHdWlkZXMsIG9yaWVudGF0aW9uLCBoZWlnaHQpLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHg6IGRlZmF1bHRQb3NpdGlvbi54LFxyXG4gICAgICAgIHk6IGRlZmF1bHRQb3NpdGlvbi55LFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXREZWZhdWx0V2lkZ2V0UG9zaXRpb24gPSAoXHJcbiAgICBkZWZhdWx0VmFsdWVzLFxyXG4gICAgZGVmYXVsdFBvc2l0aW9uLFxyXG4gICAgb3JpZW50YXRpb25cclxuKSA9PiB7XHJcbiAgICBjb25zdCB7IGhlaWdodCwgd2lkdGggfSA9IGRlZmF1bHRWYWx1ZXM7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIC4uLmRlZmF1bHRWYWx1ZXMsXHJcbiAgICAgICAgLi4uZ2V0RGVmYXVsdFBvc2l0aW9uKGhlaWdodCwgd2lkdGgsIGRlZmF1bHRQb3NpdGlvbiwgb3JpZW50YXRpb24pLFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXREZWZhdWx0V2lkZ2V0VmFsdWVzID0gKFxyXG4gICAgaGFzU3VtbWFyeVN1cHBvcnQsXHJcbiAgICBvbmxpbmVXaWRnZXRzLFxyXG4gICAgb3JpZW50YXRpb24sXHJcbiAgICBwcm9wb3NhbE51bWJlcixcclxuICAgIHRoZW1lLFxyXG4gICAgdHlwZVxyXG4pID0+IHtcclxuICAgIGNvbnN0IHsgY2hhcnRXaWRnZXRzLCB0YWJsZVdpZGdldHMgfSA9IG9ubGluZVdpZGdldHM7XHJcbiAgICBjb25zdCBsaW1pdFggPSBvcmllbnRhdGlvbiA9PT0gJ3BvcnRyYWl0JyA/IFBBR0VfV0lEVEggOiBQQUdFX0hFSUdIVDtcclxuICAgIGNvbnN0IGxpbWl0WSA9IG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIO1xyXG4gICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgY2FzZSAnY2xpcC1hcnQnOlxyXG4gICAgICAgICAgICByZXR1cm4gREVGQVVMVF9XSURHRVRfVkFMVUVTLmNsaXBBcnQ7XHJcbiAgICAgICAgY2FzZSAnaW1hZ2UnOlxyXG4gICAgICAgICAgICByZXR1cm4gREVGQVVMVF9XSURHRVRfVkFMVUVTLmltYWdlO1xyXG4gICAgICAgIGNhc2UgJ21hcCc6XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAuLi5ERUZBVUxUX1dJREdFVF9WQUxVRVMubWFwLFxyXG4gICAgICAgICAgICAgICAgaGFzU3VtbWFyeVN1cHBvcnQsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZToge1xyXG4gICAgICAgICAgICAgICAgICAgIC4uLkRFRkFVTFRfV0lER0VUX1ZBTFVFUy5tYXAudmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxfbnVtYmVyOiBwcm9wb3NhbE51bWJlcixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgY2FzZSAnc2VnbWVudHMtbWFwJzpcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgIC4uLkRFRkFVTFRfV0lER0VUX1ZBTFVFUy5zZWdtZW50c01hcCxcclxuICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IHtcclxuICAgICAgICAgICAgICAgICAgICAuLi5ERUZBVUxUX1dJREdFVF9WQUxVRVMuc2VnbWVudHNNYXAudmFsdWUsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxfbnVtYmVyOiBwcm9wb3NhbE51bWJlcixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgY2FzZSAndGV4dCc6XHJcbiAgICAgICAgICAgIHJldHVybiBERUZBVUxUX1dJREdFVF9WQUxVRVMudGV4dDtcclxuICAgICAgICBjYXNlICdzZXBhcmF0b3InOlxyXG4gICAgICAgICAgICByZXR1cm4gREVGQVVMVF9XSURHRVRfVkFMVUVTLnNlcGFyYXRvcjtcclxuICAgICAgICBjYXNlICdzaWduYXR1cmUnOlxyXG4gICAgICAgICAgICByZXR1cm4gREVGQVVMVF9XSURHRVRfVkFMVUVTLnNpZ25hdHVyZTtcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHsgaWQsIGRlZmF1bHRWYWx1ZSwgbWV0YSB9IG9mIGNoYXJ0V2lkZ2V0cykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgPT09IGlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmcmVlR3JpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlSW5mb1JlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoLFxyXG4gICAgICAgICAgICAgICAgICAgIH0gPSBtZXRhO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNsb25lZERlZmF1bHRWYWx1ZXMgPSBjbG9uZURlZXAoZGVmYXVsdFZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZWFsV2lkdGggPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9PT0gMSAmJiAhZnJlZUdyaWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gd2lkdGggKiBHUklEX1dJRFRIXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IHdpZHRoO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlYWxIZWlnaHQgPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9PT0gMSAmJiAhZnJlZUdyaWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gaGVpZ2h0ICogR1JJRF9IRUlHSFRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogaGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHJlYWxIZWlnaHQgPiBsaW1pdFkgPyBsaW1pdFkgOiByZWFsSGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlSW5mb1JlcXVpcmVkOiByZXBsYWNlSW5mb1JlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZToge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi5jbG9uZWREZWZhdWx0VmFsdWVzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3JzOiBidWlsZEdyYXBoc0NvbG9ycyhjbG9uZWREZWZhdWx0VmFsdWVzKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByb3Bvc2FsX251bWJlcjogcHJvcG9zYWxOdW1iZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiByZWFsV2lkdGggPiBsaW1pdFggPyBsaW1pdFggOiByZWFsV2lkdGgsXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZm9yIChjb25zdCB7IGlkLCBkZWZhdWx0VmFsdWUsIG1ldGEgfSBvZiB0YWJsZVdpZGdldHMpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlID09PSBpZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZnJlZUdyaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwbGFjZUluZm9SZXF1aXJlZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VwcG9ydFZlcnNpb24gPSAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aCxcclxuICAgICAgICAgICAgICAgICAgICB9ID0gbWV0YTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZWFsV2lkdGggPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9PT0gMSAmJiAhZnJlZUdyaWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gd2lkdGggKiBHUklEX1dJRFRIXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IHdpZHRoO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlYWxIZWlnaHQgPVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdXBwb3J0VmVyc2lvbiA9PT0gMSAmJiAhZnJlZUdyaWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gaGVpZ2h0ICogR1JJRF9IRUlHSFRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogaGVpZ2h0O1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc1N1bW1hcnlTdXBwb3J0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHJlYWxIZWlnaHQgPiBsaW1pdFkgPyBsaW1pdFkgOiByZWFsSGVpZ2h0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXBsYWNlSW5mb1JlcXVpcmVkOiByZXBsYWNlSW5mb1JlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZTogZ2V0KFRBQkxFX1RIRU1FUywgYCR7dGhlbWV9LnRhYmxlU3R5bGVgLCB7fSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBjbG9uZURlZXAoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLi4uZGVmYXVsdFZhbHVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvcG9zYWxfbnVtYmVyOiBwcm9wb3NhbE51bWJlcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiByZWFsV2lkdGggPiBsaW1pdFggPyBsaW1pdFggOiByZWFsV2lkdGgsXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0Rmlyc3RDYXRhbG9nSWQgPSAoY2F0YWxvZ3MpID0+IHtcclxuICAgIGlmIChjYXRhbG9ncy5sZW5ndGgpIHtcclxuICAgICAgICBjb25zdCBjYXRhbG9nID0gY2F0YWxvZ3MuZmluZCgoaXRlbSkgPT4gaXRlbS5vcmRlciA9PT0gMSk7XHJcbiAgICAgICAgaWYgKGNhdGFsb2cpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNhdGFsb2cuaWQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuICcnO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldExpbWl0cyA9IChvcmllbnRhdGlvbikgPT4ge1xyXG4gICAgaWYgKG9yaWVudGF0aW9uID09PSAnbGFuZHNjYXBlJykge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHg6IFBBR0VfSEVJR0hULFxyXG4gICAgICAgICAgICB5OiBQQUdFX1dJRFRILFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHg6IFBBR0VfV0lEVEgsXHJcbiAgICAgICAgeTogUEFHRV9IRUlHSFQsXHJcbiAgICB9O1xyXG59O1xyXG5cclxuY29uc3QgZ2V0TWlkZGxlWCA9IChzaG93R3VpZGVzLCBvcmllbnRhdGlvbiwgd2lkdGgpID0+IHtcclxuICAgIGNvbnN0IG1pZGRsZSA9XHJcbiAgICAgICAgKChvcmllbnRhdGlvbiA9PT0gJ3BvcnRyYWl0JyA/IFBBR0VfV0lEVEggOiBQQUdFX0hFSUdIVCkgLSB3aWR0aCkgLyAyO1xyXG4gICAgaWYgKHNob3dHdWlkZXMpIHtcclxuICAgICAgICByZXR1cm4gbWlkZGxlIC0gKG1pZGRsZSAlIEdSSURfV0lEVEgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG1pZGRsZTtcclxufTtcclxuXHJcbmNvbnN0IGdldE1pZGRsZVkgPSAoc2hvd0d1aWRlcywgb3JpZW50YXRpb24sIGhlaWdodCkgPT4ge1xyXG4gICAgY29uc3QgbWlkZGxlID1cclxuICAgICAgICAoKG9yaWVudGF0aW9uID09PSAncG9ydHJhaXQnID8gUEFHRV9IRUlHSFQgOiBQQUdFX1dJRFRIKSAtIGhlaWdodCkgLyAyO1xyXG4gICAgaWYgKHNob3dHdWlkZXMpIHtcclxuICAgICAgICByZXR1cm4gbWlkZGxlIC0gKG1pZGRsZSAlIEdSSURfSEVJR0hUKTtcclxuICAgIH1cclxuICAgIHJldHVybiBtaWRkbGU7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0TmV3T3JkZXJJbmRleCA9IChjdXJyZW50SW5kZXgsIG1vdmVUeXBlLCBsaXN0U2l6ZSkgPT4ge1xyXG4gICAgc3dpdGNoIChtb3ZlVHlwZSkge1xyXG4gICAgICAgIGNhc2UgTU9WRV9CQUNLOlxyXG4gICAgICAgICAgICBpZiAoY3VycmVudEluZGV4ID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gMDtcclxuICAgICAgICBjYXNlIE1PVkVfRk9SV0FSRDpcclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRJbmRleCA9PT0gbGlzdFNpemUgLSAxKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gbGlzdFNpemUgLSAxO1xyXG4gICAgICAgIGNhc2UgTU9WRV9TVEVQX0JBQ0s6XHJcbiAgICAgICAgICAgIGlmIChjdXJyZW50SW5kZXggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBjdXJyZW50SW5kZXggLSAxO1xyXG4gICAgICAgIGNhc2UgTU9WRV9TVEVQX0ZPUldBUkQ6XHJcbiAgICAgICAgICAgIGlmIChjdXJyZW50SW5kZXggPT09IGxpc3RTaXplIC0gMSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRJbmRleCArIDE7XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0TnVtID0gKHR5cGUsIGFycmF5KSA9PiB7XHJcbiAgICBjb25zdCB0eXBlTGlzdCA9IHNvcnRCeShcclxuICAgICAgICBhcnJheS5maWx0ZXIoKGl0ZW0pID0+IGl0ZW0ubmFtZSA9PT0gdHlwZSksXHJcbiAgICAgICAgW1xyXG4gICAgICAgICAgICAocCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlSW50KHAubnVtKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdXHJcbiAgICApO1xyXG4gICAgaWYgKHR5cGVMaXN0Lmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgIHJldHVybiAxO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHBhcnNlSW50KHR5cGVMaXN0W3R5cGVMaXN0Lmxlbmd0aCAtIDFdLm51bSkgKyAxO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldFBhZ2VVcGRhdGluZ0NvbnRlbnREYXRhID0gKHBhZ2UpID0+IHtcclxuICAgIGlmIChwYWdlLmlkID09PSAnJykge1xyXG4gICAgICAgIHJldHVybiBwYWdlO1xyXG4gICAgfVxyXG4gICAgbGV0IHRlbXBWYWx1ZXMgPSB7fTtcclxuICAgIGxldCB0ZW1wV2lkZ2V0cyA9IFtdO1xyXG4gICAgbGV0IHJlcGxhY2VJbmZvID0gW107XHJcbiAgICBwYWdlLndpZGdldHMuZm9yRWFjaCgod2lkZ2V0KSA9PiB7XHJcbiAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICByZXBsYWNlSW5mb0RhdGEsXHJcbiAgICAgICAgICAgIHZhbHVlLFxyXG4gICAgICAgICAgICB3aWRnZXQ6IHdpZGdldERhdGEsXHJcbiAgICAgICAgfSA9IGdldFdpZGdldFVwZGF0aW5nQ29udGVudERhdGEod2lkZ2V0KTtcclxuICAgICAgICB0ZW1wVmFsdWVzID0geyAuLi50ZW1wVmFsdWVzLCAuLi52YWx1ZSB9O1xyXG4gICAgICAgIHRlbXBXaWRnZXRzID0gWy4uLnRlbXBXaWRnZXRzLCB3aWRnZXREYXRhXTtcclxuICAgICAgICBpZiAocmVwbGFjZUluZm9EYXRhKSB7XHJcbiAgICAgICAgICAgIHJlcGxhY2VJbmZvID0gWy4uLnJlcGxhY2VJbmZvLCByZXBsYWNlSW5mb0RhdGFdO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IGNvbnRlbnQgPSBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgW3BhZ2UucGFyc2VkRGF0YUtleV06IHtcclxuICAgICAgICAgICAgdGhlbWU6IHBhZ2UudGhlbWUsXHJcbiAgICAgICAgICAgIG9yaWVudGF0aW9uOiBwYWdlLm9yaWVudGF0aW9uLFxyXG4gICAgICAgICAgICBpbmZpbml0ZU1vZGU6IHBhZ2UuaW5maW5pdGVNb2RlLFxyXG4gICAgICAgICAgICByZXBsYWNlSW5mbzogcmVwbGFjZUluZm8sXHJcbiAgICAgICAgICAgIHZhbHVlczogdGVtcFZhbHVlcyxcclxuICAgICAgICAgICAgd2lkZ2V0czogdGVtcFdpZGdldHMsXHJcbiAgICAgICAgfSxcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBibG9ja2VkOiBwYWdlLmJsb2NrZWQsXHJcbiAgICAgICAgY29tcGFueTogcGFnZS5jb21wYW55LFxyXG4gICAgICAgIGNvbnRlbnQsXHJcbiAgICAgICAgY3VzdG9tX3RlbXBsYXRlOiBwYWdlLmN1c3RvbV90ZW1wbGF0ZSxcclxuICAgICAgICBncm91cGVkX2J5OiBwYWdlLmdyb3VwZWRfYnksXHJcbiAgICAgICAgaWQ6IHBhZ2UuaWQsXHJcbiAgICAgICAgcGFnZTogcGFnZS5wYWdlLFxyXG4gICAgICAgIHBhZ2VfcGFyZW50OiBwYWdlLnBhZ2VfcGFyZW50LFxyXG4gICAgfTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBnZXRVcGRhdGluZ0NvbnRlbnREYXRhID0gKHRlbXBsYXRlRGF0YSkgPT4ge1xyXG4gICAgY29uc3QgbW9kaWZpZWRUZW1wbGF0ZURhdGEgPSB7XHJcbiAgICAgICAgY29tcGFueTogdGVtcGxhdGVEYXRhLmNvbXBhbnksXHJcbiAgICAgICAgY3JlYXRlZF9hdDogdGVtcGxhdGVEYXRhLmNyZWF0ZWRfYXQsXHJcbiAgICAgICAgZGVzY3JpcHRpb246IHRlbXBsYXRlRGF0YS5kZXNjcmlwdGlvbixcclxuICAgICAgICBmb290ZXI6IHRlbXBsYXRlRGF0YS5mb290ZXIsXHJcbiAgICAgICAgaWQ6IHRlbXBsYXRlRGF0YS5pZCxcclxuICAgICAgICBsb2dvOiB0ZW1wbGF0ZURhdGEubG9nbyxcclxuICAgICAgICBwYXJlbnQ6IHRlbXBsYXRlRGF0YS5wYXJlbnQsXHJcbiAgICAgICAgcHJpbWFyeV9jb2xvcjogdGVtcGxhdGVEYXRhLnByaW1hcnlfY29sb3IsXHJcbiAgICAgICAgc2Vjb25kYXJ5X2NvbG9yOiB0ZW1wbGF0ZURhdGEuc2Vjb25kYXJ5X2NvbG9yLFxyXG4gICAgICAgIHRpdGxlOiB0ZW1wbGF0ZURhdGEudGl0bGUsXHJcbiAgICAgICAgdHlwb2dyYXBoeTogdGVtcGxhdGVEYXRhLnR5cG9ncmFwaHksXHJcbiAgICAgICAgdXBkYXRlZF9hdDogdGVtcGxhdGVEYXRhLnVwZGF0ZWRfYXQsXHJcbiAgICAgICAgdmVyc2lvbjogdGVtcGxhdGVEYXRhLnZlcnNpb24sXHJcbiAgICAgICAgcGFnZXNfdGVtcGxhdGU6IGdldFBhZ2VzVGVtcGxhdGUodGVtcGxhdGVEYXRhLnBhZ2VzKSxcclxuICAgIH07XHJcbiAgICByZXR1cm4gbW9kaWZpZWRUZW1wbGF0ZURhdGE7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgb3JkZXJQYWdlcyA9IChwYWdlc0xpc3QpID0+IHtcclxuICAgIGxldCBpbmRleFBhZ2UgPSAwO1xyXG4gICAgcmV0dXJuIHBhZ2VzTGlzdC5tYXAoKGl0ZW0pID0+IHtcclxuICAgICAgICBpZiAoaXRlbS50eXBlID09PSAnZ3JvdXAnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpcnN0UGFnZSA9IGAke2luZGV4UGFnZSArIDF9YDtcclxuICAgICAgICAgICAgY29uc3QgbmV3UGFnZXMgPSBpdGVtLnBhZ2VzLm1hcCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgKytpbmRleFBhZ2U7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC4uLmVsZW1lbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFnZTogYCR7aW5kZXhQYWdlfWAsXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIHsgLi4uaXRlbSwgcGFnZTogZmlyc3RQYWdlLCBwYWdlczogbmV3UGFnZXMgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgKytpbmRleFBhZ2U7XHJcbiAgICAgICAgcmV0dXJuIHsgLi4uaXRlbSwgcGFnZTogYCR7aW5kZXhQYWdlfWAgfTtcclxuICAgIH0pO1xyXG59O1xyXG4iXX0=