"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getCurrencies = function getCurrencies(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/companies-currencies/');
  };
};

var _default = getCurrencies;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0Q3VycmVuY2llcy5qcyJdLCJuYW1lcyI6WyJnZXRDdXJyZW5jaWVzIiwic3RhdGUiLCJnZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztBQUVBLElBQU1BLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBQ0MsS0FBRDtBQUFBLFNBQVc7QUFBQSxXQUM3QixxQkFBT0EsS0FBUCxFQUFjQyxHQUFkLENBQWtCLCtCQUFsQixDQUQ2QjtBQUFBLEdBQVg7QUFBQSxDQUF0Qjs7ZUFHZUYsYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnZXRBcGkgZnJvbSAnLi9nZXRBcGknO1xyXG5cclxuY29uc3QgZ2V0Q3VycmVuY2llcyA9IChzdGF0ZSkgPT4gKCkgPT5cclxuICAgIGdldEFwaShzdGF0ZSkuZ2V0KCcvYXBpL3YxL2NvbXBhbmllcy1jdXJyZW5jaWVzLycpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZ2V0Q3VycmVuY2llcztcclxuIl19