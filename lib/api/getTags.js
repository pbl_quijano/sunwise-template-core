"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getTags = function getTags(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/tags-values/');
  };
};

var _default = getTags;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0VGFncy5qcyJdLCJuYW1lcyI6WyJnZXRUYWdzIiwic3RhdGUiLCJnZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztBQUVBLElBQU1BLE9BQU8sR0FBRyxTQUFWQSxPQUFVLENBQUNDLEtBQUQ7QUFBQSxTQUFXO0FBQUEsV0FBTSxxQkFBT0EsS0FBUCxFQUFjQyxHQUFkLENBQWtCLHNCQUFsQixDQUFOO0FBQUEsR0FBWDtBQUFBLENBQWhCOztlQUVlRixPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRUYWdzID0gKHN0YXRlKSA9PiAoKSA9PiBnZXRBcGkoc3RhdGUpLmdldCgnL2FwaS92MS90YWdzLXZhbHVlcy8nKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdldFRhZ3M7XHJcbiJdfQ==