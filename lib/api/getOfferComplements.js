"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getOfferComplements = function getOfferComplements(state) {
  return function (commercialOfferId) {
    return (0, _getApi.default)(state).get("/api/v1/offer-complements/".concat(commercialOfferId));
  };
};

var _default = getOfferComplements;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0T2ZmZXJDb21wbGVtZW50cy5qcyJdLCJuYW1lcyI6WyJnZXRPZmZlckNvbXBsZW1lbnRzIiwic3RhdGUiLCJjb21tZXJjaWFsT2ZmZXJJZCIsImdldCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRUEsSUFBTUEsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDQyxLQUFEO0FBQUEsU0FBVyxVQUFDQyxpQkFBRDtBQUFBLFdBQ25DLHFCQUFPRCxLQUFQLEVBQWNFLEdBQWQscUNBQStDRCxpQkFBL0MsRUFEbUM7QUFBQSxHQUFYO0FBQUEsQ0FBNUI7O2VBR2VGLG1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRPZmZlckNvbXBsZW1lbnRzID0gKHN0YXRlKSA9PiAoY29tbWVyY2lhbE9mZmVySWQpID0+XHJcbiAgICBnZXRBcGkoc3RhdGUpLmdldChgL2FwaS92MS9vZmZlci1jb21wbGVtZW50cy8ke2NvbW1lcmNpYWxPZmZlcklkfWApO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZ2V0T2ZmZXJDb21wbGVtZW50cztcclxuIl19