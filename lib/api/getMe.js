"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getChartCurrency = function getChartCurrency(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/me/');
  };
};

var _default = getChartCurrency;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0TWUuanMiXSwibmFtZXMiOlsiZ2V0Q2hhcnRDdXJyZW5jeSIsInN0YXRlIiwiZ2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7QUFFQSxJQUFNQSxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLEtBQUQ7QUFBQSxTQUFXO0FBQUEsV0FBTSxxQkFBT0EsS0FBUCxFQUFjQyxHQUFkLENBQWtCLGFBQWxCLENBQU47QUFBQSxHQUFYO0FBQUEsQ0FBekI7O2VBRWVGLGdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRDaGFydEN1cnJlbmN5ID0gKHN0YXRlKSA9PiAoKSA9PiBnZXRBcGkoc3RhdGUpLmdldCgnL2FwaS92MS9tZS8nKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdldENoYXJ0Q3VycmVuY3k7XHJcbiJdfQ==