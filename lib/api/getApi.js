"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _objectToFormdata = _interopRequireDefault(require("object-to-formdata"));

var mainSelectors = _interopRequireWildcard(require("../modules/main/selectors"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getApi = function getApi(state) {
  var baseURL = mainSelectors.getBaseUrl(state);
  var token = mainSelectors.getToken(state);

  var API = _axios.default.create({
    baseURL: baseURL
  });

  API.interceptors.request.use(function (req) {
    req.headers['Authorization'] = "JWT ".concat(token);

    if (req.headers['Content-Type'] === 'multipart/form-data' && (req.method === 'post' || req.method === 'put')) {
      req.data = (0, _objectToFormdata.default)(req.data, {
        indices: true
      });
    }

    return req;
  });
  API.interceptors.response.use(function (res) {
    return res;
  }, function (error) {
    if (error.code === 'ECONNABORTED' || typeof error.response === 'undefined') {
      var _error = Object.assign({}, error);

      return Promise.reject(_objectSpread(_objectSpread({}, _error), {}, {
        data: {
          errors: ['Ocurrio un error de conexion']
        },
        response: {
          data: {
            errors: ['Ocurrio un error de conexion']
          }
        }
      }));
    }

    return Promise.reject(error);
  });
  return API;
};

var _default = getApi;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0QXBpLmpzIl0sIm5hbWVzIjpbImdldEFwaSIsInN0YXRlIiwiYmFzZVVSTCIsIm1haW5TZWxlY3RvcnMiLCJnZXRCYXNlVXJsIiwidG9rZW4iLCJnZXRUb2tlbiIsIkFQSSIsImF4aW9zIiwiY3JlYXRlIiwiaW50ZXJjZXB0b3JzIiwicmVxdWVzdCIsInVzZSIsInJlcSIsImhlYWRlcnMiLCJtZXRob2QiLCJkYXRhIiwiaW5kaWNlcyIsInJlc3BvbnNlIiwicmVzIiwiZXJyb3IiLCJjb2RlIiwiX2Vycm9yIiwiT2JqZWN0IiwiYXNzaWduIiwiUHJvbWlzZSIsInJlamVjdCIsImVycm9ycyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsTUFBTSxHQUFHLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRCxFQUFXO0FBQ3RCLE1BQU1DLE9BQU8sR0FBR0MsYUFBYSxDQUFDQyxVQUFkLENBQXlCSCxLQUF6QixDQUFoQjtBQUNBLE1BQU1JLEtBQUssR0FBR0YsYUFBYSxDQUFDRyxRQUFkLENBQXVCTCxLQUF2QixDQUFkOztBQUNBLE1BQU1NLEdBQUcsR0FBR0MsZUFBTUMsTUFBTixDQUFhO0FBQ3JCUCxJQUFBQSxPQUFPLEVBQVBBO0FBRHFCLEdBQWIsQ0FBWjs7QUFHQUssRUFBQUEsR0FBRyxDQUFDRyxZQUFKLENBQWlCQyxPQUFqQixDQUF5QkMsR0FBekIsQ0FBNkIsVUFBQ0MsR0FBRCxFQUFTO0FBQ2xDQSxJQUFBQSxHQUFHLENBQUNDLE9BQUosQ0FBWSxlQUFaLGtCQUFzQ1QsS0FBdEM7O0FBQ0EsUUFDSVEsR0FBRyxDQUFDQyxPQUFKLENBQVksY0FBWixNQUFnQyxxQkFBaEMsS0FDQ0QsR0FBRyxDQUFDRSxNQUFKLEtBQWUsTUFBZixJQUF5QkYsR0FBRyxDQUFDRSxNQUFKLEtBQWUsS0FEekMsQ0FESixFQUdFO0FBQ0VGLE1BQUFBLEdBQUcsQ0FBQ0csSUFBSixHQUFXLCtCQUFpQkgsR0FBRyxDQUFDRyxJQUFyQixFQUEyQjtBQUFFQyxRQUFBQSxPQUFPLEVBQUU7QUFBWCxPQUEzQixDQUFYO0FBQ0g7O0FBRUQsV0FBT0osR0FBUDtBQUNILEdBVkQ7QUFXQU4sRUFBQUEsR0FBRyxDQUFDRyxZQUFKLENBQWlCUSxRQUFqQixDQUEwQk4sR0FBMUIsQ0FDSSxVQUFDTyxHQUFELEVBQVM7QUFDTCxXQUFPQSxHQUFQO0FBQ0gsR0FITCxFQUlJLFVBQUNDLEtBQUQsRUFBVztBQUNQLFFBQ0lBLEtBQUssQ0FBQ0MsSUFBTixLQUFlLGNBQWYsSUFDQSxPQUFPRCxLQUFLLENBQUNGLFFBQWIsS0FBMEIsV0FGOUIsRUFHRTtBQUNFLFVBQU1JLE1BQU0sR0FBR0MsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQkosS0FBbEIsQ0FBZjs7QUFFQSxhQUFPSyxPQUFPLENBQUNDLE1BQVIsaUNBQ0FKLE1BREE7QUFFSE4sUUFBQUEsSUFBSSxFQUFFO0FBQUVXLFVBQUFBLE1BQU0sRUFBRSxDQUFDLDhCQUFEO0FBQVYsU0FGSDtBQUdIVCxRQUFBQSxRQUFRLEVBQUU7QUFDTkYsVUFBQUEsSUFBSSxFQUFFO0FBQUVXLFlBQUFBLE1BQU0sRUFBRSxDQUFDLDhCQUFEO0FBQVY7QUFEQTtBQUhQLFNBQVA7QUFPSDs7QUFFRCxXQUFPRixPQUFPLENBQUNDLE1BQVIsQ0FBZU4sS0FBZixDQUFQO0FBQ0gsR0FyQkw7QUF1QkEsU0FBT2IsR0FBUDtBQUNILENBekNEOztlQTJDZVAsTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBheGlvcyBmcm9tICdheGlvcyc7XG5pbXBvcnQgb2JqZWN0VG9Gb3JtZGF0YSBmcm9tICdvYmplY3QtdG8tZm9ybWRhdGEnO1xuXG5pbXBvcnQgKiBhcyBtYWluU2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XG5cbmNvbnN0IGdldEFwaSA9IChzdGF0ZSkgPT4ge1xuICAgIGNvbnN0IGJhc2VVUkwgPSBtYWluU2VsZWN0b3JzLmdldEJhc2VVcmwoc3RhdGUpO1xuICAgIGNvbnN0IHRva2VuID0gbWFpblNlbGVjdG9ycy5nZXRUb2tlbihzdGF0ZSk7XG4gICAgY29uc3QgQVBJID0gYXhpb3MuY3JlYXRlKHtcbiAgICAgICAgYmFzZVVSTCxcbiAgICB9KTtcbiAgICBBUEkuaW50ZXJjZXB0b3JzLnJlcXVlc3QudXNlKChyZXEpID0+IHtcbiAgICAgICAgcmVxLmhlYWRlcnNbJ0F1dGhvcml6YXRpb24nXSA9IGBKV1QgJHt0b2tlbn1gO1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICByZXEuaGVhZGVyc1snQ29udGVudC1UeXBlJ10gPT09ICdtdWx0aXBhcnQvZm9ybS1kYXRhJyAmJlxuICAgICAgICAgICAgKHJlcS5tZXRob2QgPT09ICdwb3N0JyB8fCByZXEubWV0aG9kID09PSAncHV0JylcbiAgICAgICAgKSB7XG4gICAgICAgICAgICByZXEuZGF0YSA9IG9iamVjdFRvRm9ybWRhdGEocmVxLmRhdGEsIHsgaW5kaWNlczogdHJ1ZSB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXE7XG4gICAgfSk7XG4gICAgQVBJLmludGVyY2VwdG9ycy5yZXNwb25zZS51c2UoXG4gICAgICAgIChyZXMpID0+IHtcbiAgICAgICAgICAgIHJldHVybiByZXM7XG4gICAgICAgIH0sXG4gICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgIGVycm9yLmNvZGUgPT09ICdFQ09OTkFCT1JURUQnIHx8XG4gICAgICAgICAgICAgICAgdHlwZW9mIGVycm9yLnJlc3BvbnNlID09PSAndW5kZWZpbmVkJ1xuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgX2Vycm9yID0gT2JqZWN0LmFzc2lnbih7fSwgZXJyb3IpO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KHtcbiAgICAgICAgICAgICAgICAgICAgLi4uX2Vycm9yLFxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7IGVycm9yczogWydPY3VycmlvIHVuIGVycm9yIGRlIGNvbmV4aW9uJ10gfSxcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2U6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHsgZXJyb3JzOiBbJ09jdXJyaW8gdW4gZXJyb3IgZGUgY29uZXhpb24nXSB9LFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoZXJyb3IpO1xuICAgICAgICB9XG4gICAgKTtcbiAgICByZXR1cm4gQVBJO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0QXBpO1xuIl19