"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getDecimalFormats = function getDecimalFormats(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/decimals-formats/');
  };
};

var _default = getDecimalFormats;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0RGVjaW1hbEZvcm1hdHMuanMiXSwibmFtZXMiOlsiZ2V0RGVjaW1hbEZvcm1hdHMiLCJzdGF0ZSIsImdldCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRUEsSUFBTUEsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDQyxLQUFEO0FBQUEsU0FBVztBQUFBLFdBQ2pDLHFCQUFPQSxLQUFQLEVBQWNDLEdBQWQsQ0FBa0IsMkJBQWxCLENBRGlDO0FBQUEsR0FBWDtBQUFBLENBQTFCOztlQUdlRixpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnZXRBcGkgZnJvbSAnLi9nZXRBcGknO1xyXG5cclxuY29uc3QgZ2V0RGVjaW1hbEZvcm1hdHMgPSAoc3RhdGUpID0+ICgpID0+XHJcbiAgICBnZXRBcGkoc3RhdGUpLmdldCgnL2FwaS92MS9kZWNpbWFscy1mb3JtYXRzLycpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZ2V0RGVjaW1hbEZvcm1hdHM7XHJcbiJdfQ==