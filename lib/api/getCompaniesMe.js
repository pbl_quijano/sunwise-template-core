"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getCompaniesMe = function getCompaniesMe(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/companies/me/');
  };
};

var _default = getCompaniesMe;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0Q29tcGFuaWVzTWUuanMiXSwibmFtZXMiOlsiZ2V0Q29tcGFuaWVzTWUiLCJzdGF0ZSIsImdldCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRUEsSUFBTUEsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxLQUFEO0FBQUEsU0FBVztBQUFBLFdBQzlCLHFCQUFPQSxLQUFQLEVBQWNDLEdBQWQsQ0FBa0IsdUJBQWxCLENBRDhCO0FBQUEsR0FBWDtBQUFBLENBQXZCOztlQUdlRixjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRDb21wYW5pZXNNZSA9IChzdGF0ZSkgPT4gKCkgPT5cclxuICAgIGdldEFwaShzdGF0ZSkuZ2V0KCcvYXBpL3YxL2NvbXBhbmllcy9tZS8nKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdldENvbXBhbmllc01lO1xyXG4iXX0=