"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getTagsFormats = function getTagsFormats(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/tags-formats/');
  };
};

var _default = getTagsFormats;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0VGFnc0Zvcm1hdHMuanMiXSwibmFtZXMiOlsiZ2V0VGFnc0Zvcm1hdHMiLCJzdGF0ZSIsImdldCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRUEsSUFBTUEsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxLQUFEO0FBQUEsU0FBVztBQUFBLFdBQzlCLHFCQUFPQSxLQUFQLEVBQWNDLEdBQWQsQ0FBa0IsdUJBQWxCLENBRDhCO0FBQUEsR0FBWDtBQUFBLENBQXZCOztlQUdlRixjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRUYWdzRm9ybWF0cyA9IChzdGF0ZSkgPT4gKCkgPT5cclxuICAgIGdldEFwaShzdGF0ZSkuZ2V0KCcvYXBpL3YxL3RhZ3MtZm9ybWF0cy8nKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdldFRhZ3NGb3JtYXRzO1xyXG4iXX0=