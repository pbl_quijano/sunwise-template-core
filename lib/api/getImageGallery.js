"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getImageGallery = function getImageGallery(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/logo-companies-tags/');
  };
};

var _default = getImageGallery;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0SW1hZ2VHYWxsZXJ5LmpzIl0sIm5hbWVzIjpbImdldEltYWdlR2FsbGVyeSIsInN0YXRlIiwiZ2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7QUFFQSxJQUFNQSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUNDLEtBQUQ7QUFBQSxTQUFXO0FBQUEsV0FDL0IscUJBQU9BLEtBQVAsRUFBY0MsR0FBZCxDQUFrQiw4QkFBbEIsQ0FEK0I7QUFBQSxHQUFYO0FBQUEsQ0FBeEI7O2VBR2VGLGUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0QXBpIGZyb20gJy4vZ2V0QXBpJztcclxuXHJcbmNvbnN0IGdldEltYWdlR2FsbGVyeSA9IChzdGF0ZSkgPT4gKCkgPT5cclxuICAgIGdldEFwaShzdGF0ZSkuZ2V0KCcvYXBpL3YxL2xvZ28tY29tcGFuaWVzLXRhZ3MvJyk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBnZXRJbWFnZUdhbGxlcnk7XHJcbiJdfQ==