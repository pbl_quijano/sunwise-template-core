"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getTableWidgets = function getTableWidgets(state) {
  return function (lang) {
    return (0, _getApi.default)(state).get("/api/v1/tables-values/".concat(lang ? "?lang=".concat(lang) : ''));
  };
};

var _default = getTableWidgets;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0VGFibGVXaWRnZXRzLmpzIl0sIm5hbWVzIjpbImdldFRhYmxlV2lkZ2V0cyIsInN0YXRlIiwibGFuZyIsImdldCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7O0FBRUEsSUFBTUEsZUFBZSxHQUFHLFNBQWxCQSxlQUFrQixDQUFDQyxLQUFEO0FBQUEsU0FBVyxVQUFDQyxJQUFEO0FBQUEsV0FDL0IscUJBQU9ELEtBQVAsRUFBY0UsR0FBZCxpQ0FBMkNELElBQUksbUJBQVlBLElBQVosSUFBcUIsRUFBcEUsRUFEK0I7QUFBQSxHQUFYO0FBQUEsQ0FBeEI7O2VBR2VGLGUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0QXBpIGZyb20gJy4vZ2V0QXBpJztcclxuXHJcbmNvbnN0IGdldFRhYmxlV2lkZ2V0cyA9IChzdGF0ZSkgPT4gKGxhbmcpID0+XHJcbiAgICBnZXRBcGkoc3RhdGUpLmdldChgL2FwaS92MS90YWJsZXMtdmFsdWVzLyR7bGFuZyA/IGA/bGFuZz0ke2xhbmd9YCA6ICcnfWApO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZ2V0VGFibGVXaWRnZXRzO1xyXG4iXX0=