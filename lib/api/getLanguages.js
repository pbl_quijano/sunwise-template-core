"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getLanguages = function getLanguages(state) {
  return function () {
    return (0, _getApi.default)(state).get('/api/v1/international/languages/');
  };
};

var _default = getLanguages;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0TGFuZ3VhZ2VzLmpzIl0sIm5hbWVzIjpbImdldExhbmd1YWdlcyIsInN0YXRlIiwiZ2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7QUFFQSxJQUFNQSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDQyxLQUFEO0FBQUEsU0FBVztBQUFBLFdBQzVCLHFCQUFPQSxLQUFQLEVBQWNDLEdBQWQsQ0FBa0Isa0NBQWxCLENBRDRCO0FBQUEsR0FBWDtBQUFBLENBQXJCOztlQUdlRixZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdldEFwaSBmcm9tICcuL2dldEFwaSc7XHJcblxyXG5jb25zdCBnZXRMYW5ndWFnZXMgPSAoc3RhdGUpID0+ICgpID0+XHJcbiAgICBnZXRBcGkoc3RhdGUpLmdldCgnL2FwaS92MS9pbnRlcm5hdGlvbmFsL2xhbmd1YWdlcy8nKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGdldExhbmd1YWdlcztcclxuIl19