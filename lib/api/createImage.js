"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var createImage = function createImage(state) {
  return function (data) {
    return (0, _getApi.default)(state).post('/api/v1/templates-images/', _objectSpread({}, data));
  };
};

var _default = createImage;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvY3JlYXRlSW1hZ2UuanMiXSwibmFtZXMiOlsiY3JlYXRlSW1hZ2UiLCJzdGF0ZSIsImRhdGEiLCJwb3N0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFDQyxLQUFEO0FBQUEsU0FBVyxVQUFDQyxJQUFEO0FBQUEsV0FDM0IscUJBQU9ELEtBQVAsRUFBY0UsSUFBZCxDQUFtQiwyQkFBbkIsb0JBQXFERCxJQUFyRCxFQUQyQjtBQUFBLEdBQVg7QUFBQSxDQUFwQjs7ZUFHZUYsVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnZXRBcGkgZnJvbSAnLi9nZXRBcGknO1xyXG5cclxuY29uc3QgY3JlYXRlSW1hZ2UgPSAoc3RhdGUpID0+IChkYXRhKSA9PlxyXG4gICAgZ2V0QXBpKHN0YXRlKS5wb3N0KCcvYXBpL3YxL3RlbXBsYXRlcy1pbWFnZXMvJywgeyAuLi5kYXRhIH0pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlSW1hZ2U7XHJcbiJdfQ==