"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _getApi = _interopRequireDefault(require("./getApi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getCatalogs = function getCatalogs(state) {
  return function (type) {
    return (0, _getApi.default)(state).get("/api/v1/companies-catalogs/".concat(type));
  };
};

var _default = getCatalogs;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9hcGkvZ2V0Q2F0YWxvZ3MuanMiXSwibmFtZXMiOlsiZ2V0Q2F0YWxvZ3MiLCJzdGF0ZSIsInR5cGUiLCJnZXQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztBQUVBLElBQU1BLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQUNDLEtBQUQ7QUFBQSxTQUFXLFVBQUNDLElBQUQ7QUFBQSxXQUMzQixxQkFBT0QsS0FBUCxFQUFjRSxHQUFkLHNDQUFnREQsSUFBaEQsRUFEMkI7QUFBQSxHQUFYO0FBQUEsQ0FBcEI7O2VBR2VGLFciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0QXBpIGZyb20gJy4vZ2V0QXBpJztcclxuXHJcbmNvbnN0IGdldENhdGFsb2dzID0gKHN0YXRlKSA9PiAodHlwZSkgPT5cclxuICAgIGdldEFwaShzdGF0ZSkuZ2V0KGAvYXBpL3YxL2NvbXBhbmllcy1jYXRhbG9ncy8ke3R5cGV9YCk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBnZXRDYXRhbG9ncztcclxuIl19