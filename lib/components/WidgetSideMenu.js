"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _isNil = _interopRequireDefault(require("lodash/isNil"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _PageProperties = _interopRequireDefault(require("../modules/PageProperties"));

var templateViewSelectors = _interopRequireWildcard(require("../modules/TemplateView/selectors"));

var _WidgetMenu = _interopRequireDefault(require("../modules/WidgetMenu"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6;

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WIDGETS_SIDE_WIDTH = 332;

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    background-color: #ffffff;\n    border-left: 1px solid #eff1fb;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    position: relative;\n    z-index: 55;\n    ", "\n\n    &:hover .resize-button {\n        opacity: 1;\n    }\n"])), function (_ref) {
  var disabled = _ref.disabled;
  return disabled && "opacity: 0.6;\n    cursor: not-allowed;\n    pointer-events: none;";
});

var MenuContainer = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    background-color: #ffffff;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    display: flex;\n    flex-direction: column;\n    height: 100%;\n    min-width: ", "px;\n    overflow-y: auto;\n    width: ", "px;\n"])), WIDGETS_SIDE_WIDTH, WIDGETS_SIDE_WIDTH);

var Wrapper = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    max-width: ", "px;\n    overflow: hidden;\n    transition: max-width 0.3s linear;\n    width: ", "px;\n\n    &.hide {\n        max-width: 0;\n    }\n"])), WIDGETS_SIDE_WIDTH, WIDGETS_SIDE_WIDTH);

var ToggleButtonContainer = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    align-items: center;\n    background-color: transparent;\n    box-shadow: 10px 12px 17px 0 rgba(129, 158, 200, 0.06);\n    cursor: pointer;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    overflow: hidden;\n    transition: width 0.2s ease-in, background-color 0.2s linear;\n    width: 0;\n\n    &:hover {\n        background-color: rgba(0, 0, 0, 0.06);\n    }\n\n    &.hide-mode {\n        transition-delay: 0.25s;\n        width: 26px;\n    }\n"])));

var StyledIcon = _styledComponents.default.i(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 20px;\n"])));

var ResizeButton = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    background-color: #fff;\n    border-radius: 50%;\n    box-shadow: rgba(9, 30, 66, 0.08) 0px 0px 0px 1px,\n        rgba(9, 30, 66, 0.08) 0px 2px 4px 1px;\n    color: #ff9a00;\n    cursor: pointer;\n    font-size: 12px;\n    height: 24px;\n    line-height: 24px;\n    opacity: 0;\n    position: absolute;\n    right: ", "px;\n    text-align: center;\n    top: 75px;\n    visibility: ", ";\n    width: 24px;\n    z-index: 55;\n\n    i {\n        margin-left: 1px;\n    }\n\n    &:hover {\n        color: #fff;\n        background-color: #ff9a00;\n    }\n\n    ", "\n"])), WIDGETS_SIDE_WIDTH - 12, function (_ref2) {
  var isShowing = _ref2.isShowing;
  return isShowing ? 'visible' : 'hidden';
}, function (_ref3) {
  var isShowing = _ref3.isShowing;
  return isShowing && "transition: background-color 100ms, color 100ms,\n        visibility 0s linear 0.5s, opacity 0.2s;";
});

var WidgetSideMenu = function WidgetSideMenu(_ref4) {
  var canUpdatePanelsSowing = _ref4.canUpdatePanelsSowing,
      canUpdateProjectLocation = _ref4.canUpdateProjectLocation,
      _ref4$key = _ref4.key,
      key = _ref4$key === void 0 ? '' : _ref4$key,
      selectedPageIsBlocked = _ref4.selectedPageIsBlocked,
      _ref4$themeEnabled = _ref4.themeEnabled,
      themeEnabled = _ref4$themeEnabled === void 0 ? true : _ref4$themeEnabled;

  var _useState = (0, _react.useState)((0, _isNil.default)(localStorage.getItem(key + ':rightSideShowed')) || localStorage.getItem(key + ':rightSideShowed') === 'true'),
      _useState2 = _slicedToArray(_useState, 2),
      isShowing = _useState2[0],
      setIsShowing = _useState2[1];

  var handleSetIsShowing = function handleSetIsShowing(value) {
    localStorage.setItem(key + ':rightSideShowed', value);
    setIsShowing(value);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    disabled: selectedPageIsBlocked,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(ToggleButtonContainer, {
      className: !isShowing && 'hide-mode',
      onClick: function onClick() {
        return handleSetIsShowing(true);
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledIcon, {
        className: "fa fa-chevron-left"
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
      className: !isShowing && 'hide',
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(MenuContainer, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_PageProperties.default, {
          themeEnabled: themeEnabled
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_WidgetMenu.default, {
          canUpdatePanelsSowing: canUpdatePanelsSowing,
          canUpdateProjectLocation: canUpdateProjectLocation
        })]
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(ResizeButton, {
      className: "resize-button",
      isShowing: isShowing,
      onClick: function onClick() {
        return handleSetIsShowing(false);
      },
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
        className: "fas fa-chevron-right"
      })
    })]
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked
});
WidgetSideMenu.propTypes = {
  canUpdatePanelsSowing: _propTypes.default.bool,
  canUpdateProjectLocation: _propTypes.default.bool,
  key: _propTypes.default.string,
  selectedPageIsBlocked: _propTypes.default.bool,
  themeEnabled: _propTypes.default.bool
};

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(WidgetSideMenu);

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1dpZGdldFNpZGVNZW51LmpzIl0sIm5hbWVzIjpbIldJREdFVFNfU0lERV9XSURUSCIsIkNvbnRhaW5lciIsInN0eWxlZCIsImRpdiIsImRpc2FibGVkIiwiTWVudUNvbnRhaW5lciIsIldyYXBwZXIiLCJUb2dnbGVCdXR0b25Db250YWluZXIiLCJTdHlsZWRJY29uIiwiaSIsIlJlc2l6ZUJ1dHRvbiIsImlzU2hvd2luZyIsIldpZGdldFNpZGVNZW51IiwiY2FuVXBkYXRlUGFuZWxzU293aW5nIiwiY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uIiwia2V5Iiwic2VsZWN0ZWRQYWdlSXNCbG9ja2VkIiwidGhlbWVFbmFibGVkIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsInNldElzU2hvd2luZyIsImhhbmRsZVNldElzU2hvd2luZyIsInZhbHVlIiwic2V0SXRlbSIsIm1hcFN0YXRlVG9Qcm9wcyIsInRlbXBsYXRlVmlld1NlbGVjdG9ycyIsImdldFNlbGVjdGVkUGFnZUlzQmxvY2tlZCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBLElBQU1BLGtCQUFrQixHQUFHLEdBQTNCOztBQUVBLElBQU1DLFNBQVMsR0FBR0MsMEJBQU9DLEdBQVYsMlVBT1Q7QUFBQSxNQUFHQyxRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUNFQSxRQUFRLHdFQURWO0FBQUEsQ0FQUyxDQUFmOztBQWtCQSxJQUFNQyxhQUFhLEdBQUdILDBCQUFPQyxHQUFWLDBTQU1GSCxrQkFORSxFQVFOQSxrQkFSTSxDQUFuQjs7QUFXQSxJQUFNTSxPQUFPLEdBQUdKLDBCQUFPQyxHQUFWLG1PQUNJSCxrQkFESixFQUlBQSxrQkFKQSxDQUFiOztBQVdBLElBQU1PLHFCQUFxQixHQUFHTCwwQkFBT0MsR0FBViw4aUJBQTNCOztBQXNCQSxJQUFNSyxVQUFVLEdBQUdOLDBCQUFPTyxDQUFWLG9IQUFoQjs7QUFLQSxJQUFNQyxZQUFZLEdBQUdSLDBCQUFPQyxHQUFWLGlvQkFZTEgsa0JBQWtCLEdBQUcsRUFaaEIsRUFlQTtBQUFBLE1BQUdXLFNBQUgsU0FBR0EsU0FBSDtBQUFBLFNBQW9CQSxTQUFTLEdBQUcsU0FBSCxHQUFlLFFBQTVDO0FBQUEsQ0FmQSxFQTRCWjtBQUFBLE1BQUdBLFNBQUgsU0FBR0EsU0FBSDtBQUFBLFNBQ0VBLFNBQVMsd0dBRFg7QUFBQSxDQTVCWSxDQUFsQjs7QUFrQ0EsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixRQU1qQjtBQUFBLE1BTEZDLHFCQUtFLFNBTEZBLHFCQUtFO0FBQUEsTUFKRkMsd0JBSUUsU0FKRkEsd0JBSUU7QUFBQSx3QkFIRkMsR0FHRTtBQUFBLE1BSEZBLEdBR0UsMEJBSEksRUFHSjtBQUFBLE1BRkZDLHFCQUVFLFNBRkZBLHFCQUVFO0FBQUEsaUNBREZDLFlBQ0U7QUFBQSxNQURGQSxZQUNFLG1DQURhLElBQ2I7O0FBQ0Ysa0JBQWtDLHFCQUM5QixvQkFBTUMsWUFBWSxDQUFDQyxPQUFiLENBQXFCSixHQUFHLEdBQUcsa0JBQTNCLENBQU4sS0FDSUcsWUFBWSxDQUFDQyxPQUFiLENBQXFCSixHQUFHLEdBQUcsa0JBQTNCLE1BQW1ELE1BRnpCLENBQWxDO0FBQUE7QUFBQSxNQUFPSixTQUFQO0FBQUEsTUFBa0JTLFlBQWxCOztBQUtBLE1BQU1DLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsS0FBRCxFQUFXO0FBQ2xDSixJQUFBQSxZQUFZLENBQUNLLE9BQWIsQ0FBcUJSLEdBQUcsR0FBRyxrQkFBM0IsRUFBK0NPLEtBQS9DO0FBQ0FGLElBQUFBLFlBQVksQ0FBQ0UsS0FBRCxDQUFaO0FBQ0gsR0FIRDs7QUFLQSxzQkFDSSxzQkFBQyxTQUFEO0FBQVcsSUFBQSxRQUFRLEVBQUVOLHFCQUFyQjtBQUFBLDRCQUNJLHFCQUFDLHFCQUFEO0FBQ0ksTUFBQSxTQUFTLEVBQUUsQ0FBQ0wsU0FBRCxJQUFjLFdBRDdCO0FBRUksTUFBQSxPQUFPLEVBQUU7QUFBQSxlQUFNVSxrQkFBa0IsQ0FBQyxJQUFELENBQXhCO0FBQUEsT0FGYjtBQUFBLDZCQUlJLHFCQUFDLFVBQUQ7QUFBWSxRQUFBLFNBQVMsRUFBQztBQUF0QjtBQUpKLE1BREosZUFRSSxxQkFBQyxPQUFEO0FBQVMsTUFBQSxTQUFTLEVBQUUsQ0FBQ1YsU0FBRCxJQUFjLE1BQWxDO0FBQUEsNkJBQ0ksc0JBQUMsYUFBRDtBQUFBLGdDQUNJLHFCQUFDLHVCQUFEO0FBQWdCLFVBQUEsWUFBWSxFQUFFTTtBQUE5QixVQURKLGVBR0kscUJBQUMsbUJBQUQ7QUFDSSxVQUFBLHFCQUFxQixFQUFFSixxQkFEM0I7QUFFSSxVQUFBLHdCQUF3QixFQUFFQztBQUY5QixVQUhKO0FBQUE7QUFESixNQVJKLGVBbUJJLHFCQUFDLFlBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBQyxlQURkO0FBRUksTUFBQSxTQUFTLEVBQUVILFNBRmY7QUFHSSxNQUFBLE9BQU8sRUFBRTtBQUFBLGVBQU1VLGtCQUFrQixDQUFDLEtBQUQsQ0FBeEI7QUFBQSxPQUhiO0FBQUEsNkJBS0k7QUFBRyxRQUFBLFNBQVMsRUFBQztBQUFiO0FBTEosTUFuQko7QUFBQSxJQURKO0FBNkJILENBOUNEOztBQWdEQSxJQUFNRyxlQUFlLEdBQUcsd0NBQXlCO0FBQzdDUixFQUFBQSxxQkFBcUIsRUFBRVMscUJBQXFCLENBQUNDO0FBREEsQ0FBekIsQ0FBeEI7QUFJQWQsY0FBYyxDQUFDZSxTQUFmLEdBQTJCO0FBQ3ZCZCxFQUFBQSxxQkFBcUIsRUFBRWUsbUJBQVVDLElBRFY7QUFFdkJmLEVBQUFBLHdCQUF3QixFQUFFYyxtQkFBVUMsSUFGYjtBQUd2QmQsRUFBQUEsR0FBRyxFQUFFYSxtQkFBVUUsTUFIUTtBQUl2QmQsRUFBQUEscUJBQXFCLEVBQUVZLG1CQUFVQyxJQUpWO0FBS3ZCWixFQUFBQSxZQUFZLEVBQUVXLG1CQUFVQztBQUxELENBQTNCOztlQVFlLHlCQUFRTCxlQUFSLEVBQXlCLElBQXpCLEVBQStCWixjQUEvQixDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGlzTmlsIGZyb20gJ2xvZGFzaC9pc05pbCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IHsgY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBQYWdlUHJvcGVydGllcyBmcm9tICdAbW9kdWxlcy9QYWdlUHJvcGVydGllcyc7XG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMgZnJvbSAnQG1vZHVsZXMvVGVtcGxhdGVWaWV3L3NlbGVjdG9ycyc7XG5pbXBvcnQgV2lkZ2V0TWVudSBmcm9tICdAbW9kdWxlcy9XaWRnZXRNZW51JztcblxuY29uc3QgV0lER0VUU19TSURFX1dJRFRIID0gMzMyO1xuXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNlZmYxZmI7XG4gICAgYm94LXNoYWRvdzogMCAycHggMTJweCAwIHJnYmEoMTI5LCAxNTgsIDIwMCwgMC4wNik7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHotaW5kZXg6IDU1O1xuICAgICR7KHsgZGlzYWJsZWQgfSkgPT5cbiAgICAgICAgZGlzYWJsZWQgJiZcbiAgICAgICAgYG9wYWNpdHk6IDAuNjtcbiAgICBjdXJzb3I6IG5vdC1hbGxvd2VkO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO2B9XG5cbiAgICAmOmhvdmVyIC5yZXNpemUtYnV0dG9uIHtcbiAgICAgICAgb3BhY2l0eTogMTtcbiAgICB9XG5gO1xuXG5jb25zdCBNZW51Q29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDEycHggMCByZ2JhKDEyOSwgMTU4LCAyMDAsIDAuMDYpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgbWluLXdpZHRoOiAke1dJREdFVFNfU0lERV9XSURUSH1weDtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIHdpZHRoOiAke1dJREdFVFNfU0lERV9XSURUSH1weDtcbmA7XG5cbmNvbnN0IFdyYXBwZXIgPSBzdHlsZWQuZGl2YFxuICAgIG1heC13aWR0aDogJHtXSURHRVRTX1NJREVfV0lEVEh9cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0cmFuc2l0aW9uOiBtYXgtd2lkdGggMC4zcyBsaW5lYXI7XG4gICAgd2lkdGg6ICR7V0lER0VUU19TSURFX1dJRFRIfXB4O1xuXG4gICAgJi5oaWRlIHtcbiAgICAgICAgbWF4LXdpZHRoOiAwO1xuICAgIH1cbmA7XG5cbmNvbnN0IFRvZ2dsZUJ1dHRvbkNvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3gtc2hhZG93OiAxMHB4IDEycHggMTdweCAwIHJnYmEoMTI5LCAxNTgsIDIwMCwgMC4wNik7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIHRyYW5zaXRpb246IHdpZHRoIDAuMnMgZWFzZS1pbiwgYmFja2dyb3VuZC1jb2xvciAwLjJzIGxpbmVhcjtcbiAgICB3aWR0aDogMDtcblxuICAgICY6aG92ZXIge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMDYpO1xuICAgIH1cblxuICAgICYuaGlkZS1tb2RlIHtcbiAgICAgICAgdHJhbnNpdGlvbi1kZWxheTogMC4yNXM7XG4gICAgICAgIHdpZHRoOiAyNnB4O1xuICAgIH1cbmA7XG5cbmNvbnN0IFN0eWxlZEljb24gPSBzdHlsZWQuaWBcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBmb250LXNpemU6IDIwcHg7XG5gO1xuXG5jb25zdCBSZXNpemVCdXR0b24gPSBzdHlsZWQuZGl2YFxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJveC1zaGFkb3c6IHJnYmEoOSwgMzAsIDY2LCAwLjA4KSAwcHggMHB4IDBweCAxcHgsXG4gICAgICAgIHJnYmEoOSwgMzAsIDY2LCAwLjA4KSAwcHggMnB4IDRweCAxcHg7XG4gICAgY29sb3I6ICNmZjlhMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBoZWlnaHQ6IDI0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDI0cHg7XG4gICAgb3BhY2l0eTogMDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6ICR7V0lER0VUU19TSURFX1dJRFRIIC0gMTJ9cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRvcDogNzVweDtcbiAgICB2aXNpYmlsaXR5OiAkeyh7IGlzU2hvd2luZyB9KSA9PiAoaXNTaG93aW5nID8gJ3Zpc2libGUnIDogJ2hpZGRlbicpfTtcbiAgICB3aWR0aDogMjRweDtcbiAgICB6LWluZGV4OiA1NTtcblxuICAgIGkge1xuICAgICAgICBtYXJnaW4tbGVmdDogMXB4O1xuICAgIH1cblxuICAgICY6aG92ZXIge1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmOWEwMDtcbiAgICB9XG5cbiAgICAkeyh7IGlzU2hvd2luZyB9KSA9PlxuICAgICAgICBpc1Nob3dpbmcgJiZcbiAgICAgICAgYHRyYW5zaXRpb246IGJhY2tncm91bmQtY29sb3IgMTAwbXMsIGNvbG9yIDEwMG1zLFxuICAgICAgICB2aXNpYmlsaXR5IDBzIGxpbmVhciAwLjVzLCBvcGFjaXR5IDAuMnM7YH1cbmA7XG5cbmNvbnN0IFdpZGdldFNpZGVNZW51ID0gKHtcbiAgICBjYW5VcGRhdGVQYW5lbHNTb3dpbmcsXG4gICAgY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uLFxuICAgIGtleSA9ICcnLFxuICAgIHNlbGVjdGVkUGFnZUlzQmxvY2tlZCxcbiAgICB0aGVtZUVuYWJsZWQgPSB0cnVlLFxufSkgPT4ge1xuICAgIGNvbnN0IFtpc1Nob3dpbmcsIHNldElzU2hvd2luZ10gPSB1c2VTdGF0ZShcbiAgICAgICAgaXNOaWwobG9jYWxTdG9yYWdlLmdldEl0ZW0oa2V5ICsgJzpyaWdodFNpZGVTaG93ZWQnKSkgfHxcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKGtleSArICc6cmlnaHRTaWRlU2hvd2VkJykgPT09ICd0cnVlJ1xuICAgICk7XG5cbiAgICBjb25zdCBoYW5kbGVTZXRJc1Nob3dpbmcgPSAodmFsdWUpID0+IHtcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oa2V5ICsgJzpyaWdodFNpZGVTaG93ZWQnLCB2YWx1ZSk7XG4gICAgICAgIHNldElzU2hvd2luZyh2YWx1ZSk7XG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxDb250YWluZXIgZGlzYWJsZWQ9e3NlbGVjdGVkUGFnZUlzQmxvY2tlZH0+XG4gICAgICAgICAgICA8VG9nZ2xlQnV0dG9uQ29udGFpbmVyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXshaXNTaG93aW5nICYmICdoaWRlLW1vZGUnfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IGhhbmRsZVNldElzU2hvd2luZyh0cnVlKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8U3R5bGVkSWNvbiBjbGFzc05hbWU9XCJmYSBmYS1jaGV2cm9uLWxlZnRcIiAvPlxuICAgICAgICAgICAgPC9Ub2dnbGVCdXR0b25Db250YWluZXI+XG5cbiAgICAgICAgICAgIDxXcmFwcGVyIGNsYXNzTmFtZT17IWlzU2hvd2luZyAmJiAnaGlkZSd9PlxuICAgICAgICAgICAgICAgIDxNZW51Q29udGFpbmVyPlxuICAgICAgICAgICAgICAgICAgICA8UGFnZVByb3BlcnRpZXMgdGhlbWVFbmFibGVkPXt0aGVtZUVuYWJsZWR9IC8+XG5cbiAgICAgICAgICAgICAgICAgICAgPFdpZGdldE1lbnVcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhblVwZGF0ZVBhbmVsc1Nvd2luZz17Y2FuVXBkYXRlUGFuZWxzU293aW5nfVxuICAgICAgICAgICAgICAgICAgICAgICAgY2FuVXBkYXRlUHJvamVjdExvY2F0aW9uPXtjYW5VcGRhdGVQcm9qZWN0TG9jYXRpb259XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9NZW51Q29udGFpbmVyPlxuICAgICAgICAgICAgPC9XcmFwcGVyPlxuXG4gICAgICAgICAgICA8UmVzaXplQnV0dG9uXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicmVzaXplLWJ1dHRvblwiXG4gICAgICAgICAgICAgICAgaXNTaG93aW5nPXtpc1Nob3dpbmd9XG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gaGFuZGxlU2V0SXNTaG93aW5nKGZhbHNlKX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtY2hldnJvbi1yaWdodFwiIC8+XG4gICAgICAgICAgICA8L1Jlc2l6ZUJ1dHRvbj5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3Rvcih7XG4gICAgc2VsZWN0ZWRQYWdlSXNCbG9ja2VkOiB0ZW1wbGF0ZVZpZXdTZWxlY3RvcnMuZ2V0U2VsZWN0ZWRQYWdlSXNCbG9ja2VkLFxufSk7XG5cbldpZGdldFNpZGVNZW51LnByb3BUeXBlcyA9IHtcbiAgICBjYW5VcGRhdGVQYW5lbHNTb3dpbmc6IFByb3BUeXBlcy5ib29sLFxuICAgIGNhblVwZGF0ZVByb2plY3RMb2NhdGlvbjogUHJvcFR5cGVzLmJvb2wsXG4gICAga2V5OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHNlbGVjdGVkUGFnZUlzQmxvY2tlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgdGhlbWVFbmFibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBudWxsKShXaWRnZXRTaWRlTWVudSk7XG4iXX0=