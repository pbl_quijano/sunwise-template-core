"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = _styledComponents.default.h6(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: ", ";\n    font-size: ", "px;\n    font-weight: 600;\n    line-height: 19px;\n    margin-bottom: 0;\n    padding: 0 15px;\n    position: relative;\n\n    &:before {\n        background-color: ", ";\n        content: '';\n        display: block;\n        height: 14px;\n        left: 0;\n        margin-top: -7px;\n        position: absolute;\n        top: 50%;\n        width: 2px;\n    }\n"])), function (_ref) {
  var _ref$color = _ref.color,
      color = _ref$color === void 0 ? '#202253' : _ref$color;
  return color;
}, function (_ref2) {
  var _ref2$fontSize = _ref2.fontSize,
      fontSize = _ref2$fontSize === void 0 ? '14' : _ref2$fontSize;
  return fontSize;
}, function (_ref3) {
  var _ref3$indicatorColor = _ref3.indicatorColor,
      indicatorColor = _ref3$indicatorColor === void 0 ? '#2f4dff' : _ref3$indicatorColor;
  return indicatorColor;
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1RpdGxlV2l0aERldGFpbC5qcyJdLCJuYW1lcyI6WyJzdHlsZWQiLCJoNiIsImNvbG9yIiwiZm9udFNpemUiLCJpbmRpY2F0b3JDb2xvciJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7OztlQUVlQSwwQkFBT0MsRSx5ZEFDVDtBQUFBLHdCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCwyQkFBVyxTQUFYO0FBQUEsU0FBMkJBLEtBQTNCO0FBQUEsQyxFQUNJO0FBQUEsNkJBQUdDLFFBQUg7QUFBQSxNQUFHQSxRQUFILCtCQUFjLElBQWQ7QUFBQSxTQUF5QkEsUUFBekI7QUFBQSxDLEVBUVc7QUFBQSxtQ0FBR0MsY0FBSDtBQUFBLE1BQUdBLGNBQUgscUNBQW9CLFNBQXBCO0FBQUEsU0FBb0NBLGNBQXBDO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5leHBvcnQgZGVmYXVsdCBzdHlsZWQuaDZgXG4gICAgY29sb3I6ICR7KHsgY29sb3IgPSAnIzIwMjI1MycgfSkgPT4gY29sb3J9O1xuICAgIGZvbnQtc2l6ZTogJHsoeyBmb250U2l6ZSA9ICcxNCcgfSkgPT4gZm9udFNpemV9cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBsaW5lLWhlaWdodDogMTlweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHBhZGRpbmc6IDAgMTVweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG5cbiAgICAmOmJlZm9yZSB7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICR7KHsgaW5kaWNhdG9yQ29sb3IgPSAnIzJmNGRmZicgfSkgPT4gaW5kaWNhdG9yQ29sb3J9O1xuICAgICAgICBjb250ZW50OiAnJztcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGhlaWdodDogMTRweDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgbWFyZ2luLXRvcDogLTdweDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgd2lkdGg6IDJweDtcbiAgICB9XG5gO1xuIl19