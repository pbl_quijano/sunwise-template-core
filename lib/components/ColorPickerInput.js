"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactColor = require("react-color");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4, _templateObject5, _templateObject6, _templateObject7;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Color = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background-color: ", ";\n    border-radius: 4px;\n    height: 32px;\n    width: 32px;\n"])), function (_ref) {
  var color = _ref.color;
  return color && color;
});

var ColorPickerWrapper = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    align-items: center;\n    background-color: rgba(255, 255, 255, 0.02);\n    border-radius: 3px;\n    border: 1px solid #eff1fb;\n    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);\n    display: flex;\n    max-width: 132px;\n    min-width: 132px;\n    padding: 6px 6px;\n    position: relative;\n    width: 132px;\n"])));

var Container = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    font-size: 13px;\n"])));

var Label = _styledComponents.default.span(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    color: #848bab;\n    font-size: 13px;\n    line-height: 16px;\n    margin-right: 23px;\n    min-height: 16px;\n    text-align: right;\n    width: 135px;\n"])));

var Swatch = _styledComponents.default.div(_templateObject5 || (_templateObject5 = _taggedTemplateLiteral(["\n    background: #fff;\n    border-radius: 5px;\n    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1);\n    cursor: pointer;\n    display: inline-block;\n    margin-right: 10px;\n"])));

var Popover = _styledComponents.default.div(_templateObject6 || (_templateObject6 = _taggedTemplateLiteral(["\n    position: absolute;\n    right: 0;\n    top: 0;\n    z-index: 2;\n"])));

var CloseButton = _styledComponents.default.div(_templateObject7 || (_templateObject7 = _taggedTemplateLiteral(["\n    background-color: rgba(0, 0, 0, 0.8);\n    border-radius: 50%;\n    color: #fff;\n    cursor: pointer;\n    font-size: 12px;\n    height: 20px;\n    line-height: 20px;\n    position: absolute;\n    right: -24px;\n    text-align: center;\n    top: 0;\n    width: 20px;\n"])));

var ColorPickerInput = function ColorPickerInput(_ref2) {
  var _ref2$hasAlphaChanel = _ref2.hasAlphaChanel,
      hasAlphaChanel = _ref2$hasAlphaChanel === void 0 ? false : _ref2$hasAlphaChanel,
      label = _ref2.label,
      onChange = _ref2.onChange,
      value = _ref2.value;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      displayColorPicker = _useState2[0],
      setDisplayColorPicker = _useState2[1];

  var color = value || (hasAlphaChanel ? '#000000ff' : '#000000');

  var _useState3 = (0, _react.useState)(color),
      _useState4 = _slicedToArray(_useState3, 2),
      currentColor = _useState4[0],
      setCurrentColor = _useState4[1];

  var completeChange = function completeChange(tempColor) {
    var colorHex = hasAlphaChanel ? "".concat(tempColor.hex).concat(parseInt(tempColor.rgb.a * 255).toString(16)) : tempColor.hex;
    onChange(colorHex);
  };

  var handleChange = function handleChange(tempColor) {
    var colorHex = hasAlphaChanel ? "".concat(tempColor.hex).concat(parseInt(tempColor.rgb.a * 255).toString(16)) : tempColor.hex;
    setCurrentColor(colorHex);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(Container, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Label, {
      children: label
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(ColorPickerWrapper, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(Swatch, {
        onClick: function onClick() {
          return setDisplayColorPicker(!displayColorPicker);
        },
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Color, {
          color: currentColor
        })
      }), displayColorPicker && /*#__PURE__*/(0, _jsxRuntime.jsxs)(Popover, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(CloseButton, {
          onClick: function onClick() {
            return setDisplayColorPicker(false);
          },
          children: "x"
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactColor.ChromePicker, {
          color: currentColor,
          onChange: handleChange,
          onChangeComplete: completeChange
        })]
      }), currentColor]
    })]
  });
};

ColorPickerInput.propTypes = {
  hasAlphaChanel: _propTypes.default.bool,
  label: _propTypes.default.string,
  onChange: _propTypes.default.func,
  value: _propTypes.default.string
};
var _default = ColorPickerInput;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0NvbG9yUGlja2VySW5wdXQuanMiXSwibmFtZXMiOlsiQ29sb3IiLCJzdHlsZWQiLCJkaXYiLCJjb2xvciIsIkNvbG9yUGlja2VyV3JhcHBlciIsIkNvbnRhaW5lciIsIkxhYmVsIiwic3BhbiIsIlN3YXRjaCIsIlBvcG92ZXIiLCJDbG9zZUJ1dHRvbiIsIkNvbG9yUGlja2VySW5wdXQiLCJoYXNBbHBoYUNoYW5lbCIsImxhYmVsIiwib25DaGFuZ2UiLCJ2YWx1ZSIsImRpc3BsYXlDb2xvclBpY2tlciIsInNldERpc3BsYXlDb2xvclBpY2tlciIsImN1cnJlbnRDb2xvciIsInNldEN1cnJlbnRDb2xvciIsImNvbXBsZXRlQ2hhbmdlIiwidGVtcENvbG9yIiwiY29sb3JIZXgiLCJoZXgiLCJwYXJzZUludCIsInJnYiIsImEiLCJ0b1N0cmluZyIsImhhbmRsZUNoYW5nZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsImJvb2wiLCJzdHJpbmciLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxLQUFLLEdBQUdDLDBCQUFPQyxHQUFWLG1LQUNhO0FBQUEsTUFBR0MsS0FBSCxRQUFHQSxLQUFIO0FBQUEsU0FBZUEsS0FBSyxJQUFJQSxLQUF4QjtBQUFBLENBRGIsQ0FBWDs7QUFPQSxJQUFNQyxrQkFBa0IsR0FBR0gsMEJBQU9DLEdBQVYsNFlBQXhCOztBQWNBLElBQU1HLFNBQVMsR0FBR0osMEJBQU9DLEdBQVYsNklBQWY7O0FBTUEsSUFBTUksS0FBSyxHQUFHTCwwQkFBT00sSUFBVix1T0FBWDs7QUFVQSxJQUFNQyxNQUFNLEdBQUdQLDBCQUFPQyxHQUFWLG1QQUFaOztBQVNBLElBQU1PLE9BQU8sR0FBR1IsMEJBQU9DLEdBQVYsK0lBQWI7O0FBT0EsSUFBTVEsV0FBVyxHQUFHVCwwQkFBT0MsR0FBViwwVkFBakI7O0FBZUEsSUFBTVMsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixRQUtuQjtBQUFBLG1DQUpGQyxjQUlFO0FBQUEsTUFKRkEsY0FJRSxxQ0FKZSxLQUlmO0FBQUEsTUFIRkMsS0FHRSxTQUhGQSxLQUdFO0FBQUEsTUFGRkMsUUFFRSxTQUZGQSxRQUVFO0FBQUEsTUFERkMsS0FDRSxTQURGQSxLQUNFOztBQUNGLGtCQUFvRCxxQkFBUyxLQUFULENBQXBEO0FBQUE7QUFBQSxNQUFPQyxrQkFBUDtBQUFBLE1BQTJCQyxxQkFBM0I7O0FBQ0EsTUFBTWQsS0FBSyxHQUFHWSxLQUFLLEtBQUtILGNBQWMsR0FBRyxXQUFILEdBQWlCLFNBQXBDLENBQW5COztBQUNBLG1CQUF3QyxxQkFBU1QsS0FBVCxDQUF4QztBQUFBO0FBQUEsTUFBT2UsWUFBUDtBQUFBLE1BQXFCQyxlQUFyQjs7QUFFQSxNQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLFNBQUQsRUFBZTtBQUNsQyxRQUFNQyxRQUFRLEdBQUdWLGNBQWMsYUFDdEJTLFNBQVMsQ0FBQ0UsR0FEWSxTQUNOQyxRQUFRLENBQUNILFNBQVMsQ0FBQ0ksR0FBVixDQUFjQyxDQUFkLEdBQWtCLEdBQW5CLENBQVIsQ0FBZ0NDLFFBQWhDLENBQXlDLEVBQXpDLENBRE0sSUFFekJOLFNBQVMsQ0FBQ0UsR0FGaEI7QUFHQVQsSUFBQUEsUUFBUSxDQUFDUSxRQUFELENBQVI7QUFDSCxHQUxEOztBQU9BLE1BQU1NLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNQLFNBQUQsRUFBZTtBQUNoQyxRQUFNQyxRQUFRLEdBQUdWLGNBQWMsYUFDdEJTLFNBQVMsQ0FBQ0UsR0FEWSxTQUNOQyxRQUFRLENBQUNILFNBQVMsQ0FBQ0ksR0FBVixDQUFjQyxDQUFkLEdBQWtCLEdBQW5CLENBQVIsQ0FBZ0NDLFFBQWhDLENBQXlDLEVBQXpDLENBRE0sSUFFekJOLFNBQVMsQ0FBQ0UsR0FGaEI7QUFHQUosSUFBQUEsZUFBZSxDQUFDRyxRQUFELENBQWY7QUFDSCxHQUxEOztBQU9BLHNCQUNJLHNCQUFDLFNBQUQ7QUFBQSw0QkFDSSxxQkFBQyxLQUFEO0FBQUEsZ0JBQVFUO0FBQVIsTUFESixlQUdJLHNCQUFDLGtCQUFEO0FBQUEsOEJBQ0kscUJBQUMsTUFBRDtBQUNJLFFBQUEsT0FBTyxFQUFFO0FBQUEsaUJBQU1JLHFCQUFxQixDQUFDLENBQUNELGtCQUFGLENBQTNCO0FBQUEsU0FEYjtBQUFBLCtCQUdJLHFCQUFDLEtBQUQ7QUFBTyxVQUFBLEtBQUssRUFBRUU7QUFBZDtBQUhKLFFBREosRUFPS0Ysa0JBQWtCLGlCQUNmLHNCQUFDLE9BQUQ7QUFBQSxnQ0FDSSxxQkFBQyxXQUFEO0FBQ0ksVUFBQSxPQUFPLEVBQUU7QUFBQSxtQkFBTUMscUJBQXFCLENBQUMsS0FBRCxDQUEzQjtBQUFBLFdBRGI7QUFBQTtBQUFBLFVBREosZUFNSSxxQkFBQyx3QkFBRDtBQUNJLFVBQUEsS0FBSyxFQUFFQyxZQURYO0FBRUksVUFBQSxRQUFRLEVBQUVVLFlBRmQ7QUFHSSxVQUFBLGdCQUFnQixFQUFFUjtBQUh0QixVQU5KO0FBQUEsUUFSUixFQXFCS0YsWUFyQkw7QUFBQSxNQUhKO0FBQUEsSUFESjtBQTZCSCxDQXJERDs7QUF1REFQLGdCQUFnQixDQUFDa0IsU0FBakIsR0FBNkI7QUFDekJqQixFQUFBQSxjQUFjLEVBQUVrQixtQkFBVUMsSUFERDtBQUV6QmxCLEVBQUFBLEtBQUssRUFBRWlCLG1CQUFVRSxNQUZRO0FBR3pCbEIsRUFBQUEsUUFBUSxFQUFFZ0IsbUJBQVVHLElBSEs7QUFJekJsQixFQUFBQSxLQUFLLEVBQUVlLG1CQUFVRTtBQUpRLENBQTdCO2VBT2VyQixnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IENocm9tZVBpY2tlciB9IGZyb20gJ3JlYWN0LWNvbG9yJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBDb2xvciA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBjb2xvciB9KSA9PiBjb2xvciAmJiBjb2xvcn07XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGhlaWdodDogMzJweDtcbiAgICB3aWR0aDogMzJweDtcbmA7XG5cbmNvbnN0IENvbG9yUGlja2VyV3JhcHBlciA9IHN0eWxlZC5kaXZgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMDIpO1xuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWZmMWZiO1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDEycHggMCByZ2JhKDEyOSwgMTU4LCAyMDAsIDAuMDYpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWF4LXdpZHRoOiAxMzJweDtcbiAgICBtaW4td2lkdGg6IDEzMnB4O1xuICAgIHBhZGRpbmc6IDZweCA2cHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOiAxMzJweDtcbmA7XG5cbmNvbnN0IENvbnRhaW5lciA9IHN0eWxlZC5kaXZgXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbmA7XG5cbmNvbnN0IExhYmVsID0gc3R5bGVkLnNwYW5gXG4gICAgY29sb3I6ICM4NDhiYWI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNnB4O1xuICAgIG1hcmdpbi1yaWdodDogMjNweDtcbiAgICBtaW4taGVpZ2h0OiAxNnB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHdpZHRoOiAxMzVweDtcbmA7XG5cbmNvbnN0IFN3YXRjaCA9IHN0eWxlZC5kaXZgXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYm94LXNoYWRvdzogMCAwIDAgMXB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbmA7XG5cbmNvbnN0IFBvcG92ZXIgPSBzdHlsZWQuZGl2YFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMDtcbiAgICB0b3A6IDA7XG4gICAgei1pbmRleDogMjtcbmA7XG5cbmNvbnN0IENsb3NlQnV0dG9uID0gc3R5bGVkLmRpdmBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTI0cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogMjBweDtcbmA7XG5cbmNvbnN0IENvbG9yUGlja2VySW5wdXQgPSAoe1xuICAgIGhhc0FscGhhQ2hhbmVsID0gZmFsc2UsXG4gICAgbGFiZWwsXG4gICAgb25DaGFuZ2UsXG4gICAgdmFsdWUsXG59KSA9PiB7XG4gICAgY29uc3QgW2Rpc3BsYXlDb2xvclBpY2tlciwgc2V0RGlzcGxheUNvbG9yUGlja2VyXSA9IHVzZVN0YXRlKGZhbHNlKTtcbiAgICBjb25zdCBjb2xvciA9IHZhbHVlIHx8IChoYXNBbHBoYUNoYW5lbCA/ICcjMDAwMDAwZmYnIDogJyMwMDAwMDAnKTtcbiAgICBjb25zdCBbY3VycmVudENvbG9yLCBzZXRDdXJyZW50Q29sb3JdID0gdXNlU3RhdGUoY29sb3IpO1xuXG4gICAgY29uc3QgY29tcGxldGVDaGFuZ2UgPSAodGVtcENvbG9yKSA9PiB7XG4gICAgICAgIGNvbnN0IGNvbG9ySGV4ID0gaGFzQWxwaGFDaGFuZWxcbiAgICAgICAgICAgID8gYCR7dGVtcENvbG9yLmhleH0ke3BhcnNlSW50KHRlbXBDb2xvci5yZ2IuYSAqIDI1NSkudG9TdHJpbmcoMTYpfWBcbiAgICAgICAgICAgIDogdGVtcENvbG9yLmhleDtcbiAgICAgICAgb25DaGFuZ2UoY29sb3JIZXgpO1xuICAgIH07XG5cbiAgICBjb25zdCBoYW5kbGVDaGFuZ2UgPSAodGVtcENvbG9yKSA9PiB7XG4gICAgICAgIGNvbnN0IGNvbG9ySGV4ID0gaGFzQWxwaGFDaGFuZWxcbiAgICAgICAgICAgID8gYCR7dGVtcENvbG9yLmhleH0ke3BhcnNlSW50KHRlbXBDb2xvci5yZ2IuYSAqIDI1NSkudG9TdHJpbmcoMTYpfWBcbiAgICAgICAgICAgIDogdGVtcENvbG9yLmhleDtcbiAgICAgICAgc2V0Q3VycmVudENvbG9yKGNvbG9ySGV4KTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPENvbnRhaW5lcj5cbiAgICAgICAgICAgIDxMYWJlbD57bGFiZWx9PC9MYWJlbD5cblxuICAgICAgICAgICAgPENvbG9yUGlja2VyV3JhcHBlcj5cbiAgICAgICAgICAgICAgICA8U3dhdGNoXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHNldERpc3BsYXlDb2xvclBpY2tlcighZGlzcGxheUNvbG9yUGlja2VyKX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxDb2xvciBjb2xvcj17Y3VycmVudENvbG9yfSAvPlxuICAgICAgICAgICAgICAgIDwvU3dhdGNoPlxuXG4gICAgICAgICAgICAgICAge2Rpc3BsYXlDb2xvclBpY2tlciAmJiAoXG4gICAgICAgICAgICAgICAgICAgIDxQb3BvdmVyPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENsb3NlQnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gc2V0RGlzcGxheUNvbG9yUGlja2VyKGZhbHNlKX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB4XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0Nsb3NlQnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICAgICAgPENocm9tZVBpY2tlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPXtjdXJyZW50Q29sb3J9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2hhbmRsZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUNvbXBsZXRlPXtjb21wbGV0ZUNoYW5nZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvUG9wb3Zlcj5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIHtjdXJyZW50Q29sb3J9XG4gICAgICAgICAgICA8L0NvbG9yUGlja2VyV3JhcHBlcj5cbiAgICAgICAgPC9Db250YWluZXI+XG4gICAgKTtcbn07XG5cbkNvbG9yUGlja2VySW5wdXQucHJvcFR5cGVzID0ge1xuICAgIGhhc0FscGhhQ2hhbmVsOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBsYWJlbDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgdmFsdWU6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDb2xvclBpY2tlcklucHV0O1xuIl19