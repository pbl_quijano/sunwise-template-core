"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Spacer = exports.FlexRow = exports.FlexColumn = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: ", ";\n    flex-direction: row;\n    flex-wrap: ", ";\n    ", "\n    ", "\n    ", "\n    ", "\n"])), function (_ref) {
  var _ref$isVisible = _ref.isVisible,
      isVisible = _ref$isVisible === void 0 ? true : _ref$isVisible;
  return isVisible ? 'flex' : 'none';
}, function (_ref2) {
  var _ref2$wrap = _ref2.wrap,
      wrap = _ref2$wrap === void 0 ? 'nowrap' : _ref2$wrap;
  return wrap;
}, function (_ref3) {
  var padding = _ref3.padding;
  return padding && "padding: ".concat(padding, ";");
}, function (_ref4) {
  var margin = _ref4.margin;
  return margin && "margin: ".concat(margin, ";");
}, function (_ref5) {
  var alignItems = _ref5.alignItems;
  return alignItems && "align-items: ".concat(alignItems, ";");
}, function (_ref6) {
  var justifyContent = _ref6.justifyContent;
  return justifyContent && "justify-content: ".concat(justifyContent, ";");
});

var FlexRow = (0, _styledComponents.default)(Container)(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    flex-direction: row;\n"])));
exports.FlexRow = FlexRow;
var FlexColumn = (0, _styledComponents.default)(Container)(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    flex-direction: column;\n"])));
exports.FlexColumn = FlexColumn;

var Spacer = _styledComponents.default.div(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    flex-basis: 0;\n    flex-grow: 1;\n"])));

exports.Spacer = Spacer;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL3N0eWxlZC5qcyJdLCJuYW1lcyI6WyJDb250YWluZXIiLCJzdHlsZWQiLCJkaXYiLCJpc1Zpc2libGUiLCJ3cmFwIiwicGFkZGluZyIsIm1hcmdpbiIsImFsaWduSXRlbXMiLCJqdXN0aWZ5Q29udGVudCIsIkZsZXhSb3ciLCJGbGV4Q29sdW1uIiwiU3BhY2VyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7Ozs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxHQUFHQywwQkFBT0MsR0FBVixvTEFDQTtBQUFBLDRCQUFHQyxTQUFIO0FBQUEsTUFBR0EsU0FBSCwrQkFBZSxJQUFmO0FBQUEsU0FBMkJBLFNBQVMsR0FBRyxNQUFILEdBQVksTUFBaEQ7QUFBQSxDQURBLEVBR0U7QUFBQSx5QkFBR0MsSUFBSDtBQUFBLE1BQUdBLElBQUgsMkJBQVUsUUFBVjtBQUFBLFNBQXlCQSxJQUF6QjtBQUFBLENBSEYsRUFJVDtBQUFBLE1BQUdDLE9BQUgsU0FBR0EsT0FBSDtBQUFBLFNBQWlCQSxPQUFPLHVCQUFnQkEsT0FBaEIsTUFBeEI7QUFBQSxDQUpTLEVBS1Q7QUFBQSxNQUFHQyxNQUFILFNBQUdBLE1BQUg7QUFBQSxTQUFnQkEsTUFBTSxzQkFBZUEsTUFBZixNQUF0QjtBQUFBLENBTFMsRUFNVDtBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLFNBQW9CQSxVQUFVLDJCQUFvQkEsVUFBcEIsTUFBOUI7QUFBQSxDQU5TLEVBT1Q7QUFBQSxNQUFHQyxjQUFILFNBQUdBLGNBQUg7QUFBQSxTQUNFQSxjQUFjLCtCQUF3QkEsY0FBeEIsTUFEaEI7QUFBQSxDQVBTLENBQWY7O0FBV08sSUFBTUMsT0FBTyxHQUFHLCtCQUFPVCxTQUFQLENBQUgsbUdBQWI7O0FBSUEsSUFBTVUsVUFBVSxHQUFHLCtCQUFPVixTQUFQLENBQUgsc0dBQWhCOzs7QUFJQSxJQUFNVyxNQUFNLEdBQUdWLDBCQUFPQyxHQUFWLGdIQUFaIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XHJcblxyXG5jb25zdCBDb250YWluZXIgPSBzdHlsZWQuZGl2YFxyXG4gICAgZGlzcGxheTogJHsoeyBpc1Zpc2libGUgPSB0cnVlIH0pID0+IChpc1Zpc2libGUgPyAnZmxleCcgOiAnbm9uZScpfTtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBmbGV4LXdyYXA6ICR7KHsgd3JhcCA9ICdub3dyYXAnIH0pID0+IHdyYXB9O1xyXG4gICAgJHsoeyBwYWRkaW5nIH0pID0+IHBhZGRpbmcgJiYgYHBhZGRpbmc6ICR7cGFkZGluZ307YH1cclxuICAgICR7KHsgbWFyZ2luIH0pID0+IG1hcmdpbiAmJiBgbWFyZ2luOiAke21hcmdpbn07YH1cclxuICAgICR7KHsgYWxpZ25JdGVtcyB9KSA9PiBhbGlnbkl0ZW1zICYmIGBhbGlnbi1pdGVtczogJHthbGlnbkl0ZW1zfTtgfVxyXG4gICAgJHsoeyBqdXN0aWZ5Q29udGVudCB9KSA9PlxyXG4gICAgICAgIGp1c3RpZnlDb250ZW50ICYmIGBqdXN0aWZ5LWNvbnRlbnQ6ICR7anVzdGlmeUNvbnRlbnR9O2B9XHJcbmA7XHJcblxyXG5leHBvcnQgY29uc3QgRmxleFJvdyA9IHN0eWxlZChDb250YWluZXIpYFxyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuYDtcclxuXHJcbmV4cG9ydCBjb25zdCBGbGV4Q29sdW1uID0gc3R5bGVkKENvbnRhaW5lcilgXHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5gO1xyXG5cclxuZXhwb3J0IGNvbnN0IFNwYWNlciA9IHN0eWxlZC5kaXZgXHJcbiAgICBmbGV4LWJhc2lzOiAwO1xyXG4gICAgZmxleC1ncm93OiAxO1xyXG5gO1xyXG4iXX0=