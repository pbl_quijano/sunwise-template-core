"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NotificationWrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: flex;\n    margin: -8px;\n    padding-right: 40px;\n"])));

var NotificationIcon = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    justify-content: center;\n    text-align: center;\n    width: 60px;\n    min-width: 60px;\n    padding: 8px;\n\n    i {\n        background: #fff;\n        border-radius: 100%;\n        color: #008dff;\n        display: inline-block;\n        font-size: 16px;\n        font-weight: bold;\n        height: 35px;\n        line-height: 35px;\n        width: 35px;\n    }\n\n    ", "\n"])), function (_ref) {
  var variant = _ref.variant;

  switch (variant) {
    case 'warning':
      return "background-color: #BFD3DA;\n                        i {\n                            color: #BFD3DA;\n                        }";

    case 'success':
      return "background-color: #00B667;\n                        i {\n                            color: #00B667;\n                        }";

    case 'online':
      return "background-color: #2BF2AB;\n                        i {\n                            color: #2BF2AB;\n                        }";

    case 'offline':
    case 'danger':
      return "background-color: #FA6968;\n                        i {\n                            color: #FA6968;\n                        }";

    default:
      return "background-color: #008dff;\n                        i {\n                            color: #008dff;\n                        }";
  }
});

var NotificationContent = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #003b4b;\n    font-size: 12px;\n    line-height: 19px;\n    padding: 15px;\n    span {\n        color: #848bab;\n        line-height: 15px;\n        font-weight: 500;\n        letter-spacing: 0;\n    }\n"])));

var ToastNotification = function ToastNotification(_ref2) {
  var title = _ref2.title,
      body = _ref2.body,
      type = _ref2.type;
  var icon = 'fas fa-';

  switch (type) {
    case 'offline':
    case 'warning':
      icon += 'exclamation';
      break;

    case 'success':
      icon += 'check';
      break;

    case 'danger':
      icon += 'minus';
      break;

    case 'online':
      icon += 'wifi';
      break;

    default:
      icon += 'question';
      break;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(NotificationWrapper, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(NotificationIcon, {
      variant: type,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
        className: icon
      })
    }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(NotificationContent, {
      children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("div", {
        children: /*#__PURE__*/(0, _jsxRuntime.jsx)("strong", {
          children: title
        })
      }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
        children: body
      })]
    })]
  });
};

ToastNotification.propTypes = {
  title: _propTypes.default.string,
  body: _propTypes.default.string,
  type: _propTypes.default.string
};
var _default = ToastNotification;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1RvYXN0Tm90aWZpY2F0aW9uLmpzIl0sIm5hbWVzIjpbIk5vdGlmaWNhdGlvbldyYXBwZXIiLCJzdHlsZWQiLCJkaXYiLCJOb3RpZmljYXRpb25JY29uIiwidmFyaWFudCIsIk5vdGlmaWNhdGlvbkNvbnRlbnQiLCJUb2FzdE5vdGlmaWNhdGlvbiIsInRpdGxlIiwiYm9keSIsInR5cGUiLCJpY29uIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxtQkFBbUIsR0FBR0MsMEJBQU9DLEdBQVYsd0lBQXpCOztBQU1BLElBQU1DLGdCQUFnQixHQUFHRiwwQkFBT0MsR0FBVix5ZkFxQmhCLGdCQUFpQjtBQUFBLE1BQWRFLE9BQWMsUUFBZEEsT0FBYzs7QUFDZixVQUFRQSxPQUFSO0FBQ0ksU0FBSyxTQUFMO0FBQ0k7O0FBSUosU0FBSyxTQUFMO0FBQ0k7O0FBSUosU0FBSyxRQUFMO0FBQ0k7O0FBSUosU0FBSyxTQUFMO0FBQ0EsU0FBSyxRQUFMO0FBQ0k7O0FBSUo7QUFDSTtBQXZCUjtBQTRCSCxDQWxEaUIsQ0FBdEI7O0FBcURBLElBQU1DLG1CQUFtQixHQUFHSiwwQkFBT0MsR0FBViwrUkFBekI7O0FBYUEsSUFBTUksaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixRQUEyQjtBQUFBLE1BQXhCQyxLQUF3QixTQUF4QkEsS0FBd0I7QUFBQSxNQUFqQkMsSUFBaUIsU0FBakJBLElBQWlCO0FBQUEsTUFBWEMsSUFBVyxTQUFYQSxJQUFXO0FBQ2pELE1BQUlDLElBQUksR0FBRyxTQUFYOztBQUVBLFVBQVFELElBQVI7QUFDSSxTQUFLLFNBQUw7QUFDQSxTQUFLLFNBQUw7QUFDSUMsTUFBQUEsSUFBSSxJQUFJLGFBQVI7QUFDQTs7QUFDSixTQUFLLFNBQUw7QUFDSUEsTUFBQUEsSUFBSSxJQUFJLE9BQVI7QUFDQTs7QUFDSixTQUFLLFFBQUw7QUFDSUEsTUFBQUEsSUFBSSxJQUFJLE9BQVI7QUFDQTs7QUFDSixTQUFLLFFBQUw7QUFDSUEsTUFBQUEsSUFBSSxJQUFJLE1BQVI7QUFDQTs7QUFDSjtBQUNJQSxNQUFBQSxJQUFJLElBQUksVUFBUjtBQUNBO0FBaEJSOztBQW1CQSxzQkFDSSxzQkFBQyxtQkFBRDtBQUFBLDRCQUNJLHFCQUFDLGdCQUFEO0FBQWtCLE1BQUEsT0FBTyxFQUFFRCxJQUEzQjtBQUFBLDZCQUNJO0FBQUcsUUFBQSxTQUFTLEVBQUVDO0FBQWQ7QUFESixNQURKLGVBS0ksc0JBQUMsbUJBQUQ7QUFBQSw4QkFDSTtBQUFBLCtCQUNJO0FBQUEsb0JBQVNIO0FBQVQ7QUFESixRQURKLGVBS0k7QUFBQSxrQkFBT0M7QUFBUCxRQUxKO0FBQUEsTUFMSjtBQUFBLElBREo7QUFlSCxDQXJDRDs7QUF1Q0FGLGlCQUFpQixDQUFDSyxTQUFsQixHQUE4QjtBQUMxQkosRUFBQUEsS0FBSyxFQUFFSyxtQkFBVUMsTUFEUztBQUUxQkwsRUFBQUEsSUFBSSxFQUFFSSxtQkFBVUMsTUFGVTtBQUcxQkosRUFBQUEsSUFBSSxFQUFFRyxtQkFBVUM7QUFIVSxDQUE5QjtlQU1lUCxpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuY29uc3QgTm90aWZpY2F0aW9uV3JhcHBlciA9IHN0eWxlZC5kaXZgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IC04cHg7XG4gICAgcGFkZGluZy1yaWdodDogNDBweDtcbmA7XG5cbmNvbnN0IE5vdGlmaWNhdGlvbkljb24gPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDYwcHg7XG4gICAgbWluLXdpZHRoOiA2MHB4O1xuICAgIHBhZGRpbmc6IDhweDtcblxuICAgIGkge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICBjb2xvcjogIzAwOGRmZjtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICAgICAgICB3aWR0aDogMzVweDtcbiAgICB9XG5cbiAgICAkeyh7IHZhcmlhbnQgfSkgPT4ge1xuICAgICAgICBzd2l0Y2ggKHZhcmlhbnQpIHtcbiAgICAgICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxuICAgICAgICAgICAgICAgIHJldHVybiBgYmFja2dyb3VuZC1jb2xvcjogI0JGRDNEQTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjQkZEM0RBO1xuICAgICAgICAgICAgICAgICAgICAgICAgfWA7XG4gICAgICAgICAgICBjYXNlICdzdWNjZXNzJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gYGJhY2tncm91bmQtY29sb3I6ICMwMEI2Njc7XG4gICAgICAgICAgICAgICAgICAgICAgICBpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzAwQjY2NztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1gO1xuICAgICAgICAgICAgY2FzZSAnb25saW5lJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gYGJhY2tncm91bmQtY29sb3I6ICMyQkYyQUI7XG4gICAgICAgICAgICAgICAgICAgICAgICBpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzJCRjJBQjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1gO1xuICAgICAgICAgICAgY2FzZSAnb2ZmbGluZSc6XG4gICAgICAgICAgICBjYXNlICdkYW5nZXInOlxuICAgICAgICAgICAgICAgIHJldHVybiBgYmFja2dyb3VuZC1jb2xvcjogI0ZBNjk2ODtcbiAgICAgICAgICAgICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjRkE2OTY4O1xuICAgICAgICAgICAgICAgICAgICAgICAgfWA7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIHJldHVybiBgYmFja2dyb3VuZC1jb2xvcjogIzAwOGRmZjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjMDA4ZGZmO1xuICAgICAgICAgICAgICAgICAgICAgICAgfWA7XG4gICAgICAgIH1cbiAgICB9fVxuYDtcblxuY29uc3QgTm90aWZpY2F0aW9uQ29udGVudCA9IHN0eWxlZC5kaXZgXG4gICAgY29sb3I6ICMwMDNiNGI7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgc3BhbiB7XG4gICAgICAgIGNvbG9yOiAjODQ4YmFiO1xuICAgICAgICBsaW5lLWhlaWdodDogMTVweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgfVxuYDtcblxuY29uc3QgVG9hc3ROb3RpZmljYXRpb24gPSAoeyB0aXRsZSwgYm9keSwgdHlwZSB9KSA9PiB7XG4gICAgbGV0IGljb24gPSAnZmFzIGZhLSc7XG5cbiAgICBzd2l0Y2ggKHR5cGUpIHtcbiAgICAgICAgY2FzZSAnb2ZmbGluZSc6XG4gICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxuICAgICAgICAgICAgaWNvbiArPSAnZXhjbGFtYXRpb24nO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ3N1Y2Nlc3MnOlxuICAgICAgICAgICAgaWNvbiArPSAnY2hlY2snO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ2Rhbmdlcic6XG4gICAgICAgICAgICBpY29uICs9ICdtaW51cyc7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnb25saW5lJzpcbiAgICAgICAgICAgIGljb24gKz0gJ3dpZmknO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBpY29uICs9ICdxdWVzdGlvbic7XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG5cbiAgICByZXR1cm4gKFxuICAgICAgICA8Tm90aWZpY2F0aW9uV3JhcHBlcj5cbiAgICAgICAgICAgIDxOb3RpZmljYXRpb25JY29uIHZhcmlhbnQ9e3R5cGV9PlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT17aWNvbn0gLz5cbiAgICAgICAgICAgIDwvTm90aWZpY2F0aW9uSWNvbj5cblxuICAgICAgICAgICAgPE5vdGlmaWNhdGlvbkNvbnRlbnQ+XG4gICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgPHN0cm9uZz57dGl0bGV9PC9zdHJvbmc+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8c3Bhbj57Ym9keX08L3NwYW4+XG4gICAgICAgICAgICA8L05vdGlmaWNhdGlvbkNvbnRlbnQ+XG4gICAgICAgIDwvTm90aWZpY2F0aW9uV3JhcHBlcj5cbiAgICApO1xufTtcblxuVG9hc3ROb3RpZmljYXRpb24ucHJvcFR5cGVzID0ge1xuICAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGJvZHk6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRvYXN0Tm90aWZpY2F0aW9uO1xuIl19