"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _InputFieldText = _interopRequireDefault(require("./InputFieldText"));

var _RoundedIcon = _interopRequireDefault(require("./RoundedIcon"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledInputSearch = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n"])));

var InputSearch = function InputSearch(_ref) {
  var disabled = _ref.disabled,
      placeholder = _ref.placeholder,
      onChange = _ref.onChange,
      value = _ref.value,
      _ref$visible = _ref.visible,
      visible = _ref$visible === void 0 ? true : _ref$visible;

  var handleOnChange = function handleOnChange(e) {
    return onChange(e);
  };

  if (!visible) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(StyledInputSearch, {
    children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_RoundedIcon.default, {
      bgColor: "#ff9a00",
      disabled: disabled,
      icon: "fa fa-search",
      iconColor: "#FFF",
      noBorder: true
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_InputFieldText.default, {
      disabled: disabled,
      placeholder: placeholder,
      onChange: handleOnChange,
      value: value
    })]
  });
};

InputSearch.propTypes = {
  disabled: _propTypes.default.bool,
  placeholder: _propTypes.default.string,
  onChange: _propTypes.default.func,
  value: _propTypes.default.string,
  visible: _propTypes.default.bool
};
var _default = InputSearch;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0U2VhcmNoLmpzIl0sIm5hbWVzIjpbIlN0eWxlZElucHV0U2VhcmNoIiwic3R5bGVkIiwiZGl2IiwiSW5wdXRTZWFyY2giLCJkaXNhYmxlZCIsInBsYWNlaG9sZGVyIiwib25DaGFuZ2UiLCJ2YWx1ZSIsInZpc2libGUiLCJoYW5kbGVPbkNoYW5nZSIsImUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJib29sIiwic3RyaW5nIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsaUJBQWlCLEdBQUdDLDBCQUFPQyxHQUFWLHFIQUF2Qjs7QUFLQSxJQUFNQyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxPQU1kO0FBQUEsTUFMRkMsUUFLRSxRQUxGQSxRQUtFO0FBQUEsTUFKRkMsV0FJRSxRQUpGQSxXQUlFO0FBQUEsTUFIRkMsUUFHRSxRQUhGQSxRQUdFO0FBQUEsTUFGRkMsS0FFRSxRQUZGQSxLQUVFO0FBQUEsMEJBREZDLE9BQ0U7QUFBQSxNQURGQSxPQUNFLDZCQURRLElBQ1I7O0FBQ0YsTUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxDQUFEO0FBQUEsV0FBT0osUUFBUSxDQUFDSSxDQUFELENBQWY7QUFBQSxHQUF2Qjs7QUFDQSxNQUFJLENBQUNGLE9BQUwsRUFBYztBQUNWLFdBQU8sSUFBUDtBQUNIOztBQUNELHNCQUNJLHNCQUFDLGlCQUFEO0FBQUEsNEJBQ0kscUJBQUMsb0JBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBQyxTQURaO0FBRUksTUFBQSxRQUFRLEVBQUVKLFFBRmQ7QUFHSSxNQUFBLElBQUksRUFBQyxjQUhUO0FBSUksTUFBQSxTQUFTLEVBQUMsTUFKZDtBQUtJLE1BQUEsUUFBUTtBQUxaLE1BREosZUFTSSxxQkFBQyx1QkFBRDtBQUNJLE1BQUEsUUFBUSxFQUFFQSxRQURkO0FBRUksTUFBQSxXQUFXLEVBQUVDLFdBRmpCO0FBR0ksTUFBQSxRQUFRLEVBQUVJLGNBSGQ7QUFJSSxNQUFBLEtBQUssRUFBRUY7QUFKWCxNQVRKO0FBQUEsSUFESjtBQWtCSCxDQTdCRDs7QUErQkFKLFdBQVcsQ0FBQ1EsU0FBWixHQUF3QjtBQUNwQlAsRUFBQUEsUUFBUSxFQUFFUSxtQkFBVUMsSUFEQTtBQUVwQlIsRUFBQUEsV0FBVyxFQUFFTyxtQkFBVUUsTUFGSDtBQUdwQlIsRUFBQUEsUUFBUSxFQUFFTSxtQkFBVUcsSUFIQTtBQUlwQlIsRUFBQUEsS0FBSyxFQUFFSyxtQkFBVUUsTUFKRztBQUtwQk4sRUFBQUEsT0FBTyxFQUFFSSxtQkFBVUM7QUFMQyxDQUF4QjtlQVFlVixXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgSW5wdXRGaWVsZFRleHQgZnJvbSAnLi9JbnB1dEZpZWxkVGV4dCc7XG5pbXBvcnQgUm91bmRlZEljb24gZnJvbSAnLi9Sb3VuZGVkSWNvbic7XG5cbmNvbnN0IFN0eWxlZElucHV0U2VhcmNoID0gc3R5bGVkLmRpdmBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG5gO1xuXG5jb25zdCBJbnB1dFNlYXJjaCA9ICh7XG4gICAgZGlzYWJsZWQsXG4gICAgcGxhY2Vob2xkZXIsXG4gICAgb25DaGFuZ2UsXG4gICAgdmFsdWUsXG4gICAgdmlzaWJsZSA9IHRydWUsXG59KSA9PiB7XG4gICAgY29uc3QgaGFuZGxlT25DaGFuZ2UgPSAoZSkgPT4gb25DaGFuZ2UoZSk7XG4gICAgaWYgKCF2aXNpYmxlKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICByZXR1cm4gKFxuICAgICAgICA8U3R5bGVkSW5wdXRTZWFyY2g+XG4gICAgICAgICAgICA8Um91bmRlZEljb25cbiAgICAgICAgICAgICAgICBiZ0NvbG9yPVwiI2ZmOWEwMFwiXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAgICAgICAgIGljb249XCJmYSBmYS1zZWFyY2hcIlxuICAgICAgICAgICAgICAgIGljb25Db2xvcj1cIiNGRkZcIlxuICAgICAgICAgICAgICAgIG5vQm9yZGVyXG4gICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICA8SW5wdXRGaWVsZFRleHRcbiAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtoYW5kbGVPbkNoYW5nZX1cbiAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAvPlxuICAgICAgICA8L1N0eWxlZElucHV0U2VhcmNoPlxuICAgICk7XG59O1xuXG5JbnB1dFNlYXJjaC5wcm9wVHlwZXMgPSB7XG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2aXNpYmxlOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IElucHV0U2VhcmNoO1xuIl19