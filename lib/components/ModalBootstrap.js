"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _TitleIcon = _interopRequireDefault(require("./TitleIcon"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ModalWrapper = (0, _styledComponents.default)(_reactBootstrap.Modal)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    && {\n        .close {\n            background-color: #f9fafe;\n            border-radius: 100%;\n            height: 38px;\n            position: absolute;\n            right: 15px;\n            top: 15px;\n            width: 38px;\n            z-index: 20;\n        }\n\n        .modal-body {\n            padding: ", ";\n        }\n\n        .modal-footer {\n            padding: 0;\n            justify-content: flex-start;\n        }\n    }\n"])), function (_ref) {
  var padding = _ref.padding;
  return padding;
});

var ModalBootstrap = function ModalBootstrap(_ref2) {
  var _ref2$backdrop = _ref2.backdrop,
      backdrop = _ref2$backdrop === void 0 ? 'static' : _ref2$backdrop,
      backdropClassName = _ref2.backdropClassName,
      children = _ref2.children,
      className = _ref2.className,
      classNameBody = _ref2.classNameBody,
      _ref2$closeButton = _ref2.closeButton,
      closeButton = _ref2$closeButton === void 0 ? true : _ref2$closeButton,
      imgUrl = _ref2.imgUrl,
      _ref2$isFontIcon = _ref2.isFontIcon,
      isFontIcon = _ref2$isFontIcon === void 0 ? false : _ref2$isFontIcon,
      _ref2$keyboard = _ref2.keyboard,
      keyboard = _ref2$keyboard === void 0 ? true : _ref2$keyboard,
      onEnter = _ref2.onEnter,
      onExited = _ref2.onExited,
      onHide = _ref2.onHide,
      show = _ref2.show,
      _ref2$size = _ref2.size,
      size = _ref2$size === void 0 ? 'xl' : _ref2$size,
      title = _ref2.title,
      _ref2$padding = _ref2.padding,
      padding = _ref2$padding === void 0 ? '1rem' : _ref2$padding,
      _ref2$footerComponent = _ref2.footerComponent,
      FooterComponent = _ref2$footerComponent === void 0 ? null : _ref2$footerComponent;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(ModalWrapper, {
    backdrop: backdrop,
    backdropClassName: backdropClassName,
    centered: true,
    className: className,
    keyboard: keyboard,
    onEnter: onEnter,
    onExited: onExited,
    onHide: onHide,
    show: show,
    size: size,
    padding: padding,
    children: [/*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Modal.Body, {
      className: classNameBody,
      children: [closeButton && onHide && /*#__PURE__*/(0, _jsxRuntime.jsxs)("button", {
        type: "button",
        className: "close",
        onClick: onHide,
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          "aria-hidden": "true",
          children: "\xD7"
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
          className: "sr-only",
          children: "Close"
        })]
      }), title && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_jsxRuntime.Fragment, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Row, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Col, {
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_TitleIcon.default, {
              imgUrl: imgUrl,
              isFontIcon: isFontIcon,
              title: title,
              fontSize: "22px",
              fontWeight: "bold"
            })
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)("hr", {
          className: "mt-3"
        })]
      }), children]
    }), FooterComponent && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Modal.Footer, {
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(FooterComponent, {})
    })]
  });
};

ModalBootstrap.propTypes = {
  backdrop: _propTypes.default.string,
  backdropClassName: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.array]),
  className: _propTypes.default.string,
  classNameBody: _propTypes.default.string,
  closeButton: _propTypes.default.bool,
  imgUrl: _propTypes.default.string,
  isFontIcon: _propTypes.default.bool,
  keyboard: _propTypes.default.bool,
  onEnter: _propTypes.default.func,
  onExited: _propTypes.default.func,
  onHide: _propTypes.default.func,
  show: _propTypes.default.bool,
  size: _propTypes.default.string,
  title: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
  padding: _propTypes.default.string,
  footerComponent: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.func]),
  resetForm: _propTypes.default.func
};
var _default = ModalBootstrap;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL01vZGFsQm9vdHN0cmFwLmpzIl0sIm5hbWVzIjpbIk1vZGFsV3JhcHBlciIsIk1vZGFsIiwicGFkZGluZyIsIk1vZGFsQm9vdHN0cmFwIiwiYmFja2Ryb3AiLCJiYWNrZHJvcENsYXNzTmFtZSIsImNoaWxkcmVuIiwiY2xhc3NOYW1lIiwiY2xhc3NOYW1lQm9keSIsImNsb3NlQnV0dG9uIiwiaW1nVXJsIiwiaXNGb250SWNvbiIsImtleWJvYXJkIiwib25FbnRlciIsIm9uRXhpdGVkIiwib25IaWRlIiwic2hvdyIsInNpemUiLCJ0aXRsZSIsImZvb3RlckNvbXBvbmVudCIsIkZvb3RlckNvbXBvbmVudCIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsIm9uZU9mVHlwZSIsIm9iamVjdCIsImFycmF5IiwiYm9vbCIsImZ1bmMiLCJyZXNldEZvcm0iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7Ozs7Ozs7OztBQUVBLElBQU1BLFlBQVksR0FBRywrQkFBT0MscUJBQVAsQ0FBSCwyZ0JBY0s7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxTQUFpQkEsT0FBakI7QUFBQSxDQWRMLENBQWxCOztBQXdCQSxJQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCO0FBQUEsNkJBQ25CQyxRQURtQjtBQUFBLE1BQ25CQSxRQURtQiwrQkFDUixRQURRO0FBQUEsTUFFbkJDLGlCQUZtQixTQUVuQkEsaUJBRm1CO0FBQUEsTUFHbkJDLFFBSG1CLFNBR25CQSxRQUhtQjtBQUFBLE1BSW5CQyxTQUptQixTQUluQkEsU0FKbUI7QUFBQSxNQUtuQkMsYUFMbUIsU0FLbkJBLGFBTG1CO0FBQUEsZ0NBTW5CQyxXQU5tQjtBQUFBLE1BTW5CQSxXQU5tQixrQ0FNTCxJQU5LO0FBQUEsTUFPbkJDLE1BUG1CLFNBT25CQSxNQVBtQjtBQUFBLCtCQVFuQkMsVUFSbUI7QUFBQSxNQVFuQkEsVUFSbUIsaUNBUU4sS0FSTTtBQUFBLDZCQVNuQkMsUUFUbUI7QUFBQSxNQVNuQkEsUUFUbUIsK0JBU1IsSUFUUTtBQUFBLE1BVW5CQyxPQVZtQixTQVVuQkEsT0FWbUI7QUFBQSxNQVduQkMsUUFYbUIsU0FXbkJBLFFBWG1CO0FBQUEsTUFZbkJDLE1BWm1CLFNBWW5CQSxNQVptQjtBQUFBLE1BYW5CQyxJQWJtQixTQWFuQkEsSUFibUI7QUFBQSx5QkFjbkJDLElBZG1CO0FBQUEsTUFjbkJBLElBZG1CLDJCQWNaLElBZFk7QUFBQSxNQWVuQkMsS0FmbUIsU0FlbkJBLEtBZm1CO0FBQUEsNEJBZ0JuQmhCLE9BaEJtQjtBQUFBLE1BZ0JuQkEsT0FoQm1CLDhCQWdCVCxNQWhCUztBQUFBLG9DQWlCbkJpQixlQWpCbUI7QUFBQSxNQWlCRkMsZUFqQkUsc0NBaUJnQixJQWpCaEI7QUFBQSxzQkFtQm5CLHNCQUFDLFlBQUQ7QUFDSSxJQUFBLFFBQVEsRUFBRWhCLFFBRGQ7QUFFSSxJQUFBLGlCQUFpQixFQUFFQyxpQkFGdkI7QUFHSSxJQUFBLFFBQVEsTUFIWjtBQUlJLElBQUEsU0FBUyxFQUFFRSxTQUpmO0FBS0ksSUFBQSxRQUFRLEVBQUVLLFFBTGQ7QUFNSSxJQUFBLE9BQU8sRUFBRUMsT0FOYjtBQU9JLElBQUEsUUFBUSxFQUFFQyxRQVBkO0FBUUksSUFBQSxNQUFNLEVBQUVDLE1BUlo7QUFTSSxJQUFBLElBQUksRUFBRUMsSUFUVjtBQVVJLElBQUEsSUFBSSxFQUFFQyxJQVZWO0FBV0ksSUFBQSxPQUFPLEVBQUVmLE9BWGI7QUFBQSw0QkFhSSxzQkFBQyxxQkFBRCxDQUFPLElBQVA7QUFBWSxNQUFBLFNBQVMsRUFBRU0sYUFBdkI7QUFBQSxpQkFDS0MsV0FBVyxJQUFJTSxNQUFmLGlCQUNHO0FBQVEsUUFBQSxJQUFJLEVBQUMsUUFBYjtBQUFzQixRQUFBLFNBQVMsRUFBQyxPQUFoQztBQUF3QyxRQUFBLE9BQU8sRUFBRUEsTUFBakQ7QUFBQSxnQ0FDSTtBQUFNLHlCQUFZLE1BQWxCO0FBQUE7QUFBQSxVQURKLGVBRUk7QUFBTSxVQUFBLFNBQVMsRUFBQyxTQUFoQjtBQUFBO0FBQUEsVUFGSjtBQUFBLFFBRlIsRUFRS0csS0FBSyxpQkFDRjtBQUFBLGdDQUNJLHFCQUFDLG1CQUFEO0FBQUEsaUNBQ0kscUJBQUMsbUJBQUQ7QUFBQSxtQ0FDSSxxQkFBQyxrQkFBRDtBQUNJLGNBQUEsTUFBTSxFQUFFUixNQURaO0FBRUksY0FBQSxVQUFVLEVBQUVDLFVBRmhCO0FBR0ksY0FBQSxLQUFLLEVBQUVPLEtBSFg7QUFJSSxjQUFBLFFBQVEsRUFBQyxNQUpiO0FBS0ksY0FBQSxVQUFVLEVBQUM7QUFMZjtBQURKO0FBREosVUFESixlQWFJO0FBQUksVUFBQSxTQUFTLEVBQUM7QUFBZCxVQWJKO0FBQUEsUUFUUixFQXlCS1osUUF6Qkw7QUFBQSxNQWJKLEVBd0NLYyxlQUFlLGlCQUNaLHFCQUFDLHFCQUFELENBQU8sTUFBUDtBQUFBLDZCQUNJLHFCQUFDLGVBQUQ7QUFESixNQXpDUjtBQUFBLElBbkJtQjtBQUFBLENBQXZCOztBQW1FQWpCLGNBQWMsQ0FBQ2tCLFNBQWYsR0FBMkI7QUFDdkJqQixFQUFBQSxRQUFRLEVBQUVrQixtQkFBVUMsTUFERztBQUV2QmxCLEVBQUFBLGlCQUFpQixFQUFFaUIsbUJBQVVDLE1BRk47QUFHdkJqQixFQUFBQSxRQUFRLEVBQUVnQixtQkFBVUUsU0FBVixDQUFvQixDQUFDRixtQkFBVUcsTUFBWCxFQUFtQkgsbUJBQVVJLEtBQTdCLENBQXBCLENBSGE7QUFJdkJuQixFQUFBQSxTQUFTLEVBQUVlLG1CQUFVQyxNQUpFO0FBS3ZCZixFQUFBQSxhQUFhLEVBQUVjLG1CQUFVQyxNQUxGO0FBTXZCZCxFQUFBQSxXQUFXLEVBQUVhLG1CQUFVSyxJQU5BO0FBT3ZCakIsRUFBQUEsTUFBTSxFQUFFWSxtQkFBVUMsTUFQSztBQVF2QlosRUFBQUEsVUFBVSxFQUFFVyxtQkFBVUssSUFSQztBQVN2QmYsRUFBQUEsUUFBUSxFQUFFVSxtQkFBVUssSUFURztBQVV2QmQsRUFBQUEsT0FBTyxFQUFFUyxtQkFBVU0sSUFWSTtBQVd2QmQsRUFBQUEsUUFBUSxFQUFFUSxtQkFBVU0sSUFYRztBQVl2QmIsRUFBQUEsTUFBTSxFQUFFTyxtQkFBVU0sSUFaSztBQWF2QlosRUFBQUEsSUFBSSxFQUFFTSxtQkFBVUssSUFiTztBQWN2QlYsRUFBQUEsSUFBSSxFQUFFSyxtQkFBVUMsTUFkTztBQWV2QkwsRUFBQUEsS0FBSyxFQUFFSSxtQkFBVUUsU0FBVixDQUFvQixDQUFDRixtQkFBVUMsTUFBWCxFQUFtQkQsbUJBQVVHLE1BQTdCLENBQXBCLENBZmdCO0FBZ0J2QnZCLEVBQUFBLE9BQU8sRUFBRW9CLG1CQUFVQyxNQWhCSTtBQWlCdkJKLEVBQUFBLGVBQWUsRUFBRUcsbUJBQVVFLFNBQVYsQ0FBb0IsQ0FBQ0YsbUJBQVVHLE1BQVgsRUFBbUJILG1CQUFVTSxJQUE3QixDQUFwQixDQWpCTTtBQWtCdkJDLEVBQUFBLFNBQVMsRUFBRVAsbUJBQVVNO0FBbEJFLENBQTNCO2VBcUJlekIsYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBDb2wsIFJvdywgTW9kYWwgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBUaXRsZUljb24gZnJvbSAnLi9UaXRsZUljb24nO1xuXG5jb25zdCBNb2RhbFdyYXBwZXIgPSBzdHlsZWQoTW9kYWwpYFxuICAgICYmIHtcbiAgICAgICAgLmNsb3NlIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmOWZhZmU7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiAzOHB4O1xuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgcmlnaHQ6IDE1cHg7XG4gICAgICAgICAgICB0b3A6IDE1cHg7XG4gICAgICAgICAgICB3aWR0aDogMzhweDtcbiAgICAgICAgICAgIHotaW5kZXg6IDIwO1xuICAgICAgICB9XG5cbiAgICAgICAgLm1vZGFsLWJvZHkge1xuICAgICAgICAgICAgcGFkZGluZzogJHsoeyBwYWRkaW5nIH0pID0+IHBhZGRpbmd9O1xuICAgICAgICB9XG5cbiAgICAgICAgLm1vZGFsLWZvb3RlciB7XG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAgICAgICB9XG4gICAgfVxuYDtcblxuY29uc3QgTW9kYWxCb290c3RyYXAgPSAoe1xuICAgIGJhY2tkcm9wID0gJ3N0YXRpYycsXG4gICAgYmFja2Ryb3BDbGFzc05hbWUsXG4gICAgY2hpbGRyZW4sXG4gICAgY2xhc3NOYW1lLFxuICAgIGNsYXNzTmFtZUJvZHksXG4gICAgY2xvc2VCdXR0b24gPSB0cnVlLFxuICAgIGltZ1VybCxcbiAgICBpc0ZvbnRJY29uID0gZmFsc2UsXG4gICAga2V5Ym9hcmQgPSB0cnVlLFxuICAgIG9uRW50ZXIsXG4gICAgb25FeGl0ZWQsXG4gICAgb25IaWRlLFxuICAgIHNob3csXG4gICAgc2l6ZSA9ICd4bCcsXG4gICAgdGl0bGUsXG4gICAgcGFkZGluZyA9ICcxcmVtJyxcbiAgICBmb290ZXJDb21wb25lbnQ6IEZvb3RlckNvbXBvbmVudCA9IG51bGwsXG59KSA9PiAoXG4gICAgPE1vZGFsV3JhcHBlclxuICAgICAgICBiYWNrZHJvcD17YmFja2Ryb3B9XG4gICAgICAgIGJhY2tkcm9wQ2xhc3NOYW1lPXtiYWNrZHJvcENsYXNzTmFtZX1cbiAgICAgICAgY2VudGVyZWRcbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc05hbWV9XG4gICAgICAgIGtleWJvYXJkPXtrZXlib2FyZH1cbiAgICAgICAgb25FbnRlcj17b25FbnRlcn1cbiAgICAgICAgb25FeGl0ZWQ9e29uRXhpdGVkfVxuICAgICAgICBvbkhpZGU9e29uSGlkZX1cbiAgICAgICAgc2hvdz17c2hvd31cbiAgICAgICAgc2l6ZT17c2l6ZX1cbiAgICAgICAgcGFkZGluZz17cGFkZGluZ31cbiAgICA+XG4gICAgICAgIDxNb2RhbC5Cb2R5IGNsYXNzTmFtZT17Y2xhc3NOYW1lQm9keX0+XG4gICAgICAgICAgICB7Y2xvc2VCdXR0b24gJiYgb25IaWRlICYmIChcbiAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzc05hbWU9XCJjbG9zZVwiIG9uQ2xpY2s9e29uSGlkZX0+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPsOXPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJzci1vbmx5XCI+Q2xvc2U8L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICApfVxuXG4gICAgICAgICAgICB7dGl0bGUgJiYgKFxuICAgICAgICAgICAgICAgIDw+XG4gICAgICAgICAgICAgICAgICAgIDxSb3c+XG4gICAgICAgICAgICAgICAgICAgICAgICA8Q29sPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUaXRsZUljb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1nVXJsPXtpbWdVcmx9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzRm9udEljb249e2lzRm9udEljb259XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlPXt0aXRsZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udFNpemU9XCIyMnB4XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9udFdlaWdodD1cImJvbGRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0NvbD5cbiAgICAgICAgICAgICAgICAgICAgPC9Sb3c+XG5cbiAgICAgICAgICAgICAgICAgICAgPGhyIGNsYXNzTmFtZT1cIm10LTNcIiAvPlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgPC9Nb2RhbC5Cb2R5PlxuICAgICAgICB7Rm9vdGVyQ29tcG9uZW50ICYmIChcbiAgICAgICAgICAgIDxNb2RhbC5Gb290ZXI+XG4gICAgICAgICAgICAgICAgPEZvb3RlckNvbXBvbmVudCAvPlxuICAgICAgICAgICAgPC9Nb2RhbC5Gb290ZXI+XG4gICAgICAgICl9XG4gICAgPC9Nb2RhbFdyYXBwZXI+XG4pO1xuXG5Nb2RhbEJvb3RzdHJhcC5wcm9wVHlwZXMgPSB7XG4gICAgYmFja2Ryb3A6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgYmFja2Ryb3BDbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5vYmplY3QsIFByb3BUeXBlcy5hcnJheV0pLFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjbGFzc05hbWVCb2R5OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNsb3NlQnV0dG9uOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpbWdVcmw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaXNGb250SWNvbjogUHJvcFR5cGVzLmJvb2wsXG4gICAga2V5Ym9hcmQ6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uRW50ZXI6IFByb3BUeXBlcy5mdW5jLFxuICAgIG9uRXhpdGVkOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBvbkhpZGU6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNob3c6IFByb3BUeXBlcy5ib29sLFxuICAgIHNpemU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdGl0bGU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5vYmplY3RdKSxcbiAgICBwYWRkaW5nOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGZvb3RlckNvbXBvbmVudDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm9iamVjdCwgUHJvcFR5cGVzLmZ1bmNdKSxcbiAgICByZXNldEZvcm06IFByb3BUeXBlcy5mdW5jLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgTW9kYWxCb290c3RyYXA7XG4iXX0=