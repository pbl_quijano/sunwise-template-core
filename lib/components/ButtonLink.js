"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledButtonLink = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: #ff9a00;\n    cursor: pointer;\n    font-size: 10px;\n    margin-left: 30px;\n    margin-top: -10px;\n    text-align: right;\n"])));

var ButtonLink = function ButtonLink(_ref) {
  var children = _ref.children,
      handleClick = _ref.handleClick,
      text = _ref.text,
      _ref$visible = _ref.visible,
      visible = _ref$visible === void 0 ? true : _ref$visible;
  return visible ? /*#__PURE__*/(0, _jsxRuntime.jsxs)(StyledButtonLink, {
    onClick: handleClick,
    children: [text, children]
  }) : null;
};

ButtonLink.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string]),
  handleClick: _propTypes.default.func,
  text: _propTypes.default.string,
  visible: _propTypes.default.bool
};
var _default = ButtonLink;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0J1dHRvbkxpbmsuanMiXSwibmFtZXMiOlsiU3R5bGVkQnV0dG9uTGluayIsInN0eWxlZCIsImRpdiIsIkJ1dHRvbkxpbmsiLCJjaGlsZHJlbiIsImhhbmRsZUNsaWNrIiwidGV4dCIsInZpc2libGUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvbmVPZlR5cGUiLCJvYmplY3QiLCJzdHJpbmciLCJmdW5jIiwiYm9vbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsZ0JBQWdCLEdBQUdDLDBCQUFPQyxHQUFWLGdOQUF0Qjs7QUFTQSxJQUFNQyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxPQUFxRDtBQUFBLE1BQWxEQyxRQUFrRCxRQUFsREEsUUFBa0Q7QUFBQSxNQUF4Q0MsV0FBd0MsUUFBeENBLFdBQXdDO0FBQUEsTUFBM0JDLElBQTJCLFFBQTNCQSxJQUEyQjtBQUFBLDBCQUFyQkMsT0FBcUI7QUFBQSxNQUFyQkEsT0FBcUIsNkJBQVgsSUFBVztBQUNwRSxTQUFPQSxPQUFPLGdCQUNWLHNCQUFDLGdCQUFEO0FBQWtCLElBQUEsT0FBTyxFQUFFRixXQUEzQjtBQUFBLGVBQ0tDLElBREwsRUFFS0YsUUFGTDtBQUFBLElBRFUsR0FLVixJQUxKO0FBTUgsQ0FQRDs7QUFRQUQsVUFBVSxDQUFDSyxTQUFYLEdBQXVCO0FBQ25CSixFQUFBQSxRQUFRLEVBQUVLLG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVRSxNQUFYLEVBQW1CRixtQkFBVUcsTUFBN0IsQ0FBcEIsQ0FEUztBQUVuQlAsRUFBQUEsV0FBVyxFQUFFSSxtQkFBVUksSUFGSjtBQUduQlAsRUFBQUEsSUFBSSxFQUFFRyxtQkFBVUcsTUFIRztBQUluQkwsRUFBQUEsT0FBTyxFQUFFRSxtQkFBVUs7QUFKQSxDQUF2QjtlQU9lWCxVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBTdHlsZWRCdXR0b25MaW5rID0gc3R5bGVkLmRpdmBcbiAgICBjb2xvcjogI2ZmOWEwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuYDtcblxuY29uc3QgQnV0dG9uTGluayA9ICh7IGNoaWxkcmVuLCBoYW5kbGVDbGljaywgdGV4dCwgdmlzaWJsZSA9IHRydWUgfSkgPT4ge1xuICAgIHJldHVybiB2aXNpYmxlID8gKFxuICAgICAgICA8U3R5bGVkQnV0dG9uTGluayBvbkNsaWNrPXtoYW5kbGVDbGlja30+XG4gICAgICAgICAgICB7dGV4dH1cbiAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgPC9TdHlsZWRCdXR0b25MaW5rPlxuICAgICkgOiBudWxsO1xufTtcbkJ1dHRvbkxpbmsucHJvcFR5cGVzID0ge1xuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMub2JqZWN0LCBQcm9wVHlwZXMuc3RyaW5nXSksXG4gICAgaGFuZGxlQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxuICAgIHRleHQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBCdXR0b25MaW5rO1xuIl19