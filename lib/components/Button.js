"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _RoundedIcon = _interopRequireDefault(require("./RoundedIcon"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledButton = _styledComponents.default.button(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background: none;\n    border: none;\n    color: ", ";\n    cursor: pointer;\n    font-size: ", ";\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    outline: none;\n\n    & div {\n        margin-left: 0.5rem;\n    }\n\n    &:focus {\n        outline: none;\n    }\n\n    ", "\n\n    ", "\n\n    ", "\n"])), function (_ref) {
  var _ref$color = _ref.color,
      color = _ref$color === void 0 ? '#848bab' : _ref$color;
  return color;
}, function (_ref2) {
  var _ref2$customSize = _ref2.customSize,
      customSize = _ref2$customSize === void 0 ? 14 : _ref2$customSize;
  return "".concat(customSize, "px");
}, function (_ref3) {
  var disabled = _ref3.disabled;
  return disabled && "\n        opacity: .2;\n    ";
}, function (_ref4) {
  var variant = _ref4.variant;
  return variant === 'bold' && "\n        font-weight: 700;\n        color: ".concat(function (_ref5) {
    var _ref5$color = _ref5.color,
        color = _ref5$color === void 0 ? '#002438' : _ref5$color;
    return color;
  }, ";\n    ");
}, function (_ref6) {
  var direction = _ref6.direction;
  return direction === 'reverse' && "flex-direction: row-reverse;\n\n        & > span {\n            margin-left: 0;\n            margin-right: 0.5rem;\n\n            div{\n                margin-left: 0;\n            }\n        }\n    ";
});

var Button = function Button(_ref7) {
  var _ref7$bgColor = _ref7.bgColor,
      bgColor = _ref7$bgColor === void 0 ? '#FF9A00' : _ref7$bgColor,
      color = _ref7.color,
      className = _ref7.className,
      classNameIcon = _ref7.classNameIcon,
      customSize = _ref7.customSize,
      dataIntercomTarget = _ref7.dataIntercomTarget,
      direction = _ref7.direction,
      disabled = _ref7.disabled,
      handleClick = _ref7.handleClick,
      icon = _ref7.icon,
      _ref7$iconColor = _ref7.iconColor,
      iconColor = _ref7$iconColor === void 0 ? '#FFF' : _ref7$iconColor,
      label = _ref7.label,
      _ref7$size = _ref7.size,
      size = _ref7$size === void 0 ? 'md' : _ref7$size,
      variant = _ref7.variant,
      _ref7$visible = _ref7.visible,
      visible = _ref7$visible === void 0 ? true : _ref7$visible;
  return visible ? /*#__PURE__*/(0, _jsxRuntime.jsxs)(StyledButton, {
    color: color,
    className: className,
    customSize: customSize,
    "data-intercom-target": dataIntercomTarget,
    direction: direction,
    disabled: disabled,
    onClick: handleClick,
    variant: variant,
    children: [label, /*#__PURE__*/(0, _jsxRuntime.jsx)(_RoundedIcon.default, {
      bgColor: bgColor,
      className: classNameIcon,
      icon: icon,
      iconColor: iconColor,
      noBorder: true,
      size: size
    })]
  }) : null;
};

Button.propTypes = {
  bgColor: _propTypes.default.string,
  color: _propTypes.default.string,
  className: _propTypes.default.string,
  classNameIcon: _propTypes.default.string,
  customSize: _propTypes.default.number,
  dataIntercomTarget: _propTypes.default.string,
  direction: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  handleClick: _propTypes.default.func,
  icon: _propTypes.default.string,
  iconColor: _propTypes.default.string,
  label: _propTypes.default.string,
  size: _propTypes.default.string,
  variant: _propTypes.default.string,
  visible: _propTypes.default.bool
};
var _default = Button;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0J1dHRvbi5qcyJdLCJuYW1lcyI6WyJTdHlsZWRCdXR0b24iLCJzdHlsZWQiLCJidXR0b24iLCJjb2xvciIsImN1c3RvbVNpemUiLCJkaXNhYmxlZCIsInZhcmlhbnQiLCJkaXJlY3Rpb24iLCJCdXR0b24iLCJiZ0NvbG9yIiwiY2xhc3NOYW1lIiwiY2xhc3NOYW1lSWNvbiIsImRhdGFJbnRlcmNvbVRhcmdldCIsImhhbmRsZUNsaWNrIiwiaWNvbiIsImljb25Db2xvciIsImxhYmVsIiwic2l6ZSIsInZpc2libGUiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHQywwQkFBT0MsTUFBVix3WkFHTDtBQUFBLHdCQUFHQyxLQUFIO0FBQUEsTUFBR0EsS0FBSCwyQkFBVyxTQUFYO0FBQUEsU0FBMkJBLEtBQTNCO0FBQUEsQ0FISyxFQUtEO0FBQUEsK0JBQUdDLFVBQUg7QUFBQSxNQUFHQSxVQUFILGlDQUFnQixFQUFoQjtBQUFBLG1CQUE0QkEsVUFBNUI7QUFBQSxDQUxDLEVBbUJaO0FBQUEsTUFBR0MsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FDRUEsUUFBUSxrQ0FEVjtBQUFBLENBbkJZLEVBeUJaO0FBQUEsTUFBR0MsT0FBSCxTQUFHQSxPQUFIO0FBQUEsU0FDRUEsT0FBTyxLQUFLLE1BQVosMERBR1M7QUFBQSw0QkFBR0gsS0FBSDtBQUFBLFFBQUdBLEtBQUgsNEJBQVcsU0FBWDtBQUFBLFdBQTJCQSxLQUEzQjtBQUFBLEdBSFQsWUFERjtBQUFBLENBekJZLEVBZ0NaO0FBQUEsTUFBR0ksU0FBSCxTQUFHQSxTQUFIO0FBQUEsU0FDRUEsU0FBUyxLQUFLLFNBQWQsNk1BREY7QUFBQSxDQWhDWSxDQUFsQjs7QUErQ0EsSUFBTUMsTUFBTSxHQUFHLFNBQVRBLE1BQVMsUUFnQlQ7QUFBQSw0QkFmRkMsT0FlRTtBQUFBLE1BZkZBLE9BZUUsOEJBZlEsU0FlUjtBQUFBLE1BZEZOLEtBY0UsU0FkRkEsS0FjRTtBQUFBLE1BYkZPLFNBYUUsU0FiRkEsU0FhRTtBQUFBLE1BWkZDLGFBWUUsU0FaRkEsYUFZRTtBQUFBLE1BWEZQLFVBV0UsU0FYRkEsVUFXRTtBQUFBLE1BVkZRLGtCQVVFLFNBVkZBLGtCQVVFO0FBQUEsTUFURkwsU0FTRSxTQVRGQSxTQVNFO0FBQUEsTUFSRkYsUUFRRSxTQVJGQSxRQVFFO0FBQUEsTUFQRlEsV0FPRSxTQVBGQSxXQU9FO0FBQUEsTUFORkMsSUFNRSxTQU5GQSxJQU1FO0FBQUEsOEJBTEZDLFNBS0U7QUFBQSxNQUxGQSxTQUtFLGdDQUxVLE1BS1Y7QUFBQSxNQUpGQyxLQUlFLFNBSkZBLEtBSUU7QUFBQSx5QkFIRkMsSUFHRTtBQUFBLE1BSEZBLElBR0UsMkJBSEssSUFHTDtBQUFBLE1BRkZYLE9BRUUsU0FGRkEsT0FFRTtBQUFBLDRCQURGWSxPQUNFO0FBQUEsTUFERkEsT0FDRSw4QkFEUSxJQUNSO0FBQ0YsU0FBT0EsT0FBTyxnQkFDVixzQkFBQyxZQUFEO0FBQ0ksSUFBQSxLQUFLLEVBQUVmLEtBRFg7QUFFSSxJQUFBLFNBQVMsRUFBRU8sU0FGZjtBQUdJLElBQUEsVUFBVSxFQUFFTixVQUhoQjtBQUlJLDRCQUFzQlEsa0JBSjFCO0FBS0ksSUFBQSxTQUFTLEVBQUVMLFNBTGY7QUFNSSxJQUFBLFFBQVEsRUFBRUYsUUFOZDtBQU9JLElBQUEsT0FBTyxFQUFFUSxXQVBiO0FBUUksSUFBQSxPQUFPLEVBQUVQLE9BUmI7QUFBQSxlQVVLVSxLQVZMLGVBWUkscUJBQUMsb0JBQUQ7QUFDSSxNQUFBLE9BQU8sRUFBRVAsT0FEYjtBQUVJLE1BQUEsU0FBUyxFQUFFRSxhQUZmO0FBR0ksTUFBQSxJQUFJLEVBQUVHLElBSFY7QUFJSSxNQUFBLFNBQVMsRUFBRUMsU0FKZjtBQUtJLE1BQUEsUUFBUSxNQUxaO0FBTUksTUFBQSxJQUFJLEVBQUVFO0FBTlYsTUFaSjtBQUFBLElBRFUsR0FzQlYsSUF0Qko7QUF1QkgsQ0F4Q0Q7O0FBMENBVCxNQUFNLENBQUNXLFNBQVAsR0FBbUI7QUFDZlYsRUFBQUEsT0FBTyxFQUFFVyxtQkFBVUMsTUFESjtBQUVmbEIsRUFBQUEsS0FBSyxFQUFFaUIsbUJBQVVDLE1BRkY7QUFHZlgsRUFBQUEsU0FBUyxFQUFFVSxtQkFBVUMsTUFITjtBQUlmVixFQUFBQSxhQUFhLEVBQUVTLG1CQUFVQyxNQUpWO0FBS2ZqQixFQUFBQSxVQUFVLEVBQUVnQixtQkFBVUUsTUFMUDtBQU1mVixFQUFBQSxrQkFBa0IsRUFBRVEsbUJBQVVDLE1BTmY7QUFPZmQsRUFBQUEsU0FBUyxFQUFFYSxtQkFBVUMsTUFQTjtBQVFmaEIsRUFBQUEsUUFBUSxFQUFFZSxtQkFBVUcsSUFSTDtBQVNmVixFQUFBQSxXQUFXLEVBQUVPLG1CQUFVSSxJQVRSO0FBVWZWLEVBQUFBLElBQUksRUFBRU0sbUJBQVVDLE1BVkQ7QUFXZk4sRUFBQUEsU0FBUyxFQUFFSyxtQkFBVUMsTUFYTjtBQVlmTCxFQUFBQSxLQUFLLEVBQUVJLG1CQUFVQyxNQVpGO0FBYWZKLEVBQUFBLElBQUksRUFBRUcsbUJBQVVDLE1BYkQ7QUFjZmYsRUFBQUEsT0FBTyxFQUFFYyxtQkFBVUMsTUFkSjtBQWVmSCxFQUFBQSxPQUFPLEVBQUVFLG1CQUFVRztBQWZKLENBQW5CO2VBa0JlZixNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5pbXBvcnQgUm91bmRlZEljb24gZnJvbSAnLi9Sb3VuZGVkSWNvbic7XG5cbmNvbnN0IFN0eWxlZEJ1dHRvbiA9IHN0eWxlZC5idXR0b25gXG4gICAgYmFja2dyb3VuZDogbm9uZTtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgY29sb3I6ICR7KHsgY29sb3IgPSAnIzg0OGJhYicgfSkgPT4gY29sb3J9O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBmb250LXNpemU6ICR7KHsgY3VzdG9tU2l6ZSA9IDE0IH0pID0+IGAke2N1c3RvbVNpemV9cHhgfTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgb3V0bGluZTogbm9uZTtcblxuICAgICYgZGl2IHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcbiAgICB9XG5cbiAgICAmOmZvY3VzIHtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICB9XG5cbiAgICAkeyh7IGRpc2FibGVkIH0pID0+XG4gICAgICAgIGRpc2FibGVkICYmXG4gICAgICAgIGBcbiAgICAgICAgb3BhY2l0eTogLjI7XG4gICAgYH1cblxuICAgICR7KHsgdmFyaWFudCB9KSA9PlxuICAgICAgICB2YXJpYW50ID09PSAnYm9sZCcgJiZcbiAgICAgICAgYFxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgICBjb2xvcjogJHsoeyBjb2xvciA9ICcjMDAyNDM4JyB9KSA9PiBjb2xvcn07XG4gICAgYH1cblxuICAgICR7KHsgZGlyZWN0aW9uIH0pID0+XG4gICAgICAgIGRpcmVjdGlvbiA9PT0gJ3JldmVyc2UnICYmXG4gICAgICAgIGBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XG5cbiAgICAgICAgJiA+IHNwYW4ge1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDA7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcblxuICAgICAgICAgICAgZGl2e1xuICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgYH1cbmA7XG5cbmNvbnN0IEJ1dHRvbiA9ICh7XG4gICAgYmdDb2xvciA9ICcjRkY5QTAwJyxcbiAgICBjb2xvcixcbiAgICBjbGFzc05hbWUsXG4gICAgY2xhc3NOYW1lSWNvbixcbiAgICBjdXN0b21TaXplLFxuICAgIGRhdGFJbnRlcmNvbVRhcmdldCxcbiAgICBkaXJlY3Rpb24sXG4gICAgZGlzYWJsZWQsXG4gICAgaGFuZGxlQ2xpY2ssXG4gICAgaWNvbixcbiAgICBpY29uQ29sb3IgPSAnI0ZGRicsXG4gICAgbGFiZWwsXG4gICAgc2l6ZSA9ICdtZCcsXG4gICAgdmFyaWFudCxcbiAgICB2aXNpYmxlID0gdHJ1ZSxcbn0pID0+IHtcbiAgICByZXR1cm4gdmlzaWJsZSA/IChcbiAgICAgICAgPFN0eWxlZEJ1dHRvblxuICAgICAgICAgICAgY29sb3I9e2NvbG9yfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc05hbWV9XG4gICAgICAgICAgICBjdXN0b21TaXplPXtjdXN0b21TaXplfVxuICAgICAgICAgICAgZGF0YS1pbnRlcmNvbS10YXJnZXQ9e2RhdGFJbnRlcmNvbVRhcmdldH1cbiAgICAgICAgICAgIGRpcmVjdGlvbj17ZGlyZWN0aW9ufVxuICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAgICAgb25DbGljaz17aGFuZGxlQ2xpY2t9XG4gICAgICAgICAgICB2YXJpYW50PXt2YXJpYW50fVxuICAgICAgICA+XG4gICAgICAgICAgICB7bGFiZWx9XG5cbiAgICAgICAgICAgIDxSb3VuZGVkSWNvblxuICAgICAgICAgICAgICAgIGJnQ29sb3I9e2JnQ29sb3J9XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc05hbWVJY29ufVxuICAgICAgICAgICAgICAgIGljb249e2ljb259XG4gICAgICAgICAgICAgICAgaWNvbkNvbG9yPXtpY29uQ29sb3J9XG4gICAgICAgICAgICAgICAgbm9Cb3JkZXJcbiAgICAgICAgICAgICAgICBzaXplPXtzaXplfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgPC9TdHlsZWRCdXR0b24+XG4gICAgKSA6IG51bGw7XG59O1xuXG5CdXR0b24ucHJvcFR5cGVzID0ge1xuICAgIGJnQ29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNsYXNzTmFtZUljb246IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY3VzdG9tU2l6ZTogUHJvcFR5cGVzLm51bWJlcixcbiAgICBkYXRhSW50ZXJjb21UYXJnZXQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGlyZWN0aW9uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBoYW5kbGVDbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaWNvbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpY29uQ29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgbGFiZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc2l6ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YXJpYW50OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHZpc2libGU6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQnV0dG9uO1xuIl19