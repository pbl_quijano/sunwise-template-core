"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledInputFieldText = _styledComponents.default.input(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background: transparent;\n    border: none;\n    color: #848bab;\n    font-size: 14px;\n    outline: none;\n    padding: 8px;\n    text-overflow: ellipsis;\n    width: 100%;\n"])));

var InputFieldText = function InputFieldText(_ref) {
  var placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      onChange = _ref.onChange,
      value = _ref.value;

  var handleOnChange = function handleOnChange(e) {
    return onChange(e);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledInputFieldText, {
    placeholder: placeholder,
    onChange: handleOnChange,
    value: value,
    disabled: disabled
  });
};

InputFieldText.propTypes = {
  disabled: _propTypes.default.bool,
  onChange: _propTypes.default.func,
  placeholder: _propTypes.default.string,
  value: _propTypes.default.string
};
var _default = InputFieldText;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0RmllbGRUZXh0LmpzIl0sIm5hbWVzIjpbIlN0eWxlZElucHV0RmllbGRUZXh0Iiwic3R5bGVkIiwiaW5wdXQiLCJJbnB1dEZpZWxkVGV4dCIsInBsYWNlaG9sZGVyIiwiZGlzYWJsZWQiLCJvbkNoYW5nZSIsInZhbHVlIiwiaGFuZGxlT25DaGFuZ2UiLCJlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYm9vbCIsImZ1bmMiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7Ozs7Ozs7OztBQUVBLElBQU1BLG9CQUFvQixHQUFHQywwQkFBT0MsS0FBViwwUEFBMUI7O0FBV0EsSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixPQUFnRDtBQUFBLE1BQTdDQyxXQUE2QyxRQUE3Q0EsV0FBNkM7QUFBQSxNQUFoQ0MsUUFBZ0MsUUFBaENBLFFBQWdDO0FBQUEsTUFBdEJDLFFBQXNCLFFBQXRCQSxRQUFzQjtBQUFBLE1BQVpDLEtBQVksUUFBWkEsS0FBWTs7QUFDbkUsTUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxDQUFEO0FBQUEsV0FBT0gsUUFBUSxDQUFDRyxDQUFELENBQWY7QUFBQSxHQUF2Qjs7QUFFQSxzQkFDSSxxQkFBQyxvQkFBRDtBQUNJLElBQUEsV0FBVyxFQUFFTCxXQURqQjtBQUVJLElBQUEsUUFBUSxFQUFFSSxjQUZkO0FBR0ksSUFBQSxLQUFLLEVBQUVELEtBSFg7QUFJSSxJQUFBLFFBQVEsRUFBRUY7QUFKZCxJQURKO0FBUUgsQ0FYRDs7QUFhQUYsY0FBYyxDQUFDTyxTQUFmLEdBQTJCO0FBQ3ZCTCxFQUFBQSxRQUFRLEVBQUVNLG1CQUFVQyxJQURHO0FBRXZCTixFQUFBQSxRQUFRLEVBQUVLLG1CQUFVRSxJQUZHO0FBR3ZCVCxFQUFBQSxXQUFXLEVBQUVPLG1CQUFVRyxNQUhBO0FBSXZCUCxFQUFBQSxLQUFLLEVBQUVJLG1CQUFVRztBQUpNLENBQTNCO2VBT2VYLGMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmNvbnN0IFN0eWxlZElucHV0RmllbGRUZXh0ID0gc3R5bGVkLmlucHV0YFxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlcjogbm9uZTtcbiAgICBjb2xvcjogIzg0OGJhYjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgb3V0bGluZTogbm9uZTtcbiAgICBwYWRkaW5nOiA4cHg7XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBJbnB1dEZpZWxkVGV4dCA9ICh7IHBsYWNlaG9sZGVyLCBkaXNhYmxlZCwgb25DaGFuZ2UsIHZhbHVlIH0pID0+IHtcbiAgICBjb25zdCBoYW5kbGVPbkNoYW5nZSA9IChlKSA9PiBvbkNoYW5nZShlKTtcblxuICAgIHJldHVybiAoXG4gICAgICAgIDxTdHlsZWRJbnB1dEZpZWxkVGV4dFxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgb25DaGFuZ2U9e2hhbmRsZU9uQ2hhbmdlfVxuICAgICAgICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAvPlxuICAgICk7XG59O1xuXG5JbnB1dEZpZWxkVGV4dC5wcm9wVHlwZXMgPSB7XG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IElucHV0RmllbGRUZXh0O1xuIl19