"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3, _templateObject4;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledSection = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    display: flex;\n    line-height: 44px;\n    margin-bottom: ", ";\n    margin-top: ", ";\n"])), function (_ref) {
  var marginBottom = _ref.marginBottom;
  return marginBottom;
}, function (_ref2) {
  var marginTop = _ref2.marginTop;
  return marginTop;
});

var StyledImage = _styledComponents.default.img(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    margin-right: 10px;\n    max-width: 100%;\n"])));

var StyledTitle = _styledComponents.default.div(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: #202253;\n    font-size: ", ";\n    font-weight: ", ";\n    letter-spacing: ", ";\n    line-height: ", ";\n    max-width: 90%;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n    overflow: hidden;\n"])), function (_ref3) {
  var fontSize = _ref3.fontSize;
  return fontSize;
}, function (_ref4) {
  var fontWeight = _ref4.fontWeight;
  return fontWeight;
}, function (_ref5) {
  var letterSpacing = _ref5.letterSpacing;
  return letterSpacing;
}, function (_ref6) {
  var lineHeight = _ref6.lineHeight;
  return lineHeight;
});

var FontIcon = _styledComponents.default.i(_templateObject4 || (_templateObject4 = _taggedTemplateLiteral(["\n    color: #ff9a00;\n    font-size: 21px;\n    margin-right: 15px;\n"])));

var TitleIcon = function TitleIcon(_ref7) {
  var _ref7$fontSize = _ref7.fontSize,
      fontSize = _ref7$fontSize === void 0 ? '22px' : _ref7$fontSize,
      _ref7$fontWeight = _ref7.fontWeight,
      fontWeight = _ref7$fontWeight === void 0 ? 'normal' : _ref7$fontWeight,
      imgUrl = _ref7.imgUrl,
      isFontIcon = _ref7.isFontIcon,
      _ref7$letterSpacing = _ref7.letterSpacing,
      letterSpacing = _ref7$letterSpacing === void 0 ? '0.15px' : _ref7$letterSpacing,
      _ref7$lineHeight = _ref7.lineHeight,
      lineHeight = _ref7$lineHeight === void 0 ? '27px' : _ref7$lineHeight,
      _ref7$marginBottom = _ref7.marginBottom,
      marginBottom = _ref7$marginBottom === void 0 ? '0' : _ref7$marginBottom,
      _ref7$marginTop = _ref7.marginTop,
      marginTop = _ref7$marginTop === void 0 ? '0' : _ref7$marginTop,
      title = _ref7.title;
  return /*#__PURE__*/(0, _jsxRuntime.jsxs)(StyledSection, {
    marginBottom: marginBottom,
    marginTop: marginTop,
    children: [!imgUrl ? null : isFontIcon ? /*#__PURE__*/(0, _jsxRuntime.jsx)(FontIcon, {
      className: "fa ".concat(imgUrl)
    }) : /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledImage, {
      src: imgUrl,
      width: 19
    }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledTitle, {
      fontSize: fontSize,
      fontWeight: fontWeight,
      letterSpacing: letterSpacing,
      lineHeight: lineHeight,
      children: title
    })]
  });
};

TitleIcon.propTypes = {
  fontSize: _propTypes.default.string,
  fontWeight: _propTypes.default.string,
  imgUrl: _propTypes.default.string,
  isFontIcon: _propTypes.default.bool,
  letterSpacing: _propTypes.default.string,
  lineHeight: _propTypes.default.string,
  marginBottom: _propTypes.default.string,
  marginTop: _propTypes.default.string,
  title: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string])
};
var _default = TitleIcon;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1RpdGxlSWNvbi5qcyJdLCJuYW1lcyI6WyJTdHlsZWRTZWN0aW9uIiwic3R5bGVkIiwiZGl2IiwibWFyZ2luQm90dG9tIiwibWFyZ2luVG9wIiwiU3R5bGVkSW1hZ2UiLCJpbWciLCJTdHlsZWRUaXRsZSIsImZvbnRTaXplIiwiZm9udFdlaWdodCIsImxldHRlclNwYWNpbmciLCJsaW5lSGVpZ2h0IiwiRm9udEljb24iLCJpIiwiVGl0bGVJY29uIiwiaW1nVXJsIiwiaXNGb250SWNvbiIsInRpdGxlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwiYm9vbCIsIm9uZU9mVHlwZSIsIm9iamVjdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsYUFBYSxHQUFHQywwQkFBT0MsR0FBViwrTEFJRTtBQUFBLE1BQUdDLFlBQUgsUUFBR0EsWUFBSDtBQUFBLFNBQXNCQSxZQUF0QjtBQUFBLENBSkYsRUFLRDtBQUFBLE1BQUdDLFNBQUgsU0FBR0EsU0FBSDtBQUFBLFNBQW1CQSxTQUFuQjtBQUFBLENBTEMsQ0FBbkI7O0FBUUEsSUFBTUMsV0FBVyxHQUFHSiwwQkFBT0ssR0FBVix3SEFBakI7O0FBS0EsSUFBTUMsV0FBVyxHQUFHTiwwQkFBT0MsR0FBVixvU0FFQTtBQUFBLE1BQUdNLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQWtCQSxRQUFsQjtBQUFBLENBRkEsRUFHRTtBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLFNBQW9CQSxVQUFwQjtBQUFBLENBSEYsRUFJSztBQUFBLE1BQUdDLGFBQUgsU0FBR0EsYUFBSDtBQUFBLFNBQXVCQSxhQUF2QjtBQUFBLENBSkwsRUFLRTtBQUFBLE1BQUdDLFVBQUgsU0FBR0EsVUFBSDtBQUFBLFNBQW9CQSxVQUFwQjtBQUFBLENBTEYsQ0FBakI7O0FBWUEsSUFBTUMsUUFBUSxHQUFHWCwwQkFBT1ksQ0FBViw2SUFBZDs7QUFNQSxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxRQVVaO0FBQUEsNkJBVEZOLFFBU0U7QUFBQSxNQVRGQSxRQVNFLCtCQVRTLE1BU1Q7QUFBQSwrQkFSRkMsVUFRRTtBQUFBLE1BUkZBLFVBUUUsaUNBUlcsUUFRWDtBQUFBLE1BUEZNLE1BT0UsU0FQRkEsTUFPRTtBQUFBLE1BTkZDLFVBTUUsU0FORkEsVUFNRTtBQUFBLGtDQUxGTixhQUtFO0FBQUEsTUFMRkEsYUFLRSxvQ0FMYyxRQUtkO0FBQUEsK0JBSkZDLFVBSUU7QUFBQSxNQUpGQSxVQUlFLGlDQUpXLE1BSVg7QUFBQSxpQ0FIRlIsWUFHRTtBQUFBLE1BSEZBLFlBR0UsbUNBSGEsR0FHYjtBQUFBLDhCQUZGQyxTQUVFO0FBQUEsTUFGRkEsU0FFRSxnQ0FGVSxHQUVWO0FBQUEsTUFERmEsS0FDRSxTQURGQSxLQUNFO0FBQ0Ysc0JBQ0ksc0JBQUMsYUFBRDtBQUFlLElBQUEsWUFBWSxFQUFFZCxZQUE3QjtBQUEyQyxJQUFBLFNBQVMsRUFBRUMsU0FBdEQ7QUFBQSxlQUNLLENBQUNXLE1BQUQsR0FBVSxJQUFWLEdBQWlCQyxVQUFVLGdCQUN4QixxQkFBQyxRQUFEO0FBQVUsTUFBQSxTQUFTLGVBQVFELE1BQVI7QUFBbkIsTUFEd0IsZ0JBR3hCLHFCQUFDLFdBQUQ7QUFBYSxNQUFBLEdBQUcsRUFBRUEsTUFBbEI7QUFBMEIsTUFBQSxLQUFLLEVBQUU7QUFBakMsTUFKUixlQU9JLHFCQUFDLFdBQUQ7QUFDSSxNQUFBLFFBQVEsRUFBRVAsUUFEZDtBQUVJLE1BQUEsVUFBVSxFQUFFQyxVQUZoQjtBQUdJLE1BQUEsYUFBYSxFQUFFQyxhQUhuQjtBQUlJLE1BQUEsVUFBVSxFQUFFQyxVQUpoQjtBQUFBLGdCQU1LTTtBQU5MLE1BUEo7QUFBQSxJQURKO0FBa0JILENBN0JEOztBQStCQUgsU0FBUyxDQUFDSSxTQUFWLEdBQXNCO0FBQ2xCVixFQUFBQSxRQUFRLEVBQUVXLG1CQUFVQyxNQURGO0FBRWxCWCxFQUFBQSxVQUFVLEVBQUVVLG1CQUFVQyxNQUZKO0FBR2xCTCxFQUFBQSxNQUFNLEVBQUVJLG1CQUFVQyxNQUhBO0FBSWxCSixFQUFBQSxVQUFVLEVBQUVHLG1CQUFVRSxJQUpKO0FBS2xCWCxFQUFBQSxhQUFhLEVBQUVTLG1CQUFVQyxNQUxQO0FBTWxCVCxFQUFBQSxVQUFVLEVBQUVRLG1CQUFVQyxNQU5KO0FBT2xCakIsRUFBQUEsWUFBWSxFQUFFZ0IsbUJBQVVDLE1BUE47QUFRbEJoQixFQUFBQSxTQUFTLEVBQUVlLG1CQUFVQyxNQVJIO0FBU2xCSCxFQUFBQSxLQUFLLEVBQUVFLG1CQUFVRyxTQUFWLENBQW9CLENBQUNILG1CQUFVSSxNQUFYLEVBQW1CSixtQkFBVUMsTUFBN0IsQ0FBcEI7QUFUVyxDQUF0QjtlQVllTixTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBTdHlsZWRTZWN0aW9uID0gc3R5bGVkLmRpdmBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbGluZS1oZWlnaHQ6IDQ0cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogJHsoeyBtYXJnaW5Cb3R0b20gfSkgPT4gbWFyZ2luQm90dG9tfTtcbiAgICBtYXJnaW4tdG9wOiAkeyh7IG1hcmdpblRvcCB9KSA9PiBtYXJnaW5Ub3B9O1xuYDtcblxuY29uc3QgU3R5bGVkSW1hZ2UgPSBzdHlsZWQuaW1nYFxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG5gO1xuXG5jb25zdCBTdHlsZWRUaXRsZSA9IHN0eWxlZC5kaXZgXG4gICAgY29sb3I6ICMyMDIyNTM7XG4gICAgZm9udC1zaXplOiAkeyh7IGZvbnRTaXplIH0pID0+IGZvbnRTaXplfTtcbiAgICBmb250LXdlaWdodDogJHsoeyBmb250V2VpZ2h0IH0pID0+IGZvbnRXZWlnaHR9O1xuICAgIGxldHRlci1zcGFjaW5nOiAkeyh7IGxldHRlclNwYWNpbmcgfSkgPT4gbGV0dGVyU3BhY2luZ307XG4gICAgbGluZS1oZWlnaHQ6ICR7KHsgbGluZUhlaWdodCB9KSA9PiBsaW5lSGVpZ2h0fTtcbiAgICBtYXgtd2lkdGg6IDkwJTtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG5gO1xuXG5jb25zdCBGb250SWNvbiA9IHN0eWxlZC5pYFxuICAgIGNvbG9yOiAjZmY5YTAwO1xuICAgIGZvbnQtc2l6ZTogMjFweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG5gO1xuXG5jb25zdCBUaXRsZUljb24gPSAoe1xuICAgIGZvbnRTaXplID0gJzIycHgnLFxuICAgIGZvbnRXZWlnaHQgPSAnbm9ybWFsJyxcbiAgICBpbWdVcmwsXG4gICAgaXNGb250SWNvbixcbiAgICBsZXR0ZXJTcGFjaW5nID0gJzAuMTVweCcsXG4gICAgbGluZUhlaWdodCA9ICcyN3B4JyxcbiAgICBtYXJnaW5Cb3R0b20gPSAnMCcsXG4gICAgbWFyZ2luVG9wID0gJzAnLFxuICAgIHRpdGxlLFxufSkgPT4ge1xuICAgIHJldHVybiAoXG4gICAgICAgIDxTdHlsZWRTZWN0aW9uIG1hcmdpbkJvdHRvbT17bWFyZ2luQm90dG9tfSBtYXJnaW5Ub3A9e21hcmdpblRvcH0+XG4gICAgICAgICAgICB7IWltZ1VybCA/IG51bGwgOiBpc0ZvbnRJY29uID8gKFxuICAgICAgICAgICAgICAgIDxGb250SWNvbiBjbGFzc05hbWU9e2BmYSAke2ltZ1VybH1gfSAvPlxuICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICA8U3R5bGVkSW1hZ2Ugc3JjPXtpbWdVcmx9IHdpZHRoPXsxOX0gLz5cbiAgICAgICAgICAgICl9XG5cbiAgICAgICAgICAgIDxTdHlsZWRUaXRsZVxuICAgICAgICAgICAgICAgIGZvbnRTaXplPXtmb250U2l6ZX1cbiAgICAgICAgICAgICAgICBmb250V2VpZ2h0PXtmb250V2VpZ2h0fVxuICAgICAgICAgICAgICAgIGxldHRlclNwYWNpbmc9e2xldHRlclNwYWNpbmd9XG4gICAgICAgICAgICAgICAgbGluZUhlaWdodD17bGluZUhlaWdodH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7dGl0bGV9XG4gICAgICAgICAgICA8L1N0eWxlZFRpdGxlPlxuICAgICAgICA8L1N0eWxlZFNlY3Rpb24+XG4gICAgKTtcbn07XG5cblRpdGxlSWNvbi5wcm9wVHlwZXMgPSB7XG4gICAgZm9udFNpemU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZm9udFdlaWdodDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpbWdVcmw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaXNGb250SWNvbjogUHJvcFR5cGVzLmJvb2wsXG4gICAgbGV0dGVyU3BhY2luZzogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBsaW5lSGVpZ2h0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG1hcmdpbkJvdHRvbTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBtYXJnaW5Ub3A6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdGl0bGU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5vYmplY3QsIFByb3BUeXBlcy5zdHJpbmddKSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFRpdGxlSWNvbjtcbiJdfQ==