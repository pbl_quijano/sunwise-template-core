"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactScroll = require("react-scroll");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _LabelError = _interopRequireDefault(require("./LabelError"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WrapperInput = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    select {\n        &.with-error {\n            background-color: #ffe3e3;\n            border-color: red;\n        }\n        ", "\n    }\n\n    option[disabled] {\n        color: lightgray;\n    }\n\n    .form-group > label {\n        width: 90%;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        overflow: hidden;\n    }\n"])), function (_ref) {
  var disabled = _ref.disabled;
  return disabled && "\n            background-color: #F6F8FA;\n        ";
});

var ReduxFieldSelectComponent = function ReduxFieldSelectComponent(props) {
  var _props$input = props.input,
      name = _props$input.name,
      value = _props$input.value,
      onChange = _props$input.onChange,
      _props$meta = props.meta,
      touched = _props$meta.touched,
      error = _props$meta.error,
      placeholder = props.placeholder,
      label = props.label,
      controlConfig = props.controlConfig,
      appendIcon = props.appendIcon,
      disabled = props.disabled,
      _props$options = props.options,
      options = _props$options === void 0 ? [] : _props$options,
      prepend = props.prepend,
      prependIcon = props.prependIcon,
      prependSvgIcon = props.prependSvgIcon,
      _props$className = props.className,
      className = _props$className === void 0 ? '' : _props$className,
      _props$classNameFormG = props.classNameFormGroup,
      classNameFormGroup = _props$classNameFormG === void 0 ? '' : _props$classNameFormG,
      id = props.id,
      required = props.required,
      _props$ignore = props.ignore,
      ignore = _props$ignore === void 0 ? [] : _props$ignore;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(WrapperInput, {
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
      className: classNameFormGroup,
      children: [label && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Label, {
        title: label,
        children: label
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.InputGroup, {
        children: [(prepend || prependIcon || prependSvgIcon) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.InputGroup.Prepend, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.InputGroup.Text, {
            children: [prependIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
              className: prependIcon
            }), prependSvgIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
              alt: "",
              src: prependSvgIcon
            }), prepend]
          })
        }), name && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactScroll.Element, {
          name: "position-".concat(name)
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Control, _objectSpread(_objectSpread({
          placeholder: placeholder,
          value: value,
          onChange: onChange,
          disabled: disabled,
          as: "select"
        }, controlConfig), {}, {
          id: id,
          className: "".concat(className, " ").concat(touched && error && 'with-error', " ").concat(required && (value === null || value === '' || ignore.indexOf(value) > -1) ? 'border-danger' : ''),
          children: options.map(function (_ref2) {
            var label = _ref2.label,
                value = _ref2.value,
                optionDisabled = _ref2.disabled;
            return /*#__PURE__*/(0, _jsxRuntime.jsx)("option", {
              value: value,
              disabled: optionDisabled,
              children: label
            }, "select-".concat(value, "-").concat(label));
          })
        })), appendIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.InputGroup.Append, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.InputGroup.Text, {
            id: "inputGroupPrepend",
            children: /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
              alt: "",
              src: appendIcon
            })
          })
        })]
      }), touched && error && /*#__PURE__*/(0, _jsxRuntime.jsxs)(_LabelError.default, {
        type: "error",
        children: [error, " "]
      })]
    })
  });
};

ReduxFieldSelectComponent.propTypes = {
  input: _propTypes.default.object,
  meta: _propTypes.default.object,
  placeholder: _propTypes.default.string,
  label: _propTypes.default.string,
  controlConfig: _propTypes.default.object,
  appendIcon: _propTypes.default.string,
  disabled: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.bool]),
  options: _propTypes.default.array,
  prepend: _propTypes.default.string,
  prependIcon: _propTypes.default.string,
  prependSvgIcon: _propTypes.default.string,
  className: _propTypes.default.string,
  classNameFormGroup: _propTypes.default.string,
  id: _propTypes.default.string,
  required: _propTypes.default.bool,
  ignore: _propTypes.default.array,
  t: _propTypes.default.func
};
var _default = ReduxFieldSelectComponent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1JlZHV4RmllbGRTZWxlY3QuanMiXSwibmFtZXMiOlsiV3JhcHBlcklucHV0Iiwic3R5bGVkIiwiZGl2IiwiZGlzYWJsZWQiLCJSZWR1eEZpZWxkU2VsZWN0Q29tcG9uZW50IiwicHJvcHMiLCJpbnB1dCIsIm5hbWUiLCJ2YWx1ZSIsIm9uQ2hhbmdlIiwibWV0YSIsInRvdWNoZWQiLCJlcnJvciIsInBsYWNlaG9sZGVyIiwibGFiZWwiLCJjb250cm9sQ29uZmlnIiwiYXBwZW5kSWNvbiIsIm9wdGlvbnMiLCJwcmVwZW5kIiwicHJlcGVuZEljb24iLCJwcmVwZW5kU3ZnSWNvbiIsImNsYXNzTmFtZSIsImNsYXNzTmFtZUZvcm1Hcm91cCIsImlkIiwicmVxdWlyZWQiLCJpZ25vcmUiLCJpbmRleE9mIiwibWFwIiwib3B0aW9uRGlzYWJsZWQiLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJvYmplY3QiLCJzdHJpbmciLCJvbmVPZlR5cGUiLCJib29sIiwiYXJyYXkiLCJ0IiwiZnVuYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHQywwQkFBT0MsR0FBVixzYUFNUjtBQUFBLE1BQUdDLFFBQUgsUUFBR0EsUUFBSDtBQUFBLFNBQ0VBLFFBQVEsd0RBRFY7QUFBQSxDQU5RLENBQWxCOztBQXlCQSxJQUFNQyx5QkFBeUIsR0FBRyxTQUE1QkEseUJBQTRCLENBQUNDLEtBQUQsRUFBVztBQUN6QyxxQkFpQklBLEtBakJKLENBQ0lDLEtBREo7QUFBQSxNQUNhQyxJQURiLGdCQUNhQSxJQURiO0FBQUEsTUFDbUJDLEtBRG5CLGdCQUNtQkEsS0FEbkI7QUFBQSxNQUMwQkMsUUFEMUIsZ0JBQzBCQSxRQUQxQjtBQUFBLG9CQWlCSUosS0FqQkosQ0FFSUssSUFGSjtBQUFBLE1BRVlDLE9BRlosZUFFWUEsT0FGWjtBQUFBLE1BRXFCQyxLQUZyQixlQUVxQkEsS0FGckI7QUFBQSxNQUdJQyxXQUhKLEdBaUJJUixLQWpCSixDQUdJUSxXQUhKO0FBQUEsTUFJSUMsS0FKSixHQWlCSVQsS0FqQkosQ0FJSVMsS0FKSjtBQUFBLE1BS0lDLGFBTEosR0FpQklWLEtBakJKLENBS0lVLGFBTEo7QUFBQSxNQU1JQyxVQU5KLEdBaUJJWCxLQWpCSixDQU1JVyxVQU5KO0FBQUEsTUFPSWIsUUFQSixHQWlCSUUsS0FqQkosQ0FPSUYsUUFQSjtBQUFBLHVCQWlCSUUsS0FqQkosQ0FRSVksT0FSSjtBQUFBLE1BUUlBLE9BUkosK0JBUWMsRUFSZDtBQUFBLE1BU0lDLE9BVEosR0FpQkliLEtBakJKLENBU0lhLE9BVEo7QUFBQSxNQVVJQyxXQVZKLEdBaUJJZCxLQWpCSixDQVVJYyxXQVZKO0FBQUEsTUFXSUMsY0FYSixHQWlCSWYsS0FqQkosQ0FXSWUsY0FYSjtBQUFBLHlCQWlCSWYsS0FqQkosQ0FZSWdCLFNBWko7QUFBQSxNQVlJQSxTQVpKLGlDQVlnQixFQVpoQjtBQUFBLDhCQWlCSWhCLEtBakJKLENBYUlpQixrQkFiSjtBQUFBLE1BYUlBLGtCQWJKLHNDQWF5QixFQWJ6QjtBQUFBLE1BY0lDLEVBZEosR0FpQklsQixLQWpCSixDQWNJa0IsRUFkSjtBQUFBLE1BZUlDLFFBZkosR0FpQkluQixLQWpCSixDQWVJbUIsUUFmSjtBQUFBLHNCQWlCSW5CLEtBakJKLENBZ0JJb0IsTUFoQko7QUFBQSxNQWdCSUEsTUFoQkosOEJBZ0JhLEVBaEJiO0FBa0JBLHNCQUNJLHFCQUFDLFlBQUQ7QUFBQSwyQkFDSSxzQkFBQyxvQkFBRCxDQUFNLEtBQU47QUFBWSxNQUFBLFNBQVMsRUFBRUgsa0JBQXZCO0FBQUEsaUJBQ0tSLEtBQUssaUJBQUkscUJBQUMsb0JBQUQsQ0FBTSxLQUFOO0FBQVksUUFBQSxLQUFLLEVBQUVBLEtBQW5CO0FBQUEsa0JBQTJCQTtBQUEzQixRQURkLGVBR0ksc0JBQUMsMEJBQUQ7QUFBQSxtQkFDSyxDQUFDSSxPQUFPLElBQUlDLFdBQVgsSUFBMEJDLGNBQTNCLGtCQUNHLHFCQUFDLDBCQUFELENBQVksT0FBWjtBQUFBLGlDQUNJLHNCQUFDLDBCQUFELENBQVksSUFBWjtBQUFBLHVCQUNLRCxXQUFXLGlCQUFJO0FBQUcsY0FBQSxTQUFTLEVBQUVBO0FBQWQsY0FEcEIsRUFFS0MsY0FBYyxpQkFDWDtBQUFLLGNBQUEsR0FBRyxFQUFDLEVBQVQ7QUFBWSxjQUFBLEdBQUcsRUFBRUE7QUFBakIsY0FIUixFQUtLRixPQUxMO0FBQUE7QUFESixVQUZSLEVBWUtYLElBQUksaUJBQUkscUJBQUMsb0JBQUQ7QUFBUyxVQUFBLElBQUkscUJBQWNBLElBQWQ7QUFBYixVQVpiLGVBYUkscUJBQUMsb0JBQUQsQ0FBTSxPQUFOO0FBQ0ksVUFBQSxXQUFXLEVBQUVNLFdBRGpCO0FBRUksVUFBQSxLQUFLLEVBQUVMLEtBRlg7QUFHSSxVQUFBLFFBQVEsRUFBRUMsUUFIZDtBQUlJLFVBQUEsUUFBUSxFQUFFTixRQUpkO0FBS0ksVUFBQSxFQUFFLEVBQUM7QUFMUCxXQU1RWSxhQU5SO0FBT0ksVUFBQSxFQUFFLEVBQUVRLEVBUFI7QUFRSSxVQUFBLFNBQVMsWUFBS0YsU0FBTCxjQUNMVixPQUFPLElBQUlDLEtBQVgsSUFBb0IsWUFEZixjQUdMWSxRQUFRLEtBQ1BoQixLQUFLLEtBQUssSUFBVixJQUNHQSxLQUFLLEtBQUssRUFEYixJQUVHaUIsTUFBTSxDQUFDQyxPQUFQLENBQWVsQixLQUFmLElBQXdCLENBQUMsQ0FIckIsQ0FBUixHQUlNLGVBSk4sR0FLTSxFQVJELENBUmI7QUFBQSxvQkFtQktTLE9BQU8sQ0FBQ1UsR0FBUixDQUNHO0FBQUEsZ0JBQUdiLEtBQUgsU0FBR0EsS0FBSDtBQUFBLGdCQUFVTixLQUFWLFNBQVVBLEtBQVY7QUFBQSxnQkFBMkJvQixjQUEzQixTQUFpQnpCLFFBQWpCO0FBQUEsZ0NBQ0k7QUFFSSxjQUFBLEtBQUssRUFBRUssS0FGWDtBQUdJLGNBQUEsUUFBUSxFQUFFb0IsY0FIZDtBQUFBLHdCQUtLZDtBQUxMLGdDQUNtQk4sS0FEbkIsY0FDNEJNLEtBRDVCLEVBREo7QUFBQSxXQURIO0FBbkJMLFdBYkosRUE0Q0tFLFVBQVUsaUJBQ1AscUJBQUMsMEJBQUQsQ0FBWSxNQUFaO0FBQUEsaUNBQ0kscUJBQUMsMEJBQUQsQ0FBWSxJQUFaO0FBQWlCLFlBQUEsRUFBRSxFQUFDLG1CQUFwQjtBQUFBLG1DQUNJO0FBQUssY0FBQSxHQUFHLEVBQUMsRUFBVDtBQUFZLGNBQUEsR0FBRyxFQUFFQTtBQUFqQjtBQURKO0FBREosVUE3Q1I7QUFBQSxRQUhKLEVBdURLTCxPQUFPLElBQUlDLEtBQVgsaUJBQ0csc0JBQUMsbUJBQUQ7QUFBWSxRQUFBLElBQUksRUFBQyxPQUFqQjtBQUFBLG1CQUEwQkEsS0FBMUI7QUFBQSxRQXhEUjtBQUFBO0FBREosSUFESjtBQStESCxDQWxGRDs7QUFvRkFSLHlCQUF5QixDQUFDeUIsU0FBMUIsR0FBc0M7QUFDbEN2QixFQUFBQSxLQUFLLEVBQUV3QixtQkFBVUMsTUFEaUI7QUFFbENyQixFQUFBQSxJQUFJLEVBQUVvQixtQkFBVUMsTUFGa0I7QUFHbENsQixFQUFBQSxXQUFXLEVBQUVpQixtQkFBVUUsTUFIVztBQUlsQ2xCLEVBQUFBLEtBQUssRUFBRWdCLG1CQUFVRSxNQUppQjtBQUtsQ2pCLEVBQUFBLGFBQWEsRUFBRWUsbUJBQVVDLE1BTFM7QUFNbENmLEVBQUFBLFVBQVUsRUFBRWMsbUJBQVVFLE1BTlk7QUFPbEM3QixFQUFBQSxRQUFRLEVBQUUyQixtQkFBVUcsU0FBVixDQUFvQixDQUFDSCxtQkFBVUUsTUFBWCxFQUFtQkYsbUJBQVVJLElBQTdCLENBQXBCLENBUHdCO0FBUWxDakIsRUFBQUEsT0FBTyxFQUFFYSxtQkFBVUssS0FSZTtBQVNsQ2pCLEVBQUFBLE9BQU8sRUFBRVksbUJBQVVFLE1BVGU7QUFVbENiLEVBQUFBLFdBQVcsRUFBRVcsbUJBQVVFLE1BVlc7QUFXbENaLEVBQUFBLGNBQWMsRUFBRVUsbUJBQVVFLE1BWFE7QUFZbENYLEVBQUFBLFNBQVMsRUFBRVMsbUJBQVVFLE1BWmE7QUFhbENWLEVBQUFBLGtCQUFrQixFQUFFUSxtQkFBVUUsTUFiSTtBQWNsQ1QsRUFBQUEsRUFBRSxFQUFFTyxtQkFBVUUsTUFkb0I7QUFlbENSLEVBQUFBLFFBQVEsRUFBRU0sbUJBQVVJLElBZmM7QUFnQmxDVCxFQUFBQSxNQUFNLEVBQUVLLG1CQUFVSyxLQWhCZ0I7QUFpQmxDQyxFQUFBQSxDQUFDLEVBQUVOLG1CQUFVTztBQWpCcUIsQ0FBdEM7ZUFvQmVqQyx5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBJbnB1dEdyb3VwLCBGb3JtIH0gZnJvbSAncmVhY3QtYm9vdHN0cmFwJztcbmltcG9ydCB7IEVsZW1lbnQgfSBmcm9tICdyZWFjdC1zY3JvbGwnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmltcG9ydCBMYWJlbEVycm9yIGZyb20gJy4vTGFiZWxFcnJvcic7XG5cbmNvbnN0IFdyYXBwZXJJbnB1dCA9IHN0eWxlZC5kaXZgXG4gICAgc2VsZWN0IHtcbiAgICAgICAgJi53aXRoLWVycm9yIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmUzZTM7XG4gICAgICAgICAgICBib3JkZXItY29sb3I6IHJlZDtcbiAgICAgICAgfVxuICAgICAgICAkeyh7IGRpc2FibGVkIH0pID0+XG4gICAgICAgICAgICBkaXNhYmxlZCAmJlxuICAgICAgICAgICAgYFxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0Y2RjhGQTtcbiAgICAgICAgYH1cbiAgICB9XG5cbiAgICBvcHRpb25bZGlzYWJsZWRdIHtcbiAgICAgICAgY29sb3I6IGxpZ2h0Z3JheTtcbiAgICB9XG5cbiAgICAuZm9ybS1ncm91cCA+IGxhYmVsIHtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgfVxuYDtcblxuY29uc3QgUmVkdXhGaWVsZFNlbGVjdENvbXBvbmVudCA9IChwcm9wcykgPT4ge1xuICAgIGNvbnN0IHtcbiAgICAgICAgaW5wdXQ6IHsgbmFtZSwgdmFsdWUsIG9uQ2hhbmdlIH0sXG4gICAgICAgIG1ldGE6IHsgdG91Y2hlZCwgZXJyb3IgfSxcbiAgICAgICAgcGxhY2Vob2xkZXIsXG4gICAgICAgIGxhYmVsLFxuICAgICAgICBjb250cm9sQ29uZmlnLFxuICAgICAgICBhcHBlbmRJY29uLFxuICAgICAgICBkaXNhYmxlZCxcbiAgICAgICAgb3B0aW9ucyA9IFtdLFxuICAgICAgICBwcmVwZW5kLFxuICAgICAgICBwcmVwZW5kSWNvbixcbiAgICAgICAgcHJlcGVuZFN2Z0ljb24sXG4gICAgICAgIGNsYXNzTmFtZSA9ICcnLFxuICAgICAgICBjbGFzc05hbWVGb3JtR3JvdXAgPSAnJyxcbiAgICAgICAgaWQsXG4gICAgICAgIHJlcXVpcmVkLFxuICAgICAgICBpZ25vcmUgPSBbXSxcbiAgICB9ID0gcHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgICAgPFdyYXBwZXJJbnB1dD5cbiAgICAgICAgICAgIDxGb3JtLkdyb3VwIGNsYXNzTmFtZT17Y2xhc3NOYW1lRm9ybUdyb3VwfT5cbiAgICAgICAgICAgICAgICB7bGFiZWwgJiYgPEZvcm0uTGFiZWwgdGl0bGU9e2xhYmVsfT57bGFiZWx9PC9Gb3JtLkxhYmVsPn1cblxuICAgICAgICAgICAgICAgIDxJbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgICAgICB7KHByZXBlbmQgfHwgcHJlcGVuZEljb24gfHwgcHJlcGVuZFN2Z0ljb24pICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dEdyb3VwLlByZXBlbmQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPElucHV0R3JvdXAuVGV4dD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3ByZXBlbmRJY29uICYmIDxpIGNsYXNzTmFtZT17cHJlcGVuZEljb259IC8+fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7cHJlcGVuZFN2Z0ljb24gJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBhbHQ9XCJcIiBzcmM9e3ByZXBlbmRTdmdJY29ufSAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7cHJlcGVuZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0lucHV0R3JvdXAuVGV4dD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW5wdXRHcm91cC5QcmVwZW5kPlxuICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICB7bmFtZSAmJiA8RWxlbWVudCBuYW1lPXtgcG9zaXRpb24tJHtuYW1lfWB9IC8+fVxuICAgICAgICAgICAgICAgICAgICA8Rm9ybS5Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17ZGlzYWJsZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICBhcz1cInNlbGVjdFwiXG4gICAgICAgICAgICAgICAgICAgICAgICB7Li4uY29udHJvbENvbmZpZ31cbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPXtpZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7Y2xhc3NOYW1lfSAke1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdWNoZWQgJiYgZXJyb3IgJiYgJ3dpdGgtZXJyb3InXG4gICAgICAgICAgICAgICAgICAgICAgICB9ICR7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAodmFsdWUgPT09IG51bGwgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPT09ICcnIHx8XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlnbm9yZS5pbmRleE9mKHZhbHVlKSA+IC0xKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/ICdib3JkZXItZGFuZ2VyJ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6ICcnXG4gICAgICAgICAgICAgICAgICAgICAgICB9YH1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAge29wdGlvbnMubWFwKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICh7IGxhYmVsLCB2YWx1ZSwgZGlzYWJsZWQ6IG9wdGlvbkRpc2FibGVkIH0pID0+IChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPG9wdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5PXtgc2VsZWN0LSR7dmFsdWV9LSR7bGFiZWx9YH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtvcHRpb25EaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2xhYmVsfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgICA8L0Zvcm0uQ29udHJvbD5cbiAgICAgICAgICAgICAgICAgICAge2FwcGVuZEljb24gJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPElucHV0R3JvdXAuQXBwZW5kPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dEdyb3VwLlRleHQgaWQ9XCJpbnB1dEdyb3VwUHJlcGVuZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIGFsdD1cIlwiIHNyYz17YXBwZW5kSWNvbn0gLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0lucHV0R3JvdXAuVGV4dD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW5wdXRHcm91cC5BcHBlbmQ+XG4gICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9JbnB1dEdyb3VwPlxuICAgICAgICAgICAgICAgIHt0b3VjaGVkICYmIGVycm9yICYmIChcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsRXJyb3IgdHlwZT1cImVycm9yXCI+e2Vycm9yfSA8L0xhYmVsRXJyb3I+XG4gICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIDwvRm9ybS5Hcm91cD5cbiAgICAgICAgPC9XcmFwcGVySW5wdXQ+XG4gICAgKTtcbn07XG5cblJlZHV4RmllbGRTZWxlY3RDb21wb25lbnQucHJvcFR5cGVzID0ge1xuICAgIGlucHV0OiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIG1ldGE6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgbGFiZWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29udHJvbENvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBhcHBlbmRJY29uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMuYm9vbF0pLFxuICAgIG9wdGlvbnM6IFByb3BUeXBlcy5hcnJheSxcbiAgICBwcmVwZW5kOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHByZXBlbmRJY29uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHByZXBlbmRTdmdJY29uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjbGFzc05hbWVGb3JtR3JvdXA6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaWQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgcmVxdWlyZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIGlnbm9yZTogUHJvcFR5cGVzLmFycmF5LFxuICAgIHQ6IFByb3BUeXBlcy5mdW5jLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgUmVkdXhGaWVsZFNlbGVjdENvbXBvbmVudDtcbiJdfQ==