"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    input,\n    textarea {\n        &.with-error {\n            background-color: #ffe3e3;\n            border-color: red;\n        }\n        &.rpu-field {\n            letter-spacing: 4px;\n            font-size: 16px;\n        }\n        &::placeholder {\n            color: #ccd1e8;\n            opacity: 1;\n        }\n\n        &:-ms-input-placeholder {\n            color: red;\n        }\n\n        &::-ms-input-placeholder {\n            color: red;\n        }\n    }\n    select {\n        option:disabled {\n            color: #ccd1e8;\n        }\n        &.with-error {\n            background-color: #ffe3e3;\n            border-color: red;\n        }\n    }\n\n    .form-group > label {\n        width: 90%;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        overflow: hidden;\n    }\n    .input-group-text {\n        border: 0;\n    }\n"])));

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1dyYXBwZXJJbnB1dC5qcyJdLCJuYW1lcyI6WyJzdHlsZWQiLCJkaXYiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7Ozs7ZUFFZUEsMEJBQU9DLEciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGVkLmRpdmBcbiAgICBpbnB1dCxcbiAgICB0ZXh0YXJlYSB7XG4gICAgICAgICYud2l0aC1lcnJvciB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2UzO1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZWQ7XG4gICAgICAgIH1cbiAgICAgICAgJi5ycHUtZmllbGQge1xuICAgICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDRweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgICAgICAmOjpwbGFjZWhvbGRlciB7XG4gICAgICAgICAgICBjb2xvcjogI2NjZDFlODtcbiAgICAgICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICAgIH1cblxuICAgICAgICAmOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgICAgICAgICBjb2xvcjogcmVkO1xuICAgICAgICB9XG5cbiAgICAgICAgJjo6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgICAgIGNvbG9yOiByZWQ7XG4gICAgICAgIH1cbiAgICB9XG4gICAgc2VsZWN0IHtcbiAgICAgICAgb3B0aW9uOmRpc2FibGVkIHtcbiAgICAgICAgICAgIGNvbG9yOiAjY2NkMWU4O1xuICAgICAgICB9XG4gICAgICAgICYud2l0aC1lcnJvciB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZlM2UzO1xuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZWQ7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZm9ybS1ncm91cCA+IGxhYmVsIHtcbiAgICAgICAgd2lkdGg6IDkwJTtcbiAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgfVxuICAgIC5pbnB1dC1ncm91cC10ZXh0IHtcbiAgICAgICAgYm9yZGVyOiAwO1xuICAgIH1cbmA7XG4iXX0=