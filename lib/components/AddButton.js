"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledButton = _styledComponents.default.button(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    background: linear-gradient(208.97deg, #ffcb01 0%, #e7a804 100%);\n    border-radius: 18px;\n    border: 0 none;\n    color: #ffffff;\n    font-size: 14px;\n    font-weight: bold;\n    line-height: 18px;\n    margin-bottom: 1em;\n    padding: 10px;\n    text-align: center;\n    white-space: nowrap !important;\n    width: 100%;\n\n    &[disabled] {\n        background: linear-gradient(208.97deg, #848bab 0%, #848bab 100%);\n    }\n\n    ", "\n"])), function (_ref) {
  var size = _ref.size;
  return size === 'small' && "color: #EFF1FB;\n        font-size: 12px;\n        letter-spacing: 0.31px;\n        line-height: 18px;\n        padding: 7px 25px;\n        width: auto;\n    ";
});

var AddButton = function AddButton(_ref2) {
  var children = _ref2.children,
      className = _ref2.className,
      disabled = _ref2.disabled,
      onClick = _ref2.onClick,
      _ref2$size = _ref2.size,
      size = _ref2$size === void 0 ? 'md' : _ref2$size,
      style = _ref2.style,
      _ref2$type = _ref2.type,
      type = _ref2$type === void 0 ? 'button' : _ref2$type,
      variant = _ref2.variant,
      _ref2$visible = _ref2.visible,
      visible = _ref2$visible === void 0 ? true : _ref2$visible;
  return visible ? /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledButton, {
    className: className,
    disabled: disabled,
    onClick: onClick,
    size: size,
    variant: variant,
    style: style,
    type: type,
    children: children
  }) : null;
};

AddButton.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.array, _propTypes.default.element, _propTypes.default.string]),
  className: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  onClick: _propTypes.default.func,
  size: _propTypes.default.string,
  style: _propTypes.default.string,
  type: _propTypes.default.string,
  variant: _propTypes.default.string,
  visible: _propTypes.default.bool
};
var _default = AddButton;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0FkZEJ1dHRvbi5qcyJdLCJuYW1lcyI6WyJTdHlsZWRCdXR0b24iLCJzdHlsZWQiLCJidXR0b24iLCJzaXplIiwiQWRkQnV0dG9uIiwiY2hpbGRyZW4iLCJjbGFzc05hbWUiLCJkaXNhYmxlZCIsIm9uQ2xpY2siLCJzdHlsZSIsInR5cGUiLCJ2YXJpYW50IiwidmlzaWJsZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsIm9uZU9mVHlwZSIsImFycmF5IiwiZWxlbWVudCIsInN0cmluZyIsImJvb2wiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxZQUFZLEdBQUdDLDBCQUFPQyxNQUFWLHlnQkFrQlo7QUFBQSxNQUFHQyxJQUFILFFBQUdBLElBQUg7QUFBQSxTQUNFQSxJQUFJLEtBQUssT0FBVCxvS0FERjtBQUFBLENBbEJZLENBQWxCOztBQTZCQSxJQUFNQyxTQUFTLEdBQUcsU0FBWkEsU0FBWSxRQVVaO0FBQUEsTUFURkMsUUFTRSxTQVRGQSxRQVNFO0FBQUEsTUFSRkMsU0FRRSxTQVJGQSxTQVFFO0FBQUEsTUFQRkMsUUFPRSxTQVBGQSxRQU9FO0FBQUEsTUFORkMsT0FNRSxTQU5GQSxPQU1FO0FBQUEseUJBTEZMLElBS0U7QUFBQSxNQUxGQSxJQUtFLDJCQUxLLElBS0w7QUFBQSxNQUpGTSxLQUlFLFNBSkZBLEtBSUU7QUFBQSx5QkFIRkMsSUFHRTtBQUFBLE1BSEZBLElBR0UsMkJBSEssUUFHTDtBQUFBLE1BRkZDLE9BRUUsU0FGRkEsT0FFRTtBQUFBLDRCQURGQyxPQUNFO0FBQUEsTUFERkEsT0FDRSw4QkFEUSxJQUNSO0FBQ0YsU0FBT0EsT0FBTyxnQkFDVixxQkFBQyxZQUFEO0FBQ0ksSUFBQSxTQUFTLEVBQUVOLFNBRGY7QUFFSSxJQUFBLFFBQVEsRUFBRUMsUUFGZDtBQUdJLElBQUEsT0FBTyxFQUFFQyxPQUhiO0FBSUksSUFBQSxJQUFJLEVBQUVMLElBSlY7QUFLSSxJQUFBLE9BQU8sRUFBRVEsT0FMYjtBQU1JLElBQUEsS0FBSyxFQUFFRixLQU5YO0FBT0ksSUFBQSxJQUFJLEVBQUVDLElBUFY7QUFBQSxjQVNLTDtBQVRMLElBRFUsR0FZVixJQVpKO0FBYUgsQ0F4QkQ7O0FBMEJBRCxTQUFTLENBQUNTLFNBQVYsR0FBc0I7QUFDbEJSLEVBQUFBLFFBQVEsRUFBRVMsbUJBQVVDLFNBQVYsQ0FBb0IsQ0FDMUJELG1CQUFVRSxLQURnQixFQUUxQkYsbUJBQVVHLE9BRmdCLEVBRzFCSCxtQkFBVUksTUFIZ0IsQ0FBcEIsQ0FEUTtBQU1sQlosRUFBQUEsU0FBUyxFQUFFUSxtQkFBVUksTUFOSDtBQU9sQlgsRUFBQUEsUUFBUSxFQUFFTyxtQkFBVUssSUFQRjtBQVFsQlgsRUFBQUEsT0FBTyxFQUFFTSxtQkFBVU0sSUFSRDtBQVNsQmpCLEVBQUFBLElBQUksRUFBRVcsbUJBQVVJLE1BVEU7QUFVbEJULEVBQUFBLEtBQUssRUFBRUssbUJBQVVJLE1BVkM7QUFXbEJSLEVBQUFBLElBQUksRUFBRUksbUJBQVVJLE1BWEU7QUFZbEJQLEVBQUFBLE9BQU8sRUFBRUcsbUJBQVVJLE1BWkQ7QUFhbEJOLEVBQUFBLE9BQU8sRUFBRUUsbUJBQVVLO0FBYkQsQ0FBdEI7ZUFnQmVmLFMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdzdHlsZWQtY29tcG9uZW50cyc7XG5cbmNvbnN0IFN0eWxlZEJ1dHRvbiA9IHN0eWxlZC5idXR0b25gXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDIwOC45N2RlZywgI2ZmY2IwMSAwJSwgI2U3YTgwNCAxMDAlKTtcbiAgICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICAgIGJvcmRlcjogMCBub25lO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxZW07XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcCAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgJltkaXNhYmxlZF0ge1xuICAgICAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMjA4Ljk3ZGVnLCAjODQ4YmFiIDAlLCAjODQ4YmFiIDEwMCUpO1xuICAgIH1cblxuICAgICR7KHsgc2l6ZSB9KSA9PlxuICAgICAgICBzaXplID09PSAnc21hbGwnICYmXG4gICAgICAgIGBjb2xvcjogI0VGRjFGQjtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4zMXB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMThweDtcbiAgICAgICAgcGFkZGluZzogN3B4IDI1cHg7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgIGB9XG5gO1xuXG5jb25zdCBBZGRCdXR0b24gPSAoe1xuICAgIGNoaWxkcmVuLFxuICAgIGNsYXNzTmFtZSxcbiAgICBkaXNhYmxlZCxcbiAgICBvbkNsaWNrLFxuICAgIHNpemUgPSAnbWQnLFxuICAgIHN0eWxlLFxuICAgIHR5cGUgPSAnYnV0dG9uJyxcbiAgICB2YXJpYW50LFxuICAgIHZpc2libGUgPSB0cnVlLFxufSkgPT4ge1xuICAgIHJldHVybiB2aXNpYmxlID8gKFxuICAgICAgICA8U3R5bGVkQnV0dG9uXG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzTmFtZX1cbiAgICAgICAgICAgIGRpc2FibGVkPXtkaXNhYmxlZH1cbiAgICAgICAgICAgIG9uQ2xpY2s9e29uQ2xpY2t9XG4gICAgICAgICAgICBzaXplPXtzaXplfVxuICAgICAgICAgICAgdmFyaWFudD17dmFyaWFudH1cbiAgICAgICAgICAgIHN0eWxlPXtzdHlsZX1cbiAgICAgICAgICAgIHR5cGU9e3R5cGV9XG4gICAgICAgID5cbiAgICAgICAgICAgIHtjaGlsZHJlbn1cbiAgICAgICAgPC9TdHlsZWRCdXR0b24+XG4gICAgKSA6IG51bGw7XG59O1xuXG5BZGRCdXR0b24ucHJvcFR5cGVzID0ge1xuICAgIGNoaWxkcmVuOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtcbiAgICAgICAgUHJvcFR5cGVzLmFycmF5LFxuICAgICAgICBQcm9wVHlwZXMuZWxlbWVudCxcbiAgICAgICAgUHJvcFR5cGVzLnN0cmluZyxcbiAgICBdKSxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxuICAgIG9uQ2xpY2s6IFByb3BUeXBlcy5mdW5jLFxuICAgIHNpemU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgc3R5bGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB2YXJpYW50OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHZpc2libGU6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQWRkQnV0dG9uO1xuIl19