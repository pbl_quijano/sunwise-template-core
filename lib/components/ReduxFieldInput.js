"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBootstrap = require("react-bootstrap");

var _reactScroll = require("react-scroll");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _LabelError = _interopRequireDefault(require("./LabelError"));

var _WrapperInput = _interopRequireDefault(require("./WrapperInput"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledFormControl = (0, _styledComponents.default)(_reactBootstrap.Form.Control)(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    ", ";\n"])), function (_ref) {
  var size = _ref.size;
  return size && "font-size: 12px; line-height:15px;";
});

var ReduxFieldInput = function ReduxFieldInput(_ref2) {
  var append = _ref2.append,
      appendIcon = _ref2.appendIcon,
      as = _ref2.as,
      autoComplete = _ref2.autoComplete,
      children = _ref2.children,
      className = _ref2.className,
      classNameFormGroup = _ref2.classNameFormGroup,
      classNameInputGroup = _ref2.classNameInputGroup,
      controlConfig = _ref2.controlConfig,
      cols = _ref2.cols,
      dataIntercomTarget = _ref2.dataIntercomTarget,
      _ref2$defaultBehevior = _ref2.defaultBehevior,
      defaultBehevior = _ref2$defaultBehevior === void 0 ? false : _ref2$defaultBehevior,
      disabled = _ref2.disabled,
      id = _ref2.id,
      _ref2$input = _ref2.input,
      value = _ref2$input.value,
      name = _ref2$input.name,
      onChange = _ref2$input.onChange,
      onBlur = _ref2$input.onBlur,
      onFocus = _ref2$input.onFocus,
      _ref2$meta = _ref2.meta,
      touched = _ref2$meta.touched,
      error = _ref2$meta.error,
      placeholder = _ref2.placeholder,
      label = _ref2.label,
      readOnly = _ref2.readOnly,
      type = _ref2.type,
      max = _ref2.max,
      maxLength = _ref2.maxLength,
      min = _ref2.min,
      step = _ref2.step,
      onKeyDown = _ref2.onKeyDown,
      _ref2$onlyNumbers = _ref2.onlyNumbers,
      onlyNumbers = _ref2$onlyNumbers === void 0 ? false : _ref2$onlyNumbers,
      _ref2$onlyIntegerNumb = _ref2.onlyIntegerNumbers,
      onlyIntegerNumbers = _ref2$onlyIntegerNumb === void 0 ? false : _ref2$onlyIntegerNumb,
      numberTypeOnBlurActionDisabled = _ref2.numberTypeOnBlurActionDisabled,
      prepend = _ref2.prepend,
      prependIcon = _ref2.prependIcon,
      prependSvgIcon = _ref2.prependSvgIcon,
      required = _ref2.required,
      rows = _ref2.rows,
      size = _ref2.size,
      style = _ref2.style,
      tabIndex = _ref2.tabIndex;

  var onChangeAction = function onChangeAction(event, defaultBehevior, onlyNumbers) {
    var text = event.target.value;

    if (onlyNumbers) {
      event.target.value = text.replace(/[A-Za-z]/gi, '');
      onChange(event);
      return;
    }

    if (onlyIntegerNumbers) {
      event.target.value = text.replace(/[A-Za-z$,.]/gi, '');
      onChange(event);
      return;
    }

    if (!(text.length > 1 && text.charAt(0) === '0' && text.charAt(1) === '0' && !defaultBehevior)) {
      if (type === 'number' && parseFloat(text) >= 1) {
        event.target.value = text.replace(/^0+/, '');
      }

      onChange(event);
    } else {
      onChange(event);
    }
  };

  var onBlurAction = function onBlurAction(event, blurValue) {
    if (!numberTypeOnBlurActionDisabled && type === 'number' && event.target.value === '') {
      event.preventDefault();
      blurValue = min || 0;
      event.target.value = min || 0;
    }

    onBlur(event, blurValue);
  };

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_WrapperInput.default, {
    style: style,
    children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.Form.Group, {
      className: classNameFormGroup,
      children: [label && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.Form.Label, {
        title: label,
        children: label
      }), /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.InputGroup, {
        className: classNameInputGroup,
        children: [(prepend || prependIcon || prependSvgIcon) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.InputGroup.Prepend, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.InputGroup.Text, {
            children: [prependIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)("i", {
              className: prependIcon
            }), prependSvgIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
              alt: "",
              src: prependSvgIcon
            }), prepend]
          })
        }), name && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactScroll.Element, {
          name: "position-".concat(name)
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledFormControl, _objectSpread(_objectSpread({
          as: as,
          autoComplete: autoComplete,
          className: "".concat(className, " ").concat(touched && error && 'with-error', " ").concat(required && (value === null || value === '') ? 'border-danger' : ''),
          cols: cols,
          "data-intercom-target": dataIntercomTarget,
          disabled: disabled,
          id: id,
          max: max,
          maxLength: maxLength,
          min: min,
          onBlur: onBlurAction,
          onFocus: onFocus,
          onChange: function onChange(e) {
            return onChangeAction(e, defaultBehevior, onlyNumbers);
          },
          onKeyDown: onKeyDown,
          placeholder: placeholder,
          readOnly: readOnly,
          required: required,
          rows: rows,
          size: size,
          step: step,
          style: {
            resize: 'none'
          },
          tabIndex: tabIndex,
          type: type,
          value: value
        }, controlConfig), {}, {
          children: children
        })), (appendIcon || append) && /*#__PURE__*/(0, _jsxRuntime.jsx)(_reactBootstrap.InputGroup.Append, {
          children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_reactBootstrap.InputGroup.Text, {
            children: [appendIcon && /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
              alt: "",
              src: appendIcon
            }), append]
          })
        })]
      }), touched && error && /*#__PURE__*/(0, _jsxRuntime.jsx)(_LabelError.default, {
        type: "error",
        children: error
      })]
    })
  });
};

ReduxFieldInput.propTypes = {
  append: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.bool]),
  appendIcon: _propTypes.default.string,
  as: _propTypes.default.string,
  autoComplete: _propTypes.default.oneOfType([_propTypes.default.bool, _propTypes.default.string]),
  children: _propTypes.default.oneOfType([_propTypes.default.elementType, _propTypes.default.array]),
  className: _propTypes.default.string,
  classNameFormGroup: _propTypes.default.string,
  classNameInputGroup: _propTypes.default.string,
  controlConfig: _propTypes.default.object,
  cols: _propTypes.default.string,
  dataIntercomTarget: _propTypes.default.string,
  defaultBehevior: _propTypes.default.bool,
  disabled: _propTypes.default.bool,
  id: _propTypes.default.string,
  input: _propTypes.default.object,
  meta: _propTypes.default.object,
  placeholder: _propTypes.default.string,
  label: _propTypes.default.string,
  readOnly: _propTypes.default.bool,
  type: _propTypes.default.string,
  max: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  maxLength: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  min: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  numberTypeOnBlurActionDisabled: _propTypes.default.bool,
  step: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
  onKeyDown: _propTypes.default.func,
  onlyNumbers: _propTypes.default.bool,
  onlyIntegerNumbers: _propTypes.default.bool,
  prepend: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.bool]),
  prependIcon: _propTypes.default.string,
  prependSvgIcon: _propTypes.default.string,
  required: _propTypes.default.bool,
  rows: _propTypes.default.string,
  size: _propTypes.default.string,
  style: _propTypes.default.object,
  tabIndex: _propTypes.default.number
};
var _default = ReduxFieldInput;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1JlZHV4RmllbGRJbnB1dC5qcyJdLCJuYW1lcyI6WyJTdHlsZWRGb3JtQ29udHJvbCIsIkZvcm0iLCJDb250cm9sIiwic2l6ZSIsIlJlZHV4RmllbGRJbnB1dCIsImFwcGVuZCIsImFwcGVuZEljb24iLCJhcyIsImF1dG9Db21wbGV0ZSIsImNoaWxkcmVuIiwiY2xhc3NOYW1lIiwiY2xhc3NOYW1lRm9ybUdyb3VwIiwiY2xhc3NOYW1lSW5wdXRHcm91cCIsImNvbnRyb2xDb25maWciLCJjb2xzIiwiZGF0YUludGVyY29tVGFyZ2V0IiwiZGVmYXVsdEJlaGV2aW9yIiwiZGlzYWJsZWQiLCJpZCIsImlucHV0IiwidmFsdWUiLCJuYW1lIiwib25DaGFuZ2UiLCJvbkJsdXIiLCJvbkZvY3VzIiwibWV0YSIsInRvdWNoZWQiLCJlcnJvciIsInBsYWNlaG9sZGVyIiwibGFiZWwiLCJyZWFkT25seSIsInR5cGUiLCJtYXgiLCJtYXhMZW5ndGgiLCJtaW4iLCJzdGVwIiwib25LZXlEb3duIiwib25seU51bWJlcnMiLCJvbmx5SW50ZWdlck51bWJlcnMiLCJudW1iZXJUeXBlT25CbHVyQWN0aW9uRGlzYWJsZWQiLCJwcmVwZW5kIiwicHJlcGVuZEljb24iLCJwcmVwZW5kU3ZnSWNvbiIsInJlcXVpcmVkIiwicm93cyIsInN0eWxlIiwidGFiSW5kZXgiLCJvbkNoYW5nZUFjdGlvbiIsImV2ZW50IiwidGV4dCIsInRhcmdldCIsInJlcGxhY2UiLCJsZW5ndGgiLCJjaGFyQXQiLCJwYXJzZUZsb2F0Iiwib25CbHVyQWN0aW9uIiwiYmx1clZhbHVlIiwicHJldmVudERlZmF1bHQiLCJlIiwicmVzaXplIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwib25lT2ZUeXBlIiwic3RyaW5nIiwiYm9vbCIsImVsZW1lbnRUeXBlIiwiYXJyYXkiLCJvYmplY3QiLCJudW1iZXIiLCJmdW5jIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBRUE7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQSxJQUFNQSxpQkFBaUIsR0FBRywrQkFBT0MscUJBQUtDLE9BQVosQ0FBSCxtRkFDakI7QUFBQSxNQUFHQyxJQUFILFFBQUdBLElBQUg7QUFBQSxTQUFjQSxJQUFJLHdDQUFsQjtBQUFBLENBRGlCLENBQXZCOztBQUlBLElBQU1DLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsUUFxQ2xCO0FBQUEsTUFwQ0ZDLE1Bb0NFLFNBcENGQSxNQW9DRTtBQUFBLE1BbkNGQyxVQW1DRSxTQW5DRkEsVUFtQ0U7QUFBQSxNQWxDRkMsRUFrQ0UsU0FsQ0ZBLEVBa0NFO0FBQUEsTUFqQ0ZDLFlBaUNFLFNBakNGQSxZQWlDRTtBQUFBLE1BaENGQyxRQWdDRSxTQWhDRkEsUUFnQ0U7QUFBQSxNQS9CRkMsU0ErQkUsU0EvQkZBLFNBK0JFO0FBQUEsTUE5QkZDLGtCQThCRSxTQTlCRkEsa0JBOEJFO0FBQUEsTUE3QkZDLG1CQTZCRSxTQTdCRkEsbUJBNkJFO0FBQUEsTUE1QkZDLGFBNEJFLFNBNUJGQSxhQTRCRTtBQUFBLE1BM0JGQyxJQTJCRSxTQTNCRkEsSUEyQkU7QUFBQSxNQTFCRkMsa0JBMEJFLFNBMUJGQSxrQkEwQkU7QUFBQSxvQ0F6QkZDLGVBeUJFO0FBQUEsTUF6QkZBLGVBeUJFLHNDQXpCZ0IsS0F5QmhCO0FBQUEsTUF4QkZDLFFBd0JFLFNBeEJGQSxRQXdCRTtBQUFBLE1BdkJGQyxFQXVCRSxTQXZCRkEsRUF1QkU7QUFBQSwwQkF0QkZDLEtBc0JFO0FBQUEsTUF0Qk9DLEtBc0JQLGVBdEJPQSxLQXNCUDtBQUFBLE1BdEJjQyxJQXNCZCxlQXRCY0EsSUFzQmQ7QUFBQSxNQXRCb0JDLFFBc0JwQixlQXRCb0JBLFFBc0JwQjtBQUFBLE1BdEI4QkMsTUFzQjlCLGVBdEI4QkEsTUFzQjlCO0FBQUEsTUF0QnNDQyxPQXNCdEMsZUF0QnNDQSxPQXNCdEM7QUFBQSx5QkFyQkZDLElBcUJFO0FBQUEsTUFyQk1DLE9BcUJOLGNBckJNQSxPQXFCTjtBQUFBLE1BckJlQyxLQXFCZixjQXJCZUEsS0FxQmY7QUFBQSxNQXBCRkMsV0FvQkUsU0FwQkZBLFdBb0JFO0FBQUEsTUFuQkZDLEtBbUJFLFNBbkJGQSxLQW1CRTtBQUFBLE1BbEJGQyxRQWtCRSxTQWxCRkEsUUFrQkU7QUFBQSxNQWpCRkMsSUFpQkUsU0FqQkZBLElBaUJFO0FBQUEsTUFoQkZDLEdBZ0JFLFNBaEJGQSxHQWdCRTtBQUFBLE1BZkZDLFNBZUUsU0FmRkEsU0FlRTtBQUFBLE1BZEZDLEdBY0UsU0FkRkEsR0FjRTtBQUFBLE1BYkZDLElBYUUsU0FiRkEsSUFhRTtBQUFBLE1BWkZDLFNBWUUsU0FaRkEsU0FZRTtBQUFBLGdDQVhGQyxXQVdFO0FBQUEsTUFYRkEsV0FXRSxrQ0FYWSxLQVdaO0FBQUEsb0NBVkZDLGtCQVVFO0FBQUEsTUFWRkEsa0JBVUUsc0NBVm1CLEtBVW5CO0FBQUEsTUFURkMsOEJBU0UsU0FURkEsOEJBU0U7QUFBQSxNQVJGQyxPQVFFLFNBUkZBLE9BUUU7QUFBQSxNQVBGQyxXQU9FLFNBUEZBLFdBT0U7QUFBQSxNQU5GQyxjQU1FLFNBTkZBLGNBTUU7QUFBQSxNQUxGQyxRQUtFLFNBTEZBLFFBS0U7QUFBQSxNQUpGQyxJQUlFLFNBSkZBLElBSUU7QUFBQSxNQUhGekMsSUFHRSxTQUhGQSxJQUdFO0FBQUEsTUFGRjBDLEtBRUUsU0FGRkEsS0FFRTtBQUFBLE1BREZDLFFBQ0UsU0FERkEsUUFDRTs7QUFDRixNQUFNQyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUNDLEtBQUQsRUFBUWhDLGVBQVIsRUFBeUJxQixXQUF6QixFQUF5QztBQUM1RCxRQUFNWSxJQUFJLEdBQUdELEtBQUssQ0FBQ0UsTUFBTixDQUFhOUIsS0FBMUI7O0FBRUEsUUFBSWlCLFdBQUosRUFBaUI7QUFDYlcsTUFBQUEsS0FBSyxDQUFDRSxNQUFOLENBQWE5QixLQUFiLEdBQXFCNkIsSUFBSSxDQUFDRSxPQUFMLENBQWEsWUFBYixFQUEyQixFQUEzQixDQUFyQjtBQUNBN0IsTUFBQUEsUUFBUSxDQUFDMEIsS0FBRCxDQUFSO0FBRUE7QUFDSDs7QUFFRCxRQUFJVixrQkFBSixFQUF3QjtBQUNwQlUsTUFBQUEsS0FBSyxDQUFDRSxNQUFOLENBQWE5QixLQUFiLEdBQXFCNkIsSUFBSSxDQUFDRSxPQUFMLENBQWEsZUFBYixFQUE4QixFQUE5QixDQUFyQjtBQUNBN0IsTUFBQUEsUUFBUSxDQUFDMEIsS0FBRCxDQUFSO0FBRUE7QUFDSDs7QUFFRCxRQUNJLEVBQ0lDLElBQUksQ0FBQ0csTUFBTCxHQUFjLENBQWQsSUFDQUgsSUFBSSxDQUFDSSxNQUFMLENBQVksQ0FBWixNQUFtQixHQURuQixJQUVBSixJQUFJLENBQUNJLE1BQUwsQ0FBWSxDQUFaLE1BQW1CLEdBRm5CLElBR0EsQ0FBQ3JDLGVBSkwsQ0FESixFQU9FO0FBQ0UsVUFBSWUsSUFBSSxLQUFLLFFBQVQsSUFBcUJ1QixVQUFVLENBQUNMLElBQUQsQ0FBVixJQUFvQixDQUE3QyxFQUFnRDtBQUM1Q0QsUUFBQUEsS0FBSyxDQUFDRSxNQUFOLENBQWE5QixLQUFiLEdBQXFCNkIsSUFBSSxDQUFDRSxPQUFMLENBQWEsS0FBYixFQUFvQixFQUFwQixDQUFyQjtBQUNIOztBQUNEN0IsTUFBQUEsUUFBUSxDQUFDMEIsS0FBRCxDQUFSO0FBQ0gsS0FaRCxNQVlPO0FBQ0gxQixNQUFBQSxRQUFRLENBQUMwQixLQUFELENBQVI7QUFDSDtBQUNKLEdBaENEOztBQWtDQSxNQUFNTyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDUCxLQUFELEVBQVFRLFNBQVIsRUFBc0I7QUFDdkMsUUFDSSxDQUFDakIsOEJBQUQsSUFDQVIsSUFBSSxLQUFLLFFBRFQsSUFFQWlCLEtBQUssQ0FBQ0UsTUFBTixDQUFhOUIsS0FBYixLQUF1QixFQUgzQixFQUlFO0FBQ0U0QixNQUFBQSxLQUFLLENBQUNTLGNBQU47QUFDQUQsTUFBQUEsU0FBUyxHQUFHdEIsR0FBRyxJQUFJLENBQW5CO0FBQ0FjLE1BQUFBLEtBQUssQ0FBQ0UsTUFBTixDQUFhOUIsS0FBYixHQUFxQmMsR0FBRyxJQUFJLENBQTVCO0FBQ0g7O0FBRURYLElBQUFBLE1BQU0sQ0FBQ3lCLEtBQUQsRUFBUVEsU0FBUixDQUFOO0FBQ0gsR0FaRDs7QUFjQSxzQkFDSSxxQkFBQyxxQkFBRDtBQUFjLElBQUEsS0FBSyxFQUFFWCxLQUFyQjtBQUFBLDJCQUNJLHNCQUFDLG9CQUFELENBQU0sS0FBTjtBQUFZLE1BQUEsU0FBUyxFQUFFbEMsa0JBQXZCO0FBQUEsaUJBQ0trQixLQUFLLGlCQUFJLHFCQUFDLG9CQUFELENBQU0sS0FBTjtBQUFZLFFBQUEsS0FBSyxFQUFFQSxLQUFuQjtBQUFBLGtCQUEyQkE7QUFBM0IsUUFEZCxlQUdJLHNCQUFDLDBCQUFEO0FBQVksUUFBQSxTQUFTLEVBQUVqQixtQkFBdkI7QUFBQSxtQkFDSyxDQUFDNEIsT0FBTyxJQUFJQyxXQUFYLElBQTBCQyxjQUEzQixrQkFDRyxxQkFBQywwQkFBRCxDQUFZLE9BQVo7QUFBQSxpQ0FDSSxzQkFBQywwQkFBRCxDQUFZLElBQVo7QUFBQSx1QkFDS0QsV0FBVyxpQkFBSTtBQUFHLGNBQUEsU0FBUyxFQUFFQTtBQUFkLGNBRHBCLEVBRUtDLGNBQWMsaUJBQ1g7QUFBSyxjQUFBLEdBQUcsRUFBQyxFQUFUO0FBQVksY0FBQSxHQUFHLEVBQUVBO0FBQWpCLGNBSFIsRUFLS0YsT0FMTDtBQUFBO0FBREosVUFGUixFQVlLbkIsSUFBSSxpQkFBSSxxQkFBQyxvQkFBRDtBQUFTLFVBQUEsSUFBSSxxQkFBY0EsSUFBZDtBQUFiLFVBWmIsZUFhSSxxQkFBQyxpQkFBRDtBQUNJLFVBQUEsRUFBRSxFQUFFZCxFQURSO0FBRUksVUFBQSxZQUFZLEVBQUVDLFlBRmxCO0FBR0ksVUFBQSxTQUFTLFlBQUtFLFNBQUwsY0FDTGdCLE9BQU8sSUFBSUMsS0FBWCxJQUFvQixZQURmLGNBR0xnQixRQUFRLEtBQUt2QixLQUFLLEtBQUssSUFBVixJQUFrQkEsS0FBSyxLQUFLLEVBQWpDLENBQVIsR0FDTSxlQUROLEdBRU0sRUFMRCxDQUhiO0FBVUksVUFBQSxJQUFJLEVBQUVOLElBVlY7QUFXSSxrQ0FBc0JDLGtCQVgxQjtBQVlJLFVBQUEsUUFBUSxFQUFFRSxRQVpkO0FBYUksVUFBQSxFQUFFLEVBQUVDLEVBYlI7QUFjSSxVQUFBLEdBQUcsRUFBRWMsR0FkVDtBQWVJLFVBQUEsU0FBUyxFQUFFQyxTQWZmO0FBZ0JJLFVBQUEsR0FBRyxFQUFFQyxHQWhCVDtBQWlCSSxVQUFBLE1BQU0sRUFBRXFCLFlBakJaO0FBa0JJLFVBQUEsT0FBTyxFQUFFL0IsT0FsQmI7QUFtQkksVUFBQSxRQUFRLEVBQUUsa0JBQUNrQyxDQUFEO0FBQUEsbUJBQ05YLGNBQWMsQ0FBQ1csQ0FBRCxFQUFJMUMsZUFBSixFQUFxQnFCLFdBQXJCLENBRFI7QUFBQSxXQW5CZDtBQXNCSSxVQUFBLFNBQVMsRUFBRUQsU0F0QmY7QUF1QkksVUFBQSxXQUFXLEVBQUVSLFdBdkJqQjtBQXdCSSxVQUFBLFFBQVEsRUFBRUUsUUF4QmQ7QUF5QkksVUFBQSxRQUFRLEVBQUVhLFFBekJkO0FBMEJJLFVBQUEsSUFBSSxFQUFFQyxJQTFCVjtBQTJCSSxVQUFBLElBQUksRUFBRXpDLElBM0JWO0FBNEJJLFVBQUEsSUFBSSxFQUFFZ0MsSUE1QlY7QUE2QkksVUFBQSxLQUFLLEVBQUU7QUFBRXdCLFlBQUFBLE1BQU0sRUFBRTtBQUFWLFdBN0JYO0FBOEJJLFVBQUEsUUFBUSxFQUFFYixRQTlCZDtBQStCSSxVQUFBLElBQUksRUFBRWYsSUEvQlY7QUFnQ0ksVUFBQSxLQUFLLEVBQUVYO0FBaENYLFdBaUNRUCxhQWpDUjtBQUFBLG9CQW1DS0o7QUFuQ0wsV0FiSixFQW1ESyxDQUFDSCxVQUFVLElBQUlELE1BQWYsa0JBQ0cscUJBQUMsMEJBQUQsQ0FBWSxNQUFaO0FBQUEsaUNBQ0ksc0JBQUMsMEJBQUQsQ0FBWSxJQUFaO0FBQUEsdUJBQ0tDLFVBQVUsaUJBQUk7QUFBSyxjQUFBLEdBQUcsRUFBQyxFQUFUO0FBQVksY0FBQSxHQUFHLEVBQUVBO0FBQWpCLGNBRG5CLEVBRUtELE1BRkw7QUFBQTtBQURKLFVBcERSO0FBQUEsUUFISixFQWdFS3FCLE9BQU8sSUFBSUMsS0FBWCxpQkFDRyxxQkFBQyxtQkFBRDtBQUFZLFFBQUEsSUFBSSxFQUFDLE9BQWpCO0FBQUEsa0JBQTBCQTtBQUExQixRQWpFUjtBQUFBO0FBREosSUFESjtBQXdFSCxDQTlKRDs7QUFnS0F2QixlQUFlLENBQUN3RCxTQUFoQixHQUE0QjtBQUN4QnZELEVBQUFBLE1BQU0sRUFBRXdELG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVRSxNQUFYLEVBQW1CRixtQkFBVUcsSUFBN0IsQ0FBcEIsQ0FEZ0I7QUFFeEIxRCxFQUFBQSxVQUFVLEVBQUV1RCxtQkFBVUUsTUFGRTtBQUd4QnhELEVBQUFBLEVBQUUsRUFBRXNELG1CQUFVRSxNQUhVO0FBSXhCdkQsRUFBQUEsWUFBWSxFQUFFcUQsbUJBQVVDLFNBQVYsQ0FBb0IsQ0FBQ0QsbUJBQVVHLElBQVgsRUFBaUJILG1CQUFVRSxNQUEzQixDQUFwQixDQUpVO0FBS3hCdEQsRUFBQUEsUUFBUSxFQUFFb0QsbUJBQVVDLFNBQVYsQ0FBb0IsQ0FBQ0QsbUJBQVVJLFdBQVgsRUFBd0JKLG1CQUFVSyxLQUFsQyxDQUFwQixDQUxjO0FBTXhCeEQsRUFBQUEsU0FBUyxFQUFFbUQsbUJBQVVFLE1BTkc7QUFPeEJwRCxFQUFBQSxrQkFBa0IsRUFBRWtELG1CQUFVRSxNQVBOO0FBUXhCbkQsRUFBQUEsbUJBQW1CLEVBQUVpRCxtQkFBVUUsTUFSUDtBQVN4QmxELEVBQUFBLGFBQWEsRUFBRWdELG1CQUFVTSxNQVREO0FBVXhCckQsRUFBQUEsSUFBSSxFQUFFK0MsbUJBQVVFLE1BVlE7QUFXeEJoRCxFQUFBQSxrQkFBa0IsRUFBRThDLG1CQUFVRSxNQVhOO0FBWXhCL0MsRUFBQUEsZUFBZSxFQUFFNkMsbUJBQVVHLElBWkg7QUFheEIvQyxFQUFBQSxRQUFRLEVBQUU0QyxtQkFBVUcsSUFiSTtBQWN4QjlDLEVBQUFBLEVBQUUsRUFBRTJDLG1CQUFVRSxNQWRVO0FBZXhCNUMsRUFBQUEsS0FBSyxFQUFFMEMsbUJBQVVNLE1BZk87QUFnQnhCMUMsRUFBQUEsSUFBSSxFQUFFb0MsbUJBQVVNLE1BaEJRO0FBaUJ4QnZDLEVBQUFBLFdBQVcsRUFBRWlDLG1CQUFVRSxNQWpCQztBQWtCeEJsQyxFQUFBQSxLQUFLLEVBQUVnQyxtQkFBVUUsTUFsQk87QUFtQnhCakMsRUFBQUEsUUFBUSxFQUFFK0IsbUJBQVVHLElBbkJJO0FBb0J4QmpDLEVBQUFBLElBQUksRUFBRThCLG1CQUFVRSxNQXBCUTtBQXFCeEIvQixFQUFBQSxHQUFHLEVBQUU2QixtQkFBVUMsU0FBVixDQUFvQixDQUFDRCxtQkFBVU8sTUFBWCxFQUFtQlAsbUJBQVVFLE1BQTdCLENBQXBCLENBckJtQjtBQXNCeEI5QixFQUFBQSxTQUFTLEVBQUU0QixtQkFBVUMsU0FBVixDQUFvQixDQUFDRCxtQkFBVU8sTUFBWCxFQUFtQlAsbUJBQVVFLE1BQTdCLENBQXBCLENBdEJhO0FBdUJ4QjdCLEVBQUFBLEdBQUcsRUFBRTJCLG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVTyxNQUFYLEVBQW1CUCxtQkFBVUUsTUFBN0IsQ0FBcEIsQ0F2Qm1CO0FBd0J4QnhCLEVBQUFBLDhCQUE4QixFQUFFc0IsbUJBQVVHLElBeEJsQjtBQXlCeEI3QixFQUFBQSxJQUFJLEVBQUUwQixtQkFBVUMsU0FBVixDQUFvQixDQUFDRCxtQkFBVU8sTUFBWCxFQUFtQlAsbUJBQVVFLE1BQTdCLENBQXBCLENBekJrQjtBQTBCeEIzQixFQUFBQSxTQUFTLEVBQUV5QixtQkFBVVEsSUExQkc7QUEyQnhCaEMsRUFBQUEsV0FBVyxFQUFFd0IsbUJBQVVHLElBM0JDO0FBNEJ4QjFCLEVBQUFBLGtCQUFrQixFQUFFdUIsbUJBQVVHLElBNUJOO0FBNkJ4QnhCLEVBQUFBLE9BQU8sRUFBRXFCLG1CQUFVQyxTQUFWLENBQW9CLENBQUNELG1CQUFVRSxNQUFYLEVBQW1CRixtQkFBVUcsSUFBN0IsQ0FBcEIsQ0E3QmU7QUE4QnhCdkIsRUFBQUEsV0FBVyxFQUFFb0IsbUJBQVVFLE1BOUJDO0FBK0J4QnJCLEVBQUFBLGNBQWMsRUFBRW1CLG1CQUFVRSxNQS9CRjtBQWdDeEJwQixFQUFBQSxRQUFRLEVBQUVrQixtQkFBVUcsSUFoQ0k7QUFpQ3hCcEIsRUFBQUEsSUFBSSxFQUFFaUIsbUJBQVVFLE1BakNRO0FBa0N4QjVELEVBQUFBLElBQUksRUFBRTBELG1CQUFVRSxNQWxDUTtBQW1DeEJsQixFQUFBQSxLQUFLLEVBQUVnQixtQkFBVU0sTUFuQ087QUFvQ3hCckIsRUFBQUEsUUFBUSxFQUFFZSxtQkFBVU87QUFwQ0ksQ0FBNUI7ZUF1Q2VoRSxlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IEZvcm0sIElucHV0R3JvdXAgfSBmcm9tICdyZWFjdC1ib290c3RyYXAnO1xuaW1wb3J0IHsgRWxlbWVudCB9IGZyb20gJ3JlYWN0LXNjcm9sbCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IExhYmVsRXJyb3IgZnJvbSAnLi9MYWJlbEVycm9yJztcbmltcG9ydCBXcmFwcGVySW5wdXQgZnJvbSAnLi9XcmFwcGVySW5wdXQnO1xuXG5jb25zdCBTdHlsZWRGb3JtQ29udHJvbCA9IHN0eWxlZChGb3JtLkNvbnRyb2wpYFxuICAgICR7KHsgc2l6ZSB9KSA9PiBzaXplICYmIGBmb250LXNpemU6IDEycHg7IGxpbmUtaGVpZ2h0OjE1cHg7YH07XG5gO1xuXG5jb25zdCBSZWR1eEZpZWxkSW5wdXQgPSAoe1xuICAgIGFwcGVuZCxcbiAgICBhcHBlbmRJY29uLFxuICAgIGFzLFxuICAgIGF1dG9Db21wbGV0ZSxcbiAgICBjaGlsZHJlbixcbiAgICBjbGFzc05hbWUsXG4gICAgY2xhc3NOYW1lRm9ybUdyb3VwLFxuICAgIGNsYXNzTmFtZUlucHV0R3JvdXAsXG4gICAgY29udHJvbENvbmZpZyxcbiAgICBjb2xzLFxuICAgIGRhdGFJbnRlcmNvbVRhcmdldCxcbiAgICBkZWZhdWx0QmVoZXZpb3IgPSBmYWxzZSxcbiAgICBkaXNhYmxlZCxcbiAgICBpZCxcbiAgICBpbnB1dDogeyB2YWx1ZSwgbmFtZSwgb25DaGFuZ2UsIG9uQmx1ciwgb25Gb2N1cyB9LFxuICAgIG1ldGE6IHsgdG91Y2hlZCwgZXJyb3IgfSxcbiAgICBwbGFjZWhvbGRlcixcbiAgICBsYWJlbCxcbiAgICByZWFkT25seSxcbiAgICB0eXBlLFxuICAgIG1heCxcbiAgICBtYXhMZW5ndGgsXG4gICAgbWluLFxuICAgIHN0ZXAsXG4gICAgb25LZXlEb3duLFxuICAgIG9ubHlOdW1iZXJzID0gZmFsc2UsXG4gICAgb25seUludGVnZXJOdW1iZXJzID0gZmFsc2UsXG4gICAgbnVtYmVyVHlwZU9uQmx1ckFjdGlvbkRpc2FibGVkLFxuICAgIHByZXBlbmQsXG4gICAgcHJlcGVuZEljb24sXG4gICAgcHJlcGVuZFN2Z0ljb24sXG4gICAgcmVxdWlyZWQsXG4gICAgcm93cyxcbiAgICBzaXplLFxuICAgIHN0eWxlLFxuICAgIHRhYkluZGV4LFxufSkgPT4ge1xuICAgIGNvbnN0IG9uQ2hhbmdlQWN0aW9uID0gKGV2ZW50LCBkZWZhdWx0QmVoZXZpb3IsIG9ubHlOdW1iZXJzKSA9PiB7XG4gICAgICAgIGNvbnN0IHRleHQgPSBldmVudC50YXJnZXQudmFsdWU7XG5cbiAgICAgICAgaWYgKG9ubHlOdW1iZXJzKSB7XG4gICAgICAgICAgICBldmVudC50YXJnZXQudmFsdWUgPSB0ZXh0LnJlcGxhY2UoL1tBLVphLXpdL2dpLCAnJyk7XG4gICAgICAgICAgICBvbkNoYW5nZShldmVudCk7XG5cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChvbmx5SW50ZWdlck51bWJlcnMpIHtcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldC52YWx1ZSA9IHRleHQucmVwbGFjZSgvW0EtWmEteiQsLl0vZ2ksICcnKTtcbiAgICAgICAgICAgIG9uQ2hhbmdlKGV2ZW50KTtcblxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIShcbiAgICAgICAgICAgICAgICB0ZXh0Lmxlbmd0aCA+IDEgJiZcbiAgICAgICAgICAgICAgICB0ZXh0LmNoYXJBdCgwKSA9PT0gJzAnICYmXG4gICAgICAgICAgICAgICAgdGV4dC5jaGFyQXQoMSkgPT09ICcwJyAmJlxuICAgICAgICAgICAgICAgICFkZWZhdWx0QmVoZXZpb3JcbiAgICAgICAgICAgIClcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ251bWJlcicgJiYgcGFyc2VGbG9hdCh0ZXh0KSA+PSAxKSB7XG4gICAgICAgICAgICAgICAgZXZlbnQudGFyZ2V0LnZhbHVlID0gdGV4dC5yZXBsYWNlKC9eMCsvLCAnJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvbkNoYW5nZShldmVudCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvbkNoYW5nZShldmVudCk7XG4gICAgICAgIH1cbiAgICB9O1xuXG4gICAgY29uc3Qgb25CbHVyQWN0aW9uID0gKGV2ZW50LCBibHVyVmFsdWUpID0+IHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgIW51bWJlclR5cGVPbkJsdXJBY3Rpb25EaXNhYmxlZCAmJlxuICAgICAgICAgICAgdHlwZSA9PT0gJ251bWJlcicgJiZcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldC52YWx1ZSA9PT0gJydcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgYmx1clZhbHVlID0gbWluIHx8IDA7XG4gICAgICAgICAgICBldmVudC50YXJnZXQudmFsdWUgPSBtaW4gfHwgMDtcbiAgICAgICAgfVxuXG4gICAgICAgIG9uQmx1cihldmVudCwgYmx1clZhbHVlKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgICAgPFdyYXBwZXJJbnB1dCBzdHlsZT17c3R5bGV9PlxuICAgICAgICAgICAgPEZvcm0uR3JvdXAgY2xhc3NOYW1lPXtjbGFzc05hbWVGb3JtR3JvdXB9PlxuICAgICAgICAgICAgICAgIHtsYWJlbCAmJiA8Rm9ybS5MYWJlbCB0aXRsZT17bGFiZWx9PntsYWJlbH08L0Zvcm0uTGFiZWw+fVxuXG4gICAgICAgICAgICAgICAgPElucHV0R3JvdXAgY2xhc3NOYW1lPXtjbGFzc05hbWVJbnB1dEdyb3VwfT5cbiAgICAgICAgICAgICAgICAgICAgeyhwcmVwZW5kIHx8IHByZXBlbmRJY29uIHx8IHByZXBlbmRTdmdJY29uKSAmJiAoXG4gICAgICAgICAgICAgICAgICAgICAgICA8SW5wdXRHcm91cC5QcmVwZW5kPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dEdyb3VwLlRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtwcmVwZW5kSWNvbiAmJiA8aSBjbGFzc05hbWU9e3ByZXBlbmRJY29ufSAvPn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3ByZXBlbmRTdmdJY29uICYmIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtwcmVwZW5kU3ZnSWNvbn0gLz5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge3ByZXBlbmR9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9JbnB1dEdyb3VwLlRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L0lucHV0R3JvdXAuUHJlcGVuZD5cbiAgICAgICAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgICAgICAge25hbWUgJiYgPEVsZW1lbnQgbmFtZT17YHBvc2l0aW9uLSR7bmFtZX1gfSAvPn1cbiAgICAgICAgICAgICAgICAgICAgPFN0eWxlZEZvcm1Db250cm9sXG4gICAgICAgICAgICAgICAgICAgICAgICBhcz17YXN9XG4gICAgICAgICAgICAgICAgICAgICAgICBhdXRvQ29tcGxldGU9e2F1dG9Db21wbGV0ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YCR7Y2xhc3NOYW1lfSAke1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdWNoZWQgJiYgZXJyb3IgJiYgJ3dpdGgtZXJyb3InXG4gICAgICAgICAgICAgICAgICAgICAgICB9ICR7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQgJiYgKHZhbHVlID09PSBudWxsIHx8IHZhbHVlID09PSAnJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyAnYm9yZGVyLWRhbmdlcidcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiAnJ1xuICAgICAgICAgICAgICAgICAgICAgICAgfWB9XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzPXtjb2xzfVxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YS1pbnRlcmNvbS10YXJnZXQ9e2RhdGFJbnRlcmNvbVRhcmdldH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXtkaXNhYmxlZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPXtpZH1cbiAgICAgICAgICAgICAgICAgICAgICAgIG1heD17bWF4fVxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoPXttYXhMZW5ndGh9XG4gICAgICAgICAgICAgICAgICAgICAgICBtaW49e21pbn1cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQmx1cj17b25CbHVyQWN0aW9ufVxuICAgICAgICAgICAgICAgICAgICAgICAgb25Gb2N1cz17b25Gb2N1c31cbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZUFjdGlvbihlLCBkZWZhdWx0QmVoZXZpb3IsIG9ubHlOdW1iZXJzKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25LZXlEb3duPXtvbktleURvd259XG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seT17cmVhZE9ubHl9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZD17cmVxdWlyZWR9XG4gICAgICAgICAgICAgICAgICAgICAgICByb3dzPXtyb3dzfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZT17c2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHN0ZXA9e3N0ZXB9XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyByZXNpemU6ICdub25lJyB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiSW5kZXg9e3RhYkluZGV4fVxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT17dHlwZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5jb250cm9sQ29uZmlnfVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICB7Y2hpbGRyZW59XG4gICAgICAgICAgICAgICAgICAgIDwvU3R5bGVkRm9ybUNvbnRyb2w+XG5cbiAgICAgICAgICAgICAgICAgICAgeyhhcHBlbmRJY29uIHx8IGFwcGVuZCkgJiYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgPElucHV0R3JvdXAuQXBwZW5kPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbnB1dEdyb3VwLlRleHQ+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHthcHBlbmRJY29uICYmIDxpbWcgYWx0PVwiXCIgc3JjPXthcHBlbmRJY29ufSAvPn1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2FwcGVuZH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0lucHV0R3JvdXAuVGV4dD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW5wdXRHcm91cC5BcHBlbmQ+XG4gICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9JbnB1dEdyb3VwPlxuXG4gICAgICAgICAgICAgICAge3RvdWNoZWQgJiYgZXJyb3IgJiYgKFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWxFcnJvciB0eXBlPVwiZXJyb3JcIj57ZXJyb3J9PC9MYWJlbEVycm9yPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8L0Zvcm0uR3JvdXA+XG4gICAgICAgIDwvV3JhcHBlcklucHV0PlxuICAgICk7XG59O1xuXG5SZWR1eEZpZWxkSW5wdXQucHJvcFR5cGVzID0ge1xuICAgIGFwcGVuZDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmJvb2xdKSxcbiAgICBhcHBlbmRJY29uOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGFzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGF1dG9Db21wbGV0ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmJvb2wsIFByb3BUeXBlcy5zdHJpbmddKSxcbiAgICBjaGlsZHJlbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLmVsZW1lbnRUeXBlLCBQcm9wVHlwZXMuYXJyYXldKSxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2xhc3NOYW1lRm9ybUdyb3VwOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNsYXNzTmFtZUlucHV0R3JvdXA6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY29udHJvbENvbmZpZzogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBjb2xzOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGRhdGFJbnRlcmNvbVRhcmdldDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBkZWZhdWx0QmVoZXZpb3I6IFByb3BUeXBlcy5ib29sLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpbnB1dDogUHJvcFR5cGVzLm9iamVjdCxcbiAgICBtZXRhOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGxhYmVsOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbCxcbiAgICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG1heDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIG1heExlbmd0aDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIG1pbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIG51bWJlclR5cGVPbkJsdXJBY3Rpb25EaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgc3RlcDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm51bWJlciwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIG9uS2V5RG93bjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgb25seU51bWJlcnM6IFByb3BUeXBlcy5ib29sLFxuICAgIG9ubHlJbnRlZ2VyTnVtYmVyczogUHJvcFR5cGVzLmJvb2wsXG4gICAgcHJlcGVuZDogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmJvb2xdKSxcbiAgICBwcmVwZW5kSWNvbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBwcmVwZW5kU3ZnSWNvbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICByZXF1aXJlZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgcm93czogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzaXplOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIHRhYkluZGV4OiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgUmVkdXhGaWVsZElucHV0O1xuIl19