"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _RoundedIcon = _interopRequireDefault(require("./RoundedIcon"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledButton = _styledComponents.default.button.attrs(function (props) {
  return {
    className: "btn".concat(props.isBlock ? ' btn-block' : '').concat(props.size ? " btn-".concat(props.size) : "").concat(props.variant ? " btn-".concat(props.variant) : "").concat(props.append ? ' pr-1' : '').concat(props.prepend ? ' pl-1' : '')
  };
})(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    align-items: center;\n    cursor: pointer;\n    display: ", ";\n    position: relative;\n    ", ";\n    ", ";\n    ", ";\n    ", ";\n    ", ";\n\n    .focus,\n    :focus {\n        box-shadow: none;\n        outline: none;\n    }\n\n    &.btn-sm {\n        font-size: 13px;\n    }\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "\n\n    ", "    \n\n    ", "\n\n    ", "\n    ", "\n    ", "\n    ", "\n    ", "\n"])), function (_ref) {
  var isBlock = _ref.isBlock;
  return isBlock ? 'block' : 'inline-flex';
}, function (_ref2) {
  var fontSize = _ref2.fontSize;
  return fontSize && "font-size: ".concat(fontSize);
}, function (_ref3) {
  var fontWeight = _ref3.fontWeight;
  return fontWeight && "font-weight: ".concat(fontWeight);
}, function (_ref4) {
  var margin = _ref4.margin;
  return margin && "margin: ".concat(margin);
}, function (_ref5) {
  var width = _ref5.width;
  return width && "width: ".concat(width);
}, function (_ref6) {
  var height = _ref6.height;
  return height && "height: ".concat(height);
}, function (_ref7) {
  var variant = _ref7.variant;
  return variant === 'cancel' && "\n        border: 1px solid #D3D7EB;\n        background-color: #EFF1FB;\n        color: #848BAB;\n        &:hover {\n            color: #848BAB;\n        }\n    ";
}, function (_ref8) {
  var variant = _ref8.variant;
  return variant === 'confirm' && "\n        color: #fff;\n        background-color: #ff9a00;\n    ";
}, function (_ref9) {
  var variant = _ref9.variant;
  return variant === 'approved' && "\n        color: #fff;\n        background-color: #2BF2AB;\n    ";
}, function (_ref10) {
  var variant = _ref10.variant;
  return variant === 'rejected' && "\n        background-color: #FFFFFF;\n        border: 1px solid #FA6968;\n        color: #FA6968;\n    ";
}, function (_ref11) {
  var variant = _ref11.variant;
  return variant === 'reject-reason' && "\n        background-color: #F7FCFF;\n        color: #848BAB;\n        font-size: 10px;\n        \n    ";
}, function (_ref12) {
  var disabled = _ref12.disabled;
  return disabled && "\n        pointer-events: none;\n        opacity: 0.5;\n        cursor: default;\n    ";
}, function (_ref13) {
  var isRounded = _ref13.isRounded,
      borderRadius = _ref13.borderRadius;
  return isRounded && "border-radius: ".concat(borderRadius, ";");
}, function (_ref14) {
  var borderColor = _ref14.borderColor;
  return borderColor && "border-color: ".concat(borderColor);
}, function (_ref15) {
  var borderStyle = _ref15.borderStyle;
  return borderStyle && "border-style: ".concat(borderStyle);
}, function (_ref16) {
  var textColor = _ref16.textColor;
  return textColor && "color: ".concat(textColor);
}, function (_ref17) {
  var background = _ref17.background;
  return background && "background: ".concat(background);
});

var ButtonComponent = function ButtonComponent(_ref18) {
  var as = _ref18.as,
      background = _ref18.background,
      borderColor = _ref18.borderColor,
      _ref18$borderRadius = _ref18.borderRadius,
      borderRadius = _ref18$borderRadius === void 0 ? '30px' : _ref18$borderRadius,
      borderStyle = _ref18.borderStyle,
      children = _ref18.children,
      className = _ref18.className,
      dataIntercomTarget = _ref18.dataIntercomTarget,
      disabled = _ref18.disabled,
      iconConfig = _ref18.iconConfig,
      fontWeight = _ref18.fontWeight,
      fontSize = _ref18.fontSize,
      height = _ref18.height,
      href = _ref18.href,
      onClick = _ref18.onClick,
      isBlock = _ref18.isBlock,
      _ref18$isRounded = _ref18.isRounded,
      isRounded = _ref18$isRounded === void 0 ? true : _ref18$isRounded,
      margin = _ref18.margin,
      variant = _ref18.variant,
      size = _ref18.size,
      target = _ref18.target,
      textColor = _ref18.textColor,
      width = _ref18.width,
      to = _ref18.to,
      _ref18$visible = _ref18.visible,
      visible = _ref18$visible === void 0 ? true : _ref18$visible,
      type = _ref18.type;
  return visible ? /*#__PURE__*/(0, _jsxRuntime.jsxs)(StyledButton, {
    append: iconConfig ? iconConfig.append : null,
    as: as,
    background: background,
    borderColor: borderColor,
    borderRadius: borderRadius,
    borderStyle: borderStyle,
    className: className,
    "data-intercom-target": dataIntercomTarget,
    isBlock: isBlock,
    isRounded: isRounded,
    disabled: disabled,
    fontSize: fontSize,
    fontWeight: fontWeight,
    height: height,
    href: href,
    onClick: onClick,
    variant: variant,
    prepend: iconConfig ? iconConfig.prepend : null,
    size: size,
    target: target,
    textColor: textColor,
    width: width,
    margin: margin,
    to: to,
    type: type,
    children: [iconConfig && iconConfig.prepend && /*#__PURE__*/(0, _jsxRuntime.jsx)(_RoundedIcon.default, _objectSpread({
      className: children ? "mr-2" : ""
    }, iconConfig.prepend)), /*#__PURE__*/(0, _jsxRuntime.jsx)("span", {
      className: "flex-grow-1",
      children: children
    }), iconConfig && iconConfig.append && /*#__PURE__*/(0, _jsxRuntime.jsx)(_RoundedIcon.default, _objectSpread({
      className: "ml-2"
    }, iconConfig.append))]
  }) : null;
};

ButtonComponent.propTypes = {
  as: _propTypes.default.string,
  background: _propTypes.default.string,
  borderColor: _propTypes.default.string,
  borderRadius: _propTypes.default.string,
  borderStyle: _propTypes.default.string,
  children: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
  className: _propTypes.default.string,
  dataIntercomTarget: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  iconConfig: _propTypes.default.object,
  fontWeight: _propTypes.default.string,
  fontSize: _propTypes.default.string,
  height: _propTypes.default.string,
  href: _propTypes.default.string,
  onClick: _propTypes.default.func,
  isBlock: _propTypes.default.bool,
  isRounded: _propTypes.default.bool,
  margin: _propTypes.default.string,
  variant: _propTypes.default.string,
  size: _propTypes.default.string,
  target: _propTypes.default.string,
  textColor: _propTypes.default.string,
  width: _propTypes.default.string,
  to: _propTypes.default.string,
  visible: _propTypes.default.bool,
  type: _propTypes.default.string
};
ButtonComponent.displayName = 'ButtonComponent';
var _default = ButtonComponent;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0J1dHRvbkNvbXBvbmVudC5qcyJdLCJuYW1lcyI6WyJTdHlsZWRCdXR0b24iLCJzdHlsZWQiLCJidXR0b24iLCJhdHRycyIsInByb3BzIiwiY2xhc3NOYW1lIiwiaXNCbG9jayIsInNpemUiLCJ2YXJpYW50IiwiYXBwZW5kIiwicHJlcGVuZCIsImZvbnRTaXplIiwiZm9udFdlaWdodCIsIm1hcmdpbiIsIndpZHRoIiwiaGVpZ2h0IiwiZGlzYWJsZWQiLCJpc1JvdW5kZWQiLCJib3JkZXJSYWRpdXMiLCJib3JkZXJDb2xvciIsImJvcmRlclN0eWxlIiwidGV4dENvbG9yIiwiYmFja2dyb3VuZCIsIkJ1dHRvbkNvbXBvbmVudCIsImFzIiwiY2hpbGRyZW4iLCJkYXRhSW50ZXJjb21UYXJnZXQiLCJpY29uQ29uZmlnIiwiaHJlZiIsIm9uQ2xpY2siLCJ0YXJnZXQiLCJ0byIsInZpc2libGUiLCJ0eXBlIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwic3RyaW5nIiwib25lT2ZUeXBlIiwib2JqZWN0IiwiYm9vbCIsImZ1bmMiLCJkaXNwbGF5TmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsWUFBWSxHQUFHQywwQkFBT0MsTUFBUCxDQUFjQyxLQUFkLENBQW9CLFVBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ2pEQyxJQUFBQSxTQUFTLGVBQVFELEtBQUssQ0FBQ0UsT0FBTixHQUFnQixZQUFoQixHQUErQixFQUF2QyxTQUNMRixLQUFLLENBQUNHLElBQU4sa0JBQXFCSCxLQUFLLENBQUNHLElBQTNCLE1BREssU0FFTkgsS0FBSyxDQUFDSSxPQUFOLGtCQUF3QkosS0FBSyxDQUFDSSxPQUE5QixNQUZNLFNBR0xKLEtBQUssQ0FBQ0ssTUFBTixHQUFlLE9BQWYsR0FBeUIsRUFIcEIsU0FJTkwsS0FBSyxDQUFDTSxPQUFOLEdBQWdCLE9BQWhCLEdBQTBCLEVBSnBCO0FBRHdDLEdBQVo7QUFBQSxDQUFwQixDQUFILHNlQVNIO0FBQUEsTUFBR0osT0FBSCxRQUFHQSxPQUFIO0FBQUEsU0FBa0JBLE9BQU8sR0FBRyxPQUFILEdBQWEsYUFBdEM7QUFBQSxDQVRHLEVBV1o7QUFBQSxNQUFHSyxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFrQkEsUUFBUSx5QkFBa0JBLFFBQWxCLENBQTFCO0FBQUEsQ0FYWSxFQVlaO0FBQUEsTUFBR0MsVUFBSCxTQUFHQSxVQUFIO0FBQUEsU0FBb0JBLFVBQVUsMkJBQW9CQSxVQUFwQixDQUE5QjtBQUFBLENBWlksRUFhWjtBQUFBLE1BQUdDLE1BQUgsU0FBR0EsTUFBSDtBQUFBLFNBQWdCQSxNQUFNLHNCQUFlQSxNQUFmLENBQXRCO0FBQUEsQ0FiWSxFQWNaO0FBQUEsTUFBR0MsS0FBSCxTQUFHQSxLQUFIO0FBQUEsU0FBZUEsS0FBSyxxQkFBY0EsS0FBZCxDQUFwQjtBQUFBLENBZFksRUFlWjtBQUFBLE1BQUdDLE1BQUgsU0FBR0EsTUFBSDtBQUFBLFNBQWdCQSxNQUFNLHNCQUFlQSxNQUFmLENBQXRCO0FBQUEsQ0FmWSxFQTJCWjtBQUFBLE1BQUdQLE9BQUgsU0FBR0EsT0FBSDtBQUFBLFNBQ0VBLE9BQU8sS0FBSyxRQUFaLHdLQURGO0FBQUEsQ0EzQlksRUFzQ1o7QUFBQSxNQUFHQSxPQUFILFNBQUdBLE9BQUg7QUFBQSxTQUNFQSxPQUFPLEtBQUssU0FBWixzRUFERjtBQUFBLENBdENZLEVBNkNaO0FBQUEsTUFBR0EsT0FBSCxTQUFHQSxPQUFIO0FBQUEsU0FDRUEsT0FBTyxLQUFLLFVBQVosc0VBREY7QUFBQSxDQTdDWSxFQW9EWjtBQUFBLE1BQUdBLE9BQUgsVUFBR0EsT0FBSDtBQUFBLFNBQ0VBLE9BQU8sS0FBSyxVQUFaLDZHQURGO0FBQUEsQ0FwRFksRUE0RFo7QUFBQSxNQUFHQSxPQUFILFVBQUdBLE9BQUg7QUFBQSxTQUNFQSxPQUFPLEtBQUssZUFBWiw2R0FERjtBQUFBLENBNURZLEVBcUVaO0FBQUEsTUFBR1EsUUFBSCxVQUFHQSxRQUFIO0FBQUEsU0FDRUEsUUFBUSw0RkFEVjtBQUFBLENBckVZLEVBNkVaO0FBQUEsTUFBR0MsU0FBSCxVQUFHQSxTQUFIO0FBQUEsTUFBY0MsWUFBZCxVQUFjQSxZQUFkO0FBQUEsU0FDRUQsU0FBUyw2QkFBc0JDLFlBQXRCLE1BRFg7QUFBQSxDQTdFWSxFQStFWjtBQUFBLE1BQUdDLFdBQUgsVUFBR0EsV0FBSDtBQUFBLFNBQXFCQSxXQUFXLDRCQUFxQkEsV0FBckIsQ0FBaEM7QUFBQSxDQS9FWSxFQWdGWjtBQUFBLE1BQUdDLFdBQUgsVUFBR0EsV0FBSDtBQUFBLFNBQXFCQSxXQUFXLDRCQUFxQkEsV0FBckIsQ0FBaEM7QUFBQSxDQWhGWSxFQWlGWjtBQUFBLE1BQUdDLFNBQUgsVUFBR0EsU0FBSDtBQUFBLFNBQW1CQSxTQUFTLHFCQUFjQSxTQUFkLENBQTVCO0FBQUEsQ0FqRlksRUFrRlo7QUFBQSxNQUFHQyxVQUFILFVBQUdBLFVBQUg7QUFBQSxTQUFvQkEsVUFBVSwwQkFBbUJBLFVBQW5CLENBQTlCO0FBQUEsQ0FsRlksQ0FBbEI7O0FBcUZBLElBQU1DLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsU0EyQmxCO0FBQUEsTUExQkZDLEVBMEJFLFVBMUJGQSxFQTBCRTtBQUFBLE1BekJGRixVQXlCRSxVQXpCRkEsVUF5QkU7QUFBQSxNQXhCRkgsV0F3QkUsVUF4QkZBLFdBd0JFO0FBQUEsbUNBdkJGRCxZQXVCRTtBQUFBLE1BdkJGQSxZQXVCRSxvQ0F2QmEsTUF1QmI7QUFBQSxNQXRCRkUsV0FzQkUsVUF0QkZBLFdBc0JFO0FBQUEsTUFyQkZLLFFBcUJFLFVBckJGQSxRQXFCRTtBQUFBLE1BcEJGcEIsU0FvQkUsVUFwQkZBLFNBb0JFO0FBQUEsTUFuQkZxQixrQkFtQkUsVUFuQkZBLGtCQW1CRTtBQUFBLE1BbEJGVixRQWtCRSxVQWxCRkEsUUFrQkU7QUFBQSxNQWpCRlcsVUFpQkUsVUFqQkZBLFVBaUJFO0FBQUEsTUFoQkZmLFVBZ0JFLFVBaEJGQSxVQWdCRTtBQUFBLE1BZkZELFFBZUUsVUFmRkEsUUFlRTtBQUFBLE1BZEZJLE1BY0UsVUFkRkEsTUFjRTtBQUFBLE1BYkZhLElBYUUsVUFiRkEsSUFhRTtBQUFBLE1BWkZDLE9BWUUsVUFaRkEsT0FZRTtBQUFBLE1BWEZ2QixPQVdFLFVBWEZBLE9BV0U7QUFBQSxnQ0FWRlcsU0FVRTtBQUFBLE1BVkZBLFNBVUUsaUNBVlUsSUFVVjtBQUFBLE1BVEZKLE1BU0UsVUFURkEsTUFTRTtBQUFBLE1BUkZMLE9BUUUsVUFSRkEsT0FRRTtBQUFBLE1BUEZELElBT0UsVUFQRkEsSUFPRTtBQUFBLE1BTkZ1QixNQU1FLFVBTkZBLE1BTUU7QUFBQSxNQUxGVCxTQUtFLFVBTEZBLFNBS0U7QUFBQSxNQUpGUCxLQUlFLFVBSkZBLEtBSUU7QUFBQSxNQUhGaUIsRUFHRSxVQUhGQSxFQUdFO0FBQUEsOEJBRkZDLE9BRUU7QUFBQSxNQUZGQSxPQUVFLCtCQUZRLElBRVI7QUFBQSxNQURGQyxJQUNFLFVBREZBLElBQ0U7QUFDRixTQUFPRCxPQUFPLGdCQUNWLHNCQUFDLFlBQUQ7QUFDSSxJQUFBLE1BQU0sRUFBRUwsVUFBVSxHQUFHQSxVQUFVLENBQUNsQixNQUFkLEdBQXVCLElBRDdDO0FBRUksSUFBQSxFQUFFLEVBQUVlLEVBRlI7QUFHSSxJQUFBLFVBQVUsRUFBRUYsVUFIaEI7QUFJSSxJQUFBLFdBQVcsRUFBRUgsV0FKakI7QUFLSSxJQUFBLFlBQVksRUFBRUQsWUFMbEI7QUFNSSxJQUFBLFdBQVcsRUFBRUUsV0FOakI7QUFPSSxJQUFBLFNBQVMsRUFBRWYsU0FQZjtBQVFJLDRCQUFzQnFCLGtCQVIxQjtBQVNJLElBQUEsT0FBTyxFQUFFcEIsT0FUYjtBQVVJLElBQUEsU0FBUyxFQUFFVyxTQVZmO0FBV0ksSUFBQSxRQUFRLEVBQUVELFFBWGQ7QUFZSSxJQUFBLFFBQVEsRUFBRUwsUUFaZDtBQWFJLElBQUEsVUFBVSxFQUFFQyxVQWJoQjtBQWNJLElBQUEsTUFBTSxFQUFFRyxNQWRaO0FBZUksSUFBQSxJQUFJLEVBQUVhLElBZlY7QUFnQkksSUFBQSxPQUFPLEVBQUVDLE9BaEJiO0FBaUJJLElBQUEsT0FBTyxFQUFFckIsT0FqQmI7QUFrQkksSUFBQSxPQUFPLEVBQUVtQixVQUFVLEdBQUdBLFVBQVUsQ0FBQ2pCLE9BQWQsR0FBd0IsSUFsQi9DO0FBbUJJLElBQUEsSUFBSSxFQUFFSCxJQW5CVjtBQW9CSSxJQUFBLE1BQU0sRUFBRXVCLE1BcEJaO0FBcUJJLElBQUEsU0FBUyxFQUFFVCxTQXJCZjtBQXNCSSxJQUFBLEtBQUssRUFBRVAsS0F0Qlg7QUF1QkksSUFBQSxNQUFNLEVBQUVELE1BdkJaO0FBd0JJLElBQUEsRUFBRSxFQUFFa0IsRUF4QlI7QUF5QkksSUFBQSxJQUFJLEVBQUVFLElBekJWO0FBQUEsZUEyQktOLFVBQVUsSUFBSUEsVUFBVSxDQUFDakIsT0FBekIsaUJBQ0cscUJBQUMsb0JBQUQ7QUFDSSxNQUFBLFNBQVMsRUFBRWUsUUFBUTtBQUR2QixPQUVRRSxVQUFVLENBQUNqQixPQUZuQixFQTVCUixlQWtDSTtBQUFNLE1BQUEsU0FBUyxFQUFDLGFBQWhCO0FBQUEsZ0JBQStCZTtBQUEvQixNQWxDSixFQW9DS0UsVUFBVSxJQUFJQSxVQUFVLENBQUNsQixNQUF6QixpQkFDRyxxQkFBQyxvQkFBRDtBQUFhLE1BQUEsU0FBUyxFQUFDO0FBQXZCLE9BQWtDa0IsVUFBVSxDQUFDbEIsTUFBN0MsRUFyQ1I7QUFBQSxJQURVLEdBeUNWLElBekNKO0FBMENILENBdEVEOztBQXVFQWMsZUFBZSxDQUFDVyxTQUFoQixHQUE0QjtBQUN4QlYsRUFBQUEsRUFBRSxFQUFFVyxtQkFBVUMsTUFEVTtBQUV4QmQsRUFBQUEsVUFBVSxFQUFFYSxtQkFBVUMsTUFGRTtBQUd4QmpCLEVBQUFBLFdBQVcsRUFBRWdCLG1CQUFVQyxNQUhDO0FBSXhCbEIsRUFBQUEsWUFBWSxFQUFFaUIsbUJBQVVDLE1BSkE7QUFLeEJoQixFQUFBQSxXQUFXLEVBQUVlLG1CQUFVQyxNQUxDO0FBTXhCWCxFQUFBQSxRQUFRLEVBQUVVLG1CQUFVRSxTQUFWLENBQW9CLENBQUNGLG1CQUFVQyxNQUFYLEVBQW1CRCxtQkFBVUcsTUFBN0IsQ0FBcEIsQ0FOYztBQU94QmpDLEVBQUFBLFNBQVMsRUFBRThCLG1CQUFVQyxNQVBHO0FBUXhCVixFQUFBQSxrQkFBa0IsRUFBRVMsbUJBQVVDLE1BUk47QUFTeEJwQixFQUFBQSxRQUFRLEVBQUVtQixtQkFBVUksSUFUSTtBQVV4QlosRUFBQUEsVUFBVSxFQUFFUSxtQkFBVUcsTUFWRTtBQVd4QjFCLEVBQUFBLFVBQVUsRUFBRXVCLG1CQUFVQyxNQVhFO0FBWXhCekIsRUFBQUEsUUFBUSxFQUFFd0IsbUJBQVVDLE1BWkk7QUFheEJyQixFQUFBQSxNQUFNLEVBQUVvQixtQkFBVUMsTUFiTTtBQWN4QlIsRUFBQUEsSUFBSSxFQUFFTyxtQkFBVUMsTUFkUTtBQWV4QlAsRUFBQUEsT0FBTyxFQUFFTSxtQkFBVUssSUFmSztBQWdCeEJsQyxFQUFBQSxPQUFPLEVBQUU2QixtQkFBVUksSUFoQks7QUFpQnhCdEIsRUFBQUEsU0FBUyxFQUFFa0IsbUJBQVVJLElBakJHO0FBa0J4QjFCLEVBQUFBLE1BQU0sRUFBRXNCLG1CQUFVQyxNQWxCTTtBQW1CeEI1QixFQUFBQSxPQUFPLEVBQUUyQixtQkFBVUMsTUFuQks7QUFvQnhCN0IsRUFBQUEsSUFBSSxFQUFFNEIsbUJBQVVDLE1BcEJRO0FBcUJ4Qk4sRUFBQUEsTUFBTSxFQUFFSyxtQkFBVUMsTUFyQk07QUFzQnhCZixFQUFBQSxTQUFTLEVBQUVjLG1CQUFVQyxNQXRCRztBQXVCeEJ0QixFQUFBQSxLQUFLLEVBQUVxQixtQkFBVUMsTUF2Qk87QUF3QnhCTCxFQUFBQSxFQUFFLEVBQUVJLG1CQUFVQyxNQXhCVTtBQXlCeEJKLEVBQUFBLE9BQU8sRUFBRUcsbUJBQVVJLElBekJLO0FBMEJ4Qk4sRUFBQUEsSUFBSSxFQUFFRSxtQkFBVUM7QUExQlEsQ0FBNUI7QUE0QkFiLGVBQWUsQ0FBQ2tCLFdBQWhCLEdBQThCLGlCQUE5QjtlQUVlbEIsZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuaW1wb3J0IFJvdW5kZWRJY29uIGZyb20gJy4vUm91bmRlZEljb24nO1xuXG5jb25zdCBTdHlsZWRCdXR0b24gPSBzdHlsZWQuYnV0dG9uLmF0dHJzKChwcm9wcykgPT4gKHtcbiAgICBjbGFzc05hbWU6IGBidG4ke3Byb3BzLmlzQmxvY2sgPyAnIGJ0bi1ibG9jaycgOiAnJ30ke1xuICAgICAgICBwcm9wcy5zaXplID8gYCBidG4tJHtwcm9wcy5zaXplfWAgOiBgYFxuICAgIH0ke3Byb3BzLnZhcmlhbnQgPyBgIGJ0bi0ke3Byb3BzLnZhcmlhbnR9YCA6IGBgfSR7XG4gICAgICAgIHByb3BzLmFwcGVuZCA/ICcgcHItMScgOiAnJ1xuICAgIH0ke3Byb3BzLnByZXBlbmQgPyAnIHBsLTEnIDogJyd9YCxcbn0pKWBcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBkaXNwbGF5OiAkeyh7IGlzQmxvY2sgfSkgPT4gKGlzQmxvY2sgPyAnYmxvY2snIDogJ2lubGluZS1mbGV4Jyl9O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAkeyh7IGZvbnRTaXplIH0pID0+IGZvbnRTaXplICYmIGBmb250LXNpemU6ICR7Zm9udFNpemV9YH07XG4gICAgJHsoeyBmb250V2VpZ2h0IH0pID0+IGZvbnRXZWlnaHQgJiYgYGZvbnQtd2VpZ2h0OiAke2ZvbnRXZWlnaHR9YH07XG4gICAgJHsoeyBtYXJnaW4gfSkgPT4gbWFyZ2luICYmIGBtYXJnaW46ICR7bWFyZ2lufWB9O1xuICAgICR7KHsgd2lkdGggfSkgPT4gd2lkdGggJiYgYHdpZHRoOiAke3dpZHRofWB9O1xuICAgICR7KHsgaGVpZ2h0IH0pID0+IGhlaWdodCAmJiBgaGVpZ2h0OiAke2hlaWdodH1gfTtcblxuICAgIC5mb2N1cyxcbiAgICA6Zm9jdXMge1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cblxuICAgICYuYnRuLXNtIHtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgIH1cblxuICAgICR7KHsgdmFyaWFudCB9KSA9PlxuICAgICAgICB2YXJpYW50ID09PSAnY2FuY2VsJyAmJlxuICAgICAgICBgXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNEM0Q3RUI7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNFRkYxRkI7XG4gICAgICAgIGNvbG9yOiAjODQ4QkFCO1xuICAgICAgICAmOmhvdmVyIHtcbiAgICAgICAgICAgIGNvbG9yOiAjODQ4QkFCO1xuICAgICAgICB9XG4gICAgYH1cblxuICAgICR7KHsgdmFyaWFudCB9KSA9PlxuICAgICAgICB2YXJpYW50ID09PSAnY29uZmlybScgJiZcbiAgICAgICAgYFxuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmOWEwMDtcbiAgICBgfVxuXG4gICAgJHsoeyB2YXJpYW50IH0pID0+XG4gICAgICAgIHZhcmlhbnQgPT09ICdhcHByb3ZlZCcgJiZcbiAgICAgICAgYFxuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzJCRjJBQjtcbiAgICBgfVxuXG4gICAgJHsoeyB2YXJpYW50IH0pID0+XG4gICAgICAgIHZhcmlhbnQgPT09ICdyZWplY3RlZCcgJiZcbiAgICAgICAgYFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGRkZGO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjRkE2OTY4O1xuICAgICAgICBjb2xvcjogI0ZBNjk2ODtcbiAgICBgfVxuXG4gICAgJHsoeyB2YXJpYW50IH0pID0+XG4gICAgICAgIHZhcmlhbnQgPT09ICdyZWplY3QtcmVhc29uJyAmJlxuICAgICAgICBgXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNGN0ZDRkY7XG4gICAgICAgIGNvbG9yOiAjODQ4QkFCO1xuICAgICAgICBmb250LXNpemU6IDEwcHg7XG4gICAgICAgIFxuICAgIGB9ICAgIFxuXG4gICAgJHsoeyBkaXNhYmxlZCB9KSA9PlxuICAgICAgICBkaXNhYmxlZCAmJlxuICAgICAgICBgXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xuICAgICAgICBvcGFjaXR5OiAwLjU7XG4gICAgICAgIGN1cnNvcjogZGVmYXVsdDtcbiAgICBgfVxuXG4gICAgJHsoeyBpc1JvdW5kZWQsIGJvcmRlclJhZGl1cyB9KSA9PlxuICAgICAgICBpc1JvdW5kZWQgJiYgYGJvcmRlci1yYWRpdXM6ICR7Ym9yZGVyUmFkaXVzfTtgfVxuICAgICR7KHsgYm9yZGVyQ29sb3IgfSkgPT4gYm9yZGVyQ29sb3IgJiYgYGJvcmRlci1jb2xvcjogJHtib3JkZXJDb2xvcn1gfVxuICAgICR7KHsgYm9yZGVyU3R5bGUgfSkgPT4gYm9yZGVyU3R5bGUgJiYgYGJvcmRlci1zdHlsZTogJHtib3JkZXJTdHlsZX1gfVxuICAgICR7KHsgdGV4dENvbG9yIH0pID0+IHRleHRDb2xvciAmJiBgY29sb3I6ICR7dGV4dENvbG9yfWB9XG4gICAgJHsoeyBiYWNrZ3JvdW5kIH0pID0+IGJhY2tncm91bmQgJiYgYGJhY2tncm91bmQ6ICR7YmFja2dyb3VuZH1gfVxuYDtcblxuY29uc3QgQnV0dG9uQ29tcG9uZW50ID0gKHtcbiAgICBhcyxcbiAgICBiYWNrZ3JvdW5kLFxuICAgIGJvcmRlckNvbG9yLFxuICAgIGJvcmRlclJhZGl1cyA9ICczMHB4JyxcbiAgICBib3JkZXJTdHlsZSxcbiAgICBjaGlsZHJlbixcbiAgICBjbGFzc05hbWUsXG4gICAgZGF0YUludGVyY29tVGFyZ2V0LFxuICAgIGRpc2FibGVkLFxuICAgIGljb25Db25maWcsXG4gICAgZm9udFdlaWdodCxcbiAgICBmb250U2l6ZSxcbiAgICBoZWlnaHQsXG4gICAgaHJlZixcbiAgICBvbkNsaWNrLFxuICAgIGlzQmxvY2ssXG4gICAgaXNSb3VuZGVkID0gdHJ1ZSxcbiAgICBtYXJnaW4sXG4gICAgdmFyaWFudCxcbiAgICBzaXplLFxuICAgIHRhcmdldCxcbiAgICB0ZXh0Q29sb3IsXG4gICAgd2lkdGgsXG4gICAgdG8sXG4gICAgdmlzaWJsZSA9IHRydWUsXG4gICAgdHlwZSxcbn0pID0+IHtcbiAgICByZXR1cm4gdmlzaWJsZSA/IChcbiAgICAgICAgPFN0eWxlZEJ1dHRvblxuICAgICAgICAgICAgYXBwZW5kPXtpY29uQ29uZmlnID8gaWNvbkNvbmZpZy5hcHBlbmQgOiBudWxsfVxuICAgICAgICAgICAgYXM9e2FzfVxuICAgICAgICAgICAgYmFja2dyb3VuZD17YmFja2dyb3VuZH1cbiAgICAgICAgICAgIGJvcmRlckNvbG9yPXtib3JkZXJDb2xvcn1cbiAgICAgICAgICAgIGJvcmRlclJhZGl1cz17Ym9yZGVyUmFkaXVzfVxuICAgICAgICAgICAgYm9yZGVyU3R5bGU9e2JvcmRlclN0eWxlfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc05hbWV9XG4gICAgICAgICAgICBkYXRhLWludGVyY29tLXRhcmdldD17ZGF0YUludGVyY29tVGFyZ2V0fVxuICAgICAgICAgICAgaXNCbG9jaz17aXNCbG9ja31cbiAgICAgICAgICAgIGlzUm91bmRlZD17aXNSb3VuZGVkfVxuICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAgICAgZm9udFNpemU9e2ZvbnRTaXplfVxuICAgICAgICAgICAgZm9udFdlaWdodD17Zm9udFdlaWdodH1cbiAgICAgICAgICAgIGhlaWdodD17aGVpZ2h0fVxuICAgICAgICAgICAgaHJlZj17aHJlZn1cbiAgICAgICAgICAgIG9uQ2xpY2s9e29uQ2xpY2t9XG4gICAgICAgICAgICB2YXJpYW50PXt2YXJpYW50fVxuICAgICAgICAgICAgcHJlcGVuZD17aWNvbkNvbmZpZyA/IGljb25Db25maWcucHJlcGVuZCA6IG51bGx9XG4gICAgICAgICAgICBzaXplPXtzaXplfVxuICAgICAgICAgICAgdGFyZ2V0PXt0YXJnZXR9XG4gICAgICAgICAgICB0ZXh0Q29sb3I9e3RleHRDb2xvcn1cbiAgICAgICAgICAgIHdpZHRoPXt3aWR0aH1cbiAgICAgICAgICAgIG1hcmdpbj17bWFyZ2lufVxuICAgICAgICAgICAgdG89e3RvfVxuICAgICAgICAgICAgdHlwZT17dHlwZX1cbiAgICAgICAgPlxuICAgICAgICAgICAge2ljb25Db25maWcgJiYgaWNvbkNvbmZpZy5wcmVwZW5kICYmIChcbiAgICAgICAgICAgICAgICA8Um91bmRlZEljb25cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjaGlsZHJlbiA/IGBtci0yYCA6IGBgfVxuICAgICAgICAgICAgICAgICAgICB7Li4uaWNvbkNvbmZpZy5wcmVwZW5kfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApfVxuXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJmbGV4LWdyb3ctMVwiPntjaGlsZHJlbn08L3NwYW4+XG5cbiAgICAgICAgICAgIHtpY29uQ29uZmlnICYmIGljb25Db25maWcuYXBwZW5kICYmIChcbiAgICAgICAgICAgICAgICA8Um91bmRlZEljb24gY2xhc3NOYW1lPVwibWwtMlwiIHsuLi5pY29uQ29uZmlnLmFwcGVuZH0gLz5cbiAgICAgICAgICAgICl9XG4gICAgICAgIDwvU3R5bGVkQnV0dG9uPlxuICAgICkgOiBudWxsO1xufTtcbkJ1dHRvbkNvbXBvbmVudC5wcm9wVHlwZXMgPSB7XG4gICAgYXM6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgYmFja2dyb3VuZDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBib3JkZXJDb2xvcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBib3JkZXJSYWRpdXM6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgYm9yZGVyU3R5bGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2hpbGRyZW46IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5vYmplY3RdKSxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGF0YUludGVyY29tVGFyZ2V0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBpY29uQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxuICAgIGZvbnRXZWlnaHQ6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZm9udFNpemU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgaGVpZ2h0OiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGhyZWY6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgb25DbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaXNCbG9jazogUHJvcFR5cGVzLmJvb2wsXG4gICAgaXNSb3VuZGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBtYXJnaW46IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdmFyaWFudDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBzaXplOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIHRhcmdldDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB0ZXh0Q29sb3I6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgd2lkdGg6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdG86IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG4gICAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbn07XG5CdXR0b25Db21wb25lbnQuZGlzcGxheU5hbWUgPSAnQnV0dG9uQ29tcG9uZW50JztcblxuZXhwb3J0IGRlZmF1bHQgQnV0dG9uQ29tcG9uZW50O1xuIl19