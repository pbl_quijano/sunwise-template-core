"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _jsxRuntime = require("react/jsx-runtime");

var _templateObject, _templateObject2, _templateObject3;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Wrapper = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    display: inline-block;\n    &.disabled {\n        opacity: 0.6;\n        pointer-events: none;\n    }\n"])));

var StyledRoundedIcon = _styledComponents.default.div(_templateObject2 || (_templateObject2 = _taggedTemplateLiteral(["\n    align-items: center;\n    background-color: ", ";\n    border-radius: 30px;\n    border: ", ";\n    display: flex;\n    height: 30px;\n    justify-content: center;\n    text-align: center;\n    width: 30px;\n    border-radius: 100%;\n\n    ", "\n\n    ", "\n    ", "\n"])), function (_ref) {
  var bgColor = _ref.bgColor,
      noBackground = _ref.noBackground;
  return noBackground ? "none" : bgColor ? bgColor : "#F8FCFF";
}, function (_ref2) {
  var noBorder = _ref2.noBorder,
      borderColor = _ref2.borderColor;
  return noBorder ? "none" : "1px solid ".concat(borderColor ? "".concat(borderColor) : "#F1F7FD");
}, function (_ref3) {
  var customSize = _ref3.customSize,
      size = _ref3.size;

  switch (size) {
    case 'xs':
      return "\n                    height: 20px;\n                    width: 20px;\n                ";

    case 'md':
      return "\n                    height: 40px;\n                    width: 40px;\n                ";

    case 'lg':
      return "\n                    height: 50px;\n                    width: 50px;\n                ";

    default:
      return "\n                    height: ".concat(customSize, "px;\n                    width: ").concat(customSize, "px;\n                ");
  }
}, function (_ref4) {
  var clickable = _ref4.clickable,
      onClick = _ref4.onClick;
  return (clickable || onClick) && "cursor: pointer;";
}, function (_ref5) {
  var disabled = _ref5.disabled;
  return disabled && "cursor: not-allowed;pointer-events: none;";
});

var StyledIcon = _styledComponents.default.span(_templateObject3 || (_templateObject3 = _taggedTemplateLiteral(["\n    color: ", ";\n    font-size: ", ";\n    img {\n        display: block;\n    }\n"])), function (_ref6) {
  var iconColor = _ref6.iconColor;
  return iconColor ? iconColor : '#1F3C53';
}, function (_ref7) {
  var size = _ref7.size,
      customIconSize = _ref7.customIconSize;
  return size === 'custom' && customIconSize ? "".concat(customIconSize, "px") : size === 'xs' ? "10px" : size === 'md' ? "15px" : size === 'lg' ? "25px" : "11px";
});
/**
 *
 * Componente RoundedIcon.
 *
 * @param size Medidas xs | sm | md | lg | custom
 * @param customSize Si size=custom tomará este valor
 *        para agregar a las medidas.
 * @param icon Clase o caracter representando el icóno.
 * @param customIconSize Tamaño del icono personalizable.
 * @param bgColor Color del fondo.
 * @param borderColor Color del borde.
 * @param iconColor Color del icono.
 * @param noBorder True si no se desea el borde.
 * @param noBackgroun True si no se desea el fondo.
 * @param isIconLetter True si el icono es un caracter.
 * @param clickable True si se desea que cuando pase el raton
 *                  encima muestre la mano del mouse.
 * @param handleClik Funcion al hacer click al elemento.
 */


var RoundedIcon = function RoundedIcon(_ref8) {
  var bgColor = _ref8.bgColor,
      borderColor = _ref8.borderColor,
      className = _ref8.className,
      clickable = _ref8.clickable,
      customIconSize = _ref8.customIconSize,
      customSize = _ref8.customSize,
      disabled = _ref8.disabled,
      handleClick = _ref8.handleClick,
      icon = _ref8.icon,
      iconColor = _ref8.iconColor,
      iconImg = _ref8.iconImg,
      isIconLetter = _ref8.isIconLetter,
      noBackground = _ref8.noBackground,
      noBorder = _ref8.noBorder,
      size = _ref8.size,
      _ref8$visible = _ref8.visible,
      visible = _ref8$visible === void 0 ? true : _ref8$visible;

  if (!visible) {
    return null;
  }

  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Wrapper, {
    className: disabled && 'disabled',
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledRoundedIcon, {
      bgColor: bgColor,
      borderColor: borderColor,
      className: className,
      clickable: clickable,
      customSize: customSize,
      noBackground: noBackground,
      noBorder: noBorder,
      onClick: handleClick,
      size: size,
      disabled: disabled,
      children: /*#__PURE__*/(0, _jsxRuntime.jsx)(StyledIcon, {
        className: isIconLetter || icon,
        customIconSize: customIconSize,
        iconColor: iconColor,
        size: size,
        children: iconImg ? /*#__PURE__*/(0, _jsxRuntime.jsx)("img", {
          alt: "",
          src: iconImg,
          width: "14"
        }) : isIconLetter && icon
      })
    })
  });
};

RoundedIcon.propTypes = {
  bgColor: _propTypes.default.string,
  borderColor: _propTypes.default.string,
  className: _propTypes.default.string,
  clickable: _propTypes.default.bool,
  customIconSize: _propTypes.default.string,
  customSize: _propTypes.default.string,
  disabled: _propTypes.default.bool,
  handleClick: _propTypes.default.func,
  icon: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.element]),
  iconColor: _propTypes.default.oneOfType([_propTypes.default.bool, _propTypes.default.string]),
  iconImg: _propTypes.default.string,
  isIconLetter: _propTypes.default.bool,
  noBackground: _propTypes.default.bool,
  noBorder: _propTypes.default.bool,
  size: _propTypes.default.string,
  visible: _propTypes.default.bool
};
var _default = RoundedIcon;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL1JvdW5kZWRJY29uLmpzIl0sIm5hbWVzIjpbIldyYXBwZXIiLCJzdHlsZWQiLCJkaXYiLCJTdHlsZWRSb3VuZGVkSWNvbiIsImJnQ29sb3IiLCJub0JhY2tncm91bmQiLCJub0JvcmRlciIsImJvcmRlckNvbG9yIiwiY3VzdG9tU2l6ZSIsInNpemUiLCJjbGlja2FibGUiLCJvbkNsaWNrIiwiZGlzYWJsZWQiLCJTdHlsZWRJY29uIiwic3BhbiIsImljb25Db2xvciIsImN1c3RvbUljb25TaXplIiwiUm91bmRlZEljb24iLCJjbGFzc05hbWUiLCJoYW5kbGVDbGljayIsImljb24iLCJpY29uSW1nIiwiaXNJY29uTGV0dGVyIiwidmlzaWJsZSIsInByb3BUeXBlcyIsIlByb3BUeXBlcyIsInN0cmluZyIsImJvb2wiLCJmdW5jIiwib25lT2ZUeXBlIiwiZWxlbWVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOzs7Ozs7Ozs7O0FBRUEsSUFBTUEsT0FBTyxHQUFHQywwQkFBT0MsR0FBVixrTEFBYjs7QUFRQSxJQUFNQyxpQkFBaUIsR0FBR0YsMEJBQU9DLEdBQVYsMFZBRUM7QUFBQSxNQUFHRSxPQUFILFFBQUdBLE9BQUg7QUFBQSxNQUFZQyxZQUFaLFFBQVlBLFlBQVo7QUFBQSxTQUNoQkEsWUFBWSxZQUFZRCxPQUFPLEdBQUdBLE9BQUgsWUFEZjtBQUFBLENBRkQsRUFLVDtBQUFBLE1BQUdFLFFBQUgsU0FBR0EsUUFBSDtBQUFBLE1BQWFDLFdBQWIsU0FBYUEsV0FBYjtBQUFBLFNBQ05ELFFBQVEsZ0NBRVdDLFdBQVcsYUFBTUEsV0FBTixhQUZ0QixDQURGO0FBQUEsQ0FMUyxFQWdCakIsaUJBQTBCO0FBQUEsTUFBdkJDLFVBQXVCLFNBQXZCQSxVQUF1QjtBQUFBLE1BQVhDLElBQVcsU0FBWEEsSUFBVzs7QUFDeEIsVUFBUUEsSUFBUjtBQUNJLFNBQUssSUFBTDtBQUNJOztBQUlKLFNBQUssSUFBTDtBQUNJOztBQUlKLFNBQUssSUFBTDtBQUNJOztBQUlKO0FBQ0kscURBQ2NELFVBRGQsNkNBRWFBLFVBRmI7QUFqQlI7QUFzQkgsQ0F2Q2tCLEVBeUNqQjtBQUFBLE1BQUdFLFNBQUgsU0FBR0EsU0FBSDtBQUFBLE1BQWNDLE9BQWQsU0FBY0EsT0FBZDtBQUFBLFNBQTRCLENBQUNELFNBQVMsSUFBSUMsT0FBZCx1QkFBNUI7QUFBQSxDQXpDaUIsRUEwQ2pCO0FBQUEsTUFBR0MsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBa0JBLFFBQVEsK0NBQTFCO0FBQUEsQ0ExQ2lCLENBQXZCOztBQTZDQSxJQUFNQyxVQUFVLEdBQUdaLDBCQUFPYSxJQUFWLDZKQUNIO0FBQUEsTUFBR0MsU0FBSCxTQUFHQSxTQUFIO0FBQUEsU0FBb0JBLFNBQVMsR0FBR0EsU0FBSCxHQUFlLFNBQTVDO0FBQUEsQ0FERyxFQUVDO0FBQUEsTUFBR04sSUFBSCxTQUFHQSxJQUFIO0FBQUEsTUFBU08sY0FBVCxTQUFTQSxjQUFUO0FBQUEsU0FDVFAsSUFBSSxLQUFLLFFBQVQsSUFBcUJPLGNBQXJCLGFBQ1NBLGNBRFQsVUFFTVAsSUFBSSxLQUFLLElBQVQsWUFFQUEsSUFBSSxLQUFLLElBQVQsWUFFQUEsSUFBSSxLQUFLLElBQVQsa0JBUEc7QUFBQSxDQUZELENBQWhCO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxJQUFNUSxXQUFXLEdBQUcsU0FBZEEsV0FBYyxRQWlCZDtBQUFBLE1BaEJGYixPQWdCRSxTQWhCRkEsT0FnQkU7QUFBQSxNQWZGRyxXQWVFLFNBZkZBLFdBZUU7QUFBQSxNQWRGVyxTQWNFLFNBZEZBLFNBY0U7QUFBQSxNQWJGUixTQWFFLFNBYkZBLFNBYUU7QUFBQSxNQVpGTSxjQVlFLFNBWkZBLGNBWUU7QUFBQSxNQVhGUixVQVdFLFNBWEZBLFVBV0U7QUFBQSxNQVZGSSxRQVVFLFNBVkZBLFFBVUU7QUFBQSxNQVRGTyxXQVNFLFNBVEZBLFdBU0U7QUFBQSxNQVJGQyxJQVFFLFNBUkZBLElBUUU7QUFBQSxNQVBGTCxTQU9FLFNBUEZBLFNBT0U7QUFBQSxNQU5GTSxPQU1FLFNBTkZBLE9BTUU7QUFBQSxNQUxGQyxZQUtFLFNBTEZBLFlBS0U7QUFBQSxNQUpGakIsWUFJRSxTQUpGQSxZQUlFO0FBQUEsTUFIRkMsUUFHRSxTQUhGQSxRQUdFO0FBQUEsTUFGRkcsSUFFRSxTQUZGQSxJQUVFO0FBQUEsNEJBREZjLE9BQ0U7QUFBQSxNQURGQSxPQUNFLDhCQURRLElBQ1I7O0FBQ0YsTUFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDVixXQUFPLElBQVA7QUFDSDs7QUFDRCxzQkFDSSxxQkFBQyxPQUFEO0FBQVMsSUFBQSxTQUFTLEVBQUVYLFFBQVEsSUFBSSxVQUFoQztBQUFBLDJCQUNJLHFCQUFDLGlCQUFEO0FBQ0ksTUFBQSxPQUFPLEVBQUVSLE9BRGI7QUFFSSxNQUFBLFdBQVcsRUFBRUcsV0FGakI7QUFHSSxNQUFBLFNBQVMsRUFBRVcsU0FIZjtBQUlJLE1BQUEsU0FBUyxFQUFFUixTQUpmO0FBS0ksTUFBQSxVQUFVLEVBQUVGLFVBTGhCO0FBTUksTUFBQSxZQUFZLEVBQUVILFlBTmxCO0FBT0ksTUFBQSxRQUFRLEVBQUVDLFFBUGQ7QUFRSSxNQUFBLE9BQU8sRUFBRWEsV0FSYjtBQVNJLE1BQUEsSUFBSSxFQUFFVixJQVRWO0FBVUksTUFBQSxRQUFRLEVBQUVHLFFBVmQ7QUFBQSw2QkFZSSxxQkFBQyxVQUFEO0FBQ0ksUUFBQSxTQUFTLEVBQUVVLFlBQVksSUFBSUYsSUFEL0I7QUFFSSxRQUFBLGNBQWMsRUFBRUosY0FGcEI7QUFHSSxRQUFBLFNBQVMsRUFBRUQsU0FIZjtBQUlJLFFBQUEsSUFBSSxFQUFFTixJQUpWO0FBQUEsa0JBTUtZLE9BQU8sZ0JBQ0o7QUFBSyxVQUFBLEdBQUcsRUFBQyxFQUFUO0FBQVksVUFBQSxHQUFHLEVBQUVBLE9BQWpCO0FBQTBCLFVBQUEsS0FBSyxFQUFDO0FBQWhDLFVBREksR0FHSkMsWUFBWSxJQUFJRjtBQVR4QjtBQVpKO0FBREosSUFESjtBQTZCSCxDQWxERDs7QUFvREFILFdBQVcsQ0FBQ08sU0FBWixHQUF3QjtBQUNwQnBCLEVBQUFBLE9BQU8sRUFBRXFCLG1CQUFVQyxNQURDO0FBRXBCbkIsRUFBQUEsV0FBVyxFQUFFa0IsbUJBQVVDLE1BRkg7QUFHcEJSLEVBQUFBLFNBQVMsRUFBRU8sbUJBQVVDLE1BSEQ7QUFJcEJoQixFQUFBQSxTQUFTLEVBQUVlLG1CQUFVRSxJQUpEO0FBS3BCWCxFQUFBQSxjQUFjLEVBQUVTLG1CQUFVQyxNQUxOO0FBTXBCbEIsRUFBQUEsVUFBVSxFQUFFaUIsbUJBQVVDLE1BTkY7QUFPcEJkLEVBQUFBLFFBQVEsRUFBRWEsbUJBQVVFLElBUEE7QUFRcEJSLEVBQUFBLFdBQVcsRUFBRU0sbUJBQVVHLElBUkg7QUFTcEJSLEVBQUFBLElBQUksRUFBRUssbUJBQVVJLFNBQVYsQ0FBb0IsQ0FBQ0osbUJBQVVDLE1BQVgsRUFBbUJELG1CQUFVSyxPQUE3QixDQUFwQixDQVRjO0FBVXBCZixFQUFBQSxTQUFTLEVBQUVVLG1CQUFVSSxTQUFWLENBQW9CLENBQUNKLG1CQUFVRSxJQUFYLEVBQWlCRixtQkFBVUMsTUFBM0IsQ0FBcEIsQ0FWUztBQVdwQkwsRUFBQUEsT0FBTyxFQUFFSSxtQkFBVUMsTUFYQztBQVlwQkosRUFBQUEsWUFBWSxFQUFFRyxtQkFBVUUsSUFaSjtBQWFwQnRCLEVBQUFBLFlBQVksRUFBRW9CLG1CQUFVRSxJQWJKO0FBY3BCckIsRUFBQUEsUUFBUSxFQUFFbUIsbUJBQVVFLElBZEE7QUFlcEJsQixFQUFBQSxJQUFJLEVBQUVnQixtQkFBVUMsTUFmSTtBQWdCcEJILEVBQUFBLE9BQU8sRUFBRUUsbUJBQVVFO0FBaEJDLENBQXhCO2VBbUJlVixXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBzdHlsZWQgZnJvbSAnc3R5bGVkLWNvbXBvbmVudHMnO1xuXG5jb25zdCBXcmFwcGVyID0gc3R5bGVkLmRpdmBcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgJi5kaXNhYmxlZCB7XG4gICAgICAgIG9wYWNpdHk6IDAuNjtcbiAgICAgICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gICAgfVxuYDtcblxuY29uc3QgU3R5bGVkUm91bmRlZEljb24gPSBzdHlsZWQuZGl2YFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHsoeyBiZ0NvbG9yLCBub0JhY2tncm91bmQgfSkgPT5cbiAgICAgICAgbm9CYWNrZ3JvdW5kID8gYG5vbmVgIDogYmdDb2xvciA/IGJnQ29sb3IgOiBgI0Y4RkNGRmB9O1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm9yZGVyOiAkeyh7IG5vQm9yZGVyLCBib3JkZXJDb2xvciB9KSA9PlxuICAgICAgICBub0JvcmRlclxuICAgICAgICAgICAgPyBgbm9uZWBcbiAgICAgICAgICAgIDogYDFweCBzb2xpZCAke2JvcmRlckNvbG9yID8gYCR7Ym9yZGVyQ29sb3J9YCA6IGAjRjFGN0ZEYH1gfTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcblxuICAgICR7KHsgY3VzdG9tU2l6ZSwgc2l6ZSB9KSA9PiB7XG4gICAgICAgIHN3aXRjaCAoc2l6ZSkge1xuICAgICAgICAgICAgY2FzZSAneHMnOlxuICAgICAgICAgICAgICAgIHJldHVybiBgXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDIwcHg7XG4gICAgICAgICAgICAgICAgYDtcbiAgICAgICAgICAgIGNhc2UgJ21kJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gYFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICAgICAgICAgIGA7XG4gICAgICAgICAgICBjYXNlICdsZyc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIGBcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICAgICAgICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgICAgICAgICBgO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gYFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6ICR7Y3VzdG9tU2l6ZX1weDtcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6ICR7Y3VzdG9tU2l6ZX1weDtcbiAgICAgICAgICAgICAgICBgO1xuICAgICAgICB9XG4gICAgfX1cblxuICAgICR7KHsgY2xpY2thYmxlLCBvbkNsaWNrIH0pID0+IChjbGlja2FibGUgfHwgb25DbGljaykgJiYgYGN1cnNvcjogcG9pbnRlcjtgfVxuICAgICR7KHsgZGlzYWJsZWQgfSkgPT4gZGlzYWJsZWQgJiYgYGN1cnNvcjogbm90LWFsbG93ZWQ7cG9pbnRlci1ldmVudHM6IG5vbmU7YH1cbmA7XG5cbmNvbnN0IFN0eWxlZEljb24gPSBzdHlsZWQuc3BhbmBcbiAgICBjb2xvcjogJHsoeyBpY29uQ29sb3IgfSkgPT4gKGljb25Db2xvciA/IGljb25Db2xvciA6ICcjMUYzQzUzJyl9O1xuICAgIGZvbnQtc2l6ZTogJHsoeyBzaXplLCBjdXN0b21JY29uU2l6ZSB9KSA9PlxuICAgICAgICBzaXplID09PSAnY3VzdG9tJyAmJiBjdXN0b21JY29uU2l6ZVxuICAgICAgICAgICAgPyBgJHtjdXN0b21JY29uU2l6ZX1weGBcbiAgICAgICAgICAgIDogc2l6ZSA9PT0gJ3hzJ1xuICAgICAgICAgICAgPyBgMTBweGBcbiAgICAgICAgICAgIDogc2l6ZSA9PT0gJ21kJ1xuICAgICAgICAgICAgPyBgMTVweGBcbiAgICAgICAgICAgIDogc2l6ZSA9PT0gJ2xnJ1xuICAgICAgICAgICAgPyBgMjVweGBcbiAgICAgICAgICAgIDogYDExcHhgfTtcbiAgICBpbWcge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICB9XG5gO1xuXG4vKipcbiAqXG4gKiBDb21wb25lbnRlIFJvdW5kZWRJY29uLlxuICpcbiAqIEBwYXJhbSBzaXplIE1lZGlkYXMgeHMgfCBzbSB8IG1kIHwgbGcgfCBjdXN0b21cbiAqIEBwYXJhbSBjdXN0b21TaXplIFNpIHNpemU9Y3VzdG9tIHRvbWFyw6EgZXN0ZSB2YWxvclxuICogICAgICAgIHBhcmEgYWdyZWdhciBhIGxhcyBtZWRpZGFzLlxuICogQHBhcmFtIGljb24gQ2xhc2UgbyBjYXJhY3RlciByZXByZXNlbnRhbmRvIGVsIGljw7Nuby5cbiAqIEBwYXJhbSBjdXN0b21JY29uU2l6ZSBUYW1hw7FvIGRlbCBpY29ubyBwZXJzb25hbGl6YWJsZS5cbiAqIEBwYXJhbSBiZ0NvbG9yIENvbG9yIGRlbCBmb25kby5cbiAqIEBwYXJhbSBib3JkZXJDb2xvciBDb2xvciBkZWwgYm9yZGUuXG4gKiBAcGFyYW0gaWNvbkNvbG9yIENvbG9yIGRlbCBpY29uby5cbiAqIEBwYXJhbSBub0JvcmRlciBUcnVlIHNpIG5vIHNlIGRlc2VhIGVsIGJvcmRlLlxuICogQHBhcmFtIG5vQmFja2dyb3VuIFRydWUgc2kgbm8gc2UgZGVzZWEgZWwgZm9uZG8uXG4gKiBAcGFyYW0gaXNJY29uTGV0dGVyIFRydWUgc2kgZWwgaWNvbm8gZXMgdW4gY2FyYWN0ZXIuXG4gKiBAcGFyYW0gY2xpY2thYmxlIFRydWUgc2kgc2UgZGVzZWEgcXVlIGN1YW5kbyBwYXNlIGVsIHJhdG9uXG4gKiAgICAgICAgICAgICAgICAgIGVuY2ltYSBtdWVzdHJlIGxhIG1hbm8gZGVsIG1vdXNlLlxuICogQHBhcmFtIGhhbmRsZUNsaWsgRnVuY2lvbiBhbCBoYWNlciBjbGljayBhbCBlbGVtZW50by5cbiAqL1xuY29uc3QgUm91bmRlZEljb24gPSAoe1xuICAgIGJnQ29sb3IsXG4gICAgYm9yZGVyQ29sb3IsXG4gICAgY2xhc3NOYW1lLFxuICAgIGNsaWNrYWJsZSxcbiAgICBjdXN0b21JY29uU2l6ZSxcbiAgICBjdXN0b21TaXplLFxuICAgIGRpc2FibGVkLFxuICAgIGhhbmRsZUNsaWNrLFxuICAgIGljb24sXG4gICAgaWNvbkNvbG9yLFxuICAgIGljb25JbWcsXG4gICAgaXNJY29uTGV0dGVyLFxuICAgIG5vQmFja2dyb3VuZCxcbiAgICBub0JvcmRlcixcbiAgICBzaXplLFxuICAgIHZpc2libGUgPSB0cnVlLFxufSkgPT4ge1xuICAgIGlmICghdmlzaWJsZSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmV0dXJuIChcbiAgICAgICAgPFdyYXBwZXIgY2xhc3NOYW1lPXtkaXNhYmxlZCAmJiAnZGlzYWJsZWQnfT5cbiAgICAgICAgICAgIDxTdHlsZWRSb3VuZGVkSWNvblxuICAgICAgICAgICAgICAgIGJnQ29sb3I9e2JnQ29sb3J9XG4gICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I9e2JvcmRlckNvbG9yfVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NOYW1lfVxuICAgICAgICAgICAgICAgIGNsaWNrYWJsZT17Y2xpY2thYmxlfVxuICAgICAgICAgICAgICAgIGN1c3RvbVNpemU9e2N1c3RvbVNpemV9XG4gICAgICAgICAgICAgICAgbm9CYWNrZ3JvdW5kPXtub0JhY2tncm91bmR9XG4gICAgICAgICAgICAgICAgbm9Cb3JkZXI9e25vQm9yZGVyfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZUNsaWNrfVxuICAgICAgICAgICAgICAgIHNpemU9e3NpemV9XG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxTdHlsZWRJY29uXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17aXNJY29uTGV0dGVyIHx8IGljb259XG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbUljb25TaXplPXtjdXN0b21JY29uU2l6ZX1cbiAgICAgICAgICAgICAgICAgICAgaWNvbkNvbG9yPXtpY29uQ29sb3J9XG4gICAgICAgICAgICAgICAgICAgIHNpemU9e3NpemV9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICB7aWNvbkltZyA/IChcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgYWx0PVwiXCIgc3JjPXtpY29uSW1nfSB3aWR0aD1cIjE0XCIgLz5cbiAgICAgICAgICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzSWNvbkxldHRlciAmJiBpY29uXG4gICAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC9TdHlsZWRJY29uPlxuICAgICAgICAgICAgPC9TdHlsZWRSb3VuZGVkSWNvbj5cbiAgICAgICAgPC9XcmFwcGVyPlxuICAgICk7XG59O1xuXG5Sb3VuZGVkSWNvbi5wcm9wVHlwZXMgPSB7XG4gICAgYmdDb2xvcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBib3JkZXJDb2xvcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjbGFzc05hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgY2xpY2thYmxlOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBjdXN0b21JY29uU2l6ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjdXN0b21TaXplOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbiAgICBoYW5kbGVDbGljazogUHJvcFR5cGVzLmZ1bmMsXG4gICAgaWNvbjogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLmVsZW1lbnRdKSxcbiAgICBpY29uQ29sb3I6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5ib29sLCBQcm9wVHlwZXMuc3RyaW5nXSksXG4gICAgaWNvbkltZzogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBpc0ljb25MZXR0ZXI6IFByb3BUeXBlcy5ib29sLFxuICAgIG5vQmFja2dyb3VuZDogUHJvcFR5cGVzLmJvb2wsXG4gICAgbm9Cb3JkZXI6IFByb3BUeXBlcy5ib29sLFxuICAgIHNpemU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgdmlzaWJsZTogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBSb3VuZGVkSWNvbjtcbiJdfQ==