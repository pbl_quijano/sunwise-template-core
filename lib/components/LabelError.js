"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _templateObject;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var _default = _styledComponents.default.div(_templateObject || (_templateObject = _taggedTemplateLiteral(["\n    color: ", ";\n    font-size: ", ";\n    padding-top: 4px;\n    padding-left: 3px;\n    line-height: 11px;\n    ", ";\n"])), function (_ref) {
  var color = _ref.color;
  return color ? color : '#fff';
}, function (_ref2) {
  var fontSize = _ref2.fontSize;
  return fontSize ? fontSize : '11px';
}, function (_ref3) {
  var type = _ref3.type;
  return type === 'error' && "color: red;";
});

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb21wb25lbnRzL0xhYmVsRXJyb3IuanMiXSwibmFtZXMiOlsic3R5bGVkIiwiZGl2IiwiY29sb3IiLCJmb250U2l6ZSIsInR5cGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7Ozs7ZUFFZUEsMEJBQU9DLEcsa01BQ1Q7QUFBQSxNQUFHQyxLQUFILFFBQUdBLEtBQUg7QUFBQSxTQUFnQkEsS0FBSyxHQUFHQSxLQUFILEdBQVcsTUFBaEM7QUFBQSxDLEVBQ0k7QUFBQSxNQUFHQyxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQkEsUUFBUSxHQUFHQSxRQUFILEdBQWMsTUFBekM7QUFBQSxDLEVBSVg7QUFBQSxNQUFHQyxJQUFILFNBQUdBLElBQUg7QUFBQSxTQUFjQSxJQUFJLEtBQUssT0FBVCxpQkFBZDtBQUFBLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJztcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGVkLmRpdmBcbiAgICBjb2xvcjogJHsoeyBjb2xvciB9KSA9PiAoY29sb3IgPyBjb2xvciA6ICcjZmZmJyl9O1xuICAgIGZvbnQtc2l6ZTogJHsoeyBmb250U2l6ZSB9KSA9PiAoZm9udFNpemUgPyBmb250U2l6ZSA6ICcxMXB4Jyl9O1xuICAgIHBhZGRpbmctdG9wOiA0cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAzcHg7XG4gICAgbGluZS1oZWlnaHQ6IDExcHg7XG4gICAgJHsoeyB0eXBlIH0pID0+IHR5cGUgPT09ICdlcnJvcicgJiYgYGNvbG9yOiByZWQ7YH07XG5gO1xuIl19