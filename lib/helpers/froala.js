"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getToolbarButtons = exports.getParentNode = exports.getHtmlAllowedAttrs = exports.getFroalaEditorConfig = exports.getFontFamily = void 0;

var _i18next = _interopRequireDefault(require("i18next"));

var _froala = require("../constants/froala");

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getFontFamily = function getFontFamily() {
  return _froala.FONT_LIST.reduce(function (acc, current) {
    acc["'".concat(current, "'")] = current;
    return acc;
  }, {});
};

exports.getFontFamily = getFontFamily;

var getFroalaEditorConfig = function getFroalaEditorConfig(_ref) {
  var FroalaEditor = _ref.FroalaEditor,
      froalaLicenseKey = _ref.froalaLicenseKey,
      isFullEdition = _ref.isFullEdition,
      language = _ref.language,
      prepareEditCustomTag = _ref.prepareEditCustomTag,
      prepareEditLibraryTag = _ref.prepareEditLibraryTag,
      setCurrentFroalaEditorInstance = _ref.setCurrentFroalaEditorInstance,
      setIsToolbarDisabled = _ref.setIsToolbarDisabled;
  return _objectSpread(_objectSpread({}, _froala.FROALA_DEFAULT_CONFIG), {}, {
    key: froalaLicenseKey,
    events: {
      focus: function focus() {
        return setIsToolbarDisabled(false);
      },
      blur: function blur() {
        return setIsToolbarDisabled(true);
      },
      click: function click(e) {
        // INITIALIZE FROALA CONFIGS
        var froalaEditorInstance = this;
        var tagElement = froalaEditorInstance.$box.find(".fr-command[selected=\"true\"]");

        if (tagElement) {
          tagElement.attr('selected', false);
        }

        setCurrentFroalaEditorInstance(froalaEditorInstance);
        var element = null;

        if (e.currentTarget.className === 'fr-command sunwise-tag') {
          element = e.currentTarget;
        } else if (getParentNode(e.currentTarget, 10)) {
          element = getParentNode(e.currentTarget, 10);
        }

        if (element && element.dataset && element.dataset.settings) {
          element.setAttribute('selected', true);
          var data = element.dataset.settings;

          if ((0, _utils.isJsonString)(data)) {
            var json = JSON.parse(data);

            if (json.is_new_tag) {
              prepareEditCustomTag(json);
            } else {
              prepareEditLibraryTag(json);
            }
          }
        }
      }
    },
    fontFamily: getFontFamily(),
    htmlAllowedAttrs: getHtmlAllowedAttrs(FroalaEditor.DEFAULTS),
    language: _froala.FROALA_LANGUAGE_CODE_EQUIVALENCE[language.split('-')[0]],
    placeholderText: _i18next.default.t(_froala.FROALA_DEFAULT_CONFIG.placeholderText),
    toolbarButtons: getToolbarButtons(_froala.FROALA_TOOLBAR_BUTTONS, isFullEdition),
    toolbarButtonsMD: getToolbarButtons(_froala.FROALA_TOOLBAR_BUTTONS_MD, isFullEdition),
    toolbarButtonsSM: getToolbarButtons(_froala.FROALA_TOOLBAR_BUTTONS_SM, isFullEdition),
    toolbarButtonsXS: getToolbarButtons(_froala.FROALA_TOOLBAR_BUTTONS_XS, isFullEdition)
  });
};

exports.getFroalaEditorConfig = getFroalaEditorConfig;

var getHtmlAllowedAttrs = function getHtmlAllowedAttrs(defaults) {
  return defaults.htmlAllowedAttrs.filter(function (attr) {
    return !_froala.FROALA_HTML_DENIED_ATTRS.includes(attr);
  });
};

exports.getHtmlAllowedAttrs = getHtmlAllowedAttrs;

var getParentNode = function getParentNode(element, num) {
  var parent = element;

  for (var i = 0; i < num; i++) {
    if (parent.parentElement) {
      parent = parent.parentElement;

      if (parent.className === 'fr-command sunwise-tag') {
        break;
      }
    }
  }

  return parent;
};

exports.getParentNode = getParentNode;

var getToolbarButtons = function getToolbarButtons(buttons, isFullEdition) {
  return _objectSpread(_objectSpread({}, buttons), {}, {
    moreRich: isFullEdition ? buttons.moreRich : {
      buttons: ['insertTable']
    }
  });
};

exports.getToolbarButtons = getToolbarButtons;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oZWxwZXJzL2Zyb2FsYS5qcyJdLCJuYW1lcyI6WyJnZXRGb250RmFtaWx5IiwiRk9OVF9MSVNUIiwicmVkdWNlIiwiYWNjIiwiY3VycmVudCIsImdldEZyb2FsYUVkaXRvckNvbmZpZyIsIkZyb2FsYUVkaXRvciIsImZyb2FsYUxpY2Vuc2VLZXkiLCJpc0Z1bGxFZGl0aW9uIiwibGFuZ3VhZ2UiLCJwcmVwYXJlRWRpdEN1c3RvbVRhZyIsInByZXBhcmVFZGl0TGlicmFyeVRhZyIsInNldEN1cnJlbnRGcm9hbGFFZGl0b3JJbnN0YW5jZSIsInNldElzVG9vbGJhckRpc2FibGVkIiwiRlJPQUxBX0RFRkFVTFRfQ09ORklHIiwia2V5IiwiZXZlbnRzIiwiZm9jdXMiLCJibHVyIiwiY2xpY2siLCJlIiwiZnJvYWxhRWRpdG9ySW5zdGFuY2UiLCJ0YWdFbGVtZW50IiwiJGJveCIsImZpbmQiLCJhdHRyIiwiZWxlbWVudCIsImN1cnJlbnRUYXJnZXQiLCJjbGFzc05hbWUiLCJnZXRQYXJlbnROb2RlIiwiZGF0YXNldCIsInNldHRpbmdzIiwic2V0QXR0cmlidXRlIiwiZGF0YSIsImpzb24iLCJKU09OIiwicGFyc2UiLCJpc19uZXdfdGFnIiwiZm9udEZhbWlseSIsImh0bWxBbGxvd2VkQXR0cnMiLCJnZXRIdG1sQWxsb3dlZEF0dHJzIiwiREVGQVVMVFMiLCJGUk9BTEFfTEFOR1VBR0VfQ09ERV9FUVVJVkFMRU5DRSIsInNwbGl0IiwicGxhY2Vob2xkZXJUZXh0IiwiaTE4bmV4dCIsInQiLCJ0b29sYmFyQnV0dG9ucyIsImdldFRvb2xiYXJCdXR0b25zIiwiRlJPQUxBX1RPT0xCQVJfQlVUVE9OUyIsInRvb2xiYXJCdXR0b25zTUQiLCJGUk9BTEFfVE9PTEJBUl9CVVRUT05TX01EIiwidG9vbGJhckJ1dHRvbnNTTSIsIkZST0FMQV9UT09MQkFSX0JVVFRPTlNfU00iLCJ0b29sYmFyQnV0dG9uc1hTIiwiRlJPQUxBX1RPT0xCQVJfQlVUVE9OU19YUyIsImRlZmF1bHRzIiwiZmlsdGVyIiwiRlJPQUxBX0hUTUxfREVOSUVEX0FUVFJTIiwiaW5jbHVkZXMiLCJudW0iLCJwYXJlbnQiLCJpIiwicGFyZW50RWxlbWVudCIsImJ1dHRvbnMiLCJtb3JlUmljaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUVBOztBQVdBOzs7Ozs7Ozs7O0FBRU8sSUFBTUEsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQjtBQUFBLFNBQ3pCQyxrQkFBVUMsTUFBVixDQUFpQixVQUFDQyxHQUFELEVBQU1DLE9BQU4sRUFBa0I7QUFDL0JELElBQUFBLEdBQUcsWUFBS0MsT0FBTCxPQUFILEdBQXNCQSxPQUF0QjtBQUNBLFdBQU9ELEdBQVA7QUFDSCxHQUhELEVBR0csRUFISCxDQUR5QjtBQUFBLENBQXRCOzs7O0FBTUEsSUFBTUUscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QjtBQUFBLE1BQ2pDQyxZQURpQyxRQUNqQ0EsWUFEaUM7QUFBQSxNQUVqQ0MsZ0JBRmlDLFFBRWpDQSxnQkFGaUM7QUFBQSxNQUdqQ0MsYUFIaUMsUUFHakNBLGFBSGlDO0FBQUEsTUFJakNDLFFBSmlDLFFBSWpDQSxRQUppQztBQUFBLE1BS2pDQyxvQkFMaUMsUUFLakNBLG9CQUxpQztBQUFBLE1BTWpDQyxxQkFOaUMsUUFNakNBLHFCQU5pQztBQUFBLE1BT2pDQyw4QkFQaUMsUUFPakNBLDhCQVBpQztBQUFBLE1BUWpDQyxvQkFSaUMsUUFRakNBLG9CQVJpQztBQUFBLHlDQVU5QkMsNkJBVjhCO0FBV2pDQyxJQUFBQSxHQUFHLEVBQUVSLGdCQVg0QjtBQVlqQ1MsSUFBQUEsTUFBTSxFQUFFO0FBQ0pDLE1BQUFBLEtBQUssRUFBRTtBQUFBLGVBQU1KLG9CQUFvQixDQUFDLEtBQUQsQ0FBMUI7QUFBQSxPQURIO0FBRUpLLE1BQUFBLElBQUksRUFBRTtBQUFBLGVBQU1MLG9CQUFvQixDQUFDLElBQUQsQ0FBMUI7QUFBQSxPQUZGO0FBR0pNLE1BQUFBLEtBQUssRUFBRSxlQUFVQyxDQUFWLEVBQWE7QUFDaEI7QUFDQSxZQUFNQyxvQkFBb0IsR0FBRyxJQUE3QjtBQUNBLFlBQUlDLFVBQVUsR0FBR0Qsb0JBQW9CLENBQUNFLElBQXJCLENBQTBCQyxJQUExQixrQ0FBakI7O0FBR0EsWUFBSUYsVUFBSixFQUFnQjtBQUNaQSxVQUFBQSxVQUFVLENBQUNHLElBQVgsQ0FBZ0IsVUFBaEIsRUFBNEIsS0FBNUI7QUFDSDs7QUFDRGIsUUFBQUEsOEJBQThCLENBQUNTLG9CQUFELENBQTlCO0FBQ0EsWUFBSUssT0FBTyxHQUFHLElBQWQ7O0FBQ0EsWUFBSU4sQ0FBQyxDQUFDTyxhQUFGLENBQWdCQyxTQUFoQixLQUE4Qix3QkFBbEMsRUFBNEQ7QUFDeERGLFVBQUFBLE9BQU8sR0FBR04sQ0FBQyxDQUFDTyxhQUFaO0FBQ0gsU0FGRCxNQUVPLElBQUlFLGFBQWEsQ0FBQ1QsQ0FBQyxDQUFDTyxhQUFILEVBQWtCLEVBQWxCLENBQWpCLEVBQXdDO0FBQzNDRCxVQUFBQSxPQUFPLEdBQUdHLGFBQWEsQ0FBQ1QsQ0FBQyxDQUFDTyxhQUFILEVBQWtCLEVBQWxCLENBQXZCO0FBQ0g7O0FBRUQsWUFBSUQsT0FBTyxJQUFJQSxPQUFPLENBQUNJLE9BQW5CLElBQThCSixPQUFPLENBQUNJLE9BQVIsQ0FBZ0JDLFFBQWxELEVBQTREO0FBQ3hETCxVQUFBQSxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsVUFBckIsRUFBaUMsSUFBakM7QUFDQSxjQUFNQyxJQUFJLEdBQUdQLE9BQU8sQ0FBQ0ksT0FBUixDQUFnQkMsUUFBN0I7O0FBQ0EsY0FBSSx5QkFBYUUsSUFBYixDQUFKLEVBQXdCO0FBQ3BCLGdCQUFNQyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxJQUFYLENBQWI7O0FBQ0EsZ0JBQUlDLElBQUksQ0FBQ0csVUFBVCxFQUFxQjtBQUNqQjNCLGNBQUFBLG9CQUFvQixDQUFDd0IsSUFBRCxDQUFwQjtBQUNILGFBRkQsTUFFTztBQUNIdkIsY0FBQUEscUJBQXFCLENBQUN1QixJQUFELENBQXJCO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7QUFoQ0csS0FaeUI7QUE4Q2pDSSxJQUFBQSxVQUFVLEVBQUV0QyxhQUFhLEVBOUNRO0FBK0NqQ3VDLElBQUFBLGdCQUFnQixFQUFFQyxtQkFBbUIsQ0FBQ2xDLFlBQVksQ0FBQ21DLFFBQWQsQ0EvQ0o7QUFnRGpDaEMsSUFBQUEsUUFBUSxFQUFFaUMseUNBQWlDakMsUUFBUSxDQUFDa0MsS0FBVCxDQUFlLEdBQWYsRUFBb0IsQ0FBcEIsQ0FBakMsQ0FoRHVCO0FBaURqQ0MsSUFBQUEsZUFBZSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVaEMsOEJBQXNCOEIsZUFBaEMsQ0FqRGdCO0FBa0RqQ0csSUFBQUEsY0FBYyxFQUFFQyxpQkFBaUIsQ0FBQ0MsOEJBQUQsRUFBeUJ6QyxhQUF6QixDQWxEQTtBQW1EakMwQyxJQUFBQSxnQkFBZ0IsRUFBRUYsaUJBQWlCLENBQy9CRyxpQ0FEK0IsRUFFL0IzQyxhQUYrQixDQW5ERjtBQXVEakM0QyxJQUFBQSxnQkFBZ0IsRUFBRUosaUJBQWlCLENBQy9CSyxpQ0FEK0IsRUFFL0I3QyxhQUYrQixDQXZERjtBQTJEakM4QyxJQUFBQSxnQkFBZ0IsRUFBRU4saUJBQWlCLENBQy9CTyxpQ0FEK0IsRUFFL0IvQyxhQUYrQjtBQTNERjtBQUFBLENBQTlCOzs7O0FBaUVBLElBQU1nQyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNnQixRQUFELEVBQWM7QUFDN0MsU0FBT0EsUUFBUSxDQUFDakIsZ0JBQVQsQ0FBMEJrQixNQUExQixDQUNILFVBQUNoQyxJQUFEO0FBQUEsV0FBVSxDQUFDaUMsaUNBQXlCQyxRQUF6QixDQUFrQ2xDLElBQWxDLENBQVg7QUFBQSxHQURHLENBQVA7QUFHSCxDQUpNOzs7O0FBTUEsSUFBTUksYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDSCxPQUFELEVBQVVrQyxHQUFWLEVBQWtCO0FBQzNDLE1BQUlDLE1BQU0sR0FBR25DLE9BQWI7O0FBQ0EsT0FBSyxJQUFJb0MsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsR0FBcEIsRUFBeUJFLENBQUMsRUFBMUIsRUFBOEI7QUFDMUIsUUFBSUQsTUFBTSxDQUFDRSxhQUFYLEVBQTBCO0FBQ3RCRixNQUFBQSxNQUFNLEdBQUdBLE1BQU0sQ0FBQ0UsYUFBaEI7O0FBQ0EsVUFBSUYsTUFBTSxDQUFDakMsU0FBUCxLQUFxQix3QkFBekIsRUFBbUQ7QUFDL0M7QUFDSDtBQUNKO0FBQ0o7O0FBQ0QsU0FBT2lDLE1BQVA7QUFDSCxDQVhNOzs7O0FBYUEsSUFBTWIsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDZ0IsT0FBRCxFQUFVeEQsYUFBVjtBQUFBLHlDQUMxQndELE9BRDBCO0FBRTdCQyxJQUFBQSxRQUFRLEVBQUV6RCxhQUFhLEdBQ2pCd0QsT0FBTyxDQUFDQyxRQURTLEdBRWpCO0FBQ0lELE1BQUFBLE9BQU8sRUFBRSxDQUFDLGFBQUQ7QUFEYjtBQUp1QjtBQUFBLENBQTFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGkxOG5leHQgZnJvbSAnaTE4bmV4dCc7XG5cbmltcG9ydCB7XG4gICAgRlJPQUxBX0RFRkFVTFRfQ09ORklHLFxuICAgIEZST0FMQV9IVE1MX0RFTklFRF9BVFRSUyxcbiAgICBGUk9BTEFfTEFOR1VBR0VfQ09ERV9FUVVJVkFMRU5DRSxcbiAgICBGUk9BTEFfVE9PTEJBUl9CVVRUT05TLFxuICAgIEZST0FMQV9UT09MQkFSX0JVVFRPTlNfTUQsXG4gICAgRlJPQUxBX1RPT0xCQVJfQlVUVE9OU19TTSxcbiAgICBGUk9BTEFfVE9PTEJBUl9CVVRUT05TX1hTLFxuICAgIEZPTlRfTElTVCxcbn0gZnJvbSAnQGNvbnN0YW50cy9mcm9hbGEnO1xuXG5pbXBvcnQgeyBpc0pzb25TdHJpbmcgfSBmcm9tICdAaGVscGVycy91dGlscyc7XG5cbmV4cG9ydCBjb25zdCBnZXRGb250RmFtaWx5ID0gKCkgPT5cbiAgICBGT05UX0xJU1QucmVkdWNlKChhY2MsIGN1cnJlbnQpID0+IHtcbiAgICAgICAgYWNjW2AnJHtjdXJyZW50fSdgXSA9IGN1cnJlbnQ7XG4gICAgICAgIHJldHVybiBhY2M7XG4gICAgfSwge30pO1xuXG5leHBvcnQgY29uc3QgZ2V0RnJvYWxhRWRpdG9yQ29uZmlnID0gKHtcbiAgICBGcm9hbGFFZGl0b3IsXG4gICAgZnJvYWxhTGljZW5zZUtleSxcbiAgICBpc0Z1bGxFZGl0aW9uLFxuICAgIGxhbmd1YWdlLFxuICAgIHByZXBhcmVFZGl0Q3VzdG9tVGFnLFxuICAgIHByZXBhcmVFZGl0TGlicmFyeVRhZyxcbiAgICBzZXRDdXJyZW50RnJvYWxhRWRpdG9ySW5zdGFuY2UsXG4gICAgc2V0SXNUb29sYmFyRGlzYWJsZWQsXG59KSA9PiAoe1xuICAgIC4uLkZST0FMQV9ERUZBVUxUX0NPTkZJRyxcbiAgICBrZXk6IGZyb2FsYUxpY2Vuc2VLZXksXG4gICAgZXZlbnRzOiB7XG4gICAgICAgIGZvY3VzOiAoKSA9PiBzZXRJc1Rvb2xiYXJEaXNhYmxlZChmYWxzZSksXG4gICAgICAgIGJsdXI6ICgpID0+IHNldElzVG9vbGJhckRpc2FibGVkKHRydWUpLFxuICAgICAgICBjbGljazogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIC8vIElOSVRJQUxJWkUgRlJPQUxBIENPTkZJR1NcbiAgICAgICAgICAgIGNvbnN0IGZyb2FsYUVkaXRvckluc3RhbmNlID0gdGhpcztcbiAgICAgICAgICAgIGxldCB0YWdFbGVtZW50ID0gZnJvYWxhRWRpdG9ySW5zdGFuY2UuJGJveC5maW5kKFxuICAgICAgICAgICAgICAgIGAuZnItY29tbWFuZFtzZWxlY3RlZD1cInRydWVcIl1gXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKHRhZ0VsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICB0YWdFbGVtZW50LmF0dHIoJ3NlbGVjdGVkJywgZmFsc2UpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2V0Q3VycmVudEZyb2FsYUVkaXRvckluc3RhbmNlKGZyb2FsYUVkaXRvckluc3RhbmNlKTtcbiAgICAgICAgICAgIGxldCBlbGVtZW50ID0gbnVsbDtcbiAgICAgICAgICAgIGlmIChlLmN1cnJlbnRUYXJnZXQuY2xhc3NOYW1lID09PSAnZnItY29tbWFuZCBzdW53aXNlLXRhZycpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50ID0gZS5jdXJyZW50VGFyZ2V0O1xuICAgICAgICAgICAgfSBlbHNlIGlmIChnZXRQYXJlbnROb2RlKGUuY3VycmVudFRhcmdldCwgMTApKSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudCA9IGdldFBhcmVudE5vZGUoZS5jdXJyZW50VGFyZ2V0LCAxMCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChlbGVtZW50ICYmIGVsZW1lbnQuZGF0YXNldCAmJiBlbGVtZW50LmRhdGFzZXQuc2V0dGluZ3MpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnc2VsZWN0ZWQnLCB0cnVlKTtcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRhID0gZWxlbWVudC5kYXRhc2V0LnNldHRpbmdzO1xuICAgICAgICAgICAgICAgIGlmIChpc0pzb25TdHJpbmcoZGF0YSkpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QganNvbiA9IEpTT04ucGFyc2UoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChqc29uLmlzX25ld190YWcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByZXBhcmVFZGl0Q3VzdG9tVGFnKGpzb24pO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJlcGFyZUVkaXRMaWJyYXJ5VGFnKGpzb24pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgIH0sXG4gICAgZm9udEZhbWlseTogZ2V0Rm9udEZhbWlseSgpLFxuICAgIGh0bWxBbGxvd2VkQXR0cnM6IGdldEh0bWxBbGxvd2VkQXR0cnMoRnJvYWxhRWRpdG9yLkRFRkFVTFRTKSxcbiAgICBsYW5ndWFnZTogRlJPQUxBX0xBTkdVQUdFX0NPREVfRVFVSVZBTEVOQ0VbbGFuZ3VhZ2Uuc3BsaXQoJy0nKVswXV0sXG4gICAgcGxhY2Vob2xkZXJUZXh0OiBpMThuZXh0LnQoRlJPQUxBX0RFRkFVTFRfQ09ORklHLnBsYWNlaG9sZGVyVGV4dCksXG4gICAgdG9vbGJhckJ1dHRvbnM6IGdldFRvb2xiYXJCdXR0b25zKEZST0FMQV9UT09MQkFSX0JVVFRPTlMsIGlzRnVsbEVkaXRpb24pLFxuICAgIHRvb2xiYXJCdXR0b25zTUQ6IGdldFRvb2xiYXJCdXR0b25zKFxuICAgICAgICBGUk9BTEFfVE9PTEJBUl9CVVRUT05TX01ELFxuICAgICAgICBpc0Z1bGxFZGl0aW9uXG4gICAgKSxcbiAgICB0b29sYmFyQnV0dG9uc1NNOiBnZXRUb29sYmFyQnV0dG9ucyhcbiAgICAgICAgRlJPQUxBX1RPT0xCQVJfQlVUVE9OU19TTSxcbiAgICAgICAgaXNGdWxsRWRpdGlvblxuICAgICksXG4gICAgdG9vbGJhckJ1dHRvbnNYUzogZ2V0VG9vbGJhckJ1dHRvbnMoXG4gICAgICAgIEZST0FMQV9UT09MQkFSX0JVVFRPTlNfWFMsXG4gICAgICAgIGlzRnVsbEVkaXRpb25cbiAgICApLFxufSk7XG5cbmV4cG9ydCBjb25zdCBnZXRIdG1sQWxsb3dlZEF0dHJzID0gKGRlZmF1bHRzKSA9PiB7XG4gICAgcmV0dXJuIGRlZmF1bHRzLmh0bWxBbGxvd2VkQXR0cnMuZmlsdGVyKFxuICAgICAgICAoYXR0cikgPT4gIUZST0FMQV9IVE1MX0RFTklFRF9BVFRSUy5pbmNsdWRlcyhhdHRyKVxuICAgICk7XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0UGFyZW50Tm9kZSA9IChlbGVtZW50LCBudW0pID0+IHtcbiAgICBsZXQgcGFyZW50ID0gZWxlbWVudDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG51bTsgaSsrKSB7XG4gICAgICAgIGlmIChwYXJlbnQucGFyZW50RWxlbWVudCkge1xuICAgICAgICAgICAgcGFyZW50ID0gcGFyZW50LnBhcmVudEVsZW1lbnQ7XG4gICAgICAgICAgICBpZiAocGFyZW50LmNsYXNzTmFtZSA9PT0gJ2ZyLWNvbW1hbmQgc3Vud2lzZS10YWcnKSB7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHBhcmVudDtcbn07XG5cbmV4cG9ydCBjb25zdCBnZXRUb29sYmFyQnV0dG9ucyA9IChidXR0b25zLCBpc0Z1bGxFZGl0aW9uKSA9PiAoe1xuICAgIC4uLmJ1dHRvbnMsXG4gICAgbW9yZVJpY2g6IGlzRnVsbEVkaXRpb25cbiAgICAgICAgPyBidXR0b25zLm1vcmVSaWNoXG4gICAgICAgIDoge1xuICAgICAgICAgICAgICBidXR0b25zOiBbJ2luc2VydFRhYmxlJ10sXG4gICAgICAgICAgfSxcbn0pO1xuIl19