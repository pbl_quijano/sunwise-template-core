"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _i18next = _interopRequireDefault(require("i18next"));

var _reactToastify = require("react-toastify");

var _ToastNotification = _interopRequireDefault(require("../components/ToastNotification"));

var _jsxRuntime = require("react/jsx-runtime");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(props) {
  var defaults = {
    autoClose: 1500,
    closeButton: true,
    closeOnClick: true,
    draggable: true,
    body: 'Successfully saved',
    hideProgressBar: true,
    newestOnTop: true,
    title: '',
    type: 'success'
  };

  var values = _objectSpread(_objectSpread({}, defaults), props);

  var autoClose = values.autoClose,
      body = values.body,
      closeButton = values.closeButton,
      closeOnClick = values.closeOnClick,
      draggable = values.draggable,
      hideProgressBar = values.hideProgressBar,
      newestOnTop = values.newestOnTop,
      title = values.title,
      type = values.type;
  var translatedBody = _i18next.default.exists(body) ? _i18next.default.t(body) : body;
  return (0, _reactToastify.toast)( /*#__PURE__*/(0, _jsxRuntime.jsx)(_ToastNotification.default, {
    body: translatedBody,
    title: title,
    type: type
  }), {
    autoClose: autoClose,
    closeButton: closeButton,
    closeOnClick: closeOnClick,
    draggable: draggable,
    hideProgressBar: hideProgressBar,
    newestOnTop: newestOnTop
  });
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oZWxwZXJzL3Nob3dUb2FzdC5qcyJdLCJuYW1lcyI6WyJwcm9wcyIsImRlZmF1bHRzIiwiYXV0b0Nsb3NlIiwiY2xvc2VCdXR0b24iLCJjbG9zZU9uQ2xpY2siLCJkcmFnZ2FibGUiLCJib2R5IiwiaGlkZVByb2dyZXNzQmFyIiwibmV3ZXN0T25Ub3AiLCJ0aXRsZSIsInR5cGUiLCJ2YWx1ZXMiLCJ0cmFuc2xhdGVkQm9keSIsImkxOG5leHQiLCJleGlzdHMiLCJ0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxLQUFELEVBQVc7QUFDdEIsTUFBTUMsUUFBUSxHQUFHO0FBQ2JDLElBQUFBLFNBQVMsRUFBRSxJQURFO0FBRWJDLElBQUFBLFdBQVcsRUFBRSxJQUZBO0FBR2JDLElBQUFBLFlBQVksRUFBRSxJQUhEO0FBSWJDLElBQUFBLFNBQVMsRUFBRSxJQUpFO0FBS2JDLElBQUFBLElBQUksRUFBRSxvQkFMTztBQU1iQyxJQUFBQSxlQUFlLEVBQUUsSUFOSjtBQU9iQyxJQUFBQSxXQUFXLEVBQUUsSUFQQTtBQVFiQyxJQUFBQSxLQUFLLEVBQUUsRUFSTTtBQVNiQyxJQUFBQSxJQUFJLEVBQUU7QUFUTyxHQUFqQjs7QUFZQSxNQUFNQyxNQUFNLG1DQUFRVixRQUFSLEdBQXFCRCxLQUFyQixDQUFaOztBQUVBLE1BQ0lFLFNBREosR0FVSVMsTUFWSixDQUNJVCxTQURKO0FBQUEsTUFFSUksSUFGSixHQVVJSyxNQVZKLENBRUlMLElBRko7QUFBQSxNQUdJSCxXQUhKLEdBVUlRLE1BVkosQ0FHSVIsV0FISjtBQUFBLE1BSUlDLFlBSkosR0FVSU8sTUFWSixDQUlJUCxZQUpKO0FBQUEsTUFLSUMsU0FMSixHQVVJTSxNQVZKLENBS0lOLFNBTEo7QUFBQSxNQU1JRSxlQU5KLEdBVUlJLE1BVkosQ0FNSUosZUFOSjtBQUFBLE1BT0lDLFdBUEosR0FVSUcsTUFWSixDQU9JSCxXQVBKO0FBQUEsTUFRSUMsS0FSSixHQVVJRSxNQVZKLENBUUlGLEtBUko7QUFBQSxNQVNJQyxJQVRKLEdBVUlDLE1BVkosQ0FTSUQsSUFUSjtBQVlBLE1BQU1FLGNBQWMsR0FBR0MsaUJBQVFDLE1BQVIsQ0FBZVIsSUFBZixJQUF1Qk8saUJBQVFFLENBQVIsQ0FBVVQsSUFBVixDQUF2QixHQUF5Q0EsSUFBaEU7QUFFQSxTQUFPLHdDQUNILHFCQUFDLDBCQUFEO0FBQW1CLElBQUEsSUFBSSxFQUFFTSxjQUF6QjtBQUF5QyxJQUFBLEtBQUssRUFBRUgsS0FBaEQ7QUFBdUQsSUFBQSxJQUFJLEVBQUVDO0FBQTdELElBREcsRUFFSDtBQUNJUixJQUFBQSxTQUFTLEVBQUVBLFNBRGY7QUFFSUMsSUFBQUEsV0FBVyxFQUFYQSxXQUZKO0FBR0lDLElBQUFBLFlBQVksRUFBWkEsWUFISjtBQUlJQyxJQUFBQSxTQUFTLEVBQVRBLFNBSko7QUFLSUUsSUFBQUEsZUFBZSxFQUFFQSxlQUxyQjtBQU1JQyxJQUFBQSxXQUFXLEVBQVhBO0FBTkosR0FGRyxDQUFQO0FBV0gsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBpMThuZXh0IGZyb20gJ2kxOG5leHQnO1xuaW1wb3J0IHsgdG9hc3QgfSBmcm9tICdyZWFjdC10b2FzdGlmeSc7XG5cbmltcG9ydCBUb2FzdE5vdGlmaWNhdGlvbiBmcm9tICdAY29tcG9uZW50cy9Ub2FzdE5vdGlmaWNhdGlvbic7XG5cbmV4cG9ydCBkZWZhdWx0IChwcm9wcykgPT4ge1xuICAgIGNvbnN0IGRlZmF1bHRzID0ge1xuICAgICAgICBhdXRvQ2xvc2U6IDE1MDAsXG4gICAgICAgIGNsb3NlQnV0dG9uOiB0cnVlLFxuICAgICAgICBjbG9zZU9uQ2xpY2s6IHRydWUsXG4gICAgICAgIGRyYWdnYWJsZTogdHJ1ZSxcbiAgICAgICAgYm9keTogJ1N1Y2Nlc3NmdWxseSBzYXZlZCcsXG4gICAgICAgIGhpZGVQcm9ncmVzc0JhcjogdHJ1ZSxcbiAgICAgICAgbmV3ZXN0T25Ub3A6IHRydWUsXG4gICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgdHlwZTogJ3N1Y2Nlc3MnLFxuICAgIH07XG5cbiAgICBjb25zdCB2YWx1ZXMgPSB7IC4uLmRlZmF1bHRzLCAuLi5wcm9wcyB9O1xuXG4gICAgY29uc3Qge1xuICAgICAgICBhdXRvQ2xvc2UsXG4gICAgICAgIGJvZHksXG4gICAgICAgIGNsb3NlQnV0dG9uLFxuICAgICAgICBjbG9zZU9uQ2xpY2ssXG4gICAgICAgIGRyYWdnYWJsZSxcbiAgICAgICAgaGlkZVByb2dyZXNzQmFyLFxuICAgICAgICBuZXdlc3RPblRvcCxcbiAgICAgICAgdGl0bGUsXG4gICAgICAgIHR5cGUsXG4gICAgfSA9IHZhbHVlcztcblxuICAgIGNvbnN0IHRyYW5zbGF0ZWRCb2R5ID0gaTE4bmV4dC5leGlzdHMoYm9keSkgPyBpMThuZXh0LnQoYm9keSkgOiBib2R5O1xuXG4gICAgcmV0dXJuIHRvYXN0KFxuICAgICAgICA8VG9hc3ROb3RpZmljYXRpb24gYm9keT17dHJhbnNsYXRlZEJvZHl9IHRpdGxlPXt0aXRsZX0gdHlwZT17dHlwZX0gLz4sXG4gICAgICAgIHtcbiAgICAgICAgICAgIGF1dG9DbG9zZTogYXV0b0Nsb3NlLFxuICAgICAgICAgICAgY2xvc2VCdXR0b24sXG4gICAgICAgICAgICBjbG9zZU9uQ2xpY2ssXG4gICAgICAgICAgICBkcmFnZ2FibGUsXG4gICAgICAgICAgICBoaWRlUHJvZ3Jlc3NCYXI6IGhpZGVQcm9ncmVzc0JhcixcbiAgICAgICAgICAgIG5ld2VzdE9uVG9wLFxuICAgICAgICB9XG4gICAgKTtcbn07XG4iXX0=