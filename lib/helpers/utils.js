"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toFixed = exports.numberFormat = exports.isJsonString = exports.hasValue = exports.handleFileURL = exports.getFileNameByPath = exports.getCurrencyLocale = exports.getCurrencyIso = exports.formatNumber = exports.cloneElement = exports.arraytoDictionary = void 0;

var _get = _interopRequireDefault(require("lodash/get"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _isNull = _interopRequireDefault(require("lodash/isNull"));

var _numeral = _interopRequireDefault(require("numeral"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var arraytoDictionary = function arraytoDictionary(array, key) {
  if (!Array.isArray(array)) return {};
  return array.reduce(function (acc, current) {
    return _objectSpread(_objectSpread({}, acc), {}, _defineProperty({}, current[key], current));
  }, {});
};

exports.arraytoDictionary = arraytoDictionary;

var clearExponential = function clearExponential(value) {
  if (value) {
    var parts = value.toString().split('e');

    if (parts[0]) {
      return parts[0];
    }
  }

  return value;
};

var cloneElement = function cloneElement(value) {
  var ret = value instanceof Array ? [] : {};

  for (var key in value) {
    if (!Object.prototype.hasOwnProperty.call(value, key)) {
      continue;
    }

    var val = value[key];

    if (val && _typeof(val) == 'object') {
      val = cloneElement(val);
    }

    ret[key] = val;
  }

  return ret;
};

exports.cloneElement = cloneElement;

var formatNumber = function formatNumber(_ref) {
  var input = _ref.input,
      _ref$unit = _ref.unit,
      unit = _ref$unit === void 0 ? '' : _ref$unit,
      _ref$format = _ref.format,
      format = _ref$format === void 0 ? '0,0' : _ref$format,
      _ref$suffix = _ref.suffix,
      suffix = _ref$suffix === void 0 ? '' : _ref$suffix;
  return !input ? suffix + '0' : "".concat(suffix).concat((0, _numeral.default)(input).format(format, function (n) {
    return n;
  })).concat(unit);
};

exports.formatNumber = formatNumber;

var getCurrencyIso = function getCurrencyIso(currency) {
  if (!(0, _isEmpty.default)(currency) && !(0, _isNull.default)(currency) && hasValue(currency, 'abbreviation')) {
    return (0, _get.default)(currency, 'abbreviation', 'USD');
  }

  return 'USD';
};

exports.getCurrencyIso = getCurrencyIso;

var getCurrencyLocale = function getCurrencyLocale(currency) {
  if (!(0, _isEmpty.default)(currency) && !(0, _isNull.default)(currency) && hasValue(currency, 'locale')) {
    return (0, _get.default)(currency, 'locale', 'en-US').replace(/_/g, '-');
  }

  return 'en-US';
};

exports.getCurrencyLocale = getCurrencyLocale;

var getFileNameByPath = function getFileNameByPath(url) {
  return url.substring(url.lastIndexOf('/') + 1);
};

exports.getFileNameByPath = getFileNameByPath;

var handleFileURL = function handleFileURL(url, preffix) {
  var pattern = /^((https):\/\/)/;
  return pattern.test(url) ? url : "".concat(preffix).concat(url);
};

exports.handleFileURL = handleFileURL;

var hasValue = function hasValue(object, name) {
  return Object.prototype.hasOwnProperty.call(object, name) && !(0, _isEmpty.default)(object[name]);
};

exports.hasValue = hasValue;

var isJsonString = function isJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }

  return true;
};

exports.isJsonString = isJsonString;

var numberFormat = function numberFormat(value, options) {
  var newValue = clearExponential(value);
  var currency = options.currency,
      _options$decimals = options.decimals,
      decimals = _options$decimals === void 0 ? 2 : _options$decimals,
      _options$locale = options.locale,
      locale = _options$locale === void 0 ? 'en-US' : _options$locale,
      _options$unit = options.unit,
      unit = _options$unit === void 0 ? '' : _options$unit,
      style = options.style;
  var formatted = newValue;
  var unitText = '';

  switch (style) {
    case 'currency':
      formatted = new Intl.NumberFormat(locale, {
        style: style,
        currency: currency,
        minimumFractionDigits: decimals
      }).format(newValue);
      unitText = currency;
      break;

    case 'decimal':
      formatted = new Intl.NumberFormat(locale, {
        style: style,
        minimumFractionDigits: decimals
      }).format(newValue);
      unitText = unit;
      break;

    default:
      formatted = newValue;
      break;
  }

  return "".concat(formatted, " ").concat(!(0, _isEmpty.default)(unitText) ? unitText : '');
};

exports.numberFormat = numberFormat;

var toFixed = function toFixed(number, fixed) {
  var fixedValue = Math.pow(10, fixed);
  return parseInt(number * fixedValue) / fixedValue;
};

exports.toFixed = toFixed;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oZWxwZXJzL3V0aWxzLmpzIl0sIm5hbWVzIjpbImFycmF5dG9EaWN0aW9uYXJ5IiwiYXJyYXkiLCJrZXkiLCJBcnJheSIsImlzQXJyYXkiLCJyZWR1Y2UiLCJhY2MiLCJjdXJyZW50IiwiY2xlYXJFeHBvbmVudGlhbCIsInZhbHVlIiwicGFydHMiLCJ0b1N0cmluZyIsInNwbGl0IiwiY2xvbmVFbGVtZW50IiwicmV0IiwiT2JqZWN0IiwicHJvdG90eXBlIiwiaGFzT3duUHJvcGVydHkiLCJjYWxsIiwidmFsIiwiZm9ybWF0TnVtYmVyIiwiaW5wdXQiLCJ1bml0IiwiZm9ybWF0Iiwic3VmZml4IiwibiIsImdldEN1cnJlbmN5SXNvIiwiY3VycmVuY3kiLCJoYXNWYWx1ZSIsImdldEN1cnJlbmN5TG9jYWxlIiwicmVwbGFjZSIsImdldEZpbGVOYW1lQnlQYXRoIiwidXJsIiwic3Vic3RyaW5nIiwibGFzdEluZGV4T2YiLCJoYW5kbGVGaWxlVVJMIiwicHJlZmZpeCIsInBhdHRlcm4iLCJ0ZXN0Iiwib2JqZWN0IiwibmFtZSIsImlzSnNvblN0cmluZyIsInN0ciIsIkpTT04iLCJwYXJzZSIsImUiLCJudW1iZXJGb3JtYXQiLCJvcHRpb25zIiwibmV3VmFsdWUiLCJkZWNpbWFscyIsImxvY2FsZSIsInN0eWxlIiwiZm9ybWF0dGVkIiwidW5pdFRleHQiLCJJbnRsIiwiTnVtYmVyRm9ybWF0IiwibWluaW11bUZyYWN0aW9uRGlnaXRzIiwidG9GaXhlZCIsIm51bWJlciIsImZpeGVkIiwiZml4ZWRWYWx1ZSIsIk1hdGgiLCJwb3ciLCJwYXJzZUludCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOzs7Ozs7Ozs7Ozs7QUFFTyxJQUFNQSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLEtBQUQsRUFBUUMsR0FBUixFQUFnQjtBQUM3QyxNQUFJLENBQUNDLEtBQUssQ0FBQ0MsT0FBTixDQUFjSCxLQUFkLENBQUwsRUFBMkIsT0FBTyxFQUFQO0FBRTNCLFNBQU9BLEtBQUssQ0FBQ0ksTUFBTixDQUNILFVBQUNDLEdBQUQsRUFBTUMsT0FBTjtBQUFBLDJDQUF3QkQsR0FBeEIsMkJBQThCQyxPQUFPLENBQUNMLEdBQUQsQ0FBckMsRUFBNkNLLE9BQTdDO0FBQUEsR0FERyxFQUVILEVBRkcsQ0FBUDtBQUlILENBUE07Ozs7QUFTUCxJQUFNQyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQUNDLEtBQUQsRUFBVztBQUNoQyxNQUFJQSxLQUFKLEVBQVc7QUFDUCxRQUFNQyxLQUFLLEdBQUdELEtBQUssQ0FBQ0UsUUFBTixHQUFpQkMsS0FBakIsQ0FBdUIsR0FBdkIsQ0FBZDs7QUFDQSxRQUFJRixLQUFLLENBQUMsQ0FBRCxDQUFULEVBQWM7QUFDVixhQUFPQSxLQUFLLENBQUMsQ0FBRCxDQUFaO0FBQ0g7QUFDSjs7QUFDRCxTQUFPRCxLQUFQO0FBQ0gsQ0FSRDs7QUFVTyxJQUFNSSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDSixLQUFELEVBQVc7QUFDbkMsTUFBSUssR0FBRyxHQUFHTCxLQUFLLFlBQVlOLEtBQWpCLEdBQXlCLEVBQXpCLEdBQThCLEVBQXhDOztBQUVBLE9BQUssSUFBSUQsR0FBVCxJQUFnQk8sS0FBaEIsRUFBdUI7QUFDbkIsUUFBSSxDQUFDTSxNQUFNLENBQUNDLFNBQVAsQ0FBaUJDLGNBQWpCLENBQWdDQyxJQUFoQyxDQUFxQ1QsS0FBckMsRUFBNENQLEdBQTVDLENBQUwsRUFBdUQ7QUFDbkQ7QUFDSDs7QUFFRCxRQUFJaUIsR0FBRyxHQUFHVixLQUFLLENBQUNQLEdBQUQsQ0FBZjs7QUFFQSxRQUFJaUIsR0FBRyxJQUFJLFFBQU9BLEdBQVAsS0FBYyxRQUF6QixFQUFtQztBQUMvQkEsTUFBQUEsR0FBRyxHQUFHTixZQUFZLENBQUNNLEdBQUQsQ0FBbEI7QUFDSDs7QUFFREwsSUFBQUEsR0FBRyxDQUFDWixHQUFELENBQUgsR0FBV2lCLEdBQVg7QUFDSDs7QUFFRCxTQUFPTCxHQUFQO0FBQ0gsQ0FsQk07Ozs7QUFvQkEsSUFBTU0sWUFBWSxHQUFHLFNBQWZBLFlBQWU7QUFBQSxNQUN4QkMsS0FEd0IsUUFDeEJBLEtBRHdCO0FBQUEsdUJBRXhCQyxJQUZ3QjtBQUFBLE1BRXhCQSxJQUZ3QiwwQkFFakIsRUFGaUI7QUFBQSx5QkFHeEJDLE1BSHdCO0FBQUEsTUFHeEJBLE1BSHdCLDRCQUdmLEtBSGU7QUFBQSx5QkFJeEJDLE1BSndCO0FBQUEsTUFJeEJBLE1BSndCLDRCQUlmLEVBSmU7QUFBQSxTQU14QixDQUFDSCxLQUFELEdBQ01HLE1BQU0sR0FBRyxHQURmLGFBRVNBLE1BRlQsU0FFa0Isc0JBQVFILEtBQVIsRUFBZUUsTUFBZixDQUFzQkEsTUFBdEIsRUFBOEIsVUFBQ0UsQ0FBRDtBQUFBLFdBQU9BLENBQVA7QUFBQSxHQUE5QixDQUZsQixTQUU0REgsSUFGNUQsQ0FOd0I7QUFBQSxDQUFyQjs7OztBQVVBLElBQU1JLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ0MsUUFBRCxFQUFjO0FBQ3hDLE1BQ0ksQ0FBQyxzQkFBUUEsUUFBUixDQUFELElBQ0EsQ0FBQyxxQkFBT0EsUUFBUCxDQURELElBRUFDLFFBQVEsQ0FBQ0QsUUFBRCxFQUFXLGNBQVgsQ0FIWixFQUlFO0FBQ0UsV0FBTyxrQkFBSUEsUUFBSixFQUFjLGNBQWQsRUFBOEIsS0FBOUIsQ0FBUDtBQUNIOztBQUNELFNBQU8sS0FBUDtBQUNILENBVE07Ozs7QUFXQSxJQUFNRSxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNGLFFBQUQsRUFBYztBQUMzQyxNQUNJLENBQUMsc0JBQVFBLFFBQVIsQ0FBRCxJQUNBLENBQUMscUJBQU9BLFFBQVAsQ0FERCxJQUVBQyxRQUFRLENBQUNELFFBQUQsRUFBVyxRQUFYLENBSFosRUFJRTtBQUNFLFdBQU8sa0JBQUlBLFFBQUosRUFBYyxRQUFkLEVBQXdCLE9BQXhCLEVBQWlDRyxPQUFqQyxDQUF5QyxJQUF6QyxFQUErQyxHQUEvQyxDQUFQO0FBQ0g7O0FBQ0QsU0FBTyxPQUFQO0FBQ0gsQ0FUTTs7OztBQVdBLElBQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQ0MsR0FBRCxFQUFTO0FBQ3RDLFNBQU9BLEdBQUcsQ0FBQ0MsU0FBSixDQUFjRCxHQUFHLENBQUNFLFdBQUosQ0FBZ0IsR0FBaEIsSUFBdUIsQ0FBckMsQ0FBUDtBQUNILENBRk07Ozs7QUFJQSxJQUFNQyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNILEdBQUQsRUFBTUksT0FBTixFQUFrQjtBQUMzQyxNQUFNQyxPQUFPLEdBQUcsaUJBQWhCO0FBQ0EsU0FBT0EsT0FBTyxDQUFDQyxJQUFSLENBQWFOLEdBQWIsSUFBb0JBLEdBQXBCLGFBQTZCSSxPQUE3QixTQUF1Q0osR0FBdkMsQ0FBUDtBQUNILENBSE07Ozs7QUFLQSxJQUFNSixRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDVyxNQUFELEVBQVNDLElBQVQ7QUFBQSxTQUNwQnpCLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsY0FBakIsQ0FBZ0NDLElBQWhDLENBQXFDcUIsTUFBckMsRUFBNkNDLElBQTdDLEtBQ0EsQ0FBQyxzQkFBUUQsTUFBTSxDQUFDQyxJQUFELENBQWQsQ0FGbUI7QUFBQSxDQUFqQjs7OztBQUlBLElBQU1DLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLEdBQUQsRUFBUztBQUNqQyxNQUFJO0FBQ0FDLElBQUFBLElBQUksQ0FBQ0MsS0FBTCxDQUFXRixHQUFYO0FBQ0gsR0FGRCxDQUVFLE9BQU9HLENBQVAsRUFBVTtBQUNSLFdBQU8sS0FBUDtBQUNIOztBQUNELFNBQU8sSUFBUDtBQUNILENBUE07Ozs7QUFTQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDckMsS0FBRCxFQUFRc0MsT0FBUixFQUFvQjtBQUM1QyxNQUFNQyxRQUFRLEdBQUd4QyxnQkFBZ0IsQ0FBQ0MsS0FBRCxDQUFqQztBQUNBLE1BQ0lrQixRQURKLEdBTUlvQixPQU5KLENBQ0lwQixRQURKO0FBQUEsMEJBTUlvQixPQU5KLENBRUlFLFFBRko7QUFBQSxNQUVJQSxRQUZKLGtDQUVlLENBRmY7QUFBQSx3QkFNSUYsT0FOSixDQUdJRyxNQUhKO0FBQUEsTUFHSUEsTUFISixnQ0FHYSxPQUhiO0FBQUEsc0JBTUlILE9BTkosQ0FJSXpCLElBSko7QUFBQSxNQUlJQSxJQUpKLDhCQUlXLEVBSlg7QUFBQSxNQUtJNkIsS0FMSixHQU1JSixPQU5KLENBS0lJLEtBTEo7QUFPQSxNQUFJQyxTQUFTLEdBQUdKLFFBQWhCO0FBQ0EsTUFBSUssUUFBUSxHQUFHLEVBQWY7O0FBRUEsVUFBUUYsS0FBUjtBQUNJLFNBQUssVUFBTDtBQUNJQyxNQUFBQSxTQUFTLEdBQUcsSUFBSUUsSUFBSSxDQUFDQyxZQUFULENBQXNCTCxNQUF0QixFQUE4QjtBQUN0Q0MsUUFBQUEsS0FBSyxFQUFMQSxLQURzQztBQUV0Q3hCLFFBQUFBLFFBQVEsRUFBUkEsUUFGc0M7QUFHdEM2QixRQUFBQSxxQkFBcUIsRUFBRVA7QUFIZSxPQUE5QixFQUlUMUIsTUFKUyxDQUlGeUIsUUFKRSxDQUFaO0FBS0FLLE1BQUFBLFFBQVEsR0FBRzFCLFFBQVg7QUFDQTs7QUFDSixTQUFLLFNBQUw7QUFDSXlCLE1BQUFBLFNBQVMsR0FBRyxJQUFJRSxJQUFJLENBQUNDLFlBQVQsQ0FBc0JMLE1BQXRCLEVBQThCO0FBQ3RDQyxRQUFBQSxLQUFLLEVBQUxBLEtBRHNDO0FBRXRDSyxRQUFBQSxxQkFBcUIsRUFBRVA7QUFGZSxPQUE5QixFQUdUMUIsTUFIUyxDQUdGeUIsUUFIRSxDQUFaO0FBSUFLLE1BQUFBLFFBQVEsR0FBRy9CLElBQVg7QUFDQTs7QUFDSjtBQUNJOEIsTUFBQUEsU0FBUyxHQUFHSixRQUFaO0FBQ0E7QUFsQlI7O0FBcUJBLG1CQUFVSSxTQUFWLGNBQXVCLENBQUMsc0JBQVFDLFFBQVIsQ0FBRCxHQUFxQkEsUUFBckIsR0FBZ0MsRUFBdkQ7QUFDSCxDQWxDTTs7OztBQW9DQSxJQUFNSSxPQUFPLEdBQUcsU0FBVkEsT0FBVSxDQUFDQyxNQUFELEVBQVNDLEtBQVQsRUFBbUI7QUFDdEMsTUFBTUMsVUFBVSxHQUFHQyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxFQUFULEVBQWFILEtBQWIsQ0FBbkI7QUFDQSxTQUFPSSxRQUFRLENBQUNMLE1BQU0sR0FBR0UsVUFBVixDQUFSLEdBQWdDQSxVQUF2QztBQUNILENBSE0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC9nZXQnO1xyXG5pbXBvcnQgaXNFbXB0eSBmcm9tICdsb2Rhc2gvaXNFbXB0eSc7XHJcbmltcG9ydCBpc051bGwgZnJvbSAnbG9kYXNoL2lzTnVsbCc7XHJcbmltcG9ydCBudW1lcmFsIGZyb20gJ251bWVyYWwnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGFycmF5dG9EaWN0aW9uYXJ5ID0gKGFycmF5LCBrZXkpID0+IHtcclxuICAgIGlmICghQXJyYXkuaXNBcnJheShhcnJheSkpIHJldHVybiB7fTtcclxuXHJcbiAgICByZXR1cm4gYXJyYXkucmVkdWNlKFxyXG4gICAgICAgIChhY2MsIGN1cnJlbnQpID0+ICh7IC4uLmFjYywgW2N1cnJlbnRba2V5XV06IGN1cnJlbnQgfSksXHJcbiAgICAgICAge31cclxuICAgICk7XHJcbn07XHJcblxyXG5jb25zdCBjbGVhckV4cG9uZW50aWFsID0gKHZhbHVlKSA9PiB7XHJcbiAgICBpZiAodmFsdWUpIHtcclxuICAgICAgICBjb25zdCBwYXJ0cyA9IHZhbHVlLnRvU3RyaW5nKCkuc3BsaXQoJ2UnKTtcclxuICAgICAgICBpZiAocGFydHNbMF0pIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBhcnRzWzBdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB2YWx1ZTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBjbG9uZUVsZW1lbnQgPSAodmFsdWUpID0+IHtcclxuICAgIGxldCByZXQgPSB2YWx1ZSBpbnN0YW5jZW9mIEFycmF5ID8gW10gOiB7fTtcclxuXHJcbiAgICBmb3IgKGxldCBrZXkgaW4gdmFsdWUpIHtcclxuICAgICAgICBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbCh2YWx1ZSwga2V5KSkge1xyXG4gICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCB2YWwgPSB2YWx1ZVtrZXldO1xyXG5cclxuICAgICAgICBpZiAodmFsICYmIHR5cGVvZiB2YWwgPT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgdmFsID0gY2xvbmVFbGVtZW50KHZhbCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXRba2V5XSA9IHZhbDtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gcmV0O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGZvcm1hdE51bWJlciA9ICh7XHJcbiAgICBpbnB1dCxcclxuICAgIHVuaXQgPSAnJyxcclxuICAgIGZvcm1hdCA9ICcwLDAnLFxyXG4gICAgc3VmZml4ID0gJycsXHJcbn0pID0+XHJcbiAgICAhaW5wdXRcclxuICAgICAgICA/IHN1ZmZpeCArICcwJ1xyXG4gICAgICAgIDogYCR7c3VmZml4fSR7bnVtZXJhbChpbnB1dCkuZm9ybWF0KGZvcm1hdCwgKG4pID0+IG4pfSR7dW5pdH1gO1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbmN5SXNvID0gKGN1cnJlbmN5KSA9PiB7XHJcbiAgICBpZiAoXHJcbiAgICAgICAgIWlzRW1wdHkoY3VycmVuY3kpICYmXHJcbiAgICAgICAgIWlzTnVsbChjdXJyZW5jeSkgJiZcclxuICAgICAgICBoYXNWYWx1ZShjdXJyZW5jeSwgJ2FiYnJldmlhdGlvbicpXHJcbiAgICApIHtcclxuICAgICAgICByZXR1cm4gZ2V0KGN1cnJlbmN5LCAnYWJicmV2aWF0aW9uJywgJ1VTRCcpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuICdVU0QnO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbmN5TG9jYWxlID0gKGN1cnJlbmN5KSA9PiB7XHJcbiAgICBpZiAoXHJcbiAgICAgICAgIWlzRW1wdHkoY3VycmVuY3kpICYmXHJcbiAgICAgICAgIWlzTnVsbChjdXJyZW5jeSkgJiZcclxuICAgICAgICBoYXNWYWx1ZShjdXJyZW5jeSwgJ2xvY2FsZScpXHJcbiAgICApIHtcclxuICAgICAgICByZXR1cm4gZ2V0KGN1cnJlbmN5LCAnbG9jYWxlJywgJ2VuLVVTJykucmVwbGFjZSgvXy9nLCAnLScpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuICdlbi1VUyc7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZ2V0RmlsZU5hbWVCeVBhdGggPSAodXJsKSA9PiB7XHJcbiAgICByZXR1cm4gdXJsLnN1YnN0cmluZyh1cmwubGFzdEluZGV4T2YoJy8nKSArIDEpO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGhhbmRsZUZpbGVVUkwgPSAodXJsLCBwcmVmZml4KSA9PiB7XHJcbiAgICBjb25zdCBwYXR0ZXJuID0gL14oKGh0dHBzKTpcXC9cXC8pLztcclxuICAgIHJldHVybiBwYXR0ZXJuLnRlc3QodXJsKSA/IHVybCA6IGAke3ByZWZmaXh9JHt1cmx9YDtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBoYXNWYWx1ZSA9IChvYmplY3QsIG5hbWUpID0+XHJcbiAgICBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBuYW1lKSAmJlxyXG4gICAgIWlzRW1wdHkob2JqZWN0W25hbWVdKTtcclxuXHJcbmV4cG9ydCBjb25zdCBpc0pzb25TdHJpbmcgPSAoc3RyKSA9PiB7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIEpTT04ucGFyc2Uoc3RyKTtcclxuICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBudW1iZXJGb3JtYXQgPSAodmFsdWUsIG9wdGlvbnMpID0+IHtcclxuICAgIGNvbnN0IG5ld1ZhbHVlID0gY2xlYXJFeHBvbmVudGlhbCh2YWx1ZSk7XHJcbiAgICBjb25zdCB7XHJcbiAgICAgICAgY3VycmVuY3ksXHJcbiAgICAgICAgZGVjaW1hbHMgPSAyLFxyXG4gICAgICAgIGxvY2FsZSA9ICdlbi1VUycsXHJcbiAgICAgICAgdW5pdCA9ICcnLFxyXG4gICAgICAgIHN0eWxlLFxyXG4gICAgfSA9IG9wdGlvbnM7XHJcbiAgICBsZXQgZm9ybWF0dGVkID0gbmV3VmFsdWU7XHJcbiAgICBsZXQgdW5pdFRleHQgPSAnJztcclxuXHJcbiAgICBzd2l0Y2ggKHN0eWxlKSB7XHJcbiAgICAgICAgY2FzZSAnY3VycmVuY3knOlxyXG4gICAgICAgICAgICBmb3JtYXR0ZWQgPSBuZXcgSW50bC5OdW1iZXJGb3JtYXQobG9jYWxlLCB7XHJcbiAgICAgICAgICAgICAgICBzdHlsZSxcclxuICAgICAgICAgICAgICAgIGN1cnJlbmN5LFxyXG4gICAgICAgICAgICAgICAgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBkZWNpbWFscyxcclxuICAgICAgICAgICAgfSkuZm9ybWF0KG5ld1ZhbHVlKTtcclxuICAgICAgICAgICAgdW5pdFRleHQgPSBjdXJyZW5jeTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAnZGVjaW1hbCc6XHJcbiAgICAgICAgICAgIGZvcm1hdHRlZCA9IG5ldyBJbnRsLk51bWJlckZvcm1hdChsb2NhbGUsIHtcclxuICAgICAgICAgICAgICAgIHN0eWxlLFxyXG4gICAgICAgICAgICAgICAgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBkZWNpbWFscyxcclxuICAgICAgICAgICAgfSkuZm9ybWF0KG5ld1ZhbHVlKTtcclxuICAgICAgICAgICAgdW5pdFRleHQgPSB1bml0O1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICBmb3JtYXR0ZWQgPSBuZXdWYWx1ZTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGAke2Zvcm1hdHRlZH0gJHshaXNFbXB0eSh1bml0VGV4dCkgPyB1bml0VGV4dCA6ICcnfWA7XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgdG9GaXhlZCA9IChudW1iZXIsIGZpeGVkKSA9PiB7XHJcbiAgICBjb25zdCBmaXhlZFZhbHVlID0gTWF0aC5wb3coMTAsIGZpeGVkKTtcclxuICAgIHJldHVybiBwYXJzZUludChudW1iZXIgKiBmaXhlZFZhbHVlKSAvIGZpeGVkVmFsdWU7XHJcbn07XHJcbiJdfQ==