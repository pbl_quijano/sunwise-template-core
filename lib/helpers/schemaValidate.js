"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _set = _interopRequireDefault(require("lodash/set"));

var _getValidationTexts = _interopRequireDefault(require("./getValidationTexts"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = function _default(prepareSchema) {
  return function (values, props) {
    try {
      var validationTexts = (0, _getValidationTexts.default)();
      var schema = prepareSchema(validationTexts, props);
      schema.validateSync(values, {
        abortEarly: false
      });
      return {};
    } catch (errors) {
      return errors.inner.reduce(function (acc, current) {
        var tempObject = _objectSpread({}, acc);

        (0, _set.default)(tempObject, current.path, current.message);
        return tempObject;
      }, {});
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oZWxwZXJzL3NjaGVtYVZhbGlkYXRlLmpzIl0sIm5hbWVzIjpbInByZXBhcmVTY2hlbWEiLCJ2YWx1ZXMiLCJwcm9wcyIsInZhbGlkYXRpb25UZXh0cyIsInNjaGVtYSIsInZhbGlkYXRlU3luYyIsImFib3J0RWFybHkiLCJlcnJvcnMiLCJpbm5lciIsInJlZHVjZSIsImFjYyIsImN1cnJlbnQiLCJ0ZW1wT2JqZWN0IiwicGF0aCIsIm1lc3NhZ2UiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFFQTs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxhQUFEO0FBQUEsU0FBbUIsVUFBQ0MsTUFBRCxFQUFTQyxLQUFULEVBQW1CO0FBQ2pELFFBQUk7QUFDQSxVQUFNQyxlQUFlLEdBQUcsa0NBQXhCO0FBQ0EsVUFBTUMsTUFBTSxHQUFHSixhQUFhLENBQUNHLGVBQUQsRUFBa0JELEtBQWxCLENBQTVCO0FBQ0FFLE1BQUFBLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQkosTUFBcEIsRUFBNEI7QUFBRUssUUFBQUEsVUFBVSxFQUFFO0FBQWQsT0FBNUI7QUFDQSxhQUFPLEVBQVA7QUFDSCxLQUxELENBS0UsT0FBT0MsTUFBUCxFQUFlO0FBQ2IsYUFBT0EsTUFBTSxDQUFDQyxLQUFQLENBQWFDLE1BQWIsQ0FBb0IsVUFBQ0MsR0FBRCxFQUFNQyxPQUFOLEVBQWtCO0FBQ3pDLFlBQUlDLFVBQVUscUJBQVFGLEdBQVIsQ0FBZDs7QUFDQSwwQkFBSUUsVUFBSixFQUFnQkQsT0FBTyxDQUFDRSxJQUF4QixFQUE4QkYsT0FBTyxDQUFDRyxPQUF0QztBQUNBLGVBQU9GLFVBQVA7QUFDSCxPQUpNLEVBSUosRUFKSSxDQUFQO0FBS0g7QUFDSixHQWJjO0FBQUEsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBzZXQgZnJvbSAnbG9kYXNoL3NldCc7XG5cbmltcG9ydCBnZXRWYWxpZGF0aW9uVGV4dHMgZnJvbSAnLi9nZXRWYWxpZGF0aW9uVGV4dHMnO1xuXG5leHBvcnQgZGVmYXVsdCAocHJlcGFyZVNjaGVtYSkgPT4gKHZhbHVlcywgcHJvcHMpID0+IHtcbiAgICB0cnkge1xuICAgICAgICBjb25zdCB2YWxpZGF0aW9uVGV4dHMgPSBnZXRWYWxpZGF0aW9uVGV4dHMoKTtcbiAgICAgICAgY29uc3Qgc2NoZW1hID0gcHJlcGFyZVNjaGVtYSh2YWxpZGF0aW9uVGV4dHMsIHByb3BzKTtcbiAgICAgICAgc2NoZW1hLnZhbGlkYXRlU3luYyh2YWx1ZXMsIHsgYWJvcnRFYXJseTogZmFsc2UgfSk7XG4gICAgICAgIHJldHVybiB7fTtcbiAgICB9IGNhdGNoIChlcnJvcnMpIHtcbiAgICAgICAgcmV0dXJuIGVycm9ycy5pbm5lci5yZWR1Y2UoKGFjYywgY3VycmVudCkgPT4ge1xuICAgICAgICAgICAgbGV0IHRlbXBPYmplY3QgPSB7IC4uLmFjYyB9O1xuICAgICAgICAgICAgc2V0KHRlbXBPYmplY3QsIGN1cnJlbnQucGF0aCwgY3VycmVudC5tZXNzYWdlKTtcbiAgICAgICAgICAgIHJldHVybiB0ZW1wT2JqZWN0O1xuICAgICAgICB9LCB7fSk7XG4gICAgfVxufTtcbiJdfQ==