"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _i18next = _interopRequireDefault(require("i18next"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default() {
  return {
    CALC_REQUIRED_TEXT: _i18next.default.t('The calculator field is required'),
    FORMULA_REQUIRED_TEXT: _i18next.default.t('Add at least one element to the formula'),
    NUMBER_TYPE_TEXT: _i18next.default.t('Must be a number'),
    REQUIRED_TEXT: _i18next.default.t('This field is required'),
    getMinValueText: function getMinValueText(value) {
      return _i18next.default.t('It cannot be less than {{value}}', {
        value: value
      });
    }
  };
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9oZWxwZXJzL2dldFZhbGlkYXRpb25UZXh0cy5qcyJdLCJuYW1lcyI6WyJDQUxDX1JFUVVJUkVEX1RFWFQiLCJpMThuZXh0IiwidCIsIkZPUk1VTEFfUkVRVUlSRURfVEVYVCIsIk5VTUJFUl9UWVBFX1RFWFQiLCJSRVFVSVJFRF9URVhUIiwiZ2V0TWluVmFsdWVUZXh0IiwidmFsdWUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztlQUVlO0FBQUEsU0FBTztBQUNsQkEsSUFBQUEsa0JBQWtCLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsa0NBQVYsQ0FERjtBQUVsQkMsSUFBQUEscUJBQXFCLEVBQUVGLGlCQUFRQyxDQUFSLENBQVUseUNBQVYsQ0FGTDtBQUdsQkUsSUFBQUEsZ0JBQWdCLEVBQUVILGlCQUFRQyxDQUFSLENBQVUsa0JBQVYsQ0FIQTtBQUlsQkcsSUFBQUEsYUFBYSxFQUFFSixpQkFBUUMsQ0FBUixDQUFVLHdCQUFWLENBSkc7QUFLbEJJLElBQUFBLGVBQWUsRUFBRSx5QkFBQ0MsS0FBRDtBQUFBLGFBQ2JOLGlCQUFRQyxDQUFSLENBQVUsa0NBQVYsRUFBOEM7QUFBRUssUUFBQUEsS0FBSyxFQUFMQTtBQUFGLE9BQTlDLENBRGE7QUFBQTtBQUxDLEdBQVA7QUFBQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGkxOG5leHQgZnJvbSAnaTE4bmV4dCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCAoKSA9PiAoe1xyXG4gICAgQ0FMQ19SRVFVSVJFRF9URVhUOiBpMThuZXh0LnQoJ1RoZSBjYWxjdWxhdG9yIGZpZWxkIGlzIHJlcXVpcmVkJyksXHJcbiAgICBGT1JNVUxBX1JFUVVJUkVEX1RFWFQ6IGkxOG5leHQudCgnQWRkIGF0IGxlYXN0IG9uZSBlbGVtZW50IHRvIHRoZSBmb3JtdWxhJyksXHJcbiAgICBOVU1CRVJfVFlQRV9URVhUOiBpMThuZXh0LnQoJ011c3QgYmUgYSBudW1iZXInKSxcclxuICAgIFJFUVVJUkVEX1RFWFQ6IGkxOG5leHQudCgnVGhpcyBmaWVsZCBpcyByZXF1aXJlZCcpLFxyXG4gICAgZ2V0TWluVmFsdWVUZXh0OiAodmFsdWUpID0+XHJcbiAgICAgICAgaTE4bmV4dC50KCdJdCBjYW5ub3QgYmUgbGVzcyB0aGFuIHt7dmFsdWV9fScsIHsgdmFsdWUgfSksXHJcbn0pO1xyXG4iXX0=