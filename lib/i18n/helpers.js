"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initInternationalization = void 0;

var _i18next = _interopRequireDefault(require("i18next"));

var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));

var _reactI18next = require("react-i18next");

var _constants = require("./constants");

var _en = _interopRequireDefault(require("./translations/en.json"));

var _es = _interopRequireDefault(require("./translations/es.json"));

var _pt = _interopRequireDefault(require("./translations/pt.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initInternationalization = function initInternationalization() {
  var language = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _constants.DEFAULT_LANGUAGE;

  if ((0, _isEmpty.default)(_i18next.default.languages)) {
    _i18next.default.use(_reactI18next.initReactI18next).init({
      lng: language,
      resources: {
        en: {
          translation: _en.default
        },
        es: {
          translation: _es.default
        },
        pt: {
          translation: _pt.default
        }
      },
      fallbackLng: _constants.DEFAULT_LANGUAGE,
      debug: true,
      keySeparator: false,
      interpolation: {
        escapeValue: false
      },
      react: {
        wait: true,
        transSupportBasicHtmlNodes: true
      }
    });
  } else {
    _i18next.default.addResourceBundle('en', 'translations', _en.default);

    _i18next.default.addResourceBundle('es', 'translations', _es.default);

    _i18next.default.addResourceBundle('pt', 'translations', _pt.default);
  }
};

exports.initInternationalization = initInternationalization;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pMThuL2hlbHBlcnMuanMiXSwibmFtZXMiOlsiaW5pdEludGVybmF0aW9uYWxpemF0aW9uIiwibGFuZ3VhZ2UiLCJERUZBVUxUX0xBTkdVQUdFIiwiaTE4biIsImxhbmd1YWdlcyIsInVzZSIsImluaXRSZWFjdEkxOG5leHQiLCJpbml0IiwibG5nIiwicmVzb3VyY2VzIiwiZW4iLCJ0cmFuc2xhdGlvbiIsIlRSQU5TTEFUSU9OU19FTiIsImVzIiwiVFJBTlNMQVRJT05TX0VTIiwicHQiLCJUUkFOU0xBVElPTlNfUFQiLCJmYWxsYmFja0xuZyIsImRlYnVnIiwia2V5U2VwYXJhdG9yIiwiaW50ZXJwb2xhdGlvbiIsImVzY2FwZVZhbHVlIiwicmVhY3QiLCJ3YWl0IiwidHJhbnNTdXBwb3J0QmFzaWNIdG1sTm9kZXMiLCJhZGRSZXNvdXJjZUJ1bmRsZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOztBQUNBOztBQUNBOztBQUNBOzs7O0FBRU8sSUFBTUEsd0JBQXdCLEdBQUcsU0FBM0JBLHdCQUEyQixHQUFpQztBQUFBLE1BQWhDQyxRQUFnQyx1RUFBckJDLDJCQUFxQjs7QUFDckUsTUFBSSxzQkFBUUMsaUJBQUtDLFNBQWIsQ0FBSixFQUE2QjtBQUN6QkQscUJBQUtFLEdBQUwsQ0FBU0MsOEJBQVQsRUFBMkJDLElBQTNCLENBQWdDO0FBQzVCQyxNQUFBQSxHQUFHLEVBQUVQLFFBRHVCO0FBRTVCUSxNQUFBQSxTQUFTLEVBQUU7QUFDUEMsUUFBQUEsRUFBRSxFQUFFO0FBQ0FDLFVBQUFBLFdBQVcsRUFBRUM7QUFEYixTQURHO0FBSVBDLFFBQUFBLEVBQUUsRUFBRTtBQUNBRixVQUFBQSxXQUFXLEVBQUVHO0FBRGIsU0FKRztBQU9QQyxRQUFBQSxFQUFFLEVBQUU7QUFDQUosVUFBQUEsV0FBVyxFQUFFSztBQURiO0FBUEcsT0FGaUI7QUFhNUJDLE1BQUFBLFdBQVcsRUFBRWYsMkJBYmU7QUFjNUJnQixNQUFBQSxLQUFLLEVBQUUsSUFkcUI7QUFlNUJDLE1BQUFBLFlBQVksRUFBRSxLQWZjO0FBZ0I1QkMsTUFBQUEsYUFBYSxFQUFFO0FBQ1hDLFFBQUFBLFdBQVcsRUFBRTtBQURGLE9BaEJhO0FBbUI1QkMsTUFBQUEsS0FBSyxFQUFFO0FBQ0hDLFFBQUFBLElBQUksRUFBRSxJQURIO0FBRUhDLFFBQUFBLDBCQUEwQixFQUFFO0FBRnpCO0FBbkJxQixLQUFoQztBQXdCSCxHQXpCRCxNQXlCTztBQUNIckIscUJBQUtzQixpQkFBTCxDQUF1QixJQUF2QixFQUE2QixjQUE3QixFQUE2Q2IsV0FBN0M7O0FBQ0FULHFCQUFLc0IsaUJBQUwsQ0FBdUIsSUFBdkIsRUFBNkIsY0FBN0IsRUFBNkNYLFdBQTdDOztBQUNBWCxxQkFBS3NCLGlCQUFMLENBQXVCLElBQXZCLEVBQTZCLGNBQTdCLEVBQTZDVCxXQUE3QztBQUNIO0FBQ0osQ0EvQk0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgaTE4biBmcm9tICdpMThuZXh0JztcclxuaW1wb3J0IGlzRW1wdHkgZnJvbSAnbG9kYXNoL2lzRW1wdHknO1xyXG5pbXBvcnQgeyBpbml0UmVhY3RJMThuZXh0IH0gZnJvbSAncmVhY3QtaTE4bmV4dCc7XHJcblxyXG5pbXBvcnQgeyBERUZBVUxUX0xBTkdVQUdFIH0gZnJvbSAnLi9jb25zdGFudHMnO1xyXG5pbXBvcnQgVFJBTlNMQVRJT05TX0VOIGZyb20gJy4vdHJhbnNsYXRpb25zL2VuLmpzb24nO1xyXG5pbXBvcnQgVFJBTlNMQVRJT05TX0VTIGZyb20gJy4vdHJhbnNsYXRpb25zL2VzLmpzb24nO1xyXG5pbXBvcnQgVFJBTlNMQVRJT05TX1BUIGZyb20gJy4vdHJhbnNsYXRpb25zL3B0Lmpzb24nO1xyXG5cclxuZXhwb3J0IGNvbnN0IGluaXRJbnRlcm5hdGlvbmFsaXphdGlvbiA9IChsYW5ndWFnZSA9IERFRkFVTFRfTEFOR1VBR0UpID0+IHtcclxuICAgIGlmIChpc0VtcHR5KGkxOG4ubGFuZ3VhZ2VzKSkge1xyXG4gICAgICAgIGkxOG4udXNlKGluaXRSZWFjdEkxOG5leHQpLmluaXQoe1xyXG4gICAgICAgICAgICBsbmc6IGxhbmd1YWdlLFxyXG4gICAgICAgICAgICByZXNvdXJjZXM6IHtcclxuICAgICAgICAgICAgICAgIGVuOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNsYXRpb246IFRSQU5TTEFUSU9OU19FTixcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlczoge1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zbGF0aW9uOiBUUkFOU0xBVElPTlNfRVMsXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcHQ6IHtcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2xhdGlvbjogVFJBTlNMQVRJT05TX1BULFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZmFsbGJhY2tMbmc6IERFRkFVTFRfTEFOR1VBR0UsXHJcbiAgICAgICAgICAgIGRlYnVnOiB0cnVlLFxyXG4gICAgICAgICAgICBrZXlTZXBhcmF0b3I6IGZhbHNlLFxyXG4gICAgICAgICAgICBpbnRlcnBvbGF0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICBlc2NhcGVWYWx1ZTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJlYWN0OiB7XHJcbiAgICAgICAgICAgICAgICB3YWl0OiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNTdXBwb3J0QmFzaWNIdG1sTm9kZXM6IHRydWUsXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIGkxOG4uYWRkUmVzb3VyY2VCdW5kbGUoJ2VuJywgJ3RyYW5zbGF0aW9ucycsIFRSQU5TTEFUSU9OU19FTik7XHJcbiAgICAgICAgaTE4bi5hZGRSZXNvdXJjZUJ1bmRsZSgnZXMnLCAndHJhbnNsYXRpb25zJywgVFJBTlNMQVRJT05TX0VTKTtcclxuICAgICAgICBpMThuLmFkZFJlc291cmNlQnVuZGxlKCdwdCcsICd0cmFuc2xhdGlvbnMnLCBUUkFOU0xBVElPTlNfUFQpO1xyXG4gICAgfVxyXG59O1xyXG4iXX0=