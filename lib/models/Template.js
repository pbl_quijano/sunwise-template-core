"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxOrm = require("redux-orm");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Template = /*#__PURE__*/function (_Model) {
  _inherits(Template, _Model);

  var _super = _createSuper(Template);

  function Template() {
    _classCallCheck(this, Template);

    return _super.apply(this, arguments);
  }

  return Template;
}(_reduxOrm.Model);

exports.default = Template;
Template.modelName = 'Template';
Template.fields = {
  company: (0, _reduxOrm.attr)(),
  created_at: (0, _reduxOrm.attr)(),
  description: (0, _reduxOrm.attr)(),
  footer: (0, _reduxOrm.attr)(),
  id: (0, _reduxOrm.attr)(),
  language: (0, _reduxOrm.attr)(),
  logo: (0, _reduxOrm.attr)(),
  parent: (0, _reduxOrm.attr)(),
  primary_color: (0, _reduxOrm.attr)(),
  proposals_number: (0, _reduxOrm.attr)(),
  secondary_color: (0, _reduxOrm.attr)(),
  title: (0, _reduxOrm.attr)(),
  type: (0, _reduxOrm.attr)(),
  typography: (0, _reduxOrm.attr)(),
  updated_at: (0, _reduxOrm.attr)(),
  version: (0, _reduxOrm.attr)()
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvVGVtcGxhdGUuanMiXSwibmFtZXMiOlsiVGVtcGxhdGUiLCJNb2RlbCIsIm1vZGVsTmFtZSIsImZpZWxkcyIsImNvbXBhbnkiLCJjcmVhdGVkX2F0IiwiZGVzY3JpcHRpb24iLCJmb290ZXIiLCJpZCIsImxhbmd1YWdlIiwibG9nbyIsInBhcmVudCIsInByaW1hcnlfY29sb3IiLCJwcm9wb3NhbHNfbnVtYmVyIiwic2Vjb25kYXJ5X2NvbG9yIiwidGl0bGUiLCJ0eXBlIiwidHlwb2dyYXBoeSIsInVwZGF0ZWRfYXQiLCJ2ZXJzaW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBRXFCQSxROzs7Ozs7Ozs7Ozs7RUFBaUJDLGU7OztBQUN0Q0QsUUFBUSxDQUFDRSxTQUFULEdBQXFCLFVBQXJCO0FBQ0FGLFFBQVEsQ0FBQ0csTUFBVCxHQUFrQjtBQUNkQyxFQUFBQSxPQUFPLEVBQUUscUJBREs7QUFFZEMsRUFBQUEsVUFBVSxFQUFFLHFCQUZFO0FBR2RDLEVBQUFBLFdBQVcsRUFBRSxxQkFIQztBQUlkQyxFQUFBQSxNQUFNLEVBQUUscUJBSk07QUFLZEMsRUFBQUEsRUFBRSxFQUFFLHFCQUxVO0FBTWRDLEVBQUFBLFFBQVEsRUFBRSxxQkFOSTtBQU9kQyxFQUFBQSxJQUFJLEVBQUUscUJBUFE7QUFRZEMsRUFBQUEsTUFBTSxFQUFFLHFCQVJNO0FBU2RDLEVBQUFBLGFBQWEsRUFBRSxxQkFURDtBQVVkQyxFQUFBQSxnQkFBZ0IsRUFBRSxxQkFWSjtBQVdkQyxFQUFBQSxlQUFlLEVBQUUscUJBWEg7QUFZZEMsRUFBQUEsS0FBSyxFQUFFLHFCQVpPO0FBYWRDLEVBQUFBLElBQUksRUFBRSxxQkFiUTtBQWNkQyxFQUFBQSxVQUFVLEVBQUUscUJBZEU7QUFlZEMsRUFBQUEsVUFBVSxFQUFFLHFCQWZFO0FBZ0JkQyxFQUFBQSxPQUFPLEVBQUU7QUFoQkssQ0FBbEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2RlbCwgYXR0ciB9IGZyb20gJ3JlZHV4LW9ybSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFRlbXBsYXRlIGV4dGVuZHMgTW9kZWwge31cblRlbXBsYXRlLm1vZGVsTmFtZSA9ICdUZW1wbGF0ZSc7XG5UZW1wbGF0ZS5maWVsZHMgPSB7XG4gICAgY29tcGFueTogYXR0cigpLFxuICAgIGNyZWF0ZWRfYXQ6IGF0dHIoKSxcbiAgICBkZXNjcmlwdGlvbjogYXR0cigpLFxuICAgIGZvb3RlcjogYXR0cigpLFxuICAgIGlkOiBhdHRyKCksXG4gICAgbGFuZ3VhZ2U6IGF0dHIoKSxcbiAgICBsb2dvOiBhdHRyKCksXG4gICAgcGFyZW50OiBhdHRyKCksXG4gICAgcHJpbWFyeV9jb2xvcjogYXR0cigpLFxuICAgIHByb3Bvc2Fsc19udW1iZXI6IGF0dHIoKSxcbiAgICBzZWNvbmRhcnlfY29sb3I6IGF0dHIoKSxcbiAgICB0aXRsZTogYXR0cigpLFxuICAgIHR5cGU6IGF0dHIoKSxcbiAgICB0eXBvZ3JhcGh5OiBhdHRyKCksXG4gICAgdXBkYXRlZF9hdDogYXR0cigpLFxuICAgIHZlcnNpb246IGF0dHIoKSxcbn07XG4iXX0=