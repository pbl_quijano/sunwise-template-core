"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _maps = require("../constants/maps");

var _maps2 = require("../helpers/maps");

var _SolarModulePolygonBuilder = _interopRequireDefault(require("./SolarModulePolygonBuilder "));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _default = function _default(google, vertexChange) {
  var SolarModulePolygon = (0, _SolarModulePolygonBuilder.default)(google);

  var SegmentPolygon = /*#__PURE__*/function (_google$maps$Polygon) {
    _inherits(SegmentPolygon, _google$maps$Polygon);

    var _super = _createSuper(SegmentPolygon);

    function SegmentPolygon(_ref) {
      var _this;

      var id = _ref.id,
          _ref$solarModules = _ref.solarModules,
          solarModules = _ref$solarModules === void 0 ? [] : _ref$solarModules,
          showMeterLabels = _ref.showMeterLabels,
          _ref$options = _ref.options,
          options = _ref$options === void 0 ? {} : _ref$options,
          _ref$solarModulesStyl = _ref.solarModulesStyle,
          solarModulesStyle = _ref$solarModulesStyl === void 0 ? {} : _ref$solarModulesStyl;

      _classCallCheck(this, SegmentPolygon);

      var newOptions = _objectSpread({
        fillColor: _maps.SEGMENT_ACTIVE_COLOR,
        strokeColor: _maps.SEGMENT_STROKE_ACTIVE_COLOR,
        strokePosition: google.maps.StrokePosition.OUTSIDE,
        fillOpacity: 1,
        editable: false,
        draggable: false,
        strokeWeight: 4,
        strokeOpacity: 1,
        zIndex: 2
      }, options);

      _this = _super.call(this, newOptions);
      _this.disabled = false;
      _this.id = id;
      _this.meterLabels = (0, _maps2.createMeterLabelPopup)(options.paths, google, showMeterLabels ? _this.map : null);
      _this.solarModules = solarModules.map(function (solarModule) {
        var shape = new SolarModulePolygon(solarModule.map(function (modulePoint) {
          return {
            lat: modulePoint[0],
            lng: modulePoint[1]
          };
        }), _this.map, solarModulesStyle);
        return shape;
      });

      if (vertexChange) {
        (0, _maps2.addPathListeners)({
          google: google,
          id: _this.id,
          map: _this.map,
          meterLabels: _this.meterLabels,
          paths: _this.getPaths(),
          setSolarModules: function setSolarModules(solarModules) {
            return _this.setSolarModules(solarModules);
          },
          setSolarModulesDisabled: function setSolarModulesDisabled() {
            return _this.setSolarModulesDisabled();
          },
          vertexChange: vertexChange
        });
      }

      return _this;
    }

    _createClass(SegmentPolygon, [{
      key: "resetPosition",
      value: function resetPosition(segmentValue, showMeterLabels) {
        var _this2 = this;

        var shapeCoords = segmentValue.polygon.map(function (point) {
          return new google.maps.LatLng(point[0], point[1]);
        });
        this.setPaths(shapeCoords);
        this.setSolarModules(segmentValue.solar_modules);
        this.setLabelsVisibility(false);
        this.meterLabels = (0, _maps2.createMeterLabelPopup)(shapeCoords, google, showMeterLabels ? this.map : null);

        if (vertexChange) {
          (0, _maps2.addPathListeners)({
            google: google,
            id: this.id,
            map: this.map,
            meterLabels: this.meterLabels,
            paths: this.getPaths(),
            setSolarModules: function setSolarModules(solarModules) {
              return _this2.setSolarModules(solarModules);
            },
            setSolarModulesDisabled: function setSolarModulesDisabled() {
              return _this2.setSolarModulesDisabled();
            },
            vertexChange: vertexChange
          });
        }
      }
    }, {
      key: "setMap",
      value: function setMap(map) {
        if (map === null) {
          this.meterLabels.forEach(function (meterLabel) {
            return meterLabel.setMap(null);
          });
          this.meterLabels = [];
          this.getPaths().forEach(function (path) {
            google.maps.event.clearListeners(path, 'remove_at');
            google.maps.event.clearListeners(path, 'set_at');
            google.maps.event.clearListeners(path, 'insert_at');
          });
          this.solarModules.forEach(function (solarModule) {
            solarModule.setMap(null);
          });
          this.solarModules = [];
        }

        _get(_getPrototypeOf(SegmentPolygon.prototype), "setMap", this).call(this, map);
      }
    }, {
      key: "setSelected",
      value: function setSelected(isSelcted) {
        var _this3 = this;

        if (isSelcted) {
          this.setOptions({
            draggable: true,
            editable: true,
            fillColor: _maps.SEGMENT_ACTIVE_COLOR,
            strokeColor: _maps.SEGMENT_STROKE_ACTIVE_COLOR
          });
          this.meterLabels.forEach(function (meterLabel) {
            return meterLabel.setMap(_this3.map);
          });
        } else {
          this.setOptions({
            draggable: false,
            editable: false,
            fillColor: _maps.SEGMENT_COLOR,
            strokeColor: _maps.SEGMENT_STROKE_COLOR
          });
          this.meterLabels.forEach(function (meterLabel) {
            return meterLabel.setMap(null);
          });
        }
      }
    }, {
      key: "setDisabled",
      value: function setDisabled(isDisabled) {
        this.disabled = isDisabled;
        this.setSolarModulesDisabled(isDisabled);

        if (isDisabled) {
          this.setOptions({
            clickable: false,
            fillOpacity: 0.6,
            strokeOpacity: 0.6
          });
        } else {
          this.setOptions({
            clickable: true,
            fillOpacity: 1,
            strokeOpacity: 1
          });
        }
      }
    }, {
      key: "setSolarModules",
      value: function setSolarModules(solarModules) {
        var _this4 = this;

        this.solarModules.forEach(function (solarModule) {
          solarModule.setMap(null);
          google.maps.event.clearListeners(solarModule, 'click');
        });
        this.solarModules = solarModules.map(function (solarModule) {
          return new SolarModulePolygon(solarModule.map(function (modulePoint) {
            return {
              lat: modulePoint[0],
              lng: modulePoint[1]
            };
          }), _this4.map);
        });
      }
    }, {
      key: "setSolarModulesDisabled",
      value: function setSolarModulesDisabled() {
        var isDisabled = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
        this.solarModules.forEach(function (solarModule) {
          solarModule.setDisabled(isDisabled);
        });
      }
    }, {
      key: "setSolarModulesFillColor",
      value: function setSolarModulesFillColor(fillColor) {
        this.solarModules.forEach(function (solarModule) {
          solarModule.setOptions({
            fillColor: fillColor
          });
        });
      }
    }, {
      key: "setSolarModulesStrokeColor",
      value: function setSolarModulesStrokeColor(strokeColor) {
        this.solarModules.forEach(function (solarModule) {
          solarModule.setOptions({
            strokeColor: strokeColor
          });
        });
      }
    }, {
      key: "setLabelsVisibility",
      value: function setLabelsVisibility(isVisible) {
        var _this5 = this;

        if (isVisible) {
          this.meterLabels.forEach(function (meterLabel) {
            return meterLabel.setMap(_this5.map);
          });
        } else {
          this.meterLabels.forEach(function (meterLabel) {
            return meterLabel.setMap(null);
          });
        }
      }
    }]);

    return SegmentPolygon;
  }(google.maps.Polygon);

  return SegmentPolygon;
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvU2VnbWVudFBvbHlnb25CdWlsZGVyLmpzIl0sIm5hbWVzIjpbImdvb2dsZSIsInZlcnRleENoYW5nZSIsIlNvbGFyTW9kdWxlUG9seWdvbiIsIlNlZ21lbnRQb2x5Z29uIiwiaWQiLCJzb2xhck1vZHVsZXMiLCJzaG93TWV0ZXJMYWJlbHMiLCJvcHRpb25zIiwic29sYXJNb2R1bGVzU3R5bGUiLCJuZXdPcHRpb25zIiwiZmlsbENvbG9yIiwiU0VHTUVOVF9BQ1RJVkVfQ09MT1IiLCJzdHJva2VDb2xvciIsIlNFR01FTlRfU1RST0tFX0FDVElWRV9DT0xPUiIsInN0cm9rZVBvc2l0aW9uIiwibWFwcyIsIlN0cm9rZVBvc2l0aW9uIiwiT1VUU0lERSIsImZpbGxPcGFjaXR5IiwiZWRpdGFibGUiLCJkcmFnZ2FibGUiLCJzdHJva2VXZWlnaHQiLCJzdHJva2VPcGFjaXR5IiwiekluZGV4IiwiZGlzYWJsZWQiLCJtZXRlckxhYmVscyIsInBhdGhzIiwibWFwIiwic29sYXJNb2R1bGUiLCJzaGFwZSIsIm1vZHVsZVBvaW50IiwibGF0IiwibG5nIiwiZ2V0UGF0aHMiLCJzZXRTb2xhck1vZHVsZXMiLCJzZXRTb2xhck1vZHVsZXNEaXNhYmxlZCIsInNlZ21lbnRWYWx1ZSIsInNoYXBlQ29vcmRzIiwicG9seWdvbiIsInBvaW50IiwiTGF0TG5nIiwic2V0UGF0aHMiLCJzb2xhcl9tb2R1bGVzIiwic2V0TGFiZWxzVmlzaWJpbGl0eSIsImZvckVhY2giLCJtZXRlckxhYmVsIiwic2V0TWFwIiwicGF0aCIsImV2ZW50IiwiY2xlYXJMaXN0ZW5lcnMiLCJpc1NlbGN0ZWQiLCJzZXRPcHRpb25zIiwiU0VHTUVOVF9DT0xPUiIsIlNFR01FTlRfU1RST0tFX0NPTE9SIiwiaXNEaXNhYmxlZCIsImNsaWNrYWJsZSIsInNldERpc2FibGVkIiwiaXNWaXNpYmxlIiwiUG9seWdvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBT0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFFZSxrQkFBQ0EsTUFBRCxFQUFTQyxZQUFULEVBQTBCO0FBQ3JDLE1BQU1DLGtCQUFrQixHQUFHLHdDQUEwQkYsTUFBMUIsQ0FBM0I7O0FBRHFDLE1BRS9CRyxjQUYrQjtBQUFBOztBQUFBOztBQUdqQyxrQ0FNRztBQUFBOztBQUFBLFVBTENDLEVBS0QsUUFMQ0EsRUFLRDtBQUFBLG1DQUpDQyxZQUlEO0FBQUEsVUFKQ0EsWUFJRCxrQ0FKZ0IsRUFJaEI7QUFBQSxVQUhDQyxlQUdELFFBSENBLGVBR0Q7QUFBQSw4QkFGQ0MsT0FFRDtBQUFBLFVBRkNBLE9BRUQsNkJBRlcsRUFFWDtBQUFBLHVDQURDQyxpQkFDRDtBQUFBLFVBRENBLGlCQUNELHNDQURxQixFQUNyQjs7QUFBQTs7QUFDQyxVQUFNQyxVQUFVO0FBQ1pDLFFBQUFBLFNBQVMsRUFBRUMsMEJBREM7QUFFWkMsUUFBQUEsV0FBVyxFQUFFQyxpQ0FGRDtBQUdaQyxRQUFBQSxjQUFjLEVBQUVkLE1BQU0sQ0FBQ2UsSUFBUCxDQUFZQyxjQUFaLENBQTJCQyxPQUgvQjtBQUlaQyxRQUFBQSxXQUFXLEVBQUUsQ0FKRDtBQUtaQyxRQUFBQSxRQUFRLEVBQUUsS0FMRTtBQU1aQyxRQUFBQSxTQUFTLEVBQUUsS0FOQztBQU9aQyxRQUFBQSxZQUFZLEVBQUUsQ0FQRjtBQVFaQyxRQUFBQSxhQUFhLEVBQUUsQ0FSSDtBQVNaQyxRQUFBQSxNQUFNLEVBQUU7QUFUSSxTQVVUaEIsT0FWUyxDQUFoQjs7QUFZQSxnQ0FBTUUsVUFBTjtBQUNBLFlBQUtlLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQSxZQUFLcEIsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsWUFBS3FCLFdBQUwsR0FBbUIsa0NBQ2ZsQixPQUFPLENBQUNtQixLQURPLEVBRWYxQixNQUZlLEVBR2ZNLGVBQWUsR0FBRyxNQUFLcUIsR0FBUixHQUFjLElBSGQsQ0FBbkI7QUFNQSxZQUFLdEIsWUFBTCxHQUFvQkEsWUFBWSxDQUFDc0IsR0FBYixDQUFpQixVQUFDQyxXQUFELEVBQWlCO0FBQ2xELFlBQU1DLEtBQUssR0FBRyxJQUFJM0Isa0JBQUosQ0FDVjBCLFdBQVcsQ0FBQ0QsR0FBWixDQUFnQixVQUFDRyxXQUFEO0FBQUEsaUJBQWtCO0FBQzlCQyxZQUFBQSxHQUFHLEVBQUVELFdBQVcsQ0FBQyxDQUFELENBRGM7QUFFOUJFLFlBQUFBLEdBQUcsRUFBRUYsV0FBVyxDQUFDLENBQUQ7QUFGYyxXQUFsQjtBQUFBLFNBQWhCLENBRFUsRUFLVixNQUFLSCxHQUxLLEVBTVZuQixpQkFOVSxDQUFkO0FBUUEsZUFBT3FCLEtBQVA7QUFDSCxPQVZtQixDQUFwQjs7QUFXQSxVQUFJNUIsWUFBSixFQUFrQjtBQUNkLHFDQUFpQjtBQUNiRCxVQUFBQSxNQUFNLEVBQU5BLE1BRGE7QUFFYkksVUFBQUEsRUFBRSxFQUFFLE1BQUtBLEVBRkk7QUFHYnVCLFVBQUFBLEdBQUcsRUFBRSxNQUFLQSxHQUhHO0FBSWJGLFVBQUFBLFdBQVcsRUFBRSxNQUFLQSxXQUpMO0FBS2JDLFVBQUFBLEtBQUssRUFBRSxNQUFLTyxRQUFMLEVBTE07QUFNYkMsVUFBQUEsZUFBZSxFQUFFLHlCQUFDN0IsWUFBRDtBQUFBLG1CQUNiLE1BQUs2QixlQUFMLENBQXFCN0IsWUFBckIsQ0FEYTtBQUFBLFdBTko7QUFRYjhCLFVBQUFBLHVCQUF1QixFQUFFO0FBQUEsbUJBQ3JCLE1BQUtBLHVCQUFMLEVBRHFCO0FBQUEsV0FSWjtBQVVibEMsVUFBQUEsWUFBWSxFQUFaQTtBQVZhLFNBQWpCO0FBWUg7O0FBOUNGO0FBK0NGOztBQXhEZ0M7QUFBQTtBQUFBLGFBMERqQyx1QkFBY21DLFlBQWQsRUFBNEI5QixlQUE1QixFQUE2QztBQUFBOztBQUN6QyxZQUFNK0IsV0FBVyxHQUFHRCxZQUFZLENBQUNFLE9BQWIsQ0FBcUJYLEdBQXJCLENBQ2hCLFVBQUNZLEtBQUQ7QUFBQSxpQkFBVyxJQUFJdkMsTUFBTSxDQUFDZSxJQUFQLENBQVl5QixNQUFoQixDQUF1QkQsS0FBSyxDQUFDLENBQUQsQ0FBNUIsRUFBaUNBLEtBQUssQ0FBQyxDQUFELENBQXRDLENBQVg7QUFBQSxTQURnQixDQUFwQjtBQUdBLGFBQUtFLFFBQUwsQ0FBY0osV0FBZDtBQUNBLGFBQUtILGVBQUwsQ0FBcUJFLFlBQVksQ0FBQ00sYUFBbEM7QUFDQSxhQUFLQyxtQkFBTCxDQUF5QixLQUF6QjtBQUNBLGFBQUtsQixXQUFMLEdBQW1CLGtDQUNmWSxXQURlLEVBRWZyQyxNQUZlLEVBR2ZNLGVBQWUsR0FBRyxLQUFLcUIsR0FBUixHQUFjLElBSGQsQ0FBbkI7O0FBS0EsWUFBSTFCLFlBQUosRUFBa0I7QUFDZCx1Q0FBaUI7QUFDYkQsWUFBQUEsTUFBTSxFQUFOQSxNQURhO0FBRWJJLFlBQUFBLEVBQUUsRUFBRSxLQUFLQSxFQUZJO0FBR2J1QixZQUFBQSxHQUFHLEVBQUUsS0FBS0EsR0FIRztBQUliRixZQUFBQSxXQUFXLEVBQUUsS0FBS0EsV0FKTDtBQUtiQyxZQUFBQSxLQUFLLEVBQUUsS0FBS08sUUFBTCxFQUxNO0FBTWJDLFlBQUFBLGVBQWUsRUFBRSx5QkFBQzdCLFlBQUQ7QUFBQSxxQkFDYixNQUFJLENBQUM2QixlQUFMLENBQXFCN0IsWUFBckIsQ0FEYTtBQUFBLGFBTko7QUFRYjhCLFlBQUFBLHVCQUF1QixFQUFFO0FBQUEscUJBQ3JCLE1BQUksQ0FBQ0EsdUJBQUwsRUFEcUI7QUFBQSxhQVJaO0FBVWJsQyxZQUFBQSxZQUFZLEVBQVpBO0FBVmEsV0FBakI7QUFZSDtBQUNKO0FBcEZnQztBQUFBO0FBQUEsYUFzRmpDLGdCQUFPMEIsR0FBUCxFQUFZO0FBQ1IsWUFBSUEsR0FBRyxLQUFLLElBQVosRUFBa0I7QUFDZCxlQUFLRixXQUFMLENBQWlCbUIsT0FBakIsQ0FBeUIsVUFBQ0MsVUFBRDtBQUFBLG1CQUNyQkEsVUFBVSxDQUFDQyxNQUFYLENBQWtCLElBQWxCLENBRHFCO0FBQUEsV0FBekI7QUFHQSxlQUFLckIsV0FBTCxHQUFtQixFQUFuQjtBQUNBLGVBQUtRLFFBQUwsR0FBZ0JXLE9BQWhCLENBQXdCLFVBQUNHLElBQUQsRUFBVTtBQUM5Qi9DLFlBQUFBLE1BQU0sQ0FBQ2UsSUFBUCxDQUFZaUMsS0FBWixDQUFrQkMsY0FBbEIsQ0FBaUNGLElBQWpDLEVBQXVDLFdBQXZDO0FBQ0EvQyxZQUFBQSxNQUFNLENBQUNlLElBQVAsQ0FBWWlDLEtBQVosQ0FBa0JDLGNBQWxCLENBQWlDRixJQUFqQyxFQUF1QyxRQUF2QztBQUNBL0MsWUFBQUEsTUFBTSxDQUFDZSxJQUFQLENBQVlpQyxLQUFaLENBQWtCQyxjQUFsQixDQUFpQ0YsSUFBakMsRUFBdUMsV0FBdkM7QUFDSCxXQUpEO0FBS0EsZUFBSzFDLFlBQUwsQ0FBa0J1QyxPQUFsQixDQUEwQixVQUFDaEIsV0FBRCxFQUFpQjtBQUN2Q0EsWUFBQUEsV0FBVyxDQUFDa0IsTUFBWixDQUFtQixJQUFuQjtBQUNILFdBRkQ7QUFHQSxlQUFLekMsWUFBTCxHQUFvQixFQUFwQjtBQUNIOztBQUNELG1GQUFhc0IsR0FBYjtBQUNIO0FBdkdnQztBQUFBO0FBQUEsYUF5R2pDLHFCQUFZdUIsU0FBWixFQUF1QjtBQUFBOztBQUNuQixZQUFJQSxTQUFKLEVBQWU7QUFDWCxlQUFLQyxVQUFMLENBQWdCO0FBQ1ovQixZQUFBQSxTQUFTLEVBQUUsSUFEQztBQUVaRCxZQUFBQSxRQUFRLEVBQUUsSUFGRTtBQUdaVCxZQUFBQSxTQUFTLEVBQUVDLDBCQUhDO0FBSVpDLFlBQUFBLFdBQVcsRUFBRUM7QUFKRCxXQUFoQjtBQU1BLGVBQUtZLFdBQUwsQ0FBaUJtQixPQUFqQixDQUF5QixVQUFDQyxVQUFEO0FBQUEsbUJBQ3JCQSxVQUFVLENBQUNDLE1BQVgsQ0FBa0IsTUFBSSxDQUFDbkIsR0FBdkIsQ0FEcUI7QUFBQSxXQUF6QjtBQUdILFNBVkQsTUFVTztBQUNILGVBQUt3QixVQUFMLENBQWdCO0FBQ1ovQixZQUFBQSxTQUFTLEVBQUUsS0FEQztBQUVaRCxZQUFBQSxRQUFRLEVBQUUsS0FGRTtBQUdaVCxZQUFBQSxTQUFTLEVBQUUwQyxtQkFIQztBQUlaeEMsWUFBQUEsV0FBVyxFQUFFeUM7QUFKRCxXQUFoQjtBQU1BLGVBQUs1QixXQUFMLENBQWlCbUIsT0FBakIsQ0FBeUIsVUFBQ0MsVUFBRDtBQUFBLG1CQUNyQkEsVUFBVSxDQUFDQyxNQUFYLENBQWtCLElBQWxCLENBRHFCO0FBQUEsV0FBekI7QUFHSDtBQUNKO0FBL0hnQztBQUFBO0FBQUEsYUFpSWpDLHFCQUFZUSxVQUFaLEVBQXdCO0FBQ3BCLGFBQUs5QixRQUFMLEdBQWdCOEIsVUFBaEI7QUFDQSxhQUFLbkIsdUJBQUwsQ0FBNkJtQixVQUE3Qjs7QUFDQSxZQUFJQSxVQUFKLEVBQWdCO0FBQ1osZUFBS0gsVUFBTCxDQUFnQjtBQUNaSSxZQUFBQSxTQUFTLEVBQUUsS0FEQztBQUVackMsWUFBQUEsV0FBVyxFQUFFLEdBRkQ7QUFHWkksWUFBQUEsYUFBYSxFQUFFO0FBSEgsV0FBaEI7QUFLSCxTQU5ELE1BTU87QUFDSCxlQUFLNkIsVUFBTCxDQUFnQjtBQUNaSSxZQUFBQSxTQUFTLEVBQUUsSUFEQztBQUVackMsWUFBQUEsV0FBVyxFQUFFLENBRkQ7QUFHWkksWUFBQUEsYUFBYSxFQUFFO0FBSEgsV0FBaEI7QUFLSDtBQUNKO0FBakpnQztBQUFBO0FBQUEsYUFtSmpDLHlCQUFnQmpCLFlBQWhCLEVBQThCO0FBQUE7O0FBQzFCLGFBQUtBLFlBQUwsQ0FBa0J1QyxPQUFsQixDQUEwQixVQUFDaEIsV0FBRCxFQUFpQjtBQUN2Q0EsVUFBQUEsV0FBVyxDQUFDa0IsTUFBWixDQUFtQixJQUFuQjtBQUNBOUMsVUFBQUEsTUFBTSxDQUFDZSxJQUFQLENBQVlpQyxLQUFaLENBQWtCQyxjQUFsQixDQUFpQ3JCLFdBQWpDLEVBQThDLE9BQTlDO0FBQ0gsU0FIRDtBQUlBLGFBQUt2QixZQUFMLEdBQW9CQSxZQUFZLENBQUNzQixHQUFiLENBQWlCLFVBQUNDLFdBQUQsRUFBaUI7QUFDbEQsaUJBQU8sSUFBSTFCLGtCQUFKLENBQ0gwQixXQUFXLENBQUNELEdBQVosQ0FBZ0IsVUFBQ0csV0FBRDtBQUFBLG1CQUFrQjtBQUM5QkMsY0FBQUEsR0FBRyxFQUFFRCxXQUFXLENBQUMsQ0FBRCxDQURjO0FBRTlCRSxjQUFBQSxHQUFHLEVBQUVGLFdBQVcsQ0FBQyxDQUFEO0FBRmMsYUFBbEI7QUFBQSxXQUFoQixDQURHLEVBS0gsTUFBSSxDQUFDSCxHQUxGLENBQVA7QUFPSCxTQVJtQixDQUFwQjtBQVNIO0FBaktnQztBQUFBO0FBQUEsYUFtS2pDLG1DQUEyQztBQUFBLFlBQW5CMkIsVUFBbUIsdUVBQU4sSUFBTTtBQUN2QyxhQUFLakQsWUFBTCxDQUFrQnVDLE9BQWxCLENBQTBCLFVBQUNoQixXQUFELEVBQWlCO0FBQ3ZDQSxVQUFBQSxXQUFXLENBQUM0QixXQUFaLENBQXdCRixVQUF4QjtBQUNILFNBRkQ7QUFHSDtBQXZLZ0M7QUFBQTtBQUFBLGFBeUtqQyxrQ0FBeUI1QyxTQUF6QixFQUFvQztBQUNoQyxhQUFLTCxZQUFMLENBQWtCdUMsT0FBbEIsQ0FBMEIsVUFBQ2hCLFdBQUQsRUFBaUI7QUFDdkNBLFVBQUFBLFdBQVcsQ0FBQ3VCLFVBQVosQ0FBdUI7QUFDbkJ6QyxZQUFBQSxTQUFTLEVBQVRBO0FBRG1CLFdBQXZCO0FBR0gsU0FKRDtBQUtIO0FBL0tnQztBQUFBO0FBQUEsYUFpTGpDLG9DQUEyQkUsV0FBM0IsRUFBd0M7QUFDcEMsYUFBS1AsWUFBTCxDQUFrQnVDLE9BQWxCLENBQTBCLFVBQUNoQixXQUFELEVBQWlCO0FBQ3ZDQSxVQUFBQSxXQUFXLENBQUN1QixVQUFaLENBQXVCO0FBQ25CdkMsWUFBQUEsV0FBVyxFQUFYQTtBQURtQixXQUF2QjtBQUdILFNBSkQ7QUFLSDtBQXZMZ0M7QUFBQTtBQUFBLGFBeUxqQyw2QkFBb0I2QyxTQUFwQixFQUErQjtBQUFBOztBQUMzQixZQUFJQSxTQUFKLEVBQWU7QUFDWCxlQUFLaEMsV0FBTCxDQUFpQm1CLE9BQWpCLENBQXlCLFVBQUNDLFVBQUQ7QUFBQSxtQkFDckJBLFVBQVUsQ0FBQ0MsTUFBWCxDQUFrQixNQUFJLENBQUNuQixHQUF2QixDQURxQjtBQUFBLFdBQXpCO0FBR0gsU0FKRCxNQUlPO0FBQ0gsZUFBS0YsV0FBTCxDQUFpQm1CLE9BQWpCLENBQXlCLFVBQUNDLFVBQUQ7QUFBQSxtQkFDckJBLFVBQVUsQ0FBQ0MsTUFBWCxDQUFrQixJQUFsQixDQURxQjtBQUFBLFdBQXpCO0FBR0g7QUFDSjtBQW5NZ0M7O0FBQUE7QUFBQSxJQUVSOUMsTUFBTSxDQUFDZSxJQUFQLENBQVkyQyxPQUZKOztBQXNNckMsU0FBT3ZELGNBQVA7QUFDSCxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBTRUdNRU5UX0FDVElWRV9DT0xPUixcbiAgICBTRUdNRU5UX0NPTE9SLFxuICAgIFNFR01FTlRfU1RST0tFX0NPTE9SLFxuICAgIFNFR01FTlRfU1RST0tFX0FDVElWRV9DT0xPUixcbn0gZnJvbSAnQGNvbnN0YW50cy9tYXBzJztcblxuaW1wb3J0IHsgY3JlYXRlTWV0ZXJMYWJlbFBvcHVwLCBhZGRQYXRoTGlzdGVuZXJzIH0gZnJvbSAnQGhlbHBlcnMvbWFwcyc7XG5cbmltcG9ydCBTb2xhck1vZHVsZVBvbHlnb25CdWlsZGVyIGZyb20gJy4vU29sYXJNb2R1bGVQb2x5Z29uQnVpbGRlciAnO1xuXG5leHBvcnQgZGVmYXVsdCAoZ29vZ2xlLCB2ZXJ0ZXhDaGFuZ2UpID0+IHtcbiAgICBjb25zdCBTb2xhck1vZHVsZVBvbHlnb24gPSBTb2xhck1vZHVsZVBvbHlnb25CdWlsZGVyKGdvb2dsZSk7XG4gICAgY2xhc3MgU2VnbWVudFBvbHlnb24gZXh0ZW5kcyBnb29nbGUubWFwcy5Qb2x5Z29uIHtcbiAgICAgICAgY29uc3RydWN0b3Ioe1xuICAgICAgICAgICAgaWQsXG4gICAgICAgICAgICBzb2xhck1vZHVsZXMgPSBbXSxcbiAgICAgICAgICAgIHNob3dNZXRlckxhYmVscyxcbiAgICAgICAgICAgIG9wdGlvbnMgPSB7fSxcbiAgICAgICAgICAgIHNvbGFyTW9kdWxlc1N0eWxlID0ge30sXG4gICAgICAgIH0pIHtcbiAgICAgICAgICAgIGNvbnN0IG5ld09wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgZmlsbENvbG9yOiBTRUdNRU5UX0FDVElWRV9DT0xPUixcbiAgICAgICAgICAgICAgICBzdHJva2VDb2xvcjogU0VHTUVOVF9TVFJPS0VfQUNUSVZFX0NPTE9SLFxuICAgICAgICAgICAgICAgIHN0cm9rZVBvc2l0aW9uOiBnb29nbGUubWFwcy5TdHJva2VQb3NpdGlvbi5PVVRTSURFLFxuICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgICAgICBkcmFnZ2FibGU6IGZhbHNlLFxuICAgICAgICAgICAgICAgIHN0cm9rZVdlaWdodDogNCxcbiAgICAgICAgICAgICAgICBzdHJva2VPcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIHpJbmRleDogMixcbiAgICAgICAgICAgICAgICAuLi5vcHRpb25zLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIHN1cGVyKG5ld09wdGlvbnMpO1xuICAgICAgICAgICAgdGhpcy5kaXNhYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5pZCA9IGlkO1xuICAgICAgICAgICAgdGhpcy5tZXRlckxhYmVscyA9IGNyZWF0ZU1ldGVyTGFiZWxQb3B1cChcbiAgICAgICAgICAgICAgICBvcHRpb25zLnBhdGhzLFxuICAgICAgICAgICAgICAgIGdvb2dsZSxcbiAgICAgICAgICAgICAgICBzaG93TWV0ZXJMYWJlbHMgPyB0aGlzLm1hcCA6IG51bGxcbiAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgIHRoaXMuc29sYXJNb2R1bGVzID0gc29sYXJNb2R1bGVzLm1hcCgoc29sYXJNb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zdCBzaGFwZSA9IG5ldyBTb2xhck1vZHVsZVBvbHlnb24oXG4gICAgICAgICAgICAgICAgICAgIHNvbGFyTW9kdWxlLm1hcCgobW9kdWxlUG9pbnQpID0+ICh7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYXQ6IG1vZHVsZVBvaW50WzBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgbG5nOiBtb2R1bGVQb2ludFsxXSxcbiAgICAgICAgICAgICAgICAgICAgfSkpLFxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1hcCxcbiAgICAgICAgICAgICAgICAgICAgc29sYXJNb2R1bGVzU3R5bGVcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIHJldHVybiBzaGFwZTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaWYgKHZlcnRleENoYW5nZSkge1xuICAgICAgICAgICAgICAgIGFkZFBhdGhMaXN0ZW5lcnMoe1xuICAgICAgICAgICAgICAgICAgICBnb29nbGUsXG4gICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLmlkLFxuICAgICAgICAgICAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgICAgICAgICAgICBtZXRlckxhYmVsczogdGhpcy5tZXRlckxhYmVscyxcbiAgICAgICAgICAgICAgICAgICAgcGF0aHM6IHRoaXMuZ2V0UGF0aHMoKSxcbiAgICAgICAgICAgICAgICAgICAgc2V0U29sYXJNb2R1bGVzOiAoc29sYXJNb2R1bGVzKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTb2xhck1vZHVsZXMoc29sYXJNb2R1bGVzKSxcbiAgICAgICAgICAgICAgICAgICAgc2V0U29sYXJNb2R1bGVzRGlzYWJsZWQ6ICgpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFNvbGFyTW9kdWxlc0Rpc2FibGVkKCksXG4gICAgICAgICAgICAgICAgICAgIHZlcnRleENoYW5nZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJlc2V0UG9zaXRpb24oc2VnbWVudFZhbHVlLCBzaG93TWV0ZXJMYWJlbHMpIHtcbiAgICAgICAgICAgIGNvbnN0IHNoYXBlQ29vcmRzID0gc2VnbWVudFZhbHVlLnBvbHlnb24ubWFwKFxuICAgICAgICAgICAgICAgIChwb2ludCkgPT4gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhwb2ludFswXSwgcG9pbnRbMV0pXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgdGhpcy5zZXRQYXRocyhzaGFwZUNvb3Jkcyk7XG4gICAgICAgICAgICB0aGlzLnNldFNvbGFyTW9kdWxlcyhzZWdtZW50VmFsdWUuc29sYXJfbW9kdWxlcyk7XG4gICAgICAgICAgICB0aGlzLnNldExhYmVsc1Zpc2liaWxpdHkoZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5tZXRlckxhYmVscyA9IGNyZWF0ZU1ldGVyTGFiZWxQb3B1cChcbiAgICAgICAgICAgICAgICBzaGFwZUNvb3JkcyxcbiAgICAgICAgICAgICAgICBnb29nbGUsXG4gICAgICAgICAgICAgICAgc2hvd01ldGVyTGFiZWxzID8gdGhpcy5tYXAgOiBudWxsXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKHZlcnRleENoYW5nZSkge1xuICAgICAgICAgICAgICAgIGFkZFBhdGhMaXN0ZW5lcnMoe1xuICAgICAgICAgICAgICAgICAgICBnb29nbGUsXG4gICAgICAgICAgICAgICAgICAgIGlkOiB0aGlzLmlkLFxuICAgICAgICAgICAgICAgICAgICBtYXA6IHRoaXMubWFwLFxuICAgICAgICAgICAgICAgICAgICBtZXRlckxhYmVsczogdGhpcy5tZXRlckxhYmVscyxcbiAgICAgICAgICAgICAgICAgICAgcGF0aHM6IHRoaXMuZ2V0UGF0aHMoKSxcbiAgICAgICAgICAgICAgICAgICAgc2V0U29sYXJNb2R1bGVzOiAoc29sYXJNb2R1bGVzKSA9PlxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTb2xhck1vZHVsZXMoc29sYXJNb2R1bGVzKSxcbiAgICAgICAgICAgICAgICAgICAgc2V0U29sYXJNb2R1bGVzRGlzYWJsZWQ6ICgpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFNvbGFyTW9kdWxlc0Rpc2FibGVkKCksXG4gICAgICAgICAgICAgICAgICAgIHZlcnRleENoYW5nZSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHNldE1hcChtYXApIHtcbiAgICAgICAgICAgIGlmIChtYXAgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1ldGVyTGFiZWxzLmZvckVhY2goKG1ldGVyTGFiZWwpID0+XG4gICAgICAgICAgICAgICAgICAgIG1ldGVyTGFiZWwuc2V0TWFwKG51bGwpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1ldGVyTGFiZWxzID0gW107XG4gICAgICAgICAgICAgICAgdGhpcy5nZXRQYXRocygpLmZvckVhY2goKHBhdGgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuY2xlYXJMaXN0ZW5lcnMocGF0aCwgJ3JlbW92ZV9hdCcpO1xuICAgICAgICAgICAgICAgICAgICBnb29nbGUubWFwcy5ldmVudC5jbGVhckxpc3RlbmVycyhwYXRoLCAnc2V0X2F0Jyk7XG4gICAgICAgICAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmNsZWFyTGlzdGVuZXJzKHBhdGgsICdpbnNlcnRfYXQnKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnNvbGFyTW9kdWxlcy5mb3JFYWNoKChzb2xhck1vZHVsZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBzb2xhck1vZHVsZS5zZXRNYXAobnVsbCk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zb2xhck1vZHVsZXMgPSBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHN1cGVyLnNldE1hcChtYXApO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0U2VsZWN0ZWQoaXNTZWxjdGVkKSB7XG4gICAgICAgICAgICBpZiAoaXNTZWxjdGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICAgICAgZHJhZ2dhYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBlZGl0YWJsZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgZmlsbENvbG9yOiBTRUdNRU5UX0FDVElWRV9DT0xPUixcbiAgICAgICAgICAgICAgICAgICAgc3Ryb2tlQ29sb3I6IFNFR01FTlRfU1RST0tFX0FDVElWRV9DT0xPUixcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLm1ldGVyTGFiZWxzLmZvckVhY2goKG1ldGVyTGFiZWwpID0+XG4gICAgICAgICAgICAgICAgICAgIG1ldGVyTGFiZWwuc2V0TWFwKHRoaXMubWFwKVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0T3B0aW9ucyh7XG4gICAgICAgICAgICAgICAgICAgIGRyYWdnYWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgZmlsbENvbG9yOiBTRUdNRU5UX0NPTE9SLFxuICAgICAgICAgICAgICAgICAgICBzdHJva2VDb2xvcjogU0VHTUVOVF9TVFJPS0VfQ09MT1IsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhpcy5tZXRlckxhYmVscy5mb3JFYWNoKChtZXRlckxhYmVsKSA9PlxuICAgICAgICAgICAgICAgICAgICBtZXRlckxhYmVsLnNldE1hcChudWxsKVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBzZXREaXNhYmxlZChpc0Rpc2FibGVkKSB7XG4gICAgICAgICAgICB0aGlzLmRpc2FibGVkID0gaXNEaXNhYmxlZDtcbiAgICAgICAgICAgIHRoaXMuc2V0U29sYXJNb2R1bGVzRGlzYWJsZWQoaXNEaXNhYmxlZCk7XG4gICAgICAgICAgICBpZiAoaXNEaXNhYmxlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0T3B0aW9ucyh7XG4gICAgICAgICAgICAgICAgICAgIGNsaWNrYWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAwLjYsXG4gICAgICAgICAgICAgICAgICAgIHN0cm9rZU9wYWNpdHk6IDAuNixcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICAgICAgY2xpY2thYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICBmaWxsT3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICAgICAgc3Ryb2tlT3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHNldFNvbGFyTW9kdWxlcyhzb2xhck1vZHVsZXMpIHtcbiAgICAgICAgICAgIHRoaXMuc29sYXJNb2R1bGVzLmZvckVhY2goKHNvbGFyTW9kdWxlKSA9PiB7XG4gICAgICAgICAgICAgICAgc29sYXJNb2R1bGUuc2V0TWFwKG51bGwpO1xuICAgICAgICAgICAgICAgIGdvb2dsZS5tYXBzLmV2ZW50LmNsZWFyTGlzdGVuZXJzKHNvbGFyTW9kdWxlLCAnY2xpY2snKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdGhpcy5zb2xhck1vZHVsZXMgPSBzb2xhck1vZHVsZXMubWFwKChzb2xhck1vZHVsZSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgU29sYXJNb2R1bGVQb2x5Z29uKFxuICAgICAgICAgICAgICAgICAgICBzb2xhck1vZHVsZS5tYXAoKG1vZHVsZVBvaW50KSA9PiAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgbGF0OiBtb2R1bGVQb2ludFswXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxuZzogbW9kdWxlUG9pbnRbMV0sXG4gICAgICAgICAgICAgICAgICAgIH0pKSxcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXBcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBzZXRTb2xhck1vZHVsZXNEaXNhYmxlZChpc0Rpc2FibGVkID0gdHJ1ZSkge1xuICAgICAgICAgICAgdGhpcy5zb2xhck1vZHVsZXMuZm9yRWFjaCgoc29sYXJNb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICBzb2xhck1vZHVsZS5zZXREaXNhYmxlZChpc0Rpc2FibGVkKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0U29sYXJNb2R1bGVzRmlsbENvbG9yKGZpbGxDb2xvcikge1xuICAgICAgICAgICAgdGhpcy5zb2xhck1vZHVsZXMuZm9yRWFjaCgoc29sYXJNb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICBzb2xhck1vZHVsZS5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICAgICAgZmlsbENvbG9yLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBzZXRTb2xhck1vZHVsZXNTdHJva2VDb2xvcihzdHJva2VDb2xvcikge1xuICAgICAgICAgICAgdGhpcy5zb2xhck1vZHVsZXMuZm9yRWFjaCgoc29sYXJNb2R1bGUpID0+IHtcbiAgICAgICAgICAgICAgICBzb2xhck1vZHVsZS5zZXRPcHRpb25zKHtcbiAgICAgICAgICAgICAgICAgICAgc3Ryb2tlQ29sb3IsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHNldExhYmVsc1Zpc2liaWxpdHkoaXNWaXNpYmxlKSB7XG4gICAgICAgICAgICBpZiAoaXNWaXNpYmxlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5tZXRlckxhYmVscy5mb3JFYWNoKChtZXRlckxhYmVsKSA9PlxuICAgICAgICAgICAgICAgICAgICBtZXRlckxhYmVsLnNldE1hcCh0aGlzLm1hcClcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1ldGVyTGFiZWxzLmZvckVhY2goKG1ldGVyTGFiZWwpID0+XG4gICAgICAgICAgICAgICAgICAgIG1ldGVyTGFiZWwuc2V0TWFwKG51bGwpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBTZWdtZW50UG9seWdvbjtcbn07XG4iXX0=