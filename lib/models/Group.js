"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxOrm = require("redux-orm");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Group = /*#__PURE__*/function (_Model) {
  _inherits(Group, _Model);

  var _super = _createSuper(Group);

  function Group() {
    _classCallCheck(this, Group);

    return _super.apply(this, arguments);
  }

  return Group;
}(_reduxOrm.Model);

Group.modelName = 'Group';
Group.fields = {
  blocked: (0, _reduxOrm.attr)(),
  id: (0, _reduxOrm.attr)(),
  page: (0, _reduxOrm.attr)(),
  templateId: (0, _reduxOrm.fk)('Template', 'groups')
};
var _default = Group;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvR3JvdXAuanMiXSwibmFtZXMiOlsiR3JvdXAiLCJNb2RlbCIsIm1vZGVsTmFtZSIsImZpZWxkcyIsImJsb2NrZWQiLCJpZCIsInBhZ2UiLCJ0ZW1wbGF0ZUlkIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBRU1BLEs7Ozs7Ozs7Ozs7OztFQUFjQyxlOztBQUNwQkQsS0FBSyxDQUFDRSxTQUFOLEdBQWtCLE9BQWxCO0FBQ0FGLEtBQUssQ0FBQ0csTUFBTixHQUFlO0FBQ1hDLEVBQUFBLE9BQU8sRUFBRSxxQkFERTtBQUVYQyxFQUFBQSxFQUFFLEVBQUUscUJBRk87QUFHWEMsRUFBQUEsSUFBSSxFQUFFLHFCQUhLO0FBSVhDLEVBQUFBLFVBQVUsRUFBRSxrQkFBRyxVQUFILEVBQWUsUUFBZjtBQUpELENBQWY7ZUFNZVAsSyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIGRlZmF1bHQtY2FzZSAqL1xuaW1wb3J0IHsgTW9kZWwsIGZrLCBhdHRyIH0gZnJvbSAncmVkdXgtb3JtJztcblxuY2xhc3MgR3JvdXAgZXh0ZW5kcyBNb2RlbCB7fVxuR3JvdXAubW9kZWxOYW1lID0gJ0dyb3VwJztcbkdyb3VwLmZpZWxkcyA9IHtcbiAgICBibG9ja2VkOiBhdHRyKCksXG4gICAgaWQ6IGF0dHIoKSxcbiAgICBwYWdlOiBhdHRyKCksXG4gICAgdGVtcGxhdGVJZDogZmsoJ1RlbXBsYXRlJywgJ2dyb3VwcycpLFxufTtcbmV4cG9ydCBkZWZhdWx0IEdyb3VwO1xuIl19