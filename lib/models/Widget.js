"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxOrm = require("redux-orm");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Widget = /*#__PURE__*/function (_Model) {
  _inherits(Widget, _Model);

  var _super = _createSuper(Widget);

  function Widget() {
    _classCallCheck(this, Widget);

    return _super.apply(this, arguments);
  }

  return Widget;
}(_reduxOrm.Model);

Widget.modelName = 'Widget';
Widget.fields = {
  field: (0, _reduxOrm.attr)(),
  hasSummarySupport: (0, _reduxOrm.attr)(),
  height: (0, _reduxOrm.attr)(),
  name: (0, _reduxOrm.attr)(),
  num: (0, _reduxOrm.attr)(),
  order: (0, _reduxOrm.attr)(),
  pageId: (0, _reduxOrm.fk)('Page', 'widgets'),
  proposalNumber: (0, _reduxOrm.attr)(),
  replaceInfoRequired: (0, _reduxOrm.attr)(),
  style: (0, _reduxOrm.attr)(),
  supportVersion: (0, _reduxOrm.attr)(),
  value: (0, _reduxOrm.attr)(),
  width: (0, _reduxOrm.attr)(),
  x: (0, _reduxOrm.attr)(),
  y: (0, _reduxOrm.attr)()
};
var _default = Widget;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvV2lkZ2V0LmpzIl0sIm5hbWVzIjpbIldpZGdldCIsIk1vZGVsIiwibW9kZWxOYW1lIiwiZmllbGRzIiwiZmllbGQiLCJoYXNTdW1tYXJ5U3VwcG9ydCIsImhlaWdodCIsIm5hbWUiLCJudW0iLCJvcmRlciIsInBhZ2VJZCIsInByb3Bvc2FsTnVtYmVyIiwicmVwbGFjZUluZm9SZXF1aXJlZCIsInN0eWxlIiwic3VwcG9ydFZlcnNpb24iLCJ2YWx1ZSIsIndpZHRoIiwieCIsInkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7SUFFTUEsTTs7Ozs7Ozs7Ozs7O0VBQWVDLGU7O0FBQ3JCRCxNQUFNLENBQUNFLFNBQVAsR0FBbUIsUUFBbkI7QUFDQUYsTUFBTSxDQUFDRyxNQUFQLEdBQWdCO0FBQ1pDLEVBQUFBLEtBQUssRUFBRSxxQkFESztBQUVaQyxFQUFBQSxpQkFBaUIsRUFBRSxxQkFGUDtBQUdaQyxFQUFBQSxNQUFNLEVBQUUscUJBSEk7QUFJWkMsRUFBQUEsSUFBSSxFQUFFLHFCQUpNO0FBS1pDLEVBQUFBLEdBQUcsRUFBRSxxQkFMTztBQU1aQyxFQUFBQSxLQUFLLEVBQUUscUJBTks7QUFPWkMsRUFBQUEsTUFBTSxFQUFFLGtCQUFHLE1BQUgsRUFBVyxTQUFYLENBUEk7QUFRWkMsRUFBQUEsY0FBYyxFQUFFLHFCQVJKO0FBU1pDLEVBQUFBLG1CQUFtQixFQUFFLHFCQVRUO0FBVVpDLEVBQUFBLEtBQUssRUFBRSxxQkFWSztBQVdaQyxFQUFBQSxjQUFjLEVBQUUscUJBWEo7QUFZWkMsRUFBQUEsS0FBSyxFQUFFLHFCQVpLO0FBYVpDLEVBQUFBLEtBQUssRUFBRSxxQkFiSztBQWNaQyxFQUFBQSxDQUFDLEVBQUUscUJBZFM7QUFlWkMsRUFBQUEsQ0FBQyxFQUFFO0FBZlMsQ0FBaEI7ZUFpQmVsQixNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kZWwsIGZrLCBhdHRyIH0gZnJvbSAncmVkdXgtb3JtJztcblxuY2xhc3MgV2lkZ2V0IGV4dGVuZHMgTW9kZWwge31cbldpZGdldC5tb2RlbE5hbWUgPSAnV2lkZ2V0JztcbldpZGdldC5maWVsZHMgPSB7XG4gICAgZmllbGQ6IGF0dHIoKSxcbiAgICBoYXNTdW1tYXJ5U3VwcG9ydDogYXR0cigpLFxuICAgIGhlaWdodDogYXR0cigpLFxuICAgIG5hbWU6IGF0dHIoKSxcbiAgICBudW06IGF0dHIoKSxcbiAgICBvcmRlcjogYXR0cigpLFxuICAgIHBhZ2VJZDogZmsoJ1BhZ2UnLCAnd2lkZ2V0cycpLFxuICAgIHByb3Bvc2FsTnVtYmVyOiBhdHRyKCksXG4gICAgcmVwbGFjZUluZm9SZXF1aXJlZDogYXR0cigpLFxuICAgIHN0eWxlOiBhdHRyKCksXG4gICAgc3VwcG9ydFZlcnNpb246IGF0dHIoKSxcbiAgICB2YWx1ZTogYXR0cigpLFxuICAgIHdpZHRoOiBhdHRyKCksXG4gICAgeDogYXR0cigpLFxuICAgIHk6IGF0dHIoKSxcbn07XG5leHBvcnQgZGVmYXVsdCBXaWRnZXQ7XG4iXX0=