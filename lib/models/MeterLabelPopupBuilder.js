"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _default = function _default(google, map) {
  var MeterLabelPopup = /*#__PURE__*/function (_google$maps$OverlayV) {
    _inherits(MeterLabelPopup, _google$maps$OverlayV);

    var _super = _createSuper(MeterLabelPopup);

    function MeterLabelPopup() {
      var _this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      _classCallCheck(this, MeterLabelPopup);

      var _options$position = options.position,
          position = _options$position === void 0 ? null : _options$position,
          _options$text = options.text,
          text = _options$text === void 0 ? '' : _options$text,
          _options$rotate = options.rotate,
          rotate = _options$rotate === void 0 ? 0 : _options$rotate;
      _this = _super.call(this, _objectSpread(_objectSpread({}, options), {}, {
        zIndex: 1
      }));

      _this.setMap(map);

      _this.position = position;
      _this.rotate = rotate;
      _this.textSpan = document.createElement('span');
      _this.textSpan.innerHTML = text;
      var bubbleAnchor = document.createElement('div');
      bubbleAnchor.classList.add('popup-bubble-anchor');
      bubbleAnchor.appendChild(_this.textSpan);
      _this.containerDiv = document.createElement('div');

      _this.containerDiv.classList.add('popup-container');

      _this.containerDiv.appendChild(bubbleAnchor);

      google.maps.OverlayView.preventMapHitsAndGesturesFrom(_this.containerDiv);
      return _this;
    }

    _createClass(MeterLabelPopup, [{
      key: "setPosition",
      value: function setPosition(position) {
        this.position = position;
      }
    }, {
      key: "setRotation",
      value: function setRotation(rotation) {
        this.rotate = rotation;
      }
    }, {
      key: "setText",
      value: function setText(text) {
        this.textSpan.innerHTML = text;
      }
    }, {
      key: "onAdd",
      value: function onAdd() {
        this.getPanes().floatPane.appendChild(this.containerDiv);
      }
    }, {
      key: "onRemove",
      value: function onRemove() {
        if (this.containerDiv.parentElement) {
          this.containerDiv.parentElement.removeChild(this.containerDiv);
        }
      }
    }, {
      key: "draw",
      value: function draw() {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        var display = Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ? 'block' : 'none';

        if (display === 'block') {
          this.containerDiv.style.left = divPosition.x + 'px';
          this.containerDiv.style.top = divPosition.y + 'px';
        }

        if (this.containerDiv.style.display !== display) {
          this.containerDiv.style.display = display;
        }

        this.containerDiv.style.transform = "rotate(".concat(this.rotate, "deg)");
      }
    }]);

    return MeterLabelPopup;
  }(google.maps.OverlayView);

  return MeterLabelPopup;
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvTWV0ZXJMYWJlbFBvcHVwQnVpbGRlci5qcyJdLCJuYW1lcyI6WyJnb29nbGUiLCJtYXAiLCJNZXRlckxhYmVsUG9wdXAiLCJvcHRpb25zIiwicG9zaXRpb24iLCJ0ZXh0Iiwicm90YXRlIiwiekluZGV4Iiwic2V0TWFwIiwidGV4dFNwYW4iLCJkb2N1bWVudCIsImNyZWF0ZUVsZW1lbnQiLCJpbm5lckhUTUwiLCJidWJibGVBbmNob3IiLCJjbGFzc0xpc3QiLCJhZGQiLCJhcHBlbmRDaGlsZCIsImNvbnRhaW5lckRpdiIsIm1hcHMiLCJPdmVybGF5VmlldyIsInByZXZlbnRNYXBIaXRzQW5kR2VzdHVyZXNGcm9tIiwicm90YXRpb24iLCJnZXRQYW5lcyIsImZsb2F0UGFuZSIsInBhcmVudEVsZW1lbnQiLCJyZW1vdmVDaGlsZCIsImRpdlBvc2l0aW9uIiwiZ2V0UHJvamVjdGlvbiIsImZyb21MYXRMbmdUb0RpdlBpeGVsIiwiZGlzcGxheSIsIk1hdGgiLCJhYnMiLCJ4IiwieSIsInN0eWxlIiwibGVmdCIsInRvcCIsInRyYW5zZm9ybSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7ZUFBZSxrQkFBQ0EsTUFBRCxFQUFTQyxHQUFULEVBQWlCO0FBQUEsTUFDdEJDLGVBRHNCO0FBQUE7O0FBQUE7O0FBRXhCLCtCQUEwQjtBQUFBOztBQUFBLFVBQWRDLE9BQWMsdUVBQUosRUFBSTs7QUFBQTs7QUFDdEIsOEJBQW1EQSxPQUFuRCxDQUFRQyxRQUFSO0FBQUEsVUFBUUEsUUFBUixrQ0FBbUIsSUFBbkI7QUFBQSwwQkFBbURELE9BQW5ELENBQXlCRSxJQUF6QjtBQUFBLFVBQXlCQSxJQUF6Qiw4QkFBZ0MsRUFBaEM7QUFBQSw0QkFBbURGLE9BQW5ELENBQW9DRyxNQUFwQztBQUFBLFVBQW9DQSxNQUFwQyxnQ0FBNkMsQ0FBN0M7QUFDQSxnRUFBV0gsT0FBWDtBQUFvQkksUUFBQUEsTUFBTSxFQUFFO0FBQTVCOztBQUNBLFlBQUtDLE1BQUwsQ0FBWVAsR0FBWjs7QUFDQSxZQUFLRyxRQUFMLEdBQWdCQSxRQUFoQjtBQUNBLFlBQUtFLE1BQUwsR0FBY0EsTUFBZDtBQUNBLFlBQUtHLFFBQUwsR0FBZ0JDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixNQUF2QixDQUFoQjtBQUNBLFlBQUtGLFFBQUwsQ0FBY0csU0FBZCxHQUEwQlAsSUFBMUI7QUFDQSxVQUFJUSxZQUFZLEdBQUdILFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFuQjtBQUNBRSxNQUFBQSxZQUFZLENBQUNDLFNBQWIsQ0FBdUJDLEdBQXZCLENBQTJCLHFCQUEzQjtBQUNBRixNQUFBQSxZQUFZLENBQUNHLFdBQWIsQ0FBeUIsTUFBS1AsUUFBOUI7QUFFQSxZQUFLUSxZQUFMLEdBQW9CUCxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBcEI7O0FBQ0EsWUFBS00sWUFBTCxDQUFrQkgsU0FBbEIsQ0FBNEJDLEdBQTVCLENBQWdDLGlCQUFoQzs7QUFDQSxZQUFLRSxZQUFMLENBQWtCRCxXQUFsQixDQUE4QkgsWUFBOUI7O0FBRUFiLE1BQUFBLE1BQU0sQ0FBQ2tCLElBQVAsQ0FBWUMsV0FBWixDQUF3QkMsNkJBQXhCLENBQ0ksTUFBS0gsWUFEVDtBQWhCc0I7QUFtQnpCOztBQXJCdUI7QUFBQTtBQUFBLGFBdUJ4QixxQkFBWWIsUUFBWixFQUFzQjtBQUNsQixhQUFLQSxRQUFMLEdBQWdCQSxRQUFoQjtBQUNIO0FBekJ1QjtBQUFBO0FBQUEsYUEyQnhCLHFCQUFZaUIsUUFBWixFQUFzQjtBQUNsQixhQUFLZixNQUFMLEdBQWNlLFFBQWQ7QUFDSDtBQTdCdUI7QUFBQTtBQUFBLGFBK0J4QixpQkFBUWhCLElBQVIsRUFBYztBQUNWLGFBQUtJLFFBQUwsQ0FBY0csU0FBZCxHQUEwQlAsSUFBMUI7QUFDSDtBQWpDdUI7QUFBQTtBQUFBLGFBbUN4QixpQkFBUTtBQUNKLGFBQUtpQixRQUFMLEdBQWdCQyxTQUFoQixDQUEwQlAsV0FBMUIsQ0FBc0MsS0FBS0MsWUFBM0M7QUFDSDtBQXJDdUI7QUFBQTtBQUFBLGFBdUN4QixvQkFBVztBQUNQLFlBQUksS0FBS0EsWUFBTCxDQUFrQk8sYUFBdEIsRUFBcUM7QUFDakMsZUFBS1AsWUFBTCxDQUFrQk8sYUFBbEIsQ0FBZ0NDLFdBQWhDLENBQTRDLEtBQUtSLFlBQWpEO0FBQ0g7QUFDSjtBQTNDdUI7QUFBQTtBQUFBLGFBNkN4QixnQkFBTztBQUNILFlBQU1TLFdBQVcsR0FBRyxLQUFLQyxhQUFMLEdBQXFCQyxvQkFBckIsQ0FDaEIsS0FBS3hCLFFBRFcsQ0FBcEI7QUFJQSxZQUFNeUIsT0FBTyxHQUNUQyxJQUFJLENBQUNDLEdBQUwsQ0FBU0wsV0FBVyxDQUFDTSxDQUFyQixJQUEwQixJQUExQixJQUFrQ0YsSUFBSSxDQUFDQyxHQUFMLENBQVNMLFdBQVcsQ0FBQ08sQ0FBckIsSUFBMEIsSUFBNUQsR0FDTSxPQUROLEdBRU0sTUFIVjs7QUFLQSxZQUFJSixPQUFPLEtBQUssT0FBaEIsRUFBeUI7QUFDckIsZUFBS1osWUFBTCxDQUFrQmlCLEtBQWxCLENBQXdCQyxJQUF4QixHQUErQlQsV0FBVyxDQUFDTSxDQUFaLEdBQWdCLElBQS9DO0FBQ0EsZUFBS2YsWUFBTCxDQUFrQmlCLEtBQWxCLENBQXdCRSxHQUF4QixHQUE4QlYsV0FBVyxDQUFDTyxDQUFaLEdBQWdCLElBQTlDO0FBQ0g7O0FBQ0QsWUFBSSxLQUFLaEIsWUFBTCxDQUFrQmlCLEtBQWxCLENBQXdCTCxPQUF4QixLQUFvQ0EsT0FBeEMsRUFBaUQ7QUFDN0MsZUFBS1osWUFBTCxDQUFrQmlCLEtBQWxCLENBQXdCTCxPQUF4QixHQUFrQ0EsT0FBbEM7QUFDSDs7QUFDRCxhQUFLWixZQUFMLENBQWtCaUIsS0FBbEIsQ0FBd0JHLFNBQXhCLG9CQUE4QyxLQUFLL0IsTUFBbkQ7QUFDSDtBQS9EdUI7O0FBQUE7QUFBQSxJQUNFTixNQUFNLENBQUNrQixJQUFQLENBQVlDLFdBRGQ7O0FBaUU1QixTQUFPakIsZUFBUDtBQUNILEMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCAoZ29vZ2xlLCBtYXApID0+IHtcbiAgICBjbGFzcyBNZXRlckxhYmVsUG9wdXAgZXh0ZW5kcyBnb29nbGUubWFwcy5PdmVybGF5VmlldyB7XG4gICAgICAgIGNvbnN0cnVjdG9yKG9wdGlvbnMgPSB7fSkge1xuICAgICAgICAgICAgY29uc3QgeyBwb3NpdGlvbiA9IG51bGwsIHRleHQgPSAnJywgcm90YXRlID0gMCB9ID0gb3B0aW9ucztcbiAgICAgICAgICAgIHN1cGVyKHsgLi4ub3B0aW9ucywgekluZGV4OiAxIH0pO1xuICAgICAgICAgICAgdGhpcy5zZXRNYXAobWFwKTtcbiAgICAgICAgICAgIHRoaXMucG9zaXRpb24gPSBwb3NpdGlvbjtcbiAgICAgICAgICAgIHRoaXMucm90YXRlID0gcm90YXRlO1xuICAgICAgICAgICAgdGhpcy50ZXh0U3BhbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcbiAgICAgICAgICAgIHRoaXMudGV4dFNwYW4uaW5uZXJIVE1MID0gdGV4dDtcbiAgICAgICAgICAgIGxldCBidWJibGVBbmNob3IgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICAgIGJ1YmJsZUFuY2hvci5jbGFzc0xpc3QuYWRkKCdwb3B1cC1idWJibGUtYW5jaG9yJyk7XG4gICAgICAgICAgICBidWJibGVBbmNob3IuYXBwZW5kQ2hpbGQodGhpcy50ZXh0U3Bhbik7XG5cbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICB0aGlzLmNvbnRhaW5lckRpdi5jbGFzc0xpc3QuYWRkKCdwb3B1cC1jb250YWluZXInKTtcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2LmFwcGVuZENoaWxkKGJ1YmJsZUFuY2hvcik7XG5cbiAgICAgICAgICAgIGdvb2dsZS5tYXBzLk92ZXJsYXlWaWV3LnByZXZlbnRNYXBIaXRzQW5kR2VzdHVyZXNGcm9tKFxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2XG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0UG9zaXRpb24ocG9zaXRpb24pIHtcbiAgICAgICAgICAgIHRoaXMucG9zaXRpb24gPSBwb3NpdGlvbjtcbiAgICAgICAgfVxuXG4gICAgICAgIHNldFJvdGF0aW9uKHJvdGF0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLnJvdGF0ZSA9IHJvdGF0aW9uO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0VGV4dCh0ZXh0KSB7XG4gICAgICAgICAgICB0aGlzLnRleHRTcGFuLmlubmVySFRNTCA9IHRleHQ7XG4gICAgICAgIH1cblxuICAgICAgICBvbkFkZCgpIHtcbiAgICAgICAgICAgIHRoaXMuZ2V0UGFuZXMoKS5mbG9hdFBhbmUuYXBwZW5kQ2hpbGQodGhpcy5jb250YWluZXJEaXYpO1xuICAgICAgICB9XG5cbiAgICAgICAgb25SZW1vdmUoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5jb250YWluZXJEaXYucGFyZW50RWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2LnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodGhpcy5jb250YWluZXJEaXYpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZHJhdygpIHtcbiAgICAgICAgICAgIGNvbnN0IGRpdlBvc2l0aW9uID0gdGhpcy5nZXRQcm9qZWN0aW9uKCkuZnJvbUxhdExuZ1RvRGl2UGl4ZWwoXG4gICAgICAgICAgICAgICAgdGhpcy5wb3NpdGlvblxuICAgICAgICAgICAgKTtcblxuICAgICAgICAgICAgY29uc3QgZGlzcGxheSA9XG4gICAgICAgICAgICAgICAgTWF0aC5hYnMoZGl2UG9zaXRpb24ueCkgPCA0MDAwICYmIE1hdGguYWJzKGRpdlBvc2l0aW9uLnkpIDwgNDAwMFxuICAgICAgICAgICAgICAgICAgICA/ICdibG9jaydcbiAgICAgICAgICAgICAgICAgICAgOiAnbm9uZSc7XG5cbiAgICAgICAgICAgIGlmIChkaXNwbGF5ID09PSAnYmxvY2snKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb250YWluZXJEaXYuc3R5bGUubGVmdCA9IGRpdlBvc2l0aW9uLnggKyAncHgnO1xuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2LnN0eWxlLnRvcCA9IGRpdlBvc2l0aW9uLnkgKyAncHgnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuY29udGFpbmVyRGl2LnN0eWxlLmRpc3BsYXkgIT09IGRpc3BsYXkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lckRpdi5zdHlsZS5kaXNwbGF5ID0gZGlzcGxheTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRGl2LnN0eWxlLnRyYW5zZm9ybSA9IGByb3RhdGUoJHt0aGlzLnJvdGF0ZX1kZWcpYDtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gTWV0ZXJMYWJlbFBvcHVwO1xufTtcbiJdfQ==