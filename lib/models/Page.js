"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxOrm = require("redux-orm");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Page = /*#__PURE__*/function (_Model) {
  _inherits(Page, _Model);

  var _super = _createSuper(Page);

  function Page() {
    _classCallCheck(this, Page);

    return _super.apply(this, arguments);
  }

  return Page;
}(_reduxOrm.Model);

exports.default = Page;
Page.modelName = 'Page';
Page.fields = {
  blocked: (0, _reduxOrm.attr)(),
  content: (0, _reduxOrm.attr)(),
  company: (0, _reduxOrm.attr)(),
  created_at: (0, _reduxOrm.attr)(),
  custom_template: (0, _reduxOrm.attr)(),
  grouped_by: (0, _reduxOrm.attr)(),
  id: (0, _reduxOrm.attr)(),
  infiniteMode: (0, _reduxOrm.attr)(),
  orientation: (0, _reduxOrm.attr)(),
  page: (0, _reduxOrm.attr)(),
  page_parent: (0, _reduxOrm.attr)(),
  parsedDataKey: (0, _reduxOrm.attr)(),
  replaceInfo: (0, _reduxOrm.attr)(),
  theme: (0, _reduxOrm.attr)(),
  updated_at: (0, _reduxOrm.attr)(),
  templateId: (0, _reduxOrm.fk)('Template', 'templatePages'),
  groupId: (0, _reduxOrm.fk)('Group', 'groupPages')
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvUGFnZS5qcyJdLCJuYW1lcyI6WyJQYWdlIiwiTW9kZWwiLCJtb2RlbE5hbWUiLCJmaWVsZHMiLCJibG9ja2VkIiwiY29udGVudCIsImNvbXBhbnkiLCJjcmVhdGVkX2F0IiwiY3VzdG9tX3RlbXBsYXRlIiwiZ3JvdXBlZF9ieSIsImlkIiwiaW5maW5pdGVNb2RlIiwib3JpZW50YXRpb24iLCJwYWdlIiwicGFnZV9wYXJlbnQiLCJwYXJzZWREYXRhS2V5IiwicmVwbGFjZUluZm8iLCJ0aGVtZSIsInVwZGF0ZWRfYXQiLCJ0ZW1wbGF0ZUlkIiwiZ3JvdXBJZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUVxQkEsSTs7Ozs7Ozs7Ozs7O0VBQWFDLGU7OztBQUNsQ0QsSUFBSSxDQUFDRSxTQUFMLEdBQWlCLE1BQWpCO0FBQ0FGLElBQUksQ0FBQ0csTUFBTCxHQUFjO0FBQ1ZDLEVBQUFBLE9BQU8sRUFBRSxxQkFEQztBQUVWQyxFQUFBQSxPQUFPLEVBQUUscUJBRkM7QUFHVkMsRUFBQUEsT0FBTyxFQUFFLHFCQUhDO0FBSVZDLEVBQUFBLFVBQVUsRUFBRSxxQkFKRjtBQUtWQyxFQUFBQSxlQUFlLEVBQUUscUJBTFA7QUFNVkMsRUFBQUEsVUFBVSxFQUFFLHFCQU5GO0FBT1ZDLEVBQUFBLEVBQUUsRUFBRSxxQkFQTTtBQVFWQyxFQUFBQSxZQUFZLEVBQUUscUJBUko7QUFTVkMsRUFBQUEsV0FBVyxFQUFFLHFCQVRIO0FBVVZDLEVBQUFBLElBQUksRUFBRSxxQkFWSTtBQVdWQyxFQUFBQSxXQUFXLEVBQUUscUJBWEg7QUFZVkMsRUFBQUEsYUFBYSxFQUFFLHFCQVpMO0FBYVZDLEVBQUFBLFdBQVcsRUFBRSxxQkFiSDtBQWNWQyxFQUFBQSxLQUFLLEVBQUUscUJBZEc7QUFlVkMsRUFBQUEsVUFBVSxFQUFFLHFCQWZGO0FBZ0JWQyxFQUFBQSxVQUFVLEVBQUUsa0JBQUcsVUFBSCxFQUFlLGVBQWYsQ0FoQkY7QUFpQlZDLEVBQUFBLE9BQU8sRUFBRSxrQkFBRyxPQUFILEVBQVksWUFBWjtBQWpCQyxDQUFkIiwic291cmNlc0NvbnRlbnQiOlsiLyogZXNsaW50LWRpc2FibGUgZGVmYXVsdC1jYXNlICovXG5pbXBvcnQgeyBNb2RlbCwgYXR0ciwgZmsgfSBmcm9tICdyZWR1eC1vcm0nO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQYWdlIGV4dGVuZHMgTW9kZWwge31cblBhZ2UubW9kZWxOYW1lID0gJ1BhZ2UnO1xuUGFnZS5maWVsZHMgPSB7XG4gICAgYmxvY2tlZDogYXR0cigpLFxuICAgIGNvbnRlbnQ6IGF0dHIoKSxcbiAgICBjb21wYW55OiBhdHRyKCksXG4gICAgY3JlYXRlZF9hdDogYXR0cigpLFxuICAgIGN1c3RvbV90ZW1wbGF0ZTogYXR0cigpLFxuICAgIGdyb3VwZWRfYnk6IGF0dHIoKSxcbiAgICBpZDogYXR0cigpLFxuICAgIGluZmluaXRlTW9kZTogYXR0cigpLFxuICAgIG9yaWVudGF0aW9uOiBhdHRyKCksXG4gICAgcGFnZTogYXR0cigpLFxuICAgIHBhZ2VfcGFyZW50OiBhdHRyKCksXG4gICAgcGFyc2VkRGF0YUtleTogYXR0cigpLFxuICAgIHJlcGxhY2VJbmZvOiBhdHRyKCksXG4gICAgdGhlbWU6IGF0dHIoKSxcbiAgICB1cGRhdGVkX2F0OiBhdHRyKCksXG4gICAgdGVtcGxhdGVJZDogZmsoJ1RlbXBsYXRlJywgJ3RlbXBsYXRlUGFnZXMnKSxcbiAgICBncm91cElkOiBmaygnR3JvdXAnLCAnZ3JvdXBQYWdlcycpLFxufTtcbiJdfQ==