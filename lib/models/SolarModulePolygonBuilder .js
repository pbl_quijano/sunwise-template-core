"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _maps = require("../constants/maps");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var _default = function _default(google) {
  var SolarModulePolygon = /*#__PURE__*/function (_google$maps$Polygon) {
    _inherits(SolarModulePolygon, _google$maps$Polygon);

    var _super = _createSuper(SolarModulePolygon);

    function SolarModulePolygon(paths, map) {
      var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
          _ref$strokeColor = _ref.strokeColor,
          strokeColor = _ref$strokeColor === void 0 ? _maps.SOLAR_MODULE_STROKE_COLOR : _ref$strokeColor,
          _ref$fillColor = _ref.fillColor,
          fillColor = _ref$fillColor === void 0 ? _maps.SOLAR_MODULE_COLOR : _ref$fillColor;

      _classCallCheck(this, SolarModulePolygon);

      return _super.call(this, {
        clickable: false,
        paths: paths,
        strokeColor: strokeColor,
        fillColor: fillColor,
        fillOpacity: 1,
        strokePosition: google.maps.StrokePosition.INSIDE,
        map: map,
        strokeWeight: 1,
        zIndex: 3
      });
    }

    _createClass(SolarModulePolygon, [{
      key: "setDisabled",
      value: function setDisabled() {
        var isDisabled = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

        if (isDisabled) {
          this.setOptions({
            fillOpacity: 0.2,
            strokeWeight: 0
          });
        } else {
          this.setOptions({
            fillOpacity: 1,
            strokeWeight: 1
          });
        }
      }
    }]);

    return SolarModulePolygon;
  }(google.maps.Polygon);

  return SolarModulePolygon;
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9tb2RlbHMvU29sYXJNb2R1bGVQb2x5Z29uQnVpbGRlciAuanMiXSwibmFtZXMiOlsiZ29vZ2xlIiwiU29sYXJNb2R1bGVQb2x5Z29uIiwicGF0aHMiLCJtYXAiLCJzdHJva2VDb2xvciIsIlNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IiLCJmaWxsQ29sb3IiLCJTT0xBUl9NT0RVTEVfQ09MT1IiLCJjbGlja2FibGUiLCJmaWxsT3BhY2l0eSIsInN0cm9rZVBvc2l0aW9uIiwibWFwcyIsIlN0cm9rZVBvc2l0aW9uIiwiSU5TSURFIiwic3Ryb2tlV2VpZ2h0IiwiekluZGV4IiwiaXNEaXNhYmxlZCIsInNldE9wdGlvbnMiLCJQb2x5Z29uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztlQUVlLGtCQUFDQSxNQUFELEVBQVk7QUFBQSxNQUNqQkMsa0JBRGlCO0FBQUE7O0FBQUE7O0FBRW5CLGdDQUNJQyxLQURKLEVBRUlDLEdBRkosRUFPRTtBQUFBLHFGQURNLEVBQ047QUFBQSxrQ0FITUMsV0FHTjtBQUFBLFVBSE1BLFdBR04saUNBSG9CQywrQkFHcEI7QUFBQSxnQ0FGTUMsU0FFTjtBQUFBLFVBRk1BLFNBRU4sK0JBRmtCQyx3QkFFbEI7O0FBQUE7O0FBQUEsK0JBQ1E7QUFDRkMsUUFBQUEsU0FBUyxFQUFFLEtBRFQ7QUFFRk4sUUFBQUEsS0FBSyxFQUFMQSxLQUZFO0FBR0ZFLFFBQUFBLFdBQVcsRUFBWEEsV0FIRTtBQUlGRSxRQUFBQSxTQUFTLEVBQVRBLFNBSkU7QUFLRkcsUUFBQUEsV0FBVyxFQUFFLENBTFg7QUFNRkMsUUFBQUEsY0FBYyxFQUFFVixNQUFNLENBQUNXLElBQVAsQ0FBWUMsY0FBWixDQUEyQkMsTUFOekM7QUFPRlYsUUFBQUEsR0FBRyxFQUFIQSxHQVBFO0FBUUZXLFFBQUFBLFlBQVksRUFBRSxDQVJaO0FBU0ZDLFFBQUFBLE1BQU0sRUFBRTtBQVROLE9BRFI7QUFZRDs7QUFyQmtCO0FBQUE7QUFBQSxhQXNCbkIsdUJBQStCO0FBQUEsWUFBbkJDLFVBQW1CLHVFQUFOLElBQU07O0FBQzNCLFlBQUlBLFVBQUosRUFBZ0I7QUFDWixlQUFLQyxVQUFMLENBQWdCO0FBQ1pSLFlBQUFBLFdBQVcsRUFBRSxHQUREO0FBRVpLLFlBQUFBLFlBQVksRUFBRTtBQUZGLFdBQWhCO0FBSUgsU0FMRCxNQUtPO0FBQ0gsZUFBS0csVUFBTCxDQUFnQjtBQUNaUixZQUFBQSxXQUFXLEVBQUUsQ0FERDtBQUVaSyxZQUFBQSxZQUFZLEVBQUU7QUFGRixXQUFoQjtBQUlIO0FBQ0o7QUFsQ2tCOztBQUFBO0FBQUEsSUFDVWQsTUFBTSxDQUFDVyxJQUFQLENBQVlPLE9BRHRCOztBQXFDdkIsU0FBT2pCLGtCQUFQO0FBQ0gsQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNPTEFSX01PRFVMRV9DT0xPUiwgU09MQVJfTU9EVUxFX1NUUk9LRV9DT0xPUiB9IGZyb20gJ0Bjb25zdGFudHMvbWFwcyc7XG5cbmV4cG9ydCBkZWZhdWx0IChnb29nbGUpID0+IHtcbiAgICBjbGFzcyBTb2xhck1vZHVsZVBvbHlnb24gZXh0ZW5kcyBnb29nbGUubWFwcy5Qb2x5Z29uIHtcbiAgICAgICAgY29uc3RydWN0b3IoXG4gICAgICAgICAgICBwYXRocyxcbiAgICAgICAgICAgIG1hcCxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdHJva2VDb2xvciA9IFNPTEFSX01PRFVMRV9TVFJPS0VfQ09MT1IsXG4gICAgICAgICAgICAgICAgZmlsbENvbG9yID0gU09MQVJfTU9EVUxFX0NPTE9SLFxuICAgICAgICAgICAgfSA9IHt9XG4gICAgICAgICkge1xuICAgICAgICAgICAgc3VwZXIoe1xuICAgICAgICAgICAgICAgIGNsaWNrYWJsZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgcGF0aHMsXG4gICAgICAgICAgICAgICAgc3Ryb2tlQ29sb3IsXG4gICAgICAgICAgICAgICAgZmlsbENvbG9yLFxuICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIHN0cm9rZVBvc2l0aW9uOiBnb29nbGUubWFwcy5TdHJva2VQb3NpdGlvbi5JTlNJREUsXG4gICAgICAgICAgICAgICAgbWFwLFxuICAgICAgICAgICAgICAgIHN0cm9rZVdlaWdodDogMSxcbiAgICAgICAgICAgICAgICB6SW5kZXg6IDMsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBzZXREaXNhYmxlZChpc0Rpc2FibGVkID0gdHJ1ZSkge1xuICAgICAgICAgICAgaWYgKGlzRGlzYWJsZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldE9wdGlvbnMoe1xuICAgICAgICAgICAgICAgICAgICBmaWxsT3BhY2l0eTogMC4yLFxuICAgICAgICAgICAgICAgICAgICBzdHJva2VXZWlnaHQ6IDAsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0T3B0aW9ucyh7XG4gICAgICAgICAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgICAgICBzdHJva2VXZWlnaHQ6IDEsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gU29sYXJNb2R1bGVQb2x5Z29uO1xufTtcbiJdfQ==