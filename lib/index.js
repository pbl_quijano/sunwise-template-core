"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "PageProperties", {
  enumerable: true,
  get: function get() {
    return _PageProperties.default;
  }
});
Object.defineProperty(exports, "PageThumbnail", {
  enumerable: true,
  get: function get() {
    return _PageThumbnail.default;
  }
});
Object.defineProperty(exports, "PageToolbar", {
  enumerable: true,
  get: function get() {
    return _PageToolbar.default;
  }
});
Object.defineProperty(exports, "TemplateView", {
  enumerable: true,
  get: function get() {
    return _TemplateView.default;
  }
});
Object.defineProperty(exports, "WidgetMenu", {
  enumerable: true,
  get: function get() {
    return _WidgetMenu.default;
  }
});
Object.defineProperty(exports, "WidgetSideMenu", {
  enumerable: true,
  get: function get() {
    return _WidgetSideMenu.default;
  }
});
exports.editionLevels = void 0;
Object.defineProperty(exports, "reducer", {
  enumerable: true,
  get: function get() {
    return _reducer.default;
  }
});
exports.types = void 0;
Object.defineProperty(exports, "withTemplateCore", {
  enumerable: true,
  get: function get() {
    return _withTemplateCore.default;
  }
});
Object.defineProperty(exports, "withTemplatePages", {
  enumerable: true,
  get: function get() {
    return _withTemplatePages.default;
  }
});

var _types = require("./constants/types");

var _withTemplateCore = _interopRequireDefault(require("./hocs/withTemplateCore"));

var _withTemplatePages = _interopRequireDefault(require("./hocs/withTemplatePages"));

var _TemplateView = _interopRequireDefault(require("./modules/TemplateView"));

var _PageProperties = _interopRequireDefault(require("./modules/PageProperties"));

var _PageThumbnail = _interopRequireDefault(require("./modules/TemplateView/components/pages/PageThumbnail"));

var _PageToolbar = _interopRequireDefault(require("./modules/TemplateView/components/PageToolbar"));

var _reducer = _interopRequireDefault(require("./reducer"));

var _WidgetMenu = _interopRequireDefault(require("./modules/WidgetMenu"));

var _WidgetSideMenu = _interopRequireDefault(require("./components/WidgetSideMenu"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var types = {
  ONE_PROPOSAL_TYPE: _types.ONE_PROPOSAL_TYPE,
  MULTIPROPOSAL_TYPE: _types.MULTIPROPOSAL_TYPE,
  SMART_DOCUMENTS_TYPE: _types.SMART_DOCUMENTS_TYPE
};
exports.types = types;
var editionLevels = {
  NO_EDITION_MODE: _types.NO_EDITION_MODE,
  PARTIAL_EDITION_MODE: _types.PARTIAL_EDITION_MODE,
  FULL_EDITION_MODE: _types.FULL_EDITION_MODE
};
exports.editionLevels = editionLevels;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6WyJ0eXBlcyIsIk9ORV9QUk9QT1NBTF9UWVBFIiwiTVVMVElQUk9QT1NBTF9UWVBFIiwiU01BUlRfRE9DVU1FTlRTX1RZUEUiLCJlZGl0aW9uTGV2ZWxzIiwiTk9fRURJVElPTl9NT0RFIiwiUEFSVElBTF9FRElUSU9OX01PREUiLCJGVUxMX0VESVRJT05fTU9ERSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7QUFTQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7QUFDQTs7OztBQUNPLElBQU1BLEtBQUssR0FBRztBQUNqQkMsRUFBQUEsaUJBQWlCLEVBQWpCQSx3QkFEaUI7QUFFakJDLEVBQUFBLGtCQUFrQixFQUFsQkEseUJBRmlCO0FBR2pCQyxFQUFBQSxvQkFBb0IsRUFBcEJBO0FBSGlCLENBQWQ7O0FBS0EsSUFBTUMsYUFBYSxHQUFHO0FBQ3pCQyxFQUFBQSxlQUFlLEVBQWZBLHNCQUR5QjtBQUV6QkMsRUFBQUEsb0JBQW9CLEVBQXBCQSwyQkFGeUI7QUFHekJDLEVBQUFBLGlCQUFpQixFQUFqQkE7QUFIeUIsQ0FBdEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICAgIE9ORV9QUk9QT1NBTF9UWVBFLFxuICAgIE1VTFRJUFJPUE9TQUxfVFlQRSxcbiAgICBTTUFSVF9ET0NVTUVOVFNfVFlQRSxcbiAgICBOT19FRElUSU9OX01PREUsXG4gICAgUEFSVElBTF9FRElUSU9OX01PREUsXG4gICAgRlVMTF9FRElUSU9OX01PREUsXG59IGZyb20gJy4vY29uc3RhbnRzL3R5cGVzJztcblxuZXhwb3J0IHsgZGVmYXVsdCBhcyB3aXRoVGVtcGxhdGVDb3JlIH0gZnJvbSAnLi9ob2NzL3dpdGhUZW1wbGF0ZUNvcmUnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyB3aXRoVGVtcGxhdGVQYWdlcyB9IGZyb20gJy4vaG9jcy93aXRoVGVtcGxhdGVQYWdlcyc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFRlbXBsYXRlVmlldyB9IGZyb20gJy4vbW9kdWxlcy9UZW1wbGF0ZVZpZXcnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyBQYWdlUHJvcGVydGllcyB9IGZyb20gJy4vbW9kdWxlcy9QYWdlUHJvcGVydGllcyc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFBhZ2VUaHVtYm5haWwgfSBmcm9tICcuL21vZHVsZXMvVGVtcGxhdGVWaWV3L2NvbXBvbmVudHMvcGFnZXMvUGFnZVRodW1ibmFpbCc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFBhZ2VUb29sYmFyIH0gZnJvbSAnLi9tb2R1bGVzL1RlbXBsYXRlVmlldy9jb21wb25lbnRzL1BhZ2VUb29sYmFyJztcbmV4cG9ydCB7IGRlZmF1bHQgYXMgcmVkdWNlciB9IGZyb20gJy4vcmVkdWNlcic7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFdpZGdldE1lbnUgfSBmcm9tICcuL21vZHVsZXMvV2lkZ2V0TWVudSc7XG5leHBvcnQgeyBkZWZhdWx0IGFzIFdpZGdldFNpZGVNZW51IH0gZnJvbSAnLi9jb21wb25lbnRzL1dpZGdldFNpZGVNZW51JztcbmV4cG9ydCBjb25zdCB0eXBlcyA9IHtcbiAgICBPTkVfUFJPUE9TQUxfVFlQRSxcbiAgICBNVUxUSVBST1BPU0FMX1RZUEUsXG4gICAgU01BUlRfRE9DVU1FTlRTX1RZUEUsXG59O1xuZXhwb3J0IGNvbnN0IGVkaXRpb25MZXZlbHMgPSB7XG4gICAgTk9fRURJVElPTl9NT0RFLFxuICAgIFBBUlRJQUxfRURJVElPTl9NT0RFLFxuICAgIEZVTExfRURJVElPTl9NT0RFLFxufTtcbiJdfQ==