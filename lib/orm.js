"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _reduxOrm = require("redux-orm");

var _Group = _interopRequireDefault(require("./models/Group"));

var _Page = _interopRequireDefault(require("./models/Page"));

var _Template = _interopRequireDefault(require("./models/Template"));

var _Widget = _interopRequireDefault(require("./models/Widget"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var orm = new _reduxOrm.ORM({
  stateSelector: function stateSelector(state) {
    return state.entities;
  }
});
exports.default = orm;
orm.register(_Group.default, _Page.default, _Template.default, _Widget.default);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9vcm0uanMiXSwibmFtZXMiOlsib3JtIiwiT1JNIiwic3RhdGVTZWxlY3RvciIsInN0YXRlIiwiZW50aXRpZXMiLCJyZWdpc3RlciIsIkdyb3VwIiwiUGFnZSIsIlRlbXBsYXRlIiwiV2lkZ2V0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBRUE7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFFQSxJQUFNQSxHQUFHLEdBQUcsSUFBSUMsYUFBSixDQUFRO0FBQ2hCQyxFQUFBQSxhQUFhLEVBQUUsdUJBQUNDLEtBQUQ7QUFBQSxXQUFXQSxLQUFLLENBQUNDLFFBQWpCO0FBQUE7QUFEQyxDQUFSLENBQVo7O0FBSUFKLEdBQUcsQ0FBQ0ssUUFBSixDQUFhQyxjQUFiLEVBQW9CQyxhQUFwQixFQUEwQkMsaUJBQTFCLEVBQW9DQyxlQUFwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9STSB9IGZyb20gJ3JlZHV4LW9ybSc7XHJcblxyXG5pbXBvcnQgR3JvdXAgZnJvbSAnQG1vZGVscy9Hcm91cCc7XHJcbmltcG9ydCBQYWdlIGZyb20gJ0Btb2RlbHMvUGFnZSc7XHJcbmltcG9ydCBUZW1wbGF0ZSBmcm9tICdAbW9kZWxzL1RlbXBsYXRlJztcclxuaW1wb3J0IFdpZGdldCBmcm9tICdAbW9kZWxzL1dpZGdldCc7XHJcblxyXG5jb25zdCBvcm0gPSBuZXcgT1JNKHtcclxuICAgIHN0YXRlU2VsZWN0b3I6IChzdGF0ZSkgPT4gc3RhdGUuZW50aXRpZXMsXHJcbn0pO1xyXG5cclxub3JtLnJlZ2lzdGVyKEdyb3VwLCBQYWdlLCBUZW1wbGF0ZSwgV2lkZ2V0KTtcclxuZXhwb3J0IHsgb3JtIGFzIGRlZmF1bHQgfTtcclxuIl19