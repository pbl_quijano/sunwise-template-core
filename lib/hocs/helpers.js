"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.orderPagesInTemplateBuild = exports.duplicatePagesInTemplateBuild = exports.deletePagesInTemplateBuild = exports.addPageInTemplateBuild = void 0;

var _arrayMove = _interopRequireDefault(require("array-move"));

var _i18next = _interopRequireDefault(require("i18next"));

var _cloneDeep = _interopRequireDefault(require("lodash/cloneDeep"));

var _showToast = _interopRequireDefault(require("../helpers/showToast"));

var _constants = require("../modules/TemplateCore/constants");

var _helpers = require("../modules/TemplateCore/helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var addPageInTemplateBuild = function addPageInTemplateBuild(currentTemplateData, updateTemplatePromise, addNewPages, selectedPage) {
  if (!updateTemplatePromise || !currentTemplateData) {
    return function () {};
  }

  var tempCurrentTemplateData = (0, _cloneDeep.default)(currentTemplateData);
  return function () {
    tempCurrentTemplateData.pages = [].concat(_toConsumableArray(tempCurrentTemplateData.pages), [{
      company: tempCurrentTemplateData.company.id,
      content: _constants.NEW_PAGE_CONTENT,
      custom_template: tempCurrentTemplateData.id,
      page_parent: null,
      id: '',
      page: "".concat(tempCurrentTemplateData.pages.length + 1)
    }]);

    if (selectedPage) {
      tempCurrentTemplateData.pages = (0, _helpers.orderPages)((0, _arrayMove.default)(tempCurrentTemplateData.pages, tempCurrentTemplateData.pages.length - 1, parseInt(selectedPage.page)));
    }

    updateTemplatePromise(tempCurrentTemplateData.id, (0, _helpers.getUpdatingContentData)(tempCurrentTemplateData)).then(function (templateData) {
      addNewPages(templateData.pages_template);
      (0, _showToast.default)({
        body: _i18next.default.t('New page added successfully')
      });
    }).catch(function () {
      (0, _showToast.default)({
        type: 'danger',
        body: _i18next.default.t('An error occurred while adding new page')
      });
    });
  };
};

exports.addPageInTemplateBuild = addPageInTemplateBuild;

var deletePagesInTemplateBuild = function deletePagesInTemplateBuild(currentTemplateData, updateTemplatePromise, deletePages) {
  if (!updateTemplatePromise || !currentTemplateData) {
    return function () {};
  }

  var tempCurrentTemplateData = (0, _cloneDeep.default)(currentTemplateData);
  return function (deletingId) {
    tempCurrentTemplateData.pages = (0, _helpers.orderPages)(tempCurrentTemplateData.pages.filter(function (p) {
      return p.id !== deletingId;
    }));
    updateTemplatePromise(tempCurrentTemplateData.id, (0, _helpers.getUpdatingContentData)(tempCurrentTemplateData)).then(function (templateData) {
      deletePages(deletingId, templateData.pages_template);
      (0, _showToast.default)({
        body: _i18next.default.t('Page was successfully removed')
      });
    }).catch(function (error) {
      console.error(error);
      (0, _showToast.default)({
        type: 'danger',
        body: _i18next.default.t('An error occurred while deleting the page')
      });
    });
  };
};

exports.deletePagesInTemplateBuild = deletePagesInTemplateBuild;

var duplicatePagesInTemplateBuild = function duplicatePagesInTemplateBuild(currentTemplateData, updateTemplatePromise, addNewPages) {
  if (!updateTemplatePromise || !currentTemplateData) {
    return function () {};
  }

  var tempCurrentTemplateData = (0, _cloneDeep.default)(currentTemplateData);
  return function (duplicatedPage) {
    tempCurrentTemplateData.pages = [].concat(_toConsumableArray(tempCurrentTemplateData.pages), [{
      blocked: duplicatedPage.blocked || 0,
      company: tempCurrentTemplateData.company.id,
      content: duplicatedPage.content,
      custom_template: tempCurrentTemplateData.id,
      page_parent: null,
      id: '',
      page: "".concat(tempCurrentTemplateData.pages.length + 1)
    }]);
    tempCurrentTemplateData.pages = (0, _helpers.orderPages)((0, _arrayMove.default)(tempCurrentTemplateData.pages, tempCurrentTemplateData.pages.length - 1, parseInt(duplicatedPage.page)));
    updateTemplatePromise(tempCurrentTemplateData.id, (0, _helpers.getUpdatingContentData)(tempCurrentTemplateData)).then(function (templateData) {
      addNewPages(templateData.pages_template);
      (0, _showToast.default)({
        body: _i18next.default.t('The page was duplicated successfully')
      });
    }).catch(function () {
      (0, _showToast.default)({
        type: 'danger',
        body: _i18next.default.t('An error occurred while duplicating the page')
      });
    });
  };
};

exports.duplicatePagesInTemplateBuild = duplicatePagesInTemplateBuild;

var orderPagesInTemplateBuild = function orderPagesInTemplateBuild(currentTemplateData, updateTemplatePromise, setOrderPages) {
  if (!updateTemplatePromise || !currentTemplateData) {
    return function () {};
  }

  var tempCurrentTemplateData = (0, _cloneDeep.default)(currentTemplateData);
  return function (oldIndex, newIndex) {
    tempCurrentTemplateData.pages = (0, _helpers.orderPages)((0, _arrayMove.default)(tempCurrentTemplateData.pages, oldIndex, newIndex));
    setOrderPages(oldIndex, newIndex);
    updateTemplatePromise(tempCurrentTemplateData.id, (0, _helpers.getUpdatingContentData)(tempCurrentTemplateData)).then(function () {
      (0, _showToast.default)({
        body: _i18next.default.t('Order was successfully saved')
      });
    }).catch(function () {
      (0, _showToast.default)({
        type: 'danger',
        body: _i18next.default.t('An error occurred while ordering')
      });
      setOrderPages(newIndex, oldIndex);
    });
  };
}; // export const updatingTemplateActionsBuilder =
//     (customTemplateData, selectedPage) => (updateTemplatePromise) => ({
//         onAddPage: () => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { selectedPage },
//             //     ADD_PAGE_CHANGE
//             // );
//         },
//         onDeletePage: (updateTemplatePromise, deletingId) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { deletingId },
//             //     DELETE_PAGE_CHANGE
//             // );
//         },
//         onDuplicatePage: (updateTemplatePromise, duplicatedPageId) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { duplicatedPageId },
//             //     DUPLICATE_PAGE_CHANGE
//             // );
//         },
//         onOrderPages: (updateTemplatePromise, oldIndex, newIndex) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { oldIndex, newIndex },
//             //     ORDER_PAGES_CHANGE
//             // );
//         },
//     });
// export const getTemplateDataChanged = (
//     customTemplateData,
//     changeType,
//     changeData
// ) => {
//     let tempCustomTemplate = { ...customTemplateData };
//     switch (changeType) {
//         case ADD_PAGE_CHANGE: {
//             const { selectedPage } = changeData;
//             tempCustomTemplate.pages = [
//                 ...tempCustomTemplate.pages,
//                 {
//                     company: customTemplateData.company.id,
//                     content: NEW_PAGE_CONTENT,
//                     custom_template: customTemplateData.id,
//                     page_parent: null,
//                     id: '',
//                     page: `${tempCustomTemplate.pages.length + 1}`,
//                 },
//             ];
//             if (selectedPage) {
//                 tempCustomTemplate.pages = orderPages(
//                     arrayMove(
//                         tempCustomTemplate.pages,
//                         tempCustomTemplate.pages.length - 1,
//                         parseInt(selectedPage.page)
//                     )
//                 );
//             }
//             break;
//         }
//         case DELETE_PAGE_CHANGE: {
//             const { deletingId } = changeData;
//             tempCustomTemplate.pages = orderPages(
//                 tempCustomTemplate.pages.filter((p) => p.id !== deletingId)
//             );
//             break;
//         }
//         case DUPLICATE_PAGE_CHANGE: {
//             const { duplicatedPage } = changeData;
//             tempCustomTemplate.pages = [
//                 ...tempCustomTemplate.pages,
//                 {
//                     blocked: duplicatedPage.blocked || 0,
//                     company: customTemplateData.company.id,
//                     content: duplicatedPage.content,
//                     custom_template: customTemplateData.id,
//                     page_parent: null,
//                     id: '',
//                     page: `${tempCustomTemplate.pages.length + 1}`,
//                 },
//             ];
//             tempCustomTemplate.pages = orderPages(
//                 arrayMove(
//                     tempCustomTemplate.pages,
//                     tempCustomTemplate.pages.length - 1,
//                     parseInt(duplicatedPage.page)
//                 )
//             );
//             break;
//         }
//         case ORDER_PAGES_CHANGE: {
//             const { oldIndex, newIndex } = changeData;
//             tempCustomTemplate.pages = orderPages(
//                 arrayMove(tempCustomTemplate.pages, oldIndex, newIndex)
//             );
//             break;
//         }
//         default:
//     }
//     return getUpdatingContentData(tempCustomTemplate);
// };


exports.orderPagesInTemplateBuild = orderPagesInTemplateBuild;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob2NzL2hlbHBlcnMuanMiXSwibmFtZXMiOlsiYWRkUGFnZUluVGVtcGxhdGVCdWlsZCIsImN1cnJlbnRUZW1wbGF0ZURhdGEiLCJ1cGRhdGVUZW1wbGF0ZVByb21pc2UiLCJhZGROZXdQYWdlcyIsInNlbGVjdGVkUGFnZSIsInRlbXBDdXJyZW50VGVtcGxhdGVEYXRhIiwicGFnZXMiLCJjb21wYW55IiwiaWQiLCJjb250ZW50IiwiTkVXX1BBR0VfQ09OVEVOVCIsImN1c3RvbV90ZW1wbGF0ZSIsInBhZ2VfcGFyZW50IiwicGFnZSIsImxlbmd0aCIsInBhcnNlSW50IiwidGhlbiIsInRlbXBsYXRlRGF0YSIsInBhZ2VzX3RlbXBsYXRlIiwiYm9keSIsImkxOG5leHQiLCJ0IiwiY2F0Y2giLCJ0eXBlIiwiZGVsZXRlUGFnZXNJblRlbXBsYXRlQnVpbGQiLCJkZWxldGVQYWdlcyIsImRlbGV0aW5nSWQiLCJmaWx0ZXIiLCJwIiwiZXJyb3IiLCJjb25zb2xlIiwiZHVwbGljYXRlUGFnZXNJblRlbXBsYXRlQnVpbGQiLCJkdXBsaWNhdGVkUGFnZSIsImJsb2NrZWQiLCJvcmRlclBhZ2VzSW5UZW1wbGF0ZUJ1aWxkIiwic2V0T3JkZXJQYWdlcyIsIm9sZEluZGV4IiwibmV3SW5kZXgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFFQTs7QUFFQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQUVPLElBQU1BLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FDbENDLG1CQURrQyxFQUVsQ0MscUJBRmtDLEVBR2xDQyxXQUhrQyxFQUlsQ0MsWUFKa0MsRUFLakM7QUFDRCxNQUFJLENBQUNGLHFCQUFELElBQTBCLENBQUNELG1CQUEvQixFQUFvRDtBQUNoRCxXQUFPLFlBQU0sQ0FBRSxDQUFmO0FBQ0g7O0FBQ0QsTUFBSUksdUJBQXVCLEdBQUcsd0JBQVVKLG1CQUFWLENBQTlCO0FBQ0EsU0FBTyxZQUFNO0FBQ1RJLElBQUFBLHVCQUF1QixDQUFDQyxLQUF4QixnQ0FDT0QsdUJBQXVCLENBQUNDLEtBRC9CLElBRUk7QUFDSUMsTUFBQUEsT0FBTyxFQUFFRix1QkFBdUIsQ0FBQ0UsT0FBeEIsQ0FBZ0NDLEVBRDdDO0FBRUlDLE1BQUFBLE9BQU8sRUFBRUMsMkJBRmI7QUFHSUMsTUFBQUEsZUFBZSxFQUFFTix1QkFBdUIsQ0FBQ0csRUFIN0M7QUFJSUksTUFBQUEsV0FBVyxFQUFFLElBSmpCO0FBS0lKLE1BQUFBLEVBQUUsRUFBRSxFQUxSO0FBTUlLLE1BQUFBLElBQUksWUFBS1IsdUJBQXVCLENBQUNDLEtBQXhCLENBQThCUSxNQUE5QixHQUF1QyxDQUE1QztBQU5SLEtBRko7O0FBV0EsUUFBSVYsWUFBSixFQUFrQjtBQUNkQyxNQUFBQSx1QkFBdUIsQ0FBQ0MsS0FBeEIsR0FBZ0MseUJBQzVCLHdCQUNJRCx1QkFBdUIsQ0FBQ0MsS0FENUIsRUFFSUQsdUJBQXVCLENBQUNDLEtBQXhCLENBQThCUSxNQUE5QixHQUF1QyxDQUYzQyxFQUdJQyxRQUFRLENBQUNYLFlBQVksQ0FBQ1MsSUFBZCxDQUhaLENBRDRCLENBQWhDO0FBT0g7O0FBQ0RYLElBQUFBLHFCQUFxQixDQUNqQkcsdUJBQXVCLENBQUNHLEVBRFAsRUFFakIscUNBQXVCSCx1QkFBdkIsQ0FGaUIsQ0FBckIsQ0FJS1csSUFKTCxDQUlVLFVBQUNDLFlBQUQsRUFBa0I7QUFDcEJkLE1BQUFBLFdBQVcsQ0FBQ2MsWUFBWSxDQUFDQyxjQUFkLENBQVg7QUFDQSw4QkFBVTtBQUNOQyxRQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsNkJBQVY7QUFEQSxPQUFWO0FBR0gsS0FUTCxFQVVLQyxLQVZMLENBVVcsWUFBTTtBQUNULDhCQUFVO0FBQ05DLFFBQUFBLElBQUksRUFBRSxRQURBO0FBRU5KLFFBQUFBLElBQUksRUFBRUMsaUJBQVFDLENBQVIsQ0FBVSx5Q0FBVjtBQUZBLE9BQVY7QUFJSCxLQWZMO0FBZ0JILEdBckNEO0FBc0NILENBaERNOzs7O0FBa0RBLElBQU1HLDBCQUEwQixHQUFHLFNBQTdCQSwwQkFBNkIsQ0FDdEN2QixtQkFEc0MsRUFFdENDLHFCQUZzQyxFQUd0Q3VCLFdBSHNDLEVBSXJDO0FBQ0QsTUFBSSxDQUFDdkIscUJBQUQsSUFBMEIsQ0FBQ0QsbUJBQS9CLEVBQW9EO0FBQ2hELFdBQU8sWUFBTSxDQUFFLENBQWY7QUFDSDs7QUFDRCxNQUFJSSx1QkFBdUIsR0FBRyx3QkFBVUosbUJBQVYsQ0FBOUI7QUFDQSxTQUFPLFVBQUN5QixVQUFELEVBQWdCO0FBQ25CckIsSUFBQUEsdUJBQXVCLENBQUNDLEtBQXhCLEdBQWdDLHlCQUM1QkQsdUJBQXVCLENBQUNDLEtBQXhCLENBQThCcUIsTUFBOUIsQ0FBcUMsVUFBQ0MsQ0FBRDtBQUFBLGFBQU9BLENBQUMsQ0FBQ3BCLEVBQUYsS0FBU2tCLFVBQWhCO0FBQUEsS0FBckMsQ0FENEIsQ0FBaEM7QUFHQXhCLElBQUFBLHFCQUFxQixDQUNqQkcsdUJBQXVCLENBQUNHLEVBRFAsRUFFakIscUNBQXVCSCx1QkFBdkIsQ0FGaUIsQ0FBckIsQ0FJS1csSUFKTCxDQUlVLFVBQUNDLFlBQUQsRUFBa0I7QUFDcEJRLE1BQUFBLFdBQVcsQ0FBQ0MsVUFBRCxFQUFhVCxZQUFZLENBQUNDLGNBQTFCLENBQVg7QUFDQSw4QkFBVTtBQUNOQyxRQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsK0JBQVY7QUFEQSxPQUFWO0FBR0gsS0FUTCxFQVVLQyxLQVZMLENBVVcsVUFBQ08sS0FBRCxFQUFXO0FBQ2RDLE1BQUFBLE9BQU8sQ0FBQ0QsS0FBUixDQUFjQSxLQUFkO0FBQ0EsOEJBQVU7QUFDTk4sUUFBQUEsSUFBSSxFQUFFLFFBREE7QUFFTkosUUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUNGLDJDQURFO0FBRkEsT0FBVjtBQU1ILEtBbEJMO0FBbUJILEdBdkJEO0FBd0JILENBakNNOzs7O0FBbUNBLElBQU1VLDZCQUE2QixHQUFHLFNBQWhDQSw2QkFBZ0MsQ0FDekM5QixtQkFEeUMsRUFFekNDLHFCQUZ5QyxFQUd6Q0MsV0FIeUMsRUFJeEM7QUFDRCxNQUFJLENBQUNELHFCQUFELElBQTBCLENBQUNELG1CQUEvQixFQUFvRDtBQUNoRCxXQUFPLFlBQU0sQ0FBRSxDQUFmO0FBQ0g7O0FBQ0QsTUFBSUksdUJBQXVCLEdBQUcsd0JBQVVKLG1CQUFWLENBQTlCO0FBQ0EsU0FBTyxVQUFDK0IsY0FBRCxFQUFvQjtBQUN2QjNCLElBQUFBLHVCQUF1QixDQUFDQyxLQUF4QixnQ0FDT0QsdUJBQXVCLENBQUNDLEtBRC9CLElBRUk7QUFDSTJCLE1BQUFBLE9BQU8sRUFBRUQsY0FBYyxDQUFDQyxPQUFmLElBQTBCLENBRHZDO0FBRUkxQixNQUFBQSxPQUFPLEVBQUVGLHVCQUF1QixDQUFDRSxPQUF4QixDQUFnQ0MsRUFGN0M7QUFHSUMsTUFBQUEsT0FBTyxFQUFFdUIsY0FBYyxDQUFDdkIsT0FINUI7QUFJSUUsTUFBQUEsZUFBZSxFQUFFTix1QkFBdUIsQ0FBQ0csRUFKN0M7QUFLSUksTUFBQUEsV0FBVyxFQUFFLElBTGpCO0FBTUlKLE1BQUFBLEVBQUUsRUFBRSxFQU5SO0FBT0lLLE1BQUFBLElBQUksWUFBS1IsdUJBQXVCLENBQUNDLEtBQXhCLENBQThCUSxNQUE5QixHQUF1QyxDQUE1QztBQVBSLEtBRko7QUFZQVQsSUFBQUEsdUJBQXVCLENBQUNDLEtBQXhCLEdBQWdDLHlCQUM1Qix3QkFDSUQsdUJBQXVCLENBQUNDLEtBRDVCLEVBRUlELHVCQUF1QixDQUFDQyxLQUF4QixDQUE4QlEsTUFBOUIsR0FBdUMsQ0FGM0MsRUFHSUMsUUFBUSxDQUFDaUIsY0FBYyxDQUFDbkIsSUFBaEIsQ0FIWixDQUQ0QixDQUFoQztBQU9BWCxJQUFBQSxxQkFBcUIsQ0FDakJHLHVCQUF1QixDQUFDRyxFQURQLEVBRWpCLHFDQUF1QkgsdUJBQXZCLENBRmlCLENBQXJCLENBSUtXLElBSkwsQ0FJVSxVQUFDQyxZQUFELEVBQWtCO0FBQ3BCZCxNQUFBQSxXQUFXLENBQUNjLFlBQVksQ0FBQ0MsY0FBZCxDQUFYO0FBQ0EsOEJBQVU7QUFDTkMsUUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLHNDQUFWO0FBREEsT0FBVjtBQUdILEtBVEwsRUFVS0MsS0FWTCxDQVVXLFlBQU07QUFDVCw4QkFBVTtBQUNOQyxRQUFBQSxJQUFJLEVBQUUsUUFEQTtBQUVOSixRQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQ0YsOENBREU7QUFGQSxPQUFWO0FBTUgsS0FqQkw7QUFrQkgsR0F0Q0Q7QUF1Q0gsQ0FoRE07Ozs7QUFrREEsSUFBTWEseUJBQXlCLEdBQUcsU0FBNUJBLHlCQUE0QixDQUNyQ2pDLG1CQURxQyxFQUVyQ0MscUJBRnFDLEVBR3JDaUMsYUFIcUMsRUFJcEM7QUFDRCxNQUFJLENBQUNqQyxxQkFBRCxJQUEwQixDQUFDRCxtQkFBL0IsRUFBb0Q7QUFDaEQsV0FBTyxZQUFNLENBQUUsQ0FBZjtBQUNIOztBQUNELE1BQUlJLHVCQUF1QixHQUFHLHdCQUFVSixtQkFBVixDQUE5QjtBQUNBLFNBQU8sVUFBQ21DLFFBQUQsRUFBV0MsUUFBWCxFQUF3QjtBQUMzQmhDLElBQUFBLHVCQUF1QixDQUFDQyxLQUF4QixHQUFnQyx5QkFDNUIsd0JBQVVELHVCQUF1QixDQUFDQyxLQUFsQyxFQUF5QzhCLFFBQXpDLEVBQW1EQyxRQUFuRCxDQUQ0QixDQUFoQztBQUdBRixJQUFBQSxhQUFhLENBQUNDLFFBQUQsRUFBV0MsUUFBWCxDQUFiO0FBQ0FuQyxJQUFBQSxxQkFBcUIsQ0FDakJHLHVCQUF1QixDQUFDRyxFQURQLEVBRWpCLHFDQUF1QkgsdUJBQXZCLENBRmlCLENBQXJCLENBSUtXLElBSkwsQ0FJVSxZQUFNO0FBQ1IsOEJBQVU7QUFDTkcsUUFBQUEsSUFBSSxFQUFFQyxpQkFBUUMsQ0FBUixDQUFVLDhCQUFWO0FBREEsT0FBVjtBQUdILEtBUkwsRUFTS0MsS0FUTCxDQVNXLFlBQU07QUFDVCw4QkFBVTtBQUNOQyxRQUFBQSxJQUFJLEVBQUUsUUFEQTtBQUVOSixRQUFBQSxJQUFJLEVBQUVDLGlCQUFRQyxDQUFSLENBQVUsa0NBQVY7QUFGQSxPQUFWO0FBSUFjLE1BQUFBLGFBQWEsQ0FBQ0UsUUFBRCxFQUFXRCxRQUFYLENBQWI7QUFDSCxLQWZMO0FBZ0JILEdBckJEO0FBc0JILENBL0JNLEMsQ0FpQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgYXJyYXlNb3ZlIGZyb20gJ2FycmF5LW1vdmUnO1xyXG5pbXBvcnQgaTE4bmV4dCBmcm9tICdpMThuZXh0JztcclxuaW1wb3J0IGNsb25lRGVlcCBmcm9tICdsb2Rhc2gvY2xvbmVEZWVwJztcclxuXHJcbmltcG9ydCBzaG93VG9hc3QgZnJvbSAnQGhlbHBlcnMvc2hvd1RvYXN0JztcclxuXHJcbmltcG9ydCB7IE5FV19QQUdFX0NPTlRFTlQgfSBmcm9tICdAdGVtcGxhdGVDb3JlL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IGdldFVwZGF0aW5nQ29udGVudERhdGEsIG9yZGVyUGFnZXMgfSBmcm9tICdAdGVtcGxhdGVDb3JlL2hlbHBlcnMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGFkZFBhZ2VJblRlbXBsYXRlQnVpbGQgPSAoXHJcbiAgICBjdXJyZW50VGVtcGxhdGVEYXRhLFxyXG4gICAgdXBkYXRlVGVtcGxhdGVQcm9taXNlLFxyXG4gICAgYWRkTmV3UGFnZXMsXHJcbiAgICBzZWxlY3RlZFBhZ2VcclxuKSA9PiB7XHJcbiAgICBpZiAoIXVwZGF0ZVRlbXBsYXRlUHJvbWlzZSB8fCAhY3VycmVudFRlbXBsYXRlRGF0YSkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7fTtcclxuICAgIH1cclxuICAgIGxldCB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YSA9IGNsb25lRGVlcChjdXJyZW50VGVtcGxhdGVEYXRhKTtcclxuICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEucGFnZXMgPSBbXHJcbiAgICAgICAgICAgIC4uLnRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBjb21wYW55OiB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5jb21wYW55LmlkLFxyXG4gICAgICAgICAgICAgICAgY29udGVudDogTkVXX1BBR0VfQ09OVEVOVCxcclxuICAgICAgICAgICAgICAgIGN1c3RvbV90ZW1wbGF0ZTogdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEuaWQsXHJcbiAgICAgICAgICAgICAgICBwYWdlX3BhcmVudDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGlkOiAnJyxcclxuICAgICAgICAgICAgICAgIHBhZ2U6IGAke3RlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLmxlbmd0aCArIDF9YCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFBhZ2UpIHtcclxuICAgICAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEucGFnZXMgPSBvcmRlclBhZ2VzKFxyXG4gICAgICAgICAgICAgICAgYXJyYXlNb3ZlKFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLFxyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLmxlbmd0aCAtIDEsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyc2VJbnQoc2VsZWN0ZWRQYWdlLnBhZ2UpXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZShcclxuICAgICAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEuaWQsXHJcbiAgICAgICAgICAgIGdldFVwZGF0aW5nQ29udGVudERhdGEodGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEpXHJcbiAgICAgICAgKVxyXG4gICAgICAgICAgICAudGhlbigodGVtcGxhdGVEYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBhZGROZXdQYWdlcyh0ZW1wbGF0ZURhdGEucGFnZXNfdGVtcGxhdGUpO1xyXG4gICAgICAgICAgICAgICAgc2hvd1RvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICBib2R5OiBpMThuZXh0LnQoJ05ldyBwYWdlIGFkZGVkIHN1Y2Nlc3NmdWxseScpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzaG93VG9hc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkYW5nZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGkxOG5leHQudCgnQW4gZXJyb3Igb2NjdXJyZWQgd2hpbGUgYWRkaW5nIG5ldyBwYWdlJyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGRlbGV0ZVBhZ2VzSW5UZW1wbGF0ZUJ1aWxkID0gKFxyXG4gICAgY3VycmVudFRlbXBsYXRlRGF0YSxcclxuICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSxcclxuICAgIGRlbGV0ZVBhZ2VzXHJcbikgPT4ge1xyXG4gICAgaWYgKCF1cGRhdGVUZW1wbGF0ZVByb21pc2UgfHwgIWN1cnJlbnRUZW1wbGF0ZURhdGEpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge307XHJcbiAgICB9XHJcbiAgICBsZXQgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEgPSBjbG9uZURlZXAoY3VycmVudFRlbXBsYXRlRGF0YSk7XHJcbiAgICByZXR1cm4gKGRlbGV0aW5nSWQpID0+IHtcclxuICAgICAgICB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5wYWdlcyA9IG9yZGVyUGFnZXMoXHJcbiAgICAgICAgICAgIHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLmZpbHRlcigocCkgPT4gcC5pZCAhPT0gZGVsZXRpbmdJZClcclxuICAgICAgICApO1xyXG4gICAgICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZShcclxuICAgICAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEuaWQsXHJcbiAgICAgICAgICAgIGdldFVwZGF0aW5nQ29udGVudERhdGEodGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEpXHJcbiAgICAgICAgKVxyXG4gICAgICAgICAgICAudGhlbigodGVtcGxhdGVEYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGVQYWdlcyhkZWxldGluZ0lkLCB0ZW1wbGF0ZURhdGEucGFnZXNfdGVtcGxhdGUpO1xyXG4gICAgICAgICAgICAgICAgc2hvd1RvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICBib2R5OiBpMThuZXh0LnQoJ1BhZ2Ugd2FzIHN1Y2Nlc3NmdWxseSByZW1vdmVkJyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICBzaG93VG9hc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkYW5nZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGkxOG5leHQudChcclxuICAgICAgICAgICAgICAgICAgICAgICAgJ0FuIGVycm9yIG9jY3VycmVkIHdoaWxlIGRlbGV0aW5nIHRoZSBwYWdlJ1xyXG4gICAgICAgICAgICAgICAgICAgICksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGR1cGxpY2F0ZVBhZ2VzSW5UZW1wbGF0ZUJ1aWxkID0gKFxyXG4gICAgY3VycmVudFRlbXBsYXRlRGF0YSxcclxuICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSxcclxuICAgIGFkZE5ld1BhZ2VzXHJcbikgPT4ge1xyXG4gICAgaWYgKCF1cGRhdGVUZW1wbGF0ZVByb21pc2UgfHwgIWN1cnJlbnRUZW1wbGF0ZURhdGEpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge307XHJcbiAgICB9XHJcbiAgICBsZXQgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEgPSBjbG9uZURlZXAoY3VycmVudFRlbXBsYXRlRGF0YSk7XHJcbiAgICByZXR1cm4gKGR1cGxpY2F0ZWRQYWdlKSA9PiB7XHJcbiAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEucGFnZXMgPSBbXHJcbiAgICAgICAgICAgIC4uLnRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBibG9ja2VkOiBkdXBsaWNhdGVkUGFnZS5ibG9ja2VkIHx8IDAsXHJcbiAgICAgICAgICAgICAgICBjb21wYW55OiB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5jb21wYW55LmlkLFxyXG4gICAgICAgICAgICAgICAgY29udGVudDogZHVwbGljYXRlZFBhZ2UuY29udGVudCxcclxuICAgICAgICAgICAgICAgIGN1c3RvbV90ZW1wbGF0ZTogdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEuaWQsXHJcbiAgICAgICAgICAgICAgICBwYWdlX3BhcmVudDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGlkOiAnJyxcclxuICAgICAgICAgICAgICAgIHBhZ2U6IGAke3RlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLmxlbmd0aCArIDF9YCxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICBdO1xyXG4gICAgICAgIHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzID0gb3JkZXJQYWdlcyhcclxuICAgICAgICAgICAgYXJyYXlNb3ZlKFxyXG4gICAgICAgICAgICAgICAgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEucGFnZXMsXHJcbiAgICAgICAgICAgICAgICB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5wYWdlcy5sZW5ndGggLSAxLFxyXG4gICAgICAgICAgICAgICAgcGFyc2VJbnQoZHVwbGljYXRlZFBhZ2UucGFnZSlcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdXBkYXRlVGVtcGxhdGVQcm9taXNlKFxyXG4gICAgICAgICAgICB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5pZCxcclxuICAgICAgICAgICAgZ2V0VXBkYXRpbmdDb250ZW50RGF0YSh0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YSlcclxuICAgICAgICApXHJcbiAgICAgICAgICAgIC50aGVuKCh0ZW1wbGF0ZURhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGFkZE5ld1BhZ2VzKHRlbXBsYXRlRGF0YS5wYWdlc190ZW1wbGF0ZSk7XHJcbiAgICAgICAgICAgICAgICBzaG93VG9hc3Qoe1xyXG4gICAgICAgICAgICAgICAgICAgIGJvZHk6IGkxOG5leHQudCgnVGhlIHBhZ2Ugd2FzIGR1cGxpY2F0ZWQgc3VjY2Vzc2Z1bGx5JyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2RhbmdlcicsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogaTE4bmV4dC50KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAnQW4gZXJyb3Igb2NjdXJyZWQgd2hpbGUgZHVwbGljYXRpbmcgdGhlIHBhZ2UnXHJcbiAgICAgICAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH07XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3Qgb3JkZXJQYWdlc0luVGVtcGxhdGVCdWlsZCA9IChcclxuICAgIGN1cnJlbnRUZW1wbGF0ZURhdGEsXHJcbiAgICB1cGRhdGVUZW1wbGF0ZVByb21pc2UsXHJcbiAgICBzZXRPcmRlclBhZ2VzXHJcbikgPT4ge1xyXG4gICAgaWYgKCF1cGRhdGVUZW1wbGF0ZVByb21pc2UgfHwgIWN1cnJlbnRUZW1wbGF0ZURhdGEpIHtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge307XHJcbiAgICB9XHJcbiAgICBsZXQgdGVtcEN1cnJlbnRUZW1wbGF0ZURhdGEgPSBjbG9uZURlZXAoY3VycmVudFRlbXBsYXRlRGF0YSk7XHJcbiAgICByZXR1cm4gKG9sZEluZGV4LCBuZXdJbmRleCkgPT4ge1xyXG4gICAgICAgIHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzID0gb3JkZXJQYWdlcyhcclxuICAgICAgICAgICAgYXJyYXlNb3ZlKHRlbXBDdXJyZW50VGVtcGxhdGVEYXRhLnBhZ2VzLCBvbGRJbmRleCwgbmV3SW5kZXgpXHJcbiAgICAgICAgKTtcclxuICAgICAgICBzZXRPcmRlclBhZ2VzKG9sZEluZGV4LCBuZXdJbmRleCk7XHJcbiAgICAgICAgdXBkYXRlVGVtcGxhdGVQcm9taXNlKFxyXG4gICAgICAgICAgICB0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YS5pZCxcclxuICAgICAgICAgICAgZ2V0VXBkYXRpbmdDb250ZW50RGF0YSh0ZW1wQ3VycmVudFRlbXBsYXRlRGF0YSlcclxuICAgICAgICApXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogaTE4bmV4dC50KCdPcmRlciB3YXMgc3VjY2Vzc2Z1bGx5IHNhdmVkJyksXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2RhbmdlcicsXHJcbiAgICAgICAgICAgICAgICAgICAgYm9keTogaTE4bmV4dC50KCdBbiBlcnJvciBvY2N1cnJlZCB3aGlsZSBvcmRlcmluZycpLFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBzZXRPcmRlclBhZ2VzKG5ld0luZGV4LCBvbGRJbmRleCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfTtcclxufTtcclxuXHJcbi8vIGV4cG9ydCBjb25zdCB1cGRhdGluZ1RlbXBsYXRlQWN0aW9uc0J1aWxkZXIgPVxyXG4vLyAgICAgKGN1c3RvbVRlbXBsYXRlRGF0YSwgc2VsZWN0ZWRQYWdlKSA9PiAodXBkYXRlVGVtcGxhdGVQcm9taXNlKSA9PiAoe1xyXG4vLyAgICAgICAgIG9uQWRkUGFnZTogKCkgPT4ge1xyXG4vLyAgICAgICAgICAgICAvLyB1cGRhdGVUZW1wbGF0ZUFjdGlvbihcclxuLy8gICAgICAgICAgICAgLy8gICAgIGN1c3RvbVRlbXBsYXRlSWQsXHJcbi8vICAgICAgICAgICAgIC8vICAgICB7IHNlbGVjdGVkUGFnZSB9LFxyXG4vLyAgICAgICAgICAgICAvLyAgICAgQUREX1BBR0VfQ0hBTkdFXHJcbi8vICAgICAgICAgICAgIC8vICk7XHJcbi8vICAgICAgICAgfSxcclxuLy8gICAgICAgICBvbkRlbGV0ZVBhZ2U6ICh1cGRhdGVUZW1wbGF0ZVByb21pc2UsIGRlbGV0aW5nSWQpID0+IHtcclxuLy8gICAgICAgICAgICAgLy8gdXBkYXRlVGVtcGxhdGVBY3Rpb24oXHJcbi8vICAgICAgICAgICAgIC8vICAgICBjdXN0b21UZW1wbGF0ZUlkLFxyXG4vLyAgICAgICAgICAgICAvLyAgICAgeyBkZWxldGluZ0lkIH0sXHJcbi8vICAgICAgICAgICAgIC8vICAgICBERUxFVEVfUEFHRV9DSEFOR0VcclxuLy8gICAgICAgICAgICAgLy8gKTtcclxuLy8gICAgICAgICB9LFxyXG4vLyAgICAgICAgIG9uRHVwbGljYXRlUGFnZTogKHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSwgZHVwbGljYXRlZFBhZ2VJZCkgPT4ge1xyXG4vLyAgICAgICAgICAgICAvLyB1cGRhdGVUZW1wbGF0ZUFjdGlvbihcclxuLy8gICAgICAgICAgICAgLy8gICAgIGN1c3RvbVRlbXBsYXRlSWQsXHJcbi8vICAgICAgICAgICAgIC8vICAgICB7IGR1cGxpY2F0ZWRQYWdlSWQgfSxcclxuLy8gICAgICAgICAgICAgLy8gICAgIERVUExJQ0FURV9QQUdFX0NIQU5HRVxyXG4vLyAgICAgICAgICAgICAvLyApO1xyXG4vLyAgICAgICAgIH0sXHJcbi8vICAgICAgICAgb25PcmRlclBhZ2VzOiAodXBkYXRlVGVtcGxhdGVQcm9taXNlLCBvbGRJbmRleCwgbmV3SW5kZXgpID0+IHtcclxuLy8gICAgICAgICAgICAgLy8gdXBkYXRlVGVtcGxhdGVBY3Rpb24oXHJcbi8vICAgICAgICAgICAgIC8vICAgICBjdXN0b21UZW1wbGF0ZUlkLFxyXG4vLyAgICAgICAgICAgICAvLyAgICAgeyBvbGRJbmRleCwgbmV3SW5kZXggfSxcclxuLy8gICAgICAgICAgICAgLy8gICAgIE9SREVSX1BBR0VTX0NIQU5HRVxyXG4vLyAgICAgICAgICAgICAvLyApO1xyXG4vLyAgICAgICAgIH0sXHJcbi8vICAgICB9KTtcclxuXHJcbi8vIGV4cG9ydCBjb25zdCBnZXRUZW1wbGF0ZURhdGFDaGFuZ2VkID0gKFxyXG4vLyAgICAgY3VzdG9tVGVtcGxhdGVEYXRhLFxyXG4vLyAgICAgY2hhbmdlVHlwZSxcclxuLy8gICAgIGNoYW5nZURhdGFcclxuLy8gKSA9PiB7XHJcbi8vICAgICBsZXQgdGVtcEN1c3RvbVRlbXBsYXRlID0geyAuLi5jdXN0b21UZW1wbGF0ZURhdGEgfTtcclxuXHJcbi8vICAgICBzd2l0Y2ggKGNoYW5nZVR5cGUpIHtcclxuLy8gICAgICAgICBjYXNlIEFERF9QQUdFX0NIQU5HRToge1xyXG4vLyAgICAgICAgICAgICBjb25zdCB7IHNlbGVjdGVkUGFnZSB9ID0gY2hhbmdlRGF0YTtcclxuLy8gICAgICAgICAgICAgdGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzID0gW1xyXG4vLyAgICAgICAgICAgICAgICAgLi4udGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzLFxyXG4vLyAgICAgICAgICAgICAgICAge1xyXG4vLyAgICAgICAgICAgICAgICAgICAgIGNvbXBhbnk6IGN1c3RvbVRlbXBsYXRlRGF0YS5jb21wYW55LmlkLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IE5FV19QQUdFX0NPTlRFTlQsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgY3VzdG9tX3RlbXBsYXRlOiBjdXN0b21UZW1wbGF0ZURhdGEuaWQsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgcGFnZV9wYXJlbnQ6IG51bGwsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgaWQ6ICcnLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIHBhZ2U6IGAke3RlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcy5sZW5ndGggKyAxfWAsXHJcbi8vICAgICAgICAgICAgICAgICB9LFxyXG4vLyAgICAgICAgICAgICBdO1xyXG4vLyAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRQYWdlKSB7XHJcbi8vICAgICAgICAgICAgICAgICB0ZW1wQ3VzdG9tVGVtcGxhdGUucGFnZXMgPSBvcmRlclBhZ2VzKFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIGFycmF5TW92ZShcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgdGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgICAgICB0ZW1wQ3VzdG9tVGVtcGxhdGUucGFnZXMubGVuZ3RoIC0gMSxcclxuLy8gICAgICAgICAgICAgICAgICAgICAgICAgcGFyc2VJbnQoc2VsZWN0ZWRQYWdlLnBhZ2UpXHJcbi8vICAgICAgICAgICAgICAgICAgICAgKVxyXG4vLyAgICAgICAgICAgICAgICAgKTtcclxuLy8gICAgICAgICAgICAgfVxyXG4vLyAgICAgICAgICAgICBicmVhaztcclxuLy8gICAgICAgICB9XHJcblxyXG4vLyAgICAgICAgIGNhc2UgREVMRVRFX1BBR0VfQ0hBTkdFOiB7XHJcbi8vICAgICAgICAgICAgIGNvbnN0IHsgZGVsZXRpbmdJZCB9ID0gY2hhbmdlRGF0YTtcclxuLy8gICAgICAgICAgICAgdGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzID0gb3JkZXJQYWdlcyhcclxuLy8gICAgICAgICAgICAgICAgIHRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcy5maWx0ZXIoKHApID0+IHAuaWQgIT09IGRlbGV0aW5nSWQpXHJcbi8vICAgICAgICAgICAgICk7XHJcbi8vICAgICAgICAgICAgIGJyZWFrO1xyXG4vLyAgICAgICAgIH1cclxuXHJcbi8vICAgICAgICAgY2FzZSBEVVBMSUNBVEVfUEFHRV9DSEFOR0U6IHtcclxuLy8gICAgICAgICAgICAgY29uc3QgeyBkdXBsaWNhdGVkUGFnZSB9ID0gY2hhbmdlRGF0YTtcclxuXHJcbi8vICAgICAgICAgICAgIHRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcyA9IFtcclxuLy8gICAgICAgICAgICAgICAgIC4uLnRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcyxcclxuLy8gICAgICAgICAgICAgICAgIHtcclxuLy8gICAgICAgICAgICAgICAgICAgICBibG9ja2VkOiBkdXBsaWNhdGVkUGFnZS5ibG9ja2VkIHx8IDAsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgY29tcGFueTogY3VzdG9tVGVtcGxhdGVEYXRhLmNvbXBhbnkuaWQsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgY29udGVudDogZHVwbGljYXRlZFBhZ2UuY29udGVudCxcclxuLy8gICAgICAgICAgICAgICAgICAgICBjdXN0b21fdGVtcGxhdGU6IGN1c3RvbVRlbXBsYXRlRGF0YS5pZCxcclxuLy8gICAgICAgICAgICAgICAgICAgICBwYWdlX3BhcmVudDogbnVsbCxcclxuLy8gICAgICAgICAgICAgICAgICAgICBpZDogJycsXHJcbi8vICAgICAgICAgICAgICAgICAgICAgcGFnZTogYCR7dGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzLmxlbmd0aCArIDF9YCxcclxuLy8gICAgICAgICAgICAgICAgIH0sXHJcbi8vICAgICAgICAgICAgIF07XHJcbi8vICAgICAgICAgICAgIHRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcyA9IG9yZGVyUGFnZXMoXHJcbi8vICAgICAgICAgICAgICAgICBhcnJheU1vdmUoXHJcbi8vICAgICAgICAgICAgICAgICAgICAgdGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIHRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcy5sZW5ndGggLSAxLFxyXG4vLyAgICAgICAgICAgICAgICAgICAgIHBhcnNlSW50KGR1cGxpY2F0ZWRQYWdlLnBhZ2UpXHJcbi8vICAgICAgICAgICAgICAgICApXHJcbi8vICAgICAgICAgICAgICk7XHJcbi8vICAgICAgICAgICAgIGJyZWFrO1xyXG4vLyAgICAgICAgIH1cclxuXHJcbi8vICAgICAgICAgY2FzZSBPUkRFUl9QQUdFU19DSEFOR0U6IHtcclxuLy8gICAgICAgICAgICAgY29uc3QgeyBvbGRJbmRleCwgbmV3SW5kZXggfSA9IGNoYW5nZURhdGE7XHJcbi8vICAgICAgICAgICAgIHRlbXBDdXN0b21UZW1wbGF0ZS5wYWdlcyA9IG9yZGVyUGFnZXMoXHJcbi8vICAgICAgICAgICAgICAgICBhcnJheU1vdmUodGVtcEN1c3RvbVRlbXBsYXRlLnBhZ2VzLCBvbGRJbmRleCwgbmV3SW5kZXgpXHJcbi8vICAgICAgICAgICAgICk7XHJcbi8vICAgICAgICAgICAgIGJyZWFrO1xyXG4vLyAgICAgICAgIH1cclxuXHJcbi8vICAgICAgICAgZGVmYXVsdDpcclxuLy8gICAgIH1cclxuLy8gICAgIHJldHVybiBnZXRVcGRhdGluZ0NvbnRlbnREYXRhKHRlbXBDdXN0b21UZW1wbGF0ZSk7XHJcbi8vIH07XHJcbiJdfQ==