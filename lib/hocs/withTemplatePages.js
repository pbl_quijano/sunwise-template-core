"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var templateCoreSelectors = _interopRequireWildcard(require("../modules/TemplateCore/selectors"));

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var WithTemplatePages = function WithTemplatePages(_ref) {
  var Children = _ref.Children,
      injectedProps = _ref.injectedProps,
      templatePages = _ref.templatePages;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(Children, _objectSpread({
    templatePages: templatePages
  }, _objectSpread({}, injectedProps)));
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  templatePages: templateCoreSelectors.getCurrentTemplatePages
});
WithTemplatePages.propTypes = {
  Children: _propTypes.default.any,
  injectedProps: _propTypes.default.object,
  templatePages: _propTypes.default.array
};
var WithTemplatePagesComposed = (0, _reactRedux.connect)(mapStateToProps, null)(WithTemplatePages);

var WithTemplatePagesBuild = function WithTemplatePagesBuild(_ref2) {
  var Children = _ref2.Children,
      injectedProps = _ref2.injectedProps;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(WithTemplatePagesComposed, {
    Children: Children,
    injectedProps: injectedProps
  });
};

WithTemplatePagesBuild.propTypes = {
  Children: _propTypes.default.any,
  injectedProps: _propTypes.default.object
};

var WithTemplatePagesHOC = function WithTemplatePagesHOC(Children) {
  return function (props) {
    return WithTemplatePagesBuild({
      Children: Children,
      injectedProps: props
    });
  };
};

var _default = WithTemplatePagesHOC;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob2NzL3dpdGhUZW1wbGF0ZVBhZ2VzLmpzIl0sIm5hbWVzIjpbIldpdGhUZW1wbGF0ZVBhZ2VzIiwiQ2hpbGRyZW4iLCJpbmplY3RlZFByb3BzIiwidGVtcGxhdGVQYWdlcyIsIm1hcFN0YXRlVG9Qcm9wcyIsInRlbXBsYXRlQ29yZVNlbGVjdG9ycyIsImdldEN1cnJlbnRUZW1wbGF0ZVBhZ2VzIiwicHJvcFR5cGVzIiwiUHJvcFR5cGVzIiwiYW55Iiwib2JqZWN0IiwiYXJyYXkiLCJXaXRoVGVtcGxhdGVQYWdlc0NvbXBvc2VkIiwiV2l0aFRlbXBsYXRlUGFnZXNCdWlsZCIsIldpdGhUZW1wbGF0ZVBhZ2VzSE9DIiwicHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsSUFBTUEsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixPQUFnRDtBQUFBLE1BQTdDQyxRQUE2QyxRQUE3Q0EsUUFBNkM7QUFBQSxNQUFuQ0MsYUFBbUMsUUFBbkNBLGFBQW1DO0FBQUEsTUFBcEJDLGFBQW9CLFFBQXBCQSxhQUFvQjtBQUN0RSxzQkFDSSxxQkFBQyxRQUFEO0FBQ0ksSUFBQSxhQUFhLEVBQUVBO0FBRG5CLHVCQUVhRCxhQUZiLEdBREo7QUFNSCxDQVBEOztBQVNBLElBQU1FLGVBQWUsR0FBRyx3Q0FBeUI7QUFDN0NELEVBQUFBLGFBQWEsRUFBRUUscUJBQXFCLENBQUNDO0FBRFEsQ0FBekIsQ0FBeEI7QUFJQU4saUJBQWlCLENBQUNPLFNBQWxCLEdBQThCO0FBQzFCTixFQUFBQSxRQUFRLEVBQUVPLG1CQUFVQyxHQURNO0FBRTFCUCxFQUFBQSxhQUFhLEVBQUVNLG1CQUFVRSxNQUZDO0FBRzFCUCxFQUFBQSxhQUFhLEVBQUVLLG1CQUFVRztBQUhDLENBQTlCO0FBTUEsSUFBTUMseUJBQXlCLEdBQUcseUJBQzlCUixlQUQ4QixFQUU5QixJQUY4QixFQUdoQ0osaUJBSGdDLENBQWxDOztBQUtBLElBQU1hLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsUUFBaUM7QUFBQSxNQUE5QlosUUFBOEIsU0FBOUJBLFFBQThCO0FBQUEsTUFBcEJDLGFBQW9CLFNBQXBCQSxhQUFvQjtBQUM1RCxzQkFDSSxxQkFBQyx5QkFBRDtBQUVRRCxJQUFBQSxRQUFRLEVBQVJBLFFBRlI7QUFHUUMsSUFBQUEsYUFBYSxFQUFiQTtBQUhSLElBREo7QUFRSCxDQVREOztBQVdBVyxzQkFBc0IsQ0FBQ04sU0FBdkIsR0FBbUM7QUFDL0JOLEVBQUFBLFFBQVEsRUFBRU8sbUJBQVVDLEdBRFc7QUFFL0JQLEVBQUFBLGFBQWEsRUFBRU0sbUJBQVVFO0FBRk0sQ0FBbkM7O0FBS0EsSUFBTUksb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFDYixRQUFEO0FBQUEsU0FBYyxVQUFDYyxLQUFEO0FBQUEsV0FDdkNGLHNCQUFzQixDQUFDO0FBQ25CWixNQUFBQSxRQUFRLEVBQVJBLFFBRG1CO0FBRW5CQyxNQUFBQSxhQUFhLEVBQUVhO0FBRkksS0FBRCxDQURpQjtBQUFBLEdBQWQ7QUFBQSxDQUE3Qjs7ZUFNZUQsb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyBjcmVhdGVTdHJ1Y3R1cmVkU2VsZWN0b3IgfSBmcm9tICdyZXNlbGVjdCc7XHJcblxyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xyXG5cclxuY29uc3QgV2l0aFRlbXBsYXRlUGFnZXMgPSAoeyBDaGlsZHJlbiwgaW5qZWN0ZWRQcm9wcywgdGVtcGxhdGVQYWdlcyB9KSA9PiB7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxDaGlsZHJlblxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVBhZ2VzPXt0ZW1wbGF0ZVBhZ2VzfVxyXG4gICAgICAgICAgICB7Li4ueyAuLi5pbmplY3RlZFByb3BzIH19XHJcbiAgICAgICAgPjwvQ2hpbGRyZW4+XHJcbiAgICApO1xyXG59O1xyXG5cclxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yKHtcclxuICAgIHRlbXBsYXRlUGFnZXM6IHRlbXBsYXRlQ29yZVNlbGVjdG9ycy5nZXRDdXJyZW50VGVtcGxhdGVQYWdlcyxcclxufSk7XHJcblxyXG5XaXRoVGVtcGxhdGVQYWdlcy5wcm9wVHlwZXMgPSB7XHJcbiAgICBDaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcclxuICAgIGluamVjdGVkUHJvcHM6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICB0ZW1wbGF0ZVBhZ2VzOiBQcm9wVHlwZXMuYXJyYXksXHJcbn07XHJcblxyXG5jb25zdCBXaXRoVGVtcGxhdGVQYWdlc0NvbXBvc2VkID0gY29ubmVjdChcclxuICAgIG1hcFN0YXRlVG9Qcm9wcyxcclxuICAgIG51bGxcclxuKShXaXRoVGVtcGxhdGVQYWdlcyk7XHJcblxyXG5jb25zdCBXaXRoVGVtcGxhdGVQYWdlc0J1aWxkID0gKHsgQ2hpbGRyZW4sIGluamVjdGVkUHJvcHMgfSkgPT4ge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8V2l0aFRlbXBsYXRlUGFnZXNDb21wb3NlZFxyXG4gICAgICAgICAgICB7Li4ue1xyXG4gICAgICAgICAgICAgICAgQ2hpbGRyZW4sXHJcbiAgICAgICAgICAgICAgICBpbmplY3RlZFByb3BzLFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgIC8+XHJcbiAgICApO1xyXG59O1xyXG5cclxuV2l0aFRlbXBsYXRlUGFnZXNCdWlsZC5wcm9wVHlwZXMgPSB7XHJcbiAgICBDaGlsZHJlbjogUHJvcFR5cGVzLmFueSxcclxuICAgIGluamVjdGVkUHJvcHM6IFByb3BUeXBlcy5vYmplY3QsXHJcbn07XHJcblxyXG5jb25zdCBXaXRoVGVtcGxhdGVQYWdlc0hPQyA9IChDaGlsZHJlbikgPT4gKHByb3BzKSA9PlxyXG4gICAgV2l0aFRlbXBsYXRlUGFnZXNCdWlsZCh7XHJcbiAgICAgICAgQ2hpbGRyZW4sXHJcbiAgICAgICAgaW5qZWN0ZWRQcm9wczogcHJvcHMsXHJcbiAgICB9KTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdpdGhUZW1wbGF0ZVBhZ2VzSE9DO1xyXG4iXX0=