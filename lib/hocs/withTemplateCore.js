"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _googleMapsReact = require("google-maps-react");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = require("react");

var _reactRedux = require("react-redux");

var _recompose = require("recompose");

var _reselect = require("reselect");

var _contexts = require("../helpers/contexts");

var actions = _interopRequireWildcard(require("../modules/main/actions"));

var selectors = _interopRequireWildcard(require("../modules/main/selectors"));

var templateViewActions = _interopRequireWildcard(require("../modules/TemplateView/actions"));

var templateViewSelectors = _interopRequireWildcard(require("../modules/TemplateView/selectors"));

var templateCoreActions = _interopRequireWildcard(require("../modules/TemplateCore/actions"));

var templateCoreSelectors = _interopRequireWildcard(require("../modules/TemplateCore/selectors"));

var _helpers = require("../i18n/helpers");

var _helpers2 = require("./helpers");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var WithTemplateCore = function WithTemplateCore(_ref) {
  var Children = _ref.Children,
      addNewPages = _ref.addNewPages,
      baseUrl = _ref.baseUrl,
      currencyConfig = _ref.currencyConfig,
      currentTemplateFullData = _ref.currentTemplateFullData,
      deletePages = _ref.deletePages,
      getTemplateUpdatedData = _ref.getTemplateUpdatedData,
      google = _ref.google,
      googleApiKey = _ref.googleApiKey,
      initTemplate = _ref.initTemplate,
      injectedProps = _ref.injectedProps,
      isEmptyPages = _ref.isEmptyPages,
      onUpdatePage = _ref.onUpdatePage,
      reset = _ref.reset,
      resetTemplate = _ref.resetTemplate,
      restorePages = _ref.restorePages,
      selectPage = _ref.selectPage,
      selectedPage = _ref.selectedPage,
      setOrderPages = _ref.setOrderPages,
      setTemplateConfig = _ref.setTemplateConfig,
      setTemplateBackup = _ref.setTemplateBackup,
      templateConfig = _ref.templateConfig;
  var language = templateConfig.language,
      updateTemplatePromise = templateConfig.updateTemplatePromise,
      _onChangeInPage = templateConfig.onChangeInPage;
  (0, _react.useEffect)(function () {
    if (language) {
      (0, _helpers.initInternationalization)(language);
    }

    setTemplateConfig(templateConfig);
    return function () {
      reset();
    };
  }, []);
  var addBlankPageInTemplate = (0, _react.useCallback)((0, _helpers2.addPageInTemplateBuild)(currentTemplateFullData, updateTemplatePromise, addNewPages, selectedPage), [currentTemplateFullData, updateTemplatePromise, addNewPages, selectedPage]);
  var deletePagesInTemplate = (0, _react.useCallback)((0, _helpers2.deletePagesInTemplateBuild)(currentTemplateFullData, updateTemplatePromise, deletePages), [currentTemplateFullData, updateTemplatePromise, deletePages]);
  var duplicatePagesInTemplate = (0, _react.useCallback)((0, _helpers2.duplicatePagesInTemplateBuild)(currentTemplateFullData, updateTemplatePromise, addNewPages), [currentTemplateFullData, updateTemplatePromise, addNewPages]);
  var orderPagesInTemplate = (0, _react.useCallback)((0, _helpers2.orderPagesInTemplateBuild)(currentTemplateFullData, updateTemplatePromise, setOrderPages), [currentTemplateFullData, updateTemplatePromise, addNewPages]);

  if (baseUrl === null || googleApiKey === null) {
    return null;
  }

  var tagsLocale = currencyConfig.tagsLocale,
      _currencyConfig$chart = currencyConfig.chartIso,
      chartIso = _currencyConfig$chart === void 0 ? 'USD' : _currencyConfig$chart,
      _currencyConfig$chart2 = currencyConfig.chartLocale,
      chartLocale = _currencyConfig$chart2 === void 0 ? 'en-US' : _currencyConfig$chart2,
      _currencyConfig$table = currencyConfig.tableIso,
      tableIso = _currencyConfig$table === void 0 ? 'USD' : _currencyConfig$table,
      _currencyConfig$table2 = currencyConfig.tableLocale,
      tableLocale = _currencyConfig$table2 === void 0 ? 'en-US' : _currencyConfig$table2;
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_contexts.GeneralProvider, {
    value: {
      google: google,
      onChangeInPage: function onChangeInPage(addStateDisabled) {
        return onUpdatePage(_onChangeInPage, addStateDisabled);
      },
      tagsLocale: tagsLocale,
      currencyConfig: {
        chartIso: chartIso,
        chartLocale: chartLocale,
        tableIso: tableIso,
        tableLocale: tableLocale
      }
    },
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(Children, _objectSpread({
      addNewPages: addNewPages,
      addBlankPageInTemplate: addBlankPageInTemplate,
      deletePagesInTemplate: deletePagesInTemplate,
      duplicatePagesInTemplate: duplicatePagesInTemplate,
      getTemplateUpdatedData: getTemplateUpdatedData,
      initTemplate: initTemplate,
      isEmptyPages: isEmptyPages,
      orderPagesInTemplate: orderPagesInTemplate,
      resetTemplate: resetTemplate,
      restorePages: restorePages,
      selectPage: selectPage,
      selectedPageId: selectedPage && selectedPage.id,
      setTemplateBackup: setTemplateBackup,
      templateCreatedAt: currentTemplateFullData && currentTemplateFullData.created_at,
      templateProposalNumber: currentTemplateFullData && currentTemplateFullData.proposals_number,
      templateTitle: currentTemplateFullData && currentTemplateFullData.title,
      templateType: currentTemplateFullData && currentTemplateFullData.type,
      templateVersion: currentTemplateFullData && currentTemplateFullData.version
    }, _objectSpread({}, injectedProps)))
  });
};

var mapStateToProps = (0, _reselect.createStructuredSelector)({
  baseUrl: selectors.getBaseUrl,
  currencyConfig: selectors.getCurrencyConfig,
  currentTemplateFullData: templateCoreSelectors.getCurrentTemplateFullData,
  googleApiKey: selectors.getGoogleApiKey,
  isEmptyPages: templateCoreSelectors.getIsEmptyPages,
  language: selectors.getLanguage,
  selectedPage: templateViewSelectors.getSelectedPage,
  token: selectors.getToken
});

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    addNewPages: function addNewPages(pagesTemplate) {
      return dispatch(templateCoreActions.addNewPages(pagesTemplate));
    },
    deletePages: function deletePages(deletingId, pagesTemplate) {
      return dispatch(templateCoreActions.deletePages(deletingId, pagesTemplate));
    },
    getTemplateUpdatedData: function getTemplateUpdatedData() {
      return dispatch(actions.getTemplateUpdatedData());
    },
    initTemplate: function initTemplate(templateData, offerId) {
      return dispatch(actions.initialize(templateData, offerId));
    },
    onUpdatePage: function onUpdatePage(onChangeInPage, addStateDisabled) {
      return dispatch(templateViewActions.onUpdatePage(onChangeInPage, addStateDisabled));
    },
    reset: function reset(templateConfig) {
      return dispatch(actions.reset(templateConfig));
    },
    resetTemplate: function resetTemplate() {
      return dispatch(templateCoreActions.resetTemplate());
    },
    restorePages: function restorePages() {
      return dispatch(templateCoreActions.restorePages());
    },
    selectPage: function selectPage(pageId) {
      return dispatch(templateViewActions.selectPage(pageId));
    },
    setOrderPages: function setOrderPages(oldIndex, newIndex) {
      return dispatch(templateCoreActions.orderPages(oldIndex, newIndex));
    },
    setTemplateBackup: function setTemplateBackup(clean) {
      return dispatch(templateCoreActions.setTemplateBackup(clean));
    },
    setTemplateConfig: function setTemplateConfig(templateConfig) {
      return dispatch(actions.setTemplateConfig(templateConfig));
    }
  };
};

WithTemplateCore.propTypes = {
  Children: _propTypes.default.any,
  addNewPages: _propTypes.default.func,
  baseUrl: _propTypes.default.string,
  currencyConfig: _propTypes.default.object,
  currentTemplateFullData: _propTypes.default.object,
  deletePages: _propTypes.default.func,
  getTemplateUpdatedData: _propTypes.default.func,
  google: _propTypes.default.object,
  googleApiKey: _propTypes.default.string,
  initTemplate: _propTypes.default.func,
  injectedProps: _propTypes.default.object,
  isEmptyPages: _propTypes.default.bool,
  language: _propTypes.default.string,
  onUpdatePage: _propTypes.default.func,
  reset: _propTypes.default.func,
  resetTemplate: _propTypes.default.func,
  restorePages: _propTypes.default.func,
  selectPage: _propTypes.default.func,
  selectedPage: _propTypes.default.object,
  setOrderPages: _propTypes.default.func,
  setTemplateConfig: _propTypes.default.func,
  setTemplateBackup: _propTypes.default.func,
  token: _propTypes.default.string,
  templateConfig: _propTypes.default.object
};
var WithTemplateCoreComposed = (0, _recompose.compose)((0, _googleMapsReact.GoogleApiWrapper)(function (props) {
  return {
    apiKey: props.googleApiKey,
    libraries: ['geometry'],
    language: props.language,
    LoadingContainer: function LoadingContainer() {
      return null;
    }
  };
}), (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps))(WithTemplateCore);

var WithTemplateCoreHOC = function WithTemplateCoreHOC(getTemplateConfig) {
  return function (Children) {
    var WrappedComponent = function WrappedComponent(props) {
      return /*#__PURE__*/(0, _jsxRuntime.jsx)(WithTemplateCoreComposed, {
        Children: Children,
        injectedProps: props,
        templateConfig: getTemplateConfig(props)
      });
    };

    return WrappedComponent;
  };
};

var _default = WithTemplateCoreHOC;
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob2NzL3dpdGhUZW1wbGF0ZUNvcmUuanMiXSwibmFtZXMiOlsiV2l0aFRlbXBsYXRlQ29yZSIsIkNoaWxkcmVuIiwiYWRkTmV3UGFnZXMiLCJiYXNlVXJsIiwiY3VycmVuY3lDb25maWciLCJjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSIsImRlbGV0ZVBhZ2VzIiwiZ2V0VGVtcGxhdGVVcGRhdGVkRGF0YSIsImdvb2dsZSIsImdvb2dsZUFwaUtleSIsImluaXRUZW1wbGF0ZSIsImluamVjdGVkUHJvcHMiLCJpc0VtcHR5UGFnZXMiLCJvblVwZGF0ZVBhZ2UiLCJyZXNldCIsInJlc2V0VGVtcGxhdGUiLCJyZXN0b3JlUGFnZXMiLCJzZWxlY3RQYWdlIiwic2VsZWN0ZWRQYWdlIiwic2V0T3JkZXJQYWdlcyIsInNldFRlbXBsYXRlQ29uZmlnIiwic2V0VGVtcGxhdGVCYWNrdXAiLCJ0ZW1wbGF0ZUNvbmZpZyIsImxhbmd1YWdlIiwidXBkYXRlVGVtcGxhdGVQcm9taXNlIiwib25DaGFuZ2VJblBhZ2UiLCJhZGRCbGFua1BhZ2VJblRlbXBsYXRlIiwiZGVsZXRlUGFnZXNJblRlbXBsYXRlIiwiZHVwbGljYXRlUGFnZXNJblRlbXBsYXRlIiwib3JkZXJQYWdlc0luVGVtcGxhdGUiLCJ0YWdzTG9jYWxlIiwiY2hhcnRJc28iLCJjaGFydExvY2FsZSIsInRhYmxlSXNvIiwidGFibGVMb2NhbGUiLCJhZGRTdGF0ZURpc2FibGVkIiwiaWQiLCJjcmVhdGVkX2F0IiwicHJvcG9zYWxzX251bWJlciIsInRpdGxlIiwidHlwZSIsInZlcnNpb24iLCJtYXBTdGF0ZVRvUHJvcHMiLCJzZWxlY3RvcnMiLCJnZXRCYXNlVXJsIiwiZ2V0Q3VycmVuY3lDb25maWciLCJ0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMiLCJnZXRDdXJyZW50VGVtcGxhdGVGdWxsRGF0YSIsImdldEdvb2dsZUFwaUtleSIsImdldElzRW1wdHlQYWdlcyIsImdldExhbmd1YWdlIiwidGVtcGxhdGVWaWV3U2VsZWN0b3JzIiwiZ2V0U2VsZWN0ZWRQYWdlIiwidG9rZW4iLCJnZXRUb2tlbiIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwicGFnZXNUZW1wbGF0ZSIsInRlbXBsYXRlQ29yZUFjdGlvbnMiLCJkZWxldGluZ0lkIiwiYWN0aW9ucyIsInRlbXBsYXRlRGF0YSIsIm9mZmVySWQiLCJpbml0aWFsaXplIiwidGVtcGxhdGVWaWV3QWN0aW9ucyIsInBhZ2VJZCIsIm9sZEluZGV4IiwibmV3SW5kZXgiLCJvcmRlclBhZ2VzIiwiY2xlYW4iLCJwcm9wVHlwZXMiLCJQcm9wVHlwZXMiLCJhbnkiLCJmdW5jIiwic3RyaW5nIiwib2JqZWN0IiwiYm9vbCIsIldpdGhUZW1wbGF0ZUNvcmVDb21wb3NlZCIsInByb3BzIiwiYXBpS2V5IiwibGlicmFyaWVzIiwiTG9hZGluZ0NvbnRhaW5lciIsIldpdGhUZW1wbGF0ZUNvcmVIT0MiLCJnZXRUZW1wbGF0ZUNvbmZpZyIsIldyYXBwZWRDb21wb25lbnQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUNBOztBQUVBOztBQUVBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUNBOztBQUVBOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FBT0EsSUFBTUEsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixPQXVCbkI7QUFBQSxNQXRCRkMsUUFzQkUsUUF0QkZBLFFBc0JFO0FBQUEsTUFyQkZDLFdBcUJFLFFBckJGQSxXQXFCRTtBQUFBLE1BcEJGQyxPQW9CRSxRQXBCRkEsT0FvQkU7QUFBQSxNQW5CRkMsY0FtQkUsUUFuQkZBLGNBbUJFO0FBQUEsTUFsQkZDLHVCQWtCRSxRQWxCRkEsdUJBa0JFO0FBQUEsTUFqQkZDLFdBaUJFLFFBakJGQSxXQWlCRTtBQUFBLE1BaEJGQyxzQkFnQkUsUUFoQkZBLHNCQWdCRTtBQUFBLE1BZkZDLE1BZUUsUUFmRkEsTUFlRTtBQUFBLE1BZEZDLFlBY0UsUUFkRkEsWUFjRTtBQUFBLE1BYkZDLFlBYUUsUUFiRkEsWUFhRTtBQUFBLE1BWkZDLGFBWUUsUUFaRkEsYUFZRTtBQUFBLE1BWEZDLFlBV0UsUUFYRkEsWUFXRTtBQUFBLE1BVkZDLFlBVUUsUUFWRkEsWUFVRTtBQUFBLE1BVEZDLEtBU0UsUUFURkEsS0FTRTtBQUFBLE1BUkZDLGFBUUUsUUFSRkEsYUFRRTtBQUFBLE1BUEZDLFlBT0UsUUFQRkEsWUFPRTtBQUFBLE1BTkZDLFVBTUUsUUFORkEsVUFNRTtBQUFBLE1BTEZDLFlBS0UsUUFMRkEsWUFLRTtBQUFBLE1BSkZDLGFBSUUsUUFKRkEsYUFJRTtBQUFBLE1BSEZDLGlCQUdFLFFBSEZBLGlCQUdFO0FBQUEsTUFGRkMsaUJBRUUsUUFGRkEsaUJBRUU7QUFBQSxNQURGQyxjQUNFLFFBREZBLGNBQ0U7QUFDRixNQUFRQyxRQUFSLEdBQTRERCxjQUE1RCxDQUFRQyxRQUFSO0FBQUEsTUFBa0JDLHFCQUFsQixHQUE0REYsY0FBNUQsQ0FBa0JFLHFCQUFsQjtBQUFBLE1BQXlDQyxlQUF6QyxHQUE0REgsY0FBNUQsQ0FBeUNHLGNBQXpDO0FBQ0Esd0JBQVUsWUFBTTtBQUNaLFFBQUlGLFFBQUosRUFBYztBQUNWLDZDQUF5QkEsUUFBekI7QUFDSDs7QUFDREgsSUFBQUEsaUJBQWlCLENBQUNFLGNBQUQsQ0FBakI7QUFDQSxXQUFPLFlBQU07QUFDVFIsTUFBQUEsS0FBSztBQUNSLEtBRkQ7QUFHSCxHQVJELEVBUUcsRUFSSDtBQVVBLE1BQU1ZLHNCQUFzQixHQUFHLHdCQUMzQixzQ0FDSXJCLHVCQURKLEVBRUltQixxQkFGSixFQUdJdEIsV0FISixFQUlJZ0IsWUFKSixDQUQyQixFQU8zQixDQUNJYix1QkFESixFQUVJbUIscUJBRkosRUFHSXRCLFdBSEosRUFJSWdCLFlBSkosQ0FQMkIsQ0FBL0I7QUFlQSxNQUFNUyxxQkFBcUIsR0FBRyx3QkFDMUIsMENBQ0l0Qix1QkFESixFQUVJbUIscUJBRkosRUFHSWxCLFdBSEosQ0FEMEIsRUFNMUIsQ0FBQ0QsdUJBQUQsRUFBMEJtQixxQkFBMUIsRUFBaURsQixXQUFqRCxDQU4wQixDQUE5QjtBQVNBLE1BQU1zQix3QkFBd0IsR0FBRyx3QkFDN0IsNkNBQ0l2Qix1QkFESixFQUVJbUIscUJBRkosRUFHSXRCLFdBSEosQ0FENkIsRUFNN0IsQ0FBQ0csdUJBQUQsRUFBMEJtQixxQkFBMUIsRUFBaUR0QixXQUFqRCxDQU42QixDQUFqQztBQVNBLE1BQU0yQixvQkFBb0IsR0FBRyx3QkFDekIseUNBQ0l4Qix1QkFESixFQUVJbUIscUJBRkosRUFHSUwsYUFISixDQUR5QixFQU16QixDQUFDZCx1QkFBRCxFQUEwQm1CLHFCQUExQixFQUFpRHRCLFdBQWpELENBTnlCLENBQTdCOztBQVNBLE1BQUlDLE9BQU8sS0FBSyxJQUFaLElBQW9CTSxZQUFZLEtBQUssSUFBekMsRUFBK0M7QUFDM0MsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsTUFDSXFCLFVBREosR0FNSTFCLGNBTkosQ0FDSTBCLFVBREo7QUFBQSw4QkFNSTFCLGNBTkosQ0FFSTJCLFFBRko7QUFBQSxNQUVJQSxRQUZKLHNDQUVlLEtBRmY7QUFBQSwrQkFNSTNCLGNBTkosQ0FHSTRCLFdBSEo7QUFBQSxNQUdJQSxXQUhKLHVDQUdrQixPQUhsQjtBQUFBLDhCQU1JNUIsY0FOSixDQUlJNkIsUUFKSjtBQUFBLE1BSUlBLFFBSkosc0NBSWUsS0FKZjtBQUFBLCtCQU1JN0IsY0FOSixDQUtJOEIsV0FMSjtBQUFBLE1BS0lBLFdBTEosdUNBS2tCLE9BTGxCO0FBUUEsc0JBQ0kscUJBQUMseUJBQUQ7QUFDSSxJQUFBLEtBQUssRUFBRTtBQUNIMUIsTUFBQUEsTUFBTSxFQUFOQSxNQURHO0FBRUhpQixNQUFBQSxjQUFjLEVBQUUsd0JBQUNVLGdCQUFEO0FBQUEsZUFDWnRCLFlBQVksQ0FBQ1ksZUFBRCxFQUFpQlUsZ0JBQWpCLENBREE7QUFBQSxPQUZiO0FBSUhMLE1BQUFBLFVBQVUsRUFBVkEsVUFKRztBQUtIMUIsTUFBQUEsY0FBYyxFQUFFO0FBQ1oyQixRQUFBQSxRQUFRLEVBQVJBLFFBRFk7QUFFWkMsUUFBQUEsV0FBVyxFQUFYQSxXQUZZO0FBR1pDLFFBQUFBLFFBQVEsRUFBUkEsUUFIWTtBQUlaQyxRQUFBQSxXQUFXLEVBQVhBO0FBSlk7QUFMYixLQURYO0FBQUEsMkJBY0kscUJBQUMsUUFBRDtBQUNJLE1BQUEsV0FBVyxFQUFFaEMsV0FEakI7QUFFSSxNQUFBLHNCQUFzQixFQUFFd0Isc0JBRjVCO0FBR0ksTUFBQSxxQkFBcUIsRUFBRUMscUJBSDNCO0FBSUksTUFBQSx3QkFBd0IsRUFBRUMsd0JBSjlCO0FBS0ksTUFBQSxzQkFBc0IsRUFBRXJCLHNCQUw1QjtBQU1JLE1BQUEsWUFBWSxFQUFFRyxZQU5sQjtBQU9JLE1BQUEsWUFBWSxFQUFFRSxZQVBsQjtBQVFJLE1BQUEsb0JBQW9CLEVBQUVpQixvQkFSMUI7QUFTSSxNQUFBLGFBQWEsRUFBRWQsYUFUbkI7QUFVSSxNQUFBLFlBQVksRUFBRUMsWUFWbEI7QUFXSSxNQUFBLFVBQVUsRUFBRUMsVUFYaEI7QUFZSSxNQUFBLGNBQWMsRUFBRUMsWUFBWSxJQUFJQSxZQUFZLENBQUNrQixFQVpqRDtBQWFJLE1BQUEsaUJBQWlCLEVBQUVmLGlCQWJ2QjtBQWNJLE1BQUEsaUJBQWlCLEVBQ2JoQix1QkFBdUIsSUFDdkJBLHVCQUF1QixDQUFDZ0MsVUFoQmhDO0FBa0JJLE1BQUEsc0JBQXNCLEVBQ2xCaEMsdUJBQXVCLElBQ3ZCQSx1QkFBdUIsQ0FBQ2lDLGdCQXBCaEM7QUFzQkksTUFBQSxhQUFhLEVBQ1RqQyx1QkFBdUIsSUFBSUEsdUJBQXVCLENBQUNrQyxLQXZCM0Q7QUF5QkksTUFBQSxZQUFZLEVBQ1JsQyx1QkFBdUIsSUFBSUEsdUJBQXVCLENBQUNtQyxJQTFCM0Q7QUE0QkksTUFBQSxlQUFlLEVBQ1huQyx1QkFBdUIsSUFBSUEsdUJBQXVCLENBQUNvQztBQTdCM0QseUJBK0JhOUIsYUEvQmI7QUFkSixJQURKO0FBa0RILENBMUlEOztBQTRJQSxJQUFNK0IsZUFBZSxHQUFHLHdDQUF5QjtBQUM3Q3ZDLEVBQUFBLE9BQU8sRUFBRXdDLFNBQVMsQ0FBQ0MsVUFEMEI7QUFFN0N4QyxFQUFBQSxjQUFjLEVBQUV1QyxTQUFTLENBQUNFLGlCQUZtQjtBQUc3Q3hDLEVBQUFBLHVCQUF1QixFQUFFeUMscUJBQXFCLENBQUNDLDBCQUhGO0FBSTdDdEMsRUFBQUEsWUFBWSxFQUFFa0MsU0FBUyxDQUFDSyxlQUpxQjtBQUs3Q3BDLEVBQUFBLFlBQVksRUFBRWtDLHFCQUFxQixDQUFDRyxlQUxTO0FBTTdDMUIsRUFBQUEsUUFBUSxFQUFFb0IsU0FBUyxDQUFDTyxXQU55QjtBQU83Q2hDLEVBQUFBLFlBQVksRUFBRWlDLHFCQUFxQixDQUFDQyxlQVBTO0FBUTdDQyxFQUFBQSxLQUFLLEVBQUVWLFNBQVMsQ0FBQ1c7QUFSNEIsQ0FBekIsQ0FBeEI7O0FBV0EsSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBZTtBQUN0Q3RELElBQUFBLFdBQVcsRUFBRSxxQkFBQ3VELGFBQUQ7QUFBQSxhQUNURCxRQUFRLENBQUNFLG1CQUFtQixDQUFDeEQsV0FBcEIsQ0FBZ0N1RCxhQUFoQyxDQUFELENBREM7QUFBQSxLQUR5QjtBQUd0Q25ELElBQUFBLFdBQVcsRUFBRSxxQkFBQ3FELFVBQUQsRUFBYUYsYUFBYjtBQUFBLGFBQ1RELFFBQVEsQ0FBQ0UsbUJBQW1CLENBQUNwRCxXQUFwQixDQUFnQ3FELFVBQWhDLEVBQTRDRixhQUE1QyxDQUFELENBREM7QUFBQSxLQUh5QjtBQUt0Q2xELElBQUFBLHNCQUFzQixFQUFFO0FBQUEsYUFBTWlELFFBQVEsQ0FBQ0ksT0FBTyxDQUFDckQsc0JBQVIsRUFBRCxDQUFkO0FBQUEsS0FMYztBQU10Q0csSUFBQUEsWUFBWSxFQUFFLHNCQUFDbUQsWUFBRCxFQUFlQyxPQUFmO0FBQUEsYUFDVk4sUUFBUSxDQUFDSSxPQUFPLENBQUNHLFVBQVIsQ0FBbUJGLFlBQW5CLEVBQWlDQyxPQUFqQyxDQUFELENBREU7QUFBQSxLQU53QjtBQVF0Q2pELElBQUFBLFlBQVksRUFBRSxzQkFBQ1ksY0FBRCxFQUFpQlUsZ0JBQWpCO0FBQUEsYUFDVnFCLFFBQVEsQ0FDSlEsbUJBQW1CLENBQUNuRCxZQUFwQixDQUFpQ1ksY0FBakMsRUFBaURVLGdCQUFqRCxDQURJLENBREU7QUFBQSxLQVJ3QjtBQVl0Q3JCLElBQUFBLEtBQUssRUFBRSxlQUFDUSxjQUFEO0FBQUEsYUFBb0JrQyxRQUFRLENBQUNJLE9BQU8sQ0FBQzlDLEtBQVIsQ0FBY1EsY0FBZCxDQUFELENBQTVCO0FBQUEsS0FaK0I7QUFhdENQLElBQUFBLGFBQWEsRUFBRTtBQUFBLGFBQU15QyxRQUFRLENBQUNFLG1CQUFtQixDQUFDM0MsYUFBcEIsRUFBRCxDQUFkO0FBQUEsS0FidUI7QUFjdENDLElBQUFBLFlBQVksRUFBRTtBQUFBLGFBQU13QyxRQUFRLENBQUNFLG1CQUFtQixDQUFDMUMsWUFBcEIsRUFBRCxDQUFkO0FBQUEsS0Fkd0I7QUFldENDLElBQUFBLFVBQVUsRUFBRSxvQkFBQ2dELE1BQUQ7QUFBQSxhQUFZVCxRQUFRLENBQUNRLG1CQUFtQixDQUFDL0MsVUFBcEIsQ0FBK0JnRCxNQUEvQixDQUFELENBQXBCO0FBQUEsS0FmMEI7QUFnQnRDOUMsSUFBQUEsYUFBYSxFQUFFLHVCQUFDK0MsUUFBRCxFQUFXQyxRQUFYO0FBQUEsYUFDWFgsUUFBUSxDQUFDRSxtQkFBbUIsQ0FBQ1UsVUFBcEIsQ0FBK0JGLFFBQS9CLEVBQXlDQyxRQUF6QyxDQUFELENBREc7QUFBQSxLQWhCdUI7QUFrQnRDOUMsSUFBQUEsaUJBQWlCLEVBQUUsMkJBQUNnRCxLQUFEO0FBQUEsYUFDZmIsUUFBUSxDQUFDRSxtQkFBbUIsQ0FBQ3JDLGlCQUFwQixDQUFzQ2dELEtBQXRDLENBQUQsQ0FETztBQUFBLEtBbEJtQjtBQW9CdENqRCxJQUFBQSxpQkFBaUIsRUFBRSwyQkFBQ0UsY0FBRDtBQUFBLGFBQ2ZrQyxRQUFRLENBQUNJLE9BQU8sQ0FBQ3hDLGlCQUFSLENBQTBCRSxjQUExQixDQUFELENBRE87QUFBQTtBQXBCbUIsR0FBZjtBQUFBLENBQTNCOztBQXdCQXRCLGdCQUFnQixDQUFDc0UsU0FBakIsR0FBNkI7QUFDekJyRSxFQUFBQSxRQUFRLEVBQUVzRSxtQkFBVUMsR0FESztBQUV6QnRFLEVBQUFBLFdBQVcsRUFBRXFFLG1CQUFVRSxJQUZFO0FBR3pCdEUsRUFBQUEsT0FBTyxFQUFFb0UsbUJBQVVHLE1BSE07QUFJekJ0RSxFQUFBQSxjQUFjLEVBQUVtRSxtQkFBVUksTUFKRDtBQUt6QnRFLEVBQUFBLHVCQUF1QixFQUFFa0UsbUJBQVVJLE1BTFY7QUFNekJyRSxFQUFBQSxXQUFXLEVBQUVpRSxtQkFBVUUsSUFORTtBQU96QmxFLEVBQUFBLHNCQUFzQixFQUFFZ0UsbUJBQVVFLElBUFQ7QUFRekJqRSxFQUFBQSxNQUFNLEVBQUUrRCxtQkFBVUksTUFSTztBQVN6QmxFLEVBQUFBLFlBQVksRUFBRThELG1CQUFVRyxNQVRDO0FBVXpCaEUsRUFBQUEsWUFBWSxFQUFFNkQsbUJBQVVFLElBVkM7QUFXekI5RCxFQUFBQSxhQUFhLEVBQUU0RCxtQkFBVUksTUFYQTtBQVl6Qi9ELEVBQUFBLFlBQVksRUFBRTJELG1CQUFVSyxJQVpDO0FBYXpCckQsRUFBQUEsUUFBUSxFQUFFZ0QsbUJBQVVHLE1BYks7QUFjekI3RCxFQUFBQSxZQUFZLEVBQUUwRCxtQkFBVUUsSUFkQztBQWV6QjNELEVBQUFBLEtBQUssRUFBRXlELG1CQUFVRSxJQWZRO0FBZ0J6QjFELEVBQUFBLGFBQWEsRUFBRXdELG1CQUFVRSxJQWhCQTtBQWlCekJ6RCxFQUFBQSxZQUFZLEVBQUV1RCxtQkFBVUUsSUFqQkM7QUFrQnpCeEQsRUFBQUEsVUFBVSxFQUFFc0QsbUJBQVVFLElBbEJHO0FBbUJ6QnZELEVBQUFBLFlBQVksRUFBRXFELG1CQUFVSSxNQW5CQztBQW9CekJ4RCxFQUFBQSxhQUFhLEVBQUVvRCxtQkFBVUUsSUFwQkE7QUFxQnpCckQsRUFBQUEsaUJBQWlCLEVBQUVtRCxtQkFBVUUsSUFyQko7QUFzQnpCcEQsRUFBQUEsaUJBQWlCLEVBQUVrRCxtQkFBVUUsSUF0Qko7QUF1QnpCcEIsRUFBQUEsS0FBSyxFQUFFa0IsbUJBQVVHLE1BdkJRO0FBd0J6QnBELEVBQUFBLGNBQWMsRUFBRWlELG1CQUFVSTtBQXhCRCxDQUE3QjtBQTJCQSxJQUFNRSx3QkFBd0IsR0FBRyx3QkFDN0IsdUNBQWlCLFVBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxJQUFBQSxNQUFNLEVBQUVELEtBQUssQ0FBQ3JFLFlBRFc7QUFFekJ1RSxJQUFBQSxTQUFTLEVBQUUsQ0FBQyxVQUFELENBRmM7QUFHekJ6RCxJQUFBQSxRQUFRLEVBQUV1RCxLQUFLLENBQUN2RCxRQUhTO0FBSXpCMEQsSUFBQUEsZ0JBQWdCLEVBQUU7QUFBQSxhQUFNLElBQU47QUFBQTtBQUpPLEdBQVo7QUFBQSxDQUFqQixDQUQ2QixFQU83Qix5QkFBUXZDLGVBQVIsRUFBeUJhLGtCQUF6QixDQVA2QixFQVEvQnZELGdCQVIrQixDQUFqQzs7QUFVQSxJQUFNa0YsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDQyxpQkFBRDtBQUFBLFNBQXVCLFVBQUNsRixRQUFELEVBQWM7QUFDN0QsUUFBTW1GLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsQ0FBQ04sS0FBRCxFQUFXO0FBQ2hDLDBCQUNJLHFCQUFDLHdCQUFEO0FBRVE3RSxRQUFBQSxRQUFRLEVBQVJBLFFBRlI7QUFHUVUsUUFBQUEsYUFBYSxFQUFFbUUsS0FIdkI7QUFJUXhELFFBQUFBLGNBQWMsRUFBRTZELGlCQUFpQixDQUFDTCxLQUFEO0FBSnpDLFFBREo7QUFTSCxLQVZEOztBQVdBLFdBQU9NLGdCQUFQO0FBQ0gsR0FiMkI7QUFBQSxDQUE1Qjs7ZUFlZUYsbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBHb29nbGVBcGlXcmFwcGVyIH0gZnJvbSAnZ29vZ2xlLW1hcHMtcmVhY3QnO1xyXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xyXG5pbXBvcnQgeyB1c2VDYWxsYmFjaywgdXNlRWZmZWN0IH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xyXG5pbXBvcnQgeyBjb21wb3NlIH0gZnJvbSAncmVjb21wb3NlJztcclxuaW1wb3J0IHsgY3JlYXRlU3RydWN0dXJlZFNlbGVjdG9yIH0gZnJvbSAncmVzZWxlY3QnO1xyXG5cclxuaW1wb3J0IHsgR2VuZXJhbFByb3ZpZGVyIH0gZnJvbSAnQGhlbHBlcnMvY29udGV4dHMnO1xyXG5cclxuaW1wb3J0ICogYXMgYWN0aW9ucyBmcm9tICdAbWFpbi9hY3Rpb25zJztcclxuaW1wb3J0ICogYXMgc2VsZWN0b3JzIGZyb20gJ0BtYWluL3NlbGVjdG9ycyc7XHJcblxyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZVZpZXdBY3Rpb25zIGZyb20gJ0Btb2R1bGVzL1RlbXBsYXRlVmlldy9hY3Rpb25zJztcclxuaW1wb3J0ICogYXMgdGVtcGxhdGVWaWV3U2VsZWN0b3JzIGZyb20gJ0Btb2R1bGVzL1RlbXBsYXRlVmlldy9zZWxlY3RvcnMnO1xyXG5cclxuaW1wb3J0ICogYXMgdGVtcGxhdGVDb3JlQWN0aW9ucyBmcm9tICdAdGVtcGxhdGVDb3JlL2FjdGlvbnMnO1xyXG5pbXBvcnQgKiBhcyB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMgZnJvbSAnQHRlbXBsYXRlQ29yZS9zZWxlY3RvcnMnO1xyXG5cclxuaW1wb3J0IHsgaW5pdEludGVybmF0aW9uYWxpemF0aW9uIH0gZnJvbSAnLi4vaTE4bi9oZWxwZXJzJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBhZGRQYWdlSW5UZW1wbGF0ZUJ1aWxkLFxyXG4gICAgZGVsZXRlUGFnZXNJblRlbXBsYXRlQnVpbGQsXHJcbiAgICBkdXBsaWNhdGVQYWdlc0luVGVtcGxhdGVCdWlsZCxcclxuICAgIG9yZGVyUGFnZXNJblRlbXBsYXRlQnVpbGQsXHJcbn0gZnJvbSAnLi9oZWxwZXJzJztcclxuXHJcbmNvbnN0IFdpdGhUZW1wbGF0ZUNvcmUgPSAoe1xyXG4gICAgQ2hpbGRyZW4sXHJcbiAgICBhZGROZXdQYWdlcyxcclxuICAgIGJhc2VVcmwsXHJcbiAgICBjdXJyZW5jeUNvbmZpZyxcclxuICAgIGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLFxyXG4gICAgZGVsZXRlUGFnZXMsXHJcbiAgICBnZXRUZW1wbGF0ZVVwZGF0ZWREYXRhLFxyXG4gICAgZ29vZ2xlLFxyXG4gICAgZ29vZ2xlQXBpS2V5LFxyXG4gICAgaW5pdFRlbXBsYXRlLFxyXG4gICAgaW5qZWN0ZWRQcm9wcyxcclxuICAgIGlzRW1wdHlQYWdlcyxcclxuICAgIG9uVXBkYXRlUGFnZSxcclxuICAgIHJlc2V0LFxyXG4gICAgcmVzZXRUZW1wbGF0ZSxcclxuICAgIHJlc3RvcmVQYWdlcyxcclxuICAgIHNlbGVjdFBhZ2UsXHJcbiAgICBzZWxlY3RlZFBhZ2UsXHJcbiAgICBzZXRPcmRlclBhZ2VzLFxyXG4gICAgc2V0VGVtcGxhdGVDb25maWcsXHJcbiAgICBzZXRUZW1wbGF0ZUJhY2t1cCxcclxuICAgIHRlbXBsYXRlQ29uZmlnLFxyXG59KSA9PiB7XHJcbiAgICBjb25zdCB7IGxhbmd1YWdlLCB1cGRhdGVUZW1wbGF0ZVByb21pc2UsIG9uQ2hhbmdlSW5QYWdlIH0gPSB0ZW1wbGF0ZUNvbmZpZztcclxuICAgIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICAgICAgaWYgKGxhbmd1YWdlKSB7XHJcbiAgICAgICAgICAgIGluaXRJbnRlcm5hdGlvbmFsaXphdGlvbihsYW5ndWFnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNldFRlbXBsYXRlQ29uZmlnKHRlbXBsYXRlQ29uZmlnKTtcclxuICAgICAgICByZXR1cm4gKCkgPT4ge1xyXG4gICAgICAgICAgICByZXNldCgpO1xyXG4gICAgICAgIH07XHJcbiAgICB9LCBbXSk7XHJcblxyXG4gICAgY29uc3QgYWRkQmxhbmtQYWdlSW5UZW1wbGF0ZSA9IHVzZUNhbGxiYWNrKFxyXG4gICAgICAgIGFkZFBhZ2VJblRlbXBsYXRlQnVpbGQoXHJcbiAgICAgICAgICAgIGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLFxyXG4gICAgICAgICAgICB1cGRhdGVUZW1wbGF0ZVByb21pc2UsXHJcbiAgICAgICAgICAgIGFkZE5ld1BhZ2VzLFxyXG4gICAgICAgICAgICBzZWxlY3RlZFBhZ2VcclxuICAgICAgICApLFxyXG4gICAgICAgIFtcclxuICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGEsXHJcbiAgICAgICAgICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSxcclxuICAgICAgICAgICAgYWRkTmV3UGFnZXMsXHJcbiAgICAgICAgICAgIHNlbGVjdGVkUGFnZSxcclxuICAgICAgICBdXHJcbiAgICApO1xyXG5cclxuICAgIGNvbnN0IGRlbGV0ZVBhZ2VzSW5UZW1wbGF0ZSA9IHVzZUNhbGxiYWNrKFxyXG4gICAgICAgIGRlbGV0ZVBhZ2VzSW5UZW1wbGF0ZUJ1aWxkKFxyXG4gICAgICAgICAgICBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSxcclxuICAgICAgICAgICAgdXBkYXRlVGVtcGxhdGVQcm9taXNlLFxyXG4gICAgICAgICAgICBkZWxldGVQYWdlc1xyXG4gICAgICAgICksXHJcbiAgICAgICAgW2N1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLCB1cGRhdGVUZW1wbGF0ZVByb21pc2UsIGRlbGV0ZVBhZ2VzXVxyXG4gICAgKTtcclxuXHJcbiAgICBjb25zdCBkdXBsaWNhdGVQYWdlc0luVGVtcGxhdGUgPSB1c2VDYWxsYmFjayhcclxuICAgICAgICBkdXBsaWNhdGVQYWdlc0luVGVtcGxhdGVCdWlsZChcclxuICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGEsXHJcbiAgICAgICAgICAgIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSxcclxuICAgICAgICAgICAgYWRkTmV3UGFnZXNcclxuICAgICAgICApLFxyXG4gICAgICAgIFtjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSwgdXBkYXRlVGVtcGxhdGVQcm9taXNlLCBhZGROZXdQYWdlc11cclxuICAgICk7XHJcblxyXG4gICAgY29uc3Qgb3JkZXJQYWdlc0luVGVtcGxhdGUgPSB1c2VDYWxsYmFjayhcclxuICAgICAgICBvcmRlclBhZ2VzSW5UZW1wbGF0ZUJ1aWxkKFxyXG4gICAgICAgICAgICBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSxcclxuICAgICAgICAgICAgdXBkYXRlVGVtcGxhdGVQcm9taXNlLFxyXG4gICAgICAgICAgICBzZXRPcmRlclBhZ2VzXHJcbiAgICAgICAgKSxcclxuICAgICAgICBbY3VycmVudFRlbXBsYXRlRnVsbERhdGEsIHVwZGF0ZVRlbXBsYXRlUHJvbWlzZSwgYWRkTmV3UGFnZXNdXHJcbiAgICApO1xyXG5cclxuICAgIGlmIChiYXNlVXJsID09PSBudWxsIHx8IGdvb2dsZUFwaUtleSA9PT0gbnVsbCkge1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG4gICAgY29uc3Qge1xyXG4gICAgICAgIHRhZ3NMb2NhbGUsXHJcbiAgICAgICAgY2hhcnRJc28gPSAnVVNEJyxcclxuICAgICAgICBjaGFydExvY2FsZSA9ICdlbi1VUycsXHJcbiAgICAgICAgdGFibGVJc28gPSAnVVNEJyxcclxuICAgICAgICB0YWJsZUxvY2FsZSA9ICdlbi1VUycsXHJcbiAgICB9ID0gY3VycmVuY3lDb25maWc7XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8R2VuZXJhbFByb3ZpZGVyXHJcbiAgICAgICAgICAgIHZhbHVlPXt7XHJcbiAgICAgICAgICAgICAgICBnb29nbGUsXHJcbiAgICAgICAgICAgICAgICBvbkNoYW5nZUluUGFnZTogKGFkZFN0YXRlRGlzYWJsZWQpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgb25VcGRhdGVQYWdlKG9uQ2hhbmdlSW5QYWdlLCBhZGRTdGF0ZURpc2FibGVkKSxcclxuICAgICAgICAgICAgICAgIHRhZ3NMb2NhbGUsXHJcbiAgICAgICAgICAgICAgICBjdXJyZW5jeUNvbmZpZzoge1xyXG4gICAgICAgICAgICAgICAgICAgIGNoYXJ0SXNvLFxyXG4gICAgICAgICAgICAgICAgICAgIGNoYXJ0TG9jYWxlLFxyXG4gICAgICAgICAgICAgICAgICAgIHRhYmxlSXNvLFxyXG4gICAgICAgICAgICAgICAgICAgIHRhYmxlTG9jYWxlLFxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICA+XHJcbiAgICAgICAgICAgIDxDaGlsZHJlblxyXG4gICAgICAgICAgICAgICAgYWRkTmV3UGFnZXM9e2FkZE5ld1BhZ2VzfVxyXG4gICAgICAgICAgICAgICAgYWRkQmxhbmtQYWdlSW5UZW1wbGF0ZT17YWRkQmxhbmtQYWdlSW5UZW1wbGF0ZX1cclxuICAgICAgICAgICAgICAgIGRlbGV0ZVBhZ2VzSW5UZW1wbGF0ZT17ZGVsZXRlUGFnZXNJblRlbXBsYXRlfVxyXG4gICAgICAgICAgICAgICAgZHVwbGljYXRlUGFnZXNJblRlbXBsYXRlPXtkdXBsaWNhdGVQYWdlc0luVGVtcGxhdGV9XHJcbiAgICAgICAgICAgICAgICBnZXRUZW1wbGF0ZVVwZGF0ZWREYXRhPXtnZXRUZW1wbGF0ZVVwZGF0ZWREYXRhfVxyXG4gICAgICAgICAgICAgICAgaW5pdFRlbXBsYXRlPXtpbml0VGVtcGxhdGV9XHJcbiAgICAgICAgICAgICAgICBpc0VtcHR5UGFnZXM9e2lzRW1wdHlQYWdlc31cclxuICAgICAgICAgICAgICAgIG9yZGVyUGFnZXNJblRlbXBsYXRlPXtvcmRlclBhZ2VzSW5UZW1wbGF0ZX1cclxuICAgICAgICAgICAgICAgIHJlc2V0VGVtcGxhdGU9e3Jlc2V0VGVtcGxhdGV9XHJcbiAgICAgICAgICAgICAgICByZXN0b3JlUGFnZXM9e3Jlc3RvcmVQYWdlc31cclxuICAgICAgICAgICAgICAgIHNlbGVjdFBhZ2U9e3NlbGVjdFBhZ2V9XHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZFBhZ2VJZD17c2VsZWN0ZWRQYWdlICYmIHNlbGVjdGVkUGFnZS5pZH1cclxuICAgICAgICAgICAgICAgIHNldFRlbXBsYXRlQmFja3VwPXtzZXRUZW1wbGF0ZUJhY2t1cH1cclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlQ3JlYXRlZEF0PXtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSAmJlxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLmNyZWF0ZWRfYXRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlUHJvcG9zYWxOdW1iZXI9e1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhICYmXHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGEucHJvcG9zYWxzX251bWJlclxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVUaXRsZT17XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGEgJiYgY3VycmVudFRlbXBsYXRlRnVsbERhdGEudGl0bGVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVHlwZT17XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGEgJiYgY3VycmVudFRlbXBsYXRlRnVsbERhdGEudHlwZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVWZXJzaW9uPXtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YSAmJiBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YS52ZXJzaW9uXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB7Li4ueyAuLi5pbmplY3RlZFByb3BzIH19XHJcbiAgICAgICAgICAgID48L0NoaWxkcmVuPlxyXG4gICAgICAgIDwvR2VuZXJhbFByb3ZpZGVyPlxyXG4gICAgKTtcclxufTtcclxuXHJcbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IGNyZWF0ZVN0cnVjdHVyZWRTZWxlY3Rvcih7XHJcbiAgICBiYXNlVXJsOiBzZWxlY3RvcnMuZ2V0QmFzZVVybCxcclxuICAgIGN1cnJlbmN5Q29uZmlnOiBzZWxlY3RvcnMuZ2V0Q3VycmVuY3lDb25maWcsXHJcbiAgICBjdXJyZW50VGVtcGxhdGVGdWxsRGF0YTogdGVtcGxhdGVDb3JlU2VsZWN0b3JzLmdldEN1cnJlbnRUZW1wbGF0ZUZ1bGxEYXRhLFxyXG4gICAgZ29vZ2xlQXBpS2V5OiBzZWxlY3RvcnMuZ2V0R29vZ2xlQXBpS2V5LFxyXG4gICAgaXNFbXB0eVBhZ2VzOiB0ZW1wbGF0ZUNvcmVTZWxlY3RvcnMuZ2V0SXNFbXB0eVBhZ2VzLFxyXG4gICAgbGFuZ3VhZ2U6IHNlbGVjdG9ycy5nZXRMYW5ndWFnZSxcclxuICAgIHNlbGVjdGVkUGFnZTogdGVtcGxhdGVWaWV3U2VsZWN0b3JzLmdldFNlbGVjdGVkUGFnZSxcclxuICAgIHRva2VuOiBzZWxlY3RvcnMuZ2V0VG9rZW4sXHJcbn0pO1xyXG5cclxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiAoe1xyXG4gICAgYWRkTmV3UGFnZXM6IChwYWdlc1RlbXBsYXRlKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKHRlbXBsYXRlQ29yZUFjdGlvbnMuYWRkTmV3UGFnZXMocGFnZXNUZW1wbGF0ZSkpLFxyXG4gICAgZGVsZXRlUGFnZXM6IChkZWxldGluZ0lkLCBwYWdlc1RlbXBsYXRlKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKHRlbXBsYXRlQ29yZUFjdGlvbnMuZGVsZXRlUGFnZXMoZGVsZXRpbmdJZCwgcGFnZXNUZW1wbGF0ZSkpLFxyXG4gICAgZ2V0VGVtcGxhdGVVcGRhdGVkRGF0YTogKCkgPT4gZGlzcGF0Y2goYWN0aW9ucy5nZXRUZW1wbGF0ZVVwZGF0ZWREYXRhKCkpLFxyXG4gICAgaW5pdFRlbXBsYXRlOiAodGVtcGxhdGVEYXRhLCBvZmZlcklkKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKGFjdGlvbnMuaW5pdGlhbGl6ZSh0ZW1wbGF0ZURhdGEsIG9mZmVySWQpKSxcclxuICAgIG9uVXBkYXRlUGFnZTogKG9uQ2hhbmdlSW5QYWdlLCBhZGRTdGF0ZURpc2FibGVkKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKFxyXG4gICAgICAgICAgICB0ZW1wbGF0ZVZpZXdBY3Rpb25zLm9uVXBkYXRlUGFnZShvbkNoYW5nZUluUGFnZSwgYWRkU3RhdGVEaXNhYmxlZClcclxuICAgICAgICApLFxyXG4gICAgcmVzZXQ6ICh0ZW1wbGF0ZUNvbmZpZykgPT4gZGlzcGF0Y2goYWN0aW9ucy5yZXNldCh0ZW1wbGF0ZUNvbmZpZykpLFxyXG4gICAgcmVzZXRUZW1wbGF0ZTogKCkgPT4gZGlzcGF0Y2godGVtcGxhdGVDb3JlQWN0aW9ucy5yZXNldFRlbXBsYXRlKCkpLFxyXG4gICAgcmVzdG9yZVBhZ2VzOiAoKSA9PiBkaXNwYXRjaCh0ZW1wbGF0ZUNvcmVBY3Rpb25zLnJlc3RvcmVQYWdlcygpKSxcclxuICAgIHNlbGVjdFBhZ2U6IChwYWdlSWQpID0+IGRpc3BhdGNoKHRlbXBsYXRlVmlld0FjdGlvbnMuc2VsZWN0UGFnZShwYWdlSWQpKSxcclxuICAgIHNldE9yZGVyUGFnZXM6IChvbGRJbmRleCwgbmV3SW5kZXgpID0+XHJcbiAgICAgICAgZGlzcGF0Y2godGVtcGxhdGVDb3JlQWN0aW9ucy5vcmRlclBhZ2VzKG9sZEluZGV4LCBuZXdJbmRleCkpLFxyXG4gICAgc2V0VGVtcGxhdGVCYWNrdXA6IChjbGVhbikgPT5cclxuICAgICAgICBkaXNwYXRjaCh0ZW1wbGF0ZUNvcmVBY3Rpb25zLnNldFRlbXBsYXRlQmFja3VwKGNsZWFuKSksXHJcbiAgICBzZXRUZW1wbGF0ZUNvbmZpZzogKHRlbXBsYXRlQ29uZmlnKSA9PlxyXG4gICAgICAgIGRpc3BhdGNoKGFjdGlvbnMuc2V0VGVtcGxhdGVDb25maWcodGVtcGxhdGVDb25maWcpKSxcclxufSk7XHJcblxyXG5XaXRoVGVtcGxhdGVDb3JlLnByb3BUeXBlcyA9IHtcclxuICAgIENoaWxkcmVuOiBQcm9wVHlwZXMuYW55LFxyXG4gICAgYWRkTmV3UGFnZXM6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgYmFzZVVybDogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGN1cnJlbmN5Q29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgY3VycmVudFRlbXBsYXRlRnVsbERhdGE6IFByb3BUeXBlcy5vYmplY3QsXHJcbiAgICBkZWxldGVQYWdlczogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBnZXRUZW1wbGF0ZVVwZGF0ZWREYXRhOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIGdvb2dsZTogUHJvcFR5cGVzLm9iamVjdCxcclxuICAgIGdvb2dsZUFwaUtleTogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIGluaXRUZW1wbGF0ZTogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBpbmplY3RlZFByb3BzOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgaXNFbXB0eVBhZ2VzOiBQcm9wVHlwZXMuYm9vbCxcclxuICAgIGxhbmd1YWdlOiBQcm9wVHlwZXMuc3RyaW5nLFxyXG4gICAgb25VcGRhdGVQYWdlOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIHJlc2V0OiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIHJlc2V0VGVtcGxhdGU6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgcmVzdG9yZVBhZ2VzOiBQcm9wVHlwZXMuZnVuYyxcclxuICAgIHNlbGVjdFBhZ2U6IFByb3BUeXBlcy5mdW5jLFxyXG4gICAgc2VsZWN0ZWRQYWdlOiBQcm9wVHlwZXMub2JqZWN0LFxyXG4gICAgc2V0T3JkZXJQYWdlczogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZXRUZW1wbGF0ZUNvbmZpZzogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICBzZXRUZW1wbGF0ZUJhY2t1cDogUHJvcFR5cGVzLmZ1bmMsXHJcbiAgICB0b2tlbjogUHJvcFR5cGVzLnN0cmluZyxcclxuICAgIHRlbXBsYXRlQ29uZmlnOiBQcm9wVHlwZXMub2JqZWN0LFxyXG59O1xyXG5cclxuY29uc3QgV2l0aFRlbXBsYXRlQ29yZUNvbXBvc2VkID0gY29tcG9zZShcclxuICAgIEdvb2dsZUFwaVdyYXBwZXIoKHByb3BzKSA9PiAoe1xyXG4gICAgICAgIGFwaUtleTogcHJvcHMuZ29vZ2xlQXBpS2V5LFxyXG4gICAgICAgIGxpYnJhcmllczogWydnZW9tZXRyeSddLFxyXG4gICAgICAgIGxhbmd1YWdlOiBwcm9wcy5sYW5ndWFnZSxcclxuICAgICAgICBMb2FkaW5nQ29udGFpbmVyOiAoKSA9PiBudWxsLFxyXG4gICAgfSkpLFxyXG4gICAgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcylcclxuKShXaXRoVGVtcGxhdGVDb3JlKTtcclxuXHJcbmNvbnN0IFdpdGhUZW1wbGF0ZUNvcmVIT0MgPSAoZ2V0VGVtcGxhdGVDb25maWcpID0+IChDaGlsZHJlbikgPT4ge1xyXG4gICAgY29uc3QgV3JhcHBlZENvbXBvbmVudCA9IChwcm9wcykgPT4ge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxXaXRoVGVtcGxhdGVDb3JlQ29tcG9zZWRcclxuICAgICAgICAgICAgICAgIHsuLi57XHJcbiAgICAgICAgICAgICAgICAgICAgQ2hpbGRyZW4sXHJcbiAgICAgICAgICAgICAgICAgICAgaW5qZWN0ZWRQcm9wczogcHJvcHMsXHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVDb25maWc6IGdldFRlbXBsYXRlQ29uZmlnKHByb3BzKSxcclxuICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgIH07XHJcbiAgICByZXR1cm4gV3JhcHBlZENvbXBvbmVudDtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdpdGhUZW1wbGF0ZUNvcmVIT0M7XHJcbiJdfQ==