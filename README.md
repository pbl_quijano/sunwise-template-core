
# Sunwise Template Core
Esta librería se encarga de construir las vistas de plantillas y maneja sus interacciones. 
## Consideraciones
Necesita de datos de configuración para poder usarse, así como la estructura de datos de Plantillas obtenidas del servicio correspondiente. La estructura de datos de plantillas debe ser la usual. No utiliza servicios que envíen información al servidor (a excepción del servicio de carga de imágenes), por lo que los servicios de actualización de plantillas deben pasarse en la configuración. El alma de esta librería es un HOC llamado **withTemplateCore**, para el funcionamiento correcto, todas las vistas y HOC´s deben estar dentro del componente contenido por este HOC.
## Instalacion
Pendiente...

    yarn add 

## Configuración inicial
### *Configurar Reducer*
En el reducer principal

    import { reducer  as  templatesReducer } from  'sunwise-template-core';
    ...
    export default (history) =>
	    combineReducers({
		    sunwiseTemplateCore: templatesReducer,
		    ...
		});
### *Configurar HOC*
En el componente padre o donde se va a utilizar las plantillas

    import { withTemplateCore } from  'sunwise-template-core';
        ...
        Component
        ....
        export default withTemplateCore((props) => ({
	        ...templateConfig
        }))(Component)

### *Inicializar Plantilla*
Usar la función **initTemplate** para inicializar la construcción de plantillas con datos.
    
    const Component= ({
        ...,
        initTemplate
    }) => {
	    ...
	    initTemplate(templateData, proposalID);
    }

## HOC
### withTemplateCore

    import { withTemplateCore } from 'sunwise-template-core';

#### *Parámetros*
|  |Tipo|Requerido|Descripción |
|--|----|------------|------------|
|**baseUrl**|`string`|Si|URL para la construcción de los servicios |
|**token**|`string`|Si|Para la autenticación de los servicios            |
|**language**|`string`|Si|Idioma para los componentes ajenos a la plantilla|
|**froalaLicenseKey**|`string`|Si|Licencia de Froala|
|**googleApiKey**|`string`|Si|Licencia de Google Maps|
|**onChangeInPage**|`function`|No|Función de sobrecarga para los cambios de página. Devuelve en los parámetros la nueva estructura para enviarla al servidor|
|**updateTemplatePromise**|`function`|No|Función de sobrecarga para reflejar los cambios obtenidos del servidor en la plantilla|

#### *Funciones devueltas en Props*
|  |Parámetros|Descripción |
|--|----|------------|
|**initTemplate**|*TemplateData*:`Object`, *ProposalID*:`string`(opcional)|Construye la vista de la plantilla, páginas, grupos y widgets. *TemplateData* es la estructura de datos de plantilla obtenida de un servicio y *ProposalID* es el ID de la propuesta, requerida para los casos en que la plantilla ya tenga una relación con una propuesta |
|**getTemplateUpdatedData**||Retorna la estructura de datos con la información actual de la plantilla. Esta estructura está lista para se convertida en una cadena de texto para ser enviada a algún servicio. |
|**setTemplateBackup**|*clean*:`boolean`(opcional)|Guarda una copia de seguridad de la plantilla en el estado actual. Si se le pasa el parámetro *clean* como **true** se borra la copia de seguridad |
|**resetTemplate**||Limpia los datos de la plantilla, ahora puede inicializar una nueva plantilla|
|**restorePages**||Restaura una copia de seguridad. |
|**addNewPages**|*pagesTemplate*:`Array`|Agrega o actualiza páginas de la plantilla en relación al párametro *pagesTemplate* que es recibido de un servicio|
|**addBlankPageInTemplate**||Agrega una página en blanco a la plantilla. Esta función requiere que **updateTemplatePromise** se haya agregado como parametro del HOC|
|**deletePagesInTemplate**|*deletingId*:`string`|Elimina una página o un grupo de páginas de la plantilla. *deletingId* es el id de la página o grupo que se desea eliminar. Esta función requiere que **updateTemplatePromise** se haya agregado como parámetro del HOC|
|**duplicatePagesInTemplate**|*duplicatedPage*:`Object`|Duplica una página de la plantilla.*duplicatedPage* son los datos de la página que se desea duplicar. Esta función requiere que **updateTemplatePromise** se haya agregado como parámetro del HOC|
|**orderPagesInTemplate**|*oldIndex*:`integer`, *newIndex*:`integer`|Por medio del cambio del índice de una página se modifica el orden de las páginas requeridas.*oldIndex* es el índice actual de la página y *newIndex* es el índice nuevo de la página. Esta función requiere que **updateTemplatePromise** se haya agregado como parámetro del HOC|
|**selectPage**|*pageId*:`string`|Selecciona una página por medio de su ID. *pageId* es el ID de la página requerida|

#### *Variables devueltas en Props*
|  |Tipo|Descripción |
|--|----|------------|
|**isEmptyPages**|`boolean`|Su valor es **true** cuando la plantilla no tiene páginas|
|**selectedPageId**|`string`|ID de la página selecionada|
|**templateCreatedAt**|`string`|Fecha de creación de la plantilla|
|**templateProposalNumber**|`integer`|Número de propuestas de la plantilla|
|**templateTitle**|`string`|Nombre de la plantilla|
|**templateType**|`integer`|Tipo de la plantilla|
|**templateVersion**|`integer`|Versión de la plantilla|

### withTemplatePages
    import { withTemplatePages } from 'sunwise-template-core';
#### *No requiere parámetros*
#### *Variables devueltas en Props*
|  |Tipo|Descripción |
|--|----|------------|
|**templatePages**|`array`|Páginas de las plantillas|

## Constantes
### types
    import { types } from 'sunwise-template-core';
|  |Descripción |
|--|------------|
|**ONE_PROPOSAL_TYPE**|Tipo propuesta|
|**MULTIPROPOSAL_TYPE**|Tipo resumen|
|**SMART_DOCUMENTS_TYPE**|Tipo documento inteligente|
### editionLevels
    import { editionLevels } from 'sunwise-template-core';
|  |Descripción |
|--|------------|
|**NO_EDITION_MODE**|No se puede modificar|
|**PARTIAL_EDITION_MODE**|Se pueden modificar algunas cosas (como en el previsualizador)|
|**FULL_EDITION_MODE**|Edición total|

## Componentes
### TemplateView
    import { TemplateView } from 'sunwise-template-core';
Vista que muestra las páginas de plantilla dependiendo del nivel de edición configurado.
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
|**editionLevel**|`number`|Si||Requiere alguna constante de **types** |
|**horizontalScrollMode**|`boolean`|No|false|Apila las paginas de forma horizontal |
|**infinitePagesSupportEnabled**|`boolean`|No|true|Aplica la configuración para las páginas infinitas |

### PageToolbar
    import { PageToolbar } from 'sunwise-template-core';
Barra de herramientas para la edición de plantillas.
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
|**editionLevel**|`number`|No|FULL_EDITION_MODE|Requiere alguna constante de **editionLevels** |
|**visible**|`boolean`|No|true|La vista es visible |

### PageProperties
    import { PageProperties } from 'sunwise-template-core';
Sección de formulario para la configuración de la página.
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
|**themeEnabled**|`boolean`|No|true|Muestra el formulario  |

### PageThumbnail
    import { PageThumbnail } from 'sunwise-template-core';
Vista que muestra la página en miniatura y no puede editarse.
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
|**page**|`object`|Si||Datos de la página a visualizar|

### WidgetMenu
    import { WidgetMenu } from 'sunwise-template-core';
Lista de widgets disponibles para agregar.
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
|**canUpdatePanelsSowing**|`boolean`|Si|Se puede agregar widget de Sembrado de paneles |
|**canUpdateProjectLocation**|`boolean`|Si|Se puede agregar widget de Mapa |
|**disabled**|`boolean`|No|false|Esta sección está deshabilitada |

### WidgetSideMenu
    import { WidgetSideMenu } from 'sunwise-template-core';
Vista que muestra **PageProperties** y **WidgetMenu** .
|  |Tipo|Requerido|Default|Descripción |
|--|----|------------|------------|------------|
**canUpdatePanelsSowing**|`boolean`|Si|Se puede agregar widget de Sembrado de paneles |
|**canUpdateProjectLocation**|`boolean`|Si|Se puede agregar widget de Mapa |
|**selectedPageIsBlocked**|`boolean`|No|false|Esta sección está deshabilitada |
|**themeEnabled**|`boolean`|No|true|Muestra el formulario |


## Todo's
 - [ ] Dejar de utilizar React Bootstrap
 - [ ] Optimizar Build
 - [ ] Mejorar Exportaciones
 - [ ] Aplicar el suo de APIS sin autenticación
 - [ ] Consit