import {
    ONE_PROPOSAL_TYPE,
    MULTIPROPOSAL_TYPE,
    SMART_DOCUMENTS_TYPE,
    NO_EDITION_MODE,
    PARTIAL_EDITION_MODE,
    FULL_EDITION_MODE,
} from './constants/types';

export { default as withTemplateCore } from './hocs/withTemplateCore';
export { default as withTemplatePages } from './hocs/withTemplatePages';
export { default as TemplateView } from './modules/TemplateView';
export { default as PageProperties } from './modules/PageProperties';
export { default as PageThumbnail } from './modules/TemplateView/components/pages/PageThumbnail';
export { default as PageToolbar } from './modules/TemplateView/components/PageToolbar';
export { default as reducer } from './reducer';
export { default as WidgetMenu } from './modules/WidgetMenu';
export { default as WidgetSideMenu } from './components/WidgetSideMenu';
export const types = {
    ONE_PROPOSAL_TYPE,
    MULTIPROPOSAL_TYPE,
    SMART_DOCUMENTS_TYPE,
};
export const editionLevels = {
    NO_EDITION_MODE,
    PARTIAL_EDITION_MODE,
    FULL_EDITION_MODE,
};
