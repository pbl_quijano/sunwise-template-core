import {
    ONE_PROPOSAL_TYPE,
    MULTIPROPOSAL_TYPE,
    SMART_DOCUMENTS_TYPE,
} from '../constants/types';
export default {
    ONE_PROPOSAL_TYPE,
    MULTIPROPOSAL_TYPE,
    SMART_DOCUMENTS_TYPE,
};
