import getApi from './getApi';

const getTags = (state) => () => getApi(state).get('/api/v1/tags-values/');

export default getTags;
