import getApi from './getApi';

const getImageGallery = (state) => () =>
    getApi(state).get('/api/v1/logo-companies-tags/');

export default getImageGallery;
