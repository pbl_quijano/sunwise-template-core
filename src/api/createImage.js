import getApi from './getApi';

const createImage = (state) => (data) =>
    getApi(state).post('/api/v1/templates-images/', { ...data });

export default createImage;
