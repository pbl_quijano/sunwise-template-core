import getApi from './getApi';

const getCatalogs = (state) => (type) =>
    getApi(state).get(`/api/v1/companies-catalogs/${type}`);

export default getCatalogs;
