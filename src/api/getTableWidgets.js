import getApi from './getApi';

const getTableWidgets = (state) => (lang) =>
    getApi(state).get(`/api/v1/tables-values/${lang ? `?lang=${lang}` : ''}`);

export default getTableWidgets;
