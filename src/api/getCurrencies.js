import getApi from './getApi';

const getCurrencies = (state) => () =>
    getApi(state).get('/api/v1/companies-currencies/');

export default getCurrencies;
