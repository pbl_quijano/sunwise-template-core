import getApi from './getApi';

const getChartCurrency = (state) => () => getApi(state).get('/api/v1/me/');

export default getChartCurrency;
