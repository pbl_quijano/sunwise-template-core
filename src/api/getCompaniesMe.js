import getApi from './getApi';

const getCompaniesMe = (state) => () =>
    getApi(state).get('/api/v1/companies/me/');

export default getCompaniesMe;
