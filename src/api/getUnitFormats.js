import getApi from './getApi';

const getUnitFormats = (state) => () =>
    getApi(state).get('/api/v1/units-formats/');

export default getUnitFormats;
