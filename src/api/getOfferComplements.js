import getApi from './getApi';

const getOfferComplements = (state) => (commercialOfferId) =>
    getApi(state).get(`/api/v1/offer-complements/${commercialOfferId}`);

export default getOfferComplements;
