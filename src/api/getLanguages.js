import getApi from './getApi';

const getLanguages = (state) => () =>
    getApi(state).get('/api/v1/international/languages/');

export default getLanguages;
