import getApi from './getApi';

const getTagsFormats = (state) => () =>
    getApi(state).get('/api/v1/tags-formats/');

export default getTagsFormats;
