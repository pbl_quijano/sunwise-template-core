import getApi from './getApi';

const getChartWidgets = (state) => (lang) =>
    getApi(state).get(`/api/v1/graph-values/${lang ? `?lang=${lang}` : ''}`);

export default getChartWidgets;
