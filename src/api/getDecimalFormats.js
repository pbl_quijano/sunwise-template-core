import getApi from './getApi';

const getDecimalFormats = (state) => () =>
    getApi(state).get('/api/v1/decimals-formats/');

export default getDecimalFormats;
