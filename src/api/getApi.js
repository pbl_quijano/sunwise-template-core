import axios from 'axios';
import objectToFormdata from 'object-to-formdata';

import * as mainSelectors from '@main/selectors';

const getApi = (state) => {
    const baseURL = mainSelectors.getBaseUrl(state);
    const token = mainSelectors.getToken(state);
    const API = axios.create({
        baseURL,
    });
    API.interceptors.request.use((req) => {
        req.headers['Authorization'] = `JWT ${token}`;
        if (
            req.headers['Content-Type'] === 'multipart/form-data' &&
            (req.method === 'post' || req.method === 'put')
        ) {
            req.data = objectToFormdata(req.data, { indices: true });
        }

        return req;
    });
    API.interceptors.response.use(
        (res) => {
            return res;
        },
        (error) => {
            if (
                error.code === 'ECONNABORTED' ||
                typeof error.response === 'undefined'
            ) {
                const _error = Object.assign({}, error);

                return Promise.reject({
                    ..._error,
                    data: { errors: ['Ocurrio un error de conexion'] },
                    response: {
                        data: { errors: ['Ocurrio un error de conexion'] },
                    },
                });
            }

            return Promise.reject(error);
        }
    );
    return API;
};

export default getApi;
