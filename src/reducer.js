import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import mainReducer from '@main/reducer';

import imageUploaderReducer from '@modules/ImageUploader/reducer';
import pagePropertiesReducer from '@modules/PageProperties/reducer';
import tagSystemReducer from '@modules/TagSystem/reducer';
import templateViewReducer from '@modules/TemplateView/reducer';

import templateCoreReducer from '@templateCore/reducer';

export default combineReducers({
    form: formReducer,
    imageUploader: imageUploaderReducer,
    main: mainReducer,
    pageProperties: pagePropertiesReducer,
    tagSystem: tagSystemReducer,
    templateCore: templateCoreReducer,
    templateView: templateViewReducer,
});
