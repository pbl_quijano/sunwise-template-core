export const SEGMENT_COLOR = '#FF9A0080';
export const SEGMENT_STROKE_COLOR = '#FF9A00FF';
export const SEGMENT_ACTIVE_COLOR = '#FF9A00B3';
export const SEGMENT_STROKE_ACTIVE_COLOR = '#FF9A00FF';
export const SOLAR_MODULE_COLOR = '#185577ff';
export const SOLAR_MODULE_STROKE_COLOR = '#002438ff';

export const EXAMPLE_SEGMENT = {
    id: 'temp-id',
    name: 'temp-id',
    polygon: [
        [20.9892183617103, -89.6113760252498],
        [20.9891088012283, -89.6113948007129],
        [20.9891194442501, -89.6114826430582],
        [20.9892346392607, -89.6114638675951],
    ],
    solar_modules: [
        [
            [20.9892143, -89.6114597],
            [20.9892143, -89.6114501],
            [20.9891994, -89.6114501],
            [20.9891994, -89.6114597],
        ],
        [
            [20.9892143, -89.6114491],
            [20.9892143, -89.6114395],
            [20.9891994, -89.6114395],
            [20.9891994, -89.6114491],
        ],
        [
            [20.9892143, -89.6114385],
            [20.9892143, -89.6114289],
            [20.9891994, -89.6114289],
            [20.9891994, -89.6114385],
        ],
        [
            [20.9892143, -89.611428],
            [20.9892143, -89.6114183],
            [20.9891994, -89.6114183],
            [20.9891994, -89.611428],
        ],
        [
            [20.9892143, -89.6114174],
            [20.9892143, -89.6114078],
            [20.9891994, -89.6114078],
            [20.9891994, -89.6114174],
        ],
        [
            [20.9892143, -89.6114068],
            [20.9892143, -89.6113972],
            [20.9891994, -89.6113972],
            [20.9891994, -89.6114068],
        ],
        [
            [20.9892143, -89.6113962],
            [20.9892143, -89.6113866],
            [20.9891994, -89.6113866],
            [20.9891994, -89.6113962],
        ],
        [
            [20.989194, -89.6114703],
            [20.989194, -89.6114607],
            [20.9891791, -89.6114607],
            [20.9891791, -89.6114703],
        ],
        [
            [20.989194, -89.6114597],
            [20.989194, -89.6114501],
            [20.9891791, -89.6114501],
            [20.9891791, -89.6114597],
        ],
        [
            [20.989194, -89.6114491],
            [20.989194, -89.6114395],
            [20.9891791, -89.6114395],
            [20.9891791, -89.6114491],
        ],
        [
            [20.989194, -89.6114385],
            [20.989194, -89.6114289],
            [20.9891791, -89.6114289],
            [20.9891791, -89.6114385],
        ],
        [
            [20.989194, -89.611428],
            [20.989194, -89.6114183],
            [20.9891791, -89.6114183],
            [20.9891791, -89.611428],
        ],
        [
            [20.989194, -89.6114174],
            [20.989194, -89.6114078],
            [20.9891791, -89.6114078],
            [20.9891791, -89.6114174],
        ],
        [
            [20.989194, -89.6114068],
            [20.989194, -89.6113972],
            [20.9891791, -89.6113972],
            [20.9891791, -89.6114068],
        ],
        [
            [20.989194, -89.6113962],
            [20.989194, -89.6113866],
            [20.9891791, -89.6113866],
            [20.9891791, -89.6113962],
        ],
        [
            [20.9891737, -89.6114703],
            [20.9891737, -89.6114607],
            [20.9891588, -89.6114607],
            [20.9891588, -89.6114703],
        ],
        [
            [20.9891737, -89.6114597],
            [20.9891737, -89.6114501],
            [20.9891588, -89.6114501],
            [20.9891588, -89.6114597],
        ],
        [
            [20.9891737, -89.6114491],
            [20.9891737, -89.6114395],
            [20.9891588, -89.6114395],
            [20.9891588, -89.6114491],
        ],
        [
            [20.9891737, -89.6114385],
            [20.9891737, -89.6114289],
            [20.9891588, -89.6114289],
            [20.9891588, -89.6114385],
        ],
        [
            [20.9891737, -89.611428],
            [20.9891737, -89.6114183],
            [20.9891588, -89.6114183],
            [20.9891588, -89.611428],
        ],
        [
            [20.9891737, -89.6114174],
            [20.9891737, -89.6114078],
            [20.9891588, -89.6114078],
            [20.9891588, -89.6114174],
        ],
        [
            [20.9891737, -89.6114068],
            [20.9891737, -89.6113972],
            [20.9891588, -89.6113972],
            [20.9891588, -89.6114068],
        ],
        [
            [20.9891737, -89.6113962],
            [20.9891737, -89.6113866],
            [20.9891588, -89.6113866],
            [20.9891588, -89.6113962],
        ],
        [
            [20.9891534, -89.6114703],
            [20.9891534, -89.6114607],
            [20.9891384, -89.6114607],
            [20.9891384, -89.6114703],
        ],
        [
            [20.9891534, -89.6114597],
            [20.9891534, -89.6114501],
            [20.9891384, -89.6114501],
            [20.9891384, -89.6114597],
        ],
        [
            [20.9891534, -89.6114491],
            [20.9891534, -89.6114395],
            [20.9891384, -89.6114395],
            [20.9891384, -89.6114491],
        ],
        [
            [20.9891534, -89.6114385],
            [20.9891534, -89.6114289],
            [20.9891384, -89.6114289],
            [20.9891384, -89.6114385],
        ],
        [
            [20.9891534, -89.611428],
            [20.9891534, -89.6114183],
            [20.9891384, -89.6114183],
            [20.9891384, -89.611428],
        ],
        [
            [20.9891534, -89.6114174],
            [20.9891534, -89.6114078],
            [20.9891384, -89.6114078],
            [20.9891384, -89.6114174],
        ],
        [
            [20.9891534, -89.6114068],
            [20.9891534, -89.6113972],
            [20.9891384, -89.6113972],
            [20.9891384, -89.6114068],
        ],
        [
            [20.989133, -89.6114703],
            [20.989133, -89.6114607],
            [20.9891181, -89.6114607],
            [20.9891181, -89.6114703],
        ],
        [
            [20.989133, -89.6114597],
            [20.989133, -89.6114501],
            [20.9891181, -89.6114501],
            [20.9891181, -89.6114597],
        ],
        [
            [20.989133, -89.6114491],
            [20.989133, -89.6114395],
            [20.9891181, -89.6114395],
            [20.9891181, -89.6114491],
        ],
        [
            [20.989133, -89.6114385],
            [20.989133, -89.6114289],
            [20.9891181, -89.6114289],
            [20.9891181, -89.6114385],
        ],
        [
            [20.989133, -89.611428],
            [20.989133, -89.6114183],
            [20.9891181, -89.6114183],
            [20.9891181, -89.611428],
        ],
        [
            [20.989133, -89.6114174],
            [20.989133, -89.6114078],
            [20.9891181, -89.6114078],
            [20.9891181, -89.6114174],
        ],
        [
            [20.989133, -89.6114068],
            [20.989133, -89.6113972],
            [20.9891181, -89.6113972],
            [20.9891181, -89.6114068],
        ],
    ],
};

export const MAP_STYLES = [
    {
        elementType: 'geometry',
        stylers: [{ color: '#212121' }],
    },
    {
        elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }],
    },
    {
        elementType: 'labels.text.fill',
        stylers: [{ color: '#757575' }],
    },
    {
        elementType: 'labels.text.stroke',
        stylers: [{ color: '#212121' }],
    },
    {
        featureType: 'administrative',
        elementType: 'geometry',
        stylers: [{ color: '#757575' }],
    },
    {
        featureType: 'administrative.country',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#9e9e9e' }],
    },
    {
        featureType: 'administrative.land_parcel',
        stylers: [{ visibility: 'off' }],
    },
    {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#bdbdbd' }],
    },
    {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#757575' }],
    },
    {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{ color: '#181818' }],
    },
    {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#616161' }],
    },
    {
        featureType: 'poi.park',
        elementType: 'labels.text.stroke',
        stylers: [{ color: '#1b1b1b' }],
    },
    {
        featureType: 'road',
        elementType: 'geometry.fill',
        stylers: [{ color: '#2c2c2c' }],
    },
    {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#8a8a8a' }],
    },
    {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [{ color: '#373737' }],
    },
    {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{ color: '#3c3c3c' }],
    },
    {
        featureType: 'road.highway.controlled_access',
        elementType: 'geometry',
        stylers: [{ color: '#4e4e4e' }],
    },
    {
        featureType: 'road.local',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#616161' }],
    },
    {
        featureType: 'transit',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#757575' }],
    },
    {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{ color: '#000000' }],
    },
    {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{ color: '#3d3d3d' }],
    },
];
