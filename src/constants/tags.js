export const HIDDEN_MODAL_STATE = 0;
export const INSERT_LIBRARY_TAG_STATE = 1;
export const EDIT_LIBRARY_TAG_STATE = 2;
export const CREATE_CUSTOM_TAG_STATE = 3;
export const EDIT_CUSTOM_TAG_STATE = 4;

export const PROPOSAL_SUMMARY_TYPE = 6;

export const DOCUMENT_TAGS = ['catalogs'];
export const HIDDEN_TAGS = ['prima_neta'];
export const SMART_DOCUMENTS_CATEGORY = 'smart_documents';
export const TAG_ALL_TEXT_FORMAT_TYPES = ['', 'date', 'telephone', 'text'];
export const TAG_CURRENCY_FORMAT_TYPES = ['money'];
export const TAG_ECOLOGIC_TYPES = ['ecologic'];
export const TAG_NUMBER_FORMAT_TYPES = [
    'area',
    'hr-sol',
    'length',
    'new',
    'percentage',
    'period',
    'potency-hour',
    'potency',
    'price-kwh',
    'watt-price',
];
export const TAG_PERCENT_FORMAT_TYPES = ['percentage'];
export const TAG_TEXT_FORMAT_TYPES = ['catalogs', 'text'];
