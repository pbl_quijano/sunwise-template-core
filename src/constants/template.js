import { toFixed } from '@helpers/utils';

export const ARROW_UP_KEY = 38;
export const ARROW_DOWN_KEY = 40;
export const ARROW_RIGHT_KEY = 39;
export const ARROW_LEFT_KEY = 37;

//Widgets MOVES
export const MOVE_STEP_FORWARD = 'MOVE_STEP_FORWARD';
export const MOVE_STEP_BACK = 'MOVE_STEP_BACK';
export const MOVE_FORWARD = 'MOVE_FORWARD';
export const MOVE_BACK = 'MOVE_BACK';

export const COLUMNS = 48;
export const FORMAT_Y_TYPES = {
    INT_NUM: 0,
    CURRENCY: 1,
    DECIMAL_2: 2,
    DECIMAL_4: 3,
};
export const PAGE_HEIGHT = 1122.52;
export const PAGE_WIDTH = 793.7;
export const PAGE_ZOOM = 0.09;
export const ROWS = 68;
export const GRID_HEIGHT = toFixed(PAGE_HEIGHT / ROWS, 2);
export const GRID_WIDTH = toFixed(PAGE_WIDTH / COLUMNS, 2);
export const FREE_GRID_STEP = 2;
export const WIDGET_MOVE_BY_KEY_INTERVAL = 250;
export const WIDGET_SUPPORT_VERSION = 2;

export const TABLE_DATA_COLUMNS = [
    'accumulated_capital',
    'annual_saving',
    'bag_year',
    'capacidad',
    'capital-payment',
    'capital',
    'cenace',
    'distribucion',
    'energia',
    'financial_payment',
    'insurance-payment',
    'inverstment',
    'isr',
    'iva',
    'month-payment',
    'no_panels',
    'no-panel',
    'pago_total_cfe',
    'panel',
    'panels',
    'precio_ahorro',
    'precio_generacion',
    'rate',
    'saving',
    'saving',
    'scnmem',
    'suministro',
    'tax-insurance',
    'tax-payment',
    'total_price',
    'total_saving',
    'total-payment',
    'total',
    'transmision',
    'unit_prioce',
];
export const TABLE_FOOTER_COLUMNS = [
    'capital_total',
    'cenace',
    'distribucion',
    'infonavit_total',
    'iva_price',
    'month-payment',
    'no-panel',
    'pago_total_cfe',
    'panel',
    'precio_ahorro',
    'precio_generacion',
    'subtotal_price_no_discount',
    'subtotal_price',
    'sum_insurance_payment',
    'sum_rate',
    'sum_tax_insurance',
    'sum_tax_payment',
    'suministro',
    'total_bag',
    'total_discount',
    'total_payment',
    'total_price_generation',
    'total_price_saving',
    'total_price',
    'total',
    'transmision',
];
