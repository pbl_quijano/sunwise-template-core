export const ONE_PROPOSAL_TYPE = 0; // Plantilla de tipo propuesta
export const MULTIPROPOSAL_TYPE = 1; // Plantilla de tipo resumen
export const SMART_DOCUMENTS_TYPE = 2; // Plantilla de tipo documento inteligente

export const NO_EDITION_MODE = 'EDITION_LEVEL/NO_EDITION_MODE';
export const PARTIAL_EDITION_MODE = 'EDITION_LEVEL/PARTIAL_EDITION_MODE';
export const FULL_EDITION_MODE = 'EDITION_LEVEL/FULL_EDITION_MODE';

export const ADD_PAGE_CHANGE = 'ADD_PAGE_CHANGE';
export const DELETE_PAGE_CHANGE = 'DELETE_PAGE_CHANGE';
export const DUPLICATE_PAGE_CHANGE = 'DUPLICATE_PAGE_CHANGE';
export const ORDER_PAGES_CHANGE = 'ORDER_PAGES_CHANGE';
