import PropTypes from 'prop-types';
import { useEffect } from 'react';
import styled from 'styled-components';

import WidgetSideMenu from '@components/WidgetSideMenu';

import { FULL_EDITION_MODE } from '@constants/types';

import TemplateView from '@modules/TemplateView';
import PageToolbar from '@modules/TemplateView/components/PageToolbar';

import withTemplateCore from '../hocs/withTemplateCore';

// import { TemplateView, withTemplateCore } from '../lib';
// import { PARTIAL_EDITION_MODE } from '../lib/constants/types';

import mockup from './mockup';

const Container = styled.div`
    display: flex;
    width: 100%;
    background-color: aliceblue;
`;

const App = ({ initTemplate }) => {
    useEffect(() => {
        initTemplate(mockup);
        //initTemplate(mockup, 'cfdbdec7-87da-4884-931a-01638691c371');
    }, []);
    return (
        <Container>
            <PageToolbar />
            <TemplateView editionLevel={FULL_EDITION_MODE} />
            <WidgetSideMenu />
        </Container>
    );
};

App.propTypes = {
    initTemplate: PropTypes.func,
};

export default withTemplateCore(() => ({
    baseUrl: 'https://test.sunwise.mx',
    token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiZGY5MDI0NzItNDUwMC00MjYyLWFlZWMtZDM3ODc0MDU1OWM2IiwidXNlcm5hbWUiOiJoYW5kQHN1bndpc2UubXgiLCJleHAiOjE2NDQ2Mjk5NjAsImVtYWlsIjoiaGFuZEBzdW53aXNlLm14In0.nQxgcLGGZTxrMW_0QvTXCJdi7M8Pvdb28oAToaqip7M',
    language: 'es-mx',
    googleApiKey: 'AIzaSyCn8IpJbEuNmy999nGIHESALg9Kr_Wp81Q',
}))(App);
