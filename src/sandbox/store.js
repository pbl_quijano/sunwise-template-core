import { applyMiddleware, combineReducers, createStore } from 'redux';
// import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';

import reducer from '../reducer';

export default function configureStore() {
    const rootReducer = combineReducers({ sunwiseTemplateCore: reducer });

    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    return store;
}
