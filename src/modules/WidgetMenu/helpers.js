import i18next from 'i18next';

import {
    GRID_HEIGHT,
    GRID_WIDTH,
    PAGE_HEIGHT,
    PAGE_WIDTH,
} from '@constants/template';

import { ReactComponent as AddImageIcon } from '@res/icons/addImage.svg';
import { ReactComponent as ClipartIcon } from '@res/icons/clipart.svg';
import { ReactComponent as CombinedIcon } from '@res/icons/combined.svg';
import { ReactComponent as GraphIcon } from '@res/icons/graph.svg';
import { ReactComponent as MarkerIcon } from '@res/icons/marker.svg';
import { ReactComponent as PowerSettingsIcon } from '@res/icons/powerSettings.svg';
import { ReactComponent as SegmentsMapIcon } from '@res/icons/segmentsMap.svg';
import { ReactComponent as SeparatorIcon } from '@res/icons/separator.svg';
import { ReactComponent as SignatureIcon } from '@res/icons/signature.svg';
import { ReactComponent as TableIcon } from '@res/icons/table.svg';
import { ReactComponent as TextFormatIcon } from '@res/icons/textFormat.svg';

import { DEFAULT_WIDGET_VALUES } from '@templateCore/constants';

import {
    BASIC_COMPONENTS,
    GRAPH_COMPONENTS,
    TABLE_COMPONENTS,
} from './constants';

const getMenuData = () => {
    return [
        {
            name: i18next.t('Text and drawing elements'),
            items: [
                {
                    id: 'text',
                    name: i18next.t('Text'),
                    icon: TextFormatIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
                {
                    id: 'clip-art',
                    name: i18next.t('Shapes'),
                    icon: ClipartIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
                {
                    id: 'separator',
                    name: i18next.t('Separator'),
                    icon: SeparatorIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
                {
                    id: 'signature',
                    name: i18next.t('Signature'),
                    icon: SignatureIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
            ],
        },
        {
            name: i18next.t('Image'),
            items: [
                {
                    id: 'image',
                    name: i18next.t('Image'),
                    icon: AddImageIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
                {
                    id: 'helioscope',
                    name: 'Helioscope',
                    disabled: true,
                    icon: PowerSettingsIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
                {
                    id: 'aurora',
                    name: 'Aurora',
                    disabled: true,
                    icon: CombinedIcon,
                    group: BASIC_COMPONENTS,
                    visible: true,
                },
            ],
        },
    ];
};

export const getPlaceholderData = (widgetName, onlineWidgets, orientation) => {
    switch (widgetName) {
        case 'clip-art':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.clipArt.height,
                width: DEFAULT_WIDGET_VALUES.clipArt.width,
            };
        case 'image':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.image.height,
                width: DEFAULT_WIDGET_VALUES.image.width,
            };
        case 'map':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.map.height,
                width: DEFAULT_WIDGET_VALUES.map.width,
            };
        case 'segments-map':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.segmentsMap.height,
                width: DEFAULT_WIDGET_VALUES.segmentsMap.width,
            };
        case 'text':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.text.height,
                width: DEFAULT_WIDGET_VALUES.text.width,
            };
        case 'separator':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.separator.height,
                width: DEFAULT_WIDGET_VALUES.separator.width,
            };
        case 'signature':
            return {
                id: widgetName,
                height: DEFAULT_WIDGET_VALUES.signature.height,
                width: DEFAULT_WIDGET_VALUES.signature.width,
            };
        default:
            for (const { id, meta } of onlineWidgets) {
                if (widgetName === id) {
                    const {
                        freeGrid,
                        height,
                        supportVersion = 1,
                        width,
                    } = meta;
                    const realWidth =
                        supportVersion === 1 && !freeGrid
                            ? width * GRID_WIDTH
                            : width;
                    const realHeight =
                        supportVersion === 1 && !freeGrid
                            ? height * GRID_HEIGHT
                            : height;

                    const limitX =
                        orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT;
                    const limitY =
                        orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH;
                    return {
                        id,
                        height: realHeight > limitY ? limitY : realHeight,
                        width: realWidth > limitX ? limitX : realWidth,
                    };
                }
            }
            return null;
    }
};

export const onWidgetDoubleClickBuild = (id, onAddNewItem) => () => {
    onAddNewItem(id);
};

export const onWidgetDragEndBuild =
    (onSetDroppingItem, setIsDragging) => () => {
        setIsDragging(false);
        onSetDroppingItem(null);
    };

export const onWidgetDragStartBuild =
    (id, onSetDroppingItem, setIsDragging) => (event) => {
        event.dataTransfer.setData('text', '');
        setIsDragging(true);
        onSetDroppingItem(id);
    };

export const prepareWidgetsData = (
    tableWidgets = [],
    chartWidgets = [],
    canUpdateProjectLocation,
    canUpdatePanelsSowing
) => {
    return [
        ...getMenuData(),
        {
            name: i18next.t('Map'),
            items: [
                {
                    id: 'map',
                    name: i18next.t('Location'),
                    icon: MarkerIcon,
                    group: BASIC_COMPONENTS,
                    visible: canUpdateProjectLocation,
                },
                {
                    id: 'segments-map',
                    name: i18next.t('Solar panel layout'),
                    icon: SegmentsMapIcon,
                    group: BASIC_COMPONENTS,
                    visible: canUpdatePanelsSowing,
                },
            ],
        },
        {
            name: i18next.t('Table', { count: 2 }),
            items: tableWidgets.map((tableWidget) => ({
                ...tableWidget,
                icon: TableIcon,
                group: TABLE_COMPONENTS,
                visible: true,
            })),
        },
        {
            name: i18next.t('Graphics'),
            items: chartWidgets.map((chartWidget) => {
                return {
                    ...chartWidget,
                    // defaultValue: {
                    //     ...chartWidget.defaultValue,
                    //     colors: buildGraphsColors(chartWidget.defaultValue),
                    // },
                    icon: GraphIcon,
                    group: GRAPH_COMPONENTS,
                    visible: true,
                };
            }),
        },
    ]
        .filter((item) => !item.hidden)
        .reduce(
            (acc, widgetGroup) => [
                ...acc,
                ...widgetGroup.items.filter((item) => item.visible),
            ],
            []
        )
        .filter((item) => !item.disabled)
        .reduce(
            (acc, current) => {
                let key = '';
                switch (current.group) {
                    case BASIC_COMPONENTS:
                        key = 'basicElements';
                        break;
                    case TABLE_COMPONENTS:
                        key = 'tableElements';
                        break;
                    case GRAPH_COMPONENTS:
                        key = 'graphElements';
                        break;
                    default:
                        return acc;
                }
                return {
                    ...acc,
                    [key]: [...acc[key], current],
                };
            },
            {
                basicElements: [],
                tableElements: [],
                graphElements: [],
            }
        );
};
