import PropTypes from 'prop-types';
import { useState } from 'react';
import { Col } from 'react-bootstrap';
import styled, { css } from 'styled-components';

import { BASIC_COMPONENTS } from '../constants';
import {
    onWidgetDoubleClickBuild,
    onWidgetDragEndBuild,
    onWidgetDragStartBuild,
} from '../helpers';

const Container = styled.div`
    align-items: center;
    background-color: #ffffff;
    border-radius: 3px;
    border: 1px solid #ecedf0;
    color: #848bab;
    cursor: pointer;
    display: flex;
    align-items: center;
    height: 78px;
    margin: 4px;
    transition: all 0.2s linear;
    position: relative;
    ${(props) =>
        props.group === BASIC_COMPONENTS
            ? css`
                  flex-direction: column;
                  justify-content: center;
              `
            : css`
                  width: 100%;
              `}
    &.dragging,
    &:hover {
        background-color: #202253;
        border: 1px solid #202253;
        color: #fff;
    }
`;

const IconContent = styled.div`
    display: flex;
    justify-content: center;
    width: 42px;
    height: 24px;
    ${(props) =>
        props.group === BASIC_COMPONENTS
            ? css`
                  margin-bottom: 4px;
              `
            : css`
                  margin-right: 5px;
              `}
    pointer-events: none;
    svg {
        height: 24px;
    }
`;

const NameText = styled.span`
    font-size: 11px;
    line-height: 14px;
    min-height: 14px;
    pointer-events: none;
    text-align: ${(props) =>
        props.group === BASIC_COMPONENTS ? 'center' : 'left'};
    width: calc(100% - 20px);
`;

const WidgetItem = ({
    data,
    draggable = true,
    onAddNewItem,
    onSetDroppingItem,
}) => {
    const [isDragging, setIsDragging] = useState(false);
    const { icon: Icon, id, name } = data;

    return (
        <Col
            xs={data.group === BASIC_COMPONENTS ? 6 : 18}
            md={data.group === BASIC_COMPONENTS ? 6 : 18}
            className="p-0 mb-2"
        >
            <Container
                className={isDragging && 'dragging'}
                draggable={`${draggable}`}
                onDoubleClick={onWidgetDoubleClickBuild(id, onAddNewItem)}
                onDragEnd={onWidgetDragEndBuild(
                    onSetDroppingItem,
                    setIsDragging
                )}
                onDragStart={onWidgetDragStartBuild(
                    id,
                    onSetDroppingItem,
                    setIsDragging
                )}
                group={data.group}
            >
                <IconContent group={data.group}>
                    <Icon />
                </IconContent>

                <NameText group={data.group}>{name}</NameText>
            </Container>
        </Col>
    );
};

WidgetItem.propTypes = {
    data: PropTypes.object,
    draggable: PropTypes.bool,
    onAddNewItem: PropTypes.func,
    onSetDroppingItem: PropTypes.func,
};

export default WidgetItem;
