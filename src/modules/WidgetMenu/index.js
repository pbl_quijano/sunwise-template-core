import PropTypes from 'prop-types';
import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { Container, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import TitleWithDetail from '@components/TitleWithDetail';

import { MULTIPROPOSAL_TYPE } from '@constants/types';

import { GeneralContext } from '@helpers/contexts';

import * as mainSelectors from '@main/selectors';

import * as templateViewActions from '@modules/TemplateView/actions';
import * as templateViewSelectors from '@modules/TemplateView/selectors';

import * as templateCoreActions from '@templateCore/actions';
import * as templateCoreSelectors from '@templateCore/selectors';

import WidgetItem from './components/WidgetItem';
import { getPlaceholderData, prepareWidgetsData } from './helpers';

const Section = styled.div`
    border-top: 1px solid #eff1fb;
    display: block;
    padding: 27px;
    ${({ disabled }) =>
        disabled &&
        `opacity: 0.6;
        cursor: not-allowed;
        pointer-events: none;`}
`;

const Separator = styled.div`
    width: calc(100% - 16px);
    display: block;
    border-top: 1px solid #eff1fb;
    margin: 10px auto;
    margin-bottom: 10px;
`;

const WidgetsMenu = ({
    addWidget,
    canUpdatePanelsSowing,
    canUpdateProjectLocation,
    chartWidgets,
    disabled,
    selectedPageId,
    selectedPageIsBlocked,
    selectedPageInfiniteMode,
    selectedPageOrientation,
    setDroppingItem,
    tableWidgets,
    templateType,
}) => {
    const { onChangeInPage } = useContext(GeneralContext);
    const { t } = useTranslation();

    const [infiniteModeControl, setInfiniteModeControl] = useState(
        selectedPageInfiniteMode
    );
    const hasSummarySupport = useMemo(
        () => templateType === MULTIPROPOSAL_TYPE,
        [templateType]
    );

    const { basicElements, tableElements, graphElements } = useMemo(
        () =>
            prepareWidgetsData(
                tableWidgets,
                chartWidgets,
                canUpdateProjectLocation,
                canUpdatePanelsSowing
            ),
        [
            tableWidgets,
            chartWidgets,
            canUpdateProjectLocation,
            canUpdatePanelsSowing,
        ]
    );

    useEffect(() => {
        if (selectedPageInfiniteMode !== infiniteModeControl) {
            setInfiniteModeControl(selectedPageInfiniteMode);
        }
    }, [selectedPageInfiniteMode]);

    const handleSetDroppingItem = useCallback(
        (id) => {
            setDroppingItem(
                id
                    ? getPlaceholderData(
                          id,
                          [...tableWidgets, ...chartWidgets],
                          selectedPageOrientation
                      )
                    : null
            );
        },
        [tableWidgets, chartWidgets, selectedPageOrientation]
    );

    const handleAddNewItem = useCallback(
        (type) =>
            addWidget(
                selectedPageId,
                type,
                null,
                hasSummarySupport,
                onChangeInPage
            ),
        [selectedPageId, hasSummarySupport, onChangeInPage]
    );

    return (
        <Section disabled={selectedPageIsBlocked || disabled}>
            <TitleWithDetail className="mb-0">
                {t('Components')}
            </TitleWithDetail>
            <Container>
                <Row className="mt-2">
                    {basicElements.map((item) => (
                        <WidgetItem
                            key={item.id}
                            data={item}
                            onSetDroppingItem={handleSetDroppingItem}
                            onAddNewItem={handleAddNewItem}
                        />
                    ))}
                </Row>
                <Separator></Separator>
                <Row>
                    {tableElements.map((item) => (
                        <WidgetItem
                            key={item.id}
                            data={item}
                            onSetDroppingItem={handleSetDroppingItem}
                            onAddNewItem={handleAddNewItem}
                        />
                    ))}
                </Row>
                <Separator></Separator>
                <Row>
                    {graphElements.map((item) => (
                        <WidgetItem
                            key={item.id}
                            data={item}
                            onSetDroppingItem={handleSetDroppingItem}
                            onAddNewItem={handleAddNewItem}
                        />
                    ))}
                </Row>
            </Container>
        </Section>
    );
};

const mapStateToProps = createStructuredSelector({
    chartWidgets: mainSelectors.getDataFetchChartWidgets,
    selectedPageId: templateViewSelectors.getSelectedPageId,
    selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked,
    selectedPageInfiniteMode: templateViewSelectors.getSelectedPageInfiniteMode,
    selectedPageOrientation: templateViewSelectors.getSelectedPageOrientation,
    tableWidgets: mainSelectors.getDataFetchTableWidgets,
    templateType: templateCoreSelectors.getCurrentTemplateType,
});

const mapDispatchToProps = (dispatch) => ({
    addWidget: (
        pageId,
        type,
        defaultPosition,
        hasSummarySupport,
        onChangeInPage
    ) =>
        dispatch(
            templateCoreActions.addWidget(
                pageId,
                type,
                defaultPosition,
                hasSummarySupport,
                onChangeInPage
            )
        ),
    setDroppingItem: (item) => dispatch(templateViewActions.droppingItem(item)),
});

WidgetsMenu.propTypes = {
    addWidget: PropTypes.func,
    canUpdatePanelsSowing: PropTypes.bool,
    canUpdateProjectLocation: PropTypes.bool,
    chartWidgets: PropTypes.array,
    disabled: PropTypes.bool,
    selectedPageId: PropTypes.string,
    selectedPageIsBlocked: PropTypes.bool,
    selectedPageInfiniteMode: PropTypes.bool,
    selectedPageOrientation: PropTypes.string,
    setDroppingItem: PropTypes.func,
    tableWidgets: PropTypes.array,
    templateType: PropTypes.number,
};

export default connect(mapStateToProps, mapDispatchToProps)(WidgetsMenu);
