const NAME = 'imageUploader';
export const INITIALIZE = `${NAME}/INITIALIZE`;
export const SAVE_IMAGE = `${NAME}/SAVE_IMAGE`;
export const SAVE_IMAGE_SUCCESS = `${NAME}/SAVE_IMAGE_SUCCESS`;
