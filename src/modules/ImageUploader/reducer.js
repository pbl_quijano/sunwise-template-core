import update from 'immutability-helper';

import { INITIALIZE, SAVE_IMAGE, SAVE_IMAGE_SUCCESS } from './actionTypes';

const INITIAL_STATE = {
    saveImage: {
        data: null,
        isSaving: false,
    },
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case SAVE_IMAGE:
            return update(state, {
                saveImage: {
                    $merge: {
                        isSaving: true,
                    },
                },
            });

        case SAVE_IMAGE_SUCCESS:
            return update(state, {
                saveImage: {
                    $merge: {
                        data: action.payload,
                        isSaving: false,
                    },
                },
            });

        case INITIALIZE:
            return update(state, {
                saveImage: {
                    $merge: {
                        data: null,
                        isSaving: false,
                    },
                },
            });

        default:
            return state;
    }
}
