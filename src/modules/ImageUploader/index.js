import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { MAX_FILE_SIZE } from '@constants/values';

import showToast from '@helpers/showToast';

import * as actions from './actions';
import * as selectors from './selectors';

const Content = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const UploadButton = styled.div`
    background-color: #fff;
    border-radius: 16px;
    border: 1px solid #ccc;
    color: #ff9a00;
    cursor: pointer;
    font-size: 14px;
    font-weight: bold;
    height: 32px;
    line-height: 32px;
    opacity: ${({ isSavingImage }) => (isSavingImage ? '0.5' : '1')};
    pointer-events: ${({ isSavingImage }) => (isSavingImage ? 'none' : 'all')};
    text-align: center;
    transition: all 0.2s linear;
    width: 164px;
`;

const FileInput = styled.input`
    cursor: pointer;
    display: block;
    height: 0;
    visibility: hidden;
    width: 0;
`;

const ImageUploader = ({
    currentValue,
    handleValueChange,
    isSavingImage,
    savedImage,
    saveImage,
}) => {
    const { t } = useTranslation();
    const inputFileRef = useRef(null);

    useEffect(() => {
        if (!isNil(savedImage) && savedImage !== currentValue) {
            showToast({
                body: t('The image was uploaded successfully'),
                type: 'success',
            });
            handleValueChange(savedImage);
        }
    }, [savedImage]);

    const onChangeFile = (event) => {
        const { files } = event.target;
        if (files.length > 0) {
            const [file] = files;
            if (!['image/jpeg', 'image/png'].includes(file.type)) {
                showToast({
                    body: t('Invalid file format'),
                    type: 'danger',
                });
                return;
            }
            if (file.size >= MAX_FILE_SIZE) {
                showToast({
                    body: t('The size is larger than allowed'),
                    type: 'danger',
                });
                return;
            }
            saveImage(file);
        }
    };

    return (
        <Content>
            <UploadButton
                isSavingImage={isSavingImage}
                onClick={() => inputFileRef.current.click()}
            >
                {isSavingImage ? t('Uploading image') : t('Upload image')}
            </UploadButton>
            <FileInput
                accept="image/jpeg, image/png"
                type="file"
                ref={inputFileRef}
                onChange={onChangeFile}
            />
        </Content>
    );
};

ImageUploader.propTypes = {
    currentValue: PropTypes.string,
    handleValueChange: PropTypes.func,
    isSavingImage: PropTypes.bool,
    savedImage: PropTypes.string,
    saveImage: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
    isSavingImage: selectors.getIsSavingImage,
    savedImage: selectors.getSavedImage,
});

const mapDispatchToProps = (dispatch) => ({
    saveImage: (image) => dispatch(actions.saveImage(image)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);
