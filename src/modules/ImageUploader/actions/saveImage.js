import { createImage } from '@api';

import * as mainSelectors from '@main/selectors';

import { INITIALIZE, SAVE_IMAGE, SAVE_IMAGE_SUCCESS } from '../actionTypes';

const getImageUrl = (url, state) => {
    if (url.includes('https')) {
        return url;
    }
    const baseURL = mainSelectors.getBaseUrl(state);
    return `${baseURL}${url}`;
};

export default (image) => (dispatch, getState) => {
    if (image) {
        const state = getState();
        dispatch({ type: SAVE_IMAGE });
        createImage(state)({ image }).then((response) => {
            const imgSrc = getImageUrl(response.data.image, state);
            dispatch({ type: SAVE_IMAGE_SUCCESS, payload: imgSrc });
            dispatch({ type: INITIALIZE });
        });
    }
};
