import { createSelector } from 'reselect';

import * as selectors from '@main/selectors';

export const getModel = createSelector(
    selectors.getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.imageUploader
);

export const getIsSavingImage = createSelector(
    getModel,
    (model) => model.saveImage.isSaving
);

export const getSavedImage = createSelector(
    getModel,
    (model) => model.saveImage.data
);
