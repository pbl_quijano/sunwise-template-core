import { INITIALIZE_STATE_POOL } from '../actionTypes';

export default (newState) => (dispatch) => {
    dispatch({ type: INITIALIZE_STATE_POOL, payload: newState });
};
