import * as templateCoreSelectors from '@templateCore/selectors';

import { SELECT_PAGE } from '../actionTypes';

import initializeStatePool from './initializeStatePool';
export default (pageId) => (dispatch, getState) => {
    if (pageId) {
        const { Page } = templateCoreSelectors.getEntitiesSession(getState());
        const pageModel = Page.withId(pageId);
        dispatch({ type: SELECT_PAGE, payload: pageId });
        dispatch(
            initializeStatePool(
                JSON.stringify({
                    ...pageModel.ref,
                    widgets: pageModel.widgets.toRefArray(),
                })
            )
        );
    } else {
        dispatch({ type: SELECT_PAGE, payload: null });
    }
};
