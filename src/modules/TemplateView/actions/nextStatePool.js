import setPageData from '@templateCore/actions/setPageData';
// import * as templateCoreSelectors from '@templateCore/selectors';

import { NEXT_STATE_POOL } from '../actionTypes';
import * as selectors from '../selectors';

export default () => (dispatch, getState) => {
    const state = getState();
    const statePoolData = selectors.getStatePoolData(state);
    const statePoolIndex = selectors.getStatePoolIndex(state);
    let newPrevIndex = statePoolIndex;
    if (statePoolIndex < statePoolData.length - 1 && statePoolData.length > 1) {
        const updateData = statePoolData[newPrevIndex + 1];
        dispatch(setPageData(JSON.parse(updateData)));
        dispatch({
            type: NEXT_STATE_POOL,
            payload: newPrevIndex + 1,
        });
        // templateCoreSelectors.getTemplateUpdatingContentData(state);
    }
};
