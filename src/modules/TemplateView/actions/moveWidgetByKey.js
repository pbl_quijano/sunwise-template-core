import {
    ARROW_UP_KEY,
    ARROW_DOWN_KEY,
    ARROW_RIGHT_KEY,
    ARROW_LEFT_KEY,
    FREE_GRID_STEP,
    GRID_WIDTH,
    GRID_HEIGHT,
    PAGE_HEIGHT,
    PAGE_WIDTH,
} from '@constants/template';

export default (
        keyCode,
        orientation,
        setWidgetPosition,
        showGuides,
        selectedWidget
    ) =>
    () => {
        const { height, id, width, x, y } = selectedWidget;

        let tempStep = 0;
        let step = 0;
        let stepValue = 0;
        let stepLimit = 0;

        switch (keyCode) {
            case ARROW_UP_KEY:
                stepValue = !showGuides ? FREE_GRID_STEP : GRID_HEIGHT;
                tempStep = y - stepValue;
                stepLimit = 0;
                step = tempStep >= stepLimit ? tempStep : stepLimit;
                if (step !== y) {
                    setWidgetPosition(id, x, step);
                }
                break;
            case ARROW_DOWN_KEY:
                stepValue = !showGuides ? FREE_GRID_STEP : GRID_HEIGHT;
                tempStep = y + stepValue;
                stepLimit =
                    (orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH) -
                    height;
                step = tempStep <= stepLimit ? tempStep : stepLimit;

                if (step !== y) {
                    setWidgetPosition(id, x, step);
                }
                break;
            case ARROW_RIGHT_KEY:
                stepValue = !showGuides ? FREE_GRID_STEP : GRID_WIDTH;

                tempStep = x + stepValue;
                stepLimit =
                    (orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT) -
                    width;
                step = tempStep <= stepLimit ? tempStep : stepLimit;
                if (step !== x) {
                    setWidgetPosition(id, step, y);
                }
                break;
            case ARROW_LEFT_KEY:
                stepValue = !showGuides ? FREE_GRID_STEP : GRID_WIDTH;
                tempStep = x - stepValue;
                stepLimit = 0;
                step = tempStep >= stepLimit ? tempStep : stepLimit;
                if (step !== x) {
                    setWidgetPosition(id, step, y);
                }
                break;
            default:
                break;
        }
    };
