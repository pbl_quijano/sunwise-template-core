import { SET_COPIED_ITEM } from '../actionTypes';

export default (copyData) => (dispatch) =>
    dispatch({ type: SET_COPIED_ITEM, payload: copyData });
