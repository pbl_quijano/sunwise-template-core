import { SET_SHOW_GUIDES } from '../actionTypes';

export default (showGuides) => (dispatch) =>
    dispatch({ type: SET_SHOW_GUIDES, payload: showGuides });
