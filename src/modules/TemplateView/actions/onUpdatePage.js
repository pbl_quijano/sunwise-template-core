import * as templateCoreSelectors from '@templateCore/selectors';

import { ADD_STATE_POOL } from '../actionTypes';
import * as selectors from '../selectors';

export default (onChangeInPage = () => {}, addStateDisabled) =>
    (dispatch, getState) => {
        const state = getState();
        if (!addStateDisabled) {
            dispatch({
                type: ADD_STATE_POOL,
                payload: JSON.stringify(selectors.getSelectedPage(state)),
            });
        }

        const updatedData =
            templateCoreSelectors.getTemplateUpdatingContentData(state);

        onChangeInPage(updatedData);
    };
