import { SELECT_WIDGET } from '../actionTypes';

export default (widgetId) => (dispatch) =>
    dispatch({ type: SELECT_WIDGET, payload: widgetId });
