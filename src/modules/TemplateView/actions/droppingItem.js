import { DROPPING_ITEM } from '../actionTypes';

export default (data) => (dispatch) =>
    dispatch({ type: DROPPING_ITEM, payload: data });
