import { SET_TOOLBAR_DISABLED } from '../actionTypes';

export default (isToolbarDisabled) => (dispatch) =>
    dispatch({ type: SET_TOOLBAR_DISABLED, payload: isToolbarDisabled });
