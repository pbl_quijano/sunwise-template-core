import * as templateCoreSelectors from '@templateCore/selectors';

import selectPage from './selectPage';
export default () => (dispatch, getState) => {
    const pages = templateCoreSelectors.getCurrentTemplatePages(getState());
    if (pages.length === 0) {
        return;
    }
    const firstElement = pages[0];
    if (firstElement.type === 'group') {
        if (firstElement.pages.length === 0) {
            return;
        }
        dispatch(selectPage(firstElement.pages[0].id));
    } else {
        dispatch(selectPage(firstElement.id));
    }
};
