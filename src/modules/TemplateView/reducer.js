import update from 'immutability-helper';

import {
    ADD_STATE_POOL,
    DROPPING_ITEM,
    INITIALIZE_STATE_POOL,
    NEXT_STATE_POOL,
    PREV_STATE_POOL,
    RESET,
    SELECT_PAGE,
    SELECT_WIDGET,
    SET_COPIED_ITEM,
    SET_SHOW_GUIDES,
    SET_TOOLBAR_DISABLED,
} from './actionTypes';

const INITIAL_STATE = {
    copiedItem: null,
    droppingItem: null,
    isToolbarDisabled: false,
    selectedPageId: null,
    selectedWidgetId: null,
    showGuides: localStorage.getItem('showGuides') === 'true',
    statesPool: {
        data: [],
        index: 0,
    },
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case ADD_STATE_POOL: {
            const { data, index } = state.statesPool;
            let tempData = [...data];
            let tempIndex = index;
            if (index === tempData.length - 1) {
                tempData = [...tempData, action.payload];
            } else {
                tempData.splice(
                    tempIndex + 1,
                    tempData.length - (tempIndex + 1),
                    action.payload
                );
            }
            ++tempIndex;
            return update(state, {
                statesPool: {
                    $set: {
                        data: tempData,
                        index: tempIndex,
                    },
                },
            });
        }

        case DROPPING_ITEM:
            return update(state, {
                droppingItem: {
                    $set: action.payload,
                },
            });

        case INITIALIZE_STATE_POOL:
            return update(state, {
                statesPool: {
                    $set: {
                        data: [action.payload],
                        index: 0,
                    },
                },
            });

        case NEXT_STATE_POOL:
        case PREV_STATE_POOL:
            return update(state, {
                statesPool: {
                    $merge: {
                        index: action.payload,
                    },
                },
            });

        case RESET:
            return update(state, {
                $set: INITIAL_STATE,
            });

        case SELECT_PAGE:
            return update(state, {
                selectedPageId: {
                    $set: action.payload,
                },
            });

        case SELECT_WIDGET:
            return update(state, {
                selectedWidgetId: {
                    $set: action.payload,
                },
            });

        case SET_COPIED_ITEM:
            return update(state, {
                copiedItem: {
                    $set: action.payload,
                },
            });

        case SET_SHOW_GUIDES:
            return update(state, {
                showGuides: {
                    $set: action.payload,
                },
            });

        case SET_TOOLBAR_DISABLED:
            return update(state, {
                isToolbarDisabled: {
                    $set: action.payload,
                },
            });

        default:
            return state;
    }
}
