import get from 'lodash/get';
import isNil from 'lodash/isNil';

import {
    ARROW_UP_KEY,
    ARROW_DOWN_KEY,
    ARROW_RIGHT_KEY,
    ARROW_LEFT_KEY,
    GRID_WIDTH,
    GRID_HEIGHT,
    PAGE_HEIGHT,
    PAGE_WIDTH,
    WIDGET_MOVE_BY_KEY_INTERVAL,
} from '@constants/template';
import {
    MULTIPROPOSAL_TYPE,
    FULL_EDITION_MODE,
    PARTIAL_EDITION_MODE,
} from '@constants/types';

const getScrollParent = (node) => {
    const regex = /(auto|scroll)/;
    const parents = (_node, ps) => {
        if (_node.parentNode === null) {
            return ps;
        }
        return parents(_node.parentNode, ps.concat([_node]));
    };

    const style = (_node, prop) =>
        getComputedStyle(_node, null).getPropertyValue(prop);
    const overflow = (_node) =>
        style(_node, 'overflow') +
        style(_node, 'overflow-y') +
        style(_node, 'overflow-x');
    const scroll = (_node) => regex.test(overflow(_node));

    /* eslint-disable consistent-return */
    const scrollParent = (_node) => {
        if (!(_node instanceof HTMLElement || _node instanceof SVGElement)) {
            return;
        }

        const ps = parents(_node.parentNode, []);

        for (let i = 0; i < ps.length; i += 1) {
            if (scroll(ps[i])) {
                return ps[i];
            }
        }

        return document.scrollingElement || document.documentElement;
    };

    return scrollParent(node);
    /* eslint-enable consistent-return */
};

const getStartGridX = (tempX, pageOrientation) => {
    const resX = tempX % GRID_WIDTH;
    const newX =
        resX >= 0 && resX < GRID_WIDTH / 2
            ? tempX - resX
            : tempX + (GRID_WIDTH - resX);
    if (newX < 0) {
        return 0;
    }
    if (newX > (pageOrientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT)) {
        return tempX - resX;
    }
    return newX;
};

const getStartGridY = (tempY, pageOrientation) => {
    const resY = tempY % GRID_HEIGHT;
    const newY =
        resY >= 0 && resY < GRID_HEIGHT / 2
            ? tempY - resY
            : tempY + (GRID_HEIGHT - resY);
    if (newY < 0) {
        return 0;
    }
    if (newY > (pageOrientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH)) {
        return tempY - resY;
    }
    return newY;
};

export const calculateInfinitePages = ({
    pageId,
    rowDivs,
    tableContentHeight,
}) => {
    const PADDING_BOTTOM_SPACE = 8;
    const TABLE_HEIGHT_LIMIT = tableContentHeight + PADDING_BOTTOM_SPACE;
    let initPosition = 0;
    let lastPosition = 0;
    let extraPagesData = [];
    let rowDivsLength = rowDivs.length;

    for (var index = 0; index < rowDivsLength; index++) {
        const divHeight = rowDivs[index].offsetHeight;
        const newTempPosition = lastPosition + divHeight;
        if (
            newTempPosition - initPosition > TABLE_HEIGHT_LIMIT ||
            index + 1 === rowDivsLength
        ) {
            extraPagesData.push({
                id: `${pageId}-clone-${extraPagesData.length + 1}`,
                tablePosition: parseInt(initPosition),
                nextPosition: lastPosition,
            });
            initPosition = lastPosition;
        }
        lastPosition = newTempPosition;
    }

    if (extraPagesData.length === 0) {
        return [];
    }
    return extraPagesData;
};

export const getSelectedWidgetValidated = (
    selectedWidgetId,
    pageId,
    pageWidgets
) => {
    if (isNil(selectedWidgetId)) {
        return null;
    }
    const [widgetPageId] = selectedWidgetId.split('/');
    if (widgetPageId !== pageId) {
        return null;
    }
    return pageWidgets.find((widget) => widget.id === selectedWidgetId);
};

export const calcPositionWithLimits = (
    x,
    y,
    width,
    height,
    pageOrientation,
    showGuides
) => {
    const pageWidth = pageOrientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT;
    const pageHeight =
        pageOrientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH;
    const limitX = pageWidth - width;
    const limitY = pageHeight - height;
    const newX = x > limitX ? limitX : x;
    const newY = y > limitY ? limitY : y;
    if (showGuides) {
        const fixedX =
            newX >= GRID_WIDTH ? parseInt(newX / GRID_WIDTH) * GRID_WIDTH : 0;
        const fixedY =
            newY >= GRID_HEIGHT
                ? parseInt(newY / GRID_HEIGHT) * GRID_HEIGHT
                : 0;

        return { x: fixedX, y: fixedY };
    }
    return {
        x: newX,
        y: newY,
    };
};

export const getHasSummarySupport = (selectedWidget) =>
    !isNil(selectedWidget) && get(selectedWidget, 'hasSummarySupport', false);

export const getIsLocationWidget = (selectedWidget) =>
    !isNil(selectedWidget) &&
    ['map', 'segments-map'].includes(selectedWidget.name);

export const getOpenSettingsDisabled = (
    selectedWidget,
    templateType,
    chartWidgetsNames,
    tableWidgetsNames
) => {
    if (
        templateType === MULTIPROPOSAL_TYPE &&
        getHasSummarySupport(selectedWidget) &&
        getIsLocationWidget(selectedWidget)
    ) {
        return false;
    }
    if (!isNil(selectedWidget)) {
        return !hasSettingsPanel(
            [...chartWidgetsNames, ...tableWidgetsNames],
            selectedWidget.name
        );
    }
    return true;
};

export const getPagePosition = (elementRef) => {
    if (!elementRef) {
        return null;
    }
    const boundingClientRect = elementRef.getBoundingClientRect();
    const scrolledParent = getScrollParent(elementRef);
    return {
        x: scrolledParent.scrollLeft + boundingClientRect.x,
        y: scrolledParent.scrollTop + boundingClientRect.y,
    };
};

export const hasSettingsPanel = (onlineWidgetsNames, type) => {
    if (
        [
            ...onlineWidgetsNames,
            'clip-art',
            'equipment-table',
            'image',
            'kwh-table',
            'money-table',
            'segments-map',
            'separator',
        ].includes(type)
    ) {
        return true;
    }
    return false;
};

/* Handlers */
export const handleClickContainerBuild =
    (
        contextMenuData,
        handleCloseContextMenu,
        selectedWidgetId,
        selectWidget,
        wrapperRef
    ) =>
    (event) => {
        if (contextMenuData.show) {
            handleCloseContextMenu();
        }
        if (event.target === wrapperRef.current && !isNil(selectedWidgetId)) {
            selectWidget(null);
        }
    };

export const handleCloseContextMenuBuild = (setContextMenuData) => (event) => {
    if (event) event.preventDefault();
    setContextMenuData({
        x: 0,
        y: 0,
        pageX: 0,
        pageY: 0,
        show: false,
    });
};

export const handleCopyWidgetBuild =
    (setCopiedWidget) =>
    ({
        field,
        height,
        name,
        num,
        replaceInfoRequired,
        style,
        value,
        width,
        x,
        y,
    }) => {
        setCopiedWidget({
            field,
            height,
            name,
            num,
            replaceInfoRequired,
            style,
            value,
            width,
            x,
            y,
        });
    };

export const handleDropBuild =
    (droppingItem, onAddWidget, setDroppingItem) =>
    (event, placeholderPositionX, placeholderPositionY) => {
        event.preventDefault();
        setDroppingItem(null);
        onAddWidget(droppingItem.id, {
            x: placeholderPositionX,
            y: placeholderPositionY,
        });
    };

export const handleOnStartDragBuild =
    (id, pageOrientation, setWidgetPosition, showGuides) =>
    (_, { lastX, lastY }) => {
        setWidgetPositionByAdjustment(
            id,
            pageOrientation,
            setWidgetPosition,
            showGuides,
            lastX,
            lastY
        );
    };

export const handleOnStartResizeBuild =
    (id, pageOrientation, setWidgetPosition, showGuides, x, y) => () => {
        setWidgetPositionByAdjustment(
            id,
            pageOrientation,
            setWidgetPosition,
            showGuides,
            x,
            y
        );
    };

export const handleOpenItemContextMenuBuild =
    (setContextMenuData) => (event) => {
        event.preventDefault();
        setContextMenuData({
            x: event.clientX,
            y: event.clientY,
            pageX: event.pageX,
            pageY: event.pageY,
            show: true,
        });
    };

export const handleOpenPageContextMenuBuild =
    ({
        contextMenuData,
        editionLevel,
        selectedWidgetId,
        selectWidget,
        setContextMenuData,
        wrapperRef,
    }) =>
    (event) => {
        event.preventDefault();
        if (event.target === wrapperRef.current) {
            switch (editionLevel) {
                case FULL_EDITION_MODE:
                    if (!isNil(selectedWidgetId)) {
                        selectWidget(null);
                    }
                    setContextMenuData({
                        x: event.clientX,
                        y: event.clientY,
                        pageX: event.pageX,
                        pageY: event.pageY,
                        show: true,
                    });
                    break;
                case PARTIAL_EDITION_MODE:
                    if (!isNil(selectedWidgetId)) {
                        selectWidget(null);
                    }
                    if (contextMenuData.show) {
                        setContextMenuData({
                            x: 0,
                            y: 0,
                            pageX: 0,
                            pageY: 0,
                            show: false,
                        });
                    }
                    break;
                default:
            }
        }
    };

export const handleOpenSettingsBuild =
    (selectedWidget, settingsPanelRef) => (event) => {
        settingsPanelRef.current.openSettingsPanel(selectedWidget, event);
    };

export const handlePageKeyDown = (
    selectedWidgetName,
    moveWidgetByKey,
    interval
) => {
    if (!selectedWidgetName) {
        return null;
    }
    return (e) => {
        if (
            [
                ARROW_UP_KEY,
                ARROW_DOWN_KEY,
                ARROW_RIGHT_KEY,
                ARROW_LEFT_KEY,
            ].includes(e.keyCode) &&
            interval.current === null &&
            (selectedWidgetName !== 'text' ||
                (!e.target.className.includes('fr-element') &&
                    !e.target.className.includes('fr-view')))
        ) {
            e.preventDefault();
            e.stopPropagation();
            interval.current = setInterval(() => {
                moveWidgetByKey(e.keyCode);
            }, WIDGET_MOVE_BY_KEY_INTERVAL);
            moveWidgetByKey(e.keyCode);
        }
        if (interval.current !== null) {
            e.preventDefault();
            e.stopPropagation();
        }
    };
};

export const handlePageKeyUp = (interval) => () => {
    if (interval.current) {
        clearTimeout(interval.current);
        interval.current = null;
    }
};

export const handleRightClickBuild =
    (
        handleCloseContextMenu,
        handleOpenContextMenu,
        id,
        openSettingsDisabled,
        selectWidgetId,
        editionLevel
    ) =>
    (e) => {
        e.preventDefault();
        selectWidgetId(id);

        switch (editionLevel) {
            case FULL_EDITION_MODE:
                handleOpenContextMenu(e, true);
                break;
            case PARTIAL_EDITION_MODE:
                if (openSettingsDisabled) {
                    handleCloseContextMenu();
                } else {
                    handleOpenContextMenu(e, true);
                }
                break;
        }
    };

export const handlePasteBuild =
    (
        contextMenuData,
        copiedWidget,
        hasSummarySupport,
        onChangeInPage,
        onPasteWidget,
        page,
        pagePosition,
        showGuides
    ) =>
    () => {
        onPasteWidget(
            page.id,
            copiedWidget,
            showGuides,
            calcPositionWithLimits(
                contextMenuData.pageX - pagePosition.x,
                contextMenuData.pageY - pagePosition.y,
                copiedWidget.width,
                copiedWidget.height,
                page.orientation,
                showGuides
            ),
            hasSummarySupport,
            onChangeInPage
        );
    };

export const isInfiniteModeEnabled = (widgets) => {
    const tableCount = widgets.reduce((acc, widget) => {
        if (widget.replaceInfoRequired === 'table') {
            return acc + 1;
        }
        return acc;
    }, 0);
    return tableCount === 1;
};

export const selectedWidgetUseEffect = (
    selectedWidgetName,
    moveWidgetByKey,
    interval
) => {
    return () => {
        const handlePageKeyUpBuild = handlePageKeyUp(interval);
        const handlePageKeyDownBuild = handlePageKeyDown(
            selectedWidgetName,
            moveWidgetByKey,
            interval
        );
        window.addEventListener('keyup', handlePageKeyUpBuild, true);
        window.addEventListener('keydown', handlePageKeyDownBuild, true);
        return () => {
            window.removeEventListener('keyup', handlePageKeyUpBuild, true);
            window.removeEventListener('keydown', handlePageKeyDownBuild, true);
        };
    };
};

const setWidgetPositionByAdjustment = (
    id,
    pageOrientation,
    setWidgetPosition,
    showGuides,
    x,
    y
) => {
    if (showGuides && (x % GRID_WIDTH > 0 || y % GRID_HEIGHT > 0)) {
        setWidgetPosition(
            id,
            getStartGridX(x, pageOrientation),
            getStartGridY(y, pageOrientation)
        );
    }
};

export const sortingWidgetsByOrder = (a, b) => {
    if (a.order < b.order) {
        return -1;
    }
    if (a.order > b.order) {
        return 1;
    }
    return 0;
};
