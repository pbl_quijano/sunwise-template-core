import { createSelector } from 'reselect';

import * as selectors from '@main/selectors';

import * as templateCoreSelectors from '@templateCore/selectors';

export const getModel = createSelector(
    selectors.getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.templateView
);

export const getCopiedItem = createSelector(
    getModel,
    (model) => model.copiedItem
);

export const getDroppingItem = createSelector(
    getModel,
    (model) => model.droppingItem
);

export const getIsToolbarDisabled = createSelector(
    getModel,
    (model) => model.isToolbarDisabled
);

export const getSelectedWidgetId = createSelector(
    getModel,
    (model) => model.selectedWidgetId
);

export const getSelectedPageId = createSelector(
    getModel,
    (model) => model.selectedPageId
);

export const getSelectedPage = createSelector(
    templateCoreSelectors.getEntitiesSession,
    getSelectedPageId,
    ({ Page }, selectedPageId) => {
        if (selectedPageId === null || !Page.idExists(selectedPageId)) {
            return null;
        }
        const pageModel = Page.withId(selectedPageId);
        return {
            ...pageModel.ref,
            widgets: pageModel.widgets.toRefArray(),
        };
    }
);

export const getSelectedPageIsBlocked = createSelector(
    getSelectedPage,
    (selectedPage) => {
        if (selectedPage === null) {
            return true;
        }
        return selectedPage.blocked === 1;
    }
);

export const getSelectedPageInfiniteMode = createSelector(
    getSelectedPage,
    (selectedPage) => {
        if (selectedPage === null) {
            return false;
        }
        return selectedPage.infiniteMode;
    }
);

export const getSelectedPageOrientation = createSelector(
    getSelectedPage,
    (selectedPage) => {
        if (selectedPage === null) {
            return '';
        }
        return selectedPage.orientation;
    }
);

export const getSelectedPageTheme = createSelector(
    getSelectedPage,
    (selectedPage) => {
        if (selectedPage === null) {
            return 'defaultTheme';
        }
        return selectedPage.theme;
    }
);

export const isInfiniteModeValid = createSelector(
    getSelectedPage,
    (selectedPage) => {
        if (selectedPage === null) {
            return false;
        }
        const tableCount = selectedPage.widgets.reduce((acc, widget) => {
            if (widget.replaceInfoRequired === 'table') {
                return acc + 1;
            }
            return acc;
        }, 0);
        return tableCount === 1;
    }
);

export const getShowGuides = createSelector(
    getModel,
    (model) => model.showGuides
);

export const getStatePool = createSelector(
    getModel,
    (model) => model.statesPool
);

export const getStatePoolData = createSelector(
    getStatePool,
    (statesPool) => statesPool.data
);

export const getStatePoolDataLength = createSelector(
    getStatePoolData,
    (statePoolData = []) => statePoolData.length
);

export const getStatePoolIndex = createSelector(
    getStatePool,
    (statesPool) => statesPool.index
);
