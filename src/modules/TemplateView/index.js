import WebfontLoader from '@dr-kobros/react-webfont-loader';
import FroalaEditor from 'froala-editor';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FONT_CONFIG } from '@constants/froala';
import { FULL_EDITION_MODE, NO_EDITION_MODE } from '@constants/types';

import { getFroalaEditorConfig } from '@helpers/froala';

import * as mainSelectors from '@main/selectors';

import TagSystem from '@modules/TagSystem';
import * as tagSystemActions from '@modules/TagSystem/actions';

import * as templateCoreSelectors from '@templateCore/selectors';

import * as actions from './actions';
import TemplateContent from './components/TemplateContent';
import * as selectors from './selectors';

const TemplateView = ({
    catalogs,
    currentTemplateProposalNumber,
    currentTemplateType,
    editionLevel,
    froalaLicenseKey,
    horizontalScrollMode,
    infinitePagesSupportEnabled = true,
    isInitializing,
    language,
    pageDesignDisabled,
    prepareEditCustomTag,
    prepareEditLibraryTag,
    prepareInsertLibraryTag,
    setIsToolbarDisabled,
    showGuides,
}) => {
    const { t } = useTranslation();
    const [currentFroalaEditorInstance, setCurrentFroalaEditorInstance] =
        useState(null);

    useEffect(() => {
        const originalHelpers = FroalaEditor.MODULES.helpers;

        FroalaEditor.MODULES.helpers = function () {
            const helpers = originalHelpers.apply(this, arguments);

            helpers.screenSize = function () {
                if (window.innerWidth >= 1200) return FroalaEditor.LG;
                if (window.innerWidth >= 992) return FroalaEditor.MD;
                if (window.innerWidth >= 768) return FroalaEditor.SM;
                return FroalaEditor.XS;
            };

            return helpers;
        };

        FroalaEditor.DefineIcon('tagList', { NAME: 'tags', SVG_KEY: 'tags' });

        FroalaEditor.RegisterCommand('tagList', {
            title: t('Smart Tags'),
            undo: true,
            showOnMobile: true,
            callback: function () {
                this.selection.save();
                prepareInsertLibraryTag();
            },
        });
    }, []);
    if (isInitializing) {
        return null;
    }

    const froalaEditorConfig = getFroalaEditorConfig({
        FroalaEditor,
        froalaLicenseKey,
        isFullEdition: editionLevel === FULL_EDITION_MODE,
        language,
        prepareEditCustomTag,
        prepareEditLibraryTag,
        setCurrentFroalaEditorInstance,
        setIsToolbarDisabled,
    });

    const availableCatalogs = catalogs.filter(
        (item) => item.order <= currentTemplateProposalNumber
    );
    return (
        <WebfontLoader config={FONT_CONFIG}>
            <>
                <TemplateContent
                    availableCatalogs={availableCatalogs}
                    editionLevel={editionLevel}
                    froalaEditorConfig={froalaEditorConfig}
                    horizontalScrollMode={horizontalScrollMode}
                    infinitePagesSupportEnabled={infinitePagesSupportEnabled}
                    pageDesignDisabled={pageDesignDisabled}
                    showGuides={showGuides}
                    templateType={currentTemplateType}
                />
                {editionLevel !== NO_EDITION_MODE && (
                    <TagSystem
                        catalogs={availableCatalogs}
                        editionLevel={editionLevel}
                        froalaInstance={currentFroalaEditorInstance}
                        templateType={currentTemplateType}
                    />
                )}
            </>
        </WebfontLoader>
    );
};

const mapStateToProps = createStructuredSelector({
    currentTemplateProposalNumber:
        templateCoreSelectors.getCurrentTemplateProposalNumber,
    currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
    catalogs: mainSelectors.getFetchCatalogData,
    froalaLicenseKey: mainSelectors.getFroalaLicenseKey,
    googleApiKey: mainSelectors.getGoogleApiKey,
    isInitializing: mainSelectors.getIsInitializing,
    language: mainSelectors.getLanguage,
    showGuides: selectors.getShowGuides,
});

const mapDispatchToProps = (dispatch) => ({
    prepareEditCustomTag: (values) =>
        dispatch(tagSystemActions.prepareEditCustomTag(values)),
    prepareEditLibraryTag: (values) =>
        dispatch(tagSystemActions.prepareEditLibraryTag(values)),
    prepareInsertLibraryTag: () =>
        dispatch(tagSystemActions.prepareInsertLibraryTag()),
    setIsToolbarDisabled: (isToolbarDisabled) =>
        dispatch(actions.setToolbarDisabled(isToolbarDisabled)),
    setShowGuides: (showGuides) => dispatch(actions.setShowGuides(showGuides)),
});

TemplateView.propTypes = {
    catalogs: PropTypes.array,
    currentTemplateProposalNumber: PropTypes.number,
    currentTemplateType: PropTypes.number,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    froalaLicenseKey: PropTypes.string,
    horizontalScrollMode: PropTypes.bool,
    infinitePagesSupportEnabled: PropTypes.bool,
    isInitializing: PropTypes.bool,
    language: PropTypes.string,
    pageDesignDisabled: PropTypes.bool,
    prepareEditCustomTag: PropTypes.func,
    prepareEditLibraryTag: PropTypes.func,
    prepareInsertLibraryTag: PropTypes.func,
    setIsToolbarDisabled: PropTypes.func,
    showGuides: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(TemplateView);
