import PropTypes from 'prop-types';

import ProposalSelector from './ProposalSelector';

const LocationPanel = ({
    catalogs,
    handleValueChange,
    hasProposalSelector,
    proposalSelectorDisabled,
    value = {},
} = {}) => (
    <ProposalSelector
        catalogs={catalogs}
        disabled={proposalSelectorDisabled}
        handleValueChange={handleValueChange}
        value={value}
        visible={hasProposalSelector}
    />
);

LocationPanel.propTypes = {
    catalogs: PropTypes.array,
    handleValueChange: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    proposalSelectorDisabled: PropTypes.string,
    value: PropTypes.object,
};

export default LocationPanel;
