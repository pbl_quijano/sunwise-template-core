import PropTypes from 'prop-types';
import { Tab } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ProposalSelector from '../ProposalSelector';
import { StyledTabs } from '../Styles';

import ColumnsTab from './ColumnsTab';
import ResultsTab from './ResultsTab';
import StylesTab from './StylesTab';

const TablePanel = ({
    catalogs,
    handleStyleChange,
    handleValueChange,
    hasProposalSelector,
    proposalSelectorDisabled,
    style = {},
    value,
} = {}) => {
    const { t } = useTranslation();
    return (
        <>
            <ProposalSelector
                catalogs={catalogs}
                disabled={proposalSelectorDisabled}
                handleValueChange={handleValueChange}
                value={value}
                visible={hasProposalSelector}
            />
            <StyledTabs>
                <Tab
                    className="pr-3 pl-3"
                    eventKey="Properties"
                    title={t('Properties')}
                >
                    <StylesTab
                        handleStyleChange={handleStyleChange}
                        style={style}
                    />
                </Tab>
                <Tab
                    className="pr-3 pl-3"
                    eventKey="Columns"
                    title={t('Column', { count: 2 })}
                >
                    <ColumnsTab
                        handleValueChange={handleValueChange}
                        value={value}
                    />
                </Tab>
                <Tab
                    className="pr-3 pl-3"
                    eventKey="Footer"
                    title={t('Footer')}
                >
                    <ResultsTab
                        handleValueChange={handleValueChange}
                        value={value}
                    />
                </Tab>
            </StyledTabs>
        </>
    );
};

TablePanel.propTypes = {
    catalogs: PropTypes.array,
    handleStyleChange: PropTypes.func,
    handleValueChange: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    proposalSelectorDisabled: PropTypes.string,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default TablePanel;
