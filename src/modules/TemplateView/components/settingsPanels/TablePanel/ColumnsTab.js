import arrayMove from 'array-move';
import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { FlexColumn, FlexRow } from '@components/styled';

import { Submenu } from '../Styles';

import SortableList from './SortableList';

const Container = styled(FlexColumn)``;

const Header = styled(FlexRow)`
    .name-column {
        flex-basis: 0;
        flex-grow: 1;
        padding: 0 4px;
    }

    .text-column {
        width: 110px;
        padding: 0 4px;
    }

    .actions-column {
        width: 50px;
        padding: 0 4px;
    }
    span {
        color: #4f51a1;
        font-size: 13px;
    }
`;

const getNextID = (arraySelected, currentID) => {
    let selectedIndex = arraySelected.map((col) => col.id).indexOf(currentID);
    if (selectedIndex === 0) {
        ++selectedIndex;
    } else {
        --selectedIndex;
    }
    return arraySelected[selectedIndex].id;
};

const orderSort = (a, b) => {
    if (a.order > b.order) {
        return 1;
    }
    if (b.order > a.order) {
        return -1;
    }
    return 0;
};

const getArrayCols = (cols) => {
    return Object.keys(cols)
        .map((colKey) => ({ ...cols[colKey], id: colKey }))
        .sort(orderSort)
        .filter((col) => col.visible);
};

const ColumnsTab = ({ handleValueChange, value } = {}) => {
    const { t } = useTranslation();
    const preparedCols = Object.keys(value.cols)
        .map((key) => ({ ...value.cols[key], id: key }))
        .sort(orderSort);

    const onDragEnd = (result) => {
        const { destination, source } = result;

        if (!destination) {
            return;
        }

        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }

        const items = arrayMove(
            preparedCols,
            source.index,
            destination.index
        ).map((item, index) => ({ ...item, order: index }));
        handleValueChange({
            ...value,
            cols: items.reduce((acc, currentValue) => {
                acc[currentValue.id] = currentValue;
                return acc;
            }, {}),
        });
    };

    const handleChangeCheckbox = (id, bool) => {
        let tempCols = { ...value.cols };
        const beforeArrayCols = getArrayCols({ ...tempCols });
        tempCols[id].visible = bool;
        const arrayCols = getArrayCols({ ...tempCols });
        const elementsWithoutWidth = arrayCols.reduce((acc, current) => {
            if (isNil(current.width)) {
                ++acc;
            }
            return acc;
        }, 0);

        if (elementsWithoutWidth > 1) {
            arrayCols.forEach((element) => {
                tempCols[element.id].width = 100 / arrayCols.length;
            });
        } else {
            if (bool) {
                if (arrayCols.length < 2) {
                    tempCols[id].width = 100;
                } else {
                    const nextId = getNextID(arrayCols, id);
                    tempCols[id].width = tempCols[nextId].width / 2;
                    tempCols[nextId].width = tempCols[nextId].width / 2;
                }
            } else if (arrayCols.length > 0) {
                const nextId = getNextID(beforeArrayCols, id);
                tempCols[nextId].width += tempCols[id].width;
            }
        }

        handleValueChange({
            ...value,
            cols: tempCols,
        });
    };

    const handleChangeEditableText = (id, text) => {
        let tempCols = { ...value.cols };
        tempCols[id].editable_label = text;

        handleValueChange({
            ...value,
            cols: tempCols,
        });
    };

    return (
        <Container>
            <Submenu>{t('Data')}</Submenu>
            <Header>
                <span className="name-column">{t('Column')}</span>
                <span className="text-column">{t('Text')}</span>
                <span className="actions-column">{t('Action')}</span>
            </Header>
            <SortableList
                id="column-options"
                items={preparedCols}
                onChangeCheckbox={handleChangeCheckbox}
                onChangeEditableText={handleChangeEditableText}
                onDragEnd={onDragEnd}
            />
        </Container>
    );
};

ColumnsTab.propTypes = {
    handleValueChange: PropTypes.func,
    value: PropTypes.object,
};

export default ColumnsTab;
