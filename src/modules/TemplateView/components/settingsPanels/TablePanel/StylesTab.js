import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';

import ColorPickerInput from '@components/ColorPickerInput';
import { FlexColumn } from '@components/styled';

import {
    CheckboxInput,
    InputContainer,
    Label,
    StyledInputGroup,
    Submenu,
} from '../Styles';

const FONT_SIZES = [
    { label: '8', value: '8px' },
    { label: '9', value: '9px' },
    { label: '10', value: '10px' },
    { label: '11', value: '11px' },
    { label: '12', value: '12px' },
    { label: '13', value: '13px' },
    { label: '14', value: '14px' },
    { label: '16', value: '16px' },
    { label: '18', value: '18px' },
    { label: '20', value: '20px' },
    { label: '24', value: '24px' },
    { label: '30', value: '30px' },
    { label: '36', value: '36px' },
    { label: '48', value: '48px' },
    { label: '60', value: '60px' },
    { label: '72', value: '72px' },
    { label: '96', value: '96px' },
];

const StylesTab = ({ handleStyleChange, style } = {}) => {
    const { t } = useTranslation();
    const { borderStyle = 'hidden', borderColor = '#000' } = style.table;
    const {
        color: headerColor = '#fff',
        backgroundColor: headerBackgroundColor = '#000',
        fontSize: headerFontSize = '13px',
        fontBold: headerFontBold,
        fontItalic: headerFontItalic,
        roundedBorders,
    } = style.header;
    const {
        color: bodyColor = '#000',
        backgroundColor1: bodyBackgroundColor1 = '#fff',
        backgroundColor2: bodyBackgroundColor2 = '#ccc',
        fontSize: bodyFontSize = '13px',
        fontBold: bodyFontBold,
        fontItalic: bodyFontItalic,
        isZebraStyle,
    } = style.body;
    const {
        borderTopColor: footerBorderTopColor,
        borderTopStyle: footerBorderTopStyle,
        color: footerColor = '#000',
        backgroundColor1: footerBackgroundColor1 = '#fff',
        backgroundColor2: footerBackgroundColor2 = '#ccc',
        fontSize: footerFontSize = '13px',
        fontBold: footerFontBold,
        fontItalic: footerFontItalic,
        isZebraStyle: isFooterZebraStyle,
    } = style.footer;

    const options = [
        { value: 'hidden', label: t('No border') },
        { value: 'solid', label: t('Normal') },
        { value: 'dashed', label: t('Lined') },
        { value: 'dotted', label: t('Dotted') },
    ];

    const handleTableStyleChange = (type, field, value) => {
        let newStyle = {};
        let tableStyle = Object.assign({}, style[type]);
        tableStyle[field] = value;
        newStyle[type] = tableStyle;
        handleStyleChange({
            ...style,
            ...newStyle,
        });
    };

    return (
        <FlexColumn>
            <Submenu>{t('Table')}</Submenu>
            <InputContainer>
                <Label>{t('Border')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={borderStyle}
                        onChange={(e) =>
                            handleTableStyleChange(
                                'table',
                                'borderStyle',
                                e.target.value
                            )
                        }
                        as="select"
                        id="select-theme"
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    key={`select-${value}-${label}`}
                                    value={value}
                                    disabled={optionDisabled}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={t('Border color')}
                    value={borderColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange(
                            'table',
                            'borderColor',
                            tempColor
                        )
                    }
                />
            </InputContainer>

            <Submenu>{t('Header')}</Submenu>
            <InputContainer>
                <ColorPickerInput
                    label={t('Background color')}
                    value={headerBackgroundColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange(
                            'header',
                            'backgroundColor',
                            tempColor
                        )
                    }
                />
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={t('Font color')}
                    value={headerColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange('header', 'color', tempColor)
                    }
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Font size')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={headerFontSize}
                        onChange={(e) =>
                            handleTableStyleChange(
                                'header',
                                'fontSize',
                                e.target.value
                            )
                        }
                        as="select"
                        id="select-header-fontsize"
                    >
                        {FONT_SIZES.map(({ label, value }) => (
                            <option
                                key={`select-${value}-${label}`}
                                value={value}
                            >
                                {label}
                            </option>
                        ))}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <Label>{t('Bold')}</Label>
                <CheckboxInput
                    id="headerFontBold"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'header',
                            'fontBold',
                            e.target.checked
                        )
                    }
                    checked={headerFontBold}
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Italics')}</Label>
                <CheckboxInput
                    id="headerFontItalic"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'header',
                            'fontItalic',
                            e.target.checked
                        )
                    }
                    checked={headerFontItalic}
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Rounded')}</Label>
                <CheckboxInput
                    id="roundedBorders"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'header',
                            'roundedBorders',
                            e.target.checked
                        )
                    }
                    checked={roundedBorders}
                />
            </InputContainer>

            <Submenu>{t('Body')}</Submenu>
            <InputContainer>
                <ColorPickerInput
                    label={t('Font color')}
                    value={bodyColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange('body', 'color', tempColor)
                    }
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Alternating colors')}</Label>
                <CheckboxInput
                    id="isZebraStyle"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'body',
                            'isZebraStyle',
                            e.target.checked
                        )
                    }
                    checked={isZebraStyle}
                />
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={`${t('Background color')}${
                        isZebraStyle ? ' 1' : ''
                    }`}
                    value={bodyBackgroundColor1}
                    onChange={(tempColor) =>
                        handleTableStyleChange(
                            'body',
                            'backgroundColor1',
                            tempColor
                        )
                    }
                />
            </InputContainer>
            {isZebraStyle && (
                <InputContainer>
                    <ColorPickerInput
                        label={`${t('Background color')} 2`}
                        value={bodyBackgroundColor2}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'body',
                                'backgroundColor2',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
            )}
            <InputContainer>
                <Label>{t('Font size')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={bodyFontSize}
                        onChange={(e) =>
                            handleTableStyleChange(
                                'body',
                                'fontSize',
                                e.target.value
                            )
                        }
                        as="select"
                        id="select-body-fontsize"
                    >
                        {FONT_SIZES.map(({ label, value }) => (
                            <option
                                key={`select-${value}-${label}`}
                                value={value}
                            >
                                {label}
                            </option>
                        ))}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <Label>{t('Bold')}</Label>
                <CheckboxInput
                    id="bodyFontBold"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'body',
                            'fontBold',
                            e.target.checked
                        )
                    }
                    checked={bodyFontBold}
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Italics')}</Label>
                <CheckboxInput
                    id="bodyFontItalic"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'body',
                            'fontItalic',
                            e.target.checked
                        )
                    }
                    checked={bodyFontItalic}
                />
            </InputContainer>

            <Submenu>{t('Footer')}</Submenu>
            <InputContainer>
                <ColorPickerInput
                    label={t('Font color')}
                    value={footerColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange('footer', 'color', tempColor)
                    }
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Alternating colors')}</Label>
                <CheckboxInput
                    id="isFooterZebraStyle"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'footer',
                            'isZebraStyle',
                            e.target.checked
                        )
                    }
                    checked={isFooterZebraStyle}
                />
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={`${t('Background color')}${
                        isFooterZebraStyle ? ' 1' : ''
                    }`}
                    value={footerBackgroundColor1}
                    onChange={(tempColor) =>
                        handleTableStyleChange(
                            'footer',
                            'backgroundColor1',
                            tempColor
                        )
                    }
                />
            </InputContainer>
            {isFooterZebraStyle && (
                <InputContainer>
                    <ColorPickerInput
                        label={`${t('Background color')} 2`}
                        value={footerBackgroundColor2}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'footer',
                                'backgroundColor2',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
            )}
            <InputContainer>
                <Label>{t('Border')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={footerBorderTopStyle}
                        onChange={(e) =>
                            handleTableStyleChange(
                                'footer',
                                'borderTopStyle',
                                e.target.value
                            )
                        }
                        as="select"
                        id="select-theme"
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    key={`select-${value}-${label}`}
                                    value={value}
                                    disabled={optionDisabled}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={t('Border color')}
                    value={footerBorderTopColor}
                    onChange={(tempColor) =>
                        handleTableStyleChange(
                            'footer',
                            'borderTopColor',
                            tempColor
                        )
                    }
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Font size')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={footerFontSize}
                        onChange={(e) =>
                            handleTableStyleChange(
                                'footer',
                                'fontSize',
                                e.target.value
                            )
                        }
                        as="select"
                        id="select-footer-fontsize"
                    >
                        {FONT_SIZES.map(({ label, value }) => (
                            <option
                                key={`select-${value}-${label}`}
                                value={value}
                            >
                                {label}
                            </option>
                        ))}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <Label>{t('Bold')}</Label>
                <CheckboxInput
                    id="footerFontBold"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'footer',
                            'fontBold',
                            e.target.checked
                        )
                    }
                    checked={footerFontBold}
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Italics')}</Label>
                <CheckboxInput
                    id="footerFontItalic"
                    onChange={(e) =>
                        handleTableStyleChange(
                            'footer',
                            'fontItalic',
                            e.target.checked
                        )
                    }
                    checked={footerFontItalic}
                />
            </InputContainer>
        </FlexColumn>
    );
};

StylesTab.propTypes = {
    handleStyleChange: PropTypes.func,
    style: PropTypes.object,
};

export default StylesTab;
