import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { FlexColumn } from '@components/styled';

import { CheckboxInput, Submenu } from '../Styles';

const ResultsTable = styled.table`
    th {
        color: #4f51a1;
        font-size: 13px;
        padding: 0 2px;

        &.name-column {
            width: 145px;
        }

        &.ref-column {
            width: 136px;
        }

        &.visible-column {
            text-align: center;
        }
    }

    td {
        font-size: 12px;
        padding: 4px 2px;

        &.visible-column {
            text-align: center;
        }
    }
`;

const orderSort = (a, b) => {
    if (a.order > b.order) {
        return 1;
    }
    if (b.order > a.order) {
        return -1;
    }
    return 0;
};

const ResultsTab = ({ handleValueChange, value } = {}) => {
    const { t } = useTranslation();
    const preparedCols = Object.keys(value.cols)
        .map((key) => ({ ...value.cols[key], id: key }))
        .sort(orderSort);

    const { results = {} } = value;

    const handleChangeFooterColumn = (key, elementKey, toAttr, newValue) => {
        let tempResults = { ...value.results };
        tempResults[key].columns[elementKey][toAttr] = newValue;
        handleValueChange({
            ...value,
            results: tempResults,
        });
    };

    const handleChangeVisibleFooterRow = (key, visible) => {
        let tempResults = { ...value.results };
        tempResults[key].visible = visible;
        handleValueChange({
            ...value,
            results: tempResults,
        });
    };

    const renderFooterElement = (key, elementKey, refsUsed) => {
        const element = results[key].columns[elementKey];
        const id = `check-${key}-${elementKey}`;

        return (
            <tr key={id}>
                <td>{element.label}</td>
                <td>
                    <Form.Control
                        as="select"
                        value={element.ref}
                        onChange={(e) =>
                            handleChangeFooterColumn(
                                key,
                                elementKey,
                                'ref',
                                e.target.value
                            )
                        }
                    >
                        {preparedCols
                            .filter(
                                (col, index) =>
                                    index !== 0 &&
                                    (col.id === element.ref ||
                                        isNil(refsUsed[col.id]))
                            )
                            .map((col) => (
                                <option key={col.id} value={col.id}>
                                    {col.label}
                                </option>
                            ))}
                    </Form.Control>
                </td>
                <td className="visible-column">
                    <CheckboxInput
                        id={id}
                        onChange={(e) =>
                            handleChangeFooterColumn(
                                key,
                                elementKey,
                                'visible',
                                e.target.checked
                            )
                        }
                        checked={element.visible}
                    />
                </td>
            </tr>
        );
    };
    const renderResult = (key) => {
        const refsUsed = Object.keys(results[key].columns).reduce(
            (acc, currentKey) => {
                acc[results[key].columns[currentKey].ref] = true;
                return acc;
            },
            {}
        );
        return (
            <FlexColumn key={`row-data-${key}`}>
                <Submenu>{results[key].label}</Submenu>
                <ResultsTable>
                    <thead>
                        <tr>
                            <th className="name-column">{t('Name')}</th>
                            <th className="ref-column">{t('Column')}</th>
                            <th className="visible-column">{t('Watch')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <strong>{t('Whole row')}</strong>
                            </td>
                            <td></td>
                            <td className="visible-column">
                                <CheckboxInput
                                    id="headerFontBold"
                                    onChange={(e) =>
                                        handleChangeVisibleFooterRow(
                                            key,
                                            e.target.checked
                                        )
                                    }
                                    checked={results[key].visible}
                                />
                            </td>
                        </tr>
                        {results[key].visible &&
                            Object.keys(results[key].columns).map(
                                (elementKey) =>
                                    renderFooterElement(
                                        key,
                                        elementKey,
                                        refsUsed
                                    )
                            )}
                    </tbody>
                </ResultsTable>
            </FlexColumn>
        );
    };

    return (
        <FlexColumn>
            {Object.keys(results).map((key) => renderResult(key))}
        </FlexColumn>
    );
};

ResultsTab.propTypes = {
    handleValueChange: PropTypes.func,
    value: PropTypes.object,
};

export default ResultsTab;
