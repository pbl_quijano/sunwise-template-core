import PropTypes from 'prop-types';
import { Draggable } from 'react-beautiful-dnd';
import { createPortal } from 'react-dom';
import styled from 'styled-components';

import { FlexRow } from '@components/styled';

import { CheckboxInput } from '../Styles';

const ListItemContainer = styled.div`
    align-items: center;
    background: #fff;
    border-bottom: 1px solid #ccc;
    color: #848bab;
    display: flex;
    padding: 8px 0;
    .drag-handle {
        height: 16px;
        i {
            display: block;
        }
    }
    .name-column {
        flex-basis: 0;
        flex-grow: 1;
        padding: 0 4px;
        align-items: center;
    }

    .text-column {
        width: 110px;
        padding: 0 4px;
    }

    .actions-column {
        width: 50px;
        padding: 0 4px;
    }
`;

const ColNameText = styled.span`
    font-size: 13px;
    font-weight: bold;
    margin-left: 8px;
`;

const ColEditableText = styled.span`
    font-size: 13px;
`;

const StyledInput = styled.input`
    width: 100%;
    font-size: 13px;
    background-color: rgba(255, 255, 255, 0.02);
    border-radius: 3px;
    border: 1px solid #eff1fb;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    color: #848bab;
    padding: 4px 0 4px 6px;
    font-weight: 400;
    line-height: 1.5;
`;

const IconButton = styled.div`
    border-radius: 50%;
    text-align: center;
    cursor: pointer;
    margin-right: 4px;
    min-width: 14px;
    i {
        display: block;
        font-size: 14px;
        color: ${({ color }) => color};
    }
`;

const _dragEl = document.getElementById('fixdraggable');

const optionalPortal = (styles, element) => {
    if (styles.position === 'fixed') {
        return createPortal(element, _dragEl);
    }

    return element;
};

const SortableItem = ({
    col,
    editableTextField,
    editMode,
    handleSave,
    id,
    index,
    onChangeCheckbox,
    setEditableTextField,
    setEditMode,
}) => {
    const { id: colId, editable_label, label, visible } = col;

    const prepareEditMode = () => {
        setEditMode(colId);
        setEditableTextField(editable_label || label);
    };

    const cancelEditMode = () => {
        setEditMode('');
        setEditableTextField('');
    };

    const onChangeEdtitableTextField = (event) => {
        setEditableTextField(event.target.value);
    };

    const renderEditButtons = () => {
        if (editMode) {
            return (
                <>
                    <IconButton color="#008dff" onClick={handleSave}>
                        <i className="fas fa-save" />
                    </IconButton>
                    <IconButton color="#ff0000" onClick={cancelEditMode}>
                        <i className="fas fa-times" />
                    </IconButton>
                </>
            );
        }
        return (
            <>
                <IconButton color="#008dff" onClick={prepareEditMode}>
                    <i className="fas fa-edit" />
                </IconButton>

                <CheckboxInput
                    id={`check-${colId}`}
                    onChange={(e) => onChangeCheckbox(colId, e.target.checked)}
                    checked={visible}
                />
            </>
        );
    };

    const renderEditText = () => {
        if (editMode) {
            return (
                <StyledInput
                    onChange={onChangeEdtitableTextField}
                    value={editableTextField}
                />
            );
        }
        return <ColEditableText>{editable_label || label}</ColEditableText>;
    };

    return (
        <Draggable draggableId={id} index={index}>
            {(provided) => (
                <div>
                    {optionalPortal(
                        provided.draggableProps.style,
                        <ListItemContainer
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                        >
                            <FlexRow className="name-column">
                                <span className="drag-handle">
                                    <i className="fas fa-sort"></i>
                                </span>

                                <ColNameText>{label}</ColNameText>
                            </FlexRow>
                            <FlexRow className="text-column">
                                {renderEditText()}
                            </FlexRow>
                            <FlexRow className="actions-column">
                                {renderEditButtons()}
                            </FlexRow>
                        </ListItemContainer>
                    )}
                </div>
            )}
        </Draggable>
    );
};

SortableItem.propTypes = {
    col: PropTypes.object,
    editableTextField: PropTypes.string,
    editMode: PropTypes.bool,
    handleSave: PropTypes.func,
    id: PropTypes.string,
    index: PropTypes.number,
    onChangeCheckbox: PropTypes.func,
    setEditableTextField: PropTypes.func,
    setEditMode: PropTypes.func,
};

export default SortableItem;
