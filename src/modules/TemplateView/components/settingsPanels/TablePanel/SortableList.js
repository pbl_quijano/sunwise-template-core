import PropTypes from 'prop-types';
import { useState } from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

import SortableItem from './SortableItem';

const SortableList = ({
    id,
    items,
    onChangeCheckbox,
    onChangeEditableText,
    onDragEnd,
}) => {
    const [editItd, setEditId] = useState('');
    const [editableTextField, setEditableTextField] = useState('');

    const handleSave = () => {
        onChangeEditableText(editItd, editableTextField);
        setEditId('');
        setEditableTextField('');
    };

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId={id}>
                {(provided) => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                        {items.map((value, index) => (
                            <SortableItem
                                col={value}
                                id={`${id}-item-${index}`}
                                index={index}
                                key={`${id}-item-${index}`}
                                handleSave={handleSave}
                                editMode={editItd === value.id}
                                setEditableTextField={setEditableTextField}
                                editableTextField={editableTextField}
                                setEditMode={setEditId}
                                onChangeCheckbox={onChangeCheckbox}
                            />
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </DragDropContext>
    );
};

SortableList.propTypes = {
    id: PropTypes.string,
    items: PropTypes.array,
    onChangeCheckbox: PropTypes.func,
    onChangeEditableText: PropTypes.func,
    onDragEnd: PropTypes.func,
};

export default SortableList;
