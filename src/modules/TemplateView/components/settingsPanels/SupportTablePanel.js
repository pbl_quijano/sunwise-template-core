import PropTypes from 'prop-types';
import { Form, Tab, Tabs } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ColorPickerInput from '@components/ColorPickerInput';
import { FlexColumn } from '@components/styled';

import {
    CheckboxInput,
    InputContainer,
    Label,
    StyledInputGroup,
    Submenu,
} from './Styles';

const SupportTablePanel = ({ style = {}, handleStyleChange } = {}) => {
    const { t } = useTranslation();
    const { borderStyle = 'hidden', borderColor = '#000' } = style.table;
    const {
        color: headerColor = '#fff',
        backgroundColor: headerBackgroundColor = '#000',
        fontBold: headerFontBold,
        fontItalic: headerFontItalic,
        roundedBorders,
    } = style.header;
    const {
        color: bodyColor = '#000',
        backgroundColor1: bodyBackgroundColor1 = '#fff',
        backgroundColor2: bodyBackgroundColor2 = '#ccc',
        fontBold: bodyFontBold,
        fontItalic: bodyFontItalic,
        isZebraStyle,
    } = style.body;
    const {
        borderTopColor: footerBorderTopColor,
        borderTopStyle: footerBorderTopStyle,
        color: footerColor = '#000',
        backgroundColor1: footerBackgroundColor1 = '#fff',
        backgroundColor2: footerBackgroundColor2 = '#ccc',
        fontBold: footerFontBold,
        fontItalic: footerFontItalic,
        isZebraStyle: isFooterZebraStyle,
    } = style.footer;
    const options = [
        { value: 'hidden', label: t('No border') },
        { value: 'solid', label: t('Normal') },
        { value: 'dashed', label: t('Dashed') },
        { value: 'dotted', label: t('Dotted') },
    ];

    const handleTableStyleChange = (type, field, value) => {
        let newStyle = {};
        let tableStyle = Object.assign({}, style[type]);
        tableStyle[field] = value;
        newStyle[type] = tableStyle;
        handleStyleChange({
            ...style,
            ...newStyle,
        });
    };

    return (
        <Tabs>
            <Tab
                className="pr-3 pl-3"
                eventKey={t('Properties')}
                title={t('Properties')}
            >
                <FlexColumn>
                    <Submenu>{t('Table')}</Submenu>
                    <InputContainer>
                        <Label>{t('Border')}</Label>
                        <StyledInputGroup>
                            <Form.Control
                                as="select"
                                id="select-theme"
                                value={borderStyle}
                                onChange={(e) =>
                                    handleTableStyleChange(
                                        'table',
                                        'borderStyle',
                                        e.target.value
                                    )
                                }
                            >
                                {options.map(
                                    ({
                                        label,
                                        value,
                                        disabled: optionDisabled,
                                    }) => (
                                        <option
                                            disabled={optionDisabled}
                                            key={`select-${value}-${label}`}
                                            value={value}
                                        >
                                            {label}
                                        </option>
                                    )
                                )}
                            </Form.Control>
                        </StyledInputGroup>
                    </InputContainer>
                    <InputContainer>
                        <ColorPickerInput
                            label={t('Border color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'table',
                                    'borderColor',
                                    tempColor
                                )
                            }
                            value={borderColor}
                        />
                    </InputContainer>

                    <Submenu>{t('Header')}</Submenu>
                    <InputContainer>
                        <ColorPickerInput
                            label={t('Border color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'header',
                                    'backgroundColor',
                                    tempColor
                                )
                            }
                            value={headerBackgroundColor}
                        />
                    </InputContainer>
                    <InputContainer>
                        <ColorPickerInput
                            label={t('Font color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'header',
                                    'color',
                                    tempColor
                                )
                            }
                            value={headerColor}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Bold')}</Label>
                        <CheckboxInput
                            checked={headerFontBold}
                            id="headerFontBold"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'header',
                                    'fontBold',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Italics')}</Label>
                        <CheckboxInput
                            checked={headerFontItalic}
                            id="headerFontItalic"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'header',
                                    'fontItalic',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Rounded')}</Label>
                        <CheckboxInput
                            checked={roundedBorders}
                            id="roundedBorders"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'header',
                                    'roundedBorders',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>

                    <Submenu>{t('Body')}</Submenu>

                    <InputContainer>
                        <ColorPickerInput
                            label={t('Font color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'body',
                                    'color',
                                    tempColor
                                )
                            }
                            value={bodyColor}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Alternating colors')}</Label>
                        <CheckboxInput
                            checked={isZebraStyle}
                            id="isZebraStyle"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'body',
                                    'isZebraStyle',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <ColorPickerInput
                            label={`${t('Background color')} ${
                                isZebraStyle ? ' 1' : ''
                            }`}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'body',
                                    'backgroundColor1',
                                    tempColor
                                )
                            }
                            value={bodyBackgroundColor1}
                        />
                    </InputContainer>
                    {isZebraStyle && (
                        <InputContainer>
                            <ColorPickerInput
                                label={`${t('Background color')} 2`}
                                onChange={(tempColor) =>
                                    handleTableStyleChange(
                                        'body',
                                        'backgroundColor2',
                                        tempColor
                                    )
                                }
                                value={bodyBackgroundColor2}
                            />
                        </InputContainer>
                    )}
                    <InputContainer>
                        <Label>{t('Bold')}</Label>
                        <CheckboxInput
                            checked={bodyFontBold}
                            id="bodyFontBold"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'body',
                                    'fontBold',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Italics')}</Label>
                        <CheckboxInput
                            id="bodyFontItalic"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'body',
                                    'fontItalic',
                                    e.target.checked
                                )
                            }
                            checked={bodyFontItalic}
                        />
                    </InputContainer>

                    <Submenu>{t('Table footer')}</Submenu>
                    <InputContainer>
                        <ColorPickerInput
                            label={t('Font color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'footer',
                                    'color',
                                    tempColor
                                )
                            }
                            value={footerColor}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Alternating colors')}</Label>
                        <CheckboxInput
                            checked={isFooterZebraStyle}
                            id="isFooterZebraStyle"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'footer',
                                    'isZebraStyle',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <ColorPickerInput
                            label={`${t('Background color')} ${
                                isFooterZebraStyle ? ' 1' : ''
                            }`}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'footer',
                                    'backgroundColor1',
                                    tempColor
                                )
                            }
                            value={footerBackgroundColor1}
                        />
                    </InputContainer>
                    {isFooterZebraStyle && (
                        <InputContainer>
                            <ColorPickerInput
                                label={`${t('Background color')} 2`}
                                onChange={(tempColor) =>
                                    handleTableStyleChange(
                                        'footer',
                                        'backgroundColor2',
                                        tempColor
                                    )
                                }
                                value={footerBackgroundColor2}
                            />
                        </InputContainer>
                    )}
                    <InputContainer>
                        <Label>{t('Border')}</Label>
                        <StyledInputGroup>
                            <Form.Control
                                as="select"
                                id="select-theme"
                                onChange={(e) =>
                                    handleTableStyleChange(
                                        'footer',
                                        'borderTopStyle',
                                        e.target.value
                                    )
                                }
                                value={footerBorderTopStyle}
                            >
                                {options.map(
                                    ({
                                        label,
                                        value,
                                        disabled: optionDisabled,
                                    }) => (
                                        <option
                                            disabled={optionDisabled}
                                            key={`select-${value}-${label}`}
                                            value={value}
                                        >
                                            {label}
                                        </option>
                                    )
                                )}
                            </Form.Control>
                        </StyledInputGroup>
                    </InputContainer>
                    <InputContainer>
                        <ColorPickerInput
                            label={t('Border color')}
                            onChange={(tempColor) =>
                                handleTableStyleChange(
                                    'footer',
                                    'borderTopColor',
                                    tempColor
                                )
                            }
                            value={footerBorderTopColor}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Bold')}</Label>
                        <CheckboxInput
                            checked={footerFontBold}
                            id="footerFontBold"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'footer',
                                    'fontBold',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Italics')}</Label>
                        <CheckboxInput
                            checked={footerFontItalic}
                            id="footerFontItalic"
                            onChange={(e) =>
                                handleTableStyleChange(
                                    'footer',
                                    'fontItalic',
                                    e.target.checked
                                )
                            }
                        />
                    </InputContainer>
                </FlexColumn>
            </Tab>
        </Tabs>
    );
};

SupportTablePanel.propTypes = {
    style: PropTypes.object,
    handleStyleChange: PropTypes.func,
};

export default SupportTablePanel;
