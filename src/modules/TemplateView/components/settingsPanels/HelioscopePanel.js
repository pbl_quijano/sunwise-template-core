import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { InputContainer, Label, StyledInputGroup } from './Styles';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const HelioscopePanel = ({ handleStyleChange, style = {} } = {}) => {
    const { t } = useTranslation();
    const { objectFit = 'cover' } = style;
    const options = [
        { value: 'cover', label: t('Fill') },
        { value: 'contain', label: t('Adjust') },
        { value: 'fill', label: t('Expand') },
    ];

    return (
        <Container>
            <InputContainer>
                <Label>{t('Adjust image')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        as="select"
                        id="select-theme"
                        onChange={(e) =>
                            handleStyleChange({
                                ...style,
                                objectFit: e.target.value,
                            })
                        }
                        value={objectFit}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    key={`select-${value}-${label}`}
                                    value={value}
                                    disabled={optionDisabled}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
        </Container>
    );
};

HelioscopePanel.propTypes = {
    handleStyleChange: PropTypes.func,
    style: PropTypes.object,
};

export default HelioscopePanel;
