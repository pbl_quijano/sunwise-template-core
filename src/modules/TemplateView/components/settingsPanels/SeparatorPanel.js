import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';

import ColorPickerInput from '@components/ColorPickerInput';
import { FlexColumn } from '@components/styled';

import { InputContainer, Label, StyledInputGroup } from './Styles';

const SeparatorPanel = ({ handleStyleChange, style = {} } = {}) => {
    const { t } = useTranslation();
    const { borderBottomStyle = 'solid', borderBottomColor = '#000' } = style;
    const options = [
        { value: 'solid', label: t('Normal') },
        { value: 'dashed', label: t('Dashed') },
        { value: 'dotted', label: t('Dotted') },
    ];
    return (
        <FlexColumn>
            <InputContainer>
                <Label>{t('Type')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        as="select"
                        id="select-theme"
                        onChange={(e) =>
                            handleStyleChange({
                                ...style,
                                borderBottomStyle: e.target.value,
                            })
                        }
                        value={borderBottomStyle}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    disabled={optionDisabled}
                                    key={`select-${value}-${label}`}
                                    value={value}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={t('Color')}
                    value={borderBottomColor}
                    onChange={(tempColor) =>
                        handleStyleChange({
                            ...style,
                            borderBottomColor: tempColor,
                        })
                    }
                />
            </InputContainer>
        </FlexColumn>
    );
};

SeparatorPanel.propTypes = {
    handleStyleChange: PropTypes.func,
    style: PropTypes.object,
};

export default SeparatorPanel;
