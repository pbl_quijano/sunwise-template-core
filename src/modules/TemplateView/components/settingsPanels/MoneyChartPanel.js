import PropTypes from 'prop-types';
import { Form, InputGroup } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import ColorPickerInput from '@components/ColorPickerInput';

const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

const StyledInputGroup = styled(InputGroup)`
    height: 46px;
    width: 133px;
    & select {
        height: 46px;
        width: 133px;
        border: 1px solid #ecedf0;
        border-radius: 3px;
        background-color: #ffffff;
    }
`;

const InputContainer = styled.div`
    display: flex;
    align-items: center;
    margin: 4px 0;
`;

const Label = styled.span`
    min-height: 16px;
    width: 135px;
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    text-align: right;
    margin-right: 23px;
`;

const MoneyChartPanel = ({ handleValueChange, value = {} } = {}) => {
    const { t } = useTranslation();
    const { colors = ['#000', '#000'], type = 'bar' } = value;
    const options = [
        { value: 'bar', label: t('Bars') },
        { value: 'line', label: t('Line') },
        { value: 'area', label: t('Area') },
    ];

    const onChangeColor = (tempColors) =>
        handleValueChange({ ...value, colors: tempColors });

    const onChangeType = (e) =>
        handleValueChange({ ...value, type: e.target.value });

    return (
        <Container>
            <InputContainer>
                <Label>{t('Type')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        as="select"
                        id="select-theme"
                        onChange={onChangeType}
                        value={type}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    disabled={optionDisabled}
                                    key={`select-${value}-${label}`}
                                    value={value}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={`${t('Color')} 1`}
                    value={colors[0]}
                    onChange={(tempColor) =>
                        onChangeColor([tempColor, colors[1]])
                    }
                />
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={`${t('Color')} 2`}
                    value={colors[1]}
                    onChange={(tempColor) =>
                        onChangeColor([colors[0], tempColor])
                    }
                />
            </InputContainer>
        </Container>
    );
};

MoneyChartPanel.propTypes = {
    handleValueChange: PropTypes.func,
    value: PropTypes.string,
};

export default MoneyChartPanel;
