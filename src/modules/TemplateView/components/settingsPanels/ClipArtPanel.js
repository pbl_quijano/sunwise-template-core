import toNumber from 'lodash/toNumber';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import ColorPickerInput from '@components/ColorPickerInput';
import { FlexColumn } from '@components/styled';

import { InputContainer, Label, StyledInputGroup } from './Styles';

const StyledInput = styled.input`
    background-color: #ffffff;
    border-radius: 3px;
    border: 1px solid #ecedf0;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    color: #848bab;
    height: 46px;
    padding: 6px 8px;
    width: 133px;
`;

const SliderContainer = styled.div`
    display: flex;
    width: 164px;

    .rc-slider {
        .rc-slider-tooltip {
            z-index: 1000;
        }

        .rc-slider-track {
            background-color: #f59300;
        }

        .rc-slider-handle {
            border: solid 2px #ff9900;

            &:hover {
                border-color: #ff9a00;
            }

            &:active {
                border-color: #ff9a00;
                box-shadow: 0 0 5px #ff9a00;
            }
        }

        .rc-slider-dot-active {
            border-color: #ff9900;
        }
    }
`;

const ClipArtPanel = ({ handleStyleChange, style = {} } = {}) => {
    const { t } = useTranslation();
    const {
        type = 'circle',
        background = '#000',
        corner = 0,
        stroke = '#000',
        strokeWidth = 0,
    } = style;
    const options = [
        { value: 'circle', label: t('Circle') },
        { value: 'ellipse', label: t('Ellipse') },
        { value: 'square', label: t('Square') },
        { value: 'rectangle', label: t('Rectangle') },
        { value: 'triangle', label: t('Triangle') },
        { value: 'hexagon', label: t('Hexagon') },
    ];

    const onAfterChange = (value) =>
        handleStyleChange({ ...style, corner: value });

    const onChangeStrokeWidth = (event) => {
        let numberValue = 0;
        if (!isNaN(event.target.value)) {
            numberValue = toNumber(event.target.value);
        }
        handleStyleChange({
            ...style,
            strokeWidth: numberValue,
        });
    };

    const preventNumberInput = (e) => e.preventDefault();

    const handle = ({ value, dragging, index, ...rest }) => (
        <Tooltip
            arrowContent={null}
            key={index}
            overlay={`${value}%`}
            placement="top"
            prefixCls="panel-image rc-tooltip"
            visible={dragging}
        >
            <Slider.Handle value={value} {...rest} />
        </Tooltip>
    );

    const renderRoundedCornersSlider = () => {
        if (type !== 'square' && type !== 'rectangle') return null;

        return (
            <InputContainer>
                <Label>{t('Corner')}</Label>
                <SliderContainer>
                    <Slider
                        defaultValue={corner}
                        handle={handle}
                        marks={{ 0: '0%', 100: '100%' }}
                        max={100}
                        min={0}
                        onAfterChange={onAfterChange}
                        step={10}
                    />
                </SliderContainer>
            </InputContainer>
        );
    };

    return (
        <FlexColumn>
            <InputContainer>
                <Label>{t('Type')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        as="select"
                        id="select-theme"
                        onChange={(e) =>
                            handleStyleChange({
                                ...style,
                                type: e.target.value,
                            })
                        }
                        value={type}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    disabled={optionDisabled}
                                    key={`select-${value}-${label}`}
                                    value={value}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    hasAlphaChanel={true}
                    label={t('Background')}
                    onChange={(tempColor) =>
                        handleStyleChange({ ...style, background: tempColor })
                    }
                    value={background}
                />
            </InputContainer>
            <InputContainer>
                <Label>{t('Border')}</Label>
                <StyledInput
                    min="0"
                    onChange={onChangeStrokeWidth}
                    onKeyPress={preventNumberInput}
                    type="number"
                    step="any"
                    value={strokeWidth}
                />
            </InputContainer>
            <InputContainer>
                <ColorPickerInput
                    label={t('Border color')}
                    onChange={(tempColor) =>
                        handleStyleChange({ ...style, stroke: tempColor })
                    }
                    value={stroke}
                />
            </InputContainer>

            {renderRoundedCornersSlider()}
        </FlexColumn>
    );
};

ClipArtPanel.propTypes = {
    handleStyleChange: PropTypes.func,
    style: PropTypes.object,
};

export default ClipArtPanel;
