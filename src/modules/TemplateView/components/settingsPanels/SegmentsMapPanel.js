import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import ColorPickerInput from '@components/ColorPickerInput';

import {
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SOLAR_MODULE_COLOR,
    SOLAR_MODULE_STROKE_COLOR,
} from '@constants/maps';

import ProposalSelector from './ProposalSelector';
import { CheckboxInput, InputContainer, Submenu } from './Styles';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const Label = styled.span`
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-right: 23px;
    min-height: 16px;
    text-align: right;
    width: 135px;
`;

const SegmentsMapPanel = ({
    catalogs,
    handleStyleChange,
    handleValueChange,
    hasProposalSelector,
    proposalSelectorDisabled,
    style = {},
    value = {},
} = {}) => {
    const { t } = useTranslation();
    const {
        segment = {},
        solarModule = {},
        showLabels = true,
        showProjectMarker = true,
    } = style;
    const {
        fillColor: segmentFillColor = SEGMENT_COLOR,
        strokeColor: segmentStrokeColor = SEGMENT_STROKE_COLOR,
    } = segment;
    const {
        fillColor: solarModuleFillColor = SOLAR_MODULE_COLOR,
        strokeColor: solarModuleStrokeColor = SOLAR_MODULE_STROKE_COLOR,
    } = solarModule;

    const handleTableStyleChange = (type, field, value) => {
        let newStyle = {};
        let segmentStyle = Object.assign({}, style[type]);
        segmentStyle[field] = value;
        newStyle[type] = segmentStyle;
        handleStyleChange({
            ...style,
            ...newStyle,
        });
    };

    const onChangeValueVisibilty = (key, value) => {
        handleStyleChange({
            ...style,
            [key]: value,
        });
    };

    return (
        <>
            <ProposalSelector
                catalogs={catalogs}
                disabled={proposalSelectorDisabled}
                handleValueChange={handleValueChange}
                value={value}
                visible={hasProposalSelector}
            />
            <Container>
                <Submenu>{t('Project location')}</Submenu>
                <InputContainer>
                    <Label>{t('Show')}</Label>
                    <CheckboxInput
                        id="show-project-marker"
                        onChange={(e) =>
                            onChangeValueVisibilty(
                                'showProjectMarker',
                                e.target.checked
                            )
                        }
                        checked={showProjectMarker}
                    />
                </InputContainer>
                <Submenu>{t('Segment')}</Submenu>
                <InputContainer>
                    <ColorPickerInput
                        hasAlphaChanel={true}
                        label={t('Background')}
                        value={segmentFillColor}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'segment',
                                'fillColor',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
                <InputContainer>
                    <ColorPickerInput
                        label={t('Border')}
                        hasAlphaChanel={true}
                        value={segmentStrokeColor}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'segment',
                                'strokeColor',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
                <InputContainer>
                    <Label>{t('Show measurements')}</Label>
                    <CheckboxInput
                        id="show-labels"
                        onChange={(e) =>
                            onChangeValueVisibilty(
                                'showLabels',
                                e.target.checked
                            )
                        }
                        checked={showLabels}
                    />
                </InputContainer>
                <Submenu>{t('Panel')}</Submenu>
                <InputContainer>
                    <ColorPickerInput
                        hasAlphaChanel={true}
                        label={t('Background')}
                        value={solarModuleFillColor}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'solarModule',
                                'fillColor',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
                <InputContainer>
                    <ColorPickerInput
                        label={t('Border')}
                        hasAlphaChanel={true}
                        value={solarModuleStrokeColor}
                        onChange={(tempColor) =>
                            handleTableStyleChange(
                                'solarModule',
                                'strokeColor',
                                tempColor
                            )
                        }
                    />
                </InputContainer>
            </Container>
        </>
    );
};

SegmentsMapPanel.propTypes = {
    catalogs: PropTypes.array,
    handleValueChange: PropTypes.func,
    handleStyleChange: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    proposalSelectorDisabled: PropTypes.bool,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default SegmentsMapPanel;
