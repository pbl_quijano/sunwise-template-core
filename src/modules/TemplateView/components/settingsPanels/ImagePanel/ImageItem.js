import PropTypes from 'prop-types';
import styled from 'styled-components';

const ImageContainer = styled.div`
    display: flex;
    width: 88px;
    flex-direction: column;
    margin: 4px 6px;
    cursor: pointer;
    &.selected {
        img {
            border: 1px solid #ff9a00;
        }
        span {
            font-weight: bold;
            color: #ff9a00;
        }
    }
`;

const Image = styled.img`
    width: 88px;
    height: 88px;
    margin-bottom: 2px;
    object-fit: contain;
    border: 1px solid #ecedf0;
`;

const Name = styled.span`
    height: 15px;
    width: 88px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 12px;
    line-height: 15px;
    color: #848bab;
`;

const ImageItem = ({ handleSelectImage, isSelected, name, urlFile }) => {
    return (
        <ImageContainer
            className={`image-container ${isSelected && 'selected'}`}
        >
            <Image src={urlFile} onClick={handleSelectImage} />
            <Name title={name}>{name}</Name>
        </ImageContainer>
    );
};

ImageItem.propTypes = {
    handleSelectImage: PropTypes.func,
    isSelected: PropTypes.bool,
    name: PropTypes.string,
    urlFile: PropTypes.string,
};

export default ImageItem;
