import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import Tooltip from 'rc-tooltip';
import { Form } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { InputContainer, Label, StyledInputGroup } from '../Styles';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const SliderContainer = styled.div`
    display: flex;
    width: 164px;
    .rc-slider {
        .rc-slider-tooltip {
            z-index: 1000;
        }
        .rc-slider-track {
            background-color: #f59300;
        }
        .rc-slider-handle {
            border: solid 2px #ff9900;
            &:hover {
                border-color: #ff9a00;
            }
            &:active {
                border-color: #ff9a00;
                box-shadow: 0 0 5px #ff9a00;
            }
        }
        .rc-slider-dot-active {
            border-color: #ff9900;
        }
    }
`;

const StyleTabContent = ({ handleStyleChange, style = {} }) => {
    const { t } = useTranslation();
    const { objectFit = 'cover', opacity = 1 } = style;
    const options = [
        { value: 'cover', label: t('Fill') },
        { value: 'contain', label: t('Adjust') },
        { value: 'fill', label: t('Expand') },
    ];

    const onAfterChange = (value) =>
        handleStyleChange({ ...style, opacity: value / 100 });

    const handle = ({ value, dragging, index, ...rest }) => {
        return (
            <Tooltip
                arrowContent={null}
                key={index}
                placement="top"
                prefixCls="panel-image rc-tooltip"
                overlay={`${value}%`}
                visible={dragging}
            >
                <Slider.Handle value={value} {...rest} />
            </Tooltip>
        );
    };
    return (
        <Container>
            <InputContainer>
                <Label>{t('Adjust image')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        as="select"
                        id="select-theme"
                        onChange={(e) =>
                            handleStyleChange({
                                ...style,
                                objectFit: e.target.value,
                            })
                        }
                        value={objectFit}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    disabled={optionDisabled}
                                    key={`select-${value}-${label}`}
                                    value={value}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            <InputContainer>
                <Label>{t('Opacity')}</Label>
                <SliderContainer>
                    <Slider
                        defaultValue={opacity * 100}
                        handle={handle}
                        marks={{ 0: '0%', 100: '100%' }}
                        max={100}
                        min={0}
                        step={10}
                        onAfterChange={onAfterChange}
                    />
                </SliderContainer>
            </InputContainer>
        </Container>
    );
};

StyleTabContent.propTypes = {
    handleStyleChange: PropTypes.func,
    style: PropTypes.object,
};

export default StyleTabContent;
