import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { getFileNameByPath } from '@helpers/utils';

import ImageUploader from '@modules/ImageUploader';

import { InputContainer, Label, Submenu } from '../Styles';

import ImageItem from './ImageItem';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

const ImageUploaderContainer = styled.div`
    display: flex;
    width: 164px;
`;

const ImageLabel = styled(Label)`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    opacity: 0.5;
    cursor: default;
    ${({ isLoaded }) => isLoaded && 'font-weight: 600;opacity: 1;'}
`;

const ImagesContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const SourceTabContent = ({
    companyImageGallery = [],
    handleValueChange,
    value,
} = {}) => {
    const { t } = useTranslation();
    const isGalleryValue = companyImageGallery
        .map(({ urlFile }) => urlFile)
        .includes(value);
    const fileName =
        value && !isGalleryValue ? getFileNameByPath(value) : t('File upload');
    return (
        <Container>
            <Submenu>{t('File')}</Submenu>
            <InputContainer>
                <ImageLabel
                    title={fileName}
                    isLoaded={value && !isGalleryValue}
                >
                    {fileName}
                </ImageLabel>
                <ImageUploaderContainer>
                    <ImageUploader
                        currentValue={value}
                        handleValueChange={handleValueChange}
                    />
                </ImageUploaderContainer>
            </InputContainer>
            <Submenu>{t('Default')}</Submenu>
            <ImagesContainer>
                {companyImageGallery.map(({ id, name, urlFile }) => (
                    <ImageItem
                        handleSelectImage={() =>
                            urlFile !== value && handleValueChange(urlFile)
                        }
                        isSelected={urlFile === value}
                        key={id}
                        name={name}
                        urlFile={urlFile}
                    />
                ))}
            </ImagesContainer>
        </Container>
    );
};

SourceTabContent.propTypes = {
    companyImageGallery: PropTypes.array,
    handleValueChange: PropTypes.func,
    value: PropTypes.string,
};

export default SourceTabContent;
