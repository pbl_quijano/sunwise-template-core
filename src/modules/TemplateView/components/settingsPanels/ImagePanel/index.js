import PropTypes from 'prop-types';
import { Tab } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as mainSelectors from '@main/selectors';

import { StyledTabs } from '../Styles';

import SourceTabContent from './SourceTabContent';
import StyleTabContent from './StyleTabContent';

const ImagePanel = ({
    imageGallery,
    handleStyleChange,
    handleValueChange,
    style = {},
    value,
} = {}) => {
    const { t } = useTranslation();
    return (
        <StyledTabs>
            <Tab className="pr-3 pl-3" eventKey="Source" title={t('Source')}>
                <SourceTabContent
                    companyImageGallery={imageGallery}
                    handleValueChange={handleValueChange}
                    value={value}
                />
            </Tab>
            <Tab className="pr-3 pl-3" eventKey="Style" title={t('Style')}>
                <StyleTabContent
                    handleStyleChange={handleStyleChange}
                    style={style}
                />
            </Tab>
        </StyledTabs>
    );
};

const mapStateToProps = createStructuredSelector({
    imageGallery: mainSelectors.getFetchImageWidgetGalleryData,
});

ImagePanel.propTypes = {
    imageGallery: PropTypes.array,
    handleStyleChange: PropTypes.func,
    handleValueChange: PropTypes.func,
    style: PropTypes.object,
    value: PropTypes.string,
};

export default connect(mapStateToProps, null)(ImagePanel);
