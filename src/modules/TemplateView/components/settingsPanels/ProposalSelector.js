import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

export const StyledLabel = styled.span`
    font-size: 13px;
    font-style: normal;
    font-weight: 600;
    color: #848bab;
    margin-bottom: 0.5rem;
`;

const ProposalDataFrom = ({
    catalogs,
    disabled,
    handleValueChange,
    value,
    visible,
} = {}) => {
    const { t } = useTranslation();

    if (!visible) return null;

    const { proposal_number } = value;

    const handleChange = (proposalSourceId) => {
        handleValueChange({
            ...value,
            proposal_number: proposalSourceId,
        });
    };

    return (
        <Form.Group>
            <StyledLabel>
                {t('What proposal is the data taken from?')}
            </StyledLabel>
            <Form.Control
                as="select"
                disabled={disabled}
                name="proposal_number"
                onChange={(e) => handleChange(e.target.value)}
                value={proposal_number}
            >
                <option key="proposal-option" value="" disabled>
                    {t('Select proposal')}
                </option>
                {catalogs.map(({ name, id }) => (
                    <option
                        key={`proposal-option-${id}`}
                        value={id}
                        disabled={disabled}
                    >
                        {name}
                    </option>
                ))}
            </Form.Control>
        </Form.Group>
    );
};

ProposalDataFrom.propTypes = {
    catalogs: PropTypes.array,
    disabled: PropTypes.bool,
    handleValueChange: PropTypes.func,
    value: PropTypes.object,
    visible: PropTypes.bool,
};

export default ProposalDataFrom;
