import PropTypes from 'prop-types';
import { InputGroup } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { FlexColumn } from '@components/styled';

import { CheckboxInput } from '../Styles';

const StyledInputGroup = styled(InputGroup)`
    height: 46px;
    width: 133px;

    & select {
        background-color: #ffffff;
        border-radius: 3px;
        border: 1px solid #ecedf0;
        height: 46px;
        width: 133px;
    }
`;

const InputContainer = styled.div`
    align-items: center;
    display: flex;
    margin: 4px 0;
`;

const Label = styled.span`
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-right: 23px;
    min-height: 16px;
    text-align: right;
    width: 135px;
`;

const GeneralTab = ({
    handleValueChange,
    proposalSelectorDisabled,
    typeChart,
    value = {},
} = {}) => {
    const { t } = useTranslation();
    const {
        type = 'bar',
        stacked_bar,
        categories,
        initCategoryIndex = '0',
        finishCategoryIndex = `${value.categories.length - 1}`,
    } = value;

    const onGetChartOptions = () => {
        return [
            {
                value: 'bar',
                label: t('Bars'),
                disabled: typeChart === 'kwh-chart',
            },
            { value: 'line', label: t('Line'), disabled: false },
            {
                value: 'area',
                label: t('Area'),
                disabled: typeChart === 'kwh-chart',
            },
        ];
    };

    const onChangeType = (e) => {
        const tempValue = {
            ...value,
            type: e.target.value,
        };
        handleValueChange({
            ...tempValue,
        });
    };

    const onChangeStackedBarType = (e) => {
        if (e.target.value !== stacked_bar.selected) {
            const tempValue = {
                ...value,
                stacked_bar: {
                    ...stacked_bar,
                    selected: e.target.value,
                },
            };
            handleValueChange({
                ...tempValue,
            });
        }
    };

    const onChangeStackedBarVisibilty = (visible) => {
        const tempValue = {
            ...value,
            stacked_bar: {
                ...stacked_bar,
                visible,
            },
        };
        handleValueChange({
            ...tempValue,
        });
    };

    const onChangeInitCategoryIndex = (e) => {
        if (e.target.value !== initCategoryIndex) {
            handleValueChange({
                ...value,
                initCategoryIndex: e.target.value,
            });
        }
    };

    const onChangeFinishCategoryIndex = (e) => {
        if (e.target.value !== finishCategoryIndex) {
            handleValueChange({
                ...value,
                finishCategoryIndex: e.target.value,
            });
        }
    };

    const categoriesOptions = categories.map((cat, index) => (
        <option key={`${cat}`} value={index}>
            {cat}
        </option>
    ));

    const renderStackedBarSection = () => {
        if (!stacked_bar || type !== 'bar') {
            return null;
        }
        const { visible, selected, label } = stacked_bar;
        return (
            <FlexColumn>
                <InputContainer>
                    <Label>{t('View details')}</Label>
                    <CheckboxInput
                        id="stacked-bar"
                        onChange={(e) =>
                            onChangeStackedBarVisibilty(e.target.checked)
                        }
                        checked={visible}
                    />
                </InputContainer>
                <InputContainer>
                    <Label>{t('Type')}</Label>
                    <StyledInputGroup>
                        <Form.Control
                            value={selected}
                            onChange={onChangeStackedBarType}
                            as="select"
                            id="select-stacked"
                            disabled={!visible}
                        >
                            {Object.keys(label).map((key) => (
                                <option
                                    key={`select-${key}-${label[key]}`}
                                    value={key}
                                >
                                    {label[key]}
                                </option>
                            ))}
                        </Form.Control>
                    </StyledInputGroup>
                </InputContainer>
            </FlexColumn>
        );
    };

    return (
        <FlexColumn>
            <InputContainer>
                <Label>{t('Type')}</Label>
                <StyledInputGroup>
                    <Form.Control
                        value={type}
                        onChange={onChangeType}
                        as="select"
                        id="select-type"
                    >
                        {onGetChartOptions().map(
                            ({
                                label,
                                value: optionValue,
                                disabled: optionDisabled,
                            }) => (
                                <option
                                    key={`select-${optionValue}-${label}`}
                                    value={optionValue}
                                    disabled={optionDisabled}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                </StyledInputGroup>
            </InputContainer>
            {proposalSelectorDisabled && (
                <>
                    <InputContainer>
                        <Label>{t('Initial category')}</Label>
                        <StyledInputGroup>
                            <Form.Control
                                value={initCategoryIndex}
                                onChange={onChangeInitCategoryIndex}
                                as="select"
                                id="select-init-category"
                            >
                                {categoriesOptions.filter(
                                    (e, index) => index < finishCategoryIndex
                                )}
                            </Form.Control>
                        </StyledInputGroup>
                    </InputContainer>
                    <InputContainer>
                        <Label>{t('Final category')}</Label>
                        <StyledInputGroup>
                            <Form.Control
                                value={finishCategoryIndex}
                                onChange={onChangeFinishCategoryIndex}
                                as="select"
                                id="select-finish-category"
                            >
                                {categoriesOptions.filter(
                                    (e, index) => index > initCategoryIndex
                                )}
                            </Form.Control>
                        </StyledInputGroup>
                    </InputContainer>
                </>
            )}

            {renderStackedBarSection()}
        </FlexColumn>
    );
};

GeneralTab.propTypes = {
    handleValueChange: PropTypes.func,
    proposalSelectorDisabled: PropTypes.bool,
    typeChart: PropTypes.string,
    value: PropTypes.object,
};

export default GeneralTab;
