import PropTypes from 'prop-types';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import ColorPickerInput from '@components/ColorPickerInput';
import { FlexColumn, FlexRow } from '@components/styled';

import { cloneElement } from '@helpers/utils';

import { CheckboxInput, Submenu } from '../Styles';

const InputContainer = styled.div`
    align-items: center;
    display: flex;
    margin: 4px 0;
`;

const StyledInputGroup = styled(FlexRow)`
    height: 46px;
    width: 133px;
`;

const Label = styled.span`
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-right: 23px;
    min-height: 16px;
    text-align: right;
    width: 135px;
`;

const StyledInput = styled.input`
    width: 92px;
    font-size: 13px;
    background-color: rgba(255, 255, 255, 0.02);
    border-radius: 3px;
    border: 1px solid #eff1fb;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    color: #848bab;
    padding: 4px 0 4px 6px;
    font-weight: 400;
    line-height: 1.5;
`;

const ColEditableText = styled.span`
    font-size: 13px;
    font-style: italic;
    width: 100px;
`;

const IconButton = styled.div`
    border-radius: 50%;
    text-align: center;
    cursor: pointer;
    margin: 4px;
    min-width: 14px;
    i {
        display: block;
        font-size: 14px;
        color: ${({ color }) => color};
    }
`;

const AnnotationsTab = ({ handleValueChange, value = {} } = {}) => {
    const { t } = useTranslation();
    const { annotations = [] } = value;
    const [editIndex, setEditIndex] = useState(null);
    const [editableTextField, setEditableTextField] = useState('');

    const onChangeEdtitableTextField = (event) => {
        setEditableTextField(event.target.value);
    };

    const cancelEditMode = () => {
        setEditIndex(null);
        setEditableTextField('');
    };

    const handleSave = () => {
        const tempAnnotations = cloneElement(annotations);
        tempAnnotations[editIndex].editable_label = editableTextField;
        handleValueChange({ ...value, annotations: tempAnnotations });
        setEditIndex(null);
        setEditableTextField('');
    };

    const onChangeValueVisibilty = (index, visible) => {
        const tempAnnotations = cloneElement(annotations);
        tempAnnotations[index].visible = visible;
        handleValueChange({ ...value, annotations: tempAnnotations });
    };

    const onChangeValueColor = (index, color) => {
        const tempAnnotations = cloneElement(annotations);
        tempAnnotations[index].color = color;
        handleValueChange({ ...value, annotations: tempAnnotations });
    };

    const prepareEditMode = (id, label) => {
        setEditIndex(id);
        setEditableTextField(label);
    };

    const renderEditText = (index, editable_label, label) => {
        if (editIndex === index) {
            return (
                <>
                    <StyledInput
                        onChange={onChangeEdtitableTextField}
                        value={editableTextField}
                    />{' '}
                    <IconButton color="#008dff" onClick={handleSave}>
                        <i className="fas fa-save" />
                    </IconButton>
                    <IconButton color="#ff0000" onClick={cancelEditMode}>
                        <i className="fas fa-times" />
                    </IconButton>
                </>
            );
        }
        return (
            <>
                <ColEditableText>{editable_label || label}</ColEditableText>
                <IconButton
                    color="#008dff"
                    onClick={() =>
                        prepareEditMode(index, editable_label || label)
                    }
                >
                    <i className="fas fa-edit" />
                </IconButton>
            </>
        );
    };

    const renderResult = (index, serie) => {
        const { label, visible, color = '#ff0000', editable_label } = serie;
        return (
            <FlexColumn key={`row-data-${label}`}>
                <Submenu>{label}</Submenu>
                <InputContainer>
                    <Label>{t('Text')}</Label>
                    <StyledInputGroup alignItems="center">
                        {renderEditText(index, editable_label, label)}
                    </StyledInputGroup>
                </InputContainer>
                <InputContainer>
                    <Label>{t('Visible')}</Label>
                    <CheckboxInput
                        id="stacked-bar"
                        onChange={(e) =>
                            onChangeValueVisibilty(index, e.target.checked)
                        }
                        checked={visible}
                    />
                </InputContainer>
                <InputContainer>
                    <ColorPickerInput
                        label={t('Color')}
                        value={color}
                        onChange={(tempColor) =>
                            onChangeValueColor(index, tempColor)
                        }
                    />
                </InputContainer>
            </FlexColumn>
        );
    };

    return (
        <FlexColumn>
            {annotations.map((annotation, index) =>
                renderResult(index, annotation)
            )}
        </FlexColumn>
    );
};

AnnotationsTab.propTypes = {
    handleValueChange: PropTypes.func,
    value: PropTypes.object,
};

export default AnnotationsTab;
