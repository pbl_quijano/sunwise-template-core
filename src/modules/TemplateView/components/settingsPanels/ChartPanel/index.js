import PropTypes from 'prop-types';
import { Tab } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ProposalSelector from '../ProposalSelector';
import { StyledTabs } from '../Styles';

import AnnotationsTab from './AnnotationsTab';
import GeneralTab from './GeneralTab';
import ValuesTab from './ValuesTab';

const ChartPanel = ({
    catalogs,
    handleValueChange,
    hasProposalSelector,
    proposalSelectorDisabled,
    type,
    value = {},
} = {}) => {
    const { t } = useTranslation();
    return (
        <>
            <ProposalSelector
                catalogs={catalogs}
                disabled={proposalSelectorDisabled}
                handleValueChange={handleValueChange}
                value={value}
                visible={hasProposalSelector}
            />
            <StyledTabs>
                <Tab
                    className="pr-3 pl-3"
                    eventKey="Properties"
                    title={t('Properties')}
                >
                    <GeneralTab
                        {...{
                            value,
                            handleValueChange,
                            proposalSelectorDisabled,
                        }}
                        typeChart={type}
                    />
                </Tab>
                {(!value.stacked_bar || !value.stacked_bar.visible) && (
                    <Tab
                        className="pr-3 pl-3"
                        eventKey="Values"
                        title={t('Value', { count: 2 })}
                    >
                        <ValuesTab {...{ value, handleValueChange }} />
                    </Tab>
                )}
                {value.annotations && (
                    <Tab
                        className="pr-3 pl-3"
                        eventKey="Indicators"
                        title={t('Indicators')}
                    >
                        <AnnotationsTab {...{ value, handleValueChange }} />
                    </Tab>
                )}
            </StyledTabs>
        </>
    );
};

ChartPanel.propTypes = {
    catalogs: PropTypes.array,
    handleValueChange: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    proposalSelectorDisabled: PropTypes.bool,
    type: PropTypes.string,
    value: PropTypes.object,
};

export default ChartPanel;
