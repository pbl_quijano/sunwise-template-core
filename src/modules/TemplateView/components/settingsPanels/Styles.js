import { InputGroup, Tabs } from 'react-bootstrap';
import styled from 'styled-components';

export const Label = styled.span`
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-right: 23px;
    min-height: 16px;
    text-align: right;
    width: 135px;
`;

export const StyledInputGroup = styled(InputGroup)`
    height: 46px;
    width: 133px;

    & select {
        background-color: #ffffff;
        border-radius: 3px;
        border: 1px solid #ecedf0;
        height: 46px;
        width: 133px;
    }
`;

export const InputContainer = styled.div`
    align-items: center;
    display: flex;
    margin: 6px 0;
`;

export const Submenu = styled.span`
    border-bottom: 1px solid #4f51a1;
    color: #4f51a1;
    font-size: 13px;
    line-height: 16px;
    margin-bottom: 6px;
    padding-bottom: 6px;
    margin-top: 6px;
    min-height: 16px;
`;

export const StyledTabs = styled(Tabs)`
    .nav-link {
        color: #aaa;
        font-size: 12px;

        &.active {
            color: #202253;
        }
    }
`;

export const CheckboxInput = styled.input.attrs(() => ({ type: 'checkbox' }))`
    cursor: pointer;
`;
