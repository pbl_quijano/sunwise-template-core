import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { FULL_EDITION_MODE } from '@constants/types';

import * as mainSelectors from '@main/selectors';

import OnePageView from './OnePageView';
import PageListView from './PageListView';

const TemplateContent = ({
    availableCatalogs,
    chartWidgets,
    editionLevel,
    froalaEditorConfig,
    horizontalScrollMode,
    infinitePagesSupportEnabled,
    pageDesignDisabled,
    showGuides,
    tableWidgets,
}) => {
    const tableWidgetsNames = tableWidgets.map((tableWidget) => tableWidget.id);
    const chartWidgetsNames = chartWidgets.map((chartWidget) => chartWidget.id);
    if (editionLevel === FULL_EDITION_MODE) {
        return (
            <OnePageView
                availableCatalogs={availableCatalogs}
                chartWidgetsNames={chartWidgetsNames}
                froalaEditorConfig={froalaEditorConfig}
                pageDesignDisabled={pageDesignDisabled}
                showGuides={showGuides}
                tableWidgetsNames={tableWidgetsNames}
            />
        );
    }
    return (
        <PageListView
            availableCatalogs={availableCatalogs}
            chartWidgetsNames={chartWidgetsNames}
            editionLevel={editionLevel}
            froalaEditorConfig={froalaEditorConfig}
            horizontalScrollMode={horizontalScrollMode}
            infinitePagesSupportEnabled={infinitePagesSupportEnabled}
            pageDesignDisabled={pageDesignDisabled}
            showGuides={showGuides}
            tableWidgetsNames={tableWidgetsNames}
        />
    );
};

const mapStateToProps = createStructuredSelector({
    chartWidgets: mainSelectors.getDataFetchChartWidgets,
    tableWidgets: mainSelectors.getDataFetchTableWidgets,
});

TemplateContent.propTypes = {
    availableCatalogs: PropTypes.array,
    chartWidgets: PropTypes.array,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    horizontalScrollMode: PropTypes.bool,
    infinitePagesSupportEnabled: PropTypes.bool,
    pageDesignDisabled: PropTypes.bool,
    showGuides: PropTypes.bool,
    tableWidgets: PropTypes.array,
};

export default connect(mapStateToProps, null)(TemplateContent);
