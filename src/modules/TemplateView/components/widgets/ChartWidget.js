import PropTypes from 'prop-types';
import styled from 'styled-components';

import ChartView from './ChartView';

const Container = styled.div`
    width: 100%;
    height: 100%;
    overflow: hidden;
`;

const ChartWidget = ({
    currencyIso,
    currencyLocale,
    editionDisabled,
    innerHeight,
    innerWidth,
    value = {},
} = {}) => (
    <Container overflowYDisabled={editionDisabled}>
        <ChartView
            currencyIso={currencyIso}
            currencyLocale={currencyLocale}
            data={value}
            innerHeight={innerHeight}
            innerWidth={innerWidth}
        />
    </Container>
);

ChartWidget.propTypes = {
    currencyIso: PropTypes.string,
    currencyLocale: PropTypes.string,
    editionDisabled: PropTypes.bool,
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default ChartWidget;
