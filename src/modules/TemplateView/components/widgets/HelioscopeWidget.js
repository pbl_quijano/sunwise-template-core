import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import HelioScopeDefault from '@res/HelioScopeDefault.png';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow-y: auto;
    width: 100%;
`;

const Image = styled.img`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    ${({ isTemplate }) => isTemplate && 'filter: grayscale(0.8);'}
`;

const HelioscopeWidget = ({ value, style = {} } = {}) => (
    <Container>
        <Image
            isTemplate={isEmpty(value)}
            src={isEmpty(value) ? HelioScopeDefault : value}
            style={style}
        />
    </Container>
);

HelioscopeWidget.propTypes = {
    style: PropTypes.object,
    value: PropTypes.object,
};

export default HelioscopeWidget;
