import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { NO_EDITION_MODE, FULL_EDITION_MODE } from '@constants/types';

import defaultMap from '@res/defaultmap.png';

import MapView from './MapView';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow-y: auto;
    position: relative;
    width: 100%;
`;

const Image = styled.img`
    height: 100%;
    left: 0;
    object-fit: cover;
    object-position: center;
    position: absolute;
    top: 0;
    width: 100%;
`;

const StyledMap = styled.div`
    background-color: #c5c5c5;
    bottom: 0;
    height: ${({ heightMap }) => (heightMap ? `${heightMap}px` : '100%')};
    left: 0;
    position: relative;
    right: 0;
    top: 0;
`;

const MapWidget = ({
    editionLevel,
    handleChangeValue,
    innerHeight,
    innerWidth,
    value = {},
} = {}) => {
    const { t } = useTranslation();
    const { projectPosition, currentPosition, mapType = 'roadmap' } = value;
    const [markers, setMarkers] = useState([]);
    const [tempCurrentZoom, setTempCurrentZoom] = useState(
        currentPosition.zoom
    );
    const [tempCurrentMapType, setTempCurrentMapType] = useState(mapType);
    const [tempCurrentCenter, setTempCurrentCenter] = useState({
        lat: currentPosition.lat,
        lng: currentPosition.lng,
    });

    useEffect(() => {
        if (currentPosition.zoom !== tempCurrentZoom) {
            handleChangeValue({
                ...value,
                currentPosition: {
                    ...currentPosition,
                    zoom: tempCurrentZoom,
                },
            });
        }
    }, [tempCurrentZoom]);

    useEffect(() => {
        if (
            currentPosition.lat !== tempCurrentCenter.lat ||
            currentPosition.lng !== tempCurrentCenter.lng
        ) {
            handleChangeValue({
                ...value,
                currentPosition: {
                    ...currentPosition,
                    lat: tempCurrentCenter.lat,
                    lng: tempCurrentCenter.lng,
                },
            });
        }
    }, [tempCurrentCenter]);

    useEffect(() => {
        if (tempCurrentMapType !== mapType) {
            handleChangeValue({
                ...value,
                mapType: tempCurrentMapType,
            });
        }
    }, [tempCurrentMapType]);

    useEffect(() => {
        if (!isEmpty(projectPosition)) {
            setMarkers([
                ...markers,
                {
                    title: t('Project'),
                    lat: projectPosition.lat,
                    lng: projectPosition.lng,
                },
            ]);
        }
    }, []);

    const getMarkerPosition = () =>
        markers.reduce((acc, current) => {
            return (
                acc +
                `&markers=color:red%7Clabel:Proyecto%7C${current.lat},${current.lng}`
            );
        }, '');

    const _handleZoomChanged = (newZoom) => setTempCurrentZoom(newZoom);

    const _handleCenterChanged = (newPosition) =>
        setTempCurrentCenter({
            lat: newPosition.lat(),
            lng: newPosition.lng(),
        });

    const _handleTypeChanged = (newType) => setTempCurrentMapType(newType);

    if (editionLevel === FULL_EDITION_MODE)
        return (
            <Container>
                <Image src={defaultMap} />
            </Container>
        );

    if (editionLevel === NO_EDITION_MODE) {
        return (
            <Container>
                <Image
                    src={`https://maps.googleapis.com/maps/api/staticmap?center=${
                        tempCurrentCenter.lat
                    },${
                        tempCurrentCenter.lng
                    }&zoom=${tempCurrentZoom}&size=${Math.ceil(
                        innerWidth
                    )}x${Math.ceil(
                        innerHeight
                    )}&maptype=${tempCurrentMapType}${getMarkerPosition()}&key=${
                        process.env.REACT_APP_GOOGLE_MAPS_KEY
                    }`}
                />
            </Container>
        );
    }
    return (
        <Container>
            <StyledMap heightMap={innerHeight}>
                <MapView
                    handleCenterChanged={_handleCenterChanged}
                    handleTypeChanged={_handleTypeChanged}
                    handleZoomChanged={_handleZoomChanged}
                    lat={`${tempCurrentCenter.lat}`}
                    lng={`${tempCurrentCenter.lng}`}
                    mapType={tempCurrentMapType}
                    markers={markers}
                    zoom={tempCurrentZoom}
                />
            </StyledMap>
        </Container>
    );
};

MapWidget.propTypes = {
    editionLevel: PropTypes.string,
    handleChangeValue: PropTypes.func,
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    value: PropTypes.object,
};

export default MapWidget;
