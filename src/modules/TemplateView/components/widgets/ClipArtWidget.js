import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
    Circle,
    Ellipse,
    Hexagon,
    Rectangle,
    Square,
    Triangle,
} from './clipArts';

const Container = styled.div`
    display: block;
    height: 100%;
    width: 100%;

    svg {
        display: block;
    }
`;

const ClipArtWidget = ({
    style = {},
    innerHeight = 1,
    innerWidth = 1,
} = {}) => {
    const getClipArt = () => {
        const { type, background, corner, strokeWidth, stroke } = style;
        const basicProps = {
            width: innerWidth,
            heigth: innerHeight,
            corner,
            background,
            strokeWidth,
            stroke,
        };
        switch (type) {
            case 'circle':
                return <Circle {...basicProps} />;
            case 'ellipse':
                return <Ellipse {...basicProps} />;
            case 'square':
                return <Square {...basicProps} />;
            case 'rectangle':
                return <Rectangle {...basicProps} />;
            case 'triangle':
                return <Triangle {...basicProps} />;
            case 'hexagon':
                return <Hexagon {...basicProps} />;
            default:
                return null;
        }
    };
    return <Container>{getClipArt()}</Container>;
};

ClipArtWidget.propTypes = {
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    style: PropTypes.object,
};

export default ClipArtWidget;
