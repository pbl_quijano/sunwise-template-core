import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { TABLE_DATA_COLUMNS, TABLE_FOOTER_COLUMNS } from '@constants/template';
import {
    FULL_EDITION_MODE,
    NO_EDITION_MODE,
    PARTIAL_EDITION_MODE,
} from '@constants/types';

import { cloneElement, numberFormat } from '@helpers/utils';

import TableHeaderView from './table/TableHeaderView';

const Container = styled.div`
    width: 100%;
    height: 100%;
    background-color: #fff;
    ${({ roundedBorders }) => roundedBorders && 'border-radius: 17px 17px 0 0;'}
    border-top-width: 0px !important;
`;

const TableContent = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
`;

const DataContent = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
    overflow-x: hidden;
    overflow-y: ${({ overflowYDisabled, editionLevel }) =>
        overflowYDisabled
            ? 'hidden'
            : editionLevel === PARTIAL_EDITION_MODE
            ? 'overlay'
            : 'auto'};
`;

const BodyContent = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${({ backgroundColor1 = '#fff' }) => backgroundColor1};
`;

const BodyDataRow = styled.div`
    display: flex;
    ${({ isZebraStyle, backgroundColor2, index }) =>
        isZebraStyle &&
        (index + 1) % 2 === 0 &&
        `background-color: ${backgroundColor2};`}
`;

const BodyData = styled.div`
    display: flex;
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: ${({ fontSize = '13px' }) => fontSize};
    line-height: ${({ fontSize = '13px' }) => fontSize};
    color: ${({ color = '#000' }) => color};
    ${({ relativeWidth }) =>
        !isNil(relativeWidth) && `width: ${relativeWidth}%;`}
    padding: 8px 15px;
    overflow: hidden;
    span {
        white-space: pre-line;
        overflow: hidden;
        text-overflow: ellipsis;
    }
`;

const FooterContent = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${({ backgroundColor1 = '#fff' }) => backgroundColor1};
    border-top-style: ${({ borderTopStyle }) => borderTopStyle};
    border-top-width: ${({ borderTopWidth }) => borderTopWidth};
    border-top-color: ${({ borderTopColor }) => borderTopColor};
    ${({ isZebraStyle, backgroundColor2 }) =>
        isZebraStyle &&
        `.footer-row:nth-child(even) {
            background-color: ${backgroundColor2};
        }`}
`;

const FooterDataRow = styled.div`
    display: flex;
`;

const FooterData = styled.div`
    display: flex;
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: ${({ fontSize = '13px' }) => fontSize};
    line-height: ${({ fontSize = '13px' }) => fontSize};
    color: ${({ color = '#000' }) => color};
    ${({ relativeWidth }) =>
        !isNil(relativeWidth) && `width: ${relativeWidth}%;`}
    padding: 8px 15px;
    overflow: hidden;
    span {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: pre-line;
    }
`;

const getSizes = (preparedCols) => {
    const withoutWidthNum = preparedCols.reduce((acc, current) => {
        if (isNil(current.width)) {
            ++acc;
        }
        return acc;
    }, 0);
    if (withoutWidthNum > 0) {
        return preparedCols.map(() => 100 / preparedCols.length);
    }
    return preparedCols.map((col) => col.width);
};

const orderSort = (a, b) => {
    if (a.order > b.order) return 1;
    if (b.order > a.order) return -1;
    return 0;
};

let processAction = () => {};
let processDelay = null;

const TableWidget = ({
    currencyIso,
    currencyLocale,
    editionLevel,
    handleChangeValue,
    infinitePagesSupportEnabled,
    innerWidth,
    isSelected,
    style = {},
    value = {},
}) => {
    const { t } = useTranslation();
    const contentRef = useRef(null);

    const {
        table: tableStyle,
        header: headerStyle,
        body: bodyStyle,
        footer: footerStyle,
    } = style;

    const { data = [], results = {}, cols = {}, scrollTop = 0 } = value;

    const preparedCols = Object.keys(cols)
        .map((colKey) => ({ ...cols[colKey], id: colKey }))
        .sort(orderSort)
        .filter((col) => col.visible);

    const [currentSizes, setCurrentSizes] = useState({
        pendingSave: false,
        data: [],
    });

    useEffect(() => {
        if (
            editionLevel === PARTIAL_EDITION_MODE &&
            !infinitePagesSupportEnabled
        ) {
            if (scrollTop !== contentRef.current.scrollTop) {
                contentRef.current.scrollTop = scrollTop;
            }
            const onScrollTable = (e) => {
                let tempValue = cloneElement(value);
                tempValue.scrollTop = e.target.scrollTop;
                if (processDelay !== null) clearTimeout(processDelay);
                processAction = () => {
                    if (scrollTop !== e.target.scrollTop) {
                        let tempValue = cloneElement(value);
                        tempValue.scrollTop = e.target.scrollTop;
                        handleChangeValue(tempValue);
                    }
                };
                processDelay = setTimeout(() => {
                    processAction();
                    processAction = () => {};
                    processDelay = null;
                }, 100);
            };
            contentRef.current.addEventListener('scroll', onScrollTable, {
                passive: true,
            });
            return () => {
                if (
                    editionLevel === PARTIAL_EDITION_MODE &&
                    contentRef.current
                ) {
                    contentRef.current.removeEventListener(
                        'scroll',
                        onScrollTable
                    );
                }
            };
        }
    }, [contentRef, handleChangeValue]);

    useEffect(() => {
        setCurrentSizes({ pendingSave: false, data: getSizes(preparedCols) });
    }, [preparedCols.length]);

    useEffect(() => {
        if (currentSizes.pendingSave) {
            let tempValue = cloneElement(value);
            preparedCols.forEach((col, index) => {
                tempValue.cols[col.id].width = currentSizes.data[index];
            });
            handleChangeValue(tempValue);
        }
    }, [currentSizes]);

    const getDataTextProps = ({ fontBold, fontItalic, fontSize, color }) => {
        return { fontBold, fontItalic, fontSize, color };
    };

    const _onSaveColumnsWidth = (newSizes) => {
        setCurrentSizes({ pendingSave: true, data: newSizes });
    };

    const getIsVisible = (col, elementsByColumn) =>
        get(elementsByColumn, `${col.id}.visible`, false);

    const renderExtraRow = (resultKey) => {
        const resultElement = results[resultKey];
        if (
            isNil(resultElement) ||
            !resultElement.visible ||
            preparedCols.length <= 0
        )
            return null;
        const elementsByColumn = Object.keys(resultElement.columns).reduce(
            (acc, currentValue) => {
                const columnElement = resultElement.columns[currentValue];
                acc[columnElement.ref] = columnElement;
                return acc;
            },
            {}
        );

        return (
            <FooterDataRow
                className="footer-row widget-table-data-row"
                key={`table-row-${resultKey}`}
            >
                <FooterData
                    relativeWidth={currentSizes.data[0]}
                    {...getDataTextProps(footerStyle)}
                >
                    <span>{resultElement.label}</span>
                </FooterData>
                {[...preparedCols].slice(1).map((col, index) => (
                    <FooterData
                        key={`subtotal-${col.id}`}
                        relativeWidth={currentSizes.data[index + 1]}
                        {...getDataTextProps(footerStyle)}
                    >
                        <span>
                            {getIsVisible(col, elementsByColumn)
                                ? ValueFooterFormatted(col, elementsByColumn)
                                : null}
                        </span>
                    </FooterData>
                ))}
            </FooterDataRow>
        );
    };

    const renderFooter = () => {
        if (isEmpty(results)) return null;
        return (
            <FooterContent
                {...footerStyle}
                className="widget-table-data-footer"
            >
                {Object.keys(results).map((key) => renderExtraRow(key))}
            </FooterContent>
        );
    };

    const getWarnings = () => {
        if (editionLevel === NO_EDITION_MODE) {
            return [];
        }
        if (editionLevel === PARTIAL_EDITION_MODE) {
            const { warnings = [] } = value;
            return warnings;
        }
        return [t('The data shown is for example only')];
    };

    const ValueFormatted = (col, item) => {
        if (
            editionLevel === FULL_EDITION_MODE &&
            TABLE_DATA_COLUMNS.includes(col.id)
        ) {
            return numberFormat(item[col.id], {
                currency: currencyIso,
                style: 'currency',
                locale: currencyLocale,
            });
        }
        return item[col.id];
    };

    const ValueFooterFormatted = (col, elementsByColumn) => {
        const value = get(elementsByColumn, `${col.id}.value`, 0);
        if (
            editionLevel === FULL_EDITION_MODE &&
            TABLE_FOOTER_COLUMNS.includes(col.id)
        ) {
            return numberFormat(value, {
                currency: currencyIso,
                style: 'currency',
                locale: currencyLocale,
            });
        }
        return value;
    };

    return (
        <Container
            className="widget-table-container-resize"
            roundedBorders={headerStyle.roundedBorders}
            style={tableStyle}
        >
            <TableContent>
                <TableHeaderView
                    headerStyle={headerStyle}
                    innerWidth={innerWidth}
                    isWidgetSelected={isSelected}
                    onSaveColumnsWidth={_onSaveColumnsWidth}
                    preparedCols={preparedCols}
                    sizes={currentSizes.data}
                    textStyle={{ ...getDataTextProps(headerStyle) }}
                    warnings={getWarnings()}
                />
                <DataContent
                    className="puppeteer-scrolling widget-table-data-container"
                    data-scrolltop={scrollTop}
                    overflowYDisabled={
                        editionLevel === NO_EDITION_MODE ||
                        !isSelected ||
                        infinitePagesSupportEnabled
                    }
                    ref={contentRef}
                    editionLevel={editionLevel}
                >
                    <BodyContent
                        backgroundColor1={bodyStyle.backgroundColor1}
                        className="widget-table-data-body"
                    >
                        {data.map((item, index) => (
                            <BodyDataRow
                                backgroundColor2={bodyStyle.backgroundColor2}
                                className="widget-table-data-row"
                                key={index}
                                index={index}
                                isZebraStyle={bodyStyle.isZebraStyle}
                            >
                                {preparedCols.map((col, index) => (
                                    <BodyData
                                        key={`${index}-${col.id}`}
                                        relativeWidth={currentSizes.data[index]}
                                        {...getDataTextProps(bodyStyle)}
                                    >
                                        <span>{ValueFormatted(col, item)}</span>
                                    </BodyData>
                                ))}
                            </BodyDataRow>
                        ))}
                    </BodyContent>
                    {renderFooter()}
                </DataContent>
            </TableContent>
        </Container>
    );
};

TableWidget.propTypes = {
    currencyIso: PropTypes.string,
    currencyLocale: PropTypes.string,
    defaultCurrency: PropTypes.object,
    editionLevel: PropTypes.string,
    handleChangeValue: PropTypes.func,
    infinitePagesSupportEnabled: PropTypes.bool,
    innerWidth: PropTypes.string,
    isSelected: PropTypes.bool,
    keyName: PropTypes.string,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default TableWidget;
