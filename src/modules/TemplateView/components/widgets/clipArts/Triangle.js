import PropTypes from 'prop-types';

const Triangle = ({
    background = '#000',
    heigth,
    stroke = '#000',
    strokeWidth = 0,
    width,
} = {}) => {
    return (
        <svg
            width={width}
            height={heigth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
        >
            <polygon
                points={`${width / 2} ${strokeWidth} ${strokeWidth} ${
                    heigth - strokeWidth / 2
                } ${width - strokeWidth} ${heigth - strokeWidth / 2}`}
                stroke={stroke}
                fill={background}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};

Triangle.propTypes = {
    background: PropTypes.string,
    heigth: PropTypes.number,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
};

export default Triangle;
