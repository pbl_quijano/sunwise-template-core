import PropTypes from 'prop-types';

const Square = ({
    background = '#000',
    corner = 0,
    heigth,
    stroke = '#000',
    strokeWidth = 0,
    width,
} = {}) => {
    const l = (heigth < width ? heigth : width) - strokeWidth;
    const x = heigth < width ? (width - l) / 2 : strokeWidth / 2;
    const y = heigth < width ? strokeWidth / 2 : (heigth - l) / 2;
    const realCorner =
        heigth < width ? heigth * (corner / 100) : width * (corner / 100);

    return (
        <svg
            width={width}
            height={heigth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect
                x={x}
                y={y}
                rx={realCorner}
                ry={realCorner}
                width={l}
                height={l}
                stroke={stroke}
                fill={background}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};

Square.propTypes = {
    background: PropTypes.string,
    corner: PropTypes.number,
    heigth: PropTypes.number,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
};

export default Square;
