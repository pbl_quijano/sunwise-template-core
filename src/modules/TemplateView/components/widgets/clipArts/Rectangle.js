import PropTypes from 'prop-types';

const Rectangle = ({
    background = '#000',
    corner = 0,
    heigth,
    stroke = '#000',
    strokeWidth = 0,
    width,
} = {}) => {
    const realCorner =
        heigth < width ? heigth * (corner / 100) : width * (corner / 100);

    return (
        <svg
            width={width}
            height={heigth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect
                x={strokeWidth / 2}
                y={strokeWidth / 2}
                rx={realCorner}
                ry={realCorner}
                width={width - strokeWidth}
                height={heigth - strokeWidth}
                stroke={stroke}
                fill={background}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};

Rectangle.propTypes = {
    background: PropTypes.string,
    corner: PropTypes.number,
    heigth: PropTypes.number,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
};

export default Rectangle;
