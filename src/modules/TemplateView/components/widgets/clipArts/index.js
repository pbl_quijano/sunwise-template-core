export { default as Circle } from './Circle';
export { default as Ellipse } from './Ellipse';
export { default as Hexagon } from './Hexagon';
export { default as Rectangle } from './Rectangle';
export { default as Square } from './Square';
export { default as Triangle } from './Triangle';
