import PropTypes from 'prop-types';

const Circle = ({
    background = '#000',
    heigth,
    stroke = '#000',
    strokeWidth = 0,
    width,
} = {}) => {
    const cx = width / 2;
    const cy = heigth / 2;
    const radius = (heigth < width ? cy : cx) - strokeWidth / 2;

    return (
        <svg
            width={width}
            height={heigth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
        >
            <circle
                cx={cx}
                cy={cy}
                r={radius}
                stroke={stroke}
                fill={background}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};

Circle.propTypes = {
    background: PropTypes.string,
    heigth: PropTypes.number,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
};

export default Circle;
