import PropTypes from 'prop-types';

const Hexagon = ({
    background = '#000',
    heigth,
    stroke = '#000',
    strokeWidth = 0,
    width,
} = {}) => {
    const fourWidth = width / 4;
    return (
        <svg
            width={width}
            height={heigth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
        >
            <polygon
                points={`${fourWidth} ${strokeWidth / 2} ${fourWidth * 3} ${
                    strokeWidth / 2
                } ${width - strokeWidth / 2} ${heigth / 2} ${fourWidth * 3} ${
                    heigth - strokeWidth / 2
                } ${fourWidth} ${heigth - strokeWidth / 2} ${strokeWidth / 2} ${
                    heigth / 2
                }`}
                stroke={stroke}
                fill={background}
                strokeWidth={strokeWidth}
            />
        </svg>
    );
};

Hexagon.propTypes = {
    background: PropTypes.string,
    heigth: PropTypes.number,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
};

export default Hexagon;
