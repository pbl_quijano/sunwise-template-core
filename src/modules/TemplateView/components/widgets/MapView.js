import { Map } from 'google-maps-react';
import i18next from 'i18next';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';

import { GeneralContext } from '@helpers/contexts';

const mapValueUseEffect =
    ({
        google,
        handleCenterChanged,
        handleTypeChanged,
        handleZoomChanged,
        mapValue,
        markers,
    }) =>
    () => {
        if (mapValue) {
            mapValue.setTilt(0);
            if (handleCenterChanged) {
                google.maps.event.addListener(mapValue, 'dragend', () =>
                    handleCenterChanged(mapValue.getCenter())
                );
            }
            if (handleZoomChanged) {
                google.maps.event.addListener(mapValue, 'zoom_changed', () =>
                    handleZoomChanged(mapValue.getZoom())
                );
            }
            if (handleTypeChanged) {
                google.maps.event.addListener(
                    mapValue,
                    'maptypeid_changed',
                    () => handleZoomChanged(mapValue.getMapTypeId())
                );
            }

            markers.forEach((marker) => {
                const markerObject = new google.maps.Marker({
                    draggable: false,
                    icon: {
                        url: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png',
                        scaledSize: new google.maps.Size(32, 32),
                    },
                    name: i18next.t('Location'),
                    position: new google.maps.LatLng(marker.lat, marker.lng),
                    title: marker.title,
                });
                markerObject.setMap(mapValue);
            });
        }

        return () => {
            if (mapValue) {
                google.maps.event.clearListeners(mapValue, 'dragend');
                google.maps.event.clearListeners(mapValue, 'zoom_changed');
                google.maps.event.clearListeners(mapValue, 'maptypeid_changed');
            }
        };
    };

const MapView = ({
    handleCenterChanged,
    handleTypeChanged,
    handleZoomChanged,
    lat,
    lng,
    mapType,
    markers,
    zoom,
}) => {
    const { google } = useContext(GeneralContext);
    const [mapValue, setMapValue] = useState(null);
    useEffect(
        mapValueUseEffect({
            google,
            handleCenterChanged,
            handleTypeChanged,
            handleZoomChanged,
            mapValue,
            markers,
        }),
        [mapValue]
    );

    return (
        <Map
            fullscreenControl={false}
            google={google}
            initialCenter={{ lat, lng }}
            mapType={mapType}
            mapTypeControl={true}
            onReady={(_, map) => setMapValue(map)}
            rotateControl={false}
            streetViewControl={false}
            zoom={zoom}
            zoomControl={true}
        />
    );
};

MapView.propTypes = {
    handleCenterChanged: PropTypes.func,
    handleTypeChanged: PropTypes.func,
    handleZoomChanged: PropTypes.func,
    lat: PropTypes.string,
    lng: PropTypes.string,
    mapType: PropTypes.string,
    markers: PropTypes.array,
    zoom: PropTypes.number,
};

export default MapView;
