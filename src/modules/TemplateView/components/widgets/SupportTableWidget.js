import get from 'lodash/get';
import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

const Container = styled.div`
    width: 100%;
    height: 100%;
    background-color: #fff;
    ${({ roundedBorders }) => roundedBorders && 'border-radius: 17px 17px 0 0;'}
    border-top-width: 0px !important;
`;

const TableContent = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
`;

const HeaderContent = styled.div`
    display: flex;
    background-color: ${({ backgroundColor = '#000' }) => backgroundColor};
    ${({ roundedBorders }) => roundedBorders && 'border-radius: 17px;'}
`;

const HeaderData = styled.div`
    display: flex;
    flex: 1;
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: 13px;
    color: ${({ color = '#fff' }) => color};
    padding: 5px 15px;
`;

const DataContent = styled.div`
    display: flex;
    flex-direction: column;
    flex: 1;
    overflow-x: hidden;
    overflow-y: ${({ overflowYDisabled }) =>
        overflowYDisabled ? 'hidden' : 'auto'};
`;
const BodyContent = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${({ backgroundColor1 = '#fff' }) => backgroundColor1};
`;

const BodyDataRow = styled.div`
    display: flex;
    ${({ isZebraStyle, backgroundColor2, index }) =>
        isZebraStyle &&
        (index + 1) % 2 === 0 &&
        `background-color: ${backgroundColor2};`}
`;

const BodyData = styled.div`
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: 13px;
    color: ${({ color = '#000' }) => color};
    flex: 1;
    padding: 5px 15px;
`;

const FooterContent = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${({ backgroundColor1 = '#fff' }) => backgroundColor1};
    border-top-style: ${({ borderTopStyle }) => borderTopStyle};
    border-top-width: ${({ borderTopWidth }) => borderTopWidth};
    border-top-color: ${({ borderTopColor }) => borderTopColor};
    ${({ isZebraStyle, backgroundColor2 }) =>
        isZebraStyle &&
        `.footer-row:nth-child(even) {
            background-color: ${backgroundColor2};
        }`}
`;

const FooterDataRow = styled.div`
    display: flex;
`;

const FooterData = styled.div`
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: 13px;
    color: ${({ color = '#000' }) => color};
    flex: 1;
    padding: 5px 15px;
`;

const SupportTableWidget = ({ editionDisabled, style = {}, value = {} }) => {
    const { t } = useTranslation();
    const {
        cols = [],
        table: tableStyle,
        header: headerStyle,
        body: bodyStyle,
        footer: footerStyle,
        showTotal,
        showIVA,
        showDiscount,
    } = style;
    const { data = [], results = {} } = value;

    const showDiscountsRow = () => {
        if (
            !showTotal ||
            !showDiscount ||
            cols
                .slice(1)
                .filter(
                    (col) => !isNil(get(results, `discount.${col.name}`, null))
                ).length <= 0
        ) {
            return false;
        }
        return false;
    };

    const showIVARow = () => {
        if (
            !showTotal ||
            !showIVA ||
            cols
                .slice(1)
                .filter((col) => !isNil(get(results, `iva.${col.name}`, null)))
                .length <= 0
        ) {
            return false;
        }
        return true;
    };

    const getDataTextProps = ({ fontBold, fontItalic, color }) => ({
        fontBold,
        fontItalic,
        color,
    });

    return (
        <Container
            roundedBorders={headerStyle.roundedBorders}
            style={tableStyle}
        >
            <TableContent>
                <HeaderContent
                    backgroundColor={headerStyle.backgroundColor}
                    roundedBorders={headerStyle.roundedBorders}
                >
                    {cols.map((col) => (
                        <HeaderData
                            key={col.name}
                            {...getDataTextProps(headerStyle)}
                        >
                            <span>{col.label}</span>
                        </HeaderData>
                    ))}
                </HeaderContent>
                <DataContent overflowYDisabled={editionDisabled}>
                    <BodyContent backgroundColor1={bodyStyle.backgroundColor1}>
                        {data.map((item, index) => (
                            <BodyDataRow
                                backgroundColor2={bodyStyle.backgroundColor2}
                                key={index}
                                index={index}
                                isZebraStyle={bodyStyle.isZebraStyle}
                            >
                                {cols.map((col) => (
                                    <BodyData
                                        key={`${index}-${col.name}`}
                                        {...getDataTextProps(bodyStyle)}
                                    >
                                        <span>{item[col.name]}</span>
                                    </BodyData>
                                ))}
                            </BodyDataRow>
                        ))}
                    </BodyContent>

                    {showTotal && (
                        <FooterContent {...footerStyle}>
                            {(showIVARow() || showDiscountsRow()) && showTotal && (
                                <FooterDataRow className="footer-row">
                                    <FooterData
                                        {...getDataTextProps(footerStyle)}
                                    >
                                        {t('Subtotal')}
                                    </FooterData>
                                    {cols.slice(1).map((col) => (
                                        <FooterData
                                            key={`subtotal-${col.name}`}
                                            {...getDataTextProps(footerStyle)}
                                        >
                                            {get(
                                                results,
                                                `subtotal.${col.name}`,
                                                ''
                                            )}
                                        </FooterData>
                                    ))}
                                </FooterDataRow>
                            )}

                            {showIVARow() && (
                                <FooterDataRow className="footer-row">
                                    <FooterData
                                        {...getDataTextProps(footerStyle)}
                                    >
                                        {t('Taxes')}
                                    </FooterData>
                                    {cols.slice(1).map((col) => (
                                        <FooterData
                                            key={`iva-${col.name}`}
                                            {...getDataTextProps(footerStyle)}
                                        >
                                            {get(
                                                results,
                                                `iva.${col.name}`,
                                                ''
                                            )}
                                        </FooterData>
                                    ))}
                                </FooterDataRow>
                            )}

                            {showDiscountsRow() && (
                                <FooterDataRow className="footer-row">
                                    <FooterData
                                        {...getDataTextProps(footerStyle)}
                                    >
                                        {t('Discount')}
                                    </FooterData>
                                    {cols.slice(1).map((col) => (
                                        <FooterData
                                            key={`discount-${col.name}`}
                                            {...getDataTextProps(footerStyle)}
                                        >
                                            {get(
                                                results,
                                                `discount.${col.name}`,
                                                ''
                                            )}
                                        </FooterData>
                                    ))}
                                </FooterDataRow>
                            )}

                            <FooterDataRow className="footer-row">
                                <FooterData {...getDataTextProps(footerStyle)}>
                                    {t('Total')}
                                </FooterData>
                                {cols.slice(1).map((col) => (
                                    <FooterData
                                        key={`total-${col.name}`}
                                        {...getDataTextProps(footerStyle)}
                                    >
                                        {get(results, `total.${col.name}`, '')}
                                    </FooterData>
                                ))}
                            </FooterDataRow>
                        </FooterContent>
                    )}
                </DataContent>
            </TableContent>
        </Container>
    );
};

SupportTableWidget.propTypes = {
    editionDisabled: PropTypes.bool,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default SupportTableWidget;
