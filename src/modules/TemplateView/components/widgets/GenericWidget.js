import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

const StyledGridItem = styled.div`
    height: 100%;
    padding: 4px;
    width: 100%;
`;

const GenericWidget = () => {
    const { t } = useTranslation();
    return <StyledGridItem>{t('Widget')}</StyledGridItem>;
};

export default GenericWidget;
