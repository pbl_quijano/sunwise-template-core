import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import {
    EXAMPLE_SEGMENT,
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SOLAR_MODULE_COLOR,
    SOLAR_MODULE_STROKE_COLOR,
} from '@constants/maps';
import { NO_EDITION_MODE, FULL_EDITION_MODE } from '@constants/types';

import SegmentsMap from './SegmentsMap';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow-y: auto;
    position: relative;
    width: 100%;
`;

const ExampleAlert = styled.span`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    font-size: 11px;
    padding: 4px 6px;
    background-color: #d32f2f;
    color: #fff;
    z-index: 1;
`;

const StyledMap = styled.div`
    background-color: #c5c5c5;
    bottom: 0;
    height: ${({ heightMap }) => (heightMap ? `${heightMap}px` : '100%')};
    left: 0;
    position: relative;
    right: 0;
    top: 0;
`;

const SegmentsMapWidget = ({
    editionLevel,
    handleChangeValue,
    innerHeight,
    style,
    value = {},
} = {}) => {
    const { t } = useTranslation();
    const { parent_field_segments, currentPosition } = value;
    const [markers, setMarkers] = useState([]);
    const [tempCurrentZoom, setTempCurrentZoom] = useState(
        currentPosition.zoom
    );
    const [tempCurrentCenter, setTempCurrentCenter] = useState({
        lat: currentPosition.lat,
        lng: currentPosition.lng,
    });
    const {
        segment = {},
        showLabels = true,
        showProjectMarker = true,
        solarModule = {},
    } = style;
    const {
        fillColor: segmentFillColor = SEGMENT_COLOR,
        strokeColor: segmentStrokeColor = SEGMENT_STROKE_COLOR,
    } = segment;
    const {
        fillColor: solarModuleFillColor = SOLAR_MODULE_COLOR,
        strokeColor: solarModuleStrokeColor = SOLAR_MODULE_STROKE_COLOR,
    } = solarModule;

    useEffect(() => {
        if (!isEmpty(parent_field_segments)) {
            setMarkers([
                ...markers,
                {
                    title: t('Project'),
                    lat: parent_field_segments.latitude,
                    lng: parent_field_segments.longitude,
                },
            ]);
        }
    }, []);

    useEffect(() => {
        if (currentPosition.zoom !== tempCurrentZoom) {
            handleChangeValue({
                ...value,
                currentPosition: {
                    ...currentPosition,
                    zoom: tempCurrentZoom,
                },
            });
        }
    }, [tempCurrentZoom]);

    useEffect(() => {
        if (
            currentPosition.lat !== tempCurrentCenter.lat ||
            currentPosition.lng !== tempCurrentCenter.lng
        ) {
            handleChangeValue({
                ...value,
                currentPosition: {
                    ...currentPosition,
                    lat: tempCurrentCenter.lat,
                    lng: tempCurrentCenter.lng,
                },
            });
        }
    }, [tempCurrentCenter]);

    const _handleZoomChanged = (newZoom) => setTempCurrentZoom(newZoom);

    const _handleCenterChanged = (newPosition) =>
        setTempCurrentCenter({
            lat: newPosition.lat(),
            lng: newPosition.lng(),
        });

    if (
        editionLevel === FULL_EDITION_MODE ||
        !(parent_field_segments && parent_field_segments.field_segments)
    ) {
        return (
            <Container>
                <ExampleAlert>
                    {t(
                        'The segment and panels shown in this map are for reference'
                    )}
                </ExampleAlert>
                <StyledMap heightMap={innerHeight}>
                    <SegmentsMap
                        clickable={false}
                        height="100%"
                        initCenter={{
                            latitude: 20.9891608,
                            longitude: -89.6114954,
                        }}
                        projectLocation={{
                            latitude: 20.9891477,
                            longitude: -89.6115384,
                        }}
                        scaleControl={false}
                        segmentFillColor={segmentFillColor}
                        segments={[EXAMPLE_SEGMENT]}
                        segmentStrokeColor={segmentStrokeColor}
                        showLabels={showLabels}
                        showProjectMarker={showProjectMarker}
                        solarModuleFillColor={solarModuleFillColor}
                        solarModuleStrokeColor={solarModuleStrokeColor}
                        width="100%"
                        zoom={20}
                        zoomControl={false}
                    />
                </StyledMap>
            </Container>
        );
    }

    const segments = parent_field_segments.field_segments.map((segment) => {
        const solar_modules = segment.solar_modules.map((solarModule) => {
            return solarModule.solar_module_points.map((point) => [
                point.y_coordinate,
                point.x_coordinate,
            ]);
        });
        const polygon = segment.field_segment_points.map((point) => [
            point.y_coordinate,
            point.x_coordinate,
        ]);
        return {
            id: segment.id,
            name: segment.name,
            polygon,
            solar_modules,
        };
    });

    return (
        <Container>
            <StyledMap heightMap={innerHeight}>
                <SegmentsMap
                    clickable={editionLevel !== NO_EDITION_MODE}
                    handleCenterChanged={_handleCenterChanged}
                    handleZoomChanged={_handleZoomChanged}
                    height="100%"
                    initCenter={{
                        latitude: tempCurrentCenter.lat,
                        longitude: tempCurrentCenter.lng,
                    }}
                    projectLocation={{
                        latitude: parent_field_segments.latitude,
                        longitude: parent_field_segments.longitude,
                    }}
                    scaleControl={editionLevel !== NO_EDITION_MODE}
                    segmentFillColor={segmentFillColor}
                    segments={segments}
                    segmentStrokeColor={segmentStrokeColor}
                    showLabels={showLabels}
                    showProjectMarker={showProjectMarker}
                    solarModuleFillColor={solarModuleFillColor}
                    solarModuleStrokeColor={solarModuleStrokeColor}
                    width="100%"
                    zoom={tempCurrentZoom}
                    zoomControl={editionLevel !== NO_EDITION_MODE}
                />
            </StyledMap>
        </Container>
    );
};

SegmentsMapWidget.propTypes = {
    editionLevel: PropTypes.string,
    handleChangeValue: PropTypes.func,
    innerHeight: PropTypes.number,
    style: PropTypes.object,
    value: PropTypes.object,
};

export default SegmentsMapWidget;
