import PropTypes from 'prop-types';
import styled from 'styled-components';

import ChartView from './ChartView';

const Container = styled.div`
    height: 100%;
    overflow-x: hidden;
    overflow-y: ${({ overflowYDisabled }) =>
        overflowYDisabled ? 'hidden' : 'auto'};
    width: 100%;
`;

const SupportChartWidget = ({
    currencyIso,
    currencyLocale,
    editionDisabled,
    innerHeight,
    innerWidth,
    value = {},
} = {}) => (
    <Container overflowYDisabled={editionDisabled}>
        <ChartView
            currencyIso={currencyIso}
            currencyLocale={currencyLocale}
            data={value}
            innerHeight={innerHeight}
            innerWidth={innerWidth}
        />
    </Container>
);

SupportChartWidget.propTypes = {
    currencyIso: PropTypes.string,
    currencyLocale: PropTypes.string,
    editionDisabled: PropTypes.bool,
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    value: PropTypes.object,
};

export default SupportChartWidget;
