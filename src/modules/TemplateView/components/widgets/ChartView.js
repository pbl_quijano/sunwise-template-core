import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import Chart from 'react-apexcharts';
import styled from 'styled-components';

import { FORMAT_Y_TYPES } from '@constants/template';

import { buildGraphsColors } from '@helpers/template';
import { numberFormat } from '@helpers/utils';

const Styled = styled.div`
    &.annotation-label {
        background-color: red;
        margin-bottom: 5px;
    }

    .appexchart-content,
    .apexcharts-canvas,
    .apexcharts-canvas svg {
        height: ${({ innerHeight }) => `${innerHeight}px!important`};
        width: ${({ innerWidth }) => `${innerWidth}px!important`};
    }
`;

const getColors = (type, stacked_bar, series) => {
    if (type !== 'bar' || !stacked_bar || !stacked_bar.visible) {
        return series.map((serie) => serie.color || '#ef0d0d');
    }

    return buildGraphsColors({ type, stacked_bar });
};

const ChartView = ({
    currencyIso,
    currencyLocale,
    data = {},
    innerHeight,
    innerWidth,
}) => {
    const {
        annotations: annotationsData,
        categories,
        finishCategoryIndex = data.categories.length - 1,
        formatY = {},
        initCategoryIndex = 0,
        series: seriesData,
        stacked_bar,
        type,
    } = data;
    const [chartLoaded, setChartLoaded] = useState(true);

    useEffect(() => {
        if (!chartLoaded) setChartLoaded(true);
    }, [chartLoaded]);

    useEffect(() => {
        setChartLoaded(false);
    }, [type, stacked_bar, seriesData]);

    if (!chartLoaded) return null;

    const series = seriesData.filter((serie) => serie.visible !== false);
    const colors = getColors(type, stacked_bar, series);

    const getFormattingY = (value) => {
        const { CURRENCY, DECIMAL_2, DECIMAL_4 } = FORMAT_Y_TYPES;

        switch (formatY.type) {
            case CURRENCY:
                return numberFormat(value, {
                    currency: currencyIso,
                    locale: currencyLocale,
                    style: 'currency',
                });
            case DECIMAL_2:
                return numberFormat(value, {
                    locale: currencyLocale,
                    style: 'decimal',
                    unit: formatY.unit,
                });
            case DECIMAL_4:
                return numberFormat(value, {
                    decimals: 4,
                    locale: currencyLocale,
                    style: 'decimal',
                    unit: formatY.unit,
                });
            default:
                return numberFormat(value, {
                    decimals: 0,
                    locale: currencyLocale,
                    style: 'decimal',
                    unit: formatY.unit,
                });
        }
    };

    const getSeries = () => {
        if (type !== 'bar' || !stacked_bar || !stacked_bar.visible) {
            const seriesType = series.reduce((acc, current) => {
                acc[current.type] = 0;
                return acc;
            }, {});

            return series.map((serie) => ({
                ...serie,
                name: serie.editable_label || serie.name,
                type:
                    Object.keys(seriesType).length > 1 ? serie.type : undefined,
                data: serie.data.filter(
                    (c, index) =>
                        index >= initCategoryIndex &&
                        index <= finishCategoryIndex
                ),
            }));
        }

        const { data, selected } = stacked_bar;
        const dataRange = data.filter(
            (c, index) =>
                index >= initCategoryIndex && index <= finishCategoryIndex
        );
        const arrayDefault = new Array(dataRange.length).fill(0);
        const dataDictionary = dataRange.reduce((acc, current, index) => {
            let tempAcc = { ...acc };
            const parseItem = JSON.parse(current[selected]);
            if (parseItem) {
                Object.keys(parseItem).forEach((key) => {
                    if (isNil(tempAcc[key])) {
                        tempAcc[key] = [...arrayDefault];
                    }
                    tempAcc[key][index] = parseItem[key];
                });
            }
            return { ...acc, ...tempAcc };
        }, {});

        return Object.keys(dataDictionary).map((key) => ({
            name: key,
            data: dataDictionary[key],
        }));
    };

    const annotations = annotationsData
        ? annotationsData
              .filter((annotation) => annotation.visible !== false)
              .map((annotation) => ({
                  y: annotation.value,
                  borderColor: annotation.color,
                  strokeDashArray: 0,
                  label: {
                      borderColor: annotation.color,
                      style: {
                          cssClass: 'annotation-label',
                          color: '#FFF',
                          background: annotation.color,
                          padding: {
                              top: 5,
                              left: 5,
                              bottom: 5,
                              right: 5,
                          },
                      },
                      text: `${
                          annotation.editable_label || annotation.label
                      } ${numberFormat(annotation.value, {
                          currency: currencyIso,
                          locale: currencyLocale,
                          style: 'currency',
                      })}`,
                  },
              }))
        : [];

    const contentHeight = innerHeight - 20;

    const chartConfig = {
        options: {
            annotations: {
                yaxis: annotations,
            },
            colors,
            dataLabels: {
                enabled: false,
            },
            legend: {
                show: true,
            },
            xaxis: {
                categories: categories.filter(
                    (c, index) =>
                        index >= initCategoryIndex &&
                        index <= finishCategoryIndex
                ),
            },
            yaxis: {
                labels: {
                    formatter: getFormattingY,
                },
            },
            chart: {
                height: contentHeight,
                id: 'apexchart',
                stacked: stacked_bar && stacked_bar.visible && type === 'bar',
                toolbar: {
                    show: false,
                },
                type,
                width: innerWidth,
            },
            fill: {
                colors,
            },
            stroke: {
                colors,
            },
        },
        series: getSeries(),
    };

    return (
        <Styled innerHeight={contentHeight} innerWidth={innerWidth}>
            <Chart
                className="appexchart-content"
                height={`${contentHeight}`}
                width={`${innerWidth}`}
                options={chartConfig.options}
                series={chartConfig.series || []}
                type={type || 'bar'}
            />
        </Styled>
    );
};

ChartView.propTypes = {
    currencyIso: PropTypes.string,
    currencyLocale: PropTypes.string,
    data: PropTypes.object,
    defaultCurrency: PropTypes.object,
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    isCountryCurrencyRequired: PropTypes.bool,
};

export default ChartView;
