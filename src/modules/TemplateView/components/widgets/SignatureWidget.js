import PropTypes from 'prop-types';
import { useRef, useEffect } from 'react';
import SignaturePad from 'signature_pad';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    width: ${({ innerWidth = 1 }) => innerWidth}px;
    height: ${({ innerHeight = 1 }) => innerHeight}px;
    overflow-y: auto;
`;

const Canvas = styled.canvas`
    display: flex;
    flex-direction: column;
    height: 100%;
    width: 100%;
`;

const Line = styled.div`
    position: absolute;
    height: 2px;
    width: 100%;
    bottom: 0;
    background: #000;
    left: 0;
    pointer-events: none;
`;

const SignatureWidget = ({ value, innerHeight, innerWidth } = {}) => {
    const canvasRef = useRef(null);
    const signaturePad = useRef(null);

    const resizeCanvas = () => {
        const canvas = canvasRef.current;
        const ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext('2d').scale(ratio, ratio);
    };

    useEffect(() => {
        signaturePad.current = new SignaturePad(canvasRef.current);
        signaturePad.current.off();
    }, []);

    useEffect(() => {
        if (signaturePad.current && value) {
            signaturePad.current.fromData(JSON.parse(value));
        }
    }, [value, signaturePad.current]);

    useEffect(() => {
        if (signaturePad.current && innerHeight && innerWidth) {
            resizeCanvas();
        }
    }, [innerHeight, innerWidth, signaturePad.current]);

    return (
        <Container innerHeight={innerHeight} innerWidth={innerWidth}>
            <Canvas ref={canvasRef} />
            <Line />
        </Container>
    );
};

SignatureWidget.propTypes = {
    innerHeight: PropTypes.number,
    innerWidth: PropTypes.number,
    value: PropTypes.object,
};

export default SignatureWidget;
