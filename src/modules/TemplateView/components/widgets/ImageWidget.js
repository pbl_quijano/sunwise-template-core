import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import DefaultImage from '@res/defaultImage.png';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    overflow-y: auto;
`;

const Image = styled.img`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
`;

const ImageWidget = ({ value, style = {} } = {}) => {
    return (
        <Container>
            <Image style={style} src={isEmpty(value) ? DefaultImage : value} />
        </Container>
    );
};

ImageWidget.propTypes = {
    style: PropTypes.object,
    value: PropTypes.object,
};

export default ImageWidget;
