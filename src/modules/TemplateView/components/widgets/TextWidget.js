import PropTypes from 'prop-types';
import FroalaEditorComponent from 'react-froala-wysiwyg';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100%;
    overflow-y: hidden;
    width: 100%;
    .fr-box {
        height: 100%;
    }

    .fr-wrapper {
        height: 100%;
        background: transparent !important;
        border-width: 0 !important;

        .fr-element {
            height: 100%;
            min-height: 0 !important;
            padding: 0 !important;

            &.fr-view {
                color: #000;
                font-family: inherit;
                font-size: 14px;
                line-height: normal;
                overflow-y: ${({ overflowYDisabled }) =>
                    overflowYDisabled ? 'hidden' : 'auto'};
            }

            .fr-command {
                cursor: pointer;
                color: inherit;
                padding: 0;
                background: ${({ overflowYDisabled, editionDisabled }) =>
                    overflowYDisabled || editionDisabled
                        ? 'transparent !important'
                        : '#EEEEEE'};
            }
        }
    }

    .second-toolbar {
        display: none;
    }
`;

const FroalaContainer = styled.div`
    height: 100%;
    position: relative;
    p {
        margin-bottom: 0;
    }
`;

const TextWidget = ({
    editionDisabled,
    froalaEditorConfig,
    handleChangeValue,
    isSelected,
    value = '',
}) => {
    return (
        <Container
            overflowYDisabled={!isSelected}
            editionDisabled={editionDisabled}
        >
            <FroalaContainer>
                <FroalaEditorComponent
                    config={froalaEditorConfig}
                    model={value}
                    onModelChange={handleChangeValue}
                    tag="textarea"
                />
            </FroalaContainer>
        </Container>
    );
};

TextWidget.propTypes = {
    editionDisabled: PropTypes.bool,
    froalaEditorConfig: PropTypes.object,
    handleChangeValue: PropTypes.func,
    isSelected: PropTypes.bool,
    keyName: PropTypes.string,
    value: PropTypes.string,
};

export default TextWidget;
