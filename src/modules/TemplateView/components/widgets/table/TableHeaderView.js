import PropTypes from 'prop-types';
import styled from 'styled-components';

import SplitWrapper from './SplitWrapper';

const Container = styled.div`
    display: block;
    background-color: ${({ backgroundColor = '#000' }) => backgroundColor};
    ${({ roundedBorders }) => roundedBorders && 'border-radius: 17px;'}
    min-height: 29px;
    position: relative;
`;

const DataContent = styled.div`
    display: flex;
    font-weight: ${({ fontBold }) => (fontBold ? 'bold' : '100')};
    ${({ fontItalic }) => fontItalic && 'font-style: italic;'}
    font-size: ${({ fontSize = '13px' }) => fontSize};
    line-height: ${({ fontSize = '13px' }) => fontSize};
    color: ${({ color = '#fff' }) => color};
    padding: 8px 15px;
    align-items: center;
    overflow: hidden;
    span {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: pre-line;
    }
`;

const WarningsContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 4px 6px 0 6px;
    background-color: #d32f2f;
    span {
        min-height: 13px;
        color: #fff;
        font-size: 11px;
        font-weight: bold;
        line-height: 13px;
        margin-bottom: 4px;
    }
`;

const TableHeaderView = ({
    headerStyle,
    isWidgetSelected,
    onSaveColumnsWidth,
    preparedCols,
    sizes,
    textStyle,
    warnings,
} = {}) => {
    return (
        <Container
            backgroundColor={headerStyle.backgroundColor}
            roundedBorders={headerStyle.roundedBorders}
        >
            <SplitWrapper
                isWidgetSelected={isWidgetSelected}
                sizes={sizes}
                direction="horizontal"
                gutterSize="4"
                minSize="30"
                cursor="col-resize"
                orderCols={preparedCols.reduce(
                    (acc, current) => acc + current.label,
                    ''
                )}
                onDragEnd={onSaveColumnsWidth}
            >
                {preparedCols.map((col, index) => (
                    <DataContent key={`header-${index}`} {...textStyle}>
                        <span>{col.editable_label || col.label}</span>
                    </DataContent>
                ))}
            </SplitWrapper>
            {warnings.length > 0 && (
                <WarningsContainer>
                    {warnings.map((warning, index) => (
                        <span key={`warning-${index}`}>*{warning}</span>
                    ))}
                </WarningsContainer>
            )}
        </Container>
    );
};

TableHeaderView.propTypes = {
    headerStyle: PropTypes.object,
    isWidgetSelected: PropTypes.bool,
    onSaveColumnsWidth: PropTypes.func,
    preparedCols: PropTypes.array,
    sizes: PropTypes.array,
    textStyle: PropTypes.object,
    warnings: PropTypes.array,
};

export default TableHeaderView;
