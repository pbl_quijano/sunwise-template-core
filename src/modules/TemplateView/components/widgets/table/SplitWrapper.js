import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import Split from 'split.js';
import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    width: 100%;
    .gutter {
        ${({ isWidgetSelected }) =>
            !isWidgetSelected && 'pointer-events: none;'}
        opacity: 0;
        cursor: col-resize;
        background-color: #aaa;
        background-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='10' height='30'><path d='M2 0 v30 M5 0 v30 M8 0 v30' fill='none' stroke='black'/></svg>");
        background-repeat: no-repeat;
        background-position: center;
    }

    :hover .gutter {
        ${({ isWidgetSelected }) => isWidgetSelected && 'opacity: 1;'}
    }
`;

const SplitWrapper = ({
    sizes,
    minSize,
    expandToMin,
    gutterSize,
    gutterAlign,
    snapOffset,
    dragInterval,
    direction,
    cursor,
    gutter,
    elementStyle,
    gutterStyle,
    onDragStart,
    onDragEnd,
    orderCols,
    collapsed,
    isWidgetSelected,
    children,
}) => {
    const splitRef = useRef(null);
    const wrapperRef = useRef(null);
    const options = {
        sizes,
        minSize: Array.isArray(minSize) ? minSize : parseInt(minSize),
        expandToMin,
        gutterSize: parseInt(gutterSize),
        gutterAlign,
        snapOffset: parseInt(snapOffset),
        dragInterval: parseInt(dragInterval),
        direction,
        cursor,
        gutter,
        elementStyle,
        gutterStyle,
        onDragStart,
        onDragEnd,
        collapsed,
    };

    useEffect(() => {
        if (splitRef.current !== null) {
            splitRef.current.destroy();
        }

        const itemsArray = Array.from(wrapperRef.current.children).filter(
            (element) => !element.__isSplitGutter
        );

        if (itemsArray.length) {
            splitRef.current = Split(itemsArray, options);
        }
    }, [sizes.length, orderCols]);

    return (
        <Container isWidgetSelected={isWidgetSelected} ref={wrapperRef}>
            {children}
        </Container>
    );
};

SplitWrapper.propTypes = {
    children: PropTypes.array,
    collapsed: PropTypes.bool,
    cursor: PropTypes.string,
    direction: PropTypes.string,
    dragInterval: PropTypes.number,
    elementStyle: PropTypes.object,
    expandToMin: PropTypes.string,
    gutter: PropTypes.object,
    gutterAlign: PropTypes.string,
    gutterSize: PropTypes.string,
    gutterStyle: PropTypes.object,
    headerStyle: PropTypes.object,
    isWidgetSelected: PropTypes.bool,
    minSize: PropTypes.string,
    onDragEnd: PropTypes.func,
    onDragStart: PropTypes.func,
    onSaveColumnsWidth: PropTypes.func,
    orderCols: PropTypes.string,
    preparedCols: PropTypes.array,
    sizes: PropTypes.array,
    snapOffset: PropTypes.number,
    textStyle: PropTypes.object,
};

export default SplitWrapper;
