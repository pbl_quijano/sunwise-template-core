import { Map } from 'google-maps-react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import {
    MAP_STYLES,
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SOLAR_MODULE_COLOR,
    SOLAR_MODULE_STROKE_COLOR,
} from '@constants/maps';

import { GeneralContext } from '@helpers/contexts';
import { mapValueUseEffect } from '@helpers/maps';
import { arraytoDictionary } from '@helpers/utils';

const Wrapper = styled.div`
    position: relative;
    width: ${({ width = '100%' }) => width};
    height: ${({ height = '100%' }) => height};

    &.unclickable {
        pointer-events: none;
        .gmnoprint a,
        .gmnoprint span,
        .gm-style-cc {
            display: none;
        }
        .gmnoprint div {
            background: none !important;
        }
    }
    .popup-container {
        cursor: pointer;
        height: 0;
        position: absolute;
        width: 200px;
        transform-origin: 0% 0%;
        z-index: 1;
        &:hover {
            z-index: 2;
        }
        .popup-bubble-anchor {
            position: absolute;
            width: 100%;
            bottom: 6px;
            left: 0;
            span {
                position: absolute;
                height: 13px;
                top: 0;
                left: 0;
                transform: translate(-50%, -100%);
                background-color: #002438;
                color: #ffffff;
                padding: 0 2px;
                border-radius: 2px;
                overflow-y: hidden;
                max-height: 60px;
                font-size: 8px;
                letter-spacing: 0.2px;
                line-height: 13px;
                user-select: none;
                font-weight: 100;
            }
        }
    }
    & > div {
        position: absolute;
        width: 100%;
        height: 100%;
    }
`;

const SegmentsMap = ({
    clickable = true,
    fullscreenControl = false,
    handleCenterChanged,
    handleZoomChanged,
    height,
    initCenter,
    projectLocation,
    scaleControl = true,
    segmentFillColor = SEGMENT_COLOR,
    segments = [],
    segmentStrokeColor = SEGMENT_STROKE_COLOR,
    showLabels = true,
    showProjectMarker = true,
    solarModuleFillColor = SOLAR_MODULE_COLOR,
    solarModuleStrokeColor = SOLAR_MODULE_STROKE_COLOR,
    width,
    zoom = 18,
    zoomControl = true,
}) => {
    const { t } = useTranslation();
    const { google } = useContext(GeneralContext);
    const center = {
        lat: get(initCenter, 'latitude', 20.9664281),
        lng: get(initCenter, 'longitude', -89.6239336),
    };
    const projectMarkerCenter = {
        lat: get(projectLocation, 'latitude', 20.9664281),
        lng: get(projectLocation, 'longitude', -89.6239336),
    };
    const [mapValue, setMapValue] = useState(null);
    const [shapes, setShapes] = useState({});

    useEffect(() => {
        if (mapValue) {
            mapValue.setOptions({ zoomControl });
        }
    }, [zoomControl]);

    useEffect(() => {
        if (mapValue) {
            mapValue.setOptions({ scaleControl });
        }
    }, [scaleControl]);

    useEffect(() => {
        Object.keys(shapes).forEach((key) => {
            shapes[key].setLabelsVisibility(showLabels);
        });
    }, [showLabels]);

    useEffect(() => {
        if (mapValue) {
            if (showProjectMarker) {
                projectMarker.setMap(mapValue);
            } else {
                projectMarker.setMap(null);
            }
        }
    }, [showProjectMarker]);

    useEffect(() => {
        Object.keys(shapes).forEach((key) => {
            shapes[key].setOptions({
                fillColor: segmentFillColor,
            });
        });
    }, [segmentFillColor]);

    useEffect(() => {
        Object.keys(shapes).forEach((key) => {
            shapes[key].setOptions({
                strokeColor: segmentStrokeColor,
            });
        });
    }, [segmentStrokeColor]);

    useEffect(() => {
        Object.keys(shapes).forEach((key) => {
            shapes[key].setSolarModulesFillColor(solarModuleFillColor);
        });
    }, [solarModuleFillColor]);

    useEffect(() => {
        Object.keys(shapes).forEach((key) => {
            shapes[key].setSolarModulesStrokeColor(solarModuleStrokeColor);
        });
    }, [solarModuleStrokeColor]);

    const [projectMarker] = useState(
        new google.maps.Marker({
            title: get(projectLocation, 'address', ''),
            name: t('Location'),
            position: new google.maps.LatLng(
                projectMarkerCenter.lat,
                projectMarkerCenter.lng
            ),
        })
    );

    useEffect(
        mapValueUseEffect(
            google,
            mapValue,
            projectMarker,
            arraytoDictionary(segments, 'id'),
            handleCenterChanged,
            handleZoomChanged,
            scaleControl,
            segmentFillColor,
            segmentStrokeColor,
            showLabels,
            showProjectMarker,
            solarModuleFillColor,
            solarModuleStrokeColor,
            setShapes,
            zoomControl
        ),
        [mapValue]
    );

    return (
        <Wrapper
            className={!clickable && 'unclickable'}
            height={height}
            width={width}
        >
            <Map
                fullscreenControl={fullscreenControl}
                google={google}
                initialCenter={center}
                mapTypeControl={false}
                mapType="satellite"
                onReady={(p, map) => setMapValue(map)}
                rotateControl={false}
                scaleControl={scaleControl}
                streetViewControl={false}
                styles={MAP_STYLES}
                zoom={zoom}
            />
        </Wrapper>
    );
};

SegmentsMap.propTypes = {
    clickable: PropTypes.bool,
    fullscreenControl: PropTypes.bool,
    google: PropTypes.object,
    handleCenterChanged: PropTypes.func,
    handleZoomChanged: PropTypes.func,
    height: PropTypes.string,
    initCenter: PropTypes.object,
    projectLocation: PropTypes.object,
    scaleControl: PropTypes.bool,
    segmentFillColor: PropTypes.string,
    segments: PropTypes.array,
    segmentsDictionary: PropTypes.array,
    segmentStrokeColor: PropTypes.string,
    showLabels: PropTypes.bool,
    showProjectMarker: PropTypes.bool,
    solarModuleFillColor: PropTypes.string,
    solarModuleStrokeColor: PropTypes.string,
    width: PropTypes.string,
    zoom: PropTypes.number,
    zoomControl: PropTypes.bool,
};

export default SegmentsMap;
