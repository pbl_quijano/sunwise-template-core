import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    display: block;
    height: 0;
    width: 100%;
`;

const SeparatorWidget = ({ innerHeight = 1, style = {} } = {}) => (
    <Container style={{ ...style, borderBottomWidth: `${innerHeight}px` }} />
);

SeparatorWidget.propTypes = {
    innerHeight: PropTypes.number,
    style: PropTypes.object,
};

export default SeparatorWidget;
