import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useContext } from 'react';
import { Dropdown } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { Spacer } from '@components/styled';

import {
    MOVE_BACK,
    MOVE_FORWARD,
    MOVE_STEP_BACK,
    MOVE_STEP_FORWARD,
    PAGE_WIDTH,
} from '@constants/template';
import { FULL_EDITION_MODE, MULTIPROPOSAL_TYPE } from '@constants/types';

import { GeneralContext } from '@helpers/contexts';

import { ReactComponent as BringToBackIcon } from '@res/icons/bringToBack.svg';
import { ReactComponent as BringToFrontIcon } from '@res/icons/bringToFront.svg';
import { ReactComponent as GridsIcon } from '@res/icons/grids.svg';
import { ReactComponent as PasteIcon } from '@res/icons/paste.svg';
import { ReactComponent as RedoIcon } from '@res/icons/redo.svg';
import { ReactComponent as ToBackIcon } from '@res/icons/toBack.svg';
import { ReactComponent as ToFrontIcon } from '@res/icons/toFront.svg';

import * as templateCoreActions from '@templateCore/actions';
import * as templateCoreSelectors from '@templateCore/selectors';

import * as actions from '../actions';
import * as selectors from '../selectors';

const StyledIconGrids = styled(GridsIcon)`
    svg g {
        fill: ${({ selected }) => (selected ? '#fff' : '#002438')};
    }
`;

const StyledRedoIcon = styled(RedoIcon)`
    svg g path {
        fill: #002438 !important;
    }
`;

const StyledUndoIcon = styled(RedoIcon)`
    transform: scale(-1, 1);
    svg g path {
        fill: #002438 !important;
    }
`;

const StyledPasteIcon = styled(PasteIcon)`
    svg path {
        fill: #002438;
    }
`;

const StyledBringToFrontIcon = styled(BringToFrontIcon)`
    display: inline-block;
    margin-right: 9px;
    svg path {
        fill: #848bab;
    }
`;

const StyledToFrontIcon = styled(ToFrontIcon)`
    display: inline-block;
    margin-right: 9px;
    svg path {
        fill: #848bab;
    }
`;

const StyledBringToBackIcon = styled(BringToBackIcon)`
    display: inline-block;
    margin-right: 9px;
    svg path {
        fill: #848bab;
    }
`;

const StyledToBackIcon = styled(ToBackIcon)`
    display: inline-block;
    margin-right: 9px;
    svg path {
        fill: #848bab;
    }
`;

const ToolbarWrapper = styled.div`
    display: flex;
    position: sticky;
    top: 0;
    width: 100%;
    height: ${({ visible }) => (visible ? '48px' : '0')};
    min-height: ${({ visible }) => (visible ? '48px' : '0')};
    background-color: #fff;
    box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);
    justify-content: center;
    z-index: 54;
    overflow-y: ${({ visible }) => (visible ? 'inherit' : 'hidden')};
    visibility: ${({ visible }) => (visible ? 'visible' : 'hidden')};
`;

const ToolbarContainer = styled.div`
    display: flex;
    height: 100%;
    width: ${PAGE_WIDTH}px;
    border-bottom: 1px solid #ff9a00;
`;

const ToolbarReplace = styled.div`
    height: 100%;
    .fr-toolbar {
        border: 0 solid #cccccc !important;
        background: transparent !important;
        border-radius: 0 !important;
        .fr-btn-grp {
            margin: 0 6px;
        }
        .fr-newline {
            background: transparent !important;
        }
        .fr-command.fr-btn svg path {
            fill: #002438 !important;
        }

        .fr-command.fr-btn.fr-active svg path {
            fill: #ff9a00 !important;
        }
    }
    &.disabled {
        display: none;
    }
`;

const ToolbarOption = styled.div`
    display: flex;
    position: relative;
    width: 38px;
    height: 40px;
    justify-content: center;
    align-items: center;
    background-color: transparent;
    color: #333333;
    cursor: pointer;
    margin: 4px 2px;
    transition: all 0.5s;
    border-radius: 4px;
    ${({ disabled }) =>
        disabled &&
        `
        pointer-events: none;
        cursor: default;

        svg 
        {
            opacity: 0.2;
        }
    `}
    &:hover {
        background-color: #ccc;
    }
    &.selected {
        background-color: #ff9a00;
    }
`;

const DropdownToggle = styled(Dropdown.Toggle)`
    display: flex;
    position: relative;
    width: 38px;
    height: 40px;
    justify-content: center;
    align-items: center;
    background-color: transparent !important;
    border-color: transparent !important;
    cursor: pointer;
    margin: 4px 2px;
    transition: all 0.5s;
    border-radius: 4px;
    padding: 0;
    &::after {
        display: none;
    }

    ${({ disabled }) =>
        disabled &&
        `
        pointer-events: none;
        cursor: default;

        i 
        {
            opacity: 0.45;
        }
    `}
    &:hover {
        background-color: #ccc !important;
    }
`;

const DropdownItem = styled(Dropdown.Item)`
    color: #848bab !important;
    font-size: 12px;
    line-height: 14px;
    padding: 6px 16px 7px 16px;
`;

const StyledIcon = styled.i`
    font-size: 14px;
    color: #002438;
`;

const PageToolbar = ({
    copiedWidget,
    currentTemplateType,
    editionLevel = FULL_EDITION_MODE,
    isToolbarDisabled,
    nextStatePool,
    pasteWidget,
    prevStatePool,
    selectedPageId,
    selectedWidgetId,
    setShowGuides,
    setWidgetOrder,
    showGuides,
    stateIndexSelected,
    statePoolLength,
    visible = true,
}) => {
    const { t } = useTranslation();
    const { onChangeInPage } = useContext(GeneralContext);
    const hasSummarySupport = currentTemplateType === MULTIPROPOSAL_TYPE;

    const handleSetShowGuides = (value) => {
        localStorage.setItem('showGuides', value);
        setShowGuides(value);
    };

    const handlePasteElement = (options) =>
        pasteWidget(
            selectedPageId,
            options,
            showGuides,
            null,
            hasSummarySupport,
            onChangeInPage
        );

    const handleRedo = () => nextStatePool(onChangeInPage);
    const handleUndo = () => prevStatePool(onChangeInPage);
    const onChangeItemOrder = (widgetId, moveType) =>
        setWidgetOrder(selectedPageId, widgetId, moveType, onChangeInPage);

    return (
        <ToolbarWrapper visible={visible}>
            <ToolbarContainer>
                <ToolbarReplace
                    id="froala-content"
                    className={isToolbarDisabled && 'disabled'}
                />
                <Spacer />
                {editionLevel === FULL_EDITION_MODE && (
                    <>
                        <Dropdown alignRight>
                            <DropdownToggle disabled={isNil(selectedWidgetId)}>
                                <StyledIcon className="fas fa-clone" />
                            </DropdownToggle>
                            <Dropdown.Menu>
                                <DropdownItem
                                    onClick={() =>
                                        onChangeItemOrder(
                                            selectedWidgetId,
                                            MOVE_STEP_FORWARD
                                        )
                                    }
                                >
                                    <StyledToFrontIcon width="12" height="12" />{' '}
                                    {t('Bring forward')}
                                </DropdownItem>
                                <DropdownItem
                                    onClick={() =>
                                        onChangeItemOrder(
                                            selectedWidgetId,
                                            MOVE_STEP_BACK
                                        )
                                    }
                                >
                                    <StyledToBackIcon width="12" height="12" />{' '}
                                    {t('Send backward')}
                                </DropdownItem>
                                <DropdownItem
                                    onClick={() =>
                                        onChangeItemOrder(
                                            selectedWidgetId,
                                            MOVE_FORWARD
                                        )
                                    }
                                >
                                    <StyledBringToFrontIcon
                                        width="12"
                                        height="12"
                                    />{' '}
                                    {t('Bring to front')}
                                </DropdownItem>
                                <DropdownItem
                                    onClick={() =>
                                        onChangeItemOrder(
                                            selectedWidgetId,
                                            MOVE_BACK
                                        )
                                    }
                                >
                                    <StyledBringToBackIcon
                                        width="12"
                                        height="12"
                                    />{' '}
                                    {t('Send to back')}
                                </DropdownItem>
                            </Dropdown.Menu>
                        </Dropdown>
                        <ToolbarOption
                            disabled={isNil(copiedWidget)}
                            onClick={() => handlePasteElement(copiedWidget)}
                        >
                            <StyledPasteIcon width="16" height="16" />
                        </ToolbarOption>
                        <ToolbarOption
                            disabled={
                                statePoolLength < 2 || stateIndexSelected === 0
                            }
                            onClick={handleUndo}
                        >
                            <StyledUndoIcon width="16" height="16" />
                        </ToolbarOption>
                        <ToolbarOption
                            disabled={
                                stateIndexSelected === statePoolLength - 1
                            }
                            onClick={handleRedo}
                        >
                            <StyledRedoIcon width="16" height="16" />
                        </ToolbarOption>
                        <ToolbarOption
                            className={showGuides && 'selected'}
                            onClick={() => handleSetShowGuides(!showGuides)}
                        >
                            <StyledIconGrids
                                width="16"
                                height="16"
                                selected={showGuides}
                            />
                        </ToolbarOption>
                    </>
                )}
            </ToolbarContainer>
        </ToolbarWrapper>
    );
};

const mapStateToProps = createStructuredSelector({
    copiedWidget: selectors.getCopiedItem,
    currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
    isToolbarDisabled: selectors.getIsToolbarDisabled,
    selectedPageId: selectors.getSelectedPageId,
    selectedWidgetId: selectors.getSelectedWidgetId,
    showGuides: selectors.getShowGuides,
    stateIndexSelected: selectors.getStatePoolIndex,
    statePoolLength: selectors.getStatePoolDataLength,
});

const mapDispatchToProps = (dispatch) => ({
    nextStatePool: (onUpdatePage) =>
        dispatch(actions.nextStatePool(onUpdatePage)),
    pasteWidget: (
        pageId,
        options,
        showGuides,
        position,
        hasSummarySupport,
        onChangeInPage
    ) =>
        dispatch(
            templateCoreActions.pasteWidget(
                pageId,
                options,
                showGuides,
                position,
                hasSummarySupport,
                onChangeInPage
            )
        ),
    prevStatePool: (onUpdatePage) =>
        dispatch(actions.prevStatePool(onUpdatePage)),
    setCopiedWidget: (data) => dispatch(actions.setCopiedItem(data)),
    setShowGuides: (showGuides) => dispatch(actions.setShowGuides(showGuides)),
    setWidgetOrder: (pageId, widgetId, moveType, onChangeInPage) =>
        dispatch(
            templateCoreActions.setWidgetOrder(
                pageId,
                widgetId,
                moveType,
                onChangeInPage
            )
        ),
});

PageToolbar.propTypes = {
    copiedWidget: PropTypes.object,
    currentTemplateType: PropTypes.number,
    editionLevel: PropTypes.string,
    handleRedo: PropTypes.func,
    handleUndo: PropTypes.func,
    isToolbarDisabled: PropTypes.bool,
    nextStatePool: PropTypes.func,
    onChangeItemOrder: PropTypes.func,
    pasteWidget: PropTypes.func,
    prevStatePool: PropTypes.func,
    selectedPageId: PropTypes.string,
    selectedWidgetId: PropTypes.string,
    setShowGuides: PropTypes.func,
    setWidgetOrder: PropTypes.func,
    showGuides: PropTypes.bool,
    stateIndexSelected: PropTypes.number,
    statePoolLength: PropTypes.number,
    visible: PropTypes.bool,
};
export default connect(mapStateToProps, mapDispatchToProps)(PageToolbar);
