import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { PARTIAL_EDITION_MODE } from '@constants/types';

const MenuContainer = styled.div`
    background-color: #ffffff;
    border-radius: 3px;
    border: 0.5px solid #d3d7eb;
    box-shadow: 0 2px 24px 0 rgba(13, 13, 54, 0.06);
    left: 0;
    padding: 5px;
    position: fixed;
    top: 0;
    transform: translate(
        ${({ posX = 0 }) => posX}px,
        ${({ posY = 0 }) => posY}px
    );
    z-index: 999;
`;
const MenuItemContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;
const MenuItem = styled.div`
    background-color: transparent;
    border-radius: 15px;
    border: 0;
    color: #848bab;
    cursor: pointer;
    display: block;
    font-size: 12px;
    line-height: 14px;
    padding: 6px 16px 7px 16px;
    white-space: nowrap;
    width: 100%;

    &:hover {
        background-color: #f5f5f5;
    }
    &.disabled {
        opacity: 0.5;
        cursor: default;
        pointer-events: none;
    }
`;

const OptionIcon = styled.i`
    color: #848bab;
    font-size: 12px;
    line-height: 12px;
    margin-right: 9px;
    min-height: 10px;
`;

const ContextMenu = ({
    copiedWidget,
    editionLevel,
    handleOpenSettings,
    handleRemove,
    isSelectedItem,
    onClose,
    handleCopyWidget,
    handlePasteElement,
    openSettingsDisabled,
    x,
    y,
}) => {
    const { t } = useTranslation();
    const onSelectOption = (action) => {
        action();
        onClose();
    };

    const handleCut = () => {
        handleCopyWidget();
        handleRemove();
    };

    const renderMenu = () => {
        const deleteAndCopyDisabled = editionLevel === PARTIAL_EDITION_MODE;
        if (!isSelectedItem) {
            return (
                <MenuItemContainer>
                    <MenuItem
                        className={
                            (deleteAndCopyDisabled || isNil(copiedWidget)) &&
                            'disabled'
                        }
                        onClick={() => onSelectOption(handlePasteElement)}
                    >
                        <OptionIcon className="fas fa-paste" /> {t('Paste')}
                    </MenuItem>
                </MenuItemContainer>
            );
        }
        return (
            <MenuItemContainer>
                {!openSettingsDisabled && (
                    <MenuItem
                        onClick={(e) =>
                            onSelectOption(() => handleOpenSettings(e))
                        }
                    >
                        <OptionIcon className="fa fa-cog" /> {t('Set up')}
                    </MenuItem>
                )}

                {!deleteAndCopyDisabled && (
                    <MenuItem onClick={() => onSelectOption(handleCopyWidget)}>
                        <OptionIcon className="fas fa-copy" /> {t('Copy')}
                    </MenuItem>
                )}
                {!deleteAndCopyDisabled && (
                    <MenuItem onClick={() => onSelectOption(handleCut)}>
                        <OptionIcon className="fas fa-cut" /> {t('Cut')}
                    </MenuItem>
                )}
                {!deleteAndCopyDisabled && !isNil(copiedWidget) && (
                    <MenuItem
                        onClick={() => onSelectOption(handlePasteElement)}
                    >
                        <OptionIcon className="fas fa-paste" /> {t('Paste')}
                    </MenuItem>
                )}
                {!deleteAndCopyDisabled && (
                    <MenuItem onClick={() => onSelectOption(handleRemove)}>
                        <OptionIcon className="fas fa-trash" /> {t('Delete')}
                    </MenuItem>
                )}
            </MenuItemContainer>
        );
    };

    return (
        <MenuContainer
            posX={x}
            posY={y}
            onContextMenu={(e) => e.preventDefault()}
        >
            {renderMenu()}
        </MenuContainer>
    );
};

ContextMenu.propTypes = {
    copiedWidget: PropTypes.object,
    editionLevel: PropTypes.string,
    handleOpenSettings: PropTypes.func,
    handleRemove: PropTypes.func,
    isSelectedItem: PropTypes.bool,
    onClose: PropTypes.func,
    handleCopyWidget: PropTypes.func,
    handlePasteElement: PropTypes.func,
    openSettingsDisabled: PropTypes.bool,
    x: PropTypes.number,
    y: PropTypes.number,
};

export default ContextMenu;
