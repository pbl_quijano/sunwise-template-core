import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import styled from 'styled-components';

import PagePreview from './PagePreview';

const PageContent = styled.div`
    margin: 0;
`;

const InfinitePage = ({
    chartWidgetsNames,
    froalaEditorConfig,
    nextPosition,
    page,
    pageDesignDisabled,
    tablePosition,
    tableWidgetsNames,
}) => {
    const pageRef = useRef(null);
    useEffect(() => {
        setTimeout(() => {
            if (!isNil(pageRef) && !isNil(pageRef.current)) {
                let containerContentDiv =
                    pageRef.current.getElementsByClassName(
                        'widget-table-data-container'
                    )[0];
                let containerResizeDiv = pageRef.current.getElementsByClassName(
                    'widget-table-container-resize'
                )[0];
                if (
                    containerResizeDiv &&
                    containerContentDiv &&
                    containerContentDiv.scrollTop !== tablePosition
                ) {
                    const diffHeight =
                        containerContentDiv.offsetHeight -
                        (nextPosition - tablePosition);
                    containerResizeDiv.style.maxHeight = `${
                        containerResizeDiv.offsetHeight - diffHeight
                    }px`;
                    containerContentDiv.classList.remove('puppeteer-scrolling');
                    containerContentDiv.scrollTop = parseInt(tablePosition);
                }
            }
        }, 0);
    }, [pageRef]);
    return (
        <PageContent
            className="page puppeteer-template-page"
            data-orientation={page.orientation || 'portrait'}
            ref={pageRef}
        >
            <PagePreview
                key={`page-list-item-${page.id}`}
                chartWidgetsNames={chartWidgetsNames}
                froalaEditorConfig={froalaEditorConfig}
                infinitePagesSupportEnabled
                page={page}
                pageDesignDisabled={pageDesignDisabled}
                tableWidgetsNames={tableWidgetsNames}
            />
        </PageContent>
    );
};

InfinitePage.propTypes = {
    chartWidgetsNames: PropTypes.array,
    froalaEditorConfig: PropTypes.object,
    nextPosition: PropTypes.number,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    tablePosition: PropTypes.number,
    tableWidgetsNames: PropTypes.array,
};

export default InfinitePage;
