import PropTypes from 'prop-types';

import PageDisplay from './PageDisplay';

const PageListItem = ({
    availableCatalogs,
    chartWidgetsNames,
    editionLevel,
    froalaEditorConfig,
    infinitePagesSupportEnabled,
    page,
    pageDesignDisabled,
    showGuides,
    tableWidgetsNames,
    templateType,
}) => {
    if (page.type === 'group') {
        return page.pages.map((groupedPage) => (
            <PageDisplay
                availableCatalogs={availableCatalogs}
                chartWidgetsNames={chartWidgetsNames}
                editionLevel={editionLevel}
                froalaEditorConfig={froalaEditorConfig}
                infinitePagesSupportEnabled={infinitePagesSupportEnabled}
                key={`preview-page-${page.id}/${groupedPage.id}`}
                page={groupedPage}
                pageDesignDisabled={pageDesignDisabled}
                showGuides={showGuides}
                tableWidgetsNames={tableWidgetsNames}
                templateType={templateType}
            />
        ));
    }
    return (
        <PageDisplay
            availableCatalogs={availableCatalogs}
            chartWidgetsNames={chartWidgetsNames}
            editionLevel={editionLevel}
            froalaEditorConfig={froalaEditorConfig}
            infinitePagesSupportEnabled={infinitePagesSupportEnabled}
            key={`preview-page-${page.id}`}
            page={page}
            pageDesignDisabled={pageDesignDisabled}
            showGuides={showGuides}
            tableWidgetsNames={tableWidgetsNames}
            templateType={templateType}
        />
    );
};

PageListItem.propTypes = {
    availableCatalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    infinitePagesSupportEnabled: PropTypes.bool,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    showGuides: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
};

export default PageListItem;
