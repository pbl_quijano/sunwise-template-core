import PropTypes from 'prop-types';

import { NO_EDITION_MODE } from '@constants/types';

import { isInfiniteModeEnabled } from '../../helpers';

import InfinitePages from './InfinitePages';
import PagePreview from './PagePreview';
import PageView from './PageView';

const PageDisplay = ({
    availableCatalogs,
    chartWidgetsNames,
    editionLevel,
    froalaEditorConfig,
    infinitePagesSupportEnabled,
    page,
    pageDesignDisabled,
    tableWidgetsNames,
    templateType,
}) => {
    if (
        infinitePagesSupportEnabled &&
        isInfiniteModeEnabled(page.widgets) &&
        page.infiniteMode
    ) {
        return (
            <InfinitePages
                availableCatalogs={availableCatalogs}
                chartWidgetsNames={chartWidgetsNames}
                editionLevel={editionLevel}
                froalaEditorConfig={froalaEditorConfig}
                page={page}
                pageDesignDisabled={pageDesignDisabled}
                tableWidgetsNames={tableWidgetsNames}
                templateType={templateType}
            />
        );
    }
    if (editionLevel === NO_EDITION_MODE) {
        return (
            <PagePreview
                key={`page-list-item-${page.id}`}
                chartWidgetsNames={chartWidgetsNames}
                froalaEditorConfig={froalaEditorConfig}
                page={page}
                pageDesignDisabled={pageDesignDisabled}
                tableWidgetsNames={tableWidgetsNames}
            />
        );
    }
    return (
        <PageView
            key={`page-list-item-${page.id}`}
            availableCatalogs={availableCatalogs}
            chartWidgetsNames={chartWidgetsNames}
            froalaEditorConfig={froalaEditorConfig}
            editionLevel={page.blocked === 1 ? NO_EDITION_MODE : editionLevel}
            page={page}
            pageDesignDisabled={pageDesignDisabled}
            tableWidgetsNames={tableWidgetsNames}
            templateType={templateType}
        />
    );
};

PageDisplay.propTypes = {
    availableCatalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    infinitePagesSupportEnabled: PropTypes.bool,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
};

export default PageDisplay;
