import isNull from 'lodash/isNull';
import PropTypes from 'prop-types';
import {
    useCallback,
    useContext,
    useMemo,
    useState,
    useEffect,
    useRef,
} from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { PAGE_HEIGHT, PAGE_WIDTH } from '@constants/template';
import { MULTIPROPOSAL_TYPE, PARTIAL_EDITION_MODE } from '@constants/types';

import { GeneralContext } from '@helpers/contexts';

import * as templateCoreActions from '@templateCore/actions';

import * as actions from '../../actions';
import {
    getOpenSettingsDisabled,
    getPagePosition,
    getSelectedWidgetValidated,
    handleClickContainerBuild,
    handleCloseContextMenuBuild,
    handleCopyWidgetBuild,
    handleDropBuild,
    handleOpenItemContextMenuBuild,
    handleOpenPageContextMenuBuild,
    handleOpenSettingsBuild,
    handlePasteBuild,
    selectedWidgetUseEffect,
    sortingWidgetsByOrder,
} from '../../helpers';
import * as selectors from '../../selectors';
import ContextMenu from '../ContextMenu';
import GridItem from '../GridItem/index';
import PlaceholderItem from '../PlaceholderItem';
import SettingsPanel from '../SettingsPanel';

const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    position: relative;

    & .react-draggable {
        position: absolute !important;
        top: 0;
        left: 0;
    }

    & .react-draggable-dragging .draggable-tab,
    & .react-draggable-dragging .draggable-body {
        opacity: 0.4;
        box-shadow: 0 0 0 transparent;
    }

    & .react-draggable-dragging .draggable-body {
        outline: 1px dashed #555;
    }

    & .react-draggable-dragging .corner-marker {
        opacity: 1;
    }

    ${({ showGrid }) =>
        showGrid === true &&
        `
        box-shadow: inset 0 0 0 48.9px #fff;
        background-image: linear-gradient(rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(90deg, rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(rgba(211, 215, 235, 0.5) 1px, transparent 1px), linear-gradient(90deg, rgba(211, 215, 235, 0.5) 1px, transparent 1px);
        background-size: 16.5px 16.5px;
        `}
`;

const ContextMenuBackDrop = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
`;

const DuplicateButton = styled.div`
    position: absolute;
    top: 24px;
    right: -46px;
    width: 36px;
    height: 36px;
    border-radius: 50%;
    background-color: #fff;
    font-size: 16px;
    text-align: center;
    line-height: 36px;
    color: #7f7f7f;
    cursor: pointer;
`;

const PageWrapper = styled.div`
    background-color: #fff;
    /* box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08); */
    height: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    width: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_HEIGHT : PAGE_WIDTH}px;
    min-height: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    min-width: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_HEIGHT : PAGE_WIDTH}px;
    position: relative;
    &.page-design {
        box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);
        margin: 24px 32px;
    }
    ${({ showOutline }) => showOutline && 'outline: #ff9a00 solid 2px;'}
`;

const PageView = ({
    addWidget,
    availableCatalogs = [],
    chartWidgetsNames,
    className,
    copiedWidget,
    editionLevel,
    droppingItem,
    froalaEditorConfig,
    infinitePagesSupportEnabled,
    moveWidgetByKey,
    onDeleteWidget,
    onDuplicatePage,
    page,
    pageDesignDisabled,
    pasteWidget,
    selectedWidgetId,
    selectWidget,
    setCopiedWidget,
    setDroppingItem,
    setWidgetPosition,
    setWidgetSize,
    setWidgetStyle,
    setWidgetValue,
    showGuides,
    tableWidgetsNames,
    templateType,
}) => {
    const wrapperRef = useRef(null);
    const pagePosition = useMemo(
        () => getPagePosition(wrapperRef.current),
        [wrapperRef.current]
    );

    const settingsPanelRef = useRef(null);
    const interval = useRef(null);
    const { currencyConfig, onChangeInPage } = useContext(GeneralContext);
    const hasSummarySupport = useMemo(
        () => templateType === MULTIPROPOSAL_TYPE,
        [templateType]
    );
    const [contextMenuData, setContextMenuData] = useState({
        x: 0,
        y: 0,
        pageX: 0,
        pageY: 0,
        show: false,
    });
    const pageWidgets = useMemo(() => page.widgets, [page]);
    const selectedWidget = useMemo(
        () =>
            getSelectedWidgetValidated(selectedWidgetId, page.id, pageWidgets),
        [selectedWidgetId, page.id, pageWidgets]
    );
    const openSettingsDisabled = useMemo(
        () =>
            getOpenSettingsDisabled(
                selectedWidget,
                templateType,
                chartWidgetsNames,
                tableWidgetsNames
            ),
        [selectedWidget, templateType, chartWidgetsNames, tableWidgetsNames]
    );

    useEffect(
        selectedWidgetUseEffect(
            selectedWidget && selectedWidget.name,
            (keyCode) =>
                moveWidgetByKey(
                    keyCode,
                    page.orientation,
                    handleSetPosition,
                    showGuides,
                    selectedWidget
                ),
            interval
        ),
        [selectedWidget]
    );
    const handleCloseContextMenu = useCallback(
        handleCloseContextMenuBuild(setContextMenuData),
        []
    );
    const handleCopyWidget = useCallback(
        handleCopyWidgetBuild(setCopiedWidget),
        [setCopiedWidget]
    );

    const onAddWidget = useCallback(
        (type, defaultPosition) =>
            addWidget(
                page.id,
                type,
                defaultPosition,
                hasSummarySupport,
                onChangeInPage
            ),
        [page.id, hasSummarySupport, onChangeInPage]
    );

    const handlePasteElement = useCallback(
        handlePasteBuild(
            contextMenuData,
            copiedWidget,
            hasSummarySupport,
            onChangeInPage,
            pasteWidget,
            page,
            pagePosition,
            showGuides
        ),
        [
            contextMenuData,
            copiedWidget,
            hasSummarySupport,
            onChangeInPage,
            page,
            pagePosition,
            showGuides,
        ]
    );

    const handleSetPosition = useCallback(
        (widgetId, changedX, changedY) => {
            setWidgetPosition(widgetId, changedX, changedY, onChangeInPage);
        },
        [onChangeInPage]
    );

    const handleSetWidgetSize = useCallback(
        (widgetId, changedWidth, changedHeight) => {
            setWidgetSize(
                widgetId,
                changedWidth,
                changedHeight,
                onChangeInPage
            );
        },
        [onChangeInPage]
    );

    const handleSetWidgetStyle = useCallback(
        (widgetId, style) => {
            setWidgetStyle(widgetId, style, onChangeInPage);
        },
        [onChangeInPage]
    );

    const handleSetWidgetValue = useCallback(
        (widgetId, value) => {
            setWidgetValue(widgetId, value, onChangeInPage);
        },
        [onChangeInPage]
    );

    const handleOpenPageContextMenu = useCallback(
        handleOpenPageContextMenuBuild({
            contextMenuData,
            openSettingsDisabled,
            editionLevel,
            selectedWidgetId: selectedWidget && selectedWidget.id,
            selectWidget,
            setContextMenuData,
            wrapperRef,
        }),
        [
            contextMenuData,
            openSettingsDisabled,
            editionLevel,
            selectedWidget,
            wrapperRef.current,
        ]
    );

    return (
        <PageWrapper
            orientation={page.orientation}
            className={`${className} ${!pageDesignDisabled && 'page-design'}`}
            showOutline={!pageDesignDisabled && infinitePagesSupportEnabled}
        >
            {contextMenuData.show && (
                <ContextMenu
                    copiedWidget={copiedWidget}
                    editionLevel={editionLevel}
                    handleRemove={() =>
                        onDeleteWidget(selectedWidget.id, onChangeInPage)
                    }
                    handleOpenSettings={handleOpenSettingsBuild(
                        selectedWidget,
                        settingsPanelRef
                    )}
                    handlePasteElement={handlePasteElement}
                    openSettingsDisabled={openSettingsDisabled}
                    handleCopyWidget={() => handleCopyWidget(selectedWidget)}
                    isSelectedItem={
                        selectedWidget && selectedWidget.id !== null
                    }
                    onClose={handleCloseContextMenu}
                    x={contextMenuData.x}
                    y={contextMenuData.y}
                />
            )}
            {contextMenuData.show && (
                <ContextMenuBackDrop
                    onClick={() => {
                        handleCloseContextMenu();
                        selectWidget(null);
                    }}
                    onContextMenu={handleCloseContextMenu}
                />
            )}
            <Wrapper
                showGrid={showGuides}
                onContextMenu={handleOpenPageContextMenu}
                ref={wrapperRef}
                onClick={handleClickContainerBuild(
                    contextMenuData,
                    handleCloseContextMenu,
                    selectedWidget && selectedWidget.id,
                    selectWidget,
                    wrapperRef
                )}
            >
                {pageWidgets &&
                    pageWidgets.sort(sortingWidgetsByOrder).map((widget) => {
                        return (
                            <GridItem
                                chartWidgetsNames={chartWidgetsNames}
                                currencyConfig={currencyConfig}
                                editionLevel={editionLevel}
                                froalaEditorConfig={froalaEditorConfig}
                                handleCloseContextMenu={handleCloseContextMenu}
                                handleCopyWidget={() =>
                                    handleCopyWidget(selectedWidget)
                                }
                                handleOpenContextMenu={handleOpenItemContextMenuBuild(
                                    setContextMenuData
                                )}
                                handleOpenSettings={handleOpenSettingsBuild(
                                    selectedWidget,
                                    settingsPanelRef
                                )}
                                handleRemove={() =>
                                    onDeleteWidget(
                                        selectedWidget && selectedWidget.id,
                                        onChangeInPage
                                    )
                                }
                                infinitePagesSupportEnabled={
                                    infinitePagesSupportEnabled
                                }
                                key={widget.id}
                                pageOrientation={page.orientation}
                                selected={
                                    selectedWidget &&
                                    selectedWidget.id === widget.id
                                }
                                selectWidgetId={selectWidget}
                                setWidgetPosition={handleSetPosition}
                                setWidgetSize={handleSetWidgetSize}
                                setWidgetValue={handleSetWidgetValue}
                                showGuides={showGuides}
                                tableWidgetsNames={tableWidgetsNames}
                                templateType={templateType}
                                widgetObject={widget}
                            />
                        );
                    })}
            </Wrapper>
            {!isNull(droppingItem) && (
                <PlaceholderItem
                    droppingItem={droppingItem}
                    onHandleDrop={handleDropBuild(
                        droppingItem,
                        onAddWidget,
                        setDroppingItem
                    )}
                    pageOrientation={page.orientation}
                    showGuides={showGuides}
                />
            )}

            <SettingsPanel
                catalogs={availableCatalogs}
                chartWidgetsNames={chartWidgetsNames}
                proposalSelectorDisabled={editionLevel === PARTIAL_EDITION_MODE}
                ref={settingsPanelRef}
                tableWidgetsNames={tableWidgetsNames}
                setWidgetStyle={handleSetWidgetStyle}
                setWidgetValue={handleSetWidgetValue}
                templateType={templateType}
            />
            {editionLevel === PARTIAL_EDITION_MODE && (
                <DuplicateButton
                    className="shadow-sm"
                    onClick={() => onDuplicatePage(page.id)}
                >
                    <i className="fas fa-copy" />
                </DuplicateButton>
            )}
        </PageWrapper>
    );
};

const mapStateToProps = createStructuredSelector({
    copiedWidget: selectors.getCopiedItem,
    droppingItem: selectors.getDroppingItem,
    selectedWidgetId: selectors.getSelectedWidgetId,
});

const mapDispatchToProps = (dispatch) => ({
    addWidget: (
        pageId,
        type,
        defaultPosition,
        hasSummarySupport,
        onChangeInPage
    ) =>
        dispatch(
            templateCoreActions.addWidget(
                pageId,
                type,
                defaultPosition,
                hasSummarySupport,
                onChangeInPage
            )
        ),
    moveWidgetByKey: (
        keyCode,
        orientation,
        setWidgetPosition,
        showGuides,
        selectedWidget
    ) =>
        dispatch(
            actions.moveWidgetByKey(
                keyCode,
                orientation,
                setWidgetPosition,
                showGuides,
                selectedWidget
            )
        ),
    onDeleteWidget: (widgetId, onChangeInPage) =>
        dispatch(templateCoreActions.deleteWidget(widgetId, onChangeInPage)),
    onDuplicatePage: (id) => dispatch(templateCoreActions.duplicatePage(id)),
    setDroppingItem: (item) => dispatch(actions.droppingItem(item)),
    pasteWidget: (
        pageId,
        options,
        showGuides,
        position,
        hasSummarySupport,
        onChangeInPage
    ) =>
        dispatch(
            templateCoreActions.pasteWidget(
                pageId,
                options,
                showGuides,
                position,
                hasSummarySupport,
                onChangeInPage
            )
        ),
    selectWidget: (widgetId, pageId) =>
        dispatch(actions.selectWidget(widgetId, pageId)),
    setCopiedWidget: (data) => dispatch(actions.setCopiedItem(data)),
    setWidgetPosition: (widgetId, changedX, changedY, onChangeInPage) =>
        dispatch(
            templateCoreActions.setWidgetPosition(
                widgetId,
                changedX,
                changedY,
                onChangeInPage
            )
        ),
    setWidgetSize: (widgetId, changedWidth, changedHeight, onChangeInPage) =>
        dispatch(
            templateCoreActions.setWidgetSize(
                widgetId,
                changedWidth,
                changedHeight,
                onChangeInPage
            )
        ),
    setWidgetStyle: (widgetId, style, onChangeInPage) =>
        dispatch(
            templateCoreActions.setWidgetStyle(widgetId, style, onChangeInPage)
        ),
    setWidgetValue: (widgetId, value, onChangeInPage) =>
        dispatch(
            templateCoreActions.setWidgetValue(widgetId, value, onChangeInPage)
        ),
});

PageView.propTypes = {
    addWidget: PropTypes.func,
    availableCatalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    className: PropTypes.string,
    copiedWidget: PropTypes.object,
    droppingItem: PropTypes.object,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    infinitePagesSupportEnabled: PropTypes.bool,
    moveWidgetByKey: PropTypes.func,
    onDeleteWidget: PropTypes.func,
    onDuplicatePage: PropTypes.func,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    pasteWidget: PropTypes.func,
    proposalsNumber: PropTypes.number,
    selectedWidgetId: PropTypes.string,
    selectWidget: PropTypes.func,
    setCopiedWidget: PropTypes.func,
    setDroppingItem: PropTypes.func,
    setWidgetPosition: PropTypes.func,
    setWidgetSize: PropTypes.func,
    setWidgetStyle: PropTypes.func,
    setWidgetValue: PropTypes.func,
    showGuides: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
};

export default connect(mapStateToProps, mapDispatchToProps)(PageView);
