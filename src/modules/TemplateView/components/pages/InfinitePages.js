import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { PAGE_HEIGHT, PAGE_WIDTH } from '@constants/template';
import { NO_EDITION_MODE } from '@constants/types';

import { calculateInfinitePages } from '../../helpers';

import InfinitePage from './InfinitePage';
import PagePreview from './PagePreview';
import PageView from './PageView';

const PageContent = styled.div`
    position: relative;
`;

const Wrapper = styled.div`
    position: relative;
`;

const PageHeap = styled.div`
    position: absolute;
    top: ${({ top = 0 }) => `${top}px`};
    right: ${({ right = 0 }) => `-${right}px`};
    background: #fff;
    border: 1px solid #ccc;
    width: ${({ orientation = 'portrait' }) =>
        orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    height: ${({ orientation = 'portrait' }) =>
        orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH}px;
    min-width: ${({ orientation = 'portrait' }) =>
        orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    min-height: ${({ orientation = 'portrait' }) =>
        orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH}px;
`;

const InfinitePages = ({
    availableCatalogs,
    chartWidgetsNames,
    editionLevel,
    froalaEditorConfig,
    page,
    pageDesignDisabled,
    tableWidgetsNames,
    templateType,
}) => {
    const pageRef = useRef(null);
    const [extraPages, setExtraPages] = useState([]);
    useEffect(() => {
        setTimeout(() => {
            if (!isNil(pageRef) && !isNil(pageRef.current)) {
                const containerContentDiv =
                    pageRef.current.getElementsByClassName(
                        'widget-table-data-container'
                    )[0];
                const bodyContentDiv = pageRef.current.getElementsByClassName(
                    'widget-table-data-body'
                )[0];
                const footerContentDiv = pageRef.current.getElementsByClassName(
                    'widget-table-data-footer'
                )[0];
                const rowsContentDiv = pageRef.current.getElementsByClassName(
                    'widget-table-data-row'
                );
                let containerResizeDiv = pageRef.current.getElementsByClassName(
                    'widget-table-container-resize'
                )[0];
                if (
                    containerResizeDiv &&
                    containerContentDiv &&
                    bodyContentDiv &&
                    footerContentDiv
                ) {
                    containerContentDiv.classList.remove('puppeteer-scrolling');
                    const tempExtraPages = calculateInfinitePages({
                        pageId: page.id,
                        rowDivs: [...rowsContentDiv, footerContentDiv],
                        tableContentHeight: containerContentDiv.offsetHeight,
                    });
                    if (tempExtraPages.length > 0) {
                        const [firstElement, ...restExtraPagesData] =
                            tempExtraPages;
                        const diffHeight =
                            containerContentDiv.offsetHeight -
                            (firstElement.nextPosition -
                                firstElement.tablePosition);
                        containerResizeDiv.style.maxHeight = `${
                            containerResizeDiv.offsetHeight - diffHeight
                        }px`;
                        if (restExtraPagesData.length > 0) {
                            setExtraPages(restExtraPagesData);
                        }
                    }
                }
            }
        }, 900);
    }, []);

    if (editionLevel === NO_EDITION_MODE) {
        return (
            <>
                <Wrapper
                    ref={pageRef}
                    className="page puppeteer-template-page"
                    data-orientation={page.orientation || 'portrait'}
                >
                    <PagePreview
                        key={`page-list-item-${page.id}`}
                        chartWidgetsNames={chartWidgetsNames}
                        froalaEditorConfig={froalaEditorConfig}
                        infinitePagesSupportEnabled
                        page={page}
                        pageDesignDisabled={pageDesignDisabled}
                        tableWidgetsNames={tableWidgetsNames}
                    />
                </Wrapper>
                {extraPages.map((extraPage) => (
                    <InfinitePage
                        chartWidgets={chartWidgetsNames}
                        froalaEditorConfig={froalaEditorConfig}
                        key={`preview-page-${page.id}-extra-${extraPage.id}`}
                        nextPosition={extraPage.nextPosition}
                        page={page}
                        pageDesignDisabled={pageDesignDisabled}
                        tablePosition={extraPage.tablePosition}
                        tableWidgetsNames={tableWidgetsNames}
                    />
                ))}
            </>
        );
    }

    return (
        <>
            <Wrapper
                className="page"
                data-orientation={page.orientation || 'portrait'}
            >
                {extraPages.length > 0 && (
                    <>
                        <PageHeap
                            top="35"
                            right="12"
                            orientation={page.orientation}
                        />
                        <PageHeap
                            top="30"
                            right="7"
                            orientation={page.orientation}
                        />
                    </>
                )}
                <PageContent ref={pageRef} editionLevel={editionLevel}>
                    <PageView
                        availableCatalogs={availableCatalogs}
                        chartWidgetsNames={chartWidgetsNames}
                        froalaEditorConfig={froalaEditorConfig}
                        editionLevel={
                            page.blocked === 1 ? NO_EDITION_MODE : editionLevel
                        }
                        infinitePagesSupportEnabled
                        page={page}
                        pageDesignDisabled={pageDesignDisabled}
                        tableWidgetsNames={tableWidgetsNames}
                        templateType={templateType}
                    />
                </PageContent>
            </Wrapper>
        </>
    );
};

InfinitePages.propTypes = {
    availableCatalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
};

export default InfinitePages;
