import WebfontLoader from '@dr-kobros/react-webfont-loader';
import round from 'lodash/round';
import PropTypes from 'prop-types';
import { useContext, useMemo } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { FONT_CONFIG } from '@constants/froala';
import { PAGE_HEIGHT, PAGE_WIDTH, PAGE_ZOOM } from '@constants/template';

import { GeneralContext } from '@helpers/contexts';

import * as mainSelectors from '@main/selectors';

import { sortingWidgetsByOrder } from '../../helpers';
import WidgetPreview from '../GridItem/WidgetPreview';

const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    ${({ pageScale }) => `transform: scale(${pageScale});`}
    transform-origin: left top;
    ${({ pageScale }) => `-moz-transform: scale(${pageScale});`}
    -moz-transform-origin: left top;
    position: relative;
`;

const PageWrapper = styled.div`
    background-color: #fff;
    height: ${({ orientation, newPageHeight, newPageWidth }) =>
        orientation === 'landscape' ? newPageWidth : newPageHeight}px;
    width: ${({ orientation, newPageHeight, newPageWidth }) =>
        orientation === 'landscape' ? newPageHeight : newPageWidth}px;
    min-height: ${({ orientation, newPageHeight, newPageWidth }) =>
        orientation === 'landscape' ? newPageWidth : newPageHeight}px;
    min-width: ${({ orientation, newPageHeight, newPageWidth }) =>
        orientation === 'landscape' ? newPageHeight : newPageWidth}px;
    position: relative;
`;

const PageThumbnail = ({
    chartWidgets,
    className,
    froalaEditorConfig,
    infiniteModeEnabled,
    page,
    pageScale = PAGE_ZOOM,
    tableWidgets,
}) => {
    const { currencyConfig } = useContext(GeneralContext);
    const tableWidgetsNames = useMemo(
        () => tableWidgets.map((tableWidget) => tableWidget.id),
        [tableWidgets]
    );
    const chartWidgetsNames = useMemo(
        () => chartWidgets.map((chartWidget) => chartWidget.id),
        [chartWidgets]
    );
    const [newPageWidth, newPageHeight] = useMemo(
        () => [
            round(pageScale * PAGE_WIDTH, 3),
            round(pageScale * PAGE_HEIGHT, 3),
        ],
        [pageScale]
    );

    return (
        <WebfontLoader config={FONT_CONFIG}>
            <PageWrapper
                className={className}
                newPageHeight={newPageHeight}
                newPageWidth={newPageWidth}
                orientation={page.orientation}
            >
                <Wrapper pageScale={pageScale}>
                    {page.widgets &&
                        page.widgets
                            .sort(sortingWidgetsByOrder)
                            .map((widget) => {
                                return (
                                    <WidgetPreview
                                        chartWidgetsNames={chartWidgetsNames}
                                        currencyConfig={currencyConfig}
                                        froalaEditorConfig={froalaEditorConfig}
                                        infiniteModeEnabled={
                                            infiniteModeEnabled
                                        }
                                        key={widget.id}
                                        tableWidgetsNames={tableWidgetsNames}
                                        widgetObject={widget}
                                    />
                                );
                            })}
                </Wrapper>
            </PageWrapper>
        </WebfontLoader>
    );
};

PageThumbnail.propTypes = {
    chartWidgets: PropTypes.array,
    className: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    infiniteModeEnabled: PropTypes.bool,
    page: PropTypes.object,
    pageScale: PropTypes.number,
    tableWidgets: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
    chartWidgets: mainSelectors.getDataFetchChartWidgets,
    tableWidgets: mainSelectors.getDataFetchTableWidgets,
});

export default connect(mapStateToProps, null)(PageThumbnail);
