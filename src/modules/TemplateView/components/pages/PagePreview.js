import PropTypes from 'prop-types';
import { useContext } from 'react';
import styled from 'styled-components';

import { PAGE_HEIGHT, PAGE_WIDTH } from '@constants/template';

import { GeneralContext } from '@helpers/contexts';

import { sortingWidgetsByOrder } from '../../helpers';
import WidgetPreview from '../GridItem/WidgetPreview';

const Wrapper = styled.div`
    width: 100%;
    height: 100%;
    position: relative;
`;

const PageWrapper = styled.div`
    background-color: #fff;
    height: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    width: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_HEIGHT : PAGE_WIDTH}px;
    min-height: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_WIDTH : PAGE_HEIGHT}px;
    min-width: ${({ orientation }) =>
        orientation === 'landscape' ? PAGE_HEIGHT : PAGE_WIDTH}px;
    position: relative;
    &.page-design {
        box-shadow: 20px 2px 17px 0 rgba(120, 112, 178, 0.08);
        margin: 24px 32px;
    }
    ${({ showOutline }) => showOutline && 'outline: #ff9a00 solid 2px;'}
`;

const PagePreview = ({
    chartWidgetsNames,
    className,
    froalaEditorConfig,
    infinitePagesSupportEnabled,
    page,
    pageDesignDisabled,
    tableWidgetsNames,
}) => {
    const { currencyConfig } = useContext(GeneralContext);
    return (
        <PageWrapper
            orientation={page.orientation}
            showOutline={infinitePagesSupportEnabled && !pageDesignDisabled}
            className={`sunwise-template-core-page-preview ${className} ${
                !pageDesignDisabled && 'page-design'
            }`}
        >
            <Wrapper>
                {page.widgets &&
                    page.widgets.sort(sortingWidgetsByOrder).map((widget) => {
                        return (
                            <WidgetPreview
                                chartWidgetsNames={chartWidgetsNames}
                                currencyConfig={currencyConfig}
                                froalaEditorConfig={froalaEditorConfig}
                                infinitePagesSupportEnabled={
                                    infinitePagesSupportEnabled
                                }
                                key={widget.id}
                                tableWidgetsNames={tableWidgetsNames}
                                widgetObject={widget}
                            />
                        );
                    })}
            </Wrapper>
        </PageWrapper>
    );
};

PagePreview.propTypes = {
    chartWidgetsNames: PropTypes.array,
    className: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    infinitePagesSupportEnabled: PropTypes.bool,
    page: PropTypes.object,
    pageDesignDisabled: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
};

export default PagePreview;
