import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useState } from 'react';
import Draggable from 'react-draggable';
import styled from 'styled-components';

import { GRID_WIDTH, GRID_HEIGHT } from '@constants/template';

import { calcPositionWithLimits } from '../helpers';

const Container = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
`;

const Item = styled.div`
    width: ${({ width }) => width}px;
    height: ${({ height }) => height}px;
    background-color: rgba(255, 154, 0, 0.4);
    pointer-events: none;
    z-index: 53;
`;

const PlaceholderItem = ({
    droppingItem,
    onHandleDrop,
    showGuides,
    pageOrientation,
}) => {
    const [position, setPosition] = useState({});
    const { width, height } = droppingItem;
    const handleDragOver = (event) => {
        event.stopPropagation();
        event.preventDefault();
        const { layerX, layerY } = event.nativeEvent;
        const newPosition = calcPositionWithLimits(
            layerX,
            layerY,
            width,
            height,
            pageOrientation,
            showGuides
        );
        if (newPosition.x !== position.x || newPosition.y !== position.y) {
            setPosition({ x: newPosition.x, y: newPosition.y });
        }
    };

    const handleDrop = (e) => {
        onHandleDrop(e, position.x, position.y);
        setPosition({});
    };

    const renderPlaceholderItem = () => {
        if (isEmpty(position)) return null;
        return (
            <Draggable
                position={position}
                bounds="parent"
                grid={[GRID_WIDTH, GRID_HEIGHT]}
            >
                <Item height={height} width={width} />
            </Draggable>
        );
    };

    const _onDragEnter = (event) => {
        event.preventDefault();
    };

    const _onDragStart = (event) => {
        event.preventDefault();
    };

    const _onDragLeave = (event) => {
        event.preventDefault();
        setPosition({});
    };

    if (isEmpty(droppingItem)) return null;
    return (
        <Container
            className="droptarget"
            onDragOver={handleDragOver}
            onDrop={handleDrop}
            onDragStart={_onDragStart}
            onDragEnter={_onDragEnter}
            onDragLeave={_onDragLeave}
        >
            {renderPlaceholderItem()}
        </Container>
    );
};

PlaceholderItem.propTypes = {
    droppingItem: PropTypes.object,
    onHandleDrop: PropTypes.func,
    showGuides: PropTypes.bool,
    pageOrientation: PropTypes.string,
};

export default PlaceholderItem;
