import PropTypes from 'prop-types';

import ChartPanel from './settingsPanels/ChartPanel';
import ClipArtPanel from './settingsPanels/ClipArtPanel';
import ImagePanel from './settingsPanels/ImagePanel';
import LocationPanel from './settingsPanels/LocationPanel';
import SegmentsMapPanel from './settingsPanels/SegmentsMapPanel';
import SeparatorPanel from './settingsPanels/SeparatorPanel';
import SupportTablePanel from './settingsPanels/SupportTablePanel';
import TablePanel from './settingsPanels/TablePanel';

const SettingsPanelContent = (props) => {
    const { chartWidgetsNames, hasProposalSelector, tableWidgetsNames, type } =
        props;
    switch (type) {
        case 'clip-art':
            return <ClipArtPanel {...props} />;
        case 'image':
            return <ImagePanel {...props} />;
        case 'map':
            if (hasProposalSelector) {
                return <LocationPanel {...props} />;
            }
            return null;
        case 'segments-map':
            return <SegmentsMapPanel {...props} />;
        case 'separator':
            return <SeparatorPanel {...props} />;
        case 'money-table':
        case 'kwh-table':
        case 'equipment-table':
            return <SupportTablePanel {...props} />;
        default:
            if (tableWidgetsNames.includes(type)) {
                return <TablePanel {...props} />;
            }
            if (chartWidgetsNames.includes(type)) {
                return <ChartPanel {...props} />;
            }
            return null;
    }
};

SettingsPanelContent.propTypes = {
    chartWidgetsNames: PropTypes.array,
    hasProposalSelector: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    type: PropTypes.string,
};

export default SettingsPanelContent;
