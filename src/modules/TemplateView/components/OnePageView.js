import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useContext, useEffect } from 'react';
import mouseTrap from 'react-mousetrap';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { createStructuredSelector } from 'reselect';

import {
    FULL_EDITION_MODE,
    NO_EDITION_MODE,
    MULTIPROPOSAL_TYPE,
} from '@constants/types';

import { GeneralContext } from '@helpers/contexts';

import * as templateCoreActions from '@templateCore/actions';
import * as templateCoreSelectors from '@templateCore/selectors';

import * as actions from '../actions';
import * as selectors from '../selectors';

import PageView from './pages/PageView';

const OnePageView = ({
    availableCatalogs = [],
    bindShortcut,
    chartWidgetsNames,
    copiedWidget,
    currentTemplateType,
    froalaEditorConfig,
    nextStatePool,
    pasteWidget,
    prevStatePool,
    selectDefaultPage,
    selectedPage,
    selectedPageId,
    showGuides,
    tableWidgetsNames,
    unbindShortcut,
}) => {
    const { onChangeInPage } = useContext(GeneralContext);
    const handleRedo = () => nextStatePool(onChangeInPage);
    const handleUndo = () => prevStatePool(onChangeInPage);
    useEffect(() => {
        if (selectedPage === null) {
            selectDefaultPage();
        }
    }, []);
    useEffect(() => {
        bindShortcut(
            [
                'command+z',
                'ctrl+z',
                'command+y',
                'ctrl+y',
                'command+shift+z',
                'ctrl+shift+z',
                'command+v',
                'ctrl+v',
            ],
            (e, combo) => {
                e.preventDefault();
                switch (combo) {
                    case 'command+z':
                    case 'ctrl+z':
                        handleUndo();
                        break;
                    case 'command+v':
                    case 'ctrl+v':
                        if (!isNil(copiedWidget)) {
                            pasteWidget(
                                selectedPageId,
                                copiedWidget,
                                showGuides,
                                null,
                                currentTemplateType === MULTIPROPOSAL_TYPE,
                                onChangeInPage
                            );
                        }
                        break;
                    default:
                        handleRedo();
                }
            }
        );

        return () => {
            unbindShortcut([
                'command+z',
                'ctrl+z',
                'command+y',
                'ctrl+y',
                'command+shift+z',
                'ctrl+shift+z',
                'command+v',
                'ctrl+v',
            ]);
        };
    }, [copiedWidget, selectedPageId]);

    if (!selectedPage) {
        return null;
    }

    return (
        <PageView
            key={`page-one-mode-${selectedPage.id}`}
            availableCatalogs={availableCatalogs}
            chartWidgetsNames={chartWidgetsNames}
            editionLevel={
                selectedPage.blocked === 1 ? NO_EDITION_MODE : FULL_EDITION_MODE
            }
            froalaEditorConfig={froalaEditorConfig}
            page={selectedPage}
            showGuides={showGuides}
            tableWidgetsNames={tableWidgetsNames}
            templateType={currentTemplateType}
        />
    );
};

const mapStateToProps = createStructuredSelector({
    copiedWidget: selectors.getCopiedItem,
    currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
    selectedPage: selectors.getSelectedPage,
    selectedPageId: selectors.getSelectedPageId,
});

const mapDispatchToProps = (dispatch) => ({
    pasteWidget: (
        pageId,
        options,
        showGuides,
        position,
        hasSummarySupport,
        onChangeInPage
    ) =>
        dispatch(
            templateCoreActions.pasteWidget(
                pageId,
                options,
                showGuides,
                position,
                hasSummarySupport,
                onChangeInPage
            )
        ),
    nextStatePool: (onUpdatePage) =>
        dispatch(actions.nextStatePool(onUpdatePage)),
    prevStatePool: (onUpdatePage) =>
        dispatch(actions.prevStatePool(onUpdatePage)),
    selectDefaultPage: () => dispatch(actions.selectDefaultPage()),
});

OnePageView.propTypes = {
    availableCatalogs: PropTypes.array,
    bindShortcut: PropTypes.func,
    chartWidgetsNames: PropTypes.array,
    copiedWidget: PropTypes.object,
    currentTemplateType: PropTypes.number,
    froalaEditorConfig: PropTypes.object,
    nextStatePool: PropTypes.func,
    pasteWidget: PropTypes.func,
    prevStatePool: PropTypes.func,
    selectDefaultPage: PropTypes.func,
    selectedPage: PropTypes.object,
    selectedPageId: PropTypes.string,
    showGuides: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    unbindShortcut: PropTypes.func,
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    mouseTrap
)(OnePageView);
