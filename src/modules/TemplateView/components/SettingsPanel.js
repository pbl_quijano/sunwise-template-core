import PropTypes from 'prop-types';
import { useState, forwardRef, useImperativeHandle } from 'react';
import Draggable from 'react-draggable';
import styled from 'styled-components';

import TitleWithDetail from '@components/TitleWithDetail';

import { MULTIPROPOSAL_TYPE } from '@constants/types';

import { getHasSummarySupport } from '../helpers';

import SettingsPanelContent from './SettingsPanelContent';

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    flex-basis: 0;
    flex-grow: 1;
    z-index: 2;
`;

const Content = styled.div`
    display: flex;
    flex-basis: 0;
    flex-direction: column;
    flex-grow: 1;
    overflow-y: auto;
    padding: 16px 24px;
    width: 100%;
`;

const HeadSection = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.05);
    cursor: move;
    display: flex;
    flex-direction: column;
    padding: 24px 24px 16px 24px;
`;

const BackDrop = styled.div`
    cursor: pointer;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: 1;
`;

const SettingsContainer = styled.div`
    height: 100%;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 999;
`;

const SettingsSection = styled.div`
    background-color: #ffffff;
    border-radius: 4px;
    border: 1px solid #ecedf0;
    box-shadow: 10px 12px 32px 0 rgba(0, 0, 0, 0.3);
    display: flex;
    flex-direction: column;
    height: 500px;
    width: 396px;
    z-index: 2;
    position: absolute;
`;

const StyledTitleWithDetail = styled(TitleWithDetail)`
    text-transform: capitalize;
`;

const CloseButton = styled.div`
    background-color: rgba(0, 0, 0, 0.8);
    border-radius: 50%;
    color: #fff;
    cursor: pointer;
    font-size: 12px;
    height: 20px;
    line-height: 20px;
    position: absolute;
    right: -10px;
    text-align: center;
    top: -10px;
    width: 20px;
    z-index: 3;
`;

const SettingsPanel = forwardRef(
    (
        {
            catalogs,
            chartWidgetsNames,
            proposalSelectorDisabled,
            setWidgetStyle,
            setWidgetValue,
            tableWidgetsNames,
            templateType,
        },
        ref
    ) => {
        const [SettingsData, setSettingsData] = useState({
            show: false,
        });

        useImperativeHandle(ref, () => ({
            openSettingsPanel(widget, event) {
                const limitY = window.innerHeight - 500;
                const limitX = window.innerWidth - 396;
                setSettingsData({
                    currentValue: widget.value,
                    currentStyle: widget.style,
                    hasSummarySupport: getHasSummarySupport(widget),
                    id: widget.id,
                    name: widget.field,
                    type: widget.name,
                    position: {
                        x: event.clientX > limitX ? limitX : event.clientX,
                        y: event.clientY > limitY ? limitY : event.clientY,
                    },
                    show: true,
                });
            },
        }));

        const _handleValueChange = (tempValue) => {
            if (SettingsData.id) {
                setSettingsData({ ...SettingsData, currentValue: tempValue });
                setWidgetValue(SettingsData.id, tempValue);
            }
        };

        const _handleStyleChange = (tempStyle) => {
            if (SettingsData.id) {
                setSettingsData({ ...SettingsData, currentStyle: tempStyle });
                setWidgetStyle(SettingsData.id, tempStyle);
            }
        };

        const closeSettingsPanel = () => {
            setSettingsData({ show: false });
        };

        if (!SettingsData.show) return null;

        return (
            <SettingsContainer>
                <BackDrop onClick={() => closeSettingsPanel()} />
                <Draggable
                    defaultPosition={SettingsData.position}
                    bounds="parent"
                    handle=".handle-section"
                >
                    <SettingsSection>
                        <CloseButton onClick={() => closeSettingsPanel()}>
                            x
                        </CloseButton>
                        <Wrapper>
                            <HeadSection className="handle-section">
                                <StyledTitleWithDetail>
                                    {SettingsData.name}
                                </StyledTitleWithDetail>
                            </HeadSection>

                            <Content>
                                <SettingsPanelContent
                                    catalogs={catalogs}
                                    chartWidgetsNames={chartWidgetsNames}
                                    handleStyleChange={_handleStyleChange}
                                    handleValueChange={_handleValueChange}
                                    hasProposalSelector={
                                        templateType === MULTIPROPOSAL_TYPE &&
                                        SettingsData.hasSummarySupport
                                    }
                                    name={SettingsData.name}
                                    proposalSelectorDisabled={
                                        proposalSelectorDisabled
                                    }
                                    style={SettingsData.currentStyle}
                                    tableWidgetsNames={tableWidgetsNames}
                                    type={SettingsData.type}
                                    value={SettingsData.currentValue}
                                />
                            </Content>
                        </Wrapper>
                    </SettingsSection>
                </Draggable>
            </SettingsContainer>
        );
    }
);

SettingsPanel.propTypes = {
    catalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    proposalSelectorDisabled: PropTypes.bool,
    setWidgetStyle: PropTypes.func,
    setWidgetValue: PropTypes.func,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
};

SettingsPanel.displayName = 'SettingsPanel';

export default SettingsPanel;
