import PropTypes from 'prop-types';
import { useMemo } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { PAGE_HEIGHT, PAGE_WIDTH } from '@constants/template';
import { NO_EDITION_MODE } from '@constants/types';

import * as templateCoreSelectors from '@templateCore/selectors';

import PageListItem from './pages/PageListItem';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: ${({ pageWidth }) => pageWidth + 40}px;
    margin: 0 auto;
    align-items: center;
    &.page-design-disabled {
        width: ${({ pageWidth }) => pageWidth}px;
        align-items: flex-start;
        margin: 0;
    }
    ${({ horizontalScrollMode }) =>
        horizontalScrollMode && 'flex-direction: row;'}
`;

const PageListView = ({
    availableCatalogs,
    chartWidgetsNames,
    currentTemplatePages,
    currentTemplateType,
    editionLevel = NO_EDITION_MODE,
    froalaEditorConfig,
    horizontalScrollMode,
    infinitePagesSupportEnabled,
    pageDesignDisabled,
    showGuides,
    tableWidgetsNames,
}) => {
    const hasLandscapePage = useMemo(() => {
        let hasOnlyPortraitPage = true;
        currentTemplatePages.every((page) => {
            if (page.type === 'group') {
                page.pages.every((groupedPage) => {
                    if (groupedPage.orientation === 'landscape') {
                        hasOnlyPortraitPage = false;
                    }
                    return hasOnlyPortraitPage;
                });
                return hasOnlyPortraitPage;
            }
            if (page.orientation === 'landscape') {
                hasOnlyPortraitPage = false;
            }
            return hasOnlyPortraitPage;
        });
        return !hasOnlyPortraitPage;
    }, [currentTemplatePages]);

    return (
        <Container
            className={`${pageDesignDisabled && 'page-design-disabled'}`}
            pageWidth={hasLandscapePage ? PAGE_HEIGHT : PAGE_WIDTH}
            horizontalScrollMode={horizontalScrollMode}
        >
            {currentTemplatePages.map((page) => (
                <PageListItem
                    availableCatalogs={
                        editionLevel !== NO_EDITION_MODE
                            ? availableCatalogs
                            : []
                    }
                    chartWidgetsNames={chartWidgetsNames}
                    editionLevel={editionLevel}
                    froalaEditorConfig={froalaEditorConfig}
                    infinitePagesSupportEnabled={infinitePagesSupportEnabled}
                    key={`page-list-item-${page.id}`}
                    page={page}
                    pageDesignDisabled={pageDesignDisabled}
                    showGuides={showGuides}
                    tableWidgetsNames={tableWidgetsNames}
                    templateType={currentTemplateType}
                />
            ))}
        </Container>
    );
};

const mapStateToProps = createStructuredSelector({
    currentTemplatePages: templateCoreSelectors.getCurrentTemplatePages,
    currentTemplateType: templateCoreSelectors.getCurrentTemplateType,
});

PageListView.propTypes = {
    availableCatalogs: PropTypes.array,
    chartWidgetsNames: PropTypes.array,
    currentTemplatePages: PropTypes.array,
    currentTemplateType: PropTypes.number,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    horizontalScrollMode: PropTypes.bool,
    infinitePagesSupportEnabled: PropTypes.bool,
    pageDesignDisabled: PropTypes.bool,
    showGuides: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
};

export default connect(mapStateToProps, null)(PageListView);
