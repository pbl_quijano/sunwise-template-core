import PropTypes from 'prop-types';
import styled from 'styled-components';

import { NO_EDITION_MODE } from '@constants/types';

import WidgetItem from './WidgetItem';

const Container = styled.div`
    position: absolute;
    width: ${({ size }) => `${size.width}px`};
    height: ${({ size }) => `${size.height}px`};
    top: ${({ position }) => `${position.y}px`};
    left: ${({ position }) => `${position.x}px`};
    overflow: hidden;
    z-index: 2;
    pointer-events: none;
`;

const WidgetPreview = ({
    chartWidgetsNames,
    currencyConfig,
    froalaEditorConfig,
    infinitePagesSupportEnabled,
    tableWidgetsNames,
    widgetObject,
}) => {
    const { height, id, name, replaceInfoRequired, style, value, width, x, y } =
        widgetObject;

    return (
        <Container size={{ height, width }} position={{ x, y }}>
            <WidgetItem
                chartWidgetsNames={chartWidgetsNames}
                currencyConfig={currencyConfig}
                editionLevel={NO_EDITION_MODE}
                froalaEditorConfig={froalaEditorConfig}
                height={height}
                id={id}
                infinitePagesSupportEnabled={infinitePagesSupportEnabled}
                replaceInfoRequired={replaceInfoRequired}
                style={style}
                tableWidgetsNames={tableWidgetsNames}
                type={name}
                value={value}
                width={width}
            />
        </Container>
    );
};

WidgetPreview.propTypes = {
    chartWidgetsNames: PropTypes.array,
    currencyConfig: PropTypes.object,
    froalaEditorConfig: PropTypes.object,
    infinitePagesSupportEnabled: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    widgetObject: PropTypes.object,
};

export default WidgetPreview;
