import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    content: '';
    height: 10px;
    opacity: 0;
    pointer-events: none;
    position: absolute;
    transition: opacity 0.2s linear;
    width: 10px;
    z-index: 1;

    ${({ verticalAlign }) => {
        if (verticalAlign === 'top') {
            return 'border-bottom: 2px solid rgba(0, 0, 0, 0.4);top: -10px;';
        } else {
            return 'border-top: 2px solid rgba(0, 0, 0, 0.4);bottom: -10px;';
        }
    }}

    ${({ horizontalAlign }) => {
        if (horizontalAlign === 'left') {
            return 'border-right: 2px solid rgba(0, 0, 0, 0.4);left: -10px;';
        } else {
            return 'border-left: 2px solid rgba(0, 0, 0, 0.4);right: -10px;';
        }
    }}
`;

const CornerMarker = ({ hide, horizontalAlign, verticalAlign }) => {
    if (hide) return null;
    return (
        <Container
            className="corner-marker"
            {...{ verticalAlign, horizontalAlign }}
        />
    );
};

CornerMarker.propTypes = {
    hide: PropTypes.bool,
    horizontalAlign: PropTypes.string,
    verticalAlign: PropTypes.string,
};

export default CornerMarker;
