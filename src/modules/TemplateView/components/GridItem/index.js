import PropTypes from 'prop-types';
import { Resizable } from 're-resizable';
import { useState, useEffect } from 'react';
import Draggable from 'react-draggable';
import mouseTrap from 'react-mousetrap';
import styled from 'styled-components';

import { GRID_WIDTH, GRID_HEIGHT } from '@constants/template';
import { NO_EDITION_MODE } from '@constants/types';

import {
    getOpenSettingsDisabled,
    handleOnStartDragBuild,
    handleOnStartResizeBuild,
    handleRightClickBuild,
} from '../../helpers';

import CornerMarker from './CornerMarker';
import HandlerResize from './HandlerResize';
import Toolbar from './Toolbar';
import WidgetItem from './WidgetItem';

const DraggableBody = styled.div`
    height: 100%;
    overflow: hidden;
    ${({ editionDisabled }) => editionDisabled && 'pointer-events: none;'}
    position: relative;
    transition: all 0.2s linear;
    width: 100%;
    z-index: 2;

    &.design-mode {
        outline: 1px dashed #bbb;
    }

    &.selected {
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19),
            0 6px 6px rgba(0, 0, 0, 0.23);
        outline: 1px solid rgb(255, 154, 0);
    }
`;

const GridItem = ({
    bindShortcut,
    chartWidgetsNames,
    currencyConfig,
    editionLevel,
    froalaEditorConfig,
    handleCloseContextMenu = () => {},
    handleCopyWidget = () => {},
    handleOpenContextMenu = () => {},
    handleOpenSettings = () => {},
    handleRemove = () => {},
    infinitePagesSupportEnabled,
    pageOrientation,
    selected,
    selectWidgetId,
    setWidgetPosition,
    setWidgetSize,
    setWidgetValue = () => {},
    showGuides,
    tableWidgetsNames,
    templateType,
    unbindShortcut,
    widgetObject,
}) => {
    const {
        field,
        height,
        id,
        name,
        replaceInfoRequired,
        style,
        value,
        width,
        x,
        y,
    } = widgetObject;

    const generalGrid = [
        !showGuides ? 1 : GRID_WIDTH,
        !showGuides ? 1 : GRID_HEIGHT,
    ];

    const openSettingsDisabled = getOpenSettingsDisabled(
        widgetObject,
        templateType,
        chartWidgetsNames,
        tableWidgetsNames
    );
    const [showMenu, setShowMenu] = useState(false);

    useEffect(() => {
        if (!selected && showMenu) {
            setShowMenu(false);
        }
        const handleShortcuts = (e, combo) => {
            if (combo === 'command+c' || combo === 'ctrl+c') {
                handleCopyWidget();
            } else {
                handleCut();
            }
        };
        if (selected) {
            bindShortcut(
                ['command+c', 'ctrl+c', 'command+x', 'ctrl+x'],
                handleShortcuts
            );
        } else {
            unbindShortcut(['command+c', 'ctrl+c', 'command+x', 'ctrl+x']);
        }
        return () => {
            unbindShortcut(['command+c', 'ctrl+c', 'command+x', 'ctrl+x']);
        };
    }, [selected]);

    const handleCut = () => {
        handleCopyWidget();
        handleRemove();
    };

    return (
        <Draggable
            grid={generalGrid}
            defaultPosition={{ x, y }}
            position={{ x, y }}
            handle=".handler-section"
            bounds="parent"
            onStart={handleOnStartDragBuild(
                id,
                pageOrientation,
                setWidgetPosition,
                showGuides
            )}
            onStop={(e, { lastX, lastY }) =>
                setWidgetPosition(id, lastX, lastY)
            }
            disabled={editionLevel === NO_EDITION_MODE}
        >
            <Resizable
                size={{ width, height }}
                bounds="parent"
                enable={{ bottomRight: editionLevel !== NO_EDITION_MODE }}
                grid={generalGrid}
                minWidth={1}
                minHeight={1}
                handleStyles={{ bottomRight: { zIndex: 3 } }}
                onResizeStart={handleOnStartResizeBuild(
                    id,
                    pageOrientation,
                    setWidgetPosition,
                    showGuides,
                    x,
                    y
                )}
                onResizeStop={(e, dir, r, d) =>
                    setWidgetSize(id, d.width, d.height)
                }
            >
                <DraggableBody
                    className={`draggable-body ${selected && 'selected'} ${
                        (showGuides || editionLevel !== NO_EDITION_MODE) &&
                        'design-mode'
                    }`}
                    editionDisabled={editionLevel === NO_EDITION_MODE}
                    onClick={() => selectWidgetId(id)}
                    onContextMenu={handleRightClickBuild(
                        handleCloseContextMenu,
                        handleOpenContextMenu,
                        id,
                        openSettingsDisabled,
                        selectWidgetId,
                        editionLevel
                    )}
                >
                    <WidgetItem
                        chartWidgetsNames={chartWidgetsNames}
                        currencyConfig={currencyConfig}
                        froalaEditorConfig={froalaEditorConfig}
                        handleChangeValue={(newValue) =>
                            setWidgetValue(id, newValue)
                        }
                        height={height}
                        id={id}
                        infinitePagesSupportEnabled={
                            infinitePagesSupportEnabled
                        }
                        isSelected={selected}
                        editionLevel={editionLevel}
                        replaceInfoRequired={replaceInfoRequired}
                        style={style}
                        tableWidgetsNames={tableWidgetsNames}
                        type={name}
                        value={value}
                        width={width}
                    />
                    <HandlerResize show={editionLevel !== NO_EDITION_MODE} />
                </DraggableBody>
                <Toolbar
                    editionLevel={editionLevel}
                    handleCopyWidget={handleCopyWidget}
                    handleCut={handleCut}
                    handleOpenSettings={handleOpenSettings}
                    handleRemove={handleRemove}
                    keyName={field}
                    openSettingsDisabled={openSettingsDisabled}
                    show={selected}
                    showMenu={showMenu}
                />
                <CornerMarker
                    hide={
                        !selected ||
                        !showGuides ||
                        editionLevel === NO_EDITION_MODE
                    }
                    horizontalAlign="left"
                    verticalAlign="bottom"
                />
                <CornerMarker
                    hide={
                        !selected ||
                        !showGuides ||
                        editionLevel === NO_EDITION_MODE
                    }
                    horizontalAlign="right"
                    verticalAlign="bottom"
                />
                <CornerMarker
                    hide={
                        !selected ||
                        !showGuides ||
                        editionLevel === NO_EDITION_MODE
                    }
                    horizontalAlign="left"
                    verticalAlign="top"
                />
                <CornerMarker
                    hide={
                        !selected ||
                        !showGuides ||
                        editionLevel === NO_EDITION_MODE
                    }
                    horizontalAlign="right"
                    verticalAlign="top"
                />
            </Resizable>
        </Draggable>
    );
};

GridItem.propTypes = {
    bindShortcut: PropTypes.func,
    chartWidgetsNames: PropTypes.array,
    currencyConfig: PropTypes.object,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    handleCloseContextMenu: PropTypes.func,
    handleCopyWidget: PropTypes.func,
    handleOpenContextMenu: PropTypes.func,
    handleOpenSettings: PropTypes.func,
    handleRemove: PropTypes.func,
    infinitePagesSupportEnabled: PropTypes.bool,
    pageOrientation: PropTypes.string,
    selected: PropTypes.bool,
    selectWidgetId: PropTypes.func,
    setWidgetPosition: PropTypes.func,
    setWidgetSize: PropTypes.func,
    setWidgetValue: PropTypes.func,
    showGuides: PropTypes.bool,
    tableWidgetsNames: PropTypes.array,
    templateType: PropTypes.number,
    unbindShortcut: PropTypes.func,
    widgetObject: PropTypes.object,
};

export default mouseTrap(GridItem);
