import PropTypes from 'prop-types';

import { NO_EDITION_MODE } from '@constants/types';

import ChartWidget from '../widgets/ChartWidget';
import ClipArtWidget from '../widgets/ClipArtWidget';
import ImageWidget from '../widgets/ImageWidget';
import MapWidget from '../widgets/MapWidget';
import SegmentsMapWidget from '../widgets/SegmentsMapWidget';
import SeparatorWidget from '../widgets/SeparatorWidget';
import SignatureWidget from '../widgets/SignatureWidget';
import SupportChartWidget from '../widgets/SupportChartWidget';
import SupportTableWidget from '../widgets/SupportTableWidget';
import TableWidget from '../widgets/TableWidget';
import TextWidget from '../widgets/TextWidget';

const WidgetItem = ({
    chartWidgetsNames,
    currencyConfig,
    editionLevel,
    froalaEditorConfig,
    handleChangeValue = () => {},
    height: innerHeight,
    id,
    infinitePagesSupportEnabled,
    isSelected,
    replaceInfoRequired,
    style,
    tableWidgetsNames,
    type,
    value,
    width: innerWidth,
}) => {
    switch (type) {
        case 'clip-art':
            return ClipArtWidget({
                innerHeight,
                innerWidth,
                style,
            });
        case 'image':
            return ImageWidget({ value, style });
        case 'map':
            return MapWidget({
                keyName: id,
                editionLevel,
                handleChangeValue,
                value,
                innerHeight,
                innerWidth,
            });
        case 'segments-map':
            return SegmentsMapWidget({
                keyName: id,
                editionLevel,
                handleChangeValue,
                style,
                value,
                innerHeight,
            });
        case 'separator':
            return SeparatorWidget({ innerHeight, style });
        case 'signature':
            return SignatureWidget({ innerHeight, innerWidth, value });
        case 'money-table':
        case 'kwh-table':
        case 'equipment-table':
            return SupportTableWidget({
                editionDisabled: editionLevel === NO_EDITION_MODE,
                style,
                value,
            });
        case 'text':
            return (
                <TextWidget
                    editionDisabled={editionLevel === NO_EDITION_MODE}
                    froalaEditorConfig={froalaEditorConfig}
                    handleChangeValue={handleChangeValue}
                    isSelected={isSelected}
                    keyName={id}
                    value={value}
                />
            );
        default: {
            if (tableWidgetsNames.includes(type)) {
                const { tableIso, tableLocale } = currencyConfig;
                return TableWidget({
                    currencyIso: tableIso,
                    currencyLocale: tableLocale,
                    editionLevel,
                    handleChangeValue,
                    infinitePagesSupportEnabled,
                    innerWidth,
                    isSelected,
                    keyName: id,
                    style,
                    value,
                });
            }
            if (chartWidgetsNames.includes(type)) {
                const { chartIso, chartLocale } = currencyConfig;
                if (replaceInfoRequired !== 'graph') {
                    return SupportChartWidget({
                        currencyIso: chartIso,
                        currencyLocale: chartLocale,
                        editionDisabled: editionLevel === NO_EDITION_MODE,
                        innerHeight,
                        innerWidth,
                        value,
                    });
                }
                return ChartWidget({
                    currencyIso: chartIso,
                    currencyLocale: chartLocale,
                    innerHeight,
                    innerWidth,
                    value,
                });
            }
            return null;
        }
    }
};

WidgetItem.propTypes = {
    chartWidgetsNames: PropTypes.array,
    currencyConfig: PropTypes.object,
    editionLevel: PropTypes.string,
    froalaEditorConfig: PropTypes.object,
    handleChangeValue: PropTypes.func,
    height: PropTypes.number,
    id: PropTypes.string,
    infinitePagesSupportEnabled: PropTypes.bool,
    isSelected: PropTypes.bool,
    replaceInfoRequired: PropTypes.string,
    style: PropTypes.object,
    tableWidgetsNames: PropTypes.array,
    type: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    width: PropTypes.number,
};

export default WidgetItem;
