import PropTypes from 'prop-types';
import { Dropdown } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

import { PARTIAL_EDITION_MODE } from '@constants/types';

const Container = styled.div`
    background-color: rgb(255, 154, 0);
    display: flex;
    height: 20px;
    left: 0;
    outline: 1px solid rgb(255, 154, 0);
    padding-left: 4px;
    position: absolute;
    top: -21px;
    transition: all 0.2s linear;
    width: 100%;
    z-index: 4;
`;

const HandlerSection = styled.div`
    align-items: center;
    cursor: move;
    display: flex;
    flex-grow: 1;
    height: 20px;
    overflow: hidden;
    padding-right: 6px;
    white-space: nowrap;
`;

const NameText = styled.span`
    color: #fff;
    font-size: 11px;
    height: 14px;
    line-height: 14px;
    overflow: hidden;
    text-overflow: ellipsis;
    text-transform: capitalize;
`;

const StyledIcon = styled.i`
    color: #ffffff !important;
    font-size: 14px;
    line-height: 18px;
`;

const DropdownToggle = styled(Dropdown.Toggle)`
    background-color: transparent;
    border-color: transparent;
    display: flex;
    height: 20px;
    justify-content: center;
    padding: 0;
    width: 20px;

    &::after {
        display: none;
    }
`;

const DropdownItem = styled(Dropdown.Item)`
    color: #848bab !important;
    font-size: 12px;
    line-height: 14px;
    padding: 6px 16px 7px 16px;
`;

const OptionIcon = styled.i`
    color: #848bab;
    font-size: 12px;
    line-height: 12px;
    margin-right: 9px;
    min-height: 10px;
`;

const Toolbar = ({
    editionLevel,
    handleCopyWidget,
    handleCut,
    handleOpenSettings,
    handleRemove,
    keyName,
    openSettingsDisabled,
    show,
    showMenu,
}) => {
    const { t } = useTranslation();

    if (!show) return null;
    const deleteAndCopyDisabled = editionLevel === PARTIAL_EDITION_MODE;
    return (
        <Container className="draggable-tab" showMenu={showMenu}>
            <HandlerSection className="handler-section cursor">
                <NameText>{keyName}</NameText>
            </HandlerSection>
            {(!deleteAndCopyDisabled || !openSettingsDisabled) && (
                <Dropdown alignRight>
                    <DropdownToggle>
                        <StyledIcon className="fas fa-ellipsis-v" />
                    </DropdownToggle>
                    <Dropdown.Menu>
                        {!openSettingsDisabled && (
                            <DropdownItem onClick={handleOpenSettings}>
                                <OptionIcon className="fa fa-cog" />{' '}
                                {t('Set up')}
                            </DropdownItem>
                        )}
                        {!deleteAndCopyDisabled && (
                            <DropdownItem onClick={handleCopyWidget}>
                                <OptionIcon className="fas fa-copy" />{' '}
                                {t('Copy')}
                            </DropdownItem>
                        )}
                        {!deleteAndCopyDisabled && (
                            <DropdownItem onClick={handleCut}>
                                <OptionIcon className="fas fa-cut" /> {t('Cut')}
                            </DropdownItem>
                        )}
                        {!deleteAndCopyDisabled && (
                            <DropdownItem onClick={handleRemove}>
                                <OptionIcon className="fas fa-trash" />{' '}
                                {t('Delete')}
                            </DropdownItem>
                        )}
                    </Dropdown.Menu>
                </Dropdown>
            )}
        </Container>
    );
};

Toolbar.propTypes = {
    editionLevel: PropTypes.string,
    handleCopyWidget: PropTypes.func,
    handleCut: PropTypes.func,
    handleOpenSettings: PropTypes.func,
    handleRemove: PropTypes.func,
    keyName: PropTypes.string,
    openSettingsDisabled: PropTypes.bool,
    show: PropTypes.bool,
    showMenu: PropTypes.bool,
};

export default Toolbar;
