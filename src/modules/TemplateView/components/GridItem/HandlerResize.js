import PropTypes from 'prop-types';
import styled from 'styled-components';

const Container = styled.div`
    border-bottom: 2px solid rgba(0, 0, 0, 0.4);
    border-right: 2px solid rgba(0, 0, 0, 0.4);
    bottom: 3px;
    content: '';
    height: 5px;
    pointer-events: none;
    position: absolute;
    right: 3px;
    width: 5px;
    z-index: 9999;
`;

const HandlerResize = ({ show }) => {
    if (!show) return null;
    return <Container />;
};

HandlerResize.propTypes = {
    show: PropTypes.bool,
};

export default HandlerResize;
