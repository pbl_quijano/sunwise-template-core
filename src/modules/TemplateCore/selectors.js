import isEmpty from 'lodash/isEmpty';
import sortBy from 'lodash/sortBy';
import { createSelector } from 'reselect';

import * as selectors from '@main/selectors';

import orm from '@orm';

import { getUpdatingContentData } from './helpers';

export const getModel = createSelector(
    selectors.getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.templateCore
);

export const getEntitiesSession = createSelector(getModel, (state) =>
    orm.session(state.entities)
);

export const getCurrentTemplateId = createSelector(
    getModel,
    (model) => model.currentTemplateId
);

export const getCurrentTemplateModel = createSelector(
    getEntitiesSession,
    getCurrentTemplateId,
    ({ Template }, currentTemplateId) => {
        if (
            currentTemplateId === null ||
            !Template.idExists(currentTemplateId)
        ) {
            return null;
        }
        return Template.withId(currentTemplateId);
    }
);

export const getCurrentTemplatePages = createSelector(
    getCurrentTemplateModel,
    (currentTemplateModel) => {
        if (currentTemplateModel === null) {
            return [];
        }
        const pages = currentTemplateModel.templatePages
            .toModelArray()
            .map((pageModel) => {
                return {
                    ...pageModel.ref,
                    type: 'page',
                    widgets: pageModel.widgets.toRefArray(),
                };
            });
        const groups = currentTemplateModel.groups
            .toModelArray()
            .map((groupModel) => {
                const pagesInGroup = groupModel.groupPages
                    .toModelArray()
                    .map((pageModel) => {
                        return {
                            ...pageModel.ref,
                            type: 'page',
                            widgets: pageModel.widgets.toRefArray(),
                        };
                    });
                return {
                    ...groupModel.ref,
                    type: 'group',
                    pages: pagesInGroup,
                };
            });
        return sortBy(
            [...pages, ...groups],
            [
                (p) => {
                    return parseInt(p.page);
                },
            ]
        );
    }
);

export const getIsEmptyPages = createSelector(
    getCurrentTemplatePages,
    (currentTemplatePages) => isEmpty(currentTemplatePages)
);

export const getCurrentTemplateData = createSelector(
    getCurrentTemplateModel,
    (currentTemplateModel) => currentTemplateModel && currentTemplateModel.ref
);

export const getCurrentTemplateLanguage = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) => currentTemplateData && currentTemplateData.language
);

export const getCurrentTemplateType = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) => currentTemplateData && currentTemplateData.type
);

export const getCurrentTemplateTitle = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) => currentTemplateData && currentTemplateData.title
);

export const getCurrentTemplateCreatedAt = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) =>
        currentTemplateData && currentTemplateData.created_at
);

export const getCurrentTemplateVersion = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) => currentTemplateData && currentTemplateData.version
);

export const getCurrentTemplateProposalNumber = createSelector(
    getCurrentTemplateData,
    (currentTemplateData) =>
        currentTemplateData && currentTemplateData.proposals_number
);

export const getCurrentTemplateFullData = createSelector(
    getCurrentTemplateData,
    getCurrentTemplatePages,
    (currentTemplateData, currentTemplatePages) => {
        if (currentTemplateData === null) {
            return null;
        }
        return {
            ...currentTemplateData,
            pages: currentTemplatePages,
        };
    }
);

export const getTemplateUpdatingContentData = createSelector(
    getCurrentTemplateFullData,
    (currentTemplateFullData) => {
        if (currentTemplateFullData === null) {
            return null;
        }
        return getUpdatingContentData(currentTemplateFullData);
    }
);
