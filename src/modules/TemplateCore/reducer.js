import arrayMove from 'array-move';
import update from 'immutability-helper';
import difference from 'lodash/difference';
import isNil from 'lodash/isNil';
import sortBy from 'lodash/sortBy';

import orm from '@orm';

import {
    ADD_WIDGET,
    ADD_NEW_PAGES,
    DELETE_PAGES,
    DELETE_WIDGET,
    CHANGE_PAGE_INFINITE_MODE,
    CHANGE_PAGE_ORIENTATION,
    CHANGE_PAGE_THEME,
    CHANGE_WIDGET_ORDER,
    CHANGE_WIDGET_POSITION,
    CHANGE_WIDGET_SIZE,
    CHANGE_WIDGET_STYLE,
    CHANGE_WIDGET_VALUE,
    INITIALIZE,
    PASTE_WIDGET,
    RE_ORDER_PAGES,
    RESTORE_REVIEW_PAGES,
    RESET,
    RESET_TEMPLATE,
    SET_PAGE_DATA,
    SET_TEMPLATE_BACKUP,
} from './actionTypes';
import { createGroups, createGroup, createPage, getLimits } from './helpers';

const INITIAL_STATE = {
    entities: orm.getEmptyState(),
    currentTemplateId: null,
    templateBackUp: null,
};

export default function (state = INITIAL_STATE, action) {
    const session = orm.session(state.entities);
    const { Group, Page, Template, Widget } = session;
    switch (action.type) {
        case PASTE_WIDGET:
        case ADD_WIDGET: {
            Widget.create(action.payload);
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case ADD_NEW_PAGES: {
            const pagesTemplate = action.payload;
            const templateModel = Template.withId(state.currentTemplateId);
            const pageIds = templateModel
                ? templateModel.templatePages.toRefArray().map((p) => p.id)
                : [];
            pagesTemplate
                .filter((p) => isNil(p.grouped_by))
                .forEach((p) => {
                    if (pageIds.includes(p.id)) {
                        Page.withId(p.id).set('page', p.page);
                    } else {
                        createPage({
                            pageModel: Page,
                            tempPage: p,
                            templateId: state.currentTemplateId,
                            widgetModel: Widget,
                        });
                    }
                });
            const groupIds = templateModel.groups.toRefArray().map((g) => g.id);
            const groups = createGroups(pagesTemplate);
            for (const key in groups) {
                if (groupIds.includes(key)) {
                    Group.withId(key).set('page', groups[key].firstPage);
                    for (const p of groups[key].pages) {
                        Page.withId(p.id).set('page', p.page);
                    }
                } else {
                    createGroup({
                        group: groups[key],
                        groupModel: Group,
                        pageModel: Page,
                        templateId: state.currentTemplateId,
                        widgetModel: Widget,
                    });
                }
            }
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case CHANGE_PAGE_INFINITE_MODE:
            Page.withId(action.payload.pageId).set(
                'infiniteMode',
                action.payload.infiniteMode
            );
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });

        case CHANGE_PAGE_ORIENTATION: {
            const { orientation, pageId } = action.payload;
            Page.withId(pageId)
                .widgets.toModelArray()
                .forEach((widgetModel) => {
                    const { width, height, x, y } = widgetModel.ref;
                    let widgetWidth = 0 + width;
                    let widgetHeight = 0 + height;
                    const { x: limitX, y: limitY } = getLimits(orientation);
                    if (widgetWidth > limitX) {
                        widgetWidth = limitX;
                        widgetModel.set('width', widgetWidth);
                    }

                    if (widgetHeight > limitY) {
                        widgetHeight = limitY;
                        widgetModel.set('height', widgetHeight);
                    }

                    if (x + widgetWidth > limitX) {
                        widgetModel.set('x', limitX - widgetWidth);
                    }

                    if (y + widgetHeight > limitY) {
                        widgetModel.set('y', limitY - widgetHeight);
                    }
                });
            Page.withId(pageId).set('orientation', orientation);
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case CHANGE_PAGE_THEME:
            Page.withId(action.payload.pageId).set(
                'theme',
                action.payload.theme
            );
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });

        case CHANGE_WIDGET_ORDER: {
            const { pageId, newOrderIndex, widgetIndex } = action.payload;
            arrayMove(
                sortBy(Page.withId(pageId).widgets.toModelArray(), [
                    (w) => {
                        return parseInt(w.ref.order);
                    },
                ]),
                widgetIndex,
                newOrderIndex
            ).forEach((widgetModel, index) => {
                widgetModel.set('order', index);
            });
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case CHANGE_WIDGET_POSITION: {
            const widgetModel = Widget.withId(action.payload.widgetId);
            widgetModel.set('x', action.payload.changedX);
            widgetModel.set('y', action.payload.changedY);
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case CHANGE_WIDGET_SIZE: {
            const widgetModel = Widget.withId(action.payload.widgetId);
            widgetModel.set(
                'width',
                widgetModel.ref.width + action.payload.changedWidth
            );
            widgetModel.set(
                'height',
                widgetModel.ref.height + action.payload.changedHeight
            );
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case CHANGE_WIDGET_STYLE:
            Widget.withId(action.payload.widgetId).set(
                'style',
                action.payload.style
            );
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        case CHANGE_WIDGET_VALUE:
            Widget.withId(action.payload.widgetId).set(
                'value',
                action.payload.value
            );
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });

        case DELETE_PAGES: {
            const pagesTemplate = action.payload;
            difference(
                Template.withId(state.currentTemplateId)
                    .templatePages.toRefArray()
                    .map((p) => p.id),
                pagesTemplate
                    .filter((p) => isNil(p.grouped_by))
                    .map((p) => p.id)
            ).forEach((p) => {
                Page.withId(p).widgets.delete();
                Page.withId(p).delete();
            });

            difference(
                Template.withId(state.currentTemplateId)
                    .templatePages.toRefArray()
                    .map((p) => p.id),
                pagesTemplate
                    .filter((p) => isNil(p.grouped_by))
                    .map((p) => p.id)
            ).forEach((p) => {
                Page.withId(p).widgets.delete();
                Page.withId(p).delete();
            });
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case DELETE_WIDGET:
            Widget.withId(action.payload).delete();
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });

        case INITIALIZE: {
            const { pages_template: pagesTemplate, ...templateData } =
                action.payload;
            Template.create({ ...templateData });
            pagesTemplate
                .filter((p) => isNil(p.grouped_by))
                .forEach((p) =>
                    createPage({
                        pageModel: Page,
                        tempPage: p,
                        templateId: templateData.id,
                        widgetModel: Widget,
                    })
                );
            const groups = createGroups(pagesTemplate);
            for (const groupKey in groups) {
                createGroup({
                    group: groups[groupKey],
                    groupModel: Group,
                    pageModel: Page,
                    templateId: templateData.id,
                    widgetModel: Widget,
                });
            }
            return update(state, {
                currentTemplateId: {
                    $set: templateData.id,
                },
                entities: {
                    $set: session.state,
                },
            });
        }

        case RE_ORDER_PAGES: {
            const pagesTemplate = action.payload;
            pagesTemplate.forEach((p) => {
                if (Page.idExists(p.id)) {
                    Page.withId(p.id).set('page', p.page);
                }
            });
            const groups = createGroups(pagesTemplate);
            for (const key in groups) {
                const currentGroup = groups[key];
                Group.withId(currentGroup.id).set(
                    'page',
                    currentGroup.firstPage
                );
            }
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case RESET:
        case RESET_TEMPLATE:
            return update(state, {
                $set: INITIAL_STATE,
            });

        case SET_PAGE_DATA: {
            const { id, infiniteMode, orientation, theme, widgets } =
                action.payload;
            Page.withId(id).update({
                infiniteMode: infiniteMode,
                orientation: orientation,
                theme: theme,
            });
            difference(
                Page.withId(id)
                    .widgets.toRefArray()
                    .map((w) => w.id),
                widgets.map((w) => w.id)
            ).forEach((widgetId) => {
                Widget.withId(widgetId).delete();
            });
            widgets.forEach((widget) => {
                if (Widget.idExists(widget.id)) {
                    Widget.withId(widget.id).update(widget);
                } else {
                    Widget.create(widget);
                }
            });
            return update(state, {
                entities: {
                    $set: session.state,
                },
            });
        }

        case SET_TEMPLATE_BACKUP:
            return update(state, {
                templateBackUp: {
                    $set: action.payload,
                },
            });

        case RESTORE_REVIEW_PAGES: {
            const backupPages = JSON.parse(state.templateBackUp);
            backupPages.forEach((p) => {
                if (Page.idExists(p.id)) {
                    Page.withId(p.id).update({
                        infiniteMode: p.infiniteMode,
                        orientation: p.orientation,
                        theme: p.theme,
                    });
                    difference(
                        Page.withId(p.id)
                            .widgets.toRefArray()
                            .map((w) => w.id),
                        p.widgets.map((w) => w.id)
                    ).forEach((widgetId) => {
                        Widget.withId(widgetId).delete();
                    });

                    p.widgets.forEach((widget) => {
                        if (Widget.idExists(widget.id)) {
                            Widget.withId(widget.id).update(widget);
                        } else {
                            Widget.create(widget);
                        }
                    });
                }
            });
            difference(
                Template.withId(state.currentTemplateId)
                    .templatePages.toRefArray()
                    .map((p) => p.id),
                backupPages.map((p) => p.id)
            ).forEach((p) => {
                Page.withId(p).widgets.delete();
                Page.withId(p).delete();
            });
            return update(state, {
                templateBackUp: {
                    $set: null,
                },
                entities: {
                    $set: session.state,
                },
            });
        }

        default:
            return state;
    }
}
