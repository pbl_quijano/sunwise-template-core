export const ADD_WIDGET = 'ADD_WIDGET';
export const ADD_NEW_PAGES = 'ADD_NEW_PAGES';
export const DELETE_PAGES = 'DELETE_PAGES';

export const RESET = 'RESET';
export const RESET_TEMPLATE = 'RESET_TEMPLATE';

export const INITIALIZE = 'INITIALIZE';

export const PASTE_WIDGET = 'PASTE_WIDGET';
export const CHANGE_PAGE_INFINITE_MODE = 'CHANGE_PAGE_INFINITE_MODE';
export const CHANGE_PAGE_ORIENTATION = 'CHANGE_PAGE_ORIENTATION';
export const CHANGE_PAGE_THEME = 'CHANGE_PAGE_THEME';

export const DELETE_WIDGET = 'DELETE_WIDGET';
export const CHANGE_WIDGET_ORDER = 'CHANGE_WIDGET_ORDER';
export const CHANGE_WIDGET_POSITION = 'CHANGE_WIDGET_POSITION';
export const CHANGE_WIDGET_SIZE = 'CHANGE_WIDGET_SIZE';
export const CHANGE_WIDGET_STYLE = 'CHANGE_WIDGET_STYLE';
export const CHANGE_WIDGET_VALUE = 'CHANGE_WIDGET_VALUE';

export const RE_ORDER_PAGES = 'RE_ORDER_PAGES';
export const RESTORE_REVIEW_PAGES = 'RESTORE_REVIEW_PAGES';

export const SET_PAGE_DATA = 'SET_PAGE_DATA';
export const SET_TEMPLATE_BACKUP = 'SET_TEMPLATE_BACKUP';
