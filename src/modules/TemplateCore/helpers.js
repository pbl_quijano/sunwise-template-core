/* eslint-disable no-misleading-character-class */
/* eslint-disable no-control-regex */
import cloneDeep from 'lodash/cloneDeep';
import get from 'lodash/get';
import isNil from 'lodash/isNil';
import sortBy from 'lodash/sortBy';

import {
    GRID_WIDTH,
    GRID_HEIGHT,
    MOVE_BACK,
    MOVE_FORWARD,
    MOVE_STEP_BACK,
    MOVE_STEP_FORWARD,
    PAGE_HEIGHT,
    PAGE_WIDTH,
    WIDGET_SUPPORT_VERSION,
} from '@constants/template';

import { buildGraphsColors } from '@helpers/template';

import {
    DEFAULT_WIDGET_VALUES,
    // NEW_PAGE_CONTENT,
    TABLE_THEMES,
} from './constants';

const getPagesTemplate = (pages) => {
    return pages.reduce((acc, item) => {
        if (item.type === 'group') {
            const tempGroupPages = item.pages.map((page) =>
                getPageUpdatingContentData(page)
            );
            return [...acc, ...tempGroupPages];
        }
        return [...acc, getPageUpdatingContentData(item)];
    }, []);
};

const getWidgetUpdatingContentData = (widget) => {
    const widgetData = {
        field: widget.field,
        meta: {
            hasSummarySupport: widget.hasSummarySupport,
            height: widget.height,
            num: widget.num,
            order: widget.order,
            replaceInfoRequired: widget.replaceInfoRequired,
            style: widget.style,
            value: widget.value,
            supportVersion: widget.supportVersion,
            width: widget.width,
            x: widget.x,
            y: widget.y,
        },
        name: widget.name,
    };

    return {
        widget: widgetData,
        value: { [widget.field]: widget.value },
        replaceInfoData: widget.replaceInfoRequired
            ? {
                  field: widget.field,
                  replaceBy: widget.replaceInfoRequired,
              }
            : null,
    };
};

export const buildTemplateWidgets = ({
    pageId,
    values,
    widgetModel,
    widgets,
}) => {
    widgets.forEach((widget) => {
        const { name, field, meta } = widget;
        const {
            freeGrid,
            hasSummarySupport,
            height,
            id,
            num,
            order,
            replaceInfoRequired,
            style = {},
            supportVersion = 1,
            width,
            x,
            y,
        } = meta;
        widgetModel.create({
            id: id || `${pageId}/${field}`,
            field,
            hasSummarySupport,
            height:
                supportVersion === 1 && !freeGrid
                    ? height * GRID_HEIGHT
                    : height,
            name,
            num,
            order,
            pageId,
            replaceInfoRequired,
            style,
            supportVersion: WIDGET_SUPPORT_VERSION,
            value: values[field],
            width:
                supportVersion === 1 && !freeGrid ? width * GRID_WIDTH : width,
            x: supportVersion === 1 && !freeGrid ? x * GRID_WIDTH : x,
            y: supportVersion === 1 && !freeGrid ? y * GRID_HEIGHT : y,
        });
    });
};

export const createGroup = ({
    group,
    groupModel,
    pageModel,
    templateId,
    widgetModel,
}) => {
    const { blocked: groupBlocked, firstPage, id: groupId, pages = [] } = group;
    groupModel.create({
        blocked: groupBlocked,
        id: groupId,
        page: firstPage,
        templateId,
    });

    pages.forEach((p) =>
        createPage({
            pageModel: pageModel,
            tempPage: p,
            groupId: groupId,
            widgetModel: widgetModel,
        })
    );
};

export const createGroups = (pagesTemplate) => {
    return sortBy(
        pagesTemplate.filter((p) => !isNil(p.grouped_by)),
        [
            (p) => {
                return parseInt(p.page);
            },
        ]
    ).reduce((acc, current) => {
        if (acc[current.grouped_by]) {
            return {
                ...acc,
                [current.grouped_by]: {
                    ...acc[current.grouped_by],
                    pages: [...acc[current.grouped_by].pages, current],
                },
            };
        } else {
            return {
                ...acc,
                [current.grouped_by]: {
                    blocked: current.blocked,
                    firstPage: current.page,
                    id: current.grouped_by,
                    pages: [current],
                },
            };
        }
    }, {});
};

export const createPage = ({
    groupId,
    pageModel,
    tempPage,
    templateId,
    widgetModel,
}) => {
    const {
        blocked = 0,
        content,
        created_at,
        custom_template,
        grouped_by,
        id,
        page,
        page_parent,
        company,
        updated_at,
    } = tempPage;
    const parsedData = JSON.parse(content);
    const [parsedDataKey = {}] = Object.keys(parsedData);
    const {
        infiniteMode = false,
        orientation = 'portrait',
        replaceInfo,
        theme = 'defaultTheme',
        values,
        widgets,
    } = parsedData[parsedDataKey];

    let tempFields = {
        blocked,
        content,
        created_at,
        custom_template,
        grouped_by,
        id,
        infiniteMode,
        orientation,
        page,
        page_parent,
        parsedDataKey,
        replaceInfo,
        templateId,
        theme,
        company,
        updated_at,
    };

    if (groupId) {
        tempFields.groupId = groupId;
    }

    if (templateId) {
        tempFields.templateId = templateId;
    }

    pageModel.create(tempFields);

    buildTemplateWidgets({
        pageId: id,
        values,
        widgetModel,
        widgets,
    });
};

export const getDefaultPosition = (
    height,
    width,
    defaultPosition,
    orientation,
    showGuides
) => {
    if (defaultPosition === null) {
        return {
            x: getMiddleX(showGuides, orientation, width),
            y: getMiddleY(showGuides, orientation, height),
        };
    }
    return {
        x: defaultPosition.x,
        y: defaultPosition.y,
    };
};

export const getDefaultWidgetPosition = (
    defaultValues,
    defaultPosition,
    orientation
) => {
    const { height, width } = defaultValues;
    return {
        ...defaultValues,
        ...getDefaultPosition(height, width, defaultPosition, orientation),
    };
};

export const getDefaultWidgetValues = (
    hasSummarySupport,
    onlineWidgets,
    orientation,
    proposalNumber,
    theme,
    type
) => {
    const { chartWidgets, tableWidgets } = onlineWidgets;
    const limitX = orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT;
    const limitY = orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH;
    switch (type) {
        case 'clip-art':
            return DEFAULT_WIDGET_VALUES.clipArt;
        case 'image':
            return DEFAULT_WIDGET_VALUES.image;
        case 'map':
            return {
                ...DEFAULT_WIDGET_VALUES.map,
                hasSummarySupport,
                value: {
                    ...DEFAULT_WIDGET_VALUES.map.value,
                    proposal_number: proposalNumber,
                },
            };
        case 'segments-map':
            return {
                ...DEFAULT_WIDGET_VALUES.segmentsMap,
                hasSummarySupport,
                value: {
                    ...DEFAULT_WIDGET_VALUES.segmentsMap.value,
                    proposal_number: proposalNumber,
                },
            };
        case 'text':
            return DEFAULT_WIDGET_VALUES.text;
        case 'separator':
            return DEFAULT_WIDGET_VALUES.separator;
        case 'signature':
            return DEFAULT_WIDGET_VALUES.signature;
        default:
            for (const { id, defaultValue, meta } of chartWidgets) {
                if (type === id) {
                    const {
                        freeGrid,
                        height,
                        replaceInfoRequired,
                        supportVersion = 1,
                        width,
                    } = meta;
                    const clonedDefaultValues = cloneDeep(defaultValue);
                    const realWidth =
                        supportVersion === 1 && !freeGrid
                            ? width * GRID_WIDTH
                            : width;
                    const realHeight =
                        supportVersion === 1 && !freeGrid
                            ? height * GRID_HEIGHT
                            : height;
                    return {
                        hasSummarySupport,
                        height: realHeight > limitY ? limitY : realHeight,
                        replaceInfoRequired: replaceInfoRequired,
                        style: {},
                        value: {
                            ...clonedDefaultValues,
                            colors: buildGraphsColors(clonedDefaultValues),
                            proposal_number: proposalNumber,
                        },
                        width: realWidth > limitX ? limitX : realWidth,
                    };
                }
            }

            for (const { id, defaultValue, meta } of tableWidgets) {
                if (type === id) {
                    const {
                        freeGrid,
                        height,
                        replaceInfoRequired,
                        supportVersion = 1,
                        width,
                    } = meta;
                    const realWidth =
                        supportVersion === 1 && !freeGrid
                            ? width * GRID_WIDTH
                            : width;
                    const realHeight =
                        supportVersion === 1 && !freeGrid
                            ? height * GRID_HEIGHT
                            : height;
                    return {
                        hasSummarySupport,
                        height: realHeight > limitY ? limitY : realHeight,
                        replaceInfoRequired: replaceInfoRequired,
                        style: get(TABLE_THEMES, `${theme}.tableStyle`, {}),
                        value: cloneDeep({
                            ...defaultValue,
                            proposal_number: proposalNumber,
                        }),
                        width: realWidth > limitX ? limitX : realWidth,
                    };
                }
            }
            return {};
    }
};

export const getFirstCatalogId = (catalogs) => {
    if (catalogs.length) {
        const catalog = catalogs.find((item) => item.order === 1);
        if (catalog) {
            return catalog.id;
        }
    }
    return '';
};

export const getLimits = (orientation) => {
    if (orientation === 'landscape') {
        return {
            x: PAGE_HEIGHT,
            y: PAGE_WIDTH,
        };
    }
    return {
        x: PAGE_WIDTH,
        y: PAGE_HEIGHT,
    };
};

const getMiddleX = (showGuides, orientation, width) => {
    const middle =
        ((orientation === 'portrait' ? PAGE_WIDTH : PAGE_HEIGHT) - width) / 2;
    if (showGuides) {
        return middle - (middle % GRID_WIDTH);
    }
    return middle;
};

const getMiddleY = (showGuides, orientation, height) => {
    const middle =
        ((orientation === 'portrait' ? PAGE_HEIGHT : PAGE_WIDTH) - height) / 2;
    if (showGuides) {
        return middle - (middle % GRID_HEIGHT);
    }
    return middle;
};

export const getNewOrderIndex = (currentIndex, moveType, listSize) => {
    switch (moveType) {
        case MOVE_BACK:
            if (currentIndex === 0) {
                return null;
            }
            return 0;
        case MOVE_FORWARD:
            if (currentIndex === listSize - 1) {
                return null;
            }
            return listSize - 1;
        case MOVE_STEP_BACK:
            if (currentIndex === 0) {
                return null;
            }
            return currentIndex - 1;
        case MOVE_STEP_FORWARD:
            if (currentIndex === listSize - 1) {
                return null;
            }
            return currentIndex + 1;
        default:
            return null;
    }
};

export const getNum = (type, array) => {
    const typeList = sortBy(
        array.filter((item) => item.name === type),
        [
            (p) => {
                return parseInt(p.num);
            },
        ]
    );
    if (typeList.length === 0) {
        return 1;
    }
    return parseInt(typeList[typeList.length - 1].num) + 1;
};

export const getPageUpdatingContentData = (page) => {
    if (page.id === '') {
        return page;
    }
    let tempValues = {};
    let tempWidgets = [];
    let replaceInfo = [];
    page.widgets.forEach((widget) => {
        const {
            replaceInfoData,
            value,
            widget: widgetData,
        } = getWidgetUpdatingContentData(widget);
        tempValues = { ...tempValues, ...value };
        tempWidgets = [...tempWidgets, widgetData];
        if (replaceInfoData) {
            replaceInfo = [...replaceInfo, replaceInfoData];
        }
    });

    const content = JSON.stringify({
        [page.parsedDataKey]: {
            theme: page.theme,
            orientation: page.orientation,
            infiniteMode: page.infiniteMode,
            replaceInfo: replaceInfo,
            values: tempValues,
            widgets: tempWidgets,
        },
    });
    return {
        blocked: page.blocked,
        company: page.company,
        content,
        custom_template: page.custom_template,
        grouped_by: page.grouped_by,
        id: page.id,
        page: page.page,
        page_parent: page.page_parent,
    };
};

export const getUpdatingContentData = (templateData) => {
    const modifiedTemplateData = {
        company: templateData.company,
        created_at: templateData.created_at,
        description: templateData.description,
        footer: templateData.footer,
        id: templateData.id,
        logo: templateData.logo,
        parent: templateData.parent,
        primary_color: templateData.primary_color,
        secondary_color: templateData.secondary_color,
        title: templateData.title,
        typography: templateData.typography,
        updated_at: templateData.updated_at,
        version: templateData.version,
        pages_template: getPagesTemplate(templateData.pages),
    };
    return modifiedTemplateData;
};

export const orderPages = (pagesList) => {
    let indexPage = 0;
    return pagesList.map((item) => {
        if (item.type === 'group') {
            const firstPage = `${indexPage + 1}`;
            const newPages = item.pages.map((element) => {
                ++indexPage;
                return {
                    ...element,
                    page: `${indexPage}`,
                };
            });
            return { ...item, page: firstPage, pages: newPages };
        }
        ++indexPage;
        return { ...item, page: `${indexPage}` };
    });
};
