import arrayMove from 'array-move';
import sortBy from 'lodash/sortBy';

import * as templateCoreSelectors from '@templateCore/selectors';

import { orderPages } from '../helpers';

import addNewPages from './addNewPages';

export default (pageId) => (dispatch, getState) => {
    const updateData = templateCoreSelectors.getTemplateUpdatingContentData(
        getState()
    );
    const indexOfPageId = updateData.pages_template
        .map((p) => p.id)
        .indexOf(pageId);
    if (indexOfPageId >= 0) {
        const duplicatedPage = {
            ...updateData.pages_template[indexOfPageId],
            id: `PageCopy-${new Date().getTime()}`,
        };
        let pagesTemplate = orderPages(
            arrayMove(
                [
                    ...sortBy(updateData.pages_template, [
                        (w) => {
                            return parseInt(w.page);
                        },
                    ]),
                    duplicatedPage,
                ],
                updateData.pages_template.length,
                parseInt(duplicatedPage.page)
            )
        );
        dispatch(addNewPages(pagesTemplate));
    }
};
