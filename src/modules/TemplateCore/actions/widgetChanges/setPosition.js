import { CHANGE_WIDGET_POSITION } from '../../actionTypes';

export default (widgetId, changedX, changedY, onChangeInPage) => (dispatch) => {
    if (!widgetId) {
        return;
    }
    dispatch({
        type: CHANGE_WIDGET_POSITION,
        payload: { widgetId, changedX, changedY },
    });
    onChangeInPage();
};
