import { CHANGE_WIDGET_VALUE } from '../../actionTypes';

export default (widgetId, value, onChangeInPage) => (dispatch) => {
    if (!widgetId) {
        return;
    }
    dispatch({
        type: CHANGE_WIDGET_VALUE,
        payload: { widgetId, value },
    });
    onChangeInPage();
};
