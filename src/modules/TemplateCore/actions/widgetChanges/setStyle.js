import { CHANGE_WIDGET_STYLE } from '../../actionTypes';

export default (widgetId, style, onChangeInPage) => (dispatch) => {
    if (!widgetId) {
        return;
    }
    dispatch({
        type: CHANGE_WIDGET_STYLE,
        payload: { widgetId, style },
    });
    onChangeInPage();
};
