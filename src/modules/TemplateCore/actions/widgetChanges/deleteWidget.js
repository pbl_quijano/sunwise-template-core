import selectWidget from '@modules/TemplateView/actions/selectWidget';

import { DELETE_WIDGET } from '../../actionTypes';

export default (widgetId, onChangeInPage) => (dispatch) => {
    if (!widgetId) {
        return;
    }
    dispatch({
        type: DELETE_WIDGET,
        payload: widgetId,
    });
    dispatch(selectWidget(null));
    onChangeInPage();
};
