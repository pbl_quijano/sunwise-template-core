import { CHANGE_WIDGET_SIZE } from '../../actionTypes';

export default (widgetId, changedWidth, changedHeight, onChangeInPage) =>
    (dispatch) => {
        if (!widgetId) {
            return;
        }
        dispatch({
            type: CHANGE_WIDGET_SIZE,
            payload: { widgetId, changedWidth, changedHeight },
        });
        onChangeInPage();
    };
