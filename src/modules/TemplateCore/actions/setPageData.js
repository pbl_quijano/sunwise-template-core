import { SET_PAGE_DATA } from '../actionTypes';

export default (pageData) => (dispatch) => {
    dispatch({ type: SET_PAGE_DATA, payload: pageData });
};
