import { RESET_TEMPLATE } from '../actionTypes';

export default () => ({ type: RESET_TEMPLATE });
