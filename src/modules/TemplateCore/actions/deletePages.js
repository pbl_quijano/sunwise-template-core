import selectPage from '@modules/TemplateView/actions/selectPage';
import * as templateViewSelectors from '@modules/TemplateView/selectors';

import * as templateCoreSelectors from '@templateCore/selectors';

import { DELETE_PAGES } from '../actionTypes';

export default (deletingId, pagesTemplate) => (dispatch, getState) => {
    const state = getState();
    const selectedPage = templateViewSelectors.getSelectedPage(state);
    if (deletingId === selectedPage.groupId || deletingId === selectedPage.id) {
        const currentPages =
            templateCoreSelectors.getCurrentTemplatePages(state);
        const pagesIds = currentPages.reduce((acc, current) => {
            if (current.type === 'group') {
                return [...acc, ...current.pages.map((p) => p.id)];
            }
            return [...acc, current.id];
        }, []);
        let selectedPageIndex = 0;
        if (selectedPage.groupId) {
            let selectedGroupIndex = currentPages
                .map((p) => p.id)
                .indexOf(selectedPage.groupId);
            if (selectedGroupIndex === 0) {
                selectedPageIndex = 0;
            } else {
                selectedPageIndex = pagesIds.indexOf(
                    currentPages[selectedGroupIndex].pages[0].id
                );
            }
        } else {
            selectedPageIndex = pagesIds.indexOf(selectedPage.id);
        }
        if (selectedPageIndex === 0) {
            if (!selectedPage.groupId && pagesIds.length > 1) {
                dispatch(selectPage(pagesIds[1]));
            } else {
                dispatch(selectPage(null));
            }
        } else {
            dispatch(selectPage(pagesIds[selectedPageIndex - 1]));
        }
    }
    dispatch({
        type: DELETE_PAGES,
        payload: pagesTemplate,
    });
};
