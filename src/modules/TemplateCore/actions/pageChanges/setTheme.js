import { CHANGE_PAGE_THEME } from '../../actionTypes';

export default (pageId, theme, onChangeInPage) => (dispatch) => {
    if (!pageId) {
        return;
    }
    dispatch({
        type: CHANGE_PAGE_THEME,
        payload: { pageId, theme },
    });
    onChangeInPage();
};
