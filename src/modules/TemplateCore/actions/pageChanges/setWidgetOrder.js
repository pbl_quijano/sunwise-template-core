import { CHANGE_WIDGET_ORDER } from '../../actionTypes';
import { getNewOrderIndex } from '../../helpers';
import * as selectors from '../../selectors';

export default (pageId, widgetId, moveType, onChangeInPage) =>
    (dispatch, getState) => {
        const state = getState();
        const { Page } = selectors.getEntitiesSession(state);
        if (!pageId || !Page.idExists(pageId)) {
            return;
        }
        const pageModel = Page.withId(pageId);
        const pageWidgets = pageModel.widgets.toRefArray();
        const widgetIndex = pageWidgets.map((w) => w.id).indexOf(widgetId);
        const newOrderIndex = getNewOrderIndex(
            widgetIndex,
            moveType,
            pageWidgets.length
        );

        if (newOrderIndex !== null) {
            dispatch({
                type: CHANGE_WIDGET_ORDER,
                payload: { pageId, widgetIndex, newOrderIndex },
            });
            onChangeInPage();
        }
    };
