import { CHANGE_PAGE_ORIENTATION } from '../../actionTypes';
export default (pageId, orientation, onChangeInPage) => (dispatch) => {
    if (!pageId) {
        return;
    }
    dispatch({
        type: CHANGE_PAGE_ORIENTATION,
        payload: { pageId, orientation },
    });
    onChangeInPage();
};
