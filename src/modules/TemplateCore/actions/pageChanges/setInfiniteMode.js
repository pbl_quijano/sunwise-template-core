import { CHANGE_PAGE_INFINITE_MODE } from '../../actionTypes';
export default (pageId, infiniteMode, onChangeInPage) => (dispatch) => {
    if (!pageId) {
        return;
    }
    dispatch({
        type: CHANGE_PAGE_INFINITE_MODE,
        payload: { pageId, infiniteMode },
    });
    onChangeInPage();
};
