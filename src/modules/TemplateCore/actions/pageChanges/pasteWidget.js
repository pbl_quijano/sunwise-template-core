import cloneDeep from 'lodash/cloneDeep';

import selectWidget from '@modules/TemplateView/actions/selectWidget';

import { PASTE_WIDGET } from '../../actionTypes';
import { getDefaultPosition, getNum } from '../../helpers';
import * as selectors from '../../selectors';

export default (
        pageId,
        optionsRef,
        showGuides,
        position = null,
        hasSummarySupport,
        onChangeInPage
    ) =>
    (dispatch, getState) => {
        const state = getState();
        const { Page } = selectors.getEntitiesSession(state);
        if (!pageId || !Page.idExists(pageId)) {
            return;
        }
        const pageModel = Page.withId(pageId);
        const pageData = pageModel.ref;
        const pageWidgets = pageModel.widgets.toRefArray();
        const {
            name,
            height,
            replaceInfoRequired,
            style,
            supportVersion = 2,
            value,
            width,
        } = cloneDeep(optionsRef);
        const newNum = getNum(name, pageWidgets);
        const newName = `${name}-${newNum}`;
        const { x, y } = getDefaultPosition(
            height,
            width,
            position,
            pageData.orientation,
            showGuides
        );
        const newWidget = {
            id: `${pageId}/${newName}`,
            field: newName,
            hasSummarySupport,
            height,
            name,
            num: newNum,
            order: pageWidgets.length,
            pageId,
            replaceInfoRequired,
            style,
            supportVersion,
            value,
            width,
            x,
            y,
        };
        dispatch({
            type: PASTE_WIDGET,
            payload: newWidget,
        });
        dispatch(selectWidget(newWidget.id));

        onChangeInPage();
    };
