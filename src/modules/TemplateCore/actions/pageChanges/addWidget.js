import { WIDGET_SUPPORT_VERSION } from '@constants/template';

import * as mainSelectors from '@main/selectors';

import selectWidget from '@modules/TemplateView/actions/selectWidget';

import { ADD_WIDGET } from '../../actionTypes';
import {
    getDefaultWidgetPosition,
    getDefaultWidgetValues,
    getFirstCatalogId,
    getNum,
} from '../../helpers';
import * as selectors from '../../selectors';

export default (
        pageId,
        type,
        defaultPosition,
        hasSummarySupport,
        onChangeInPage
    ) =>
    (dispatch, getState) => {
        const state = getState();
        const { Page } = selectors.getEntitiesSession(state);
        if (!pageId || !Page.idExists(pageId)) {
            return;
        }
        const pageModel = Page.withId(pageId);
        const pageData = pageModel.ref;
        const pageWidgets = pageModel.widgets.toRefArray();
        const chartWidgets = mainSelectors.getDataFetchChartWidgets(state);
        const tableWidgets = mainSelectors.getDataFetchTableWidgets(state);
        const catalogs = mainSelectors.getFetchCatalogData(state);

        const newNum = getNum(type, pageWidgets);
        const newName = `${type}-${newNum}`;
        const {
            hasSummarySupport: hasSummarySupportWidget,
            height,
            replaceInfoRequired,
            style,
            value,
            width,
            x,
            y,
        } = getDefaultWidgetPosition(
            getDefaultWidgetValues(
                hasSummarySupport,
                { chartWidgets, tableWidgets },
                pageData.orientation,
                hasSummarySupport ? getFirstCatalogId(catalogs) : '',
                pageData.theme,
                type
            ),
            defaultPosition,
            pageData.orientation
        );
        const newWidget = {
            id: `${pageId}/${newName}`,
            field: newName,
            hasSummarySupport: hasSummarySupportWidget,
            height,
            name: type,
            num: newNum,
            order: pageWidgets.length,
            pageId,
            replaceInfoRequired,
            style,
            supportVersion: WIDGET_SUPPORT_VERSION,
            value,
            width,
            x,
            y,
        };

        dispatch({
            type: ADD_WIDGET,
            payload: newWidget,
        });
        dispatch(selectWidget(newWidget.id));
        onChangeInPage();
    };
