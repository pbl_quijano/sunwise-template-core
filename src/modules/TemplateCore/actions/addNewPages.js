import selectPage from '@modules/TemplateView/actions/selectPage';
import * as templateViewSelectors from '@modules/TemplateView/selectors';

import { ADD_NEW_PAGES } from '../actionTypes';

export default (pagesTemplate) => (dispatch, getState) => {
    dispatch({
        type: ADD_NEW_PAGES,
        payload: pagesTemplate,
    });
    const selectedPageFromAdd = templateViewSelectors.getSelectedPage(
        getState()
    );
    if (selectedPageFromAdd === null && pagesTemplate.length > 0) {
        dispatch(selectPage(pagesTemplate[0].id));
    }
};
