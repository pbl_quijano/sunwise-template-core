import arrayMove from 'array-move';

import * as templateCoreSelectors from '@templateCore/selectors';

import { RE_ORDER_PAGES } from '../actionTypes';
import { orderPages } from '../helpers';

export default (oldIndex, newIndex) => (dispatch, getState) => {
    const currentPages = templateCoreSelectors.getCurrentTemplatePages(
        getState()
    );
    const newPages = orderPages(arrayMove(currentPages, oldIndex, newIndex));
    dispatch({
        type: RE_ORDER_PAGES,
        payload: newPages,
    });
};
