import { SET_TEMPLATE_BACKUP } from '../actionTypes';
import * as selectors from '../selectors';

export default (clean) => (dispatch, getState) => {
    if (clean) {
        dispatch({
            type: SET_TEMPLATE_BACKUP,
            payload: null,
        });
    } else {
        const currentPages = selectors.getCurrentTemplatePages(getState());
        dispatch({
            type: SET_TEMPLATE_BACKUP,
            payload: JSON.stringify(
                currentPages.filter((p) => p.type === 'page')
            ),
        });
    }
};
