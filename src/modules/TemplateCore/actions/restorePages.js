import { RESTORE_REVIEW_PAGES } from '../actionTypes';

export default () => (dispatch) => {
    dispatch({
        type: RESTORE_REVIEW_PAGES,
    });
};
