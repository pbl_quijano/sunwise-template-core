import {
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SOLAR_MODULE_COLOR,
    SOLAR_MODULE_STROKE_COLOR,
} from '@constants/maps';

export const DEFAULT_WIDGET_VALUES = {
    clipArt: {
        hasSummarySupport: false,
        height: 100,
        replaceInfoRequired: null,
        style: {
            type: 'circle',
            background: '#000000',
        },
        value: null,
        width: 100,
    },
    image: {
        hasSummarySupport: false,
        height: 82.5,
        replaceInfoRequired: null,
        style: {
            objectFit: 'cover',
            opacity: 1,
        },
        value: null,
        width: 165.3,
    },
    map: {
        hasSummarySupport: false,
        height: 82.5,
        replaceInfoRequired: 'map',
        style: {},
        value: {
            currentPosition: {
                lat: 20.9746787,
                lng: -89.632493,
                zoom: 13,
            },
            mapType: 'satellite',
            projectPosition: {},
            proposal_number: '',
        },
        width: 165.3,
    },
    segmentsMap: {
        hasSummarySupport: false,
        height: 82.5,
        replaceInfoRequired: 'segments-map',
        style: {
            solarModule: {
                fillColor: SOLAR_MODULE_COLOR,
                strokeColor: SOLAR_MODULE_STROKE_COLOR,
                strokeWeight: 1,
            },
            segment: {
                fillColor: SEGMENT_COLOR,
                strokeColor: SEGMENT_STROKE_COLOR,
                strokeWeight: 4,
            },
            showLabels: true,
            showProjectMarker: true,
        },
        value: {
            currentPosition: {
                lat: 20.9746787,
                lng: -89.632493,
                zoom: 13,
            },
            projectPosition: {},
            proposal_number: '',
        },
        width: 165.3,
    },
    separator: {
        hasSummarySupport: false,
        height: 1,
        replaceInfoRequired: null,
        style: {
            borderBottomColor: '#000000',
            borderBottomStyle: 'solid',
        },
        value: null,
        width: 100,
    },
    signature: {
        hasSummarySupport: false,
        height: 73,
        replaceInfoRequired: 'signature',
        style: {},
        value: '[]',
        width: 226,
    },
    text: {
        hasSummarySupport: false,
        height: 99,
        replaceInfoRequired: null,
        style: {},
        value: '',
        width: 330.6,
    },
};

export const TABLE_THEMES = {
    defaultTheme: {
        name: 'Default',
        tableStyle: {
            table: {
                borderWidth: '1px',
                borderColor: '#000000',
                borderStyle: 'solid',
            },
            header: {
                backgroundColor: '#000000',
                color: '#ffffff',
                fontBold: true,
                fontItalic: false,
                roundedBorders: false,
            },
            body: {
                backgroundColor1: '#fff',
                backgroundColor2: '#ccc',
                color: '#000',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: false,
            },
            footer: {
                borderTopWidth: '1px',
                borderTopColor: '#000000',
                borderTopStyle: 'solid',
                backgroundColor1: '#fff',
                backgroundColor2: '#ccc',
                color: '#000',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: false,
            },
        },
    },
    simpleTheme: {
        name: 'Simple',
        tableStyle: {
            table: {
                borderWidth: '1px',
                borderColor: '#000000',
                borderStyle: 'hidden',
            },
            header: {
                backgroundColor: '#ffffff',
                color: '#4E4B49',
                fontBold: true,
                fontItalic: false,
                roundedBorders: false,
            },
            body: {
                backgroundColor1: '#fff',
                backgroundColor2: '#F7F7F7',
                color: '#898A91',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: true,
                roundedBorders: true,
            },
            footer: {
                borderTopWidth: '1px',
                borderTopColor: '#FED871',
                borderTopStyle: 'solid',
                backgroundColor1: '#fff',
                backgroundColor2: '#ccc',
                color: '#898A91',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: false,
            },
        },
    },
    orangeTheme: {
        name: 'Orange',
        tableStyle: {
            table: {
                borderWidth: '1px',
                borderColor: '#000000',
                borderStyle: 'hidden',
            },
            header: {
                backgroundColor: '#FF5733',
                color: '#ffffff',
                fontBold: true,
                fontItalic: false,
                roundedBorders: true,
            },
            body: {
                backgroundColor1: '#fff',
                backgroundColor2: '#f7f7f7',
                color: '#000',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: true,
            },
            footer: {
                borderTopWidth: '1px',
                borderTopColor: '#000000',
                borderTopStyle: 'hidden',
                backgroundColor1: '#fff',
                backgroundColor2: '#ccc',
                color: '#000',
                fontBold: false,
                fontItalic: false,
                isZebraStyle: false,
            },
        },
    },
};

export const NEW_PAGE_CONTENT =
    '{"Page1":{"theme":"defaultTheme","values":{},"widgets":[], "replaceInfo":[]}}';
