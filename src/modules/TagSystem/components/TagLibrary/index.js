import PropTypes from 'prop-types';
import { Container as BootstrapContainer } from 'react-bootstrap';

import {
    EDIT_LIBRARY_TAG_STATE,
    INSERT_LIBRARY_TAG_STATE,
} from '@constants/tags';
import { SMART_DOCUMENTS_TYPE } from '@constants/types';

import { handleSubmitTagLibraryBuild } from '../../helpers';

import TagForm from './TagForm';
import TagLibraryView from './TagLibraryView';

const TagLibrary = ({
    catalogs,
    tagsLocale,
    contentViewState,
    editionLevel,
    handleEditTag,
    handleInsertTag,
    hasProposalSelector,
    isFromCustomTag,
    isInitialazing,
    prepareCustomTagFromSelectTag,
    templateType,
}) => {
    const handleOnSubmit = handleSubmitTagLibraryBuild(
        tagsLocale,
        isFromCustomTag,
        contentViewState,
        handleEditTag,
        handleInsertTag,
        prepareCustomTagFromSelectTag
    );
    return (
        <BootstrapContainer fluid>
            {(isFromCustomTag ||
                contentViewState === INSERT_LIBRARY_TAG_STATE) && (
                <TagLibraryView
                    catalogs={catalogs}
                    tagsLocale={tagsLocale}
                    editionLevel={editionLevel}
                    handleOnSubmit={handleOnSubmit}
                    hasProposalSelector={hasProposalSelector}
                    isForSmartDocuments={templateType === SMART_DOCUMENTS_TYPE}
                    isFromCustomTag={isFromCustomTag}
                    isInitialazing={isInitialazing}
                />
            )}
            {contentViewState === EDIT_LIBRARY_TAG_STATE && (
                <TagForm
                    catalogs={catalogs}
                    tagsLocale={tagsLocale}
                    editionLevel={editionLevel}
                    handleOnSubmit={handleOnSubmit}
                    hasProposalSelector={hasProposalSelector}
                    isInitialazing={isInitialazing}
                />
            )}
        </BootstrapContainer>
    );
};

TagLibrary.propTypes = {
    catalogs: PropTypes.array,
    tagsLocale: PropTypes.string,
    contentViewState: PropTypes.number,
    editionLevel: PropTypes.string,
    handleEditTag: PropTypes.func,
    handleInsertTag: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    isFromCustomTag: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    prepareCustomTagFromSelectTag: PropTypes.func,
    reset: PropTypes.func,
    templateType: PropTypes.number,
};

export default TagLibrary;
