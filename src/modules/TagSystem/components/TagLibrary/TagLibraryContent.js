import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ButtonLink from '@components/ButtonLink';

import { CategoryTitle, StyledText } from '../styledComponents';

import CategoryList from './CategoryList';
import TagForm from './TagForm';
import TagList from './TagList';

const TagLibraryContent = ({
    catalogs,
    tagsLocale,
    editionLevel,
    filteredTags,
    handleOnSubmit,
    handleSelectCategory,
    handleSelectTag,
    hasProposalSelector,
    isForSmartDocuments,
    isFromCustomTag,
    isInitialazing,
    selectedCategory,
    selectedTag,
}) => {
    const { t } = useTranslation();
    const tagsDiv = useRef(null);

    useEffect(() => {
        tagsDiv.current.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
    }, [selectedCategory]);

    return (
        <Row className="tags-container">
            <Col xs="3" className="pl-0 pr-0 tags-column border-left">
                <CategoryTitle className="mt-2 p-1 pl-3">
                    {t('Categories')}
                </CategoryTitle>
                <CategoryList
                    filteredTags={filteredTags}
                    handleSelectCategory={handleSelectCategory}
                    isForSmartDocuments={isForSmartDocuments}
                    selectedCategory={selectedCategory}
                />
                <ButtonLink
                    as="a"
                    className="mt-2 text-left"
                    fontSize="13px"
                    href="https://academy.sunwise.mx/es/articles/4220938-etiquetas-inteligentes"
                    iconConfig={{
                        prepend: {
                            bgColor: '#FF9A00',
                            icon: 'fa fa-eye',
                            iconColor: '#FFFFFF',
                            size: 'md',
                        },
                    }}
                    target="_blank"
                    textColor="#2F4DFF"
                >
                    {t('Tag manuals')}
                </ButtonLink>
            </Col>

            <Col
                xs="4"
                className="pl-0 pr-0 tags-column border-left"
                ref={tagsDiv}
            >
                <TagList
                    filteredTags={filteredTags}
                    handleSelectTag={handleSelectTag}
                    isFromCustomTag={isFromCustomTag}
                    selectedCategory={selectedCategory}
                    selectedTag={selectedTag}
                />
            </Col>

            <Col xs="11" className="tags-column border-left">
                <div className="d-flex flex-column">
                    {!selectedTag && (
                        <Row className="m-0 mt-3">
                            <Col>
                                <StyledText>
                                    {t('Select a tag to configure')}
                                </StyledText>
                            </Col>
                        </Row>
                    )}
                    {selectedTag && (
                        <TagForm
                            catalogs={catalogs}
                            tagsLocale={tagsLocale}
                            editionLevel={editionLevel}
                            handleOnSubmit={handleOnSubmit}
                            hasFilterByActive
                            hasProposalSelector={hasProposalSelector}
                            isFromCustomTag={isFromCustomTag}
                            isInitialazing={isInitialazing}
                            isInsertTagMode
                        />
                    )}
                </div>
            </Col>
        </Row>
    );
};

TagLibraryContent.propTypes = {
    catalogs: PropTypes.array,
    tagsLocale: PropTypes.string,
    editionLevel: PropTypes.string,
    filteredTags: PropTypes.object,
    handleOnSubmit: PropTypes.func,
    handleSelectCategory: PropTypes.func,
    handleSelectTag: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    isForSmartDocuments: PropTypes.bool,
    isFromCustomTag: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    selectCategory: PropTypes.func,
    selectedCategory: PropTypes.string,
    selectedTag: PropTypes.string,
    selectTag: PropTypes.func,
};

export default TagLibraryContent;
