import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ReduxFieldInput from '@components/ReduxFieldInput';

import { StyledTitleField, TagTitle } from '../styledComponents';

const FormTitle = ({ formValuesTitle, initialValuesTitle }) => {
    const { t } = useTranslation();
    if (isEmpty(initialValuesTitle)) {
        return (
            <Form.Group className="mb-0">
                <StyledTitleField
                    component={ReduxFieldInput}
                    name="title"
                    placeholder={t('Tag title')}
                    type="text"
                />
            </Form.Group>
        );
    }
    return (
        <>
            <TagTitle>{formValuesTitle}</TagTitle>
            <hr />
        </>
    );
};

FormTitle.propTypes = {
    formValuesTitle: PropTypes.string,
    initialValuesTitle: PropTypes.string,
};

export default FormTitle;
