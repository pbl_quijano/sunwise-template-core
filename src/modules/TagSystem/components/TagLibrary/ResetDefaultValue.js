import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import { ReactComponent as RefreshIcon } from '@res/icons/refresh.svg';

import { ButtonReset } from '../styledComponents';

const ResetDefaultValue = ({
    defaultValue,
    resetToDefaultValue,
    showButtonReset,
    value,
}) => {
    const { t } = useTranslation();
    if (value != defaultValue && showButtonReset) {
        return (
            <Col className="text-right">
                <ButtonReset
                    onClick={() => resetToDefaultValue()}
                    type="button"
                >
                    <img alt="reset" className="mr-2" src={RefreshIcon} />
                    {t('Reset to default')}
                </ButtonReset>
            </Col>
        );
    }
    return null;
};

ResetDefaultValue.propTypes = {
    defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    resetToDefaultValue: PropTypes.func,
    showButtonReset: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

export default ResetDefaultValue;
