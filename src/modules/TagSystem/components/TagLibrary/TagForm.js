import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useEffect } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { change, reduxForm } from 'redux-form';
import { createStructuredSelector } from 'reselect';

import ReduxFieldInput from '@components/ReduxFieldInput';
import ReduxFieldSelect from '@components/ReduxFieldSelect';

import {
    DOCUMENT_TAGS,
    TAG_CURRENCY_FORMAT_TYPES,
    TAG_ECOLOGIC_TYPES,
    TAG_NUMBER_FORMAT_TYPES,
    TAG_PERCENT_FORMAT_TYPES,
    TAG_TEXT_FORMAT_TYPES,
} from '@constants/tags';
import { PARTIAL_EDITION_MODE } from '@constants/types';

import { ReactComponent as SettingsIcon } from '@res/icons/settings.svg';

import * as actions from '../../actions';
import {
    getBorderStyle,
    getColSize,
    getDecimalFormatsToSelect,
    getDocumentsToSelect,
    getFormattedValue,
    getPropsValueField,
    getTagFormatsToSelect,
    getTagLibraryTextButton,
    getUnitsToSelect,
    normalizeCurrencyField,
    normalizeDecimalField,
    normalizeDocumentField,
} from '../../helpers';
import * as selectors from '../../selectors';
import validate from '../../tagLibraryValidate';
import {
    ButtonInsert,
    Caption,
    FormLabel,
    SettingsBox,
    StyledAlert,
    StyledField,
    TagLabel,
} from '../styledComponents';
import WrapperField from '../WrapperField';

import FormTitle from './FormTitle';
import ProposalSelector from './ProposalSelector';
import ResetDefaultValue from './ResetDefaultValue';

const TagForm = ({
    catalogs,
    changeInput,
    tagsLocale,
    currencies,
    decimalFormatsData,
    documents,
    editionLevel,
    errors,
    fetchDocuments,
    formValues,
    handleOnSubmit,
    handleSubmit,
    hasFilterByActive,
    hasProposalSelector,
    initialValues,
    isFetchingDocuments,
    isInsertTagMode,
    isFromCustomTag,
    isInitialazing,
    tagFormatsData,
    unitFormatsData,
}) => {
    const { t } = useTranslation();
    const borderStyle = getBorderStyle(isInsertTagMode, isFromCustomTag);
    const formatType = get(formValues, 'format_type', '');
    const catalogType = get(formValues, 'catalog_type', '');
    const isForDocument = DOCUMENT_TAGS.includes(formatType);

    useEffect(() => {
        if (!isEmpty(catalogType)) {
            fetchDocuments(catalogType);
        }
    }, [catalogType]);

    const fieldsDisabled = editionLevel === PARTIAL_EDITION_MODE;
    return (
        <Form onSubmit={handleSubmit(handleOnSubmit)}>
            <SettingsBox className="mt-3" style={borderStyle}>
                <ProposalSelector
                    catalogs={catalogs}
                    disabled={fieldsDisabled}
                    visible={
                        hasProposalSelector &&
                        !DOCUMENT_TAGS.includes(formatType)
                    }
                />
                <Row className="mt-2">
                    <Col>
                        <FormTitle
                            formValuesTitle={formValues.title}
                            initialValuesTitle={initialValues.title}
                        />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Caption className="mt-3">{t('Description')}</Caption>
                        <p>{formValues.description}</p>
                    </Col>
                </Row>
                <SettingsBox className="mt-3" style={borderStyle}>
                    <Row className="align-items-center">
                        <Col>
                            <Caption>
                                <SettingsIcon
                                    width="17"
                                    height="17"
                                    className="mr-2"
                                />
                                {t('Setting', { count: 2 })}
                            </Caption>
                        </Col>
                        <ResetDefaultValue
                            defaultValue={formValues.default_value}
                            resetToDefaultValue={() =>
                                changeInput('value', formValues.default_value)
                            }
                            showButtonReset={fieldsDisabled}
                            value={formValues.value}
                        />
                    </Row>
                    <hr />
                    <Row className="mt-2">
                        <WrapperField visible={!isForDocument}>
                            <Col xs={getColSize(formatType, 6, 3)}>
                                <Form.Group className="mb-0">
                                    <FormLabel>{t('Value')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldInput}
                                        name="value"
                                        type="string"
                                        {...getPropsValueField(formatType)}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField
                            visible={TAG_CURRENCY_FORMAT_TYPES.includes(
                                formatType
                            )}
                        >
                            <Col xs="5">
                                <Form.Group>
                                    <FormLabel>{t('Currency')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="currency"
                                        normalize={normalizeCurrencyField(
                                            changeInput,
                                            currencies,
                                            ['currencyLocale', 'unit', 'symbol']
                                        )}
                                        options={[
                                            {
                                                label: t('Select currency'),
                                                value: '',
                                                disabled: true,
                                            },
                                            ...currencies.map((item) => ({
                                                label: item.name,
                                                value: item.id,
                                            })),
                                        ]}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField visible={isForDocument}>
                            <Col xs="6">
                                <Form.Group>
                                    <FormLabel>{t('Catalog type')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="catalog_type"
                                        normalize={(value) => {
                                            changeInput('smart_tag', '');
                                            fetchDocuments(value);
                                            return value;
                                        }}
                                        options={getUnitsToSelect(
                                            currencies,
                                            formatType,
                                            isInitialazing,
                                            unitFormatsData
                                        )}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField visible={isForDocument}>
                            <Col xs="6">
                                <Form.Group>
                                    <FormLabel>{t('Document')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="smart_tag"
                                        normalize={normalizeDocumentField(
                                            changeInput,
                                            documents
                                        )}
                                        options={getDocumentsToSelect(
                                            documents,
                                            hasFilterByActive,
                                            isFetchingDocuments
                                        )}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField
                            visible={
                                TAG_NUMBER_FORMAT_TYPES.includes(formatType) ||
                                TAG_PERCENT_FORMAT_TYPES.includes(formatType)
                            }
                        >
                            <Col xs="5">
                                <Form.Group>
                                    <FormLabel>{t('Unit')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="unit"
                                        options={getUnitsToSelect(
                                            currencies,
                                            formatType,
                                            isInitialazing,
                                            unitFormatsData
                                        )}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField
                            visible={
                                TAG_CURRENCY_FORMAT_TYPES.includes(
                                    formatType
                                ) ||
                                TAG_ECOLOGIC_TYPES.includes(formatType) ||
                                TAG_NUMBER_FORMAT_TYPES.includes(formatType) ||
                                TAG_PERCENT_FORMAT_TYPES.includes(formatType)
                            }
                        >
                            <Col xs="5">
                                <Form.Group>
                                    <FormLabel>{t('Decimals')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="decimal_type"
                                        normalize={normalizeDecimalField(
                                            changeInput,
                                            decimalFormatsData,
                                            'decimal_places'
                                        )}
                                        options={getDecimalFormatsToSelect(
                                            decimalFormatsData,
                                            isInitialazing
                                        )}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>

                        <WrapperField
                            visible={TAG_TEXT_FORMAT_TYPES.includes(formatType)}
                        >
                            <Col xs={getColSize(formatType, 6, 5)}>
                                <Form.Group>
                                    <FormLabel>{t('Format')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldSelect}
                                        disabled={fieldsDisabled}
                                        name="format"
                                        options={getTagFormatsToSelect(
                                            isInitialazing,
                                            tagFormatsData,
                                            formatType
                                        )}
                                    />
                                </Form.Group>
                            </Col>
                        </WrapperField>
                    </Row>
                    <Row>
                        <Col>
                            <Caption className="mt-3">
                                {t('Default value')}
                            </Caption>
                            {formValues.value && (
                                <TagLabel background={formValues.color}>
                                    {getFormattedValue(
                                        false,
                                        tagsLocale,
                                        formValues
                                    )}
                                </TagLabel>
                            )}
                        </Col>
                    </Row>
                </SettingsBox>

                {!isEmpty(errors) && (
                    <Row className="mt-3">
                        <Col>
                            <StyledAlert variant="warning">
                                {t('You need to capture all fields')}
                            </StyledAlert>
                        </Col>
                    </Row>
                )}

                <Row className="mt-3 justify-content-center">
                    <Col xs="10">
                        <ButtonInsert size="small" type="submit">
                            <i className="fas fa-arrow-right mr-2"></i>
                            {getTagLibraryTextButton(
                                isInsertTagMode,
                                isFromCustomTag
                            )}
                        </ButtonInsert>
                    </Col>
                </Row>
            </SettingsBox>
        </Form>
    );
};

const mapStateToProps = createStructuredSelector({
    currencies: selectors.getCurrenciesData,
    decimalFormatsData: selectors.getDecimalFormatsData,
    errors: selectors.getTagLibraryFormErrors,
    documents: selectors.getDocumentsData,
    formValues: selectors.getTagLibraryValues,
    isFetchingDocuments: selectors.getIsFetchingDocuments,
    initialValues: selectors.getInitialTagLibraryValues,
    tagFormatsData: selectors.getTagFormatsData,
    unitFormatsData: selectors.getUnitFormatsData,
});

const mapDispatchToProps = (dispatch) => ({
    changeInput: (field, value) =>
        dispatch(change('tag-library-form', field, value)),
    fetchDocuments: (type) => dispatch(actions.fetchDocuments(type)),
});

const FormContainer = reduxForm({
    enableReinitialize: true,
    form: 'tag-library-form',
    validate,
})(TagForm);

TagForm.propTypes = {
    catalogs: PropTypes.array,
    changeInput: PropTypes.func,
    tagsLocale: PropTypes.string,
    currencies: PropTypes.array,
    decimalFormatsData: PropTypes.array,
    documents: PropTypes.array,
    editionLevel: PropTypes.string,
    errors: PropTypes.object,
    fetchDocuments: PropTypes.func,
    formValues: PropTypes.object,
    handleOnSubmit: PropTypes.func,
    handleSubmit: PropTypes.func,
    hasFilterByActive: PropTypes.bool,
    hasProposalSelector: PropTypes.bool,
    initializeForm: PropTypes.func,
    initialValues: PropTypes.object,
    isFetchingCurrencies: PropTypes.bool,
    isFetchingDocuments: PropTypes.bool,
    isFromCustomTag: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    isInsertTagMode: PropTypes.bool,
    prepareInsert: PropTypes.func,
    save: PropTypes.func,
    selectedTag: PropTypes.object,
    tagFormatsData: PropTypes.array,
    unitFormatsData: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);
