import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import ContentLoader from 'react-content-loader';

const Placeholder = (
    <>
        <Row className="m-0 mb-3">
            <Col md="14" lg="14" className="d-flex">
                <ContentLoader height={20} />
            </Col>
            <Col md="4" lg="4" className="d-flex">
                <ContentLoader height={20} />
            </Col>
        </Row>
        <Row className="m-0 mb-3">
            <Col md="3" lg="3" className="d-flex">
                <ContentLoader height={100} />
            </Col>
            <Col md="4" lg="4" className="d-flex">
                <ContentLoader height={100} />
            </Col>
            <Col md="11" lg="11" className="d-flex">
                <ContentLoader height={100} />
            </Col>
        </Row>
    </>
);

const PlaceholderLibraryTags = ({ children, ready }) => {
    return <>{ready ? Placeholder : children}</>;
};

PlaceholderLibraryTags.propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    ready: PropTypes.bool,
};

export default PlaceholderLibraryTags;
