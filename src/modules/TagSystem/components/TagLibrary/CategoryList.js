import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import { getFilteredCategories } from '../../helpers';
import { CategoryWrapper, NavItem, StyledText } from '../styledComponents';

const CategoryList = ({
    filteredTags,
    handleSelectCategory,
    isForSmartDocuments,
    selectedCategory,
}) => {
    const { t } = useTranslation();
    if (isEmpty(filteredTags)) {
        return (
            <Row className="m-0 mt-3">
                <Col>
                    <StyledText>
                        {t('There are no categories to display')}
                    </StyledText>
                </Col>
            </Row>
        );
    }

    const categories = getFilteredCategories(filteredTags, isForSmartDocuments);

    return (
        <CategoryWrapper>
            {categories.map((category) => {
                return (
                    <NavItem
                        className={selectedCategory === category && 'active'}
                        key={filteredTags[category].label}
                        onClick={() => handleSelectCategory(category)}
                    >
                        {filteredTags[category].label}
                    </NavItem>
                );
            })}
        </CategoryWrapper>
    );
};

CategoryList.propTypes = {
    filteredTags: PropTypes.object,
    handleSelectCategory: PropTypes.func,
    isForSmartDocuments: PropTypes.bool,
    selectedCategory: PropTypes.string,
};

export default CategoryList;
