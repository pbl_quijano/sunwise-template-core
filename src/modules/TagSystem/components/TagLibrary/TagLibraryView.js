import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Button from '@components/Button';
import InputSearch from '@components/InputSearch';

import { CREATE_CUSTOM_TAG_STATE } from '@constants/tags';

import * as actions from '../../actions';
import { searchTags } from '../../helpers';
import * as selectors from '../../selectors';

import PlaceholderLibraryTags from './PlaceholderLibraryTags';
import TagLibraryContent from './TagLibraryContent';

const LibraryView = ({
    catalogs,
    tagsLocale,
    fetchDocuments,
    fetchTags,
    filteredTags,
    filterTags,
    editionLevel,
    handleOnSubmit,
    hasProposalSelector,
    isFetchingTags,
    isForSmartDocuments,
    isFromCustomTag,
    isInitialazing,
    selectCategory,
    selectedCategory,
    selectedTag,
    selectTag,
    setContentViewState,
    tags,
}) => {
    const { t } = useTranslation();
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        fetchTags();
    }, []);

    useEffect(() => {
        searchTags(tags, searchText, filterTags);
    }, [tags, searchText]);

    return (
        <PlaceholderLibraryTags ready={isFetchingTags || isInitialazing}>
            <>
                <Row>
                    <Col xs="14">
                        <InputSearch
                            onChange={(e) => setSearchText(e.target.value)}
                            placeholder={t('Search')}
                            value={searchText}
                        />
                    </Col>
                    <Col xs="4">
                        <Button
                            className="ml-auto"
                            color="#2F4DFF"
                            direction="reverse"
                            handleClick={() =>
                                setContentViewState(CREATE_CUSTOM_TAG_STATE)
                            }
                            icon="fas fa-plus"
                            label={t('Create tag', { count: 2 })}
                            variant="bold"
                            visible={!isFromCustomTag}
                        />
                    </Col>
                </Row>

                <hr className="mb-0" />

                <TagLibraryContent
                    catalogs={catalogs}
                    tagsLocale={tagsLocale}
                    editionLevel={editionLevel}
                    fetchDocuments={fetchDocuments}
                    filteredTags={filteredTags}
                    handleOnSubmit={handleOnSubmit}
                    handleSelectCategory={(category) =>
                        selectCategory(category)
                    }
                    handleSelectTag={(tag) =>
                        selectTag(tag, hasProposalSelector)
                    }
                    hasProposalSelector={hasProposalSelector}
                    isForSmartDocuments={isForSmartDocuments}
                    isFromCustomTag={isFromCustomTag}
                    isInitialazing={isInitialazing}
                    selectedCategory={selectedCategory}
                    selectedTag={selectedTag}
                />
            </>
        </PlaceholderLibraryTags>
    );
};

const mapStateToProps = createStructuredSelector({
    filteredTags: selectors.getFilteredTags,
    isFetchingTags: selectors.getIsFetchingTags,
    selectedCategory: selectors.getSelectedCategory,
    selectedTag: selectors.getSelectedTag,
    tags: selectors.getDataFetchTags,
});

const mapDispatchToProps = (dispatch) => ({
    fetchTags: () => dispatch(actions.fetchTags()),
    filterTags: (values) => dispatch(actions.filterTags(values)),
    selectCategory: (category) => dispatch(actions.selectCategory(category)),
    selectTag: (tag, hasProposalSelector) =>
        dispatch(actions.selectTag(tag, hasProposalSelector)),
    setContentViewState: (contentViewState) =>
        dispatch(actions.setContentViewState(contentViewState)),
});

LibraryView.propTypes = {
    catalogs: PropTypes.array,
    tagsLocale: PropTypes.string,
    editionLevel: PropTypes.string,
    fetchDocuments: PropTypes.func,
    fetchTags: PropTypes.func,
    filteredTags: PropTypes.object,
    filterTags: PropTypes.func,
    handleOnSubmit: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    isFetchingTags: PropTypes.bool,
    isForSmartDocuments: PropTypes.bool,
    isFromCustomTag: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    selectCategory: PropTypes.func,
    selectedCategory: PropTypes.string,
    selectedTag: PropTypes.string,
    selectTag: PropTypes.func,
    setContentViewState: PropTypes.func,
    tags: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(LibraryView);
