import PropTypes from 'prop-types';
import { Col, Form, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ReduxFieldSelect from '@components/ReduxFieldSelect';

import { FormLabel, StyledField } from '../styledComponents';

const ProposalDataFrom = ({ catalogs, disabled, visible } = {}) => {
    const { t } = useTranslation();

    if (!visible) return null;

    return (
        <Row>
            <Col>
                <Form.Group className="mb-0">
                    <FormLabel>
                        {t('What proposal is the data taken from?')}
                    </FormLabel>
                    <StyledField
                        component={ReduxFieldSelect}
                        disabled={disabled}
                        name="proposal_number"
                        options={[
                            {
                                label: t('Select proposal'),
                                value: '',
                                disabled: true,
                            },
                            ...catalogs.map((item) => ({
                                label: item.name,
                                value: item.id,
                            })),
                        ]}
                    />
                </Form.Group>
            </Col>
        </Row>
    );
};

ProposalDataFrom.propTypes = {
    catalogs: PropTypes.array,
    disabled: PropTypes.bool,
    visible: PropTypes.bool,
};

export default ProposalDataFrom;
