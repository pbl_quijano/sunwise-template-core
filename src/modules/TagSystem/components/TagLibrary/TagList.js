import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import { HIDDEN_TAGS, TAG_ALL_TEXT_FORMAT_TYPES } from '@constants/tags';

import { NavItem, StyledText } from '../styledComponents';

const TagList = ({
    filteredTags,
    handleSelectTag,
    isFromCustomTag,
    selectedCategory,
    selectedTag,
}) => {
    const { t } = useTranslation();
    if (isNil(selectedCategory) || isNil(filteredTags[selectedCategory])) {
        return (
            <Row className="m-0 mt-3">
                <Col>
                    <StyledText>{t('Select a category')}</StyledText>
                </Col>
            </Row>
        );
    }

    const getTag = (tag) => {
        const filteredTag = filteredTags[selectedCategory].values[tag];
        return { ...filteredTag, tag: tag };
    };

    const isDisabled = (tag) => {
        if (isFromCustomTag && tag.calculable === 'false') {
            return true;
        }
        if (
            isFromCustomTag &&
            TAG_ALL_TEXT_FORMAT_TYPES.includes(tag.format_type)
        ) {
            return true;
        }
        if (
            isFromCustomTag &&
            tag.calculable === 'true' &&
            TAG_ALL_TEXT_FORMAT_TYPES.includes(tag.format_type)
        ) {
            return true;
        }
        return false;
    };

    return (
        <div className="mt-3">
            {Object.keys(filteredTags[selectedCategory].values).map((tag) => {
                return (
                    !HIDDEN_TAGS.includes(tag) && (
                        <NavItem
                            className={selectedTag === tag && 'active'}
                            disabled={isDisabled(getTag(tag))}
                            format-type={getTag(tag).format_type || 'none'}
                            key={`tag-${tag}`}
                            onClick={() => handleSelectTag(getTag(tag))}
                        >
                            {getTag(tag).label}
                        </NavItem>
                    )
                );
            })}
        </div>
    );
};

TagList.propTypes = {
    filteredTags: PropTypes.object,
    handleSelectTag: PropTypes.func,
    isFromCustomTag: PropTypes.bool,
    selectedCategory: PropTypes.string,
    selectedTag: PropTypes.string,
};

export default TagList;
