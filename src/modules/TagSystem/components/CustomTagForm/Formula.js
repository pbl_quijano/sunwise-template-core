import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import { Field } from 'redux-form';

import ReduxFieldInput from '@components/ReduxFieldInput';

import { TagLabel } from '../styledComponents';

const Formula = ({ calculator }) => (
    <Row className="mt-3">
        <Col>
            {calculator.map((item) => (
                <TagLabel
                    background={item.is_operator ? 'transparent' : item.color}
                    key={`tag-label-${Math.random()}`}
                    size={item.is_operator ? 15 : 12}
                >
                    {item.title}
                </TagLabel>
            ))}
            <Field
                component={ReduxFieldInput}
                className="d-none"
                name="tag"
                type="text"
            />
        </Col>
    </Row>
);

Formula.propTypes = {
    calculator: PropTypes.array,
};

export default Formula;
