import PropTypes from 'prop-types';
import { Container as BootstrapContainer } from 'react-bootstrap';
import { connect } from 'react-redux';

import * as actions from '../../actions';
import { handleSubmitCustomTagBuild } from '../../helpers';

import TagFormWithCalculator from './TagFormWithCalculator';
import TagLibraryModal from './TagLibraryModal';

const Container = ({
    catalogs,
    tagsLocale,
    contentViewState,
    editionLevel,
    handleEditTag,
    handleInsertTag,
    hasProposalSelector,
    isInitialazing,
    setIsOpenTagLibraryModal,
}) => {
    const handleOnSubmit = handleSubmitCustomTagBuild(
        contentViewState,
        tagsLocale,
        handleEditTag,
        handleInsertTag
    );

    return (
        <>
            <BootstrapContainer fluid>
                <TagFormWithCalculator
                    tagsLocale={tagsLocale}
                    contentViewState={contentViewState}
                    editionLevel={editionLevel}
                    handleOnSubmit={handleOnSubmit}
                    isInitialazing={isInitialazing}
                    setIsOpenTagLibraryModal={setIsOpenTagLibraryModal}
                />
            </BootstrapContainer>
            <TagLibraryModal
                catalogs={catalogs}
                tagsLocale={tagsLocale}
                editionLevel={editionLevel}
                hasProposalSelector={hasProposalSelector}
                isInitialazing={isInitialazing}
            />
        </>
    );
};

const mapDispatchToProps = (dispatch) => ({
    setIsOpenTagLibraryModal: (isOpenModal) =>
        dispatch(actions.setIsOpenTagLibraryModal(isOpenModal)),
});

Container.propTypes = {
    catalogs: PropTypes.array,
    tagsLocale: PropTypes.string,
    contentViewState: PropTypes.number,
    editionLevel: PropTypes.string,
    handleEditTag: PropTypes.func,
    handleInsertTag: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    reset: PropTypes.func,
    setIsOpenTagLibraryModal: PropTypes.func,
};

export default connect(null, mapDispatchToProps)(Container);
