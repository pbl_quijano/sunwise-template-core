import PropTypes from 'prop-types';
import { Col, Form, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import ReduxFieldInput from '@components/ReduxFieldInput';
import ReduxFieldSelect from '@components/ReduxFieldSelect';

import {
    TAG_CURRENCY_FORMAT_TYPES,
    TAG_NUMBER_FORMAT_TYPES,
    TAG_PERCENT_FORMAT_TYPES,
} from '@constants/tags';
import { PARTIAL_EDITION_MODE } from '@constants/types';

import {
    getDecimalFormatsToSelect,
    normalizeCurrencyField,
    normalizeDecimalField,
    getUnitsToSelect,
} from '../../helpers';
import { FormLabel, StyledField } from '../styledComponents';
import WrapperField from '../WrapperField';

const ReduxFieldArrayItems = ({
    changeInput,
    currencies,
    decimalFormatsData,
    editionLevel,
    fields,
    formValues,
    isInitialazing,
    unitFormatsData,
}) => {
    const { t } = useTranslation();
    const fieldsDisabled = editionLevel === PARTIAL_EDITION_MODE;
    return (
        <>
            {fields.map((item, i) => {
                if (!formValues.calculator[i].is_operator) {
                    return (
                        <Row className="mt-2" key={i}>
                            <Col xs="5">
                                <Form.Group className="mb-0">
                                    <FormLabel>{t('Tag')}</FormLabel>
                                    <StyledField
                                        component={ReduxFieldInput}
                                        disabled
                                        name={`${item}.title`}
                                        type="text"
                                    />
                                </Form.Group>
                            </Col>

                            <WrapperField
                                visible={TAG_CURRENCY_FORMAT_TYPES.includes(
                                    formValues.calculator[i].format_type
                                )}
                            >
                                <Col xs="5">
                                    <Form.Group>
                                        <FormLabel>{t('Currency')}</FormLabel>
                                        <StyledField
                                            component={ReduxFieldSelect}
                                            disabled={fieldsDisabled}
                                            name={`${item}.currency`}
                                            normalize={normalizeCurrencyField(
                                                changeInput,
                                                currencies,
                                                [
                                                    `${item}.currencyLocale`,
                                                    `${item}.unit`,
                                                    `${item}.symbol`,
                                                ]
                                            )}
                                            options={[
                                                {
                                                    label: t('Select currency'),
                                                    value: '',
                                                    disabled: true,
                                                },
                                                ...currencies.map((item) => ({
                                                    label: item.name,
                                                    value: item.id,
                                                })),
                                            ]}
                                        />
                                    </Form.Group>
                                </Col>
                            </WrapperField>

                            <WrapperField
                                visible={
                                    TAG_NUMBER_FORMAT_TYPES.includes(
                                        formValues.calculator[i].format_type
                                    ) ||
                                    TAG_PERCENT_FORMAT_TYPES.includes(
                                        formValues.calculator[i].format_type
                                    )
                                }
                            >
                                <Col xs="5">
                                    <Form.Group>
                                        <FormLabel>{t('Unit')}</FormLabel>
                                        <StyledField
                                            component={ReduxFieldSelect}
                                            disabled={fieldsDisabled}
                                            name={`${item}.unit`}
                                            options={getUnitsToSelect(
                                                currencies,
                                                formValues.calculator[i]
                                                    .format_type,
                                                isInitialazing,
                                                unitFormatsData
                                            )}
                                        />
                                    </Form.Group>
                                </Col>
                            </WrapperField>

                            <WrapperField
                                visible={
                                    TAG_NUMBER_FORMAT_TYPES.includes(
                                        formValues.calculator[i].format_type
                                    ) ||
                                    TAG_PERCENT_FORMAT_TYPES.includes(
                                        formValues.calculator[i].format_type
                                    ) ||
                                    TAG_CURRENCY_FORMAT_TYPES.includes(
                                        formValues.calculator[i].format_type
                                    )
                                }
                            >
                                <Col xs="5">
                                    <Form.Group>
                                        <FormLabel>{t('Decimals')}</FormLabel>
                                        <StyledField
                                            component={ReduxFieldSelect}
                                            disabled={fieldsDisabled}
                                            name={`${item}.decimal_type`}
                                            normalize={normalizeDecimalField(
                                                changeInput,
                                                decimalFormatsData,
                                                `${item}.decimal_places`
                                            )}
                                            options={getDecimalFormatsToSelect(
                                                decimalFormatsData,
                                                isInitialazing
                                            )}
                                        />
                                    </Form.Group>
                                </Col>
                            </WrapperField>
                        </Row>
                    );
                }
            })}
        </>
    );
};

ReduxFieldArrayItems.propTypes = {
    changeInput: PropTypes.func,
    currencies: PropTypes.array,
    decimalFormatsData: PropTypes.array,
    editionLevel: PropTypes.string,
    fields: PropTypes.object,
    formValues: PropTypes.object,
    isInitialazing: PropTypes.bool,
    unitFormatsData: PropTypes.array,
};

export default ReduxFieldArrayItems;
