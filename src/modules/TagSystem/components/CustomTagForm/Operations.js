import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

import {
    ButtonInsertNumber,
    ButtonOperator,
    StyledInputGroup,
    StyledInputNumber,
} from '../styledComponents';

const Operations = ({
    handleClickOperator,
    handleInsertNumber,
    number,
    setNumber,
}) => {
    const { t } = useTranslation();
    return (
        <Row>
            <Col>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: '-',
                            tag: '-',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    <i className="fas fa-minus"></i>
                </ButtonOperator>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: '+',
                            tag: '+',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    <i className="fa fa-plus"></i>
                </ButtonOperator>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: 'x',
                            tag: '*',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    <i className="fa fa-times"></i>
                </ButtonOperator>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: '÷',
                            tag: '/',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    <i className="fas fa-divide"></i>
                </ButtonOperator>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: '[',
                            tag: '[',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    &#91;
                </ButtonOperator>
                <ButtonOperator
                    onClick={() =>
                        handleClickOperator({
                            title: ']',
                            tag: ']',
                            is_operator: true,
                        })
                    }
                    type="button"
                >
                    &#93;
                </ButtonOperator>
                <StyledInputGroup>
                    <StyledInputNumber
                        id="number"
                        min="1"
                        onChange={(e) => setNumber(e.target.value)}
                        placeholder="--"
                        type="number"
                        value={number}
                    />
                    <div className="input-group-append">
                        <ButtonInsertNumber
                            className="border-left-0"
                            disabled={!number || number === 0}
                            onClick={() => handleInsertNumber()}
                            title={t('Insert number')}
                            type="button"
                        >
                            <i
                                className="fa fa-arrow-right"
                                aria-hidden="true"
                            />
                        </ButtonInsertNumber>
                    </div>
                </StyledInputGroup>
            </Col>
        </Row>
    );
};

Operations.propTypes = {
    handleClickOperator: PropTypes.func,
    handleInsertNumber: PropTypes.func,
    number: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    setNumber: PropTypes.func,
};

export default Operations;
