import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import { CREATE_CUSTOM_TAG_STATE } from '@constants/tags';

import { ReactComponent as BackspaceIcon } from '@res/icons/backspace.svg';
import { ReactComponent as RefreshIcon } from '@res/icons/refresh.svg';

import { ButtonReset } from '../styledComponents';

const FormulaControlButtons = ({
    calculator,
    handleClickEraser,
    handleClickReset,
    contentViewState,
}) => {
    const { t } = useTranslation();
    if (
        calculator.length === 0 ||
        contentViewState !== CREATE_CUSTOM_TAG_STATE
    ) {
        return null;
    }
    return (
        <>
            <ButtonReset
                onClick={() => handleClickEraser()}
                title={t('Delete last element of the formula')}
                type="button"
            >
                <img
                    alt="backspace"
                    className="mr-2"
                    src={BackspaceIcon}
                    width="14"
                />
                {t('Erase')}
            </ButtonReset>
            <ButtonReset
                onClick={() => handleClickReset()}
                title={t('Clear')}
                type="button"
            >
                <img alt="reset" className="mr-2" src={RefreshIcon} />
                {t('Reset formula')}
            </ButtonReset>
        </>
    );
};

FormulaControlButtons.propTypes = {
    calculator: PropTypes.array,
    handleClickEraser: PropTypes.func,
    handleClickReset: PropTypes.func,
    contentViewState: PropTypes.number,
};

export default FormulaControlButtons;
