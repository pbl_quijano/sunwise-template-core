import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { change, FieldArray, reduxForm } from 'redux-form';
import { createStructuredSelector } from 'reselect';

import ReduxFieldInput from '@components/ReduxFieldInput';
import ReduxFieldSelect from '@components/ReduxFieldSelect';

import { CREATE_CUSTOM_TAG_STATE } from '@constants/tags';
import { PARTIAL_EDITION_MODE } from '@constants/types';

import { ReactComponent as CalculatorIcon } from '@res/icons/calculator.svg';
import { ReactComponent as SettingsIcon } from '@res/icons/settings.svg';

import validate from '../../customTagValidate';
import {
    getFormattedValue,
    getDecimalFormatsToSelect,
    getTextButton,
    handleClickEraser,
    handleClickOperator,
    handleInsertNumber,
    normalizeDecimalField,
    updateTag,
} from '../../helpers';
import * as selectors from '../../selectors';
import {
    ButtonInsert,
    ButtonNew,
    Caption,
    FormLabel,
    SettingsBox,
    StyledAlert,
    StyledField,
    StyledTitleField,
    TagLabel,
} from '../styledComponents';

import Formula from './Formula';
import FormulaControlButtons from './FormulaControlButtons';
import Operations from './Operations';
import ReduxFieldArrayItems from './ReduxFieldArrayItems';
import ResetDefaultValue from './ResetDefaultValue';

const TagFormWithCalculator = ({
    changeInput,
    contentViewState,
    currencies,
    decimalFormatsData,
    editionLevel,
    tagsLocale,
    errors,
    formValues,
    handleOnSubmit,
    handleSubmit,
    isInitialazing,
    setIsOpenTagLibraryModal,
    unitFormatsData,
}) => {
    const { t } = useTranslation();
    const [number, setNumber] = useState(1);

    const data = get(formValues, 'calculator', []);

    useEffect(() => {
        updateTag(changeInput, data);
    }, [data]);

    return (
        <div className="mt-3">
            <Form onSubmit={handleSubmit(handleOnSubmit)}>
                <Row className="m-0 mt-3 align-items-center">
                    <Col>
                        <Form.Group className="mb-0">
                            {!isEmpty(formValues.title) && (
                                <FormLabel>{t('Tag title')}</FormLabel>
                            )}
                            <StyledTitleField
                                component={ReduxFieldInput}
                                name="title"
                                placeholder={t('Tag title')}
                                type="text"
                            />
                        </Form.Group>
                    </Col>
                </Row>

                <Row className="m-0 mt-3">
                    <Col>
                        <Form.Group className="mb-0">
                            <FormLabel>{t('Description')}</FormLabel>
                            <StyledField
                                component={ReduxFieldInput}
                                name="description"
                                type="text"
                            />
                        </Form.Group>
                    </Col>
                </Row>

                <SettingsBox className="mt-3">
                    <Row>
                        <Col>
                            <Caption className="mb-0">
                                <CalculatorIcon
                                    width="14"
                                    height="14"
                                    className="mr-2"
                                />

                                {t('Calculator')}
                            </Caption>
                        </Col>
                    </Row>
                    <hr />

                    <Row className="mb-2">
                        <Col>
                            <FormLabel>{t('Tag', { count: 2 })}</FormLabel>
                        </Col>
                    </Row>

                    <FieldArray
                        changeInput={changeInput}
                        component={ReduxFieldArrayItems}
                        currencies={currencies}
                        decimalFormatsData={decimalFormatsData}
                        editionLevel={editionLevel}
                        formValues={formValues}
                        isInitialazing={isInitialazing}
                        name="calculator"
                        type="array"
                        unitFormatsData={unitFormatsData}
                    />

                    {contentViewState === CREATE_CUSTOM_TAG_STATE && (
                        <Row className="mt-2">
                            <Col>
                                <ButtonNew
                                    onClick={() =>
                                        setIsOpenTagLibraryModal(true)
                                    }
                                    type="button"
                                >
                                    <i
                                        className="fa fa-plus mr-1"
                                        aria-hidden="true"
                                    ></i>
                                    {t('New tag')}
                                </ButtonNew>
                            </Col>
                        </Row>
                    )}

                    <hr className="mt-3" />

                    {contentViewState === CREATE_CUSTOM_TAG_STATE && (
                        <Row className="mt-3">
                            <Col>
                                <FormLabel>{t('Operations')}</FormLabel>
                                <Operations
                                    handleClickOperator={(operator) =>
                                        handleClickOperator(
                                            changeInput,
                                            data,
                                            operator
                                        )
                                    }
                                    handleInsertNumber={() =>
                                        handleInsertNumber(
                                            changeInput,
                                            data,
                                            number,
                                            setNumber
                                        )
                                    }
                                    number={number}
                                    setNumber={setNumber}
                                />
                            </Col>
                        </Row>
                    )}

                    <Row className="mt-3 align-items-center">
                        <Col>
                            <Caption className="mb-0">{t('Formula')}</Caption>
                        </Col>
                        <Col className="text-right">
                            <FormulaControlButtons
                                calculator={data}
                                changeInput={changeInput}
                                handleClickEraser={() =>
                                    handleClickEraser(changeInput, data)
                                }
                                handleClickReset={() =>
                                    changeInput('calculator', [])
                                }
                                contentViewState={contentViewState}
                            />
                        </Col>
                    </Row>
                    <Formula calculator={data} />
                </SettingsBox>

                <SettingsBox className="mt-3">
                    <Row className="align-items-center">
                        <Col>
                            <Caption className="mb-0">
                                <SettingsIcon
                                    width="17"
                                    height="17"
                                    className="mr-2"
                                />

                                {t('Setting', { count: 2 })}
                            </Caption>
                        </Col>
                        <ResetDefaultValue
                            defaultValue={formValues.default_value}
                            showButtonReset={
                                editionLevel === PARTIAL_EDITION_MODE
                            }
                            resetToDefaultValue={() =>
                                changeInput('value', formValues.default_value)
                            }
                            value={formValues.value}
                        />
                    </Row>
                    <hr />

                    <Row className="mt-3">
                        <Col xs="3">
                            <Form.Group className="mb-0">
                                <FormLabel>{t('Value')}</FormLabel>
                                <StyledField
                                    component={ReduxFieldInput}
                                    name="value"
                                    onlyNumbers
                                    type="string"
                                />
                            </Form.Group>
                        </Col>

                        <Col xs="5">
                            <Form.Group>
                                <FormLabel>{t('Unit')}</FormLabel>
                                <StyledField
                                    component={ReduxFieldInput}
                                    disabled={
                                        editionLevel === PARTIAL_EDITION_MODE
                                    }
                                    name="unit"
                                    type="text"
                                />
                            </Form.Group>
                        </Col>

                        <Col xs="5">
                            <Form.Group>
                                <FormLabel>{t('Decimals')}</FormLabel>
                                <StyledField
                                    component={ReduxFieldSelect}
                                    disabled={
                                        editionLevel === PARTIAL_EDITION_MODE
                                    }
                                    name="decimal_type"
                                    normalize={normalizeDecimalField(
                                        changeInput,
                                        decimalFormatsData,
                                        'decimal_places'
                                    )}
                                    options={getDecimalFormatsToSelect(
                                        decimalFormatsData,
                                        isInitialazing
                                    )}
                                />
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row className="mt-3">
                        <Col>
                            <Caption className="mt-3">
                                {t('Default value')}
                            </Caption>
                            {formValues.value && (
                                <TagLabel background={formValues.color}>
                                    {getFormattedValue(
                                        true,
                                        tagsLocale,
                                        formValues
                                    )}
                                </TagLabel>
                            )}
                        </Col>
                    </Row>
                </SettingsBox>

                {!isEmpty(errors) && (
                    <Row className="mt-3">
                        <Col>
                            <StyledAlert variant="warning">
                                {t('You need to capture all fields')}
                            </StyledAlert>
                        </Col>
                    </Row>
                )}

                <Row className="mt-3 justify-content-center">
                    <Col xs="10">
                        <ButtonInsert size="small" type="submit">
                            <i className="fas fa-arrow-right mr-2"></i>
                            {getTextButton(contentViewState)}
                        </ButtonInsert>
                    </Col>
                </Row>
            </Form>
        </div>
    );
};

const mapStateToProps = createStructuredSelector({
    currencies: selectors.getCurrenciesData,
    decimalFormatsData: selectors.getDecimalFormatsData,
    errors: selectors.getCustomTagFormErrors,
    formValues: selectors.getCustomTagValues,
    initialValues: selectors.getInitialCustomTagValues,
    unitFormatsData: selectors.getUnitFormatsData,
});

const mapDispatchToProps = (dispatch) => ({
    changeInput: (field, value) =>
        dispatch(change('custom-tag-form', field, value)),
});

TagFormWithCalculator.propTypes = {
    changeInput: PropTypes.func,
    tagsLocale: PropTypes.string,
    contentViewState: PropTypes.number,
    currencies: PropTypes.array,
    decimalFormatsData: PropTypes.array,
    editionLevel: PropTypes.string,
    errors: PropTypes.object,
    formValues: PropTypes.object,
    handleOnSubmit: PropTypes.func,
    handleSubmit: PropTypes.func,
    isInitialazing: PropTypes.bool,
    setIsOpenTagLibraryModal: PropTypes.func,
    unitFormatsData: PropTypes.array,
};

const FormContainer = reduxForm({
    enableReinitialize: true,
    form: 'custom-tag-form',
    validate,
})(TagFormWithCalculator);

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);
