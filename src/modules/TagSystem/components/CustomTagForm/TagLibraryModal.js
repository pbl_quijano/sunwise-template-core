import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import ModalBootstrap from '@components/ModalBootstrap';

import * as actions from '../../actions';
import * as selectors from '../../selectors';
import TagLibrary from '../TagLibrary';

export const ModalTitle = styled.h3`
    color: #002438;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 32px;
    margin-bottom: 0;
`;

const TagLibraryModal = ({
    catalogs,
    tagsLocale,
    editionLevel,
    hasProposalSelector,
    isInitialazing,
    isOpenTagLibraryModal,
    prepareCustomTagFromSelectTag,
    setIsOpenTagLibraryModal,
}) => {
    const { t } = useTranslation();
    return (
        <ModalBootstrap
            backdropClassName="second-modal-backdrop"
            className="second-modal"
            closeButton={false}
            onHide={() => setIsOpenTagLibraryModal(false)}
            show={isOpenTagLibraryModal}
            size="xl"
            title={
                <ModalTitle>
                    <button
                        className="btn btn-transparent p-0 mr-2"
                        onClick={() => setIsOpenTagLibraryModal(false)}
                        title={t('Back')}
                        type="button"
                    >
                        <i className="fas fa-arrow-left mr-2"></i>
                    </button>
                    {t(
                        'Select a tag from the library and insert to the formula'
                    )}
                </ModalTitle>
            }
        >
            <TagLibrary
                catalogs={catalogs}
                tagsLocale={tagsLocale}
                editionLevel={editionLevel}
                hasProposalSelector={hasProposalSelector}
                isInitialazing={isInitialazing}
                isFromCustomTag
                prepareCustomTagFromSelectTag={prepareCustomTagFromSelectTag}
            />
        </ModalBootstrap>
    );
};

const mapStateToProps = createStructuredSelector({
    isOpenTagLibraryModal: selectors.getIsOpenTagLibraryModal,
});

const mapDispatchToProps = (dispatch) => ({
    prepareCustomTagFromSelectTag: (tag) =>
        dispatch(actions.prepareCustomTagFromSelectTag(tag)),
    setIsOpenTagLibraryModal: (isOpenModal) =>
        dispatch(actions.setIsOpenTagLibraryModal(isOpenModal)),
});

TagLibraryModal.propTypes = {
    catalogs: PropTypes.array,
    tagsLocale: PropTypes.string,
    editionLevel: PropTypes.string,
    hasProposalSelector: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    isOpenTagLibraryModal: PropTypes.bool,
    prepareCustomTagFromSelectTag: PropTypes.func,
    setIsOpenTagLibraryModal: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(TagLibraryModal);
