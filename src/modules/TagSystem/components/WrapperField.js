import PropTypes from 'prop-types';

const WrapperField = ({ children, visible }) => {
    if (!visible) {
        return null;
    }
    return <>{children}</>;
};

WrapperField.propTypes = {
    children: PropTypes.object,
    visible: PropTypes.bool,
};

export default WrapperField;
