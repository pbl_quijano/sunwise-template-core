import PropTypes from 'prop-types';

import {
    CREATE_CUSTOM_TAG_STATE,
    EDIT_CUSTOM_TAG_STATE,
    HIDDEN_MODAL_STATE,
} from '@constants/tags';

import CustomTagForm from './CustomTagForm';
import TagLibrary from './TagLibrary';

const ContentView = ({
    catalogs,
    contentViewState,
    editionLevel,
    handleEditTag,
    handleInsertTag,
    hasProposalSelector,
    isInitialazing,
    tagsLocale,
    templateType,
}) => {
    if (contentViewState === HIDDEN_MODAL_STATE) {
        return null;
    }
    if (
        contentViewState === CREATE_CUSTOM_TAG_STATE ||
        contentViewState === EDIT_CUSTOM_TAG_STATE
    ) {
        return (
            <CustomTagForm
                catalogs={catalogs}
                tagsLocale={tagsLocale}
                contentViewState={contentViewState}
                editionLevel={editionLevel}
                handleEditTag={handleEditTag}
                handleInsertTag={handleInsertTag}
                hasProposalSelector={hasProposalSelector}
                isInitialazing={isInitialazing}
            />
        );
    }
    return (
        <TagLibrary
            catalogs={catalogs}
            tagsLocale={tagsLocale}
            contentViewState={contentViewState}
            editionLevel={editionLevel}
            handleEditTag={handleEditTag}
            handleInsertTag={handleInsertTag}
            hasProposalSelector={hasProposalSelector}
            isInitialazing={isInitialazing}
            templateType={templateType}
        />
    );
};

ContentView.propTypes = {
    catalogs: PropTypes.array,
    contentViewState: PropTypes.number,
    editionLevel: PropTypes.string,
    handleEditTag: PropTypes.func,
    handleInsertTag: PropTypes.func,
    hasProposalSelector: PropTypes.bool,
    isInitialazing: PropTypes.bool,
    tagsLocale: PropTypes.string,
    templateType: PropTypes.number,
};

export default ContentView;
