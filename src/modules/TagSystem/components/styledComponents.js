import { Alert, Form, NavLink } from 'react-bootstrap';
import { Field } from 'redux-form';
import styled from 'styled-components';

import AddButton from '@components/AddButton';

export const ButtonInsert = styled(AddButton)`
    width: 100%;
    background: #ff9a00;
    text-transform: uppercase;
    box-shadow: none !important;
    outline: none !important;
`;

export const ButtonInsertNumber = styled.button`
    height: ${({ height = 40 }) => `${height}px`};
    width: 40px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    border: 0;
    border-top-left-radius: 0;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 0;
    background-color: ${({ disabled, background = '#2F4DFF' }) =>
        disabled ? '#e9ecef' : background};
    box-shadow: none !important;
    color: ${({ disabled, color = '#FFFFFF' }) =>
        disabled ? '#848BAB' : color};
    font-size: 12px;
    font-weight: 800;
    letter-spacing: 0.31px;
    line-height: 15px;
`;

export const ButtonNew = styled.button`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    box-sizing: border-box;
    width: 100%;
    height: 49px;
    border: 1px dashed #d3d7eb;
    border-radius: 5px;
    background-color: #ffffff;
    box-shadow: none !important;
    outline: none !important;
    color: #002438;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 15px;
    .fa {
        color: #2f4dff;
    }
`;

export const ButtonOperator = styled.button`
    height: 40px;
    width: 40px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    border: 1px solid #eff1fb;
    border-radius: 3px;
    background-color: ${({ disabled }) => (disabled ? '#e9ecef' : '#FFFFFF')};
    margin-right: 0.5rem;
    margin-bottom: 0.5rem;
    box-shadow: none !important;
    outline: none !important;
    color: #848bab;
    font-size: 12px;
    font-weight: 800;
    letter-spacing: 0.31px;
    line-height: 15px;
`;

export const ButtonReset = styled.button`
    height: 30px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    border: 1px solid #ecedf0;
    border-radius: 3px;
    background-color: #ffffff;
    box-shadow: none !important;
    outline: none !important;
    margin-right: 0.5rem;
    color: #848bab;
    font-size: 12px;
    letter-spacing: 0;
    line-height: 14px;
    img {
        object-fit: contain;
        object-position: center;
        height: 14px;
    }
`;

export const Caption = styled.h5`
    color: #002438;
    font-size: 16px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 19px;
`;

export const CategoryTitle = styled.h4`
    color: #002438;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 22px;
`;

export const CategoryWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: calc(100% - 108px);
    overflow-y: auto;
`;

export const FormLabel = styled.label`
    color: #2f4dff !important;
    font-size: 10px !important;
    letter-spacing: 0;
    line-height: 13px !important;
    padding-left: 0 !important;
    margin-bottom: 0;
`;

export const NavItem = styled(NavLink)`
    color: #002438;
    font-size: 15px;
    letter-spacing: 0;
    line-height: 20px;
    padding: 0.8rem 1rem;
    &:hover,
    &.active {
        background-color: #f6f8fa;
    }
`;

export const SettingsBox = styled.div`
    width: 100%;
    min-height: 296px;
    box-sizing: border-box;
    border: 1px solid #eff1fb;
    border-radius: 3px;
    background-color: rgba(255, 255, 255, 0.02);
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    padding: 1rem;
`;

export const StyledAlert = styled(Alert)`
    font-size: 11px !important;
    font-weight: bold;
`;

export const StyledField = styled(Field)`
    color: #202253;
    font-size: 13px;
    font-weight: 600;
    letter-spacing: 0;
    line-height: 16px;
    padding-left: 0 !important;
    border: 0 !important;
    border-bottom: 1px solid #eff1fb !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    outline: none !important;
`;

export const StyledInputGroup = styled.div`
    position: relative;
    display: inline-flex;
    align-items: stretch;
    margin-right: 0.5rem;
    margin-bottom: 0.5rem;
`;

export const StyledInputNumber = styled(Form.Control)`
    height: 40px;
    max-width: 100px;
    border: 1px solid #eff1fb;
    border-radius: 3px;
    border-top-left-radius: 3px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 3px;
    background-color: ${({ disabled }) => (disabled ? '#e9ecef' : '#FFFFFF')};
    box-shadow: none !important;
    color: #202253;
    font-size: 13px;
    font-weight: 600;
    text-align: center;
    letter-spacing: 0;
    line-height: 16px;
    &:focus {
        border: 1px solid #eff1fb;
    }
`;

export const StyledText = styled.p`
    font-size: 13px;
`;

export const StyledTitleField = styled(Field)`
    color: #002438;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 22px;
    padding-left: 0 !important;
    border: 0 !important;
    border-bottom: 1px solid #eff1fb !important;
    border-radius: 0 !important;
    box-shadow: none !important;
    outline: none !important;
`;

export const TagLabel = styled.div`
    display: inline-flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: fit-content;
    min-height: 24px;
    padding: 10px;
    border-radius: 5px;
    background: ${({ background }) => background || '#EEEEEE'};
    margin-right: 0.5rem;
    margin-bottom: 0.5rem;
    color: #002438;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0;
    line-height: 15px;
`;

export const TagTitle = styled.h5`
    color: #002438;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 22px;
`;
