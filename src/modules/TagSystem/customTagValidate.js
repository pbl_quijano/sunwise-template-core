import { array, number, object, string } from 'yup';

import {
    TAG_CURRENCY_FORMAT_TYPES,
    TAG_NUMBER_FORMAT_TYPES,
    TAG_PERCENT_FORMAT_TYPES,
} from '@constants/tags';

import schemaValidate from '@helpers/schemaValidate';

export default schemaValidate(
    ({
        CALC_REQUIRED_TEXT,
        FORMULA_REQUIRED_TEXT,
        getMinValueText,
        NUMBER_TYPE_TEXT,
        REQUIRED_TEXT,
    }) =>
        object().shape({
            title: string().required(REQUIRED_TEXT).nullable(),
            description: string().required(REQUIRED_TEXT).nullable(),
            calculator: array()
                .of(
                    object().shape({
                        title: string().required(REQUIRED_TEXT).nullable(),
                        unit: string().when('format_type', (value, schema) => {
                            if (
                                TAG_NUMBER_FORMAT_TYPES.includes(value) ||
                                TAG_PERCENT_FORMAT_TYPES.includes(value)
                            ) {
                                return schema
                                    .required(REQUIRED_TEXT)
                                    .nullable();
                            }
                            return schema.nullable();
                        }),
                        currency: string().when(
                            'format_type',
                            (value, schema) => {
                                if (TAG_CURRENCY_FORMAT_TYPES.includes(value)) {
                                    return schema
                                        .required(REQUIRED_TEXT)
                                        .nullable();
                                }
                                return schema.nullable();
                            }
                        ),
                        decimal_type: string().when(
                            'format_type',
                            (value, schema) => {
                                if (
                                    TAG_CURRENCY_FORMAT_TYPES.includes(value) ||
                                    TAG_NUMBER_FORMAT_TYPES.includes(value) ||
                                    TAG_PERCENT_FORMAT_TYPES.includes(value)
                                ) {
                                    return schema
                                        .required(REQUIRED_TEXT)
                                        .nullable();
                                }
                                return schema.nullable();
                            }
                        ),
                    })
                )
                .required(CALC_REQUIRED_TEXT),
            tag: string().required(FORMULA_REQUIRED_TEXT).nullable(),
            value: number()
                .typeError(NUMBER_TYPE_TEXT)
                .required(REQUIRED_TEXT)
                .min(0, getMinValueText(0))
                .nullable(),
            unit: string().required(REQUIRED_TEXT).nullable(),
            decimal_type: string().required(REQUIRED_TEXT).nullable(),
        })
);
