import update from 'immutability-helper';

import { HIDDEN_MODAL_STATE } from '@constants/tags';

import {
    ADD_TAG_TO_CALCULATOR,
    FETCH_CURRENCIES,
    FETCH_CURRENCIES_SUCCESS,
    FETCH_CURRENCIES_FAILURE,
    FETCH_DOCUMENTS,
    FETCH_DOCUMENTS_FAILURE,
    FETCH_DOCUMENTS_SUCCESS,
    FETCH_TAGS,
    FETCH_TAGS_FAILURE,
    FETCH_TAGS_SUCCESS,
    FETCH_TAG_FORMATS,
    FETCH_TAG_FORMATS_SUCCESS,
    FETCH_TAG_FORMATS_FAILURE,
    FETCH_UNITS_FORMATS,
    FETCH_UNITS_FORMATS_SUCCESS,
    FETCH_UNITS_FORMATS_FAILURE,
    FETCH_DECIMALS_FORMATS,
    FETCH_DECIMALS_FORMATS_SUCCESS,
    FETCH_DECIMALS_FORMATS_FAILURE,
    FILTER_TAGS,
    INITIALIZE_CUSTOM_TAG_FORM,
    INITIALIZE_TAG_LIBRARY_FORM,
    RESET,
    RESET_CUSTOM_TAG_FORM,
    RESET_TAG_LIBRARY_FORM,
    SELECT_CATEGORY,
    SELECT_TAG,
    SET_CONTENT_VIEW_STATE,
    SET_IS_OPEN_MODAL,
    SET_IS_OPEN_TAG_LIBARY_MODAL,
} from './actionTypes';

const INITIAL_STATE = {
    contentViewState: HIDDEN_MODAL_STATE,
    fetchCurrencies: {
        isFetching: false,
        data: [],
        error: null,
    },
    fetchDocuments: {
        data: [],
        error: null,
        isFetching: false,
    },
    fetchTags: {
        isFetching: true,
        data: {},
        errors: [],
    },
    fetchTagFormats: {
        isFetching: false,
        data: [],
        error: null,
    },
    fetchUnitFormats: {
        isFetching: false,
        data: [],
        error: null,
    },
    fetchDecimalFormats: {
        isFetching: false,
        data: [],
        error: null,
    },
    filteredTags: {},
    isOpenModal: false,
    initialCustomTagValues: {
        calculator: [],
        color: '#FFFF66',
        decimal_places: 0,
        decimal_type: '',
        default_value: '',
        description: '',
        format_type: 'new',
        format: '',
        is_new_tag: true,
        symbol: '',
        tag: '',
        title: '',
        unit: '',
        value: '',
    },
    initialTagLibraryValues: {
        catalog_type: '',
        color: '',
        currency: null,
        currencyLocale: '',
        decimal_places: 0,
        decimal_type: '',
        default_value: '',
        description: '',
        format_type: '',
        format: '',
        proposal_number: '',
        hasProposalSelector: false,
        symbol: '',
        tag: '',
        title: '',
        unit: '',
        value: '',
        smart_tag: '',
    },
    isOpenTagLibraryModal: false,
    selectedCategory: null,
    selectedTag: null,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case ADD_TAG_TO_CALCULATOR:
            return update(state, {
                initialCustomTagValues: {
                    $merge: {
                        calculator: action.payload,
                        isSaving: false,
                    },
                },
            });
        case FETCH_CURRENCIES:
            return update(state, {
                fetchCurrencies: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_CURRENCIES_SUCCESS:
            return update(state, {
                fetchCurrencies: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_CURRENCIES_FAILURE:
            return update(state, {
                fetchCurrencies: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_DOCUMENTS:
            return update(state, {
                fetchDocuments: {
                    $merge: {
                        errors: [],
                        isFetching: true,
                    },
                },
            });

        case FETCH_DOCUMENTS_FAILURE:
            return update(state, {
                fetchDocuments: {
                    $merge: {
                        errors: action.payload,
                        isFetching: false,
                    },
                },
            });

        case FETCH_DOCUMENTS_SUCCESS:
            return update(state, {
                fetchDocuments: {
                    $merge: {
                        data: action.payload,
                        isFetching: false,
                    },
                },
            });

        case FETCH_TAGS:
            return update(state, {
                fetchTags: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_TAGS_SUCCESS:
            return update(state, {
                fetchTags: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_TAGS_FAILURE:
            return update(state, {
                fetchTags: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_TAG_FORMATS:
            return update(state, {
                fetchTagFormats: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_TAG_FORMATS_SUCCESS:
            return update(state, {
                fetchTagFormats: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_TAG_FORMATS_FAILURE:
            return update(state, {
                fetchTagFormats: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_UNITS_FORMATS:
            return update(state, {
                fetchUnitFormats: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_UNITS_FORMATS_SUCCESS:
            return update(state, {
                fetchUnitFormats: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_UNITS_FORMATS_FAILURE:
            return update(state, {
                fetchUnitFormats: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_DECIMALS_FORMATS:
            return update(state, {
                fetchDecimalFormats: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_DECIMALS_FORMATS_SUCCESS:
            return update(state, {
                fetchDecimalFormats: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_DECIMALS_FORMATS_FAILURE:
            return update(state, {
                fetchDecimalFormats: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FILTER_TAGS:
            return update(state, {
                filteredTags: { $set: action.payload },
            });

        case INITIALIZE_CUSTOM_TAG_FORM:
            return update(state, {
                initialCustomTagValues: { $merge: action.payload },
            });

        case INITIALIZE_TAG_LIBRARY_FORM:
            return update(state, {
                initialTagLibraryValues: { $merge: action.payload },
            });

        case RESET:
            return update(state, {
                $set: INITIAL_STATE,
            });

        case RESET_CUSTOM_TAG_FORM:
            return update(state, {
                initialCustomTagValues: {
                    $set: INITIAL_STATE.initialCustomTagValues,
                },
            });

        case RESET_TAG_LIBRARY_FORM:
            return update(state, {
                initialTagLibraryValues: {
                    $set: INITIAL_STATE.initialTagLibraryValues,
                },
            });

        case SELECT_CATEGORY:
            return update(state, {
                selectedCategory: { $set: action.payload },
            });

        case SELECT_TAG:
            return update(state, {
                selectedTag: { $set: action.payload },
            });

        case SET_CONTENT_VIEW_STATE:
            return update(state, {
                contentViewState: {
                    $set: action.payload,
                },
            });

        case SET_IS_OPEN_MODAL:
            return update(state, {
                isOpenModal: {
                    $set: action.payload,
                },
            });

        case SET_IS_OPEN_TAG_LIBARY_MODAL:
            return update(state, {
                isOpenTagLibraryModal: {
                    $set: action.payload,
                },
            });

        default:
            return state;
    }
}
