const NAME = 'TagSystem';

export const ADD_TAG_TO_CALCULATOR = `${NAME}/ADD_TAG_TO_CALCULATOR`;

export const FETCH_CURRENCIES = `${NAME}/FETCH_CURRENCIES`;
export const FETCH_CURRENCIES_SUCCESS = `${NAME}/FETCH_CURRENCIES_SUCCESS`;
export const FETCH_CURRENCIES_FAILURE = `${NAME}/FETCH_CURRENCIES_FAILURE`;

export const FETCH_DOCUMENTS = `${NAME}/FETCH_DOCUMENTS`;
export const FETCH_DOCUMENTS_FAILURE = `${NAME}/FETCH_DOCUMENTS_FAILURE`;
export const FETCH_DOCUMENTS_SUCCESS = `${NAME}/FETCH_DOCUMENTS_SUCCESS`;

export const FETCH_TAGS = `${NAME}/FETCH_TAGS`;
export const FETCH_TAGS_FAILURE = `${NAME}/FETCH_TAGS_FAILURE`;
export const FETCH_TAGS_SUCCESS = `${NAME}/FETCH_TAGS_SUCCESS`;

export const FETCH_TAG_FORMATS = `${NAME}/FETCH_TAG_FORMATS`;
export const FETCH_TAG_FORMATS_SUCCESS = `${NAME}/FETCH_TAG_FORMATS_SUCCESS`;
export const FETCH_TAG_FORMATS_FAILURE = `${NAME}/FETCH_TAG_FORMATS_FAILURE`;

export const FETCH_UNITS_FORMATS = `${NAME}/FETCH_UNITS_FORMATS`;
export const FETCH_UNITS_FORMATS_SUCCESS = `${NAME}/FETCH_UNITS_FORMATS_SUCCESS`;
export const FETCH_UNITS_FORMATS_FAILURE = `${NAME}/FETCH_UNITS_FORMATS_FAILURE`;

export const FETCH_DECIMALS_FORMATS = `${NAME}/FETCH_DECIMALS_FORMATS`;
export const FETCH_DECIMALS_FORMATS_SUCCESS = `${NAME}/FETCH_DECIMALS_FORMATS_SUCCESS`;
export const FETCH_DECIMALS_FORMATS_FAILURE = `${NAME}/FETCH_DECIMALS_FORMATS_FAILURE`;

export const FILTER_TAGS = `${NAME}/FILTER_TAGS`;

export const SELECT_CATEGORY = `${NAME}/SELECT_CATEGORY`;
export const SELECT_TAG = `${NAME}/SELECT_TAG`;

export const SET_CONTENT_VIEW_STATE = `${NAME}/SET_CONTENT_VIEW_STATE`;

export const SET_IS_OPEN_MODAL = `${NAME}/SET_IS_OPEN_MODAL`;

export const SET_IS_OPEN_TAG_LIBARY_MODAL = `${NAME}/SET_IS_OPEN_TAG_LIBARY_MODAL`;

export const INITIALIZE_CUSTOM_TAG_FORM = `${NAME}/INITIALIZE_CUSTOM_TAG_FORM`;
export const INITIALIZE_TAG_LIBRARY_FORM = `${NAME}/INITIALIZE_TAG_LIBRARY_FORM`;

export const RESET = `${NAME}/RESET`;
export const RESET_CUSTOM_TAG_FORM = `${NAME}/RESET_CUSTOM_TAG_FORM`;
export const RESET_TAG_LIBRARY_FORM = `${NAME}/RESET_TAG_LIBRARY_FORM`;
