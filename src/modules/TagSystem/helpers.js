import i18next from 'i18next';
import isEmpty from 'lodash/isEmpty';
import isNil from 'lodash/isNil';

import {
    CREATE_CUSTOM_TAG_STATE,
    DOCUMENT_TAGS,
    EDIT_CUSTOM_TAG_STATE,
    EDIT_LIBRARY_TAG_STATE,
    INSERT_LIBRARY_TAG_STATE,
    SMART_DOCUMENTS_CATEGORY,
    TAG_ALL_TEXT_FORMAT_TYPES,
    TAG_CURRENCY_FORMAT_TYPES,
    TAG_ECOLOGIC_TYPES,
    TAG_NUMBER_FORMAT_TYPES,
    TAG_PERCENT_FORMAT_TYPES,
    TAG_TEXT_FORMAT_TYPES,
} from '@constants/tags';

import { hasValue, numberFormat } from '@helpers/utils';

const capitalize = (value) => {
    if (typeof value !== 'string') return '';
    return (
        value.toLowerCase().charAt(0).toUpperCase() +
        value.toLowerCase().slice(1)
    );
};

export const generateFormatter = (tag) => {
    if (TAG_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
        return tag.format;
    }
    if (
        TAG_ECOLOGIC_TYPES.includes(tag.format_type) ||
        TAG_NUMBER_FORMAT_TYPES.includes(tag.format_type) ||
        TAG_PERCENT_FORMAT_TYPES.includes(tag.format_type)
    ) {
        return `${getPatternFormat(tag.decimal_places)} ${tag.unit}`;
    }
    if (TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type)) {
        return `${tag.symbol}${getPatternFormat(tag.decimal_places)} ${
            tag.unit
        }`;
    }
    return '';
};

export const getBorderStyle = (isInsertTagMode, isFromCustomTag) => {
    if (isFromCustomTag || isInsertTagMode) {
        return {};
    }
    return {
        border: 0,
        padding: 0,
        boxShadow: 'none',
    };
};

const getChildNode = (element, num) => {
    let children = element;
    for (var i = 0; i < num; i++) {
        if (children.length > 0 && !isNil(children[0].lastElementChild)) {
            children = children[0].children;
        }
    }
    return children[0];
};

export const getColSize = (formatType, size1, size2) =>
    TAG_ALL_TEXT_FORMAT_TYPES.includes(formatType) ? size1 : size2;

export const getCurrencyByUnit = (currencies, unit) => {
    return (
        currencies.find((currency) => currency.abbreviation === unit) || {
            id: null,
            symbol: '',
        }
    );
};

export const getDecimalFormatsToSelect = (decimalFormats, isInitialazing) => {
    return [
        {
            label: isInitialazing
                ? i18next.t('Loading decimals...')
                : i18next.t('Select decimals'),
            value: '',
            disabled: true,
        },
        ...decimalFormats.map((item) => ({
            label: item.example,
            value: item.type,
        })),
    ];
};

export const getDocumentsToSelect = (
    documents,
    hasFilterByActive,
    isFetching
) => {
    let documentsFiltered = documents;
    if (hasFilterByActive) {
        documentsFiltered = documents.filter((item) => item.is_active);
    }
    return [
        {
            label: isFetching
                ? `${i18next.t('Loading document', { count: 2 })}...`
                : i18next.t('Select a document'),
            value: '',
            disabled: true,
        },
        ...documentsFiltered.map((item) => ({
            label: item.name,
            value: item.id,
            disabled: !item.is_active,
        })),
    ];
};

export const getFilteredCategories = (tags, isForSmartDocuments) => {
    if (!isForSmartDocuments) {
        return Object.keys(tags).filter(
            (category) => category !== SMART_DOCUMENTS_CATEGORY
        );
    }
    return Object.keys(tags);
};

export const getFormattedValue = (isNewTag, defaultCurrencyLocale, tag) => {
    if (TAG_TEXT_FORMAT_TYPES.includes(tag.format_type)) {
        switch (tag.format) {
            case 'MAYÚSCULAS':
                return tag.value.toUpperCase();
            case 'minúsculas':
                return tag.value.toLowerCase();
            case 'Normal':
                return capitalize(tag.value);
            case 'Oración':
                return sentence(tag.value);
            case 'Sunwise':
                return tag.value;
        }
    }
    if (
        TAG_ECOLOGIC_TYPES.includes(tag.format_type) ||
        TAG_NUMBER_FORMAT_TYPES.includes(tag.format_type) ||
        TAG_PERCENT_FORMAT_TYPES.includes(tag.format_type) ||
        (TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type) && isNewTag)
    ) {
        const newValue =
            tag.decimal_places == 0 ? Math.trunc(tag.value) : tag.value;
        return numberFormat(newValue, {
            decimals: tag.decimal_places,
            locale: defaultCurrencyLocale,
            style: 'decimal',
            unit: tag.unit,
        });
    }
    if (
        TAG_CURRENCY_FORMAT_TYPES.includes(tag.format_type) &&
        !isEmpty(tag.unit)
    ) {
        const locale = !isEmpty(tag.currencyLocale)
            ? tag.currencyLocale
            : defaultCurrencyLocale;
        return numberFormat(tag.value, {
            currency: tag.unit,
            decimals: tag.decimal_places,
            locale,
            style: 'currency',
        });
    }
    return tag.value;
};

const getLabelTextForUnits = (formatType, isInitialazing) => {
    if (DOCUMENT_TAGS.includes(formatType) && isInitialazing) {
        return i18next.t('Loading catalogs...');
    }

    if (DOCUMENT_TAGS.includes(formatType) && !isInitialazing) {
        return i18next.t('Select a catalog');
    }

    if (isInitialazing) {
        return i18next.t('Loading units...');
    }

    return i18next.t('Select unit');
};

const getParsedUnitFormats = (currencies, formatType, unitFormats) => {
    if (!['price-kwh', 'watt-price'].includes(formatType)) {
        return unitFormats;
    }
    const unitFiltered = unitFormats.find(
        (item) => item.format_type === formatType
    );
    if (unitFiltered) {
        return currencies.map((item) => ({
            example: `${item.abbreviation}${unitFiltered.unit}`,
            unit: `${item.abbreviation}${unitFiltered.unit}`,
            format_type: formatType,
        }));
    }
    return [];
};

const getPatternFormat = (decimal_places) => {
    if (parseInt(decimal_places) > 0) {
        return `{:0,.${decimal_places}f}`;
    }
    return '{:,}';
};

export const getPropsValueField = (formatType) => {
    if (TAG_ALL_TEXT_FORMAT_TYPES.includes(formatType)) {
        return { type: 'text' };
    }
    return { onlyNumbers: true };
};

const getTagElement = (froalaInstance) => {
    if (froalaInstance) {
        return froalaInstance.$box.find(`.fr-command[selected="true"]`);
    }
    return null;
};

export const getTagFormatsToSelect = (
    isInitialazing,
    tagFormats,
    formatType
) => {
    const type = DOCUMENT_TAGS.includes(formatType) ? 'text' : formatType;
    const items = tagFormats
        .filter((item) => item.format_type === type)
        .map((item) => ({
            label: item.example,
            value: item.format,
        }));
    return [
        {
            label: isInitialazing
                ? i18next.t('Loading formats...')
                : i18next.t('Select format'),
            value: '',
            disabled: true,
        },
        ...items,
    ];
};

export const getTextButton = (contentViewState) => {
    if (contentViewState === CREATE_CUSTOM_TAG_STATE) {
        return i18next.t('Create tag');
    }
    return i18next.t('Save settings');
};

export const getTagLibraryTextButton = (isInsertTagMode, isFromCustomTag) => {
    if (isFromCustomTag) {
        return i18next.t('Insert to formula');
    }
    if (isInsertTagMode) {
        return i18next.t('Insert tag');
    }
    return i18next.t('Save settings');
};

export const getTitleModal = (option) => {
    switch (option) {
        case CREATE_CUSTOM_TAG_STATE:
            return i18next.t('Create smart tags');
        case EDIT_LIBRARY_TAG_STATE:
            return i18next.t('Set up library tag');
        case EDIT_CUSTOM_TAG_STATE:
            return i18next.t('Set up own tag');
        default:
            return i18next.t('Select library tag');
    }
};

export const getUnitsToSelect = (
    currencies,
    formatType,
    isInitialazing,
    unitFormats
) => {
    const items = getParsedUnitFormats(currencies, formatType, unitFormats)
        .filter((item) => item.format_type === formatType)
        .map((item) => ({
            label: item.example,
            value: item.unit,
        }));

    return [
        {
            label: getLabelTextForUnits(formatType, isInitialazing),
            value: '',
            disabled: true,
        },
        ...items,
    ];
};

export const handleClickEraser = (changeInput, data) => {
    const newData = [...data];
    const index = newData.length - 1;
    newData.splice(index, 1);
    changeInput('calculator', newData);
};

export const handleClickOperator = (changeInput, data, operator) => {
    changeInput('calculator', [...data, operator]);
};

export const handleInsertNumber = (changeInput, data, number, setNumber) => {
    changeInput('calculator', [
        ...data,
        { title: number, tag: number, is_operator: true },
    ]);
    setNumber(1);
};

export const handleInsertTagBuild =
    (froalaInstance, setIsOpenModal) => (tag) => {
        const settings = JSON.stringify(tag);
        froalaInstance.selection.restore();
        froalaInstance.html.insert(
            `<span>&nbsp;&nbsp;</span><mark class="fr-command sunwise-tag" data-calculator="${tag.is_new_tag}" data-tag="${tag.tag}" data-settings='${settings}' title="${tag.title}" style="background-color: ${tag.color}">${tag.text_value}</mark><span>&nbsp;&nbsp;</span>`
        );
        froalaInstance.undo.saveStep();
        setIsOpenModal(false);
    };

export const handleEditTagBuild = (froalaInstance, setIsOpenModal) => (tag) => {
    let tagElement = getTagElement(froalaInstance);
    if (tagElement) {
        const settings = JSON.stringify(tag);
        tagElement.attr('contenteditable', false);
        tagElement.attr('data-settings', settings);
        tagElement.attr('title', tag.title);
        tagElement.attr('selected', false);
        tagElement.css('background-color', tag.color);
        if (getChildNode(tagElement, 10)) {
            getChildNode(tagElement, 10).innerText = tag.text_value;
        } else {
            tagElement.text(tag.text_value);
        }
        setIsOpenModal(false);
    }
};

export const handleSubmitCustomTagBuild =
    (contentViewState, tagsLocale, handleEditTag, handleInsertTag) =>
    (values) => {
        const format = generateFormatter(values);
        const newItem = {
            ...values,
            format,
            text_value: getFormattedValue(true, tagsLocale, values),
        };
        if (contentViewState === CREATE_CUSTOM_TAG_STATE) {
            handleInsertTag(newItem);
        } else {
            handleEditTag(newItem);
        }
    };

export const handleSubmitTagLibraryBuild =
    (
        tagsLocale,
        isFromCustomTag,
        contentViewState,
        handleEditTag,
        handleInsertTag,
        prepareCustomTagFromSelectTag
    ) =>
    (values) => {
        const format = generateFormatter(values);
        if (isFromCustomTag) {
            prepareCustomTagFromSelectTag({
                ...values,
                format,
                is_operator: false,
                title: values.label,
            });
        } else {
            const newItem = {
                ...values,
                is_new_tag: false,
                format,
                text_value: getFormattedValue(false, tagsLocale, values),
            };
            if (contentViewState === INSERT_LIBRARY_TAG_STATE) {
                handleInsertTag(newItem);
            } else {
                handleEditTag(newItem);
            }
        }
    };

export const normalizeCurrencyField =
    (changeInput, currencies, names) => (value) => {
        const item = currencies.find((item) => item.id === value);
        if (!isNil(item)) {
            changeInput(names[0], item.locale);
            changeInput(names[1], item.abbreviation);
            changeInput(names[2], item.symbol);
        }

        return value;
    };

export const normalizeDecimalField =
    (changeInput, decimalFormats, name) => (value) => {
        const type = decimalFormats.find((decimal) => decimal.type === value);
        if (!isNil(type)) {
            changeInput(name, type.decimal_places);
        }
        return value;
    };

export const normalizeDocumentField = (changeInput, documents) => (value) => {
    const document = documents.find((item) => item.id === value);
    if (!isNil(document)) {
        changeInput('value', document.name);
        changeInput('default_value', document.name);
    }
    return value;
};

export const searchTags = (tags, searchText = '', filterTags) => {
    if (searchText === '') {
        filterTags(tags);
        return;
    }

    const tempFiltered = {};

    Object.keys(tags).forEach((category) => {
        let hasItems = false;
        let items = {};

        Object.keys(tags[category].values).forEach((subcategory) => {
            if (
                subcategory.toUpperCase().search(searchText.toUpperCase()) >=
                    0 ||
                tags[category].values[subcategory].label
                    .toUpperCase()
                    .search(searchText.toUpperCase()) >= 0 ||
                (tags[category].values[subcategory].description &&
                    tags[category].values[subcategory].description
                        .toUpperCase()
                        .search(searchText.toUpperCase()) >= 0) ||
                (tags[category].values[subcategory].format_type &&
                    tags[category].values[subcategory].format_type
                        .toUpperCase()
                        .search(searchText.toUpperCase()) >= 0)
            ) {
                hasItems = true;
                items[subcategory] = tags[category].values[subcategory];
            }
        });

        if (hasItems) {
            tempFiltered[category] = {
                label: tags[category].label,
                values: items,
            };
        }
    });

    filterTags(tempFiltered);
};

const sentence = (value) => {
    if (typeof value !== 'string') return '';
    return value.split(' ').map(capitalize).join(' ');
};

export const updateMultipleCurrencyTags = (calculator, currencies) => {
    const items = calculator.map((item) => {
        if (
            !item.is_operator &&
            TAG_CURRENCY_FORMAT_TYPES.includes(item.format_type) &&
            hasValue(item, 'currency') &&
            hasValue(item, 'unit')
        ) {
            const currency = getCurrencyByUnit(currencies, item.unit);
            return {
                ...item,
                currency: currency.id,
                symbol: currency.symbol,
                locale: currency.locale,
            };
        }
        return item;
    });
    return items;
};

export const updateTag = (changeInput, data) => {
    const formula = data.map((e) => e.tag).join(' ');
    changeInput('tag', formula);
};
