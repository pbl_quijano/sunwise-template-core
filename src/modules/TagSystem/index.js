import PropTypes from 'prop-types';
import { useContext, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import ModalBootstrap from '@components/ModalBootstrap';

import {
    CREATE_CUSTOM_TAG_STATE,
    HIDDEN_MODAL_STATE,
    INSERT_LIBRARY_TAG_STATE,
} from '@constants/tags';
import { MULTIPROPOSAL_TYPE } from '@constants/types';

import { GeneralContext } from '@helpers/contexts';

import * as mainActions from '@main/actions';

import * as actions from './actions';
import ContentView from './components/ContentView';
import {
    getTitleModal,
    handleEditTagBuild,
    handleInsertTagBuild,
} from './helpers';
import * as selectors from './selectors';

export const ModalTitle = styled.h3`
    color: #002438;
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 0;
    line-height: 32px;
    margin-bottom: 0;
`;

const TagSystem = ({
    availableCatalogs = [],
    contentViewState,
    editionLevel,
    fetchTagsCurrency,
    froalaInstance,
    initialize,
    isFetchingCurrencies,
    isFetchingDecimalFormats,
    isFetchingTagFormats,
    isFetchingUnitFormats,
    isOpenModal,
    setContentViewState,
    setIsOpenModal,
    templateType,
}) => {
    const { t } = useTranslation();
    const { tagsLocale } = useContext(GeneralContext);
    useEffect(() => {
        if (!tagsLocale) {
            fetchTagsCurrency();
        }
    }, [tagsLocale]);

    if (!tagsLocale) {
        return null;
    }
    return (
        <ModalBootstrap
            closeButton
            className={`modal-${contentViewState}`}
            onEnter={() => initialize()}
            onHide={() => {
                setContentViewState(HIDDEN_MODAL_STATE);
                setIsOpenModal(false);
            }}
            onExit={() => setContentViewState(HIDDEN_MODAL_STATE)}
            show={isOpenModal}
            size={contentViewState === INSERT_LIBRARY_TAG_STATE ? 'xl' : 'lg'}
            title={
                <ModalTitle>
                    {contentViewState === CREATE_CUSTOM_TAG_STATE && (
                        <button
                            className="btn btn-transparent p-0 mr-2"
                            onClick={() =>
                                setContentViewState(INSERT_LIBRARY_TAG_STATE)
                            }
                            title={t('Back')}
                            type="button"
                        >
                            <i className="fas fa-arrow-left mr-2"></i>
                        </button>
                    )}
                    {getTitleModal(contentViewState)}
                </ModalTitle>
            }
        >
            <ContentView
                catalogs={availableCatalogs}
                contentViewState={contentViewState}
                editionLevel={editionLevel}
                handleEditTag={handleEditTagBuild(
                    froalaInstance,
                    setIsOpenModal
                )}
                handleInsertTag={handleInsertTagBuild(
                    froalaInstance,
                    setIsOpenModal
                )}
                hasProposalSelector={templateType === MULTIPROPOSAL_TYPE}
                isInitialazing={
                    isFetchingCurrencies ||
                    isFetchingDecimalFormats ||
                    isFetchingTagFormats ||
                    isFetchingUnitFormats
                }
                tagsLocale={tagsLocale}
                templateType={templateType}
            />
        </ModalBootstrap>
    );
};

const mapStateToProps = createStructuredSelector({
    contentViewState: selectors.getContentViewState,
    isFetchingCurrencies: selectors.getIsFetchingCurrencies,
    isFetchingDecimalFormats: selectors.getIsFetchingDecimalFormats,
    isFetchingTagFormats: selectors.getIsFetchingTagFormats,
    isFetchingUnitFormats: selectors.getIsFetchingUnitFormats,
    isOpenModal: selectors.getIsOpenModal,
});

const mapDispatchToProps = (dispatch) => ({
    fetchTagsCurrency: () => dispatch(mainActions.fetchTagsCurrency()),
    initialize: () => dispatch(actions.initialize()),
    reset: () => dispatch(actions.reset()),
    setContentViewState: (contentViewState) =>
        dispatch(actions.setContentViewState(contentViewState)),
    setIsOpenModal: (isOpenModal) =>
        dispatch(actions.setIsOpenModal(isOpenModal)),
});

TagSystem.propTypes = {
    availableCatalogs: PropTypes.array,
    contentViewState: PropTypes.number,
    editionLevel: PropTypes.string,
    fetchTagsCurrency: PropTypes.func,
    froalaInstance: PropTypes.object,
    initialize: PropTypes.func,
    isFetchingCurrencies: PropTypes.bool,
    isFetchingDecimalFormats: PropTypes.bool,
    isFetchingTagFormats: PropTypes.bool,
    isFetchingUnitFormats: PropTypes.bool,
    isOpenModal: PropTypes.bool,
    setContentViewState: PropTypes.func,
    setIsOpenModal: PropTypes.func,
    templateType: PropTypes.number,
};

export default connect(mapStateToProps, mapDispatchToProps)(TagSystem);
