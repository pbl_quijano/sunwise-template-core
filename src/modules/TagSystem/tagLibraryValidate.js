import isUndefined from 'lodash/isUndefined';
import { object, string } from 'yup';

import {
    DOCUMENT_TAGS,
    TAG_ALL_TEXT_FORMAT_TYPES,
    TAG_CURRENCY_FORMAT_TYPES,
    TAG_NUMBER_FORMAT_TYPES,
    TAG_PERCENT_FORMAT_TYPES,
    TAG_TEXT_FORMAT_TYPES,
} from '@constants/tags';

import schemaValidate from '@helpers/schemaValidate';

export default schemaValidate(
    ({ getMinValueText, NUMBER_TYPE_TEXT, REQUIRED_TEXT }) =>
        object().shape({
            proposal_number: string().when(
                ['hasProposalSelector', 'format_type'],
                (hasProposalSelector, formatType, schema) => {
                    if (
                        !isUndefined(hasProposalSelector) &&
                        hasProposalSelector &&
                        !isUndefined(formatType) &&
                        !DOCUMENT_TAGS.includes(formatType)
                    ) {
                        return schema.required(REQUIRED_TEXT).nullable();
                    }
                    return schema.nullable();
                }
            ),
            catalog_type: string().when('format_type', (value, schema) => {
                if (DOCUMENT_TAGS.includes(value)) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
            smart_tag: string().when('format_type', (value, schema) => {
                if (DOCUMENT_TAGS.includes(value)) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
            value: string().when('format_type', (value, schema) => {
                if (TAG_ALL_TEXT_FORMAT_TYPES.includes(value)) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema
                    .typeError(NUMBER_TYPE_TEXT)
                    .required(REQUIRED_TEXT)
                    .min(0, getMinValueText(0))
                    .nullable();
            }),
            unit: string().when('format_type', (value, schema) => {
                if (
                    TAG_NUMBER_FORMAT_TYPES.includes(value) ||
                    TAG_PERCENT_FORMAT_TYPES.includes(value)
                ) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
            currency: string().when('format_type', (value, schema) => {
                if (TAG_CURRENCY_FORMAT_TYPES.includes(value)) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
            decimal_type: string().when('format_type', (value, schema) => {
                if (
                    TAG_CURRENCY_FORMAT_TYPES.includes(value) ||
                    TAG_NUMBER_FORMAT_TYPES.includes(value) ||
                    TAG_PERCENT_FORMAT_TYPES.includes(value)
                ) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
            format: string().when('format_type', (value, schema) => {
                if (TAG_TEXT_FORMAT_TYPES.includes(value)) {
                    return schema.required(REQUIRED_TEXT).nullable();
                }
                return schema.nullable();
            }),
        })
);
