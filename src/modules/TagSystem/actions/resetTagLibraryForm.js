import { RESET_TAG_LIBRARY_FORM } from '../actionTypes';

export default () => ({ type: RESET_TAG_LIBRARY_FORM });
