import { EDIT_CUSTOM_TAG_STATE } from '@constants/tags';

import initializeCustomTagForm from './initializeCustomTagForm';
import setContentViewState from './setContentViewState';
import setIsOpenModal from './setIsOpenModal';

export default (values) => (dispatch) => {
    dispatch(setContentViewState(EDIT_CUSTOM_TAG_STATE));
    dispatch(setIsOpenModal(true));
    dispatch(initializeCustomTagForm(values, true));
};
