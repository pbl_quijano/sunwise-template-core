import { RESET_CUSTOM_TAG_FORM } from '../actionTypes';

export default () => ({ type: RESET_CUSTOM_TAG_FORM });
