import fetchCurrencies from './fetchCurrencies';
import fetchDecimalFormats from './fetchDecimalFormats';
import fetchTagFormats from './fetchTagFormats';
import fetchUnitFormats from './fetchUnitFormats';

export default () => (dispatch) => {
    dispatch(fetchDecimalFormats());
    dispatch(fetchTagFormats());
    dispatch(fetchUnitFormats());
    dispatch(fetchCurrencies());
};
