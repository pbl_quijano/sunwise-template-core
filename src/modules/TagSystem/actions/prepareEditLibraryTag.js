import { EDIT_LIBRARY_TAG_STATE } from '@constants/tags';

import initializeTagLibraryForm from './initializeTagLibraryForm';
import setContentViewState from './setContentViewState';
import setIsOpenModal from './setIsOpenModal';

export default (values) => (dispatch) => {
    dispatch(setContentViewState(EDIT_LIBRARY_TAG_STATE));
    dispatch(setIsOpenModal(true));
    dispatch(initializeTagLibraryForm(values));
};
