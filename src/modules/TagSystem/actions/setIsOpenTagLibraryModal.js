import { SET_IS_OPEN_TAG_LIBARY_MODAL } from '../actionTypes';

export default (isOpenModal) => ({
    type: SET_IS_OPEN_TAG_LIBARY_MODAL,
    payload: isOpenModal,
});
