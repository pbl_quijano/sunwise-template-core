import { FILTER_TAGS } from '../actionTypes';

export default (values) => ({ type: FILTER_TAGS, payload: values });
