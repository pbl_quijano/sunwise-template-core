import { getCurrencies } from '@api';

import {
    FETCH_CURRENCIES,
    FETCH_CURRENCIES_SUCCESS,
    FETCH_CURRENCIES_FAILURE,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_CURRENCIES });

    getCurrencies(getState())()
        .then((response) =>
            dispatch({
                type: FETCH_CURRENCIES_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({
                type: FETCH_CURRENCIES_FAILURE,
                payload: error.response.data.errors,
            })
        );
};
