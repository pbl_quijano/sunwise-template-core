import { TAG_CURRENCY_FORMAT_TYPES } from '@constants/tags';

import { hasValue } from '@helpers/utils';

import { INITIALIZE_TAG_LIBRARY_FORM } from '../actionTypes';
import { getCurrencyByUnit } from '../helpers';
import * as selectors from '../selectors';

export default (values) => (dispatch, getState) => {
    if (
        TAG_CURRENCY_FORMAT_TYPES.includes(values.format_type) &&
        hasValue(values, 'currency') &&
        hasValue(values, 'unit')
    ) {
        const currencies = selectors.getCurrenciesData(getState());

        const newCurrency = getCurrencyByUnit(currencies, values.unit);
        const currency = newCurrency.id;
        const symbol = newCurrency.symbol;

        dispatch({
            type: INITIALIZE_TAG_LIBRARY_FORM,
            payload: { ...values, currency, symbol },
        });
    } else {
        dispatch({ type: INITIALIZE_TAG_LIBRARY_FORM, payload: { ...values } });
    }
};
