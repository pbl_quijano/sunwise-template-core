import { INSERT_LIBRARY_TAG_STATE } from '@constants/tags';

import setContentViewState from './setContentViewState';
import setIsOpenModal from './setIsOpenModal';

export default () => (dispatch) => {
    dispatch(setContentViewState(INSERT_LIBRARY_TAG_STATE));
    dispatch(setIsOpenModal(true));
};
