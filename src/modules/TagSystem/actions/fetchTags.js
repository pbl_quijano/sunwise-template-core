import { getTags } from '@api';

import {
    FETCH_TAGS,
    FETCH_TAGS_FAILURE,
    FETCH_TAGS_SUCCESS,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_TAGS });

    return getTags(getState())()
        .then((response) =>
            dispatch({
                type: FETCH_TAGS_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({
                type: FETCH_TAGS_FAILURE,
                payload: error,
            })
        );
};
