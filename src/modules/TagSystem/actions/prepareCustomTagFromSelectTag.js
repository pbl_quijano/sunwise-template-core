import addTagToCalculator from './addTagToCalculator';
import setIsOpenTagLibraryModal from './setIsOpenTagLibraryModal';

export default (tag) => (dispatch) => {
    dispatch(addTagToCalculator(tag));
    dispatch(setIsOpenTagLibraryModal(false));
};
