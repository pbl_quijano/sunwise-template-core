import { getTagsFormats } from '@api';

import {
    FETCH_TAG_FORMATS,
    FETCH_TAG_FORMATS_SUCCESS,
    FETCH_TAG_FORMATS_FAILURE,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_TAG_FORMATS });

    getTagsFormats(getState())()
        .then((response) =>
            dispatch({
                type: FETCH_TAG_FORMATS_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({
                type: FETCH_TAG_FORMATS_FAILURE,
                payload: error.data.errors,
            })
        );
};
