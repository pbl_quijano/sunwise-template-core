import { SET_IS_OPEN_MODAL } from '../actionTypes';

export default (isOpenModal) => ({
    type: SET_IS_OPEN_MODAL,
    payload: isOpenModal,
});
