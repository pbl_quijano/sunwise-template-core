import { getUnitFormats } from '@api';

import {
    FETCH_UNITS_FORMATS,
    FETCH_UNITS_FORMATS_SUCCESS,
    FETCH_UNITS_FORMATS_FAILURE,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_UNITS_FORMATS });

    getUnitFormats(getState())()
        .then((response) =>
            dispatch({
                type: FETCH_UNITS_FORMATS_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({
                type: FETCH_UNITS_FORMATS_FAILURE,
                payload: error.data.errors,
            })
        );
};
