import { INITIALIZE_CUSTOM_TAG_FORM } from '../actionTypes';
import { updateMultipleCurrencyTags } from '../helpers';
import * as selectors from '../selectors';

import resetCustomTagForm from './resetCustomTagForm';

export default (values, isEditMode) => (dispatch, getState) => {
    dispatch(resetCustomTagForm());
    const { calculator } = values;
    const currencies = selectors.getCurrenciesData(getState());

    dispatch({
        type: INITIALIZE_CUSTOM_TAG_FORM,
        payload: {
            ...values,
            calculator: isEditMode
                ? updateMultipleCurrencyTags(calculator, currencies)
                : calculator,
        },
    });
};
