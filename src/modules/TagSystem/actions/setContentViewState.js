import { SET_CONTENT_VIEW_STATE } from '../actionTypes';

export default (contentViewState) => ({
    type: SET_CONTENT_VIEW_STATE,
    payload: contentViewState,
});
