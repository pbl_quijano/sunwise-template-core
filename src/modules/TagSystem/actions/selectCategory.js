import { SELECT_CATEGORY } from '../actionTypes';

export default (value) => ({ type: SELECT_CATEGORY, payload: value });
