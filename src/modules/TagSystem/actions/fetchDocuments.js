import { getCatalogs } from '@api';

import {
    FETCH_DOCUMENTS,
    FETCH_DOCUMENTS_FAILURE,
    FETCH_DOCUMENTS_SUCCESS,
} from '../actionTypes';

export default (type) => (dispatch, getState) => {
    dispatch({ type: FETCH_DOCUMENTS });

    getCatalogs(getState())(type)
        .then((response) =>
            dispatch({
                type: FETCH_DOCUMENTS_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({ type: FETCH_DOCUMENTS_FAILURE, payload: error })
        );
};
