import { SELECT_TAG } from '../actionTypes';

import initializeTagLibraryForm from './initializeTagLibraryForm';
import resetTagLibraryForm from './resetTagLibraryForm';

export default (values, hasProposalSelector) => (dispatch) => {
    dispatch(resetTagLibraryForm());
    dispatch({ type: SELECT_TAG, payload: values && values.tag });
    if (values) {
        dispatch(
            initializeTagLibraryForm({
                ...values,
                default_value: values.default_label,
                title: values.label,
                hasProposalSelector,
            })
        );
    }
};
