import { getDecimalFormats } from '@api';

import {
    FETCH_DECIMALS_FORMATS,
    FETCH_DECIMALS_FORMATS_SUCCESS,
    FETCH_DECIMALS_FORMATS_FAILURE,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_DECIMALS_FORMATS });

    getDecimalFormats(getState())()
        .then((response) =>
            dispatch({
                type: FETCH_DECIMALS_FORMATS_SUCCESS,
                payload: response.data.data,
            })
        )
        .catch((error) =>
            dispatch({
                type: FETCH_DECIMALS_FORMATS_FAILURE,
                payload: error.data.errors,
            })
        );
};
