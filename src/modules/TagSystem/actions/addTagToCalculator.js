import { change } from 'redux-form';

import * as selectors from '../selectors';

export default (values) => (dispatch, getState) => {
    const calculator = selectors.getDataCalculator(getState());
    const newData = [...calculator, values];
    dispatch(change('custom-tag-form', 'calculator', newData));
};
