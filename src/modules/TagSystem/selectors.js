import isObject from 'lodash/isObject';
import uniqBy from 'lodash/uniqBy';
import { getFormSyncErrors, getFormValues } from 'redux-form';
import { createSelector } from 'reselect';

import * as selectors from '@main/selectors';

export const getModel = createSelector(
    selectors.getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.tagSystem
);

export const getContentViewState = createSelector(
    getModel,
    (model) => model.contentViewState
);

export const getIsOpenModal = createSelector(
    getModel,
    (model) => model.isOpenModal
);

export const getTagFormats = createSelector(
    getModel,
    (model) => model.fetchTagFormats
);

export const getIsFetchingTagFormats = createSelector(
    getTagFormats,
    (fetchTagFormats) => fetchTagFormats.isFetching
);

export const getTagFormatsData = createSelector(
    getTagFormats,
    (fetchTagFormats) => {
        return uniqBy(
            Object.keys(fetchTagFormats.data).reduce((acc, format_type) => {
                return [
                    ...acc,
                    ...fetchTagFormats.data[format_type].map((item) => ({
                        id: item.id,
                        example: item.example,
                        format: item.format,
                        format_type: format_type,
                        number_value: item.number_value,
                        unit: item.unit,
                    })),
                ];
            }, []),
            'id'
        );
    }
);

export const getUnitFormats = createSelector(
    getModel,
    (model) => model.fetchUnitFormats
);

export const getIsFetchingUnitFormats = createSelector(
    getUnitFormats,
    (fetchUnitFormats) => fetchUnitFormats.isFetching
);

export const getUnitFormatsData = createSelector(
    getUnitFormats,
    (fetchUnitFormats) => {
        return uniqBy(
            Object.keys(fetchUnitFormats.data).reduce((acc, format_type) => {
                return [
                    ...acc,
                    ...fetchUnitFormats.data[format_type].map((item) => {
                        const example = isObject(item) ? item.example : item;
                        const unit = isObject(item)
                            ? item.companies_catalog
                            : item;
                        return {
                            example: example,
                            unit: unit,
                            format_type: format_type,
                        };
                    }),
                ];
            }, []),
            'unit'
        );
    }
);

export const getDecimalFormats = createSelector(
    getModel,
    (model) => model.fetchDecimalFormats
);

export const getIsFetchingDecimalFormats = createSelector(
    getDecimalFormats,
    (fetchDecimalFormats) => fetchDecimalFormats.isFetching
);

export const getDecimalFormatsData = createSelector(
    getDecimalFormats,
    (fetchDecimalFormats) => fetchDecimalFormats.data
);

export const getFetchCurrencies = createSelector(
    getModel,
    (model) => model.fetchCurrencies
);

export const getIsFetchingCurrencies = createSelector(
    getFetchCurrencies,
    (fetchCurrencies) => fetchCurrencies.isFetching
);

export const getFetchCurrenciesData = createSelector(
    getFetchCurrencies,
    (fetchCurrencies) => fetchCurrencies.data || []
);

export const getCurrenciesData = createSelector(
    getFetchCurrenciesData,
    (currencies) =>
        currencies
            .filter(
                (item) => item.is_enabled && !item.is_deleted && item.currency
            )
            .map((item) => ({
                ...item.currency,
                dollar_price: item.dollar_price,
            })) || []
);

export const getCustomTagFormErrors = createSelector(
    (state) => getFormSyncErrors('custom-tag-form')(state),
    (errors) => errors || {}
);

export const getTagLibraryFormErrors = createSelector(
    (state) => getFormSyncErrors('tag-library-form')(state),
    (errors) => errors || {}
);

export const getInitialCustomTagValues = createSelector(
    getModel,
    (model) => model.initialCustomTagValues
);

export const getInitialTagLibraryValues = createSelector(
    getModel,
    (model) => model.initialTagLibraryValues
);

export const getCustomTagValues = createSelector(
    (state) => getFormValues('custom-tag-form')(state),
    (values) => values || {}
);

export const getTagLibraryValues = createSelector(
    (state) => getFormValues('tag-library-form')(state),
    (values) => values || {}
);

export const getDataCalculator = createSelector(
    getCustomTagValues,
    (model) => model.calculator
);

export const getIsOpenTagLibraryModal = createSelector(
    getModel,
    (model) => model.isOpenTagLibraryModal
);

export const getDocuments = createSelector(
    getModel,
    (model) => model.fetchDocuments
);

export const getDocumentsData = createSelector(
    getDocuments,
    (model) => model.data
);

export const getIsFetchingDocuments = createSelector(
    getDocuments,
    (model) => model.isFetching
);

export const getFilteredTags = createSelector(
    getModel,
    (model) => model.filteredTags
);

export const getFetchTags = createSelector(
    getModel,
    (model) => model.fetchTags
);

export const getDataFetchTags = createSelector(
    getFetchTags,
    (fetchTags) => fetchTags.data || []
);

export const getIsFetchingTags = createSelector(
    getFetchTags,
    (fetchTags) => fetchTags.isFetching
);

export const getSelectedCategory = createSelector(
    getModel,
    (model) => model.selectedCategory
);

export const getSelectedTag = createSelector(
    getModel,
    (model) => model.selectedTag
);
