import get from 'lodash/get';

import { getMe } from '@api';

import { FETCH_CHART_CURRENCY } from '../actionTypes';

export default () => (dispatch, getState) => {
    getMe(getState())().then((response) => {
        dispatch({
            type: FETCH_CHART_CURRENCY,
            payload: get(response.data, 'currency_company_locale', {}),
        });
    });
};
