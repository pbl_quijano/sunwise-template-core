import get from 'lodash/get';

import { getCompaniesMe, getOfferComplements } from '@api';

import { FETCH_TABLE_CURRENCY, FETCH_TAGS_CURRENCY } from '../actionTypes';

export default (offerId, token) => (dispatch, getState) => {
    if (offerId) {
        getOfferComplements(getState())(offerId).then((response) => {
            const defaultCurrency = get(response.data, 'currency', {});
            dispatch({
                type: FETCH_TABLE_CURRENCY,
                payload: defaultCurrency,
            });
        });
    } else if (token) {
        getCompaniesMe(getState())().then((response) => {
            const defaultCurrency = get(response.data, 'currency', {});
            dispatch({
                type: FETCH_TABLE_CURRENCY,
                payload: defaultCurrency,
            });
            dispatch({
                type: FETCH_TAGS_CURRENCY,
                payload: defaultCurrency,
            });
        });
    }
};
