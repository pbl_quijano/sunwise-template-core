import { getImageGallery } from '@api';

import { handleFileURL } from '@helpers/utils';

import {
    FETCH_IMAGE_GALLERY,
    FETCH_IMAGE_GALLERY_FAILURE,
    FETCH_IMAGE_GALLERY_SUCCESS,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_IMAGE_GALLERY });
    getImageGallery(getState())()
        .then((response) => {
            dispatch({
                type: FETCH_IMAGE_GALLERY_SUCCESS,
                payload: Object.keys(response.data.data).reduce(
                    (acc, key) => [
                        ...acc,
                        ...response.data.data[key].map(
                            ({ id, name, url_file }) => ({
                                id,
                                name,
                                urlFile: handleFileURL(
                                    url_file,
                                    process.env.REACT_APP_S3_MEDIA_PATH
                                ),
                            })
                        ),
                    ],
                    []
                ),
            });
        })
        .catch((error) =>
            dispatch({
                type: FETCH_IMAGE_GALLERY_FAILURE,
                payload: error.response.data.errors,
            })
        );
};
