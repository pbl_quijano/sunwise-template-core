import { getChartWidgets } from '@api';

import {
    FETCH_CHART_WIDGETS,
    FETCH_CHART_WIDGETS_SUCCESS,
    FETCH_CHART_WIDGETS_FAILURE,
} from '../actionTypes';

export default (lang) => (dispatch, getState) => {
    dispatch({ type: FETCH_CHART_WIDGETS });

    return getChartWidgets(getState())(lang)
        .then((response) => {
            dispatch({
                type: FETCH_CHART_WIDGETS_SUCCESS,
                payload: response.data.data,
            });
        })
        .catch((error) =>
            dispatch({
                type: FETCH_CHART_WIDGETS_FAILURE,
                payload: error,
            })
        );
};
