import { getCatalogs } from '@api';

import {
    FETCH_CATALOGS,
    FETCH_CATALOGS_FAILURE,
    FETCH_CATALOGS_SUCCESS,
} from '../actionTypes';

export default (type) => (dispatch, getState) => {
    dispatch({ type: FETCH_CATALOGS });

    getCatalogs(getState())(type)
        .then((response) => {
            dispatch({
                type: FETCH_CATALOGS_SUCCESS,
                payload: response.data.data,
            });
        })
        .catch((error) =>
            dispatch({ type: FETCH_CATALOGS_FAILURE, payload: error })
        );
};
