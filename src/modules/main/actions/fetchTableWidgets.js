import { getTableWidgets } from '@api';

import {
    FETCH_TABLE_WIDGETS,
    FETCH_TABLE_WIDGETS_SUCCESS,
    FETCH_TABLE_WIDGETS_FAILURE,
} from '../actionTypes';

export default (lang) => (dispatch, getState) => {
    dispatch({ type: FETCH_TABLE_WIDGETS });

    return getTableWidgets(getState())(lang)
        .then((response) => {
            dispatch({
                type: FETCH_TABLE_WIDGETS_SUCCESS,
                payload: response.data.data,
            });
        })
        .catch((error) =>
            dispatch({
                type: FETCH_TABLE_WIDGETS_FAILURE,
                payload: error,
            })
        );
};
