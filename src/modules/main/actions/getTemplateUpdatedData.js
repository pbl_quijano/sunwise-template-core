import * as templateCoreSelectors from '@templateCore/selectors';

export default () => (_, getState) => {
    const templateData = templateCoreSelectors.getTemplateUpdatingContentData(
        getState()
    );
    return templateData;
};
