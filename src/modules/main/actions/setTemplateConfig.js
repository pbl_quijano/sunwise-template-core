import { SET_TEMPLATE_CONFIG } from '../actionTypes';

export default (templateConfig) => ({
    type: SET_TEMPLATE_CONFIG,
    payload: templateConfig,
});
