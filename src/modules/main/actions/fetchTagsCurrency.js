import get from 'lodash/get';

import { getCompaniesMe } from '@api';

import { FETCH_TAGS_CURRENCY } from '../actionTypes';

export default () => (dispatch, getState) => {
    getCompaniesMe(getState())().then((response) => {
        const defaultCurrency = get(response.data, 'currency', {});

        dispatch({
            type: FETCH_TAGS_CURRENCY,
            payload: defaultCurrency,
        });
    });
};
