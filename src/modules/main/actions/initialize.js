import { PROPOSAL_SUMMARY_TYPE } from '@constants/tags';

import {
    INITIALIZE,
    INITIALIZE_SUCCESS,
    INITIALIZE_FAILURE,
} from '../actionTypes';
import * as selectors from '../selectors';

import fetchCatalogs from './fetchCatalogs';
import fetchChartCurrency from './fetchChartCurrency';
import fetchChartWidgets from './fetchChartWidgets';
import fetchCompanyImageGallery from './fetchImageGallery';
import fetchTableCurrency from './fetchTableCurrency';
import fetchTableWidgets from './fetchTableWidgets';

const getActions = (templateData, token, offerId, dispatch) => {
    let actions = [
        dispatch(fetchChartCurrency()),
        dispatch(fetchTableWidgets(templateData.language)),
        dispatch(fetchChartWidgets(templateData.language)),
    ];
    if (offerId) {
        return [dispatch(fetchTableCurrency(offerId, token)), ...actions];
    }
    if (token) {
        return [
            dispatch(fetchCompanyImageGallery()),
            dispatch(fetchCatalogs(PROPOSAL_SUMMARY_TYPE)),
            ...actions,
        ];
    }
    return actions;
};
export default (templateData, offerId) => (dispatch, getState) => {
    dispatch({ type: INITIALIZE, payload: templateData });
    const token = selectors.getToken(getState());
    Promise.all(getActions(templateData, token, offerId, dispatch))
        .then(() => {
            dispatch({ type: INITIALIZE_SUCCESS });
        })
        .catch(() => {
            dispatch({ type: INITIALIZE_FAILURE });
        });
};
