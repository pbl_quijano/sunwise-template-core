import { createSelector } from 'reselect';

export const getLibState = (state) => state.sunwiseTemplateCore;

export const getModel = createSelector(
    getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.main
);

export const getBaseUrl = createSelector(getModel, (model) => model.baseUrl);

export const getToken = createSelector(getModel, (model) => model.token);

export const getLanguage = createSelector(getModel, (model) => model.language);

export const getCurrencyConfig = createSelector(
    getModel,
    (model) => model.currencyConfig
);

export const getGoogleApiKey = createSelector(
    getModel,
    (model) => model.googleApiKey
);

export const getFroalaLicenseKey = createSelector(
    getModel,
    (model) => model.froalaLicenseKey
);

export const getFetchCustomTemplate = createSelector(
    getModel,
    (model) => model.fetchCustomTemplate
);

export const getDataFetchCustomTemplate = createSelector(
    getFetchCustomTemplate,
    (fetchCustomTemplate) => fetchCustomTemplate.data || null
);

export const getFetchTableWidgets = createSelector(
    getModel,
    (model) => model.fetchTableWidgets
);

export const getDataFetchTableWidgets = createSelector(
    getFetchTableWidgets,
    (fetchTableWidgets) => fetchTableWidgets.data || []
);

export const getFetchChartWidgets = createSelector(
    getModel,
    (model) => model.fetchChartWidgets
);

export const getDataFetchChartWidgets = createSelector(
    getFetchChartWidgets,
    (fetchChartWidgets) => fetchChartWidgets.data || []
);

export const getIsInitializing = createSelector(getModel, (data) => {
    return data.initializing;
});

export const getFetchImageGallery = createSelector(
    getModel,
    (model) => model.fetchImageGallery
);

export const getIsFetchingImageWidgetGallery = createSelector(
    getFetchImageGallery,
    (fetchImageGallery) => fetchImageGallery.isFetching
);

export const getFetchImageWidgetGalleryData = createSelector(
    getFetchImageGallery,
    (fetchImageGallery) => fetchImageGallery.data
);

export const getFetchCatalogs = createSelector(
    getModel,
    (model) => model.fetchCatalogs
);

export const getIsFetchingCatalogs = createSelector(
    getFetchCatalogs,
    (fetchCatalogs) => fetchCatalogs.isFetching
);

export const getFetchCatalogData = createSelector(
    getFetchCatalogs,
    (fetchCatalogs) => fetchCatalogs.data
);
