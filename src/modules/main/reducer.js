import update from 'immutability-helper';

import { getCurrencyIso, getCurrencyLocale } from '@helpers/utils';

import {
    FETCH_CATALOGS,
    FETCH_CATALOGS_FAILURE,
    FETCH_CATALOGS_SUCCESS,
    FETCH_CHART_CURRENCY,
    FETCH_CHART_WIDGETS,
    FETCH_CHART_WIDGETS_FAILURE,
    FETCH_CHART_WIDGETS_SUCCESS,
    FETCH_IMAGE_GALLERY,
    FETCH_IMAGE_GALLERY_FAILURE,
    FETCH_IMAGE_GALLERY_SUCCESS,
    FETCH_TABLE_CURRENCY,
    FETCH_TABLE_WIDGETS,
    FETCH_TABLE_WIDGETS_FAILURE,
    FETCH_TABLE_WIDGETS_SUCCESS,
    FETCH_TAGS_CURRENCY,
    INITIALIZE,
    INITIALIZE_FAILURE,
    INITIALIZE_SUCCESS,
    RESET,
    SET_TEMPLATE_CONFIG,
} from './actionTypes';

const INITIAL_STATE = {
    currencyConfig: {},
    token: null,
    baseUrl: null,
    language: null,
    googleApiKey: null,
    froalaLicenseKey: '',
    fetchCatalogs: {
        isFetching: false,
        data: [],
        error: null,
    },
    fetchTableWidgets: {
        isFetching: true,
        data: [],
        errors: [],
    },
    fetchChartWidgets: {
        isFetching: true,
        data: [],
        errors: [],
    },
    fetchImageGallery: {
        isFetching: false,
        data: [],
        errors: null,
    },
    initializing: true,
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_CATALOGS:
            return update(state, {
                fetchCatalogs: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_CATALOGS_SUCCESS:
            return update(state, {
                fetchCatalogs: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_CATALOGS_FAILURE:
            return update(state, {
                fetchCatalogs: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_CHART_CURRENCY: {
            const chartIso = getCurrencyIso(action.payload);
            const chartLocale = getCurrencyLocale(action.payload);
            return update(state, {
                currencyConfig: {
                    $merge: {
                        chartIso,
                        chartLocale,
                    },
                },
            });
        }
        case FETCH_IMAGE_GALLERY:
            return update(state, {
                fetchImageGallery: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_IMAGE_GALLERY_SUCCESS:
            return update(state, {
                fetchImageGallery: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_IMAGE_GALLERY_FAILURE:
            return update(state, {
                fetchImageGallery: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_TABLE_CURRENCY: {
            const tableIso = getCurrencyIso(action.payload);
            const tableLocale = getCurrencyLocale(action.payload);
            return update(state, {
                currencyConfig: {
                    $merge: {
                        tableIso,
                        tableLocale,
                    },
                },
            });
        }

        case FETCH_TABLE_WIDGETS:
            return update(state, {
                fetchTableWidgets: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_TABLE_WIDGETS_SUCCESS:
            return update(state, {
                fetchTableWidgets: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_TABLE_WIDGETS_FAILURE:
            return update(state, {
                fetchTableWidgets: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case FETCH_TAGS_CURRENCY: {
            const tagsLocale = getCurrencyLocale(action.payload);
            return update(state, {
                currencyConfig: {
                    $merge: {
                        tagsLocale,
                    },
                },
            });
        }

        case FETCH_CHART_WIDGETS:
            return update(state, {
                fetchChartWidgets: {
                    $merge: {
                        isFetching: true,
                        errors: [],
                    },
                },
            });

        case FETCH_CHART_WIDGETS_SUCCESS:
            return update(state, {
                fetchChartWidgets: {
                    $merge: {
                        isFetching: false,
                        data: action.payload,
                    },
                },
            });

        case FETCH_CHART_WIDGETS_FAILURE:
            return update(state, {
                fetchTableWidgets: {
                    $merge: {
                        isFetching: false,
                        errors: action.payload,
                    },
                },
            });

        case INITIALIZE:
            return update(state, {
                initializing: {
                    $set: true,
                },
            });
        case INITIALIZE_FAILURE:
        case INITIALIZE_SUCCESS:
            return update(state, {
                initializing: {
                    $set: false,
                },
            });

        case RESET:
            return update(state, {
                $set: INITIAL_STATE,
            });

        case SET_TEMPLATE_CONFIG: {
            const { baseUrl, froalaLicenseKey, googleApiKey, token, language } =
                action.payload;
            return update(state, {
                baseUrl: {
                    $set: baseUrl,
                },
                token: {
                    $set: token,
                },
                googleApiKey: {
                    $set: googleApiKey,
                },
                froalaLicenseKey: {
                    $set: froalaLicenseKey,
                },
                language: {
                    $set: language,
                },
            });
        }

        default:
            return state;
    }
}
