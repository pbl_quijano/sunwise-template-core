import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { Col, InputGroup } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import Button from '@components/ButtonComponent';
import { FlexRow } from '@components/styled';
import TitleWithDetail from '@components/TitleWithDetail';

import { GeneralContext } from '@helpers/contexts';

import * as templateViewSelectors from '@modules/TemplateView/selectors';

import * as templateCoreActions from '@templateCore/actions';
import { TABLE_THEMES } from '@templateCore/constants';
import * as templateCoreSelectors from '@templateCore/selectors';

import * as actions from './actions';
import { getTemplateLanguage, getTemplateTypeText } from './helpers';
import * as selectors from './selectors';

const Container = styled.div`
    display: block;
    padding: 25px 27px 20px 27px;
    ${({ disabled }) =>
        disabled &&
        `opacity: 0.6;
        cursor: not-allowed;
        pointer-events: none;`}
`;

const InstructionText = styled.span`
    display: block;
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-top: 14px;
    padding-left: 12px;
`;

const ThemeSelectContainer = styled.div`
    align-items: center;
    display: flex;
    justify-content: space-between;
    margin-top: 17px;
    padding-left: 12px;
`;

const TemplatePropiertyContainer = styled(FlexRow)`
    padding-left: 12px;
    margin-top: 32px;
`;

const LabelText = styled.span`
    color: #848bab;
    font-size: 12px;
    line-height: 15px;
    min-height: 15px;
`;

const TemplatePropiertyText = styled.p`
    font-weight: 500;
    font-size: 12px;
    line-height: 12px;
    letter-spacing: 0.25px;
    color: #848bab;
    margin-bottom: 0;
`;

const StyledInputGroup = styled(InputGroup)`
    height: 32px;
    width: ${({ width = '190px' }) => width};

    & select {
        background-color: #ffffff;
        border-radius: 3px;
        border: 1px solid #ecedf0;
        font-size: 12px;
        height: 32px;
        width: ${({ width = '190px' }) => width};
    }
`;

const PageProperties = ({
    fetchLanguages,
    isInfiniteModeValid,
    languagesDictionary,
    selectedPageId,
    selectedPageInfiniteMode,
    selectedPageIsBlocked,
    selectedPageOrientation,
    selectedPageTheme,
    setInfiniteMode,
    setOrientation,
    setTheme,
    templateLanguage,
    templateType,
    themeEnabled = true,
}) => {
    const { onChangeInPage } = useContext(GeneralContext);
    const { t } = useTranslation();

    useEffect(() => {
        fetchLanguages();
    }, []);

    const [infiniteModeControl, setInfiniteModeControl] = useState(
        selectedPageInfiniteMode
    );

    useEffect(() => {
        if (selectedPageInfiniteMode !== infiniteModeControl) {
            setInfiniteModeControl(selectedPageInfiniteMode);
        }
    }, [selectedPageInfiniteMode]);

    const onChangeThemeSelect = (e) => {
        setTheme(selectedPageId, e.target.value, onChangeInPage);
    };

    const onChangeOrientationSelect = (e) => {
        setOrientation(selectedPageId, e.target.value, onChangeInPage);
    };

    const handleChangeInfiniteMode = (e) => {
        setInfiniteMode(selectedPageId, e.target.checked, onChangeInPage);
    };

    if (!themeEnabled) {
        return null;
    }
    return (
        <Container disabled={selectedPageIsBlocked}>
            <TitleWithDetail className="mb-0">
                {t('Properties')}
            </TitleWithDetail>

            <TemplatePropiertyContainer alignItems="center">
                <Col xs="6 pl-0">
                    <TemplatePropiertyText>
                        {t('Language')}:
                    </TemplatePropiertyText>
                </Col>
                <Col xs="12 pl-0">
                    <TemplatePropiertyText>
                        {getTemplateLanguage(
                            templateLanguage,
                            languagesDictionary
                        )}
                    </TemplatePropiertyText>
                </Col>
            </TemplatePropiertyContainer>

            <TemplatePropiertyContainer alignItems="center">
                <Col xs="6 pl-0">
                    <TemplatePropiertyText>
                        {t('Template type')}:
                    </TemplatePropiertyText>
                </Col>
                <Col xs="12 pl-0">
                    <TemplatePropiertyText>
                        {getTemplateTypeText(templateType)}
                    </TemplatePropiertyText>
                </Col>
            </TemplatePropiertyContainer>

            <FlexRow justifyContent="center" className="mt-2">
                <Button
                    as="a"
                    className="mt-2 text-left"
                    fontSize="12px"
                    fontWeight="bold"
                    href="http://academy.sunwise.mx/es/articles/5547159-resumen-de-propuestas"
                    target="_blank"
                    textColor="#1F3C53"
                >
                    {t('See manual')}
                </Button>
            </FlexRow>

            <InstructionText>
                {t(
                    'This theme will be used in the new components of your template.'
                )}
            </InstructionText>

            <ThemeSelectContainer>
                <LabelText>{t('Theme')}:</LabelText>

                <StyledInputGroup>
                    <Form.Control
                        value={selectedPageTheme}
                        onChange={onChangeThemeSelect}
                        as="select"
                        id="select-theme"
                    >
                        {Object.keys(TABLE_THEMES).map((key) => {
                            return (
                                <option
                                    key={`select-${key}-${TABLE_THEMES[key].name}`}
                                    value={key}
                                >
                                    {TABLE_THEMES[key].name}
                                </option>
                            );
                        })}
                    </Form.Control>
                </StyledInputGroup>
            </ThemeSelectContainer>
            <ThemeSelectContainer>
                <LabelText>{t('Orientation')}:</LabelText>

                <StyledInputGroup width="160px">
                    <Form.Control
                        value={selectedPageOrientation}
                        onChange={onChangeOrientationSelect}
                        as="select"
                        id="select-orientation"
                    >
                        <option value="portrait">{t('Vertical')}</option>
                        <option value="landscape">{t('Horizontal')}</option>
                    </Form.Control>
                </StyledInputGroup>
            </ThemeSelectContainer>
            <ThemeSelectContainer className="text-top">
                <LabelText>{t('Dynamic content')}:</LabelText>
                <StyledInputGroup width="160px">
                    <Form.Check
                        type="checkbox"
                        id="infite-mode"
                        onChange={handleChangeInfiniteMode}
                        disabled={!isInfiniteModeValid}
                        label=""
                        checked={infiniteModeControl}
                        value={infiniteModeControl}
                    />
                </StyledInputGroup>
            </ThemeSelectContainer>
        </Container>
    );
};

const mapStateToProps = createStructuredSelector({
    isInfiniteModeValid: templateViewSelectors.isInfiniteModeValid,
    languagesDictionary: selectors.getLanguagesDictionary,
    selectedPageId: templateViewSelectors.getSelectedPageId,
    selectedPageInfiniteMode: templateViewSelectors.getSelectedPageInfiniteMode,
    selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked,
    selectedPageOrientation: templateViewSelectors.getSelectedPageOrientation,
    selectedPageTheme: templateViewSelectors.getSelectedPageTheme,
    templateLanguage: templateCoreSelectors.getCurrentTemplateLanguage,
    templateType: templateCoreSelectors.getCurrentTemplateType,
});

const mapDispatchToProps = (dispatch) => ({
    fetchLanguages: () => dispatch(actions.fetchLanguages()),
    setInfiniteMode: (pageId, infiniteMode, onChangeInPage) =>
        dispatch(
            templateCoreActions.setInfiniteMode(
                pageId,
                infiniteMode,
                onChangeInPage
            )
        ),
    setOrientation: (pageId, orientation, onChangeInPage) =>
        dispatch(
            templateCoreActions.setOrientation(
                pageId,
                orientation,
                onChangeInPage
            )
        ),
    setTheme: (pageId, theme, onChangeInPage) =>
        dispatch(templateCoreActions.setTheme(pageId, theme, onChangeInPage)),
});

PageProperties.propTypes = {
    chartWidgets: PropTypes.array,
    fetchLanguages: PropTypes.func,
    isInfiniteModeValid: PropTypes.bool,
    languagesDictionary: PropTypes.object,
    selectedPageId: PropTypes.string,
    selectedPageInfiniteMode: PropTypes.bool,
    selectedPageIsBlocked: PropTypes.bool,
    selectedPageOrientation: PropTypes.string,
    selectedPageTheme: PropTypes.string,
    setInfiniteMode: PropTypes.func,
    setOrientation: PropTypes.func,
    setTheme: PropTypes.func,
    templateLanguage: PropTypes.string,
    templateType: PropTypes.number,
    themeEnabled: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(PageProperties);
