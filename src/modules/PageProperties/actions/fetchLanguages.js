import { getLanguages } from '@api';

import {
    FETCH_LANGUAGES,
    FETCH_LANGUAGES_FAILURE,
    FETCH_LANGUAGES_SUCCESS,
} from '../actionTypes';

export default () => (dispatch, getState) => {
    dispatch({ type: FETCH_LANGUAGES });

    getLanguages(getState())()
        .then((response) => {
            dispatch({
                type: FETCH_LANGUAGES_SUCCESS,
                payload: response.data.data,
            });
        })
        .catch((error) =>
            dispatch({
                type: FETCH_LANGUAGES_FAILURE,
                payload: error.data.errors,
            })
        );
};
