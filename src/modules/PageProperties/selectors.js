import { createSelector } from 'reselect';

import { arraytoDictionary } from '@helpers/utils';

import * as selectors from '@main/selectors';

export const getModel = createSelector(
    selectors.getLibState,
    (sunwiseTemplateCore) => sunwiseTemplateCore.pageProperties
);

export const getLanguageModel = createSelector(
    getModel,
    (model) => model.languages
);

export const getLanguagesData = createSelector(
    getLanguageModel,
    (model) => model.data || []
);

export const getLanguagesDictionary = createSelector(
    getLanguagesData,
    (languagesData) => arraytoDictionary(languagesData, 'key')
);
