import update from 'immutability-helper';

import {
    FETCH_LANGUAGES,
    FETCH_LANGUAGES_FAILURE,
    FETCH_LANGUAGES_SUCCESS,
} from './actionTypes';

const INITIAL_STATE = {
    languages: {
        data: [],
        errors: [],
        isFetching: false,
    },
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_LANGUAGES:
            return update(state, {
                languages: {
                    $merge: {
                        errors: [],
                        isFetching: true,
                    },
                },
            });

        case FETCH_LANGUAGES_FAILURE:
            return update(state, {
                languages: {
                    $merge: {
                        errors: action.payload,
                        isFetching: false,
                    },
                },
            });

        case FETCH_LANGUAGES_SUCCESS:
            return update(state, {
                languages: {
                    $merge: {
                        data: action.payload,
                        isFetching: false,
                    },
                },
            });

        default:
            return state;
    }
}
