import i18next from 'i18next';

import {
    ONE_PROPOSAL_TYPE,
    MULTIPROPOSAL_TYPE,
    SMART_DOCUMENTS_TYPE,
} from '@constants/types';

export const getTemplateTypeText = (type) => {
    switch (type) {
        case ONE_PROPOSAL_TYPE:
            return i18next.t('Proposal', { count: 2 });
        case MULTIPROPOSAL_TYPE:
            return i18next.t('Summary');
        case SMART_DOCUMENTS_TYPE:
            return i18next.t('Smart documents');
        default:
            return '';
    }
};

export const getTemplateLanguage = (templateLanguage, languagesDictionary) => {
    const selectedLanguage = languagesDictionary[templateLanguage];
    if (selectedLanguage && selectedLanguage.language) {
        return selectedLanguage.language;
    }
    return i18next.t('Not available');
};
