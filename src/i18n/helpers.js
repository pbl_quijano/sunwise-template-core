import i18n from 'i18next';
import isEmpty from 'lodash/isEmpty';
import { initReactI18next } from 'react-i18next';

import { DEFAULT_LANGUAGE } from './constants';
import TRANSLATIONS_EN from './translations/en.json';
import TRANSLATIONS_ES from './translations/es.json';
import TRANSLATIONS_PT from './translations/pt.json';

export const initInternationalization = (language = DEFAULT_LANGUAGE) => {
    if (isEmpty(i18n.languages)) {
        i18n.use(initReactI18next).init({
            lng: language,
            resources: {
                en: {
                    translation: TRANSLATIONS_EN,
                },
                es: {
                    translation: TRANSLATIONS_ES,
                },
                pt: {
                    translation: TRANSLATIONS_PT,
                },
            },
            fallbackLng: DEFAULT_LANGUAGE,
            debug: true,
            keySeparator: false,
            interpolation: {
                escapeValue: false,
            },
            react: {
                wait: true,
                transSupportBasicHtmlNodes: true,
            },
        });
    } else {
        i18n.addResourceBundle('en', 'translations', TRANSLATIONS_EN);
        i18n.addResourceBundle('es', 'translations', TRANSLATIONS_ES);
        i18n.addResourceBundle('pt', 'translations', TRANSLATIONS_PT);
    }
};
