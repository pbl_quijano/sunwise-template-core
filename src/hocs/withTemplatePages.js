import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as templateCoreSelectors from '@templateCore/selectors';

const WithTemplatePages = ({ Children, injectedProps, templatePages }) => {
    return (
        <Children
            templatePages={templatePages}
            {...{ ...injectedProps }}
        ></Children>
    );
};

const mapStateToProps = createStructuredSelector({
    templatePages: templateCoreSelectors.getCurrentTemplatePages,
});

WithTemplatePages.propTypes = {
    Children: PropTypes.any,
    injectedProps: PropTypes.object,
    templatePages: PropTypes.array,
};

const WithTemplatePagesComposed = connect(
    mapStateToProps,
    null
)(WithTemplatePages);

const WithTemplatePagesBuild = ({ Children, injectedProps }) => {
    return (
        <WithTemplatePagesComposed
            {...{
                Children,
                injectedProps,
            }}
        />
    );
};

WithTemplatePagesBuild.propTypes = {
    Children: PropTypes.any,
    injectedProps: PropTypes.object,
};

const WithTemplatePagesHOC = (Children) => (props) =>
    WithTemplatePagesBuild({
        Children,
        injectedProps: props,
    });

export default WithTemplatePagesHOC;
