import arrayMove from 'array-move';
import i18next from 'i18next';
import cloneDeep from 'lodash/cloneDeep';

import showToast from '@helpers/showToast';

import { NEW_PAGE_CONTENT } from '@templateCore/constants';
import { getUpdatingContentData, orderPages } from '@templateCore/helpers';

export const addPageInTemplateBuild = (
    currentTemplateData,
    updateTemplatePromise,
    addNewPages,
    selectedPage
) => {
    if (!updateTemplatePromise || !currentTemplateData) {
        return () => {};
    }
    let tempCurrentTemplateData = cloneDeep(currentTemplateData);
    return () => {
        tempCurrentTemplateData.pages = [
            ...tempCurrentTemplateData.pages,
            {
                company: tempCurrentTemplateData.company.id,
                content: NEW_PAGE_CONTENT,
                custom_template: tempCurrentTemplateData.id,
                page_parent: null,
                id: '',
                page: `${tempCurrentTemplateData.pages.length + 1}`,
            },
        ];
        if (selectedPage) {
            tempCurrentTemplateData.pages = orderPages(
                arrayMove(
                    tempCurrentTemplateData.pages,
                    tempCurrentTemplateData.pages.length - 1,
                    parseInt(selectedPage.page)
                )
            );
        }
        updateTemplatePromise(
            tempCurrentTemplateData.id,
            getUpdatingContentData(tempCurrentTemplateData)
        )
            .then((templateData) => {
                addNewPages(templateData.pages_template);
                showToast({
                    body: i18next.t('New page added successfully'),
                });
            })
            .catch(() => {
                showToast({
                    type: 'danger',
                    body: i18next.t('An error occurred while adding new page'),
                });
            });
    };
};

export const deletePagesInTemplateBuild = (
    currentTemplateData,
    updateTemplatePromise,
    deletePages
) => {
    if (!updateTemplatePromise || !currentTemplateData) {
        return () => {};
    }
    let tempCurrentTemplateData = cloneDeep(currentTemplateData);
    return (deletingId) => {
        tempCurrentTemplateData.pages = orderPages(
            tempCurrentTemplateData.pages.filter((p) => p.id !== deletingId)
        );
        updateTemplatePromise(
            tempCurrentTemplateData.id,
            getUpdatingContentData(tempCurrentTemplateData)
        )
            .then((templateData) => {
                deletePages(deletingId, templateData.pages_template);
                showToast({
                    body: i18next.t('Page was successfully removed'),
                });
            })
            .catch((error) => {
                console.error(error);
                showToast({
                    type: 'danger',
                    body: i18next.t(
                        'An error occurred while deleting the page'
                    ),
                });
            });
    };
};

export const duplicatePagesInTemplateBuild = (
    currentTemplateData,
    updateTemplatePromise,
    addNewPages
) => {
    if (!updateTemplatePromise || !currentTemplateData) {
        return () => {};
    }
    let tempCurrentTemplateData = cloneDeep(currentTemplateData);
    return (duplicatedPage) => {
        tempCurrentTemplateData.pages = [
            ...tempCurrentTemplateData.pages,
            {
                blocked: duplicatedPage.blocked || 0,
                company: tempCurrentTemplateData.company.id,
                content: duplicatedPage.content,
                custom_template: tempCurrentTemplateData.id,
                page_parent: null,
                id: '',
                page: `${tempCurrentTemplateData.pages.length + 1}`,
            },
        ];
        tempCurrentTemplateData.pages = orderPages(
            arrayMove(
                tempCurrentTemplateData.pages,
                tempCurrentTemplateData.pages.length - 1,
                parseInt(duplicatedPage.page)
            )
        );
        updateTemplatePromise(
            tempCurrentTemplateData.id,
            getUpdatingContentData(tempCurrentTemplateData)
        )
            .then((templateData) => {
                addNewPages(templateData.pages_template);
                showToast({
                    body: i18next.t('The page was duplicated successfully'),
                });
            })
            .catch(() => {
                showToast({
                    type: 'danger',
                    body: i18next.t(
                        'An error occurred while duplicating the page'
                    ),
                });
            });
    };
};

export const orderPagesInTemplateBuild = (
    currentTemplateData,
    updateTemplatePromise,
    setOrderPages
) => {
    if (!updateTemplatePromise || !currentTemplateData) {
        return () => {};
    }
    let tempCurrentTemplateData = cloneDeep(currentTemplateData);
    return (oldIndex, newIndex) => {
        tempCurrentTemplateData.pages = orderPages(
            arrayMove(tempCurrentTemplateData.pages, oldIndex, newIndex)
        );
        setOrderPages(oldIndex, newIndex);
        updateTemplatePromise(
            tempCurrentTemplateData.id,
            getUpdatingContentData(tempCurrentTemplateData)
        )
            .then(() => {
                showToast({
                    body: i18next.t('Order was successfully saved'),
                });
            })
            .catch(() => {
                showToast({
                    type: 'danger',
                    body: i18next.t('An error occurred while ordering'),
                });
                setOrderPages(newIndex, oldIndex);
            });
    };
};

// export const updatingTemplateActionsBuilder =
//     (customTemplateData, selectedPage) => (updateTemplatePromise) => ({
//         onAddPage: () => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { selectedPage },
//             //     ADD_PAGE_CHANGE
//             // );
//         },
//         onDeletePage: (updateTemplatePromise, deletingId) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { deletingId },
//             //     DELETE_PAGE_CHANGE
//             // );
//         },
//         onDuplicatePage: (updateTemplatePromise, duplicatedPageId) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { duplicatedPageId },
//             //     DUPLICATE_PAGE_CHANGE
//             // );
//         },
//         onOrderPages: (updateTemplatePromise, oldIndex, newIndex) => {
//             // updateTemplateAction(
//             //     customTemplateId,
//             //     { oldIndex, newIndex },
//             //     ORDER_PAGES_CHANGE
//             // );
//         },
//     });

// export const getTemplateDataChanged = (
//     customTemplateData,
//     changeType,
//     changeData
// ) => {
//     let tempCustomTemplate = { ...customTemplateData };

//     switch (changeType) {
//         case ADD_PAGE_CHANGE: {
//             const { selectedPage } = changeData;
//             tempCustomTemplate.pages = [
//                 ...tempCustomTemplate.pages,
//                 {
//                     company: customTemplateData.company.id,
//                     content: NEW_PAGE_CONTENT,
//                     custom_template: customTemplateData.id,
//                     page_parent: null,
//                     id: '',
//                     page: `${tempCustomTemplate.pages.length + 1}`,
//                 },
//             ];
//             if (selectedPage) {
//                 tempCustomTemplate.pages = orderPages(
//                     arrayMove(
//                         tempCustomTemplate.pages,
//                         tempCustomTemplate.pages.length - 1,
//                         parseInt(selectedPage.page)
//                     )
//                 );
//             }
//             break;
//         }

//         case DELETE_PAGE_CHANGE: {
//             const { deletingId } = changeData;
//             tempCustomTemplate.pages = orderPages(
//                 tempCustomTemplate.pages.filter((p) => p.id !== deletingId)
//             );
//             break;
//         }

//         case DUPLICATE_PAGE_CHANGE: {
//             const { duplicatedPage } = changeData;

//             tempCustomTemplate.pages = [
//                 ...tempCustomTemplate.pages,
//                 {
//                     blocked: duplicatedPage.blocked || 0,
//                     company: customTemplateData.company.id,
//                     content: duplicatedPage.content,
//                     custom_template: customTemplateData.id,
//                     page_parent: null,
//                     id: '',
//                     page: `${tempCustomTemplate.pages.length + 1}`,
//                 },
//             ];
//             tempCustomTemplate.pages = orderPages(
//                 arrayMove(
//                     tempCustomTemplate.pages,
//                     tempCustomTemplate.pages.length - 1,
//                     parseInt(duplicatedPage.page)
//                 )
//             );
//             break;
//         }

//         case ORDER_PAGES_CHANGE: {
//             const { oldIndex, newIndex } = changeData;
//             tempCustomTemplate.pages = orderPages(
//                 arrayMove(tempCustomTemplate.pages, oldIndex, newIndex)
//             );
//             break;
//         }

//         default:
//     }
//     return getUpdatingContentData(tempCustomTemplate);
// };
