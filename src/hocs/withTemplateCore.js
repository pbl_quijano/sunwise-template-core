import { GoogleApiWrapper } from 'google-maps-react';
import PropTypes from 'prop-types';
import { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { createStructuredSelector } from 'reselect';

import { GeneralProvider } from '@helpers/contexts';

import * as actions from '@main/actions';
import * as selectors from '@main/selectors';

import * as templateViewActions from '@modules/TemplateView/actions';
import * as templateViewSelectors from '@modules/TemplateView/selectors';

import * as templateCoreActions from '@templateCore/actions';
import * as templateCoreSelectors from '@templateCore/selectors';

import { initInternationalization } from '../i18n/helpers';

import {
    addPageInTemplateBuild,
    deletePagesInTemplateBuild,
    duplicatePagesInTemplateBuild,
    orderPagesInTemplateBuild,
} from './helpers';

const WithTemplateCore = ({
    Children,
    addNewPages,
    baseUrl,
    currencyConfig,
    currentTemplateFullData,
    deletePages,
    getTemplateUpdatedData,
    google,
    googleApiKey,
    initTemplate,
    injectedProps,
    isEmptyPages,
    onUpdatePage,
    reset,
    resetTemplate,
    restorePages,
    selectPage,
    selectedPage,
    setOrderPages,
    setTemplateConfig,
    setTemplateBackup,
    templateConfig,
}) => {
    const { language, updateTemplatePromise, onChangeInPage } = templateConfig;
    useEffect(() => {
        if (language) {
            initInternationalization(language);
        }
        setTemplateConfig(templateConfig);
        return () => {
            reset();
        };
    }, []);

    const addBlankPageInTemplate = useCallback(
        addPageInTemplateBuild(
            currentTemplateFullData,
            updateTemplatePromise,
            addNewPages,
            selectedPage
        ),
        [
            currentTemplateFullData,
            updateTemplatePromise,
            addNewPages,
            selectedPage,
        ]
    );

    const deletePagesInTemplate = useCallback(
        deletePagesInTemplateBuild(
            currentTemplateFullData,
            updateTemplatePromise,
            deletePages
        ),
        [currentTemplateFullData, updateTemplatePromise, deletePages]
    );

    const duplicatePagesInTemplate = useCallback(
        duplicatePagesInTemplateBuild(
            currentTemplateFullData,
            updateTemplatePromise,
            addNewPages
        ),
        [currentTemplateFullData, updateTemplatePromise, addNewPages]
    );

    const orderPagesInTemplate = useCallback(
        orderPagesInTemplateBuild(
            currentTemplateFullData,
            updateTemplatePromise,
            setOrderPages
        ),
        [currentTemplateFullData, updateTemplatePromise, addNewPages]
    );

    if (baseUrl === null || googleApiKey === null) {
        return null;
    }
    const {
        tagsLocale,
        chartIso = 'USD',
        chartLocale = 'en-US',
        tableIso = 'USD',
        tableLocale = 'en-US',
    } = currencyConfig;

    return (
        <GeneralProvider
            value={{
                google,
                onChangeInPage: (addStateDisabled) =>
                    onUpdatePage(onChangeInPage, addStateDisabled),
                tagsLocale,
                currencyConfig: {
                    chartIso,
                    chartLocale,
                    tableIso,
                    tableLocale,
                },
            }}
        >
            <Children
                addNewPages={addNewPages}
                addBlankPageInTemplate={addBlankPageInTemplate}
                deletePagesInTemplate={deletePagesInTemplate}
                duplicatePagesInTemplate={duplicatePagesInTemplate}
                getTemplateUpdatedData={getTemplateUpdatedData}
                initTemplate={initTemplate}
                isEmptyPages={isEmptyPages}
                orderPagesInTemplate={orderPagesInTemplate}
                resetTemplate={resetTemplate}
                restorePages={restorePages}
                selectPage={selectPage}
                selectedPageId={selectedPage && selectedPage.id}
                setTemplateBackup={setTemplateBackup}
                templateCreatedAt={
                    currentTemplateFullData &&
                    currentTemplateFullData.created_at
                }
                templateProposalNumber={
                    currentTemplateFullData &&
                    currentTemplateFullData.proposals_number
                }
                templateTitle={
                    currentTemplateFullData && currentTemplateFullData.title
                }
                templateType={
                    currentTemplateFullData && currentTemplateFullData.type
                }
                templateVersion={
                    currentTemplateFullData && currentTemplateFullData.version
                }
                {...{ ...injectedProps }}
            ></Children>
        </GeneralProvider>
    );
};

const mapStateToProps = createStructuredSelector({
    baseUrl: selectors.getBaseUrl,
    currencyConfig: selectors.getCurrencyConfig,
    currentTemplateFullData: templateCoreSelectors.getCurrentTemplateFullData,
    googleApiKey: selectors.getGoogleApiKey,
    isEmptyPages: templateCoreSelectors.getIsEmptyPages,
    language: selectors.getLanguage,
    selectedPage: templateViewSelectors.getSelectedPage,
    token: selectors.getToken,
});

const mapDispatchToProps = (dispatch) => ({
    addNewPages: (pagesTemplate) =>
        dispatch(templateCoreActions.addNewPages(pagesTemplate)),
    deletePages: (deletingId, pagesTemplate) =>
        dispatch(templateCoreActions.deletePages(deletingId, pagesTemplate)),
    getTemplateUpdatedData: () => dispatch(actions.getTemplateUpdatedData()),
    initTemplate: (templateData, offerId) =>
        dispatch(actions.initialize(templateData, offerId)),
    onUpdatePage: (onChangeInPage, addStateDisabled) =>
        dispatch(
            templateViewActions.onUpdatePage(onChangeInPage, addStateDisabled)
        ),
    reset: (templateConfig) => dispatch(actions.reset(templateConfig)),
    resetTemplate: () => dispatch(templateCoreActions.resetTemplate()),
    restorePages: () => dispatch(templateCoreActions.restorePages()),
    selectPage: (pageId) => dispatch(templateViewActions.selectPage(pageId)),
    setOrderPages: (oldIndex, newIndex) =>
        dispatch(templateCoreActions.orderPages(oldIndex, newIndex)),
    setTemplateBackup: (clean) =>
        dispatch(templateCoreActions.setTemplateBackup(clean)),
    setTemplateConfig: (templateConfig) =>
        dispatch(actions.setTemplateConfig(templateConfig)),
});

WithTemplateCore.propTypes = {
    Children: PropTypes.any,
    addNewPages: PropTypes.func,
    baseUrl: PropTypes.string,
    currencyConfig: PropTypes.object,
    currentTemplateFullData: PropTypes.object,
    deletePages: PropTypes.func,
    getTemplateUpdatedData: PropTypes.func,
    google: PropTypes.object,
    googleApiKey: PropTypes.string,
    initTemplate: PropTypes.func,
    injectedProps: PropTypes.object,
    isEmptyPages: PropTypes.bool,
    language: PropTypes.string,
    onUpdatePage: PropTypes.func,
    reset: PropTypes.func,
    resetTemplate: PropTypes.func,
    restorePages: PropTypes.func,
    selectPage: PropTypes.func,
    selectedPage: PropTypes.object,
    setOrderPages: PropTypes.func,
    setTemplateConfig: PropTypes.func,
    setTemplateBackup: PropTypes.func,
    token: PropTypes.string,
    templateConfig: PropTypes.object,
};

const WithTemplateCoreComposed = compose(
    GoogleApiWrapper((props) => ({
        apiKey: props.googleApiKey,
        libraries: ['geometry'],
        language: props.language,
        LoadingContainer: () => null,
    })),
    connect(mapStateToProps, mapDispatchToProps)
)(WithTemplateCore);

const WithTemplateCoreHOC = (getTemplateConfig) => (Children) => {
    const WrappedComponent = (props) => {
        return (
            <WithTemplateCoreComposed
                {...{
                    Children,
                    injectedProps: props,
                    templateConfig: getTemplateConfig(props),
                }}
            />
        );
    };
    return WrappedComponent;
};

export default WithTemplateCoreHOC;
