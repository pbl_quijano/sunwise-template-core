import { ORM } from 'redux-orm';

import Group from '@models/Group';
import Page from '@models/Page';
import Template from '@models/Template';
import Widget from '@models/Widget';

const orm = new ORM({
    stateSelector: (state) => state.entities,
});

orm.register(Group, Page, Template, Widget);
export { orm as default };
