export const buildGraphsColors = ({ type, stacked_bar }) => {
    const colorList = [
        '#02E675',
        '#FF9A00',
        '#2F4DFF',
        '#002438',
        '#785F9D',
        '#FA6968',
        '#0073d0',
        '#600980',
        '#803909',
    ];

    const tempColorList = [...colorList];
    let keyList = [];

    if (
        stacked_bar &&
        stacked_bar.data &&
        stacked_bar.data.length > 0 &&
        stacked_bar.visible &&
        type === 'bar'
    ) {
        keyList = Object.keys(
            JSON.parse(stacked_bar.data[0][stacked_bar.selected])
        );
    }

    const results = keyList.map((o, index) => {
        const colorListIndex =
            index -
            parseInt(index / tempColorList.length) * tempColorList.length;

        return tempColorList[colorListIndex];
    });

    return results;
};
