import i18next from 'i18next';
import { toast } from 'react-toastify';

import ToastNotification from '@components/ToastNotification';

export default (props) => {
    const defaults = {
        autoClose: 1500,
        closeButton: true,
        closeOnClick: true,
        draggable: true,
        body: 'Successfully saved',
        hideProgressBar: true,
        newestOnTop: true,
        title: '',
        type: 'success',
    };

    const values = { ...defaults, ...props };

    const {
        autoClose,
        body,
        closeButton,
        closeOnClick,
        draggable,
        hideProgressBar,
        newestOnTop,
        title,
        type,
    } = values;

    const translatedBody = i18next.exists(body) ? i18next.t(body) : body;

    return toast(
        <ToastNotification body={translatedBody} title={title} type={type} />,
        {
            autoClose: autoClose,
            closeButton,
            closeOnClick,
            draggable,
            hideProgressBar: hideProgressBar,
            newestOnTop,
        }
    );
};
