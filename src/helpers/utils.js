import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isNull from 'lodash/isNull';
import numeral from 'numeral';

export const arraytoDictionary = (array, key) => {
    if (!Array.isArray(array)) return {};

    return array.reduce(
        (acc, current) => ({ ...acc, [current[key]]: current }),
        {}
    );
};

const clearExponential = (value) => {
    if (value) {
        const parts = value.toString().split('e');
        if (parts[0]) {
            return parts[0];
        }
    }
    return value;
};

export const cloneElement = (value) => {
    let ret = value instanceof Array ? [] : {};

    for (let key in value) {
        if (!Object.prototype.hasOwnProperty.call(value, key)) {
            continue;
        }

        let val = value[key];

        if (val && typeof val == 'object') {
            val = cloneElement(val);
        }

        ret[key] = val;
    }

    return ret;
};

export const formatNumber = ({
    input,
    unit = '',
    format = '0,0',
    suffix = '',
}) =>
    !input
        ? suffix + '0'
        : `${suffix}${numeral(input).format(format, (n) => n)}${unit}`;

export const getCurrencyIso = (currency) => {
    if (
        !isEmpty(currency) &&
        !isNull(currency) &&
        hasValue(currency, 'abbreviation')
    ) {
        return get(currency, 'abbreviation', 'USD');
    }
    return 'USD';
};

export const getCurrencyLocale = (currency) => {
    if (
        !isEmpty(currency) &&
        !isNull(currency) &&
        hasValue(currency, 'locale')
    ) {
        return get(currency, 'locale', 'en-US').replace(/_/g, '-');
    }
    return 'en-US';
};

export const getFileNameByPath = (url) => {
    return url.substring(url.lastIndexOf('/') + 1);
};

export const handleFileURL = (url, preffix) => {
    const pattern = /^((https):\/\/)/;
    return pattern.test(url) ? url : `${preffix}${url}`;
};

export const hasValue = (object, name) =>
    Object.prototype.hasOwnProperty.call(object, name) &&
    !isEmpty(object[name]);

export const isJsonString = (str) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

export const numberFormat = (value, options) => {
    const newValue = clearExponential(value);
    const {
        currency,
        decimals = 2,
        locale = 'en-US',
        unit = '',
        style,
    } = options;
    let formatted = newValue;
    let unitText = '';

    switch (style) {
        case 'currency':
            formatted = new Intl.NumberFormat(locale, {
                style,
                currency,
                minimumFractionDigits: decimals,
            }).format(newValue);
            unitText = currency;
            break;
        case 'decimal':
            formatted = new Intl.NumberFormat(locale, {
                style,
                minimumFractionDigits: decimals,
            }).format(newValue);
            unitText = unit;
            break;
        default:
            formatted = newValue;
            break;
    }

    return `${formatted} ${!isEmpty(unitText) ? unitText : ''}`;
};

export const toFixed = (number, fixed) => {
    const fixedValue = Math.pow(10, fixed);
    return parseInt(number * fixedValue) / fixedValue;
};
