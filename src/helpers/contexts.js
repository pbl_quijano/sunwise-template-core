import React from 'react';

export const _GeneralContext = React.createContext({
    google: null,
    onChangeInPage: () => {},
    currencyConfig: {},
});

export const GeneralProvider = _GeneralContext.Provider;
export const GeneralConsumer = _GeneralContext.Consumer;
export const GeneralContext = _GeneralContext;
