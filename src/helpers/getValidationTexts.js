import i18next from 'i18next';

export default () => ({
    CALC_REQUIRED_TEXT: i18next.t('The calculator field is required'),
    FORMULA_REQUIRED_TEXT: i18next.t('Add at least one element to the formula'),
    NUMBER_TYPE_TEXT: i18next.t('Must be a number'),
    REQUIRED_TEXT: i18next.t('This field is required'),
    getMinValueText: (value) =>
        i18next.t('It cannot be less than {{value}}', { value }),
});
