import isNil from 'lodash/isNil';

import {
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SOLAR_MODULE_COLOR,
    SOLAR_MODULE_STROKE_COLOR,
} from '@constants/maps';

import { formatNumber } from '@helpers/utils';

import MeterLabelPopupBuilder from '@models/MeterLabelPopupBuilder';
import SegmentPolygonBuilder from '@models/SegmentPolygonBuilder';

export const mapValueUseEffect = (
    google,
    mapValue,
    projectMarker,
    segmentsDictionary,
    handleCenterChanged,
    handleZoomChanged,
    scaleControl,
    segmentFillColor = SEGMENT_COLOR,
    segmentStrokeColor = SEGMENT_STROKE_COLOR,
    showLabels,
    showProjectMarker,
    solarModuleFillColor = SOLAR_MODULE_COLOR,
    solarModuleStrokeColor = SOLAR_MODULE_STROKE_COLOR,
    setShapes,
    zoomControl
) => {
    const buildShapes = (myMapValue) => {
        let shapes = {};
        for (const segmentId in segmentsDictionary) {
            const segmentValue = segmentsDictionary[segmentId];
            const shapeCoords = segmentValue.polygon.map(
                (point) => new google.maps.LatLng(point[0], point[1])
            );
            const SegmentPolygon = SegmentPolygonBuilder(google);

            const newShape = new SegmentPolygon({
                id: segmentId,
                options: {
                    paths: shapeCoords,
                    editable: false,
                    draggable: false,
                    fillColor: segmentFillColor,
                    strokeColor: segmentStrokeColor,
                    map: myMapValue,
                },
                showMeterLabels: showLabels,
                solarModules: segmentValue.solar_modules,
                solarModulesStyle: {
                    fillColor: solarModuleFillColor,
                    strokeColor: solarModuleStrokeColor,
                },
            });
            shapes[segmentId] = newShape;
        }
        setShapes(shapes);
    };
    return () => {
        if (mapValue) {
            mapValue.setOptions({ scaleControl, zoomControl });
            if (showProjectMarker) {
                projectMarker.setMap(mapValue);
            } else {
                projectMarker.setMap(null);
            }
            mapValue.setTilt(0);

            if (!isNil(mapValue.getProjection())) {
                buildShapes(mapValue);
            } else {
                google.maps.event.addListenerOnce(
                    mapValue,
                    'projection_changed',
                    () => buildShapes(mapValue)
                );
            }

            if (handleCenterChanged) {
                google.maps.event.addListener(mapValue, 'dragend', () =>
                    handleCenterChanged(mapValue.getCenter())
                );
            }

            if (handleZoomChanged) {
                google.maps.event.addListener(mapValue, 'zoom_changed', () =>
                    handleZoomChanged(mapValue.getZoom())
                );
            }
        }
        return () => {
            return () => {
                if (mapValue) {
                    google.maps.event.clearListeners(mapValue, 'dragend');
                    google.maps.event.clearListeners(mapValue, 'zoom_changed');
                }
            };
        };
    };
};

export const addPathListeners = ({
    google,
    id,
    map,
    meterLabels,
    paths,
    setSolarModules,
    setSolarModulesDisabled,
    vertexChange,
}) => {
    paths.forEach((path) => {
        google.maps.event.addListener(path, 'remove_at', () => {
            setSolarModulesDisabled();
            vertexChange(path.getArray(), map.zoom, id, (solarModules) => {
                setSolarModules(solarModules);
            });
            changeListener(path, meterLabels, google, map);
        });
        google.maps.event.addListener(path, 'insert_at', () => {
            setSolarModulesDisabled();
            vertexChange(path.getArray(), map.zoom, id, (solarModules) => {
                setSolarModules(solarModules);
            });
            changeListener(path, meterLabels, google, map);
        });

        google.maps.event.addListener(path, 'set_at', () => {
            setSolarModulesDisabled();
            vertexChange(path.getArray(), map.zoom, id, (solarModules) => {
                setSolarModules(solarModules);
            });
            changeListener(path, meterLabels, google, map);
        });
    });
};

export const changeListener = (path, meterLabels, google, map) => {
    if (path.getArray().length > meterLabels.length) {
        const MeterLabelPopup = MeterLabelPopupBuilder(google, map);
        meterLabels.push(new MeterLabelPopup());
    } else if (path.getArray().length < meterLabels.length) {
        meterLabels[meterLabels.length - 1].setMap(null);
        meterLabels.pop();
    }
    const isClockwise = getIsClockwise(
        path.getArray().map((point) => [point.lat(), point.lng()])
    );

    path.getArray().forEach((latLng, index, array) => {
        let toPoint;
        const fromPoint = latLng;
        if (index + 1 >= array.length) {
            toPoint = array[0];
        } else {
            toPoint = array[index + 1];
        }
        meterLabels[index].setPosition(
            getMiddleCoords(
                fromPoint.lat(),
                fromPoint.lng(),
                toPoint.lat(),
                toPoint.lng(),
                google
            )
        );
        meterLabels[index].setRotation(
            getLabelOrientation(
                fromPoint.lat(),
                fromPoint.lng(),
                toPoint.lat(),
                toPoint.lng(),
                isClockwise
            )
        );
        meterLabels[index].setText(
            `${formatNumber({
                input: getDistance(
                    fromPoint.lat(),
                    fromPoint.lng(),
                    toPoint.lat(),
                    toPoint.lng()
                ),
                format: '0,0.00',
            })} m`
        );
    });
};

export const getDistance = (lat1, lon1, lat2, lon2) => {
    const rad = (x) => {
        return (x * Math.PI) / 180;
    };
    const R = 6378137;
    const dLat = rad(lat2 - lat1);
    const dLong = rad(lon2 - lon1);
    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(lat1)) *
            Math.cos(rad(lat2)) *
            Math.sin(dLong / 2) *
            Math.sin(dLong / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;
    return d;
};

export const getIsClockwise = (polygon = []) => {
    const parsedPolygon = polygon.map((point) => toWorldCoords(point));
    const val =
        (parsedPolygon[1][1] - parsedPolygon[0][1]) *
            (parsedPolygon[2][0] - parsedPolygon[1][0]) -
        (parsedPolygon[1][0] - parsedPolygon[0][0]) *
            (parsedPolygon[2][1] - parsedPolygon[1][1]);
    if (val > 0) return false;
    return true;
};

export const getLabelOrientation = (lat1, lon1, lat2, lon2, isClockwise) => {
    const point1 = toWorldCoords([lat1, lon1]);
    const point2 = toWorldCoords([lat2, lon2]);
    const TWOPI = 6.2831853071795865;
    const RAD2DEG = 57.2957795130823209;
    let theta = Math.atan2(point2[0] - point1[0], point1[1] - point2[1]);
    if (theta < 0.0) {
        theta += TWOPI;
    }
    const inDeegres = RAD2DEG * theta;
    let accuracy = 0;
    if (isClockwise) {
        return inDeegres - (90 + accuracy);
    }
    return inDeegres + 90;
};

export const getMiddleCoords = (lat1, lon1, lat2, lon2, google) => {
    return new google.maps.LatLng((lat1 + lat2) / 2, (lon1 + lon2) / 2);
};

const toWorldCoords = (latLng) => {
    var siny = Math.sin((latLng[0] * Math.PI) / 180);
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);

    return [
        256 * (0.5 + latLng[1] / 360),
        256 * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)),
    ];
};

export const createMeterLabelPopup = (paths, google, map) => {
    const MeterLabelPopup = MeterLabelPopupBuilder(google, map);
    const isClockwise = getIsClockwise(
        paths.map((point) => [point.lat(), point.lng()])
    );

    return paths.map((latLng, index, currentArray) => {
        let toPoint;
        const fromPoint = latLng;
        if (index + 1 >= currentArray.length) {
            toPoint = currentArray[0];
        } else {
            toPoint = currentArray[index + 1];
        }
        return new MeterLabelPopup({
            text: `${formatNumber({
                input: getDistance(
                    fromPoint.lat(),
                    fromPoint.lng(),
                    toPoint.lat(),
                    toPoint.lng()
                ),
                format: '0,0.0',
            })} m`,
            position: getMiddleCoords(
                fromPoint.lat(),
                fromPoint.lng(),
                toPoint.lat(),
                toPoint.lng(),
                google
            ),
            rotate: getLabelOrientation(
                fromPoint.lat(),
                fromPoint.lng(),
                toPoint.lat(),
                toPoint.lng(),
                isClockwise
            ),
        });
    });
};
