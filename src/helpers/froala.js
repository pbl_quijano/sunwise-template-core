import i18next from 'i18next';

import {
    FROALA_DEFAULT_CONFIG,
    FROALA_HTML_DENIED_ATTRS,
    FROALA_LANGUAGE_CODE_EQUIVALENCE,
    FROALA_TOOLBAR_BUTTONS,
    FROALA_TOOLBAR_BUTTONS_MD,
    FROALA_TOOLBAR_BUTTONS_SM,
    FROALA_TOOLBAR_BUTTONS_XS,
    FONT_LIST,
} from '@constants/froala';

import { isJsonString } from '@helpers/utils';

export const getFontFamily = () =>
    FONT_LIST.reduce((acc, current) => {
        acc[`'${current}'`] = current;
        return acc;
    }, {});

export const getFroalaEditorConfig = ({
    FroalaEditor,
    froalaLicenseKey,
    isFullEdition,
    language,
    prepareEditCustomTag,
    prepareEditLibraryTag,
    setCurrentFroalaEditorInstance,
    setIsToolbarDisabled,
}) => ({
    ...FROALA_DEFAULT_CONFIG,
    key: froalaLicenseKey,
    events: {
        focus: () => setIsToolbarDisabled(false),
        blur: () => setIsToolbarDisabled(true),
        click: function (e) {
            // INITIALIZE FROALA CONFIGS
            const froalaEditorInstance = this;
            let tagElement = froalaEditorInstance.$box.find(
                `.fr-command[selected="true"]`
            );
            if (tagElement) {
                tagElement.attr('selected', false);
            }
            setCurrentFroalaEditorInstance(froalaEditorInstance);
            let element = null;
            if (e.currentTarget.className === 'fr-command sunwise-tag') {
                element = e.currentTarget;
            } else if (getParentNode(e.currentTarget, 10)) {
                element = getParentNode(e.currentTarget, 10);
            }

            if (element && element.dataset && element.dataset.settings) {
                element.setAttribute('selected', true);
                const data = element.dataset.settings;
                if (isJsonString(data)) {
                    const json = JSON.parse(data);
                    if (json.is_new_tag) {
                        prepareEditCustomTag(json);
                    } else {
                        prepareEditLibraryTag(json);
                    }
                }
            }
        },
    },
    fontFamily: getFontFamily(),
    htmlAllowedAttrs: getHtmlAllowedAttrs(FroalaEditor.DEFAULTS),
    language: FROALA_LANGUAGE_CODE_EQUIVALENCE[language.split('-')[0]],
    placeholderText: i18next.t(FROALA_DEFAULT_CONFIG.placeholderText),
    toolbarButtons: getToolbarButtons(FROALA_TOOLBAR_BUTTONS, isFullEdition),
    toolbarButtonsMD: getToolbarButtons(
        FROALA_TOOLBAR_BUTTONS_MD,
        isFullEdition
    ),
    toolbarButtonsSM: getToolbarButtons(
        FROALA_TOOLBAR_BUTTONS_SM,
        isFullEdition
    ),
    toolbarButtonsXS: getToolbarButtons(
        FROALA_TOOLBAR_BUTTONS_XS,
        isFullEdition
    ),
});

export const getHtmlAllowedAttrs = (defaults) => {
    return defaults.htmlAllowedAttrs.filter(
        (attr) => !FROALA_HTML_DENIED_ATTRS.includes(attr)
    );
};

export const getParentNode = (element, num) => {
    let parent = element;
    for (var i = 0; i < num; i++) {
        if (parent.parentElement) {
            parent = parent.parentElement;
            if (parent.className === 'fr-command sunwise-tag') {
                break;
            }
        }
    }
    return parent;
};

export const getToolbarButtons = (buttons, isFullEdition) => ({
    ...buttons,
    moreRich: isFullEdition
        ? buttons.moreRich
        : {
              buttons: ['insertTable'],
          },
});
