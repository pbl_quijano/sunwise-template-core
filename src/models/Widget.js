import { Model, fk, attr } from 'redux-orm';

class Widget extends Model {}
Widget.modelName = 'Widget';
Widget.fields = {
    field: attr(),
    hasSummarySupport: attr(),
    height: attr(),
    name: attr(),
    num: attr(),
    order: attr(),
    pageId: fk('Page', 'widgets'),
    proposalNumber: attr(),
    replaceInfoRequired: attr(),
    style: attr(),
    supportVersion: attr(),
    value: attr(),
    width: attr(),
    x: attr(),
    y: attr(),
};
export default Widget;
