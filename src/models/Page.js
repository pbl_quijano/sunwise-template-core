/* eslint-disable default-case */
import { Model, attr, fk } from 'redux-orm';

export default class Page extends Model {}
Page.modelName = 'Page';
Page.fields = {
    blocked: attr(),
    content: attr(),
    company: attr(),
    created_at: attr(),
    custom_template: attr(),
    grouped_by: attr(),
    id: attr(),
    infiniteMode: attr(),
    orientation: attr(),
    page: attr(),
    page_parent: attr(),
    parsedDataKey: attr(),
    replaceInfo: attr(),
    theme: attr(),
    updated_at: attr(),
    templateId: fk('Template', 'templatePages'),
    groupId: fk('Group', 'groupPages'),
};
