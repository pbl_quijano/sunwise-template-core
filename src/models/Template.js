import { Model, attr } from 'redux-orm';

export default class Template extends Model {}
Template.modelName = 'Template';
Template.fields = {
    company: attr(),
    created_at: attr(),
    description: attr(),
    footer: attr(),
    id: attr(),
    language: attr(),
    logo: attr(),
    parent: attr(),
    primary_color: attr(),
    proposals_number: attr(),
    secondary_color: attr(),
    title: attr(),
    type: attr(),
    typography: attr(),
    updated_at: attr(),
    version: attr(),
};
