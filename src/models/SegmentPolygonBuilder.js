import {
    SEGMENT_ACTIVE_COLOR,
    SEGMENT_COLOR,
    SEGMENT_STROKE_COLOR,
    SEGMENT_STROKE_ACTIVE_COLOR,
} from '@constants/maps';

import { createMeterLabelPopup, addPathListeners } from '@helpers/maps';

import SolarModulePolygonBuilder from './SolarModulePolygonBuilder ';

export default (google, vertexChange) => {
    const SolarModulePolygon = SolarModulePolygonBuilder(google);
    class SegmentPolygon extends google.maps.Polygon {
        constructor({
            id,
            solarModules = [],
            showMeterLabels,
            options = {},
            solarModulesStyle = {},
        }) {
            const newOptions = {
                fillColor: SEGMENT_ACTIVE_COLOR,
                strokeColor: SEGMENT_STROKE_ACTIVE_COLOR,
                strokePosition: google.maps.StrokePosition.OUTSIDE,
                fillOpacity: 1,
                editable: false,
                draggable: false,
                strokeWeight: 4,
                strokeOpacity: 1,
                zIndex: 2,
                ...options,
            };
            super(newOptions);
            this.disabled = false;
            this.id = id;
            this.meterLabels = createMeterLabelPopup(
                options.paths,
                google,
                showMeterLabels ? this.map : null
            );

            this.solarModules = solarModules.map((solarModule) => {
                const shape = new SolarModulePolygon(
                    solarModule.map((modulePoint) => ({
                        lat: modulePoint[0],
                        lng: modulePoint[1],
                    })),
                    this.map,
                    solarModulesStyle
                );
                return shape;
            });
            if (vertexChange) {
                addPathListeners({
                    google,
                    id: this.id,
                    map: this.map,
                    meterLabels: this.meterLabels,
                    paths: this.getPaths(),
                    setSolarModules: (solarModules) =>
                        this.setSolarModules(solarModules),
                    setSolarModulesDisabled: () =>
                        this.setSolarModulesDisabled(),
                    vertexChange,
                });
            }
        }

        resetPosition(segmentValue, showMeterLabels) {
            const shapeCoords = segmentValue.polygon.map(
                (point) => new google.maps.LatLng(point[0], point[1])
            );
            this.setPaths(shapeCoords);
            this.setSolarModules(segmentValue.solar_modules);
            this.setLabelsVisibility(false);
            this.meterLabels = createMeterLabelPopup(
                shapeCoords,
                google,
                showMeterLabels ? this.map : null
            );
            if (vertexChange) {
                addPathListeners({
                    google,
                    id: this.id,
                    map: this.map,
                    meterLabels: this.meterLabels,
                    paths: this.getPaths(),
                    setSolarModules: (solarModules) =>
                        this.setSolarModules(solarModules),
                    setSolarModulesDisabled: () =>
                        this.setSolarModulesDisabled(),
                    vertexChange,
                });
            }
        }

        setMap(map) {
            if (map === null) {
                this.meterLabels.forEach((meterLabel) =>
                    meterLabel.setMap(null)
                );
                this.meterLabels = [];
                this.getPaths().forEach((path) => {
                    google.maps.event.clearListeners(path, 'remove_at');
                    google.maps.event.clearListeners(path, 'set_at');
                    google.maps.event.clearListeners(path, 'insert_at');
                });
                this.solarModules.forEach((solarModule) => {
                    solarModule.setMap(null);
                });
                this.solarModules = [];
            }
            super.setMap(map);
        }

        setSelected(isSelcted) {
            if (isSelcted) {
                this.setOptions({
                    draggable: true,
                    editable: true,
                    fillColor: SEGMENT_ACTIVE_COLOR,
                    strokeColor: SEGMENT_STROKE_ACTIVE_COLOR,
                });
                this.meterLabels.forEach((meterLabel) =>
                    meterLabel.setMap(this.map)
                );
            } else {
                this.setOptions({
                    draggable: false,
                    editable: false,
                    fillColor: SEGMENT_COLOR,
                    strokeColor: SEGMENT_STROKE_COLOR,
                });
                this.meterLabels.forEach((meterLabel) =>
                    meterLabel.setMap(null)
                );
            }
        }

        setDisabled(isDisabled) {
            this.disabled = isDisabled;
            this.setSolarModulesDisabled(isDisabled);
            if (isDisabled) {
                this.setOptions({
                    clickable: false,
                    fillOpacity: 0.6,
                    strokeOpacity: 0.6,
                });
            } else {
                this.setOptions({
                    clickable: true,
                    fillOpacity: 1,
                    strokeOpacity: 1,
                });
            }
        }

        setSolarModules(solarModules) {
            this.solarModules.forEach((solarModule) => {
                solarModule.setMap(null);
                google.maps.event.clearListeners(solarModule, 'click');
            });
            this.solarModules = solarModules.map((solarModule) => {
                return new SolarModulePolygon(
                    solarModule.map((modulePoint) => ({
                        lat: modulePoint[0],
                        lng: modulePoint[1],
                    })),
                    this.map
                );
            });
        }

        setSolarModulesDisabled(isDisabled = true) {
            this.solarModules.forEach((solarModule) => {
                solarModule.setDisabled(isDisabled);
            });
        }

        setSolarModulesFillColor(fillColor) {
            this.solarModules.forEach((solarModule) => {
                solarModule.setOptions({
                    fillColor,
                });
            });
        }

        setSolarModulesStrokeColor(strokeColor) {
            this.solarModules.forEach((solarModule) => {
                solarModule.setOptions({
                    strokeColor,
                });
            });
        }

        setLabelsVisibility(isVisible) {
            if (isVisible) {
                this.meterLabels.forEach((meterLabel) =>
                    meterLabel.setMap(this.map)
                );
            } else {
                this.meterLabels.forEach((meterLabel) =>
                    meterLabel.setMap(null)
                );
            }
        }
    }

    return SegmentPolygon;
};
