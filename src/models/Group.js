/* eslint-disable default-case */
import { Model, fk, attr } from 'redux-orm';

class Group extends Model {}
Group.modelName = 'Group';
Group.fields = {
    blocked: attr(),
    id: attr(),
    page: attr(),
    templateId: fk('Template', 'groups'),
};
export default Group;
