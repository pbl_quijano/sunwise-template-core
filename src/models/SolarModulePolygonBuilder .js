import { SOLAR_MODULE_COLOR, SOLAR_MODULE_STROKE_COLOR } from '@constants/maps';

export default (google) => {
    class SolarModulePolygon extends google.maps.Polygon {
        constructor(
            paths,
            map,
            {
                strokeColor = SOLAR_MODULE_STROKE_COLOR,
                fillColor = SOLAR_MODULE_COLOR,
            } = {}
        ) {
            super({
                clickable: false,
                paths,
                strokeColor,
                fillColor,
                fillOpacity: 1,
                strokePosition: google.maps.StrokePosition.INSIDE,
                map,
                strokeWeight: 1,
                zIndex: 3,
            });
        }
        setDisabled(isDisabled = true) {
            if (isDisabled) {
                this.setOptions({
                    fillOpacity: 0.2,
                    strokeWeight: 0,
                });
            } else {
                this.setOptions({
                    fillOpacity: 1,
                    strokeWeight: 1,
                });
            }
        }
    }

    return SolarModulePolygon;
};
