import PropTypes from 'prop-types';
import styled from 'styled-components';

import RoundedIcon from './RoundedIcon';

const StyledButton = styled.button.attrs((props) => ({
    className: `btn${props.isBlock ? ' btn-block' : ''}${
        props.size ? ` btn-${props.size}` : ``
    }${props.variant ? ` btn-${props.variant}` : ``}${
        props.append ? ' pr-1' : ''
    }${props.prepend ? ' pl-1' : ''}`,
}))`
    align-items: center;
    cursor: pointer;
    display: ${({ isBlock }) => (isBlock ? 'block' : 'inline-flex')};
    position: relative;
    ${({ fontSize }) => fontSize && `font-size: ${fontSize}`};
    ${({ fontWeight }) => fontWeight && `font-weight: ${fontWeight}`};
    ${({ margin }) => margin && `margin: ${margin}`};
    ${({ width }) => width && `width: ${width}`};
    ${({ height }) => height && `height: ${height}`};

    .focus,
    :focus {
        box-shadow: none;
        outline: none;
    }

    &.btn-sm {
        font-size: 13px;
    }

    ${({ variant }) =>
        variant === 'cancel' &&
        `
        border: 1px solid #D3D7EB;
        background-color: #EFF1FB;
        color: #848BAB;
        &:hover {
            color: #848BAB;
        }
    `}

    ${({ variant }) =>
        variant === 'confirm' &&
        `
        color: #fff;
        background-color: #ff9a00;
    `}

    ${({ variant }) =>
        variant === 'approved' &&
        `
        color: #fff;
        background-color: #2BF2AB;
    `}

    ${({ variant }) =>
        variant === 'rejected' &&
        `
        background-color: #FFFFFF;
        border: 1px solid #FA6968;
        color: #FA6968;
    `}

    ${({ variant }) =>
        variant === 'reject-reason' &&
        `
        background-color: #F7FCFF;
        color: #848BAB;
        font-size: 10px;
        
    `}    

    ${({ disabled }) =>
        disabled &&
        `
        pointer-events: none;
        opacity: 0.5;
        cursor: default;
    `}

    ${({ isRounded, borderRadius }) =>
        isRounded && `border-radius: ${borderRadius};`}
    ${({ borderColor }) => borderColor && `border-color: ${borderColor}`}
    ${({ borderStyle }) => borderStyle && `border-style: ${borderStyle}`}
    ${({ textColor }) => textColor && `color: ${textColor}`}
    ${({ background }) => background && `background: ${background}`}
`;

const ButtonComponent = ({
    as,
    background,
    borderColor,
    borderRadius = '30px',
    borderStyle,
    children,
    className,
    dataIntercomTarget,
    disabled,
    iconConfig,
    fontWeight,
    fontSize,
    height,
    href,
    onClick,
    isBlock,
    isRounded = true,
    margin,
    variant,
    size,
    target,
    textColor,
    width,
    to,
    visible = true,
    type,
}) => {
    return visible ? (
        <StyledButton
            append={iconConfig ? iconConfig.append : null}
            as={as}
            background={background}
            borderColor={borderColor}
            borderRadius={borderRadius}
            borderStyle={borderStyle}
            className={className}
            data-intercom-target={dataIntercomTarget}
            isBlock={isBlock}
            isRounded={isRounded}
            disabled={disabled}
            fontSize={fontSize}
            fontWeight={fontWeight}
            height={height}
            href={href}
            onClick={onClick}
            variant={variant}
            prepend={iconConfig ? iconConfig.prepend : null}
            size={size}
            target={target}
            textColor={textColor}
            width={width}
            margin={margin}
            to={to}
            type={type}
        >
            {iconConfig && iconConfig.prepend && (
                <RoundedIcon
                    className={children ? `mr-2` : ``}
                    {...iconConfig.prepend}
                />
            )}

            <span className="flex-grow-1">{children}</span>

            {iconConfig && iconConfig.append && (
                <RoundedIcon className="ml-2" {...iconConfig.append} />
            )}
        </StyledButton>
    ) : null;
};
ButtonComponent.propTypes = {
    as: PropTypes.string,
    background: PropTypes.string,
    borderColor: PropTypes.string,
    borderRadius: PropTypes.string,
    borderStyle: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    className: PropTypes.string,
    dataIntercomTarget: PropTypes.string,
    disabled: PropTypes.bool,
    iconConfig: PropTypes.object,
    fontWeight: PropTypes.string,
    fontSize: PropTypes.string,
    height: PropTypes.string,
    href: PropTypes.string,
    onClick: PropTypes.func,
    isBlock: PropTypes.bool,
    isRounded: PropTypes.bool,
    margin: PropTypes.string,
    variant: PropTypes.string,
    size: PropTypes.string,
    target: PropTypes.string,
    textColor: PropTypes.string,
    width: PropTypes.string,
    to: PropTypes.string,
    visible: PropTypes.bool,
    type: PropTypes.string,
};
ButtonComponent.displayName = 'ButtonComponent';

export default ButtonComponent;
