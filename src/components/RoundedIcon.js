import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
    display: inline-block;
    &.disabled {
        opacity: 0.6;
        pointer-events: none;
    }
`;

const StyledRoundedIcon = styled.div`
    align-items: center;
    background-color: ${({ bgColor, noBackground }) =>
        noBackground ? `none` : bgColor ? bgColor : `#F8FCFF`};
    border-radius: 30px;
    border: ${({ noBorder, borderColor }) =>
        noBorder
            ? `none`
            : `1px solid ${borderColor ? `${borderColor}` : `#F1F7FD`}`};
    display: flex;
    height: 30px;
    justify-content: center;
    text-align: center;
    width: 30px;
    border-radius: 100%;

    ${({ customSize, size }) => {
        switch (size) {
            case 'xs':
                return `
                    height: 20px;
                    width: 20px;
                `;
            case 'md':
                return `
                    height: 40px;
                    width: 40px;
                `;
            case 'lg':
                return `
                    height: 50px;
                    width: 50px;
                `;
            default:
                return `
                    height: ${customSize}px;
                    width: ${customSize}px;
                `;
        }
    }}

    ${({ clickable, onClick }) => (clickable || onClick) && `cursor: pointer;`}
    ${({ disabled }) => disabled && `cursor: not-allowed;pointer-events: none;`}
`;

const StyledIcon = styled.span`
    color: ${({ iconColor }) => (iconColor ? iconColor : '#1F3C53')};
    font-size: ${({ size, customIconSize }) =>
        size === 'custom' && customIconSize
            ? `${customIconSize}px`
            : size === 'xs'
            ? `10px`
            : size === 'md'
            ? `15px`
            : size === 'lg'
            ? `25px`
            : `11px`};
    img {
        display: block;
    }
`;

/**
 *
 * Componente RoundedIcon.
 *
 * @param size Medidas xs | sm | md | lg | custom
 * @param customSize Si size=custom tomará este valor
 *        para agregar a las medidas.
 * @param icon Clase o caracter representando el icóno.
 * @param customIconSize Tamaño del icono personalizable.
 * @param bgColor Color del fondo.
 * @param borderColor Color del borde.
 * @param iconColor Color del icono.
 * @param noBorder True si no se desea el borde.
 * @param noBackgroun True si no se desea el fondo.
 * @param isIconLetter True si el icono es un caracter.
 * @param clickable True si se desea que cuando pase el raton
 *                  encima muestre la mano del mouse.
 * @param handleClik Funcion al hacer click al elemento.
 */
const RoundedIcon = ({
    bgColor,
    borderColor,
    className,
    clickable,
    customIconSize,
    customSize,
    disabled,
    handleClick,
    icon,
    iconColor,
    iconImg,
    isIconLetter,
    noBackground,
    noBorder,
    size,
    visible = true,
}) => {
    if (!visible) {
        return null;
    }
    return (
        <Wrapper className={disabled && 'disabled'}>
            <StyledRoundedIcon
                bgColor={bgColor}
                borderColor={borderColor}
                className={className}
                clickable={clickable}
                customSize={customSize}
                noBackground={noBackground}
                noBorder={noBorder}
                onClick={handleClick}
                size={size}
                disabled={disabled}
            >
                <StyledIcon
                    className={isIconLetter || icon}
                    customIconSize={customIconSize}
                    iconColor={iconColor}
                    size={size}
                >
                    {iconImg ? (
                        <img alt="" src={iconImg} width="14" />
                    ) : (
                        isIconLetter && icon
                    )}
                </StyledIcon>
            </StyledRoundedIcon>
        </Wrapper>
    );
};

RoundedIcon.propTypes = {
    bgColor: PropTypes.string,
    borderColor: PropTypes.string,
    className: PropTypes.string,
    clickable: PropTypes.bool,
    customIconSize: PropTypes.string,
    customSize: PropTypes.string,
    disabled: PropTypes.bool,
    handleClick: PropTypes.func,
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    iconColor: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    iconImg: PropTypes.string,
    isIconLetter: PropTypes.bool,
    noBackground: PropTypes.bool,
    noBorder: PropTypes.bool,
    size: PropTypes.string,
    visible: PropTypes.bool,
};

export default RoundedIcon;
