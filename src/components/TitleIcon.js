import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledSection = styled.div`
    align-items: center;
    display: flex;
    line-height: 44px;
    margin-bottom: ${({ marginBottom }) => marginBottom};
    margin-top: ${({ marginTop }) => marginTop};
`;

const StyledImage = styled.img`
    margin-right: 10px;
    max-width: 100%;
`;

const StyledTitle = styled.div`
    color: #202253;
    font-size: ${({ fontSize }) => fontSize};
    font-weight: ${({ fontWeight }) => fontWeight};
    letter-spacing: ${({ letterSpacing }) => letterSpacing};
    line-height: ${({ lineHeight }) => lineHeight};
    max-width: 90%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
`;

const FontIcon = styled.i`
    color: #ff9a00;
    font-size: 21px;
    margin-right: 15px;
`;

const TitleIcon = ({
    fontSize = '22px',
    fontWeight = 'normal',
    imgUrl,
    isFontIcon,
    letterSpacing = '0.15px',
    lineHeight = '27px',
    marginBottom = '0',
    marginTop = '0',
    title,
}) => {
    return (
        <StyledSection marginBottom={marginBottom} marginTop={marginTop}>
            {!imgUrl ? null : isFontIcon ? (
                <FontIcon className={`fa ${imgUrl}`} />
            ) : (
                <StyledImage src={imgUrl} width={19} />
            )}

            <StyledTitle
                fontSize={fontSize}
                fontWeight={fontWeight}
                letterSpacing={letterSpacing}
                lineHeight={lineHeight}
            >
                {title}
            </StyledTitle>
        </StyledSection>
    );
};

TitleIcon.propTypes = {
    fontSize: PropTypes.string,
    fontWeight: PropTypes.string,
    imgUrl: PropTypes.string,
    isFontIcon: PropTypes.bool,
    letterSpacing: PropTypes.string,
    lineHeight: PropTypes.string,
    marginBottom: PropTypes.string,
    marginTop: PropTypes.string,
    title: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
};

export default TitleIcon;
