import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButton = styled.button`
    background: linear-gradient(208.97deg, #ffcb01 0%, #e7a804 100%);
    border-radius: 18px;
    border: 0 none;
    color: #ffffff;
    font-size: 14px;
    font-weight: bold;
    line-height: 18px;
    margin-bottom: 1em;
    padding: 10px;
    text-align: center;
    white-space: nowrap !important;
    width: 100%;

    &[disabled] {
        background: linear-gradient(208.97deg, #848bab 0%, #848bab 100%);
    }

    ${({ size }) =>
        size === 'small' &&
        `color: #EFF1FB;
        font-size: 12px;
        letter-spacing: 0.31px;
        line-height: 18px;
        padding: 7px 25px;
        width: auto;
    `}
`;

const AddButton = ({
    children,
    className,
    disabled,
    onClick,
    size = 'md',
    style,
    type = 'button',
    variant,
    visible = true,
}) => {
    return visible ? (
        <StyledButton
            className={className}
            disabled={disabled}
            onClick={onClick}
            size={size}
            variant={variant}
            style={style}
            type={type}
        >
            {children}
        </StyledButton>
    ) : null;
};

AddButton.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element,
        PropTypes.string,
    ]),
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    size: PropTypes.string,
    style: PropTypes.string,
    type: PropTypes.string,
    variant: PropTypes.string,
    visible: PropTypes.bool,
};

export default AddButton;
