import styled from 'styled-components';

export default styled.div`
    input,
    textarea {
        &.with-error {
            background-color: #ffe3e3;
            border-color: red;
        }
        &.rpu-field {
            letter-spacing: 4px;
            font-size: 16px;
        }
        &::placeholder {
            color: #ccd1e8;
            opacity: 1;
        }

        &:-ms-input-placeholder {
            color: red;
        }

        &::-ms-input-placeholder {
            color: red;
        }
    }
    select {
        option:disabled {
            color: #ccd1e8;
        }
        &.with-error {
            background-color: #ffe3e3;
            border-color: red;
        }
    }

    .form-group > label {
        width: 90%;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .input-group-text {
        border: 0;
    }
`;
