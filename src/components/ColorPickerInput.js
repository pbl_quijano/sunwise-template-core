import PropTypes from 'prop-types';
import { useState } from 'react';
import { ChromePicker } from 'react-color';
import styled from 'styled-components';

const Color = styled.div`
    background-color: ${({ color }) => color && color};
    border-radius: 4px;
    height: 32px;
    width: 32px;
`;

const ColorPickerWrapper = styled.div`
    align-items: center;
    background-color: rgba(255, 255, 255, 0.02);
    border-radius: 3px;
    border: 1px solid #eff1fb;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    display: flex;
    max-width: 132px;
    min-width: 132px;
    padding: 6px 6px;
    position: relative;
    width: 132px;
`;

const Container = styled.div`
    align-items: center;
    display: flex;
    font-size: 13px;
`;

const Label = styled.span`
    color: #848bab;
    font-size: 13px;
    line-height: 16px;
    margin-right: 23px;
    min-height: 16px;
    text-align: right;
    width: 135px;
`;

const Swatch = styled.div`
    background: #fff;
    border-radius: 5px;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1);
    cursor: pointer;
    display: inline-block;
    margin-right: 10px;
`;

const Popover = styled.div`
    position: absolute;
    right: 0;
    top: 0;
    z-index: 2;
`;

const CloseButton = styled.div`
    background-color: rgba(0, 0, 0, 0.8);
    border-radius: 50%;
    color: #fff;
    cursor: pointer;
    font-size: 12px;
    height: 20px;
    line-height: 20px;
    position: absolute;
    right: -24px;
    text-align: center;
    top: 0;
    width: 20px;
`;

const ColorPickerInput = ({
    hasAlphaChanel = false,
    label,
    onChange,
    value,
}) => {
    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const color = value || (hasAlphaChanel ? '#000000ff' : '#000000');
    const [currentColor, setCurrentColor] = useState(color);

    const completeChange = (tempColor) => {
        const colorHex = hasAlphaChanel
            ? `${tempColor.hex}${parseInt(tempColor.rgb.a * 255).toString(16)}`
            : tempColor.hex;
        onChange(colorHex);
    };

    const handleChange = (tempColor) => {
        const colorHex = hasAlphaChanel
            ? `${tempColor.hex}${parseInt(tempColor.rgb.a * 255).toString(16)}`
            : tempColor.hex;
        setCurrentColor(colorHex);
    };

    return (
        <Container>
            <Label>{label}</Label>

            <ColorPickerWrapper>
                <Swatch
                    onClick={() => setDisplayColorPicker(!displayColorPicker)}
                >
                    <Color color={currentColor} />
                </Swatch>

                {displayColorPicker && (
                    <Popover>
                        <CloseButton
                            onClick={() => setDisplayColorPicker(false)}
                        >
                            x
                        </CloseButton>
                        <ChromePicker
                            color={currentColor}
                            onChange={handleChange}
                            onChangeComplete={completeChange}
                        />
                    </Popover>
                )}
                {currentColor}
            </ColorPickerWrapper>
        </Container>
    );
};

ColorPickerInput.propTypes = {
    hasAlphaChanel: PropTypes.bool,
    label: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.string,
};

export default ColorPickerInput;
