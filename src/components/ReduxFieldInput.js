import PropTypes from 'prop-types';
import { Form, InputGroup } from 'react-bootstrap';
import { Element } from 'react-scroll';
import styled from 'styled-components';

import LabelError from './LabelError';
import WrapperInput from './WrapperInput';

const StyledFormControl = styled(Form.Control)`
    ${({ size }) => size && `font-size: 12px; line-height:15px;`};
`;

const ReduxFieldInput = ({
    append,
    appendIcon,
    as,
    autoComplete,
    children,
    className,
    classNameFormGroup,
    classNameInputGroup,
    controlConfig,
    cols,
    dataIntercomTarget,
    defaultBehevior = false,
    disabled,
    id,
    input: { value, name, onChange, onBlur, onFocus },
    meta: { touched, error },
    placeholder,
    label,
    readOnly,
    type,
    max,
    maxLength,
    min,
    step,
    onKeyDown,
    onlyNumbers = false,
    onlyIntegerNumbers = false,
    numberTypeOnBlurActionDisabled,
    prepend,
    prependIcon,
    prependSvgIcon,
    required,
    rows,
    size,
    style,
    tabIndex,
}) => {
    const onChangeAction = (event, defaultBehevior, onlyNumbers) => {
        const text = event.target.value;

        if (onlyNumbers) {
            event.target.value = text.replace(/[A-Za-z]/gi, '');
            onChange(event);

            return;
        }

        if (onlyIntegerNumbers) {
            event.target.value = text.replace(/[A-Za-z$,.]/gi, '');
            onChange(event);

            return;
        }

        if (
            !(
                text.length > 1 &&
                text.charAt(0) === '0' &&
                text.charAt(1) === '0' &&
                !defaultBehevior
            )
        ) {
            if (type === 'number' && parseFloat(text) >= 1) {
                event.target.value = text.replace(/^0+/, '');
            }
            onChange(event);
        } else {
            onChange(event);
        }
    };

    const onBlurAction = (event, blurValue) => {
        if (
            !numberTypeOnBlurActionDisabled &&
            type === 'number' &&
            event.target.value === ''
        ) {
            event.preventDefault();
            blurValue = min || 0;
            event.target.value = min || 0;
        }

        onBlur(event, blurValue);
    };

    return (
        <WrapperInput style={style}>
            <Form.Group className={classNameFormGroup}>
                {label && <Form.Label title={label}>{label}</Form.Label>}

                <InputGroup className={classNameInputGroup}>
                    {(prepend || prependIcon || prependSvgIcon) && (
                        <InputGroup.Prepend>
                            <InputGroup.Text>
                                {prependIcon && <i className={prependIcon} />}
                                {prependSvgIcon && (
                                    <img alt="" src={prependSvgIcon} />
                                )}
                                {prepend}
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                    )}
                    {name && <Element name={`position-${name}`} />}
                    <StyledFormControl
                        as={as}
                        autoComplete={autoComplete}
                        className={`${className} ${
                            touched && error && 'with-error'
                        } ${
                            required && (value === null || value === '')
                                ? 'border-danger'
                                : ''
                        }`}
                        cols={cols}
                        data-intercom-target={dataIntercomTarget}
                        disabled={disabled}
                        id={id}
                        max={max}
                        maxLength={maxLength}
                        min={min}
                        onBlur={onBlurAction}
                        onFocus={onFocus}
                        onChange={(e) =>
                            onChangeAction(e, defaultBehevior, onlyNumbers)
                        }
                        onKeyDown={onKeyDown}
                        placeholder={placeholder}
                        readOnly={readOnly}
                        required={required}
                        rows={rows}
                        size={size}
                        step={step}
                        style={{ resize: 'none' }}
                        tabIndex={tabIndex}
                        type={type}
                        value={value}
                        {...controlConfig}
                    >
                        {children}
                    </StyledFormControl>

                    {(appendIcon || append) && (
                        <InputGroup.Append>
                            <InputGroup.Text>
                                {appendIcon && <img alt="" src={appendIcon} />}
                                {append}
                            </InputGroup.Text>
                        </InputGroup.Append>
                    )}
                </InputGroup>

                {touched && error && (
                    <LabelError type="error">{error}</LabelError>
                )}
            </Form.Group>
        </WrapperInput>
    );
};

ReduxFieldInput.propTypes = {
    append: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    appendIcon: PropTypes.string,
    as: PropTypes.string,
    autoComplete: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    children: PropTypes.oneOfType([PropTypes.elementType, PropTypes.array]),
    className: PropTypes.string,
    classNameFormGroup: PropTypes.string,
    classNameInputGroup: PropTypes.string,
    controlConfig: PropTypes.object,
    cols: PropTypes.string,
    dataIntercomTarget: PropTypes.string,
    defaultBehevior: PropTypes.bool,
    disabled: PropTypes.bool,
    id: PropTypes.string,
    input: PropTypes.object,
    meta: PropTypes.object,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    readOnly: PropTypes.bool,
    type: PropTypes.string,
    max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    numberTypeOnBlurActionDisabled: PropTypes.bool,
    step: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onKeyDown: PropTypes.func,
    onlyNumbers: PropTypes.bool,
    onlyIntegerNumbers: PropTypes.bool,
    prepend: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    prependIcon: PropTypes.string,
    prependSvgIcon: PropTypes.string,
    required: PropTypes.bool,
    rows: PropTypes.string,
    size: PropTypes.string,
    style: PropTypes.object,
    tabIndex: PropTypes.number,
};

export default ReduxFieldInput;
