import isNil from 'lodash/isNil';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import PageProperties from '@modules/PageProperties';
import * as templateViewSelectors from '@modules/TemplateView/selectors';
import WidgetMenu from '@modules/WidgetMenu';

const WIDGETS_SIDE_WIDTH = 332;

const Container = styled.div`
    display: flex;
    background-color: #ffffff;
    border-left: 1px solid #eff1fb;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    position: relative;
    z-index: 55;
    ${({ disabled }) =>
        disabled &&
        `opacity: 0.6;
    cursor: not-allowed;
    pointer-events: none;`}

    &:hover .resize-button {
        opacity: 1;
    }
`;

const MenuContainer = styled.div`
    background-color: #ffffff;
    box-shadow: 0 2px 12px 0 rgba(129, 158, 200, 0.06);
    display: flex;
    flex-direction: column;
    height: 100%;
    min-width: ${WIDGETS_SIDE_WIDTH}px;
    overflow-y: auto;
    width: ${WIDGETS_SIDE_WIDTH}px;
`;

const Wrapper = styled.div`
    max-width: ${WIDGETS_SIDE_WIDTH}px;
    overflow: hidden;
    transition: max-width 0.3s linear;
    width: ${WIDGETS_SIDE_WIDTH}px;

    &.hide {
        max-width: 0;
    }
`;

const ToggleButtonContainer = styled.div`
    align-items: center;
    background-color: transparent;
    box-shadow: 10px 12px 17px 0 rgba(129, 158, 200, 0.06);
    cursor: pointer;
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: hidden;
    transition: width 0.2s ease-in, background-color 0.2s linear;
    width: 0;

    &:hover {
        background-color: rgba(0, 0, 0, 0.06);
    }

    &.hide-mode {
        transition-delay: 0.25s;
        width: 26px;
    }
`;

const StyledIcon = styled.i`
    color: #848bab;
    font-size: 20px;
`;

const ResizeButton = styled.div`
    background-color: #fff;
    border-radius: 50%;
    box-shadow: rgba(9, 30, 66, 0.08) 0px 0px 0px 1px,
        rgba(9, 30, 66, 0.08) 0px 2px 4px 1px;
    color: #ff9a00;
    cursor: pointer;
    font-size: 12px;
    height: 24px;
    line-height: 24px;
    opacity: 0;
    position: absolute;
    right: ${WIDGETS_SIDE_WIDTH - 12}px;
    text-align: center;
    top: 75px;
    visibility: ${({ isShowing }) => (isShowing ? 'visible' : 'hidden')};
    width: 24px;
    z-index: 55;

    i {
        margin-left: 1px;
    }

    &:hover {
        color: #fff;
        background-color: #ff9a00;
    }

    ${({ isShowing }) =>
        isShowing &&
        `transition: background-color 100ms, color 100ms,
        visibility 0s linear 0.5s, opacity 0.2s;`}
`;

const WidgetSideMenu = ({
    canUpdatePanelsSowing,
    canUpdateProjectLocation,
    key = '',
    selectedPageIsBlocked,
    themeEnabled = true,
}) => {
    const [isShowing, setIsShowing] = useState(
        isNil(localStorage.getItem(key + ':rightSideShowed')) ||
            localStorage.getItem(key + ':rightSideShowed') === 'true'
    );

    const handleSetIsShowing = (value) => {
        localStorage.setItem(key + ':rightSideShowed', value);
        setIsShowing(value);
    };

    return (
        <Container disabled={selectedPageIsBlocked}>
            <ToggleButtonContainer
                className={!isShowing && 'hide-mode'}
                onClick={() => handleSetIsShowing(true)}
            >
                <StyledIcon className="fa fa-chevron-left" />
            </ToggleButtonContainer>

            <Wrapper className={!isShowing && 'hide'}>
                <MenuContainer>
                    <PageProperties themeEnabled={themeEnabled} />

                    <WidgetMenu
                        canUpdatePanelsSowing={canUpdatePanelsSowing}
                        canUpdateProjectLocation={canUpdateProjectLocation}
                    />
                </MenuContainer>
            </Wrapper>

            <ResizeButton
                className="resize-button"
                isShowing={isShowing}
                onClick={() => handleSetIsShowing(false)}
            >
                <i className="fas fa-chevron-right" />
            </ResizeButton>
        </Container>
    );
};

const mapStateToProps = createStructuredSelector({
    selectedPageIsBlocked: templateViewSelectors.getSelectedPageIsBlocked,
});

WidgetSideMenu.propTypes = {
    canUpdatePanelsSowing: PropTypes.bool,
    canUpdateProjectLocation: PropTypes.bool,
    key: PropTypes.string,
    selectedPageIsBlocked: PropTypes.bool,
    themeEnabled: PropTypes.bool,
};

export default connect(mapStateToProps, null)(WidgetSideMenu);
