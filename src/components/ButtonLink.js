import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledButtonLink = styled.div`
    color: #ff9a00;
    cursor: pointer;
    font-size: 10px;
    margin-left: 30px;
    margin-top: -10px;
    text-align: right;
`;

const ButtonLink = ({ children, handleClick, text, visible = true }) => {
    return visible ? (
        <StyledButtonLink onClick={handleClick}>
            {text}
            {children}
        </StyledButtonLink>
    ) : null;
};
ButtonLink.propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    handleClick: PropTypes.func,
    text: PropTypes.string,
    visible: PropTypes.bool,
};

export default ButtonLink;
