import PropTypes from 'prop-types';
import { Col, Row, Modal } from 'react-bootstrap';
import styled from 'styled-components';

import TitleIcon from './TitleIcon';

const ModalWrapper = styled(Modal)`
    && {
        .close {
            background-color: #f9fafe;
            border-radius: 100%;
            height: 38px;
            position: absolute;
            right: 15px;
            top: 15px;
            width: 38px;
            z-index: 20;
        }

        .modal-body {
            padding: ${({ padding }) => padding};
        }

        .modal-footer {
            padding: 0;
            justify-content: flex-start;
        }
    }
`;

const ModalBootstrap = ({
    backdrop = 'static',
    backdropClassName,
    children,
    className,
    classNameBody,
    closeButton = true,
    imgUrl,
    isFontIcon = false,
    keyboard = true,
    onEnter,
    onExited,
    onHide,
    show,
    size = 'xl',
    title,
    padding = '1rem',
    footerComponent: FooterComponent = null,
}) => (
    <ModalWrapper
        backdrop={backdrop}
        backdropClassName={backdropClassName}
        centered
        className={className}
        keyboard={keyboard}
        onEnter={onEnter}
        onExited={onExited}
        onHide={onHide}
        show={show}
        size={size}
        padding={padding}
    >
        <Modal.Body className={classNameBody}>
            {closeButton && onHide && (
                <button type="button" className="close" onClick={onHide}>
                    <span aria-hidden="true">×</span>
                    <span className="sr-only">Close</span>
                </button>
            )}

            {title && (
                <>
                    <Row>
                        <Col>
                            <TitleIcon
                                imgUrl={imgUrl}
                                isFontIcon={isFontIcon}
                                title={title}
                                fontSize="22px"
                                fontWeight="bold"
                            />
                        </Col>
                    </Row>

                    <hr className="mt-3" />
                </>
            )}
            {children}
        </Modal.Body>
        {FooterComponent && (
            <Modal.Footer>
                <FooterComponent />
            </Modal.Footer>
        )}
    </ModalWrapper>
);

ModalBootstrap.propTypes = {
    backdrop: PropTypes.string,
    backdropClassName: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    className: PropTypes.string,
    classNameBody: PropTypes.string,
    closeButton: PropTypes.bool,
    imgUrl: PropTypes.string,
    isFontIcon: PropTypes.bool,
    keyboard: PropTypes.bool,
    onEnter: PropTypes.func,
    onExited: PropTypes.func,
    onHide: PropTypes.func,
    show: PropTypes.bool,
    size: PropTypes.string,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    padding: PropTypes.string,
    footerComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
    resetForm: PropTypes.func,
};

export default ModalBootstrap;
