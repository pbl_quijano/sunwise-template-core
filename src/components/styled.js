import styled from 'styled-components';

const Container = styled.div`
    display: ${({ isVisible = true }) => (isVisible ? 'flex' : 'none')};
    flex-direction: row;
    flex-wrap: ${({ wrap = 'nowrap' }) => wrap};
    ${({ padding }) => padding && `padding: ${padding};`}
    ${({ margin }) => margin && `margin: ${margin};`}
    ${({ alignItems }) => alignItems && `align-items: ${alignItems};`}
    ${({ justifyContent }) =>
        justifyContent && `justify-content: ${justifyContent};`}
`;

export const FlexRow = styled(Container)`
    flex-direction: row;
`;

export const FlexColumn = styled(Container)`
    flex-direction: column;
`;

export const Spacer = styled.div`
    flex-basis: 0;
    flex-grow: 1;
`;
