import styled from 'styled-components';

export default styled.div`
    color: ${({ color }) => (color ? color : '#fff')};
    font-size: ${({ fontSize }) => (fontSize ? fontSize : '11px')};
    padding-top: 4px;
    padding-left: 3px;
    line-height: 11px;
    ${({ type }) => type === 'error' && `color: red;`};
`;
