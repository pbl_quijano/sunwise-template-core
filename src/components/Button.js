import PropTypes from 'prop-types';
import styled from 'styled-components';

import RoundedIcon from './RoundedIcon';

const StyledButton = styled.button`
    background: none;
    border: none;
    color: ${({ color = '#848bab' }) => color};
    cursor: pointer;
    font-size: ${({ customSize = 14 }) => `${customSize}px`};
    display: flex;
    align-items: center;
    justify-content: center;
    outline: none;

    & div {
        margin-left: 0.5rem;
    }

    &:focus {
        outline: none;
    }

    ${({ disabled }) =>
        disabled &&
        `
        opacity: .2;
    `}

    ${({ variant }) =>
        variant === 'bold' &&
        `
        font-weight: 700;
        color: ${({ color = '#002438' }) => color};
    `}

    ${({ direction }) =>
        direction === 'reverse' &&
        `flex-direction: row-reverse;

        & > span {
            margin-left: 0;
            margin-right: 0.5rem;

            div{
                margin-left: 0;
            }
        }
    `}
`;

const Button = ({
    bgColor = '#FF9A00',
    color,
    className,
    classNameIcon,
    customSize,
    dataIntercomTarget,
    direction,
    disabled,
    handleClick,
    icon,
    iconColor = '#FFF',
    label,
    size = 'md',
    variant,
    visible = true,
}) => {
    return visible ? (
        <StyledButton
            color={color}
            className={className}
            customSize={customSize}
            data-intercom-target={dataIntercomTarget}
            direction={direction}
            disabled={disabled}
            onClick={handleClick}
            variant={variant}
        >
            {label}

            <RoundedIcon
                bgColor={bgColor}
                className={classNameIcon}
                icon={icon}
                iconColor={iconColor}
                noBorder
                size={size}
            />
        </StyledButton>
    ) : null;
};

Button.propTypes = {
    bgColor: PropTypes.string,
    color: PropTypes.string,
    className: PropTypes.string,
    classNameIcon: PropTypes.string,
    customSize: PropTypes.number,
    dataIntercomTarget: PropTypes.string,
    direction: PropTypes.string,
    disabled: PropTypes.bool,
    handleClick: PropTypes.func,
    icon: PropTypes.string,
    iconColor: PropTypes.string,
    label: PropTypes.string,
    size: PropTypes.string,
    variant: PropTypes.string,
    visible: PropTypes.bool,
};

export default Button;
