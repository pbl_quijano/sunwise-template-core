import PropTypes from 'prop-types';
import styled from 'styled-components';

import InputFieldText from './InputFieldText';
import RoundedIcon from './RoundedIcon';

const StyledInputSearch = styled.div`
    align-items: center;
    display: flex;
`;

const InputSearch = ({
    disabled,
    placeholder,
    onChange,
    value,
    visible = true,
}) => {
    const handleOnChange = (e) => onChange(e);
    if (!visible) {
        return null;
    }
    return (
        <StyledInputSearch>
            <RoundedIcon
                bgColor="#ff9a00"
                disabled={disabled}
                icon="fa fa-search"
                iconColor="#FFF"
                noBorder
            />

            <InputFieldText
                disabled={disabled}
                placeholder={placeholder}
                onChange={handleOnChange}
                value={value}
            />
        </StyledInputSearch>
    );
};

InputSearch.propTypes = {
    disabled: PropTypes.bool,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    value: PropTypes.string,
    visible: PropTypes.bool,
};

export default InputSearch;
