import PropTypes from 'prop-types';
import { InputGroup, Form } from 'react-bootstrap';
import { Element } from 'react-scroll';
import styled from 'styled-components';

import LabelError from './LabelError';

const WrapperInput = styled.div`
    select {
        &.with-error {
            background-color: #ffe3e3;
            border-color: red;
        }
        ${({ disabled }) =>
            disabled &&
            `
            background-color: #F6F8FA;
        `}
    }

    option[disabled] {
        color: lightgray;
    }

    .form-group > label {
        width: 90%;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
`;

const ReduxFieldSelectComponent = (props) => {
    const {
        input: { name, value, onChange },
        meta: { touched, error },
        placeholder,
        label,
        controlConfig,
        appendIcon,
        disabled,
        options = [],
        prepend,
        prependIcon,
        prependSvgIcon,
        className = '',
        classNameFormGroup = '',
        id,
        required,
        ignore = [],
    } = props;
    return (
        <WrapperInput>
            <Form.Group className={classNameFormGroup}>
                {label && <Form.Label title={label}>{label}</Form.Label>}

                <InputGroup>
                    {(prepend || prependIcon || prependSvgIcon) && (
                        <InputGroup.Prepend>
                            <InputGroup.Text>
                                {prependIcon && <i className={prependIcon} />}
                                {prependSvgIcon && (
                                    <img alt="" src={prependSvgIcon} />
                                )}
                                {prepend}
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                    )}
                    {name && <Element name={`position-${name}`} />}
                    <Form.Control
                        placeholder={placeholder}
                        value={value}
                        onChange={onChange}
                        disabled={disabled}
                        as="select"
                        {...controlConfig}
                        id={id}
                        className={`${className} ${
                            touched && error && 'with-error'
                        } ${
                            required &&
                            (value === null ||
                                value === '' ||
                                ignore.indexOf(value) > -1)
                                ? 'border-danger'
                                : ''
                        }`}
                    >
                        {options.map(
                            ({ label, value, disabled: optionDisabled }) => (
                                <option
                                    key={`select-${value}-${label}`}
                                    value={value}
                                    disabled={optionDisabled}
                                >
                                    {label}
                                </option>
                            )
                        )}
                    </Form.Control>
                    {appendIcon && (
                        <InputGroup.Append>
                            <InputGroup.Text id="inputGroupPrepend">
                                <img alt="" src={appendIcon} />
                            </InputGroup.Text>
                        </InputGroup.Append>
                    )}
                </InputGroup>
                {touched && error && (
                    <LabelError type="error">{error} </LabelError>
                )}
            </Form.Group>
        </WrapperInput>
    );
};

ReduxFieldSelectComponent.propTypes = {
    input: PropTypes.object,
    meta: PropTypes.object,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    controlConfig: PropTypes.object,
    appendIcon: PropTypes.string,
    disabled: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    options: PropTypes.array,
    prepend: PropTypes.string,
    prependIcon: PropTypes.string,
    prependSvgIcon: PropTypes.string,
    className: PropTypes.string,
    classNameFormGroup: PropTypes.string,
    id: PropTypes.string,
    required: PropTypes.bool,
    ignore: PropTypes.array,
    t: PropTypes.func,
};

export default ReduxFieldSelectComponent;
