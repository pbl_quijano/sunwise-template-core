import PropTypes from 'prop-types';
import styled from 'styled-components';

const NotificationWrapper = styled.div`
    display: flex;
    margin: -8px;
    padding-right: 40px;
`;

const NotificationIcon = styled.div`
    align-items: center;
    display: flex;
    justify-content: center;
    text-align: center;
    width: 60px;
    min-width: 60px;
    padding: 8px;

    i {
        background: #fff;
        border-radius: 100%;
        color: #008dff;
        display: inline-block;
        font-size: 16px;
        font-weight: bold;
        height: 35px;
        line-height: 35px;
        width: 35px;
    }

    ${({ variant }) => {
        switch (variant) {
            case 'warning':
                return `background-color: #BFD3DA;
                        i {
                            color: #BFD3DA;
                        }`;
            case 'success':
                return `background-color: #00B667;
                        i {
                            color: #00B667;
                        }`;
            case 'online':
                return `background-color: #2BF2AB;
                        i {
                            color: #2BF2AB;
                        }`;
            case 'offline':
            case 'danger':
                return `background-color: #FA6968;
                        i {
                            color: #FA6968;
                        }`;
            default:
                return `background-color: #008dff;
                        i {
                            color: #008dff;
                        }`;
        }
    }}
`;

const NotificationContent = styled.div`
    color: #003b4b;
    font-size: 12px;
    line-height: 19px;
    padding: 15px;
    span {
        color: #848bab;
        line-height: 15px;
        font-weight: 500;
        letter-spacing: 0;
    }
`;

const ToastNotification = ({ title, body, type }) => {
    let icon = 'fas fa-';

    switch (type) {
        case 'offline':
        case 'warning':
            icon += 'exclamation';
            break;
        case 'success':
            icon += 'check';
            break;
        case 'danger':
            icon += 'minus';
            break;
        case 'online':
            icon += 'wifi';
            break;
        default:
            icon += 'question';
            break;
    }

    return (
        <NotificationWrapper>
            <NotificationIcon variant={type}>
                <i className={icon} />
            </NotificationIcon>

            <NotificationContent>
                <div>
                    <strong>{title}</strong>
                </div>

                <span>{body}</span>
            </NotificationContent>
        </NotificationWrapper>
    );
};

ToastNotification.propTypes = {
    title: PropTypes.string,
    body: PropTypes.string,
    type: PropTypes.string,
};

export default ToastNotification;
