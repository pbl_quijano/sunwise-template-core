import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledInputFieldText = styled.input`
    background: transparent;
    border: none;
    color: #848bab;
    font-size: 14px;
    outline: none;
    padding: 8px;
    text-overflow: ellipsis;
    width: 100%;
`;

const InputFieldText = ({ placeholder, disabled, onChange, value }) => {
    const handleOnChange = (e) => onChange(e);

    return (
        <StyledInputFieldText
            placeholder={placeholder}
            onChange={handleOnChange}
            value={value}
            disabled={disabled}
        />
    );
};

InputFieldText.propTypes = {
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    value: PropTypes.string,
};

export default InputFieldText;
